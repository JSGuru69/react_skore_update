/*jshint node:true*/
module.exports = function(grunt) {
    'use strict';

    var defineEmptyPaths = function(paths) {
        try {
            var content = grunt.file.read('js/skore.js');
            var sandbox = {};
            sandbox.SAAS_MODE = false;
            sandbox.define = function(name) {
                paths[name] = 'empty:'
            };
            sandbox.requirejs = function() {};
            sandbox.requirejs.config = function() {};
            sandbox.require = sandbox.requirejs;

            require('vm').runInNewContext(content, sandbox);
        } catch (e) {
            grunt.fail.fatal(e);
        }
        return paths
    };

    // var UglifyJS = require('grunt-contrib-uglify/node_modules/uglify-js');
    var UglifyJS = require('uglify-js');



    function applyConditionalVars(input, target, options) {
        var defs = {
            DEV: false,
            SAAS_MODE: false
        };
        var modes = {
            web: 'WEB_MODE',
            electron: 'ELECTRON',
            viewer: 'VIEWER_MODE'
        };
        for (var m in modes) {
            if (modes.hasOwnProperty(m)) {
                defs[modes[m]] = m === target
            }
        }
        var ast = UglifyJS.parse(input);
        ast.figure_out_scope();
        ast = ast.transform(UglifyJS.Compressor({
            drop_console: true,
            global_defs: defs,
            dead_code: true
        }));
        return ast.print_to_string(options || {
            beautify: true
        });
    }

    function jsEscape(content) {
        return content.replace(/(['\\])/g, '\\$1')
            .replace(/[\f]/g, "\\f")
            .replace(/[\b]/g, "\\b")
            .replace(/[\n]/g, "\\n")
            .replace(/[\t]/g, "\\t")
            .replace(/[\r]/g, "\\r")
            .replace(/[\u2028]/g, "\\u2028")
            .replace(/[\u2029]/g, "\\u2029");
    }

    function extractTextFromTextModule(content) {
        var text = '';
        try {
            var sandbox = {};
            sandbox.define = function(mod, dep, fn) {
                text = fn();
            };
            require('vm').runInNewContext(content, sandbox);
        } catch (e) {
            grunt.fail.fatal(e);
        }
        if (!text) {
            grunt.fail.fatal(new Error('expect define to be called'));
        }
        return text;
    }

    // load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);

    // set base path
    grunt.file.setBase("app");

    //noinspection JSUnusedGlobalSymbols
    grunt.initConfig({

        clean: {
            options: { force: true },
            all: [
                '.tmp',
                'build',
                '../build'
            ]
        },

        jshint: {
            options: {
                jshintrc: true,
                //reporter: 'jshint-file-reporter',
                ignores: [

                    'js/lib-electron/**/*.js',
                    'js/lib/**/*.js',
                ]
            },
            all: [
                'js/**/*.js'
            ]
        },

        jscs: {
            src: ["js/**/sk*.js", "js-electron/**/*", "!js/**/*-spec.js", "background.js"],
            options: {
                preset: "jquery",
                verbose: true, // If you need output with rule names http://jscs.info/overview.html#verbose
                fix: true, // Autofix code style violations when possible.
                requireCurlyBraces: [ "if" ],
                maximumLineLength : {
                    value : 150,
                    allExcept : ["comments"]
                },
                // "validateQuoteMarks": { "mark": "\"", "escape": true },
                requireCamelCaseOrUpperCaseIdentifiers : false,
                validateLineBreaks: false,
                requirePaddingNewlinesBeforeKeywords: false,
                requirePaddingNewlinesInBlocks: { "open": true, "close": true },
                disallowMultipleLineBreaks: true,
                requireCapitalizedComments: false

            }
        },


        requirejs: {
            js: {
                options: {
                    findNestedDependencies: true,
                    baseUrl: 'js',
                    optimize: 'none',
                    include: ['skore'],
                    out: '.tmp/<%= target %>/js/skore.js',
                    paths: defineEmptyPaths({
                        root: '../',
                        text: 'lib/text'
                    }),
                    stubModules: ['text'],
                    onBuildRead: function(moduleName, path, content) {
                        return applyConditionalVars(content, grunt.config('target'));
                    },
                    onBuildWrite: function(moduleName, path, content) {
                        if (/^text!.*\.html$/.test(moduleName)) {
                            // var minify = require('grunt-contrib-htmlmin/node_modules/html-minifier').minify;
                            content = extractTextFromTextModule(content);
                            // content = minify(content, {
                            //     removeComments: false,
                            //     collapseWhitespace: true,
                            //     minifyCSS: true,
                            //     minifyJS: true
                            // });
                            console.log(content);
                            content = jsEscape(content);
                            content = "define('" + moduleName + "',[],function () { return '" + content + "';});";
                        }
                        return content;
                    }
                }
            }
        },

        htmlmin: {
            options: {
                removeComments: false,
                collapseWhitespace: true
            },
            index: {
                'build/<%= target %>/index.html': '../build/<%= target %>/index.html',
                'build/<%= target %>/template_printPreview.html': '../build/<%= target %>/template_printPreview.html',
                'build/<%= target %>/template_imageExport.html': '../build/<%= target %>/template_imageExport.html',
                'build/<%= target %>/template_exportHtml.html': '../build/<%= target %>/template_exportHtml.html'
            }
        },

        uglify: {
            options: {
                compress: {
                    drop_console: true,
                    dead_code: true
                }
            }
        },

        useminPrepare: {
            html: [
                'index.html',
                'template_printPreview.html',
                'template_imageExport.html'],
            options: {
                dest: '../build/<%= target %>',
                staging: '.tmp/<%= target %>'
            }
        },

        usemin: {
            html: [
                '../build/<%= target %>/index.html',
                '../build/<%= target %>/template_printPreview.html',
                '../build/<%= target %>/template_imageExport.html'
            ],
            options: {
                blockReplacements: {
                    script: function(block) {

                        return '<script src="' + block.dest + '"></script>';
                    },
                    bust: function() {
                        // replace with hash of app.js?
                        var s = new Date().toJSON(),
                            v = s.slice(0, 4) + s.slice(5, 7) + s.slice(8, 13) + s.slice(14, 16);
                        return '<script>requirejs.config({urlArgs:\'' + v + '\'})</script>'
                    },
                    optimise: function(block) {
                        var re = /<script\b[^>]*>([\s\S]*?)<\/script>/gm;

                        var match, orig = block.raw.join('\n'),
                            code = [];
                        //jshint -W084
                        while (match = re.exec(orig)) {
                            code.push(match[1]);
                        }
                        code = code.join(';');

                        code = applyConditionalVars(code, grunt.config('target'), {});

                        return code ? '<script>' + code + '</script>' : ''
                    }
                }
            }
        },

        compress: {
            cws: {
                options: {
                    archive: './cws.zip',
                    mode: 'zip'
                },
                files: [{
                    src: './cws/**'
                }]
            }
        },

        build: {
            web: {},
            electron: {},
            viewer:  {}
        },

        // karma: {
        //     unit: {
        //         configFile: 'karma.conf.js',
        //         singleRun: true
        //     }
        // },

        "replace": {
            build: {
                src: ['../build/viewer/index.html'],             // source files array (supports minimatch)
                overwrite: true,
                replacements: [{
                    from: '<link rel="stylesheet" href="styles/skEditor.css">',                   // string replacement
                    to: ''
                }, {
                    from: '<script src="js/vendorEditor.js"></script>',
                    to: ''
                }]
            }
        }

        // run_grunt: {
        //     options: {
        //         minimumFiles: 1
        //     },
        //     build: {
        //         options: {
        //             log: true,
        //             process: function(res){
        //                 if (res.fail){
        //                     res.output = 'new content'
        //                     grunt.log.writeln('bork bork', res);
        //                 }
        //             },
        //             task: ['build:<%= target %>']
        //         },
        //         src: [ '../Gruntfile_analytics.js']
        //     },
        // }
    });

    grunt.registerMultiTask('build', function() {

        grunt.config('target', this.target);

        grunt.config(

            'clean.target',
            [
                '.tmp/<%= target %>',
                'build/<%= target %>',
                '../build/<%= target %>'
            ]
        );

        grunt.config('copy.target', {
            nonull: true,
            expand: true,
            dest: '../build/<%= target %>',
            src: [
                'index.html',
                'favicon.ico',

                /* mxgraph */
                'mxgraph/css/**',
                'mxgraph/images/**',
                'mxgraph/resources/**',
                'mxgraph/js/mxClient.min.js',

                (this.target !== "viewer" ? [
                    // 'js/lib/datatables.min.js',
                    'js/lib/bootstrap3-typeahead.js'
                    ] : []
                ),

                'package.json',

                /* electron template stuffs & examples */
                (this.target === "electron" ? [
                    'background.js',
                    'template_exportHtml.html',
                    'template_imageExport.html',
                    'js/lib-electron/**',
                    'js-electron/**',
                    'js/demoContent/**',
                    'node_modules/**'
                    ] : []
                ),
                'template_printPreview.html',

                'styles/images/*',

                'styles/**/*', '!styles/**/*.css',

                'images/**/*'
            ]
        });

        grunt.config('concat.generated', null);
        grunt.config('cssmin.generated', null);
        grunt.config('uglify.generated', null);

        grunt.config('uglify.rjs', {
            src: '.tmp/<%= target %>/js/skore.js',
            dest: '../build/<%= target %>/js/skore.js'
        });


        grunt.config('cacheBust.target', {

            options: {
                baseDir : '../build/<%= target %>/',
                assets: ['js/**']
            },
            src: [
                '../build/<%= target %>/analytics.html',
                '../build/<%= target %>/index.html']

        });

        grunt.task.run([
            'jshint',
            'jscs',
            'clean:target',
            'copy:target',
            'useminPrepare',
            'concat:generated',
            'cssmin:generated',
            'uglify:generated',
            'requirejs',
            'uglify:rjs',

            'usemin',
            'htmlmin'
        ]);

        // if not viewer, we create the "analytics" again
        // if (this.target !== 'viewer') {
        //     grunt.task.run('run_grunt');
        // }

        // bust the files
        grunt.task.run("cacheBust:target");

        if ( this.target === "viewer" ) {

            // will remove manually the editor stuffs
            grunt.task.run("replace");

        }
    });
};
