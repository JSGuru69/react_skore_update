
# skoreapp

Graph viewer & editor

# Get started

`$npm install` shoud do the trick

# Architecture

There is Skore --> the graph editor

And analytics --> the analytics module.

The analytics is opened in an iframe. On build, this is built separately with a separate grunt file ( i haven't managed to make it in the same)

## Architecture of Skore

### skGraph

Displays a static graph

### skGraphEditor

Extends skGraph to add editing features (text editor)

### skNavigator

Related to navigating in the graph (enterDetails, etc. )

### skNavigatorUI / skMenu

Related to the menus and everything related to the "chrome" of Skore

### skGraphEditorUI

Extends skNavigatorUI for grapheditor chrome

# Test develoment

## make clean code

`npm run verify`

will do jshint and jscs (coding style) for every file

## browser

`http-server .` and visit : localhost:8080/app

## desktop

test development

`npm start`

test built code (without packaging for release)

`grunt build:electron && npm run check`

it's the closest you can have to the final version of the software

# Release

## desktop

Copy or install certificate in right folder (todo : instructions) for code signing

`npm run release`

will create build of software for current platform

## viewer

`grunt build:viewer`

output is in build/viewer

## web mode (with editor)

`grunt build:web`

- output build/web

# Deploy (FTP)

`npm run deploy dev|prod' will deploy to the appropriate servers.

## Dev

* https://justskore.it/dev/ editor | viewer
* https://www.skoreappdev.com/ editing | viewer

## Prod

* https://justskore.it/ editor | viewer
* https://app.justskore.it/ editing | viewer

# library customization

jquery typeahead

I had to customize because the first declaration doesn't work with modules.export so i remove the first check

bootstrap

had to remove color in print

```
@media print {
  *,
  *:before,
  *:after {
    /*color: #000 !important; Skore commented */
```
