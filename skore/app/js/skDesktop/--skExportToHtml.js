
define( function( require ) {

    "use strict";

    var html = require( "text!root/template_exportHtml.html" );

    return function( xml, thumbnail ) {

        return html.replace( "{%xml%}", xml ).replace( "{%thumbnail%}", thumbnail );

    };

} );
