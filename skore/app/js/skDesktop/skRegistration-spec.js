define( [ "Injector" ], function( Injector ) {

    "use strict";

    describe( "skRegistration", function() {

        var injector = new Injector();

        beforeEach( injector.reset );

        var saveAppshell, menu;

        beforeEach( function() {

            saveAppshell = window.appshell;
            window.appshell = {
                app: jasmine.createSpyObj( "app", [ "registerLicenseObserver", "setMenuItemState" ] )
            };
            window.appshell.app.setMenuItemState.and.callFake( function( item, enabled ) {

                menu[ item ] = enabled;

            } );
            menu = {};

        } );
        afterEach( function() {

            window.appshell = saveAppshell;

        } );

        var $;
        var skRegistration;
        var skShell;

        beforeEach( injector.run(
            function( _$ /*jquery*/,
                      _skRegistration /*skDesktop/skRegistration*/,
                      _skShell /*skShell*/) {

                $ = _$;
                skRegistration = _skRegistration;
                skShell = _skShell;

            }
        ) );

        function expectDisabledMenu() {

            expect( menu[ "native.save" ] ).toBe( false );
            expect( menu[ "native.saveAs" ] ).toBe( false );

        }

        function expectEnabledMenu() {

            expect( menu[ "native.save" ] ).toBe( true );
            expect( menu[ "native.saveAs" ] ).toBe( true );

        }

        it( "should register license observer", function() {

            skRegistration();
            expect( window.appshell.app.registerLicenseObserver.calls.count() ).toBe( 1 );
            expect( window.appshell.app.registerLicenseObserver ).toHaveBeenCalledWith( jasmine.any( Function ) );

        } );

        it( "should initially disable menus", function() {

            skRegistration();
            expectDisabledMenu();

        } );

        describe( "licenseObserver", function() {

            var licenseObserver;

            beforeEach( function() {

                skRegistration();
                licenseObserver = window.appshell.app.registerLicenseObserver.calls.first().args[ 0 ];
                jasmine.getFixtures().set( '<p class="skRegistration"><span class="skDemoDaysLeft"></span></p>' );
                $( ".skRegistration" ).hide();
                skShell.hint = jasmine.createSpyObj( "hint", [ "hey" ] );
                skShell.hint.container$ = $();

            } );

            function callLicenseObserver( options ) {

                var args = [];

                args.push( options.error || 0 );
                args.push( options.daysOfUse );
                args.push( options.isTrial );

                if ( options.expiresOn ) {

                    args.push( options.expiresOn );
                    args.push( options.timeIsUp );

                }
                licenseObserver.apply( null, args );
                var call = skShell.hint.hey.calls.mostRecent();

                if ( call ) {

                    // Hey calls callback via timeout, simulate timeout by calling callback after licenseObserver
                    var cb = call.args[ 4 ];

                    if ( cb ) {

                        cb.apply( call.object, call.args );

                    }

                }

            }
            var expiresOn = new Date( 2016, 10, 15 );

            it( "should handle the registered version", function() {

                callLicenseObserver( { daysOfUse: 100, isTrial: false } );
                expect( $( ".skRegistration" ) ).toBeHidden();
                expectEnabledMenu();

            } );

            describe( "trial period", function() {

                it( "should handle the first day of usage", function() {

                    callLicenseObserver( { daysOfUse: 0, isTrial: true } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo: 35 days remaining." );
                    expectEnabledMenu();

                } );

                it( "should handle several days of usage", function() {

                    callLicenseObserver( { daysOfUse: 10, isTrial: true } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo: 25 days remaining." );
                    expectEnabledMenu();

                } );

                it( "should handle the usage beyond the trial period", function() {

                    callLicenseObserver( { daysOfUse: 35, isTrial: true } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expired, you cannot save anymore." );
                    expectDisabledMenu();

                } );

                it( "should handle transition from the trial to an extended trial", function() {

                    callLicenseObserver( { daysOfUse: 0, isTrial: true } );
                    callLicenseObserver( { daysOfUse: 0, isTrial: true, expiresOn: expiresOn, timeIsUp: false } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expires on Tue Nov 15 2016" );
                    expectEnabledMenu();

                } );

                it( "should handle transition from the expired trial to an extended trial", function() {

                    callLicenseObserver( { daysOfUse: 100, isTrial: true } );
                    callLicenseObserver( { daysOfUse: 100, isTrial: true, expiresOn: expiresOn, timeIsUp: false } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expires on Tue Nov 15 2016" );
                    expectEnabledMenu();

                } );

                it( "should handle transition from the expired trial to a registered version", function() {

                    callLicenseObserver( { daysOfUse: 100, isTrial: true } );
                    callLicenseObserver( { daysOfUse: 100, isTrial: false } );
                    expect( $( ".skRegistration" ) ).toBeHidden();
                    expectEnabledMenu();

                } );

            } );

            describe( "extended trial", function() {

                it( "should handle the extended trial (1)", function() {

                    callLicenseObserver( { daysOfUse: 0, isTrial: true, expiresOn: expiresOn, timeIsUp: false } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expires on Tue Nov 15 2016" );
                    expectEnabledMenu();

                } );

                it( "should handle the extended trial (2)", function() {

                    callLicenseObserver( { daysOfUse: 5, isTrial: true, expiresOn: expiresOn, timeIsUp: false } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expires on Tue Nov 15 2016" );
                    expectEnabledMenu();

                } );

                it( "should handle the extended trial (3)", function() {

                    callLicenseObserver( { daysOfUse: 100, isTrial: true, expiresOn: expiresOn, timeIsUp: false } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expires on Tue Nov 15 2016" );
                    expectEnabledMenu();

                } );

                it( "should handle the usage beyond the extended trial (1)", function() {

                    callLicenseObserver( { daysOfUse: 5, isTrial: true, expiresOn: expiresOn, timeIsUp: true } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expired, you cannot save anymore." );
                    expectDisabledMenu();

                } );

                it( "should handle the usage beyond the extended trial (2)", function() {

                    callLicenseObserver( { daysOfUse: 100, isTrial: true, expiresOn: expiresOn, timeIsUp: true } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expired, you cannot save anymore." );
                    expectDisabledMenu();

                } );

                it( "should handle transition from the expired extended trial to an another extended trial", function() {

                    var expiresOn2 = new Date( 2017, 10, 15 );

                    callLicenseObserver( { daysOfUse: 100, isTrial: true, expiresOn: expiresOn, timeIsUp: true } );
                    callLicenseObserver( { daysOfUse: 100, isTrial: true, expiresOn: expiresOn2, timeIsUp: false } );
                    expect( $( ".skRegistration" ) ).toBeVisible();
                    expect( $( ".skDemoDaysLeft" ) ).toContainText( "Demo expires on Wed Nov 15 2017" );
                    expectEnabledMenu();

                } );

                it( "should handle transition from the extended trial to a registered version", function() {

                    callLicenseObserver( { daysOfUse: 100, isTrial: true, expiresOn: expiresOn, timeIsUp: false } );
                    callLicenseObserver( { daysOfUse: 100, isTrial: false } );
                    expect( $( ".skRegistration" ) ).toBeHidden();
                    expectEnabledMenu();

                } );

                it( "should handle transition from the expired extended trial to a registered version", function() {

                    callLicenseObserver( { daysOfUse: 100, isTrial: true, expiresOn: expiresOn, timeIsUp: true } );
                    callLicenseObserver( { daysOfUse: 100, isTrial: false } );
                    expect( $( ".skRegistration" ) ).toBeHidden();
                    expectEnabledMenu();

                } );

            } );

        } );

    } );

} );
