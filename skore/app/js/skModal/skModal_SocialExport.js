
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skShell = require( "skShell" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );
    var skThumbnail = require( "skNavigatorUI/skThumbnail" );
    var skUtils = require( "skUtils" );

    function skModal_SocialExport( navigatorUI ) {

        console.log( "skModal_SocialExport" );

        if ( ELECTRON ) {

            var ftp;
            var fsj = window.requireNode( "fs-jetpack" );

            /*
             * Get config
             *
             */

            var config = fsj.read( window.requireNode( "electron" ).remote.app.getPath( "home" ) + "/skoreFtpForSocialConfig.json", "json" );

            if ( config ) {

                var ipcRenderer = window.requireNode( "electron" ).ipcRenderer;
                var JSFtp = window.requireNode( "jsftp" );

                /*
                 * Ftp connection
                 *
                 */

                var startFTPConnection = function startFTPConnectionF() {

                    if ( !ftp ) {

                        ftp = new JSFtp( {
                            host: config.ftpAdress,
                            port: config.ftpPort,
                            user: config.ftpUser,
                            pass: config.ftpPass
                        } );

                    }

                    return ftp;

                };

                var fullUrlHtml, fullUrlImage, nameForExport;

                /*
                 * Call back when html file is saved
                 *
                 */

                var uploadForSocial = function uploadForSocialF( event, status, filePath ) {

                    if ( status === "ok" ) {

                        $( ".socialURLAvailability" ).prepend( $( "<div>HTML file created.</div>" ) );

                        /*
                         * File is ready, now we need to add the header and the function that will show sharing button
                         *
                         */
                        var html = fsj.read( filePath, "utf-8" );

                        // Open grah & twitter shit

                        var headerOpenGraph = "<head>" +
                            '<meta property="og:url"                content="' + fullUrlHtml + '" />' +
                            '<meta property="og:type"               content="website" />' +
                            '<meta property="og:site_name"          content="Skore app" />' +
                            '<meta property="og:title"              content="' + $( "#socialTitle" ).val() + '" />' +
                            '<meta property="og:description"        content="' + $( "#socialDesc" ).val() + '" />' +
                            '<meta property="og:image"              content="' + fullUrlImage.replace( "https", "http" ) + '" />' +
                            '<meta property="og:image:secure_url"   content="' + fullUrlImage + '" />' +
                            '<meta property="og:image:width"        content="' + skShell.tmpImage.getSize().width + '" />' +
                            '<meta property="og:image:height"       content="' + skShell.tmpImage.getSize().height + '" />' +

                            // Facebook
                            '<meta property="fb:app_id"             content="' + config.facebookAppId + '" />' +

                            // Twitter
                            '<meta name="twitter:card"               content="summary_large_image">' +
                            '<meta name="twitter:site"               content="@SkoreApp">' +
                            '<meta name="twitter:creator"            content="' + config.twitterContentCreator + '">' +
                            '<meta name="twitter:creator:id"            content="' + config.twitterContentCreatorID + '">' +
                            '<meta name="twitter:title"         content="' + $( "#socialTitle" ).val() + '">' +
                            '<meta name="twitter:text:title"         content="' + $( "#socialTitle" ).val() + '">' +
                            '<meta name="twitter:description"   content="' + $( "#socialDesc" ).val() + '">' +
                            '<meta name="twitter:text:description"   content="' + $( "#socialDesc" ).val() + '">' +
                            '<meta name="twitter:image"              content="' + fullUrlImage.replace( "https", "http" ) + '">' +

                            // Closing
                            "";

                        html = html.replace( "<head>", headerOpenGraph );

                        // Facebook stuffs
                        if ( config.facebookAppId ) {

                            var fbtext = "<body><script>window.fbAsyncInit = function() {FB.init({appId: '" + config.facebookAppId +
                                "',xfbml: true,version: 'v2.5'});};(function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0];" +
                                "if (d.getElementById(id)) {return;}js = d.createElement(s); js.id = id;" +
                                "js.src = '//connect.facebook.net/en_US/sdk.js';fjs.parentNode.insertBefore(js, fjs);}(document, 'script', " +
                                "'facebook-jssdk'));</script>";
                            html = html.replace( "<body>", fbtext );

                        }

                        // Now add the text content of the process
                        var t = skGraphUtils.getAllProcessTextAsHtml( navigatorUI.graphManager.getCurrentGraph() );

                        console.log( skUtils.renderMarkdown( t ) );

                        html = html.replace( '<div class="textContent"></div>', '<div class="textContent" style="display:none;">' +
                            skUtils.renderMarkdown( t ) + "</div>" );

                        // And now the image

                        html = html.replace( '<div class="imageContent"></div>', '<div class="imageContent" style="display:none;"><img src="' +
                            fullUrlImage + '"/></div>' );

                        // Add google analytics
                        html = html.replace( "</body>", "</body>" +
                        "<script>" +
                            "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){" +
                            "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o)," +
                            "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)" +
                            "})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');" +
                            "ga('create', 'UA-39788147-3', 'auto');" +
                            "ga('require', 'displayfeatures');" +
                            "ga('set', 'forceSSL', true);" +
                            "ga('send', 'pageview');" +
                        "</script>" );

                        // Add the share buttons

                        html = html.replace( "<body>", "<body>" +

                            // For facebook

                            '<div id="fb-root"></div>' +
                                "<script>(function(d, s, id) {" +
                                  "var js, fjs = d.getElementsByTagName(s)[0];" +
                                  "if (d.getElementById(id)) return;" +
                                  "js = d.createElement(s); js.id = id;" +
                                  'js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1157808504250186";' +
                                  "fjs.parentNode.insertBefore(js, fjs);" +
                                '}(document, "script", "facebook-jssdk"));</script>' +

                            // For twitter
                            "<script>window.twttr = (function(d, s, id) {" +
                              "var js, fjs = d.getElementsByTagName(s)[0]," +
                                "t = window.twttr || {};" +
                              "if (d.getElementById(id)) return t;" +
                              "js = d.createElement(s);" +
                              "js.id = id;" +
                              'js.src = "https://platform.twitter.com/widgets.js";' +
                              "fjs.parentNode.insertBefore(js, fjs);" +
                              "t._e = [];" +
                              "t.ready = function(f) {" +
                                "t._e.push(f);" +
                              "};" +
                              "return t;" +
                            '}(document, "script", "twitter-wjs"));</script>' +

                            '<div style="position:absolute; right:10px; top:4px; ">' +

                                // Twitter
                                '<a href="https://twitter.com/share" class="twitter-share-button" data-url="' +
                                fullUrlHtml + '" data-via="skoreApp" data-related="' + config.twitterContentCreator + '">Tweet</a>' +

                                "&nbsp;&nbsp;&nbsp;" +

                                // Facebook
                                '<div class="fb-share-button" data-href="' + fullUrlHtml + '" data-layout="icon"></div>' +

                                "&nbsp;&nbsp;&nbsp;" +

                                // Linked
                                '<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>' +
                                '<script type="IN/Share" data-url="' + fullUrlHtml + '"></script>' +

                            "</div>" +

                            "" );

                        // Rewrite the file...
                        fsj.write( filePath, html );

                        $( ".socialURLAvailability" ).prepend( $( "<div>HTML file customized.</div>" ) );

                        // Now, Upload the file
                        startFTPConnection().put( filePath, "/" + nameForExport + ".html", function( hadError ) {

                            if ( !hadError ) {

                                $( ".socialURLAvailability" ).prepend( $( "<div>HTML file uploaded.</div>" ) );

                                // And the image
                                startFTPConnection().put( skShell.tmpImage.toPng(), "/" + nameForExport + ".png", function( hadError ) {

                                    if ( !hadError ) {

                                        $( ".socialURLAvailability" ).prepend( $( "<div>Image file uploaded.</div>" ) );
                                        $( ".socialURLAvailability" ).prepend( $( "<div>Done.</div>" ) );
                                        $( ".socialURLAvailability" ).prepend(
                                            $( "<div>" + fullUrlHtml + " <a>Copy to clipboard</a></div>" ).on( "click", function() {

                                                var clipboard = window.requireNode( "electron" ).clipboard;
                                                clipboard.writeText( fullUrlHtml );

                                            } ) );
                                        $( ".socialURLAvailability" ).prepend( $( "<div><a>Check image on facebook</a></div>" ).on( "click",
                                            function() {

                                                skUtils.openURL( "https://developers.facebook.com/tools/debug/og/object?q=" + fullUrlHtml );

                                            } ) );

                                        $( ".socialURLAvailability" ).prepend( $( "<div><a>Check card on twitter</a></div>" ).on( "click",
                                            function() {

                                                skUtils.openURL( "https://cards-dev.twitter.com/validator" );

                                            } ) );

                                    }

                                } );

                            }

                        } );

                    }

                };

                return navigatorUI.loadModal(
                    "skModal_socialExport",
                    $( require( "text!skModal/skModal_SocialExport.html" ) ),

                    // shown
                    function() {

                        $( ".skExportUrl" ).val( navigatorUI.getCurrentGraph().getTitle() );

                        // Generate the images and place preview

                        var imageExport = require( "skModal/skModal_SocialExport" );
                        this.iex = new imageExport( navigatorUI );
                        this.im = navigatorUI.imageExportNow( "social" );

                        ipcRenderer.on( "fileSaved", uploadForSocial );

                        skShell.setStatus( "generating Image..." );

                        setTimeout( function() {

                            var img = $( "<img style='margin:0 10px; box-shadow:0 0 14px #8E8E8E;' class='center-block img-responsive' src='" +
                                            skShell.tmpImage.toDataURL() + "' />" );

                            $( ".skSocialImage" ).empty().append( img );

                        }, 3000 );

                        $( ".socialURLAvailability" ).html( "Ready" );

                    },

                    // hidden
                    function() {

                        if ( ftp ) {

                            ftp.raw.quit( function( err ) {

                                if ( err ) {

                                    return console.error( err );

                                }

                                console.log( "FTP : Bye!" );
                                ftp.destroy();

                            } );

                        }

                        ipcRenderer.removeListener( "fileSaved", uploadForSocial );
                        skShell.tmpImage = null;
                        $( ".skSocialImage" ).empty();

                    },

                    // one off
                    function() {

                        $( ".skSocialUrlPrefix" ).html( config.urlPrefix );
                        $( ".skSocialUrlExt" ).html( ".html" );

                        $( ".skExportUrl" ).on( "keyup", function() {

                            $( ".skPublishFileName" ).html( "published name : <strong>" + $( this ).val().replace( / /g, "-" ) +
                                "</strong>. Don't forget to check availability." );

                        } );

                        /*
                         * Counters
                         *
                         */

                        $( "#socialDesc" ).on( "keyup", function() {

                            $( "#helpDescBlock" ).html( " " + $( this ).val().length + " chars / 200" );

                        } );

                        $( "#socialTitle" ).on( "keyup", function() {

                            $( "#helpTitleBlock" ).html( " " + $( this ).val().length + " chars / 70" );

                        } );

                        $( ".skSocialCheckName" ).on( "click", function() {

                            nameForExport = $( ".skExportUrl" ).val().replace( / /g, "-" );

                            $( ".socialURLAvailability" ).prepend( $( "<div>Checking " + nameForExport + ".html" + "</div>" ) );

                            var found = false;

                            startFTPConnection().ls( ".", function( err, res ) {

                                res.forEach( function( file ) {

                                    console.log( file.name );
                                    if ( file.name === nameForExport + ".html" ) {

                                        $( ".socialURLAvailability" ).prepend( $( "<div>" + nameForExport + ".html" + " <b>already exists</b>. " +
                                            "Override is not recommended because image will not be updated by Facebook.</div>" ) );

                                        found = true;

                                        return false;

                                    }

                                } );

                                if ( !found ) {

                                    $( ".socialURLAvailability" ).prepend( $( "<div>" + nameForExport + ": available</div>" ) );

                                }

                            } );

                        } );

                        /*
                         * Save the process as HTML
                         *
                         */

                        $( ".skExportSocialNow" ).on( "click", function() {

                            nameForExport = $( ".skExportUrl" ).val().replace( / /g, "-" );
                            var data = mxUtils.getXml( navigatorUI.getCurrentGraph().getGraphXml() );
                            var app = window.requireNode( "electron" ).remote.app;
                            var filePath = app.getPath( "temp" ) + "com.the-skore.com/" + nameForExport + ".html";
                            var htmlServer = skShell.HTMLServer;
                            var thumbnail = ( new skThumbnail() ).createDataURL( navigatorUI.getCurrentGraph() );
                            fullUrlHtml = config.urlPrefix + nameForExport + ".html";
                            fullUrlImage = config.urlPrefix + nameForExport + ".png";

                            $( ".socialURLAvailability" ).prepend( $( "<div>Prepare HTML file...</div>" ) );

                            ipcRenderer.send( "hereAreTheThingsToSave", JSON.stringify( {
                                filePath: filePath,
                                currentTitle: null,
                                data: data,
                                thumbnail: thumbnail,
                                actionToDoAfter: null,
                                htmlServer: htmlServer,
                                silent: true,
                                mode: "social"
                            } ) );

                        } );

                    }
                );

            }

        }

    }

    return skModal_SocialExport;

} );
