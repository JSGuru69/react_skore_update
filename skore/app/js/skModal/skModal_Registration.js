
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEventObject = require( "mxEventObject" );
    var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );

    function skModal_Registration ( navigatorUI ) {

        var ipcRenderer;

        if ( ELECTRON ) {

            ipcRenderer = window.requireNode( "electron" ).ipcRenderer;

        } else {

            return;

        }

        console.log( "skRegistration" );

        /*jshint validthis:true*/
        var that = this;

        ipcRenderer.removeAllListeners( "checkLicenseAnswer" );
        ipcRenderer.on( "checkLicenseAnswer", function( event, result, expiryDate ) {

            console.log( "license status..." );

            var enableSave = false;

            switch ( result ) {

                case "NOCODE" :

                that.setMessage( "danger", "no code, mate." );

                break;

                case "FREETRIAL" :

                    that.setMessage( "warning", "Free trial, expires " + expiryDate.slice( 0, 15 ) );

                    enableSave = true;

                    // setupBuyMessage();

                break;

                case "FREETRIALEXPIRED" :

                    that.setMessage( "danger", "Free trial expired on " + expiryDate.slice( 0, 15 ) );

                break;

                case "EXPIRED" :

                    that.setMessage( "danger", "Your demo has expired." );

                break;

                case "DEMO" :

                    that.setMessage( "warning", "Demo, expires : " + expiryDate.slice( 0, 15 ) );

                    enableSave = true;

                break;

                case "INVALID" :

                    that.setMessage( "warning", "Invalid license key" );

                break;

                case "GOODFOREVER" :

                    that.setMessage( "success", "Valid code, you rock." );

                    $( ".thanksAndAll" ).html( "<p>Thanks for purchasing Skore app! We LOVE you!" );

                    enableSave = true;

                break;
            }

            navigatorUI.fireEvent( new mxEventObject( "enableSave", "enable", enableSave ) );

            if ( !enableSave ) {

                skShell.skStatus.setAndAction(
                    "Skore license has expired; you will not be able to save your work anymore.",

                    // ActionMessage
                    "Please purchase a license",

                    // ActionFunction
                    function() {

                        skUtils.openURL( "https://www.getskore.com/buy-skore/" );

                    },

                    // Icon
                    null
                    );

                ipcRenderer.send( "enableSave", false );

            } else {

                ipcRenderer.send( "enableSave", true );

            }

        } );

        ipcRenderer.removeAllListeners( "getLicenseCodeAnswer" );
        ipcRenderer.on( "getLicenseCodeAnswer", function( event, code ) {

            $( "#skCode" ).val( code );

        } );

        return navigatorUI.loadModal(
            "skModal_Registration",
            require( "text!./skModal_Registration.html" ),

            // shown
            function() {

                ipcRenderer.send( "getLicenseCode" );

            },

            // hidden
            null,

            // oneoff
            function() {

                if ( ELECTRON ) {

                    var ipcRenderer = window.requireNode( "electron" ).ipcRenderer;

                    ipcRenderer.removeAllListeners( "saveLicenseCodeAnswer" );
                    ipcRenderer.on( "saveLicenseCodeAnswer", function() {

                        skShell.setStatus( "Licence code saved" );

                    } );

                    // var setupBuyMessage = function setupBuyMessageF() {

                    //     $( ".skRegistration" ).show();
                    //     $( ".skBuySkoreButton" ).show().on( "click", function() {

                    //         skUtils.openURL( "https://www.getskore.com/buy-skore/" );

                    //     } );
                    //     $( ".skRegistrationDetails" ).show();
                    //     $( ".skBuyContactUs" ).show().on( "click", function() {

                    //         skUtils.openURL( "mailto:info@getskore.com" );

                    //     } );

                    // };

                    // On button click
                    $( ".skCheckCode" ).on( "click", function( event ) {

                        event.preventDefault();
                        ipcRenderer.send( "checkLicense", $( "#skCode" ).val() );

                    } );

                }

            }
        );

    }

    skModal_Registration.prototype.setMessage = function( severity, message ) {

        $( ".skSubmitMessage div" ).empty().removeClass().addClass( "alert alert-" + severity ).html( message );

    };

    return skModal_Registration;

} );
