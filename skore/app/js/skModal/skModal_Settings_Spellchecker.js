
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var spellchecker = require( "skGraphEditorUI/skSpellChecker" );

    /**
     * Constructs a new page setup dialog.
     */
    return function skModal_Settings_Spellchecker() {

        console.log( "skModal_Settings_Spellchecker" );

        var div = require( "text!./skModal_Settings_Spellchecker.html" );

        $( ".skSettings_spellcheck" ).empty().append( $( div ) );

        $( ".spellcheckbox" ).prop( "checked", spellchecker.isEnabled() );

        $( ".spellcheckbox" ).off( "change" );

        $( ".spellcheckbox" ).on( "change", function() {

            spellchecker.setEnabled( $( this ).is( ":checked" ) );

        } );

    };

} );
