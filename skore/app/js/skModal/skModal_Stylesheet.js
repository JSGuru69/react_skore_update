
define(
    [ "require", "jquery", "mxCell", "mxConstants", "mxUtils", "skConstants", "skShell", "colors", "skUtils",
       "text!skModal/skModal_Stylesheet.html", "lib/bootstrap3-typeahead" ],
    function( require, $, mxCell, mxConstants, mxUtils, skConstants, skShell, colors, skUtils, html ) {

    "use strict";

    var loadMatrix = {
        fontColor: [ "whatbox", "group", "whybox", "note" ],
        fontSize: [ "whatbox", "group", "whybox", "note" ],
        linkColor: [ "whatbox", "group", "whybox", "note" ],
        detailColor: [ "whatbox" ],
        attachmentColor: [ "whatbox" ],
        strokeColor: [ "whatbox", "group", "whybox", "note", "line", "wy2ss" ],
        strokeWidth: [ "whatbox", "group", "whybox", "note", "line", "wy2ss" ],
        dashed: [ "whatbox", "group", "whybox", "note", "line", "wy2ss" ],
        rounded: [ "whatbox", "group", "note" ],
        fillColor: [ "whatbox", "group", "note" ],
        opacity: [ "whatbox", "whybox", "group", "note", "line", "wy2ss" ],
        whoWhatLine: [ "whatbox" ],
        whoWhatLineCSS: [ "whatbox" ],
        resizable: [ "whatbox", "whybox", "note" ],
        width: [ "whatbox", "whybox", "note" ],
        height: [ "whatbox", "whybox", "note" ],
        align: [ "whatbox", "whybox", "note" ]
    };

    var theModal;
    /**
     * Constructs a stylesheet setup dialog.
     *
     * this page can be used to create a stylesheet or change style of given cells
     *
     * A stylesheet will override "only" the default styles and apply it elsewhere. Think CSS file.
     * A style change will override "all" styles for a cells. Think inline
     *
     *
     */
    function skModal_Stylesheet( navigatorUI ) {

        console.log( "load style sheet" );

        /*jshint validthis:true*/
        var that = this;
        this.graphManager = navigatorUI.graphManager;

        var modalStylesheet = navigatorUI.loadModal(

            "skModal_Stylesheet",

            html,

            // on modal shown
            mxUtils.bind( this, function() {

                this.styleObjectForCells = {};

                this.loadtype = $( "#skModal_Stylesheet" ).data( "loadtype" );

                // they might have been hidden earlier
                $( ".skCellTypeTab li" ).show();

                that.loadUIForCell();

                /*
                 * Clear saved stylesheet
                 *
                 */

                // $(".skClearSaved").on("click", function(){

                //     If ( ELECTRON ) {
                //         var jetpack = window.requireNode("fs-jetpack");
                //         var app = window.requireNode("electron").remote.app;
                //         var userDataDir = jetpack.cwd(app.getPath('userData'));
                //         userDataDir.write("stylesheets.xml", "");

                //         SkShell.setStatus("Saved stylesheet cleared");
                //     }
                //     else {
                //         skShell.setStatus("Desktop only");
                //     }
                // })

                /*
                 * Reset to system defaults
                 *
                 */

                // $(".skResetDefault").on("click", function(){

                //     // re-create the base stylesheet
                //     skShell.graph.setStylesheet( skShell.graph.createStylesheet() );
                //     skShell.graph.model.root.stylesheets = null;
                //     // skShell.graph.refresh();
                //     this.stylesheetsManager = skShell.StylesheetsManager = new stylesheetsManager();

                //     That.init();

                // });

            } ), // end on shown

            null,

            // one off
            mxUtils.bind( this, function( myModal ) {

                theModal = myModal;

                console.log( "one off modal ss" );

                var that = this;

                /*
                 *
                 * tabs
                 *
                 */

                $( ".skCellTypeTab a", theModal ).click( function( e ) {

                    // save the current style
                    that.saveStyleForCellType();

                    // change the tab
                    $( ".skCellTypeTab li", theModal ).removeClass( "active" );
                    $( this ).parents( "li" ).addClass( "active" );
                    e.preventDefault();

                    // change the content
                    var cellType = $( this ).data( "skcelltype" );

                    that.loadUIForCell( cellType );

                } );

                /*
                 *
                 * color management
                 *
                 *
                 */

                // make the typeahead on relevant inputs
                $( ".colorTypeahead", theModal ).typeahead( {
                    autoSelect: false,
                    items:"all",
                    minLength:0,
                    source: colors
                } );

                $( ".colorTypeahead", theModal ).on( "change", function() {

                    $( this ).siblings( ".input-group-addon" ).css( { "background-color": $( this ).val() } );

                } );

                /*
                 * Loads color panel at the bottom
                 *
                 */

                var cp = $( ".skColorPanel", theModal );
                var d;

                colors.forEach( function( e ) {

                    d = $( "" +
                        "<div class='col-md-3'>" +
                            "<p class='selectionAllowed' style='background-color:" + e.htmlcode + "; color:black; padding:10px'>" +
                                e.name +
                            "</p>" +
                        "</div>" );
                    cp.append( d );

                } );

                // make it "toggleable"
                $( ".skColorPanelHeading", theModal ).on( "click", function() {

                    $( ".skColorPanelContainer", theModal ).toggle();

                } );

                /*
                 *
                 * apply button
                 *
                 *
                 */

                $( ".skApplyStylesheet", theModal ).on( "click", function() {

                    // Saves the values
                    that.writeStyle( that.sortedCells );

                    // If checkbox checked ....
                    if ( ELECTRON && $( ".skApplyAsDefault", theModal ).is( ":checked" ) ) {

                        // Save in file
                        var jetpack = window.requireNode( "fs-jetpack" );
                        var app = window.requireNode( "electron" ).remote.app;
                        var userDataDir = jetpack.cwd( app.getPath( "userData" ) );

                        userDataDir.write( "stylesheets.xml", mxUtils.getPrettyXml( navigatorUI.getCurrentGraph().model.root.stylesheets ) );

                        skShell.setStatus( "Stylesheet saved for future use" );

                    }

                    // Close the modal
                    $( "#skModal_Stylesheet" ).modal( "hide" );

                } );

                /*
                 * Reset to saved stylesheet
                 *
                 */

                $( ".skResetToSaved", theModal ).on( "click", function() {

                    if ( ELECTRON && Object.keys( that.sortedCells ).length === 0 ) {

                        // Forces read of the saved stylesheet
                        that.graphManager.getCurrentGraph().getStylesheet().loadCustomStylesheet( null, false );

                        that.loadUIForCell( that.currentCellType );

                    } else if ( that.sortedCells ) {

                        // combine all the cells
                        that.graphManager.getCurrentGraph().getStylesheet().applyStyleToCells(
                            that.graphManager.getCurrentGraph().getSelectionCells(), "" );

                        $( "#skModal_Stylesheet" ).modal( "hide" );

                    } else {

                        skShell.setStatus( "not availabed" );

                    }

                } );

                /*
                 *
                 * hide "save as default" in NOT ELECTRON
                 *
                 *
                 */

                if ( !ELECTRON ) {

                    $( ".skApplyAsDefaultContainer", theModal ).hide();

                }

            } )
        );

        return modalStylesheet;

    }

    skModal_Stylesheet.prototype.loadUIForCell = function( cellType ) {

        this.sortedCells = {};

        // when loading only for some cells
        if ( this.loadtype === "stylecell" ) {

            this.sortedCells = getSortedCells( this.graphManager.getCurrentGraph() );

            cellType = Object.keys( this.sortedCells )[ 0 ];

            /*
             * Update buttons
             *
             */

            $( ".skApplyStylesheet", theModal )
                .show()
                .text( "Apply to selected boxes & lines only" );
            $( ".skApplyAsDefaultContainer", theModal )
                .hide();
            $( ".skStylesheetAlert", theModal )
                .show()
                .html( "<p>Customize only selected elements</p>" );

        }

        // load for the graph
        else {

            cellType = cellType || skConstants.whatbox;

            $( ".skStylesheetAlert", theModal )
                .show()
                .html( "<p>You are editing the default stylesheet. Change will apply to all elements</p>" +
                    "<p>To customize <strong>selected elements only</strong>, " +
                    "select them first and click on the counter in the footer.</p>" +
                    "<p>Elements that have been individually customized will not be affected by changes made here." );
            $( ".skApplyStylesheet", theModal ).show().text( "Apply" );

            if ( ELECTRON ) {

                $( ".skApplyAsDefaultContainer" ).show();

            }

        }

        var styleSheetForCellType = this.graphManager.getStyleForCell( null, cellType, null, this.sortedCells[ cellType ] );

        this.currentCellType = cellType;

        // go through all the style attribute for the given cellType and display (see loadMatrix)
        var items = Object.keys( loadMatrix );
        for ( var i = 0; i < items.length; i++ ) {

            var styleAttr = items[ i ] ;

            // is this a valid attribute for the cellType ?
            var okForCellType = loadMatrix[ styleAttr ].includes( cellType );

            // my current input field
            var currentInput  = $( "[data-styleattr='" +  styleAttr + "']", theModal );

            currentInput

                // add marker for later
                .attr( "data-enabledforcelltype", okForCellType )

                // hide it if not ok
                .parents( ".form-group" ).toggle( okForCellType );

            // set the value from the existing stylesset
            if ( okForCellType ) {

                // checkbox to be checked when appropriate
                if ( currentInput.is( ":checkbox" ) ) {

                    currentInput.prop( "checked", ( "" + styleSheetForCellType[ styleAttr ] === "1" ? true : false ) );

                }

                // radio button, ditto
                else if ( currentInput.is( ":radio" ) ) {

                    // reset all
                    currentInput.prop( "checked", false );

                    // choose the right one
                    currentInput.filter( "input[value='" + styleSheetForCellType[ styleAttr ] + "']" ).prop( "checked", true );

                }

                // for normal input
                else {

                    var tmp = styleSheetForCellType[ styleAttr ];

                    if ( currentInput.hasClass( "styleFreeText" ) ) {

                        if ( tmp ) {

                            currentInput.val( decodeURIComponent( tmp ) );

                        }

                    } else {

                        currentInput.val( tmp );

                    }

                    if ( currentInput.hasClass( "colorTypeahead" ) ) {

                        currentInput.siblings( ".input-group-addon" ).css( { "background-color": styleSheetForCellType[ styleAttr ] } );

                    }

                }

            }

        }

        // go through all the "themes" (font, border, fill, size) and hide if empty
        $( ".skStyleTheme", theModal ).each( function( i, theme ) {

            $( theme ).toggle( ( $( "[data-enabledforcelltype='true']", theme ).length > 0 ) );

        } );

    };

    // sort the cells correctly and update the counters

    var getSortedCells = function( graph ) {

        var sortedCells = {};

        $( ".skCellTypeTab li" ).hide();

        graph.getSelectionCells().forEach( function( cell ) {

            var cellType = cell.getCellType();
            if ( sortedCells[ cellType ] === undefined ) {

                sortedCells[ cellType ] = [];

            }
            var i = sortedCells[ cellType ].push( cell );

            // show the tab if items
            $( "a[data-skcelltype='" + cellType + "']", theModal ).parents( "li" ).toggle( i > 0 );

            // update counter
            $( "a[data-skcelltype='" + cellType + "'] .badge", theModal ).text( i );

        } );

        return sortedCells;

    };

    /*
     *
     * get the new user style as a string that will be passed to graphManager
     *
     */

    skModal_Stylesheet.prototype.saveStyleForCellType = function() {

        var styleString = {};

        $( "[data-enabledforcelltype='true']" ).each( function( i, e ) {

            var key = $( e ).data( "styleattr" );
            var val;

            if ( $( e ).is( ":checkbox" ) ) {

                val = $( e ).is( ":checked" ) ? "1" : null;

            } else if ( $( e ).is( ":radio" ) ) {

                val = $( e ).is( ":checked" ) ? $( e ).val() : null;

            } else {

                val = $( e ).val().trim();

                if ( $( e ).hasClass( "styleFreeText" ) ) {

                    val = encodeURIComponent( val ) ;

                }

                if ( $( e ).attr( "type" ) == "number" ) {

                    if ( val !== "" ) {

                        val = parseInt( val, 10 );

                    }

                }

            }

            if ( val !== null && val !== "" ) {

                styleString[ key ] = val;

            }

        } );

        console.log( "style string", this.currentCellType, JSON.stringify( styleString ) );

        if ( Object.keys( styleString ).length ) {

            this.styleObjectForCells[ this.currentCellType ] = styleString;

        }

    };

    skModal_Stylesheet.prototype.saveStylesheet = function() {

        this.graphManager.setStyleSheet( null, this.styleObjectForCells, null, this.sortedCells );

    };

    /*
     * Saves the style created in the form
     *
     * - stylesheet
     *    saves the "variants" with the default style
     *
     * - selection of cells
     *      saves the full style against the cell
     */

    skModal_Stylesheet.prototype.writeStyle = function writeStyleF( ) {

        this.saveStyleForCellType();
        this.saveStylesheet();

    };

    skModal_Stylesheet.prototype.sortedCells = null;
    skModal_Stylesheet.prototype.styleObjectForCells = null;
    skModal_Stylesheet.prototype.graphManager = null;
    skModal_Stylesheet.prototype.currentCellType = null;
    return skModal_Stylesheet;

} );
