define( function( require ) {

	"use strict";

	function skModal_Formatting( navigatorUI ) {

		return navigatorUI.loadModal(
            "skModal_Formatting",
            require( "text!skModal/skModal_Formatting.html" ),
            null, null,
            null
        );

	}

	return skModal_Formatting;

} );
