define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skModal_checkUpdate = require( "text!./skModal_CheckUpdate.html" );
    var skShell = require( "skShell" );

    var checkUrl = "https://www.getskore.com/updates";

    var timestamp = "";

    if ( ELECTRON ) {

        timestamp = window.requireNode( "fs-jetpack" ).read( window.requireNode( "electron" ).remote.app.getAppPath() + "/package.json", "json" )
        .versionDate;

    }

    // Comment out to test, it will load updates file from the root of app folder
    // checkUrl = "app://skore/updates"
    function makeRequest( successFn, errorFn ) {

        $.ajax( {
            url: checkUrl,
            type: "GET",
            dataType: "json",
            cache: false,
            success: function( data ) {

                // Filter out
                var result = [];

                if ( $.isPlainObject( data ) && $.isArray( data.updates ) ) {

                    for ( var i = 0, len = data.updates.length; i < len; i += 1 ) {

                        var update = data.updates[ i ];

                        if ( $.type( update.timestamp ) === "string" && $.type( update.text ) === "string" &&
                            update.timestamp > timestamp ) {

                            result.push( update );

                        }

                    }

                }
                result.sort( function( u1, u2 ) {

                    return u1 > u2 ? -1 : u1 < u2 ? 1 : 0;

                } );
                successFn( result );

            },
            error: function( jqXHR, textStatus, errorThrown ) {

                errorFn( jqXHR, textStatus, errorThrown );

            }
        } );

    }
    var inProgress = false, displayMessageIfUpToDate = false;

    function success( updates ) {

        inProgress = false;

        // Updates is array of {timestamp: <date-time in rfc3339 format>, text: <markdown_text>}
        // for example [(timestamp: "2014-04-21", text: "Example")]
        // only updates with the timestamp greater then the current version timestamp are included
        // updates are sorted from the most recent update to the least recent update
        if ( updates.length > 0 ) {

            /*
             * Add modal
             *
             */

            var m = $( skModal_checkUpdate );

            $( document.body ).append( m );

            $( "#skModalCheckUpdate" ).on( "shown.bs.modal", function() {

                updates.forEach( function( e ) {

                    $( ".skCheckUpdateContent" )
                        .append( $( '<div class="panel panel-default">' +
                            '<div class="panel-heading">' +
                                '<h3 class="panel-title">' + e.timestamp + "</h3>" +
                            "</div>" +
                            '<div class="panel-body">' +
                                e.text +
                            "</div>" +
                        "</div>" ) );

                } );

            } );
            /*
             * Add message to sideBar button
             *
             */

            skShell.skStatus.setAndAction( "Update available", "Show me", function() {

                $( "#skModalCheckUpdate" ).modal( "show" );

            } );

            $( ".skCheckUpdate" ).show();

            $( ".skCheckUpdateLog" ).on( "click", function() {

                $( "#skModalCheckUpdate" ).modal( "show" );

            } );

        } else if ( displayMessageIfUpToDate ) {

            skShell.skStatus.set( "You are using the latest version!" );

        }

    }

    function error( jqXHR, textStatus, errorThrown ) {

        inProgress = false;

        console.log( jqXHR, textStatus, errorThrown );

        // Show error
        //alert('Failed to get updates')
        skShell.skStatus.set( "Failing to check for updates :-(" );

    }

    function doCheck( needToDisplayMessageIfUpToDate ) {

        if ( inProgress ) {

            displayMessageIfUpToDate = displayMessageIfUpToDate || needToDisplayMessageIfUpToDate;

        } else {

            inProgress = true;
            displayMessageIfUpToDate = needToDisplayMessageIfUpToDate;
            makeRequest( success, error );

        }

    }

    function skCheckUpdate( source ) {

        var undef, needToCheck = true, now = ( new Date() ).valueOf();
        var needToDisplayMessageIfUpToDate = true;
        var locStorage = window.localStorage;

        //Noinspection JSUnusedAssignment
        if ( source === undef ) {

            // Automatic check, do not do it too often
            var checked = parseInt( locStorage.getItem( "updates.checked" ), 10 ) || 0;

            needToCheck = ( ( now - checked ) > 24 * 60 * 60 * 1000 ); // > 24 hours
            needToDisplayMessageIfUpToDate = false;

        }

        if ( needToCheck ) {

            locStorage.setItem( "updates.checked", now );
            doCheck( needToDisplayMessageIfUpToDate );

        }

    }

    function checkIfOnline() {

        if ( navigator.onLine ) {

            skCheckUpdate();

        }

    }

    skCheckUpdate.checkOnStartup = function checkOnStartupF() {

        window.setTimeout( checkIfOnline, 3000 ); //Autocheck after 30 seconds

    };

    return skCheckUpdate;

} ); // End define
