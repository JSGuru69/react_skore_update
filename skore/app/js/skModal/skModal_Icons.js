
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skShell = require( "skShell" );

    function skModal_Icons( navigatorUI ) {

        var dragHandles = [];

        return navigatorUI.loadModal(

            "skModal_Icons",
            require( "text!skModal/skModal_Icons.html" ),
            null, null,
            function() {

                dragHandles = [];

                /*
                 * Load the icons
                 * font-awesome 4.5.0
                 *
                 * !!!! change path to font folder in the css
                 *      create faf and lnrf classes
                 *
                 * to update icon : download font-awesome, update array below (sort alpha)
                 *
                 */

                // jscs:disable maximumLineLength

                var icons = [ "500px", "adjust", "adn", "align-center", "align-justify", "align-left", "align-right", "amazon", "ambulance", "american-sign-language-interpreting", "anchor", "android", "angellist", "angle-double-down", "angle-double-left", "angle-double-right", "angle-double-up", "angle-down", "angle-left", "angle-right", "angle-up", "apple", "archive", "area-chart", "arrow-circle-down", "arrow-circle-left", "arrow-circle-o-down", "arrow-circle-o-left", "arrow-circle-o-right", "arrow-circle-o-up", "arrow-circle-right", "arrow-circle-up", "arrow-down", "arrow-left", "arrow-right", "arrow-up", "arrows", "arrows-alt", "arrows-h", "arrows-v", "asl-interpreting", "assistive-listening-systems", "asterisk", "at", "audio-description", "automobile", "backward", "balance-scale", "ban", "bank", "bar-chart", "bar-chart-o", "barcode", "bars", "battery-0", "battery-1", "battery-2", "battery-3", "battery-4", "battery-empty", "battery-full", "battery-half", "battery-quarter", "battery-three-quarters", "bed", "beer", "behance", "behance-square", "bell", "bell-o", "bell-slash", "bell-slash-o", "bicycle", "binoculars", "birthday-cake", "bitbucket", "bitbucket-square", "bitcoin", "black-tie", "blind", "bluetooth", "bluetooth-b", "bold", "bolt", "bomb", "book", "bookmark", "bookmark-o", "braille", "briefcase", "btc", "bug", "building", "building-o", "bullhorn", "bullseye", "bus", "buysellads", "cab", "calculator", "calendar", "calendar-check-o", "calendar-minus-o", "calendar-o", "calendar-plus-o", "calendar-times-o", "camera", "camera-retro", "car", "caret-down", "caret-left", "caret-right", "caret-square-o-down", "caret-square-o-left", "caret-square-o-right", "caret-square-o-up", "caret-up", "cart-arrow-down", "cart-plus", "cc", "cc-amex", "cc-diners-club", "cc-discover", "cc-jcb", "cc-mastercard", "cc-paypal", "cc-stripe", "cc-visa", "certificate", "chain", "chain-broken", "check", "check-circle", "check-circle-o", "check-square", "check-square-o", "chevron-circle-down", "chevron-circle-left", "chevron-circle-right", "chevron-circle-up", "chevron-down", "chevron-left", "chevron-right", "chevron-up", "child", "chrome", "circle", "circle-o", "circle-o-notch", "circle-thin", "clipboard", "clock-o", "clone", "close", "cloud", "cloud-download", "cloud-upload", "cny", "code", "code-fork", "codepen", "codiepie", "coffee", "cog", "cogs", "columns", "comment", "comment-o", "commenting", "commenting-o", "comments", "comments-o", "compass", "compress", "connectdevelop", "contao", "copy", "copyright", "creative-commons", "credit-card", "credit-card-alt", "crop", "crosshairs", "css3", "cube", "cubes", "cut", "cutlery", "dashboard", "dashcube", "database", "deaf", "deafness", "dedent", "delicious", "desktop", "deviantart", "diamond", "digg", "dollar", "dot-circle-o", "download", "dribbble", "dropbox", "drupal", "edge", "edit", "eject", "ellipsis-h", "ellipsis-v", "empire", "envelope", "envelope-o", "envelope-square", "envira", "eraser", "eur", "euro", "exchange", "exclamation", "exclamation-circle", "exclamation-triangle", "expand", "expeditedssl", "external-link", "external-link-square", "eye", "eye-slash", "eyedropper", "facebook", "facebook-f", "facebook-official", "facebook-square", "fast-backward", "fast-forward", "fax", "feed", "female", "fighter-jet", "file", "file-archive-o", "file-audio-o", "file-code-o", "file-excel-o", "file-image-o", "file-movie-o", "file-o", "file-pdf-o", "file-photo-o", "file-picture-o", "file-powerpoint-o", "file-sound-o", "file-text", "file-text-o", "file-video-o", "file-word-o", "file-zip-o", "files-o", "film", "filter", "fire", "fire-extinguisher", "firefox", "flag", "flag-checkered", "flag-o", "flash", "flask", "flickr", "floppy-o", "folder", "folder-o", "folder-open", "folder-open-o", "font", "fonticons", "fort-awesome", "forumbee", "forward", "foursquare", "frown-o", "futbol-o", "gamepad", "gavel", "gbp", "ge", "gear", "gears", "genderless", "get-pocket", "gg", "gg-circle", "gift", "git", "git-square", "github", "github-alt", "github-square", "gitlab", "gittip", "glass", "glide", "glide-g", "globe", "google", "google-plus", "google-plus-square", "google-wallet", "graduation-cap", "gratipay", "group", "h-square", "hacker-news", "hand-grab-o", "hand-lizard-o", "hand-o-down", "hand-o-left", "hand-o-right", "hand-o-up", "hand-paper-o", "hand-peace-o", "hand-pointer-o", "hand-rock-o", "hand-scissors-o", "hand-spock-o", "hand-stop-o", "hard-of-hearing", "hashtag", "hdd-o", "header", "headphones", "heart", "heart-o", "heartbeat", "history", "home", "hospital-o", "hotel", "hourglass", "hourglass-1", "hourglass-2", "hourglass-3", "hourglass-end", "hourglass-half", "hourglass-o", "hourglass-start", "houzz", "html5", "i-cursor", "ils", "image", "inbox", "indent", "industry", "info", "info-circle", "inr", "instagram", "institution", "internet-explorer", "intersex", "ioxhost", "italic", "joomla", "jpy", "jsfiddle", "key", "keyboard-o", "krw", "language", "laptop", "lastfm", "lastfm-square", "leaf", "leanpub", "legal", "lemon-o", "level-down", "level-up", "life-bouy", "life-buoy", "life-ring", "life-saver", "lightbulb-o", "line-chart", "link", "linkedin", "linkedin-square", "linux", "list", "list-alt", "list-ol", "list-ul", "location-arrow", "lock", "long-arrow-down", "long-arrow-left", "long-arrow-right", "long-arrow-up", "low-vision", "magic", "magnet", "mail-forward", "mail-reply", "mail-reply-all", "male", "map", "map-marker", "map-o", "map-pin", "map-signs", "mars", "mars-double", "mars-stroke", "mars-stroke-h", "mars-stroke-v", "maxcdn", "meanpath", "medium", "medkit", "meh-o", "mercury", "microphone", "microphone-slash", "minus", "minus-circle", "minus-square", "minus-square-o", "mixcloud", "mobile", "mobile-phone", "modx", "money", "moon-o", "mortar-board", "motorcycle", "mouse-pointer", "music", "navicon", "neuter", "newspaper-o", "object-group", "object-ungroup", "odnoklassniki", "odnoklassniki-square", "opencart", "openid", "opera", "optin-monster", "outdent", "pagelines", "paint-brush", "paper-plane", "paper-plane-o", "paperclip", "paragraph", "paste", "pause", "pause-circle", "pause-circle-o", "paw", "paypal", "pencil", "pencil-square", "pencil-square-o", "percent", "phone", "phone-square", "photo", "picture-o", "pie-chart", "pied-piper", "pied-piper-alt", "pinterest", "pinterest-p", "pinterest-square", "plane", "play", "play-circle", "play-circle-o", "plug", "plus", "plus-circle", "plus-square", "plus-square-o", "power-off", "print", "product-hunt", "puzzle-piece", "qq", "qrcode", "question", "question-circle", "question-circle-o", "quote-left", "quote-right", "ra", "random", "rebel", "recycle", "reddit", "reddit-alien", "reddit-square", "refresh", "registered", "remove", "renren", "reorder", "repeat", "reply", "reply-all", "retweet", "rmb", "road", "rocket", "rotate-left", "rotate-right", "rouble", "rss", "rss-square", "rub", "ruble", "rupee", "safari", "save", "scissors", "scribd", "search", "search-minus", "search-plus", "sellsy", "send", "send-o", "server", "share", "share-alt", "share-alt-square", "share-square", "share-square-o", "shekel", "sheqel", "shield", "ship", "shirtsinbulk", "shopping-bag", "shopping-basket", "shopping-cart", "sign-in", "sign-language", "sign-out", "signal", "signing", "simplybuilt", "sitemap", "skyatlas", "skype", "slack", "sliders", "slideshare", "smile-o", "snapchat", "snapchat-ghost", "snapchat-square", "soccer-ball-o", "sort", "sort-alpha-asc", "sort-alpha-desc", "sort-amount-asc", "sort-amount-desc", "sort-asc", "sort-desc", "sort-down", "sort-numeric-asc", "sort-numeric-desc", "sort-up", "soundcloud", "space-shuttle", "spinner", "spoon", "spotify", "square", "square-o", "stack-exchange", "stack-overflow", "star", "star-half", "star-half-empty", "star-half-full", "star-half-o", "star-o", "steam", "steam-square", "step-backward", "step-forward", "stethoscope", "sticky-note", "sticky-note-o", "stop", "stop-circle", "stop-circle-o", "street-view", "strikethrough", "stumbleupon", "stumbleupon-circle", "subscript", "subway", "suitcase", "sun-o", "superscript", "support", "table", "tablet", "tachometer", "tag", "tags", "tasks", "taxi", "television", "tencent-weibo", "terminal", "text-height", "text-width", "th", "th-large", "th-list", "thumb-tack", "thumbs-down", "thumbs-o-down", "thumbs-o-up", "thumbs-up", "ticket", "times", "times-circle", "times-circle-o", "tint", "toggle-down", "toggle-left", "toggle-off", "toggle-on", "toggle-right", "toggle-up", "trademark", "train", "transgender", "transgender-alt", "trash", "trash-o", "tree", "trello", "tripadvisor", "trophy", "truck", "try", "tty", "tumblr", "tumblr-square", "turkish-lira", "tv", "twitch", "twitter", "twitter-square", "umbrella", "underline", "undo", "universal-access", "university", "unlink", "unlock", "unlock-alt", "unsorted", "upload", "usb", "usd", "user", "user-md", "user-plus", "user-secret", "user-times", "users", "venus", "venus-double", "venus-mars", "viacoin", "viadeo", "viadeo-square", "video-camera", "vimeo", "vimeo-square", "vine", "vk", "volume-control-phone", "volume-down", "volume-off", "volume-up", "warning", "wechat", "weibo", "weixin", "whatsapp", "wheelchair", "wheelchair-alt", "wifi", "wikipedia-w", "windows", "won", "wordpress", "wpbeginner", "wpforms", "wrench", "xing", "xing-square", "y-combinator", "y-combinator-square", "yahoo", "yc", "yc-square", "yelp", "yen", "youtube", "youtube-play", "youtube-square" ];
                var linearicons = [ "alarm", "apartment", "arrow-down", "arrow-down-circle", "arrow-left", "arrow-left-circle", "arrow-right", "arrow-right-circle", "arrow-up", "arrow-up-circle", "bicycle", "bold", "book", "bookmark", "briefcase", "bubble", "bug", "bullhorn", "bus", "calendar-full", "camera", "camera-video", "car", "cart", "chart-bars", "checkmark-circle", "chevron-down", "chevron-down-circle", "chevron-left", "chevron-left-circle", "chevron-right", "chevron-right-circle", "chevron-up", "chevron-up-circle", "circle-minus", "clock", "cloud", "cloud-check", "cloud-download", "cloud-sync", "cloud-upload", "code", "coffee-cup", "cog", "construction", "crop", "cross", "cross-circle", "database", "diamond", "dice", "dinner", "direction-ltr", "direction-rtl", "download", "drop", "earth", "enter", "enter-down", "envelope", "exit", "exit-up", "eye", "file-add", "file-empty", "film-play", "flag", "frame-contract", "frame-expand", "funnel", "gift", "graduation-hat", "hand", "heart", "heart-pulse", "highlight", "history", "home", "hourglass", "inbox", "indent-decrease", "indent-increase", "italic", "keyboard", "laptop", "laptop-phone", "layers", "leaf", "license", "lighter", "line-spacing", "linearicons", "link", "list", "location", "lock", "magic-wand", "magnifier", "map", "map-marker", "menu", "menu-circle", "mic", "moon", "move", "music-note", "mustache", "neutral", "page-break", "paperclip", "paw", "pencil", "phone", "phone-handset", "picture", "pie-chart", "pilcrow", "plus-circle", "pointer-down", "pointer-left", "pointer-right", "pointer-up", "poop", "power-switch", "printer", "pushpin", "question-circle", "redo", "rocket", "sad", "screen", "select", "shirt", "smartphone", "smile", "sort-alpha-asc", "sort-amount-asc", "spell-check", "star", "star-empty", "star-half", "store", "strikethrough", "sun", "sync", "tablet", "tag", "text-align-center", "text-align-justify", "text-align-left", "text-align-right", "text-format", "text-format-remove", "text-size", "thumbs-down", "thumbs-up", "train", "trash", "underline", "undo", "unlink", "upload", "user", "users", "volume", "volume-high", "volume-low", "volume-medium", "warning", "wheelchair" ];

                // jscs:enable maximumLineLength

                linearicons = linearicons.map( function( e ) {

                    return "l-" + e;

                } );

                var containerForIcons = $( ".skIconList" );
                containerForIcons.empty();

                var createIconButtons = function createIconButtonsF( icons, prefix ) {

                    icons.forEach( function( e ) {

                        var iconToDrag = $( "" +
                            '<div class="col-md-2">' +
                                '<div class="skIconItem iconItem"' +
                                    'data-toggle="tooltip" data-placement="top" title="' + e + '" data-icon="' + e + '">' +

                                    // Adds f for font          v
                                    '<i class="fa ' +
                                        "" + prefix + "f" + " fa-fw fa-2x " + prefix + "-" + ( e.startsWith( "l-" ) ? e.substring( 2 ) : e ) + '">' +
                                    "</i>" +
                                    '<span style="display:none">fa-2x ' + prefix + "-" + e + '"</span>' +
                                "</div>" +
                            "</div>" );

                        containerForIcons.append( iconToDrag );

                        // copy to clipboard
                        if ( ELECTRON ) {

                            $( iconToDrag ).on( "click", function() {

                                window.requireNode( "electron" ).clipboard.writeText( "![icon](" + e + ")" );
                                skShell.setStatus( "short code copied to clipboard" );

                            } );

                        }

                        // save the dragHandles to be easier to enable / disable on events
                        dragHandles.push(
                            navigatorUI.graphManager.makeCardshoeBoxDraggable(
                                iconToDrag[ 0 ],
                                skConstants.stickynote,
                                "![icon 2x](" + e + ")"
                            )
                        );

                    } );

                };

                createIconButtons( icons, "fa" );
                createIconButtons( linearicons, "lnr" );

                // Animation false otherwise the footer flickers on chrome

                $( '#skModal_Icons [data-toggle="tooltip"]' ).tooltip( { container:"body" } );

                /*
                 * Search filter
                 *
                 */

                $( "#skIconFilter" ).on( "keyup", function() {

                    var search = $( this ).val();

                    if ( search.trim() !== "" ) {

                        var elem = $( ".skIconList" ).findElementsContainingText( search );

                        if ( elem.length ) {

                            // Hide all
                            $( ".skIconList > .col-md-2" ).hide();

                            // Just show the ones we want
                            elem.parents( ".col-md-2" ).show();

                            // Display numbers
                            $( ".skIconResult" ).show().html( "" + elem.length + " icon(s) found." );

                        }

                        // No results
                        else {

                            // Show all
                            $( ".skIconList > .col-md-2" ).show();

                            // $(".skIconItem").show();
                            // hide results
                            $( ".skIconResult" ).show().html( "No icon found. Displaying all." );

                        }

                    } else {

                        $( ".skIconList > .col-md-2" ).show();
                        $( ".skIconResult" ).hide();

                    }

                } );

                var setEnabled = function( enabled ) {

                    $( dragHandles ).each( function( i, e ) {

                        e.setEnabled( enabled );

                    } );
                    $( ".iconItem" ).toggleClass( "disabled", !enabled );

                };

                setEnabled( navigatorUI.getCurrentGraph().isEnabled() );

                navigatorUI.getGraphManager().addListener(
                    "graphEnabled",
                    mxUtils.bind( this, function( sender, event ) {

                        // var enabled = event.getProperty( "enabled" );
                        var graph = event.getProperty( "graph" );
                        setEnabled( graph.isEnabled() );

                    }
                ) );

            }
        );

    }

    return skModal_Icons;

} );
