
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skConstants = require( "skConstants" );
    var skUtils = require( "skUtils" );

    return function skSettings_mrt( graph ) {

        /*
         * Settings panel in HTML
         *
         */

        /*jshint validthis:true*/
        var div = $(
            '<div class="panel panel-default">' +
                '<div class="panel-heading">' +
                    '<h3 class="panel-title">Responsibility model</h3>' +
                "</div>" +
                '<div class="panel-body">' +

                    // default
                    '<div class="radio">' +
                        "<label>" +
                            '<input name="formType" data-formtype="' + skConstants.TEMPLATE_BASE + '" type="radio" >' +
                                "Default (multi-roles)" +
                        "</label>" +
                    "</div>" +

                    // RACI
                    '<div class="radio">' +
                        "<label>" +
                            '<input name="formType" data-formtype="' + skConstants.RESP_RACI + '" type="radio" ' +
                                ' aria-describedby="skHelpBlockRACI" >' +
                                    "Enable RACI" +
                        "</label>" +
                        '<span id="skHelpBlockRACI" class="help-block">' +
                            "RACI stands for Responsible Accountable Consulted and Informed" +
                            "</span>" +
                    "</div>" +

                    // RATSI
                    '<div class="radio">' +
                        "<label>" +
                            '<input name="formType" data-formtype="' + skConstants.RESP_RATSI + '" type="radio" ' +
                            ' aria-describedby="skHelpBlockRATSI" >' +
                                "Enable RATSI" +
                        "</label>" +
                        '<span id="skHelpBlockRATSI" class="help-block">' +
                            "RATSI stands for Responsible, Authority, Task, Support & Informed. " +
                            'Read our <a class="skExtLink" href="https://www.getskore.com/roles-responsibilities-in-process-new-use-case/">' +
                                "blog post about RATSI" +
                            "</a> to learn more." +
                            "</span>" +
                    "</div>" +

                    // Custom
                    '<div class="radio">' +
                        "<label>" +
                            '<input name="formType" data-formtype="' + skConstants.TEMPLATE_CUSTOM + '" type="radio" ' +
                                ' aria-describedby="skHelpBlockCustom" >' +
                                "Custom " +
                        "</label>" +
                        '<span id="skHelpBlockCustom" class="help-block">' +
                            '<a class="skBlogCustom">Learn how to setup custom tags here</a>.' +
                            "</span>" +

                    "</div>" +

                    // custom textarea
                    '<div class="customizer" >' +
                        '<textarea class="selectionAllowed form-control" rows="5" aria-describedby="helpTACust"></textarea>' +

                        '<span id="helpTACust" class="help-block">' +
                            "Paste your code above." +
                        "</span>" +
                        '<a class="btn btn-default okCustom" role="button">Save custom template</a>' +
                        '<br/><span class="jsonstatus"></span>' +
                    "</div>" +

                "</div>" +
            "</div>" );

        console.log( "default values..." );

        $( ".skBlogCustom", div ).on( "click", function() {

            skUtils.openURL( "https://www.getskore.com/manual/how-to-add-custom-tags-to-roles-in-skore-app/" );

        } );

        // select the right type
        $( "[data-formtype=" + graph.templateManager.currentTemplateForForm.responsibility + "]", div ).prop( "checked", true );
        $( ".customizer", div ).toggle( graph.templateManager.currentTemplateForForm.responsibility === skConstants.TEMPLATE_CUSTOM );
        $( ".customizer textarea", div ).html( JSON.stringify( graph.templateManager.getTemplate( skConstants.RESPONSIBILITY ), null, 2 ) );

        // select change

        $( "input", div ).on( "change", function() {

            var t = $( this ).data( "formtype" );

            $( ".customizer", div ).toggle( t === skConstants.TEMPLATE_CUSTOM );

            // will load the preview with current tepalte
            if ( t !== skConstants.TEMPLATE_CUSTOM ) {

                graph.templateManager.setTemplateForForm( skConstants.RESPONSIBILITY, t );

                var obj = graph.templateManager.getTemplate( skConstants.RESPONSIBILITY );
                delete obj.isBuiltIn;

                // update the preview
                $( ".customizer textarea", div ).html(
                    JSON.stringify(
                        obj,
                        null,
                        2 ) );

            }

        } );

        var validateJSON = function( text ) {

            try {

              var c = $.parseJSON( text );
              $( ".jsonstatus", div ).html( "JSON ok!" );
              return c;

            }
            catch ( err ) {

                console.error( err );

                $( ".jsonstatus", div ).html( "JSON invalid" );
                return false;

            }

        };

        $( "textarea", div ).on( "keyup", function() {

            $( ".jsonstatus", div ).empty();

        } );

        // validate custom
        $( ".okCustom", div ).on( "click", function() {

            var text = $( "textarea", div ).val();
            if ( validateJSON( text ) ) {

                graph.templateManager.setTemplateForForm(
                    skConstants.RESPONSIBILITY,
                    skConstants.TEMPLATE_CUSTOM,
                    text );

                $( ".jsonstatus", div ).html( $( ".jsonstatus", div ).html() + " Saved!" );

            }

        } );

        $( ".skSettings_mrt" ).append( $( div ) );

    };

} );
