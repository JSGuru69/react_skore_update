
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skConstants = require( "skConstants" );

    /**
     * Constructs a new page setup dialog.
     */
    return function skModal_Settings_HTMLExport() {

        var div = require( "text!./skModal_Settings_HTMLExport.html" );

        $( ".skSettings_htmlExport" ).empty().append( $( div ) );

        $( "#htmlServer" ).val( skConstants.justSkoreItBaseUrl );

        $( ".skSaveHTMLServer" ).on( "click", function() {

            skConstants.justSkoreItBaseUrl = $( "#htmlServer" ).val();

            if ( DEV && $( ".useProdHtmlEmbed" ).is( ":checked" ) ) {

                skConstants.justSkoreItBaseUrl = skConstants.justSkoreItBaseUrlProd;
                $( "#htmlServer" ).val( skConstants.justSkoreItBaseUrl );

            }

        } );

    };

} );
