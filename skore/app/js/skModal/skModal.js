define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skShell = require( "skShell" );

    function skModal( navigatorUI ) {

        /*jshint validthis:true*/
        this.modals = [];
        this.navigatorUI = navigatorUI;

    }

    skModal.prototype.navigatorUI = null;
    skModal.prototype.modals = null;

    skModal.prototype.load = function( name, html, shown, hidden, firstLoadFunction ) {

        if ( !$( "#" + name ).length ) {

            var myModal = $( html );
            $( document.body ).append( myModal );

            if ( shown ) {

                myModal.on( "shown.bs.modal", shown );

            }

            if ( hidden ) {

                myModal.on( "hidden.bs.modal", hidden );

            }

            myModal.on( "hidden.bs.modal", function() {

                myModal.remove();

            } );

            if ( firstLoadFunction ) {

                firstLoadFunction( myModal );

            }

            this.modals[ name ] = myModal;

        }

        return this.modals[ name ];

    };

    // http://stackoverflow.com/questions/14242227/bootstrap-modal-body-max-height-100
    skModal.prototype.fit_modal_body = function fit_modal_bodyF( modal ) {

        console.log( "fit modal body" );

        var body, bodypaddings, header, headerheight, footer, footerheight, height, modalheight;

        header = $( ".modal-header", modal );
        body = $( ".modal-body", modal );
        footer = $( ".modal-footer", modal );
        modalheight = parseInt( modal.css( "height" ), 10 );
        headerheight = parseInt( header.css( "height" ), 10 ) +
            parseInt( header.css( "padding-top" ), 10 ) +
            parseInt( header.css( "padding-bottom" ), 10 );
        footerheight = parseInt( footer.css( "height" ), 10 ) +
            parseInt( footer.css( "padding-top" ), 10 ) +
            parseInt( footer.css( "padding-bottom" ), 10 );
        bodypaddings = parseInt( body.css( "padding-top" ), 10 ) +
            parseInt( body.css( "padding-bottom" ), 10 );
        height = modalheight - footerheight - headerheight - bodypaddings - 5;
        $( ".skXMLTextarea" ).css( "height", "" + height + "px" );

    };

    skModal.prototype.showSideModal = function showSideModalF( modal$, onCloseFunc, position ) {

        var that = this;
        position = position || "right";
        var positionClass  = ( position === "right" ? "sideRight" : "sideLeft" );

        $( ".sideModal." + positionClass ).hide();
        $( document.body ).removeClass( "modal-open" );

        modal$.show();
        $( document.body ).addClass( "modal-open" );

        modal$.trigger( "shown.bs.modal" );

        /*
         * width
         *
         */

        var w = window.localStorage.getItem( "skSideModalWidth_" + modal$.id );

        if ( !w ) {

            w = "430px";

        }

        $( ".modal-dialog", modal$ ).css( { "width": w } );
        $( ".modal-content", modal$ ).css( { "width": w } );

        // Resizable

        if ( !$( ".modal-header", modal$ ).hasClass( "ui-resizable" ) ) {

            var handles = ( position === "right" ? "w" : "e" );

            // Enable resizer
            $( ".modal-content", modal$ ).resizable( {
                minWidth: 300,

                // alsoResize: $(".modal-footer, .modal-body", modal$),
                handles: handles,
                ghost: true,
                helper: "ui-resizable-helper",
                resize: function( event, ui ) {

                    $( ui.helper ).css( {
                        "z-index":"99999"
                    } );

                },
                stop: function( event, ui ) {

                    console.log( ui.size );

                    $( ".modal-dialog, .modal-content", modal$ ).css( {
                        width: ui.size.width + "px"
                    } );

                    if ( position === "right" ) {

                        $( ".modal-dialog, .modal-content", modal$ ).css( {
                            left: ""
                        } );

                    }

                    window.localStorage.setItem( "skSideModalWidth_" + modal$.id, "" + ui.size.width + "px" );

                    that.navigatorUI.refreshNow();

                    that.navigatorUI.getCurrentGraph().sizeDidChange();

                }

            } );

        }

        /*
         * close
         *
         */

        $( ".modal-header .close, .modal-footer .close", modal$ ).on( "click", function() {

            if ( onCloseFunc && ( typeof onCloseFunc === "function" ) && onCloseFunc().allowClose === false ) {

                skShell.setStatus( onCloseFunc().message );

            } else {

                modal$.hide();

                $( document.body ).removeClass( "modal-open" );

                modal$.trigger( "hidden.bs.modal" );

                $( ".modal-header .close, .modal-footer .close", modal$ ).off( "click" );

                that.navigatorUI.refreshNow();

                that.navigatorUI.getCurrentGraph().sizeDidChange();

            }

        } );

        /*
         * Height will be recomputed in event
         *
         */

        that.navigatorUI.refreshNow();

        that.navigatorUI.getCurrentGraph().sizeDidChange();

    };

    return skModal;

} );
