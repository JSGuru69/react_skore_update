// skModal_Splash.js

define( function( require ) {

	"use strict";

	var $ = require( "jquery" );
	var skGraphEditorTutorial = require( "skGraphEditorUI/skGraphEditorTutorial" );
	var skShell = require( "skShell" );

	function skModal_Splash( navigatorUI ) {

		var tutKey = "DONTSHOWAGAINtutorial";

        if ( localStorage.hasOwnProperty( tutKey ) === false || parseInt( localStorage.getItem( tutKey ), 10 ) === false ) {

            // Starts the tutorial after 2 seconds
            window.setTimeout( function() {

                console.log( "will open bloody modal" );

                // Splash screen

				var m = $( require( "text!skModal/skModal_Splash.html" ) );

		        $( document.body ).append( m );

		        $( "#skModal_Splash" ).modal( "show" );

		        $( ".skStartTutorial", m ).on( "click", function() {

		            $( "#skModal_Splash" ).modal( "hide" );
		            skShell.tutorial = new skGraphEditorTutorial( navigatorUI );

		            if ( !DEV && !ELECTRON && ga ) {

		                ga( "send", "event", "SkoreAction", "welcomeScreen", "Start Tutorial" );

		            }

		        } );

		        $( ".skCloseForNow", m ).on( "click", function() {

		            $( "#skModal_Splash" ).modal( "hide" );
		            window.localStorage.setItem( tutKey, true );

		        } );

            }, 2000 );

        }

	}

	return skModal_Splash;

} );
