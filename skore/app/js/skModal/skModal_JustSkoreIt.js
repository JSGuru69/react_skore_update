define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );

    var myData;

    function skModal_JustSkoreIt( navigatorUI ) {

        /*jshint validthis:true */
        this.navigatorUI = navigatorUI;
        var that = this;

        return navigatorUI.loadModal(
            "skModal_JustSkoreIt",
            require( "text!skModal/skModal_JustSkoreIt.html" ),

            // shown
            null,

            // hidden
            null,

            // one off
            function() {

                $( ".jsit_go" ).on( "click", function( event ) {

                    console.log( "jsit go" );

                    // Congratulates
                    skShell.wellDoneMessage.hey( this );

                    // Published online + callback
                    that.justSkoreIt_post( mxUtils.bind( that, that.sucessJSITCallback ) );

                    if ( !DEV && !ELECTRON && ga ) {

                        ga( "send", "event", "SkoreAction", "sideBar", "JustSkoreIt_Go" );

                    }

                    event.stopPropagation();

                } );

            }
        );

    }

    skModal_JustSkoreIt.prototype.sucessJSITCallback = function sucessJSITCallbackF( data ) {

        //Save the url
        myData = data;

        // Create the list item
        var li = $( "" +

            "<li class='list-group-item'>" +
                "<a class='jsit_urlLink'>Open...</a> &nbsp;" +
                "<a style='display:none;' class='jsit_copyClipbard'>Copy link to clipboard</a> &nbsp;" +
                "<a class='jsit_email'>Email link</a> &nbsp;" +
                "<input class='jsit_urlInput selectionAllowed' type='text' />" +
                "<br/>" +
                "<span class='jsit_timeStamp'></span>" +
            "</li>" );

        $( ".skJSITList" ).prepend( li );

        // Timestamp
        $( ".jsit_timeStamp", li ).html( "Published: " + new Date( myData.created_at ).toString() );

        // Make the link
        $( ".jsit_urlLink", li ).on( "click", function() {

            skUtils.openURL( skConstants.justSkoreItBaseUrl + myData.id );

        } );

        // Fill the textbox
        var $url = $( ".jsit_urlInput", li ).val( skConstants.justSkoreItBaseUrl + myData.id );

        // Pre-selects the value
        var $input = $url;

        $input.select();

        // Work around Chrome's little problem (CC : i don't know i just copied this)
        $input.mouseup( function() {

            // Prevent further mouseup intervention
            $input.unbind( "mouseup" );

            return false;

        } );

        // Email
        $( ".jsit_email", li ).on( "click", function() {

            var s = "mailto:?subject=Skore&body=%0D%0AHello, %0D%0AHere is a Skore I just made : " +
                skConstants.justSkoreItBaseUrl + myData.id +
                "%0D%0A%0D%0A Skore app is a simple way to visualize how things work. Take a look for yourself.%0D%0A%0D%0ACheers, ";

            skUtils.openURL( s );

        } );

        // Replace publish text
        $( ".jsit_go" ).html( "publish again (new link!)" );

        // clipboard

        if ( ELECTRON ) {

            $( ".jsit_copyClipbard" ).show();
            var clipboard = window.requireNode( "electron" ).clipboard;

            clipboard.writeText( skConstants.justSkoreItBaseUrl + myData.id );
            skShell.setStatus( "copied to clipboard" );

        }

    };

    skModal_JustSkoreIt.prototype.navigatorUI = null;

    skModal_JustSkoreIt.prototype.justSkoreIt_post = function( callback ) {

        var data = mxUtils.getPrettyXml( this.navigatorUI.getCurrentGraph().getGraphXml() );

        var visibility = false;

        var key = {
            "description": "Created with Skore, see https://www.getskore.com",
            "public": visibility
        };

        var deneme = {};

        deneme[ "gist.skore" ] = {
            content: "" + encodeURIComponent( data ) + ""
        };

        key.files = deneme;

        var type, url;

        // If (existingDataForUpdate) { // update
        //     type = "PATCH";
        //     url = "https://api.github.com/gists/" + existingDataForUpdate.id;
        // } else { // post new
        type = "POST";
        url = "https://api.github.com/gists";

        // }

        return $.ajax( {
            url: url,
            type: type,
            dataType: "json",
            data: JSON.stringify( key ),
            success: function( data ) {

                callback( data );

            },
            error: function() {

                console.error( "error posting to gist" );
                skShell.skStatus.set( "error :-(" );

            }
        } );

    };

    return skModal_JustSkoreIt;

} );
