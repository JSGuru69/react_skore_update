define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEventObject = require( "mxEventObject" );

    function skModal_Settings( navigatorUI ) {

        return navigatorUI.loadModal(
            "skModal_Settings",
            require( "text!./skModal_Settings.html" ),

            // shown
            function() {

                $( ".skSettings_pagesetup, .skSettings_mrt, .skSettings_handoverhighlight" ).empty();

                var graph = navigatorUI.getCurrentGraph();

                var pageSetup = require( "./skModal_Settings_Pagesetup" );

                $( ".skSettings_pagesetup" ).append( pageSetup( null, null, navigatorUI.getCurrentGraph(), function( settings ) {

                    navigatorUI.graphManager.fireEvent( new mxEventObject( "pageSettingsChanged",
                        "graph", navigatorUI.getCurrentGraph(),
                        "settings",  settings
                    ) );

                } ) );

                require( "./skModal_Settings_Handoverhighlight" )( navigatorUI );
                require( "./skModal_Settings_MRT" )( graph );

                if ( ELECTRON && DEV ) {

                    require( "./skModal_Settings_HTMLExport" )( );

                }

                require( "./skModal_Settings_Background" )(  );

                if ( ELECTRON ) {

                    require( "./skModal_Settings_Spellchecker" )();

                }

                if ( !DEV && !ELECTRON && ga ) {

                    ga( "send", "event", "SkoreAction", "sideBar", "Settings" );

                }

            },

            // hidden
            null,

            // one off
            null
        );

    }

    return skModal_Settings;

} );
