
define( function( require ) {

    "use strict";
    var $ = require( "jquery" );

    function skModal_ImageExport( navigatorUI ) {

        if ( ELECTRON ) {

            var remote = window.requireNode( "electron" ).remote;
            var app = remote.app;
            var fsj = window.requireNode( "fs-jetpack" );
            var skGraph = require( "skGraph/skGraph" );
            var skShell = require( "skShell" );

        } else {

            return;

        }

        /*
         * supercharge skNavigatorUI
         *
         */

        navigatorUI.imageExportNow = function imageExportNowF(  whereToSave, modal$ ) {

            console.log( "imageExportNow" );

            var that = this;

            var graph = this.getCurrentGraph();

            if ( graph.getSelectionCells().length ||
                ( graph.getDefaultParent().children !== null &&
                    graph.getDefaultParent().children.length ) ) {

                // Create the page
                var fullHTML = require( "text!root/template_imageExport.html" );

                var res = this.create_html_string_for_image_export( graph );

                fullHTML = fullHTML.replace( "{%svg%}", res.html );

                // Write the html file that will be imageExport

                /*
                 * Get the bloody html and css in the temp directory
                 *
                 */

                skShell.copyFileOutOfElectron(
                    "template_imageExport.html",
                    app.getPath( "temp" ) + "com.the-skore.skore/template_imageExport.html" );
                skShell.copyFileOutOfElectron(
                    "styles",
                    app.getPath( "temp" ) + "com.the-skore.skore/styles" );

                var userDataDir = fsj.cwd( app.getPath( "temp" ) + "com.the-skore.skore" );
                var stateStoreFile = "template_imageExport.html";

                userDataDir.writeAsync( stateStoreFile, fullHTML, { atomic: true } ).then( function( error ) {

                    if ( error ) {

                        throw error;

                    } else {

                        console.log( "ready for screenshot" );

                        var BrowserWindow = window.requireNode( "electron" ).remote.BrowserWindow;

                        var bw = new BrowserWindow( {
                            x: 0,
                            y: 0,
                            width: parseInt( res.width, 10 ),
                            height: parseInt( res.height, 10 ),
                            show: false,
                            frame: false,

                            nodeIntegration: false,
                            transparent: false,
                            enableLargerThanScreen: true,
                            skipTaskbar: true,
                            directWrite: true
                        } );

                        bw.loadURL( "file://" + userDataDir.cwd() + "/" + stateStoreFile );

                        bw.webContents.on( "did-finish-load", function() {

                            window.setTimeout( function() {

                                bw.capturePage( {
                                    x: 0,
                                    y: 0,
                                    width: parseInt( res.width, 10 ),
                                    height: parseInt( res.height, 10 )
                                }, function( image ) {

                                    // SkShell.tmpImageBase64 = image.toDataURL();
                                    skShell.tmpImage = image;

                                    if ( whereToSave == "preview" ) {

                                        // Create Modal to display the image
                                        modal$.modal( "show", image.size );

                                    } else if ( whereToSave == "social" ) {

                                        return image.size;

                                    } else {

                                        that.saveImage( whereToSave );

                                    }

                                    window.setTimeout( function() {

                                        bw.close();

                                    }, 100 );

                                } );

                            }, 300 );

                        } );

                    }

                } ); // End writeFile

            } else {

                skShell.setStatus( "No content to export as image" );

            }

        };

        navigatorUI.create_html_string_for_image_export = function create_html_string_for_image_exportF( graphToExport ) {

            console.log( "create_html_string_for_image_export" );

            // FIXME: handle the case when there are no cells at all

            /* ⓘ info
             *
             * for the rendering to work, i  need to create a new container
             * need to make the container visible so that it generates the
             * box with the right size (otherwise, foreignobject have width and height of 1)
             *
             */

            var container = document.createElement( "div" );

            container.id = "newGraph";

            // container.style.position = "absolute";

            // Container.style.display = "none";
            container.style.width = "1px";
            container.style.height = "1px";
            container.style.top = "100%";
            container.style.overflow = "visible";
            document.body.appendChild( container );

            var newGraph = new skGraph(
                null,
                null,
                null,
                graphToExport.getStylesheet(),
                graphToExport.model.root.templates.cloneNode( true ),
                graphToExport.model.root.roles.cloneNode( true ) );

            newGraph.labelMode = "htmlstring";

            if ( graphToExport.background !== "#ffffff" ) {

                newGraph.background = graphToExport.background;

            }

            // Get the selected cells
            var cells = graphToExport.getSelectionCells();

            if ( cells.length ) {

                $( ".skSelectedCellsOnly" ).text( "Only selected cells used in image." );

            }

            // If not; get all the cells
            if ( !cells.length ) {

                $( ".skSelectedCellsOnly" ).empty().hide();
                cells = graphToExport.model.getChildren( graphToExport.getDefaultParent() );

            }

            cells = newGraph.cloneCells( cells );

            newGraph.addCells( cells );

            // Shift the cells
            var bounds = newGraph.getBoundingBoxFromGeometry( cells, true );

            // newGraph.translateViewer( newGraph.resetToOrigin() );

            newGraph.moveCells( cells, -bounds.x + 15, -bounds.y + 15 );

            newGraph.init( container );

            newGraph.refresh();

            // Does some work on the container to make our lives easier
            var svg = container.getElementsByTagName( "svg" )[ 0 ];

            svg.style.minWidth = "";
            svg.style.minHeight = "";

            // // need to make all the domNode compliants
            // var goodHTML = function(html) {

            //     var doc = document.implementation.createHTMLDocument("");
            //     doc.write(html);

            //     // You must manually set the xmlns if you intend to immediately serialize
            //     // the HTML document to a string as opposed to appending it to a
            //     // <foreignObject> in the DOM
            //     doc.documentElement.setAttribute("xmlns", doc.documentElement.namespaceURI);

            //     var divs = doc.documentElement.querySelectorAll("div");
            //     for ( var i = 0; i < divs.length; i++ ) {
            //         divs[i].setAttribute("xmlns", doc.documentElement.namespaceURI);
            //     }

            //     // Get well-formed markup
            //     return (new XMLSerializer).serializeToString(doc);
            // }

            // var makeGoodSvg = function(element) {

                // object.svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
                // object.svg.setAttribute("width", object.width);
                // object.svg.setAttribute("height", object.height);

            //     $("foreignObject", element).each(function(i, fo){

            //         var good = goodHTML(fo.innerHTML);
            //         var parent= fo.parentElement;
            //         $(fo).empty();
            //         fo.innerHTML = good;
            //     })
            // }

            // var makeImage = function( element ) {

            //     var html = element.outerHTML;

            //     var canvas = document.createElement("canvas");
            //     document.body.appendChild(canvas);

            //     var ctx = canvas.getContext('2d');

            //     var DOMURL = window.URL || window.webkitURL || window;

            //     var img = new Image();
            //     var svg = new Blob([html], {type: 'image/svg+xml'});
            //     var url = DOMURL.createObjectURL(svg);

            //     img.onload = function () {
            //       ctx.drawImage(img, 0, 0);
            //       DOMURL.revokeObjectURL(url);
            //     }

            //     img.src = url;
            //     img.classList.add("imageExport");

            //     $(document.body).append(img);
            // }

            // makeGoodSvg(container);
            // makeImage(container.querySelector("svg"));

            var result = {
                html:   container.innerHTML,
                height: bounds.height + 30,
                width:  bounds.width + 30
            };

            console.log( "w", bounds.width, "h", bounds.height );

            container.innerHTML = "";
            document.body.removeChild( container );
            newGraph = null;

            return result;

        };

        navigatorUI.saveFile = function saveFileF( path ) {

            fsj.writeAsync(
                path,
                skShell.tmpImage.toPng() ) //New Buffer.Buffer( skShell.tmpImageBase64, "base64" ) )
            .then(
                function( err ) {

                    if ( err ) {

                        throw err;

                    } else {

                        if ( path.startsWith( "\\" ) ) {

                            skShell.skStatus.set( "Image saved :-)" );

                        } else {

                            skShell.skStatus.setAndAction( "Image saved :-)", "Open containing folder", function() {

                                window.requireNode( "electron" ).shell.showItemInFolder( path );

                            } );

                        }

                        // SkShell.setStatus("Image saved :) <br/>" + path);

                    }

                }
            );

        };

        navigatorUI.saveImage = function saveImageF( whereToSave ) {

            if ( skShell.allowSave ) {

                var that = this;

                if ( whereToSave == "clipboard" ) {

                    // Create the NativeImage
                    // var nat = remote.require( "native-image" );

                    // Var img = nat.createFromDataUrl("data:image/png;base64," + skShell.tmpImageBase64);
                    // var img = nat.createFromBuffer( new Buffer.Buffer( skShell.tmpImageBase64, "base64" ) );

                    // Copy to clipbard
                    var clipboard = window.requireNode( "electron" ).clipboard;

                    // Bug may 2016... seems to not work directly??

                    // clipboard.writeImage( skShell.tmpImage );

                    // Alternative :

                    clipboard.writeImage(
                        window.requireNode( "electron" ).nativeImage
                        .createFromDataURL( skShell.tmpImage.toDataURL() )
                    );

                    skShell.setStatus( "image in clipboard, ready to be pasted!" );

                } else if ( whereToSave == "desktop" || whereToSave == "file" ) {

                    var path;

                    var tmpD = ( new Date().toString().substr( 0, 24 ) + ").png" ).replace( /:/gi, "-" );

                    if ( whereToSave === "desktop" ) {

                        path = remote.app.getPath( "userDesktop" ) + "/" + "Skore (" + tmpD;
                        that.saveFile( path );

                    } else if ( whereToSave === "file" ) {

                        var dialog = remote.require( "dialog" );
                        var name = "Skore (" + tmpD;

                        dialog.showSaveDialog(
                        {
                            title: name,
                            defaultPath: name,
                            filters: [
                                { name: "Image", extensions: [ "png" ] }
                            ]
                        },
                        function( fileName ) {

                            if ( fileName === undefined ) {

                                return;

                            }
                            that.saveFile( fileName );

                        } );

                    }

                }

            } else {

                skShell.setStatus( "need license to save." );

            }

        };

        return navigatorUI.loadModal(

            // modal
            "skModal_ImageExport",

            // html
            require( "text!skModal/skModal_ImageExport.html" ),

            // shown
            function() {

                window.setTimeout( function() {

                    var img = $( "<img style='box-shadow:0 0 14px #8E8E8E;' class='center-block img-responsive' src='" +
                            skShell.tmpImage.toDataURL() + "' />" );

                    $( ".skImageExportContainer" ).append( img );

                    $( ".skImageDetails" ).html( "" +
                        "width : " + skShell.tmpImage.getSize().width + "px, " +
                        "height : " + skShell.tmpImage.getSize().height + "px " +
                        "<br/>(previewed image above may not be original size)" );

                }, 100 );

                $( ".skImageExportButton" ).toggleClass( "disabled", !skShell.allowSave );

            },

            // hidden
            function() {

                skShell.tmpImage = null;
                $( ".skImageExportContainer" ).empty();

                // SkShell.tmpImageBase64 = null;
                skShell.tmpImage = null;

            },

            // one off
            function() {

                /*
                 * Init buttons sideMenu & modal
                 *
                 */

                $( ".skImageExportButton" ).on( "click", function() {

                    navigatorUI.saveImage( $( this ).data( "target" ) );

                } );

            }
        );

    }

    skModal_ImageExport.prototype.navigatorUI = null;
    return skModal_ImageExport;

} );
