
define( function( require ) {

    "use strict";

    var skShell = require( "skShell" );
    var $ = require( "jquery" );
    var skConstants = require( "skConstants" );
    var spellcheck;

    function skModal_XML( navigatorUI ) {

        var modalXML = navigatorUI.loadModal(
            "skModal_XML",
            require( "text!./skModal_XML.html" ),

            // shown
            function() {

                $( ".skXMLTextarea" ).val( "Loading..." );

                // Disable spell check temporarily
                if ( ELECTRON ) {

                    spellcheck = require( "skGraphEditorUI/skSpellChecker" );
                    spellcheck.pause();

                }

                $( ".skXMLTextarea" ).val( navigatorUI.getXmlForGraph( null, true ) );
                $( ".skXMLTextarea" ).select();

                navigatorUI.modal.fit_modal_body( $( "#skModal_XML" ) );

            },

            // hidden
            function() {

                // Disable spell check temporarily

                if ( ELECTRON ) {

                    spellcheck.resume();

                }

            },

            // loaded
            function() {

                $( ".skXMLApplyChange" ).on( "click", function() {

                    var $btn = $( this ).button( "loading" );

                    skShell.wellDoneMessage.hey( this );
                    $( "#skModal_XML" ).modal( "hide" );

                    navigatorUI.graphManager.openGraph(
                        {
                            xml:$( ".skXMLTextarea" ).val(),
                            source:skConstants.SOURCE_XMLEDIT
                        }
                    );

                    $btn.button( "reset" );

                } );

            } );

        return modalXML;

    }

    return skModal_XML;

} );
