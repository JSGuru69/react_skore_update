
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxPrintPreview = require( "mxPrintPreview" );
    var mxRectangle = require( "mxRectangle" );
    var mxUtils = require( "mxUtils" );
    var skGraph = require( "skGraph/skGraph" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );
    var skUtils = require( "skUtils" );

    // var pageFormatForm_setup = require( "./skModal_Settings_PageSize" );
    var newGraph;
    var pageSettings = {};
    var createDuplicateGraph = true;

    function skModal_Print( navigatorUI ) {

        /*jshint validthis:true*/
        this.navigatorUI = navigatorUI;

        var that = this;

        var sendDataToPrint = function( event ) {

            console.log( "sendDataToPrint", event.data );

            if ( event.data === "ready" ) {

                var html = $( ".mxPages" ).html();
                var e = encodeURI( html );

                var dataToTransfer = {
                    html: e,
                    paperSize: pageSettings.paperSize,
                    printLandscape: pageSettings.landscape
                };

                event.source.postMessage( JSON.stringify( dataToTransfer ), "*" );

                that.clearStuffs();

            }

        };

        // Listens to when the page is ready
        window.removeEventListener( "message", sendDataToPrint, false );
        window.addEventListener( "message", sendDataToPrint, false );

        var modalPrint = navigatorUI.loadModal(

            "skModal_Print",

            require( "text!skModal/skModal_Print.html" ),

            // shown
            mxUtils.bind( this, function() {

                this.graph = this.navigatorUI.getCurrentGraph();

                var pageSetup = require( "./skModal_Settings_Pagesetup" );

                // get the original pageSettings
                pageSettings = $.extend( {}, skGraphUtils.getPageSettings( this.graph ), {
                    grid:false,
                    pageView: true
                } );

                var pSetup = pageSetup(

                    // hide the fieldsw we don't need
                    {
                        pageView: false,
                        grid: false,
                        backgroundColor: false
                    },
                    pageSettings, null, function( settings ) {

                        pageSettings = $.extend( {}, pageSettings, settings );

                    }
                );

                $( ".col-sm-4", pSetup ).removeClass( "col-sm-4" ).addClass( "col-sm-2" );
                $( ".col-sm-8", pSetup ).removeClass( "col-sm-8" ).addClass( "col-sm-10" );

                $( "#skPageSettings" ).empty().append( $( ".form-horizontal", pSetup )[ 0 ] );

                // pageFormatForm = {
                //     getPageSize: function() {},
                //     getPageSizeObject: function() {}
                // }; //pageFormatForm_setup();

                this.clearStuffs();

                console.log( "init print modal" );

                // $( "#skPageSettings" ).empty().append( pageFormatForm.init( this.graph, 2, 10 ) );

                // pageFormat = pageFormatForm.getPageSize();
                // printFormat = pageFormatForm.getPageSizeObject().name;
                // printLandscape = pageFormatForm.getPageSizeObject().landscape;

            } ),

            // hidden
            mxUtils.bind( this, function() {

                window.removeEventListener( "message", sendDataToPrint, false );
                this.clearStuffs();
                this.graph.refresh();

            } ),

            // call back
            mxUtils.bind( this, function() {

                $( ".skModalPrintPreview" ).on( "click", mxUtils.bind( this, function() {

                     // printing graph
                    if ( $( "#skPrintWhatProcess" ).is( ":checked" ) ) {

                        // Duplicate the graph
                        var preview;

                        if ( $( "input[name=skPrintScope]:checked" ).val() === "skCurrentLevel" ) {

                            preview = this.createGraphForPrint( this.graph, this.graph.getDefaultParent(), true );
                            preview.open( null, window );

                        } else if ( $( "input[name=skPrintScope]:checked" ).val() === "skAllLevels" )  {

                            // Get the list of diagrams to print (root + whatboxdetailed)

                            var cells = this.graph.model.filterDescendants( function( cell ) {

                                if ( cell.id == 1 || cell.isWhatboxDetailed() ) {

                                    return true;

                                }

                            }, this.graph.getDefaultParent() );

                            var that = this;

                            cells.forEach( function( e ) {

                                preview = that.createGraphForPrint( that.graph, e, true );
                                preview.open( null, window );

                            } );

                        }

                        // Fetch the content so it's easier to handle
                        if ( $( ".mxPages" ).length === 0 ) {

                            $( document.body ).append( $( "<div class='mxPages'>" ) );

                        }

                        $( "[id^=mxPage-]" ).addClass( "mxPage" ).appendTo( $( ".mxPages" ) );

                    }

                    // attachment
                    if ( $( "#skPrintWhatAtt" ).is( ":checked" ) ) {

                        this.createAttachmentList( this.graph, $( "input[name=skPrintScope]:checked" ).val() === "skCurrentLevel"  );

                    }

                    if ( ELECTRON ) {

                        window.open(
                            window.requireNode( "electron" ).remote.app.getAppPath() + "/template_printPreview.html",
                            "",
                            "autoHideMenuBar=yes"
                        );

                    } else {

                        window.open( "template_printPreview.html" );

                    }

                } ) ) ;

                $( ".skPagePoster" ).on( "change", function() {

                    if ( $( this ).is( ":checked" ) ) {

                        $( "#skPaperPosterNbPages" ).attr( "disabled", false );

                    } else {

                        $( "#skPaperPosterNbPages" ).attr( "disabled", true );

                    }

                } );

            } )
        );

        return modalPrint;

    }

    skModal_Print.prototype.navigatorUI = null;

    skModal_Print.prototype.clearStuffs = function() {

        // Cleanup...
        $( ".newGraph" ).empty().remove();
        $( ".mxPages" ).empty().remove();

        if ( createDuplicateGraph ) {

            newGraph = null;

        }

    };

    skModal_Print.prototype.createAttachmentList = function( graph, currentLevelOnly ) {

        if ( $( ".mxPages" ).length === 0 ) {

            $( document.body ).append( $( "<div class='mxPages'>" ) );

        }

        // Get the cells with detailed view

        var cells;

        if ( currentLevelOnly ) {

            cells = [ graph.getDefaultParent() ];

        } else {

            cells = graph.model.filterDescendants( function( cell ) {

                if ( cell.id == 1 || cell.isWhatboxDetailed() ) {

                    return true;

                }

            }, graph.getDefaultParent() );

        }

        var pageAdded;

        cells.forEach( function( cell ) {

            pageAdded = false;

            if ( cell.children && cell.children.length ) {

                var att_page = $( "" +
                    "<div class='att_page'>"  +
                        "<h1 class='att_title'>" +
                            cell.getBoxText( true, false ) +
                        "</h1>" +
                        "<div class='att_page_content'></div>" +
                    "<div>" );

                cell.children.forEach( function( currentCell ) {

                    if ( currentCell.numberOfAttachments() > 0 ) {

                        if ( !pageAdded ) {

                            $( ".mxPages" ).append( att_page );
                            pageAdded = true;

                        }

                        var panel = $( "" +
                        '<div class="panel panel-default">' +
                            '<div class="panel-heading">' +
                                currentCell.getBoxText( true, false ) +
                            "</div>" +
                            '<ul class="list-group"></ul>' +
                        "</div>" );

                        $( ".att_page_content", att_page ).append( panel );

                        var ul = $( "ul", panel );

                        $( "attachment", currentCell.value ).each( function( i, att ) {

                            // switch( $(att).attr("type") ) {

                                // case "url" :

                                // break;

                                // case "text" :

                                    console.log( "printing attachment ", skUtils.readValue( att ) );

                                    ul.append( $( "" +
                                        '<li class="list-group-item">' +
                                            '<p class="list-group-item-text">' +
                                                skUtils.renderMarkdown( skUtils.readValue( att ) ) +
                                            "</p>" +
                                        "</li>" ) );

                                // break;
                            // }

                        } );

                    }

                } );

            }

        } );

    };

    skModal_Print.prototype.createGraphForPrint = function createGraphForPrintF( graph, parentCell, createDuplicateGraph ) {

        createDuplicateGraph = true;

        if ( createDuplicateGraph ) {

            $( ".newGraph" ).remove();

            var container = document.createElement( "div" );

            container.className = "newGraph";
            container.style.position = "absolute";
            container.style.top = "100%";
            document.body.appendChild( container );

            newGraph = new skGraph(
                null,
                null, null,
                graph.getStylesheet(),
                graph.model.root.templates.cloneNode( true ),
                graph.model.root.roles.cloneNode( true ) );

            newGraph.init( container );

            newGraph.labelMode = "htmlstring";

        } else {

            newGraph = graph;

        }

        var autoOrigin = $( ".skPageFitOne" ).is( ":checked" ) || $( ".skPagePoster" ).is( ":checked" );
        var printScale = 0.9; // to account for borders
        var scale = 1 / graph.pageScale;
        var pf = skGraphUtils.getPageFormat( pageSettings.paperSize, pageSettings.landscape );

        if ( createDuplicateGraph ) {

            // Get the selected cells
            var cells = graph.model.getChildren( parentCell );

            cells = newGraph.cloneCells( cells );

            newGraph.addCells( cells );

            // To be sure (following Rosa's bug)
            newGraph.resetToOrigin( true, pf );

        }

        if ( autoOrigin ) {

            var pageCount = $( ".skPageFitOne" ).is( ":checked" ) ? 1 : parseInt( $( "#skPaperPosterNbPages" ).val(), 10 );

            if ( !isNaN( pageCount ) ) {

                scale = mxUtils.getScaleForPageCount( pageCount, newGraph, pf );

            }

        }

        // var scale = mxUtils.getScaleForPageCount( poster, graph, pageFormatForm.getPageSize() );

        // implement graph scale
        // var printScale = parseInt( $("#skPrintScale").val(), 10 ) / 100;

        // if ( isNaN(printScale) ) {

        //     printScale = 1;

        //     $("#skPrintScale").val("100%");

        // }

        // scale *= 1 / graph.pageScale;

        // Negative coordinates are cropped or shifted if page visible
        // var gb = newGraph.getGraphBounds();
        var border = 0;
        var x0 = 0;
        var y0 = 0;

        pf = mxRectangle.fromRectangle( pf );
        pf.width = Math.ceil( pf.width * printScale );
        pf.height = Math.ceil( pf.height * printScale );
        scale *= printScale;

        // Starts at first visible page
        if ( !autoOrigin && newGraph.pageVisible )
        {

            var layout = newGraph.getPageLayout();
            x0 -= layout.x * pf.width;
            y0 -= layout.y * pf.height;

        }

        // else
        // {
        //     autoOrigin = true;
        // }

        var preview = new mxPrintPreview( newGraph, scale, pf, border, x0, y0 );

        preview.autoOrigin = autoOrigin;
        preview.graph.isPrintPreview = true;

        return preview;

    };

    return skModal_Print;

} );
