
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skUtils = require( "skUtils" );
    var skConstants = require( "skConstants" );

    var wasAttTypePreviously = null;

    function skModal_Attachments( navigatorUI ) {

        /*jshint validthis:true*/
        var that = this;
        this.navigatorUI = navigatorUI;

        return navigatorUI.loadModal(
            "skModal_Attachments",

            require( "text!skModal/skModal_Attachments.html" ),

            // shown
            function() {

                var graph = navigatorUI.getCurrentGraph();
                var cell = $( "#skModal_Attachments" ).data( "cell" );
                var attType = $( "#skModal_Attachments" ).data( "atttype" ) || wasAttTypePreviously || "attachments";
                console.log( "loaded for cell ", cell.id );

                /*
                 * title of the modal
                 *
                 */

                $( ".skAttachmentBoxName" ).empty().html( skUtils.renderMarkdown( cell.getBoxText(), true ) );

                /*
                 * render existing and update search
                 *
                 */

                that.loadForType( attType, graph, cell );

                // $( ".skAttList" ).empty().append( myRenderer.makeForm( null, allAttachments, allAttachments, true, true ) );

                // todo : maybe could be done by events
                if ( navigatorUI.ui.search && navigatorUI.ui.search.prevKeyword ) {

                    $( "#skModal_Attachments .skSearchable" ).highlight( navigatorUI.ui.search.prevKeyword );

                }

                /*
                 * add attachments buttons
                 *
                 */

                if ( WEB_MODE || ELECTRON ) {

                    if ( graph.isEnabled() ) {

                        // buttons to add attachments
                        var possibleFields = that.navigatorUI.graphManager.getAttachmentsType();

                        var btnGrp = $( "" +
                            '<div class="btn-group dropup btn-group-sm">' +
                                '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" ' +
                                'aria-haspopup="true" aria-expanded="false">' +
                                    '<span>New attachment</span> <span class="caret"></span>' +
                                "</button>" +
                                '<ul class="dropdown-menu">' +
                                "</ul>" +
                            "</div>" );

                        $( ".newAttaButtonGroup" ).empty().append( btnGrp );

                        // for each button type
                        possibleFields.forEach( function( fieldName ) {

                            var li = $( "<li></li>" );
                            li.append(
                                $( "<a>" + fieldName + "</a>" ).on( "click", function() {

                                    var form = navigatorUI.graphManager.createNewAttachment( null, cell, fieldName );

                                    $( ".skAttList .skFormContainer" ).append( form.container );

                                    setTimeout( function() {

                                         $( $( ".skAttList .skFormContainer textarea, .skAttList .skFormContainer input[type=text]" )[ 0 ] ).select();

                                    }, 100 );

                                } ) );

                            $( ".dropdown-menu", btnGrp ).append( li ) ;

                        } );

                    }

                }

            },

            // hidden
            null,

            // first load function
            function() {

                /*
                 * button switch
                 *
                 */

                // enable tooltips
                $( '.skAttSwitcher [data-toggle="tooltip"]' ).tooltip( { "container":"body" } );

                $( "[data-attbtn]" ).on( "click", function() {

                    that.loadForType( $( this ).data( "attbtn" ) );
                    $( ".skAttSwitcher button" ).removeClass( "active" );
                    $( this ).toggleClass( "active" );

                    // remove current class of icon & replace with button
                    $( ".skAttCurrentIcon i" ).removeClass();
                    $( ".skAttCurrentIcon i" ).addClass( $( "i", this ).attr( "class" ) );

                } );

                /*
                 * help text
                 *
                 */

                if ( !navigatorUI.getCurrentGraph().isEnabled() &&
                    localStorage.hasOwnProperty( "DONTSHOWAGAINAttachmentMessage" ) === true &&
                    parseInt( localStorage.getItem( "DONTSHOWAGAINAttachmentMessage" ), 10 ) === 1  ) {

                    $( ".skAttPanelTutorial" ).show();
                    $( ".skAttPanelTutorial p" ).text( "Follow these links for guidance notes and templates" );

                    // Hide skAttPanelTutorial if been hidden by user
                    $( ".skAttPanelTutorialClose" ).on( "click", function() {

                        localStorage.setItem( "DONTSHOWAGAINAttachmentMessage", 1 );

                    } );

                } else {

                    $( ".skAttPanelTutorial" ).hide();

                }

            }
        );

    }

    skModal_Attachments.prototype.navigatorUI = null;
    skModal_Attachments.prototype.loadForType = function( type, graph, cell ) {

        graph = graph || this.navigatorUI.getCurrentGraph();
        cell = cell || $( "#skModal_Attachments" ).data( "cell" );

        var form = this.navigatorUI.graphManager.renderAttachments( graph, cell, type );

        wasAttTypePreviously = type;

        if ( type == skConstants.ATTACHMENTS ) {

            $( ".newAttaButtonGroup" ).hide();

        }

        $( ".skAttList" ).empty().append( form.container );

    };

    return skModal_Attachments;

} );
