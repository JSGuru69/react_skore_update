
define( [ "require", "jquery", "mxUtils", "skUtils", "text!skModal/skModal_Surveys.html", "text!skModal/skModal_Survey.html" ],
    function( require, $, mxUtils, skUtils, survsHtml, survHtml ) {

    "use strict";

    // var $ = require( "jquery" );
    // var mxUtils = require( "mxUtils" );
    // var skUtils = require( "skUtils" );

    function skModal_Surveys( navigatorUI ) {

        if ( !$( "#skModal_Surveys" ).length ) {

            // $( document.body ).append( $( require( "text!skModal/skModal_Surveys.html" ) ) );
            $( document.body ).append( $( survsHtml ) );

            // $( document.body ).append( $( require( "text!skModal/skModal_Survey.html" ) ) );
            $( document.body ).append( $( survHtml ) );

        }

        /*jshint validthis:true*/
        var that = this;

        $( "#skModal_Surveys" ).on( "shown.bs.modal", function() {

            that.initForGraph( navigatorUI.getCurrentGraph() );

            $( ".skSurveysList" ).empty();

            if ( navigatorUI.getCurrentGraph().model.root.surveys ) {

                $( "survey", navigatorUI.getCurrentGraph().model.root.surveys ).each( function( i, e ) {

                    $( ".skSurveysList" ).append( $( "<option value='" + skUtils.readValue( e, "slug" ) + "'>" + skUtils.readValue( e, "title" ) +
                        "</option>" ) );

                } );

            }

            $( ".skSurveysList" ).off( "change" );

            $( ".skSurveysList" ).on( "change", function( event ) {

                console.log( "change" );

                that.createSurveyPanel( $( "[slug=" + event.target.value + "]", navigatorUI.getCurrentGraph().model.root.surveys ) );

            } );

        } );

        $( "#skModal_Survey" ).on( "show.bs.modal", function( event ) {

            var button = $( event.relatedTarget ); // Button that triggered the modal

            var surveyID = button.data( "surveyid" ); // Extract info from data-* attributes
            var surveySource = button.data( "surveysource" ); // Extract info from data-* attributes

            var modal = $( this );

            if ( surveySource ) {

                modal.find( "iframe" ).attr( "src", "https://airtable.com/embed/" + surveyID + "?backgroundColor=red" );

            }

            // modal.find('.modal-body input').val(recipient)

        } );

    }

    skModal_Surveys.prototype.initForGraph = function( graph ) {

        if ( !graph.model.root.surveys ) {

            graph.model.root.surveys = mxUtils.createXmlDocument().createElement( "surveys" );

            $( graph.model.root.surveys ).append(
                $( "<survey " +
                    'formid="shr3DjDlBLCcucArE" ' +
                    'source="airtable" ' +
                    'slug="pbs" ' +
                    'title="Identify problems" ' +
                    'description="Identify problems in these activities">' ) );

            $( graph.model.root.surveys ).append(
                $( "<survey " +
                    'source="airtable" ' +
                    'base=""' +
                    'formid="shr10JZNpNU9NK8O5" ' +
                    'viewid="shrStNZo1KZPMixas" ' +
                    'slug="sys" ' +
                    'title="Identify systems" ' +
                    'description="Identify systems in use in the process">' ) );

        }

        return graph.model.root.surveys;

    };

    // skModal_Surveys.prototype.initForCell = function( cell ) {

    // }

    skModal_Surveys.prototype.createSurveyPanel = function( survey ) {

        var panel = $( "" +
            '<div class="panel panel-default">' +
                '<div class="panel-heading">' + survey.attr( "title" ) + "</div>" +
                '<div class="panel-body">' +
                    "<p>" + survey.attr( "description" ) + "</p>" +
                "</div>" +
                '<ul class="list-group">' +
                "</ul>" +
                '<div class="panel-body">' +
                    "<p>" +
                        "<a " +
                            'class="btn btn-default skSurveyAddEntry" ' +
                            'data-toggle="modal" data-target="#skModal_Survey" ' +
                            'data-surveyid="' + survey.attr( "formid" ) + '"' +
                            'data-surveysource="' + survey.attr( "source" ) + '"' +
                            'role="button">' +
                            "Add entry " +
                        "</a>" +
                    "</p>" +
                "</div>" +
            "</div>" +
        "" );

        // var at = require('airtable');
        var Airtable = window.Airtable;

        Airtable.configure( {
            endpointUrl: "https://api.airtable.com",
            apiKey: "keyVtZmRfnMvzJEdX"
        } );
        var base = Airtable.base( "apprtUTKWRoguwDUp" );

        base( "Survey" ).select( {

            // Selecting the first 3 records in Main View:
            view: "Main View",
            filterByFormula: "BoxID = 3"
        } ).eachPage( function page( records, fetchNextPage ) {

            // This function (`page`) will get called for each page of records.

            $( ".list-group" ).append( $( '<li class="list-group-item">' + records.length + " entries</li>" ) );

            // records.forEach(function(record) {
            //     console.log('Retrieved ', record.get('Name'));
            // });

            // To fetch the next page of records, call `fetchNextPage`.
            // If there are more records, `page` will get called again.
            // If there are no more records, `done` will get called.
            fetchNextPage();

        }, function done( error ) {

            if ( error ) {

                console.log( error );

            }

        } );

        $( ".skSurveys" ).empty();

        $( ".skSurveys" ).append( panel );

        // $(".skSurveyAddEntry").on("click", function(){

        //     $(".skSurveyForm iframe").attr("src", "https://airtable.com/embed/" + $(this).data("surveyid") + "?backgroundColor=red");

        // })

    };

    return skModal_Surveys;

} );
