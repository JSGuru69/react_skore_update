define( [ "require", "jquery", "colors", "mxEventObject", "mxUtils", "skShell", "skGraph/skGraphUtils", "skConstants",
    "text!skModal/skModal_Settings_PageSetup.html", "lib/bootstrap3-typeahead" ],
    function( require, $, colors, mxEventObject, mxUtils, skShell, skGraphUtils, skConstants, html ) {

    "use strict";

    /**
     * Constructs a new page setup dialog.
     */
    function skSettings_pagesetup( formParams, pageSettings, graph, callback ) {

        console.log( "skSettings_pagesetup" );

        pageSettings = pageSettings || skGraphUtils.getPageSettings( graph );

        var div = $( html );

        /*
         *
         * page view
         *
         *
         */

        var pageViewForm = $( "[data-skparam='pageView']", div );
        pageViewForm.toggle( !formParams || ( formParams && formParams.pageView ) );
        $( "input", pageViewForm ).attr( "checked", pageSettings.grid ).on( "change", function() {

            callback( { pageView: $( this ).is( ":checked" ) } );
            enableOtherPageSettings( $( this ).is( ":checked" ) );

        } );

        var enableOtherPageSettings = function( enabled ) {

            $( pageScaleForm ).attr( "disabled", enabled ? null : "disabled" );
            $( paperSizeForm ).attr( "disabled", enabled ? null : "disabled" );
            $( landscapeForm ).attr( "disabled", enabled ? null : "disabled" );

        };

        // init
        enableOtherPageSettings( pageSettings.pageViewForm );

        /*
         *
         * grid
         *
         *
         */

        var gridForm = $( "[data-skparam='grid']", div );
        gridForm.toggle( !formParams || ( formParams && formParams.grid ) );
        $( "input", gridForm ).attr( "checked", pageSettings.grid ).on( "change", function() {

            callback( { grid: $( this ).is( ":checked" ) } );

        } );

        /*
         *
         * page Scale
         *
         *
         */

        var pageScaleForm = $( "[data-skparam='pageScale']", div );
        pageScaleForm.toggle( !formParams || ( formParams && formParams.pageScale ) );
        var scale = parseInt( pageSettings.pageScale * 100, 10 );
        $( "input", pageScaleForm ).val( scale + " %" );

        $( ".skPageScaleBtn", pageScaleForm ).on( "click", function( event ) {

            scale = scale + ( $( event.target ).hasClass( "skMinus" ) ? -10 : 10 );

            $( "input", pageScaleForm ).val( "" + scale + " %" );

            callback( { pageScale: ( scale / 100 ) } );

        } );

        /*
         *
         * paper Size
         *
         *
         */

        var paperSizeForm = $( "[data-skparam='paperSize']", div );
        paperSizeForm.toggle( !formParams || ( formParams && formParams.paperSize ) );

        var paperSizeInput = $( "select", paperSizeForm );

        // populate paperSizes
        for ( var i = 0; i < Object.keys( skConstants.paperSizes ).length; i++ ) {

            var name = Object.keys( skConstants.paperSizes )[ i ];

            var option = $( "" +
            "<option>" +
                skConstants.paperSizes[ name ].displayName +
            "</option>" );

            option.attr( "value", skConstants.paperSizes[ name ].name );
            option.attr( "selected", ( pageSettings.paperSize === skConstants.paperSizes[ name ].name ? true : false ) );

            paperSizeInput.append( option );

        }

        paperSizeInput.on( "change", function() {

            callback( { paperSize: $( this ).val() } );

        } );

        /*
         *
         * landscape
         *
         */
        var landscapeForm = $( "[data-skparam='landscape']", div );
        landscapeForm.toggle( !formParams || ( formParams && formParams.landscape ) );

        $( "input", landscapeForm ).attr( "checked", pageSettings.landscape );

        $( "input", landscapeForm ).on( "change", function() {

            callback( { landscape: ( $( this ).is( ":checked" ) ) } );

        } );

        /*
         *
         * background color
         *
         *
         */

        var backgroundColorForm = $( "[data-skparam='backgroundColor']", div );
        backgroundColorForm.toggle( !formParams || ( formParams && formParams.backgroundColor ) );

        var currentBgColor = pageSettings.backgroundColor;

        // Tries to find the human name if it's a html color (likely)
        if ( currentBgColor.startsWith( "#" ) ) {

            colors.some( function( e ) {

                if ( e.htmlcode == currentBgColor.toUpperCase() ) {

                    currentBgColor = e.name;
                    return true;

                }

            } );

        }

        $( "input", backgroundColorForm ).val( currentBgColor ).typeahead( {
            autoSelect: false,
            items:"all",
            minLength:0,
            source: colors
        } );

        $( "button", backgroundColorForm ).on( "click", function() {

            callback( { backgroundColor: $( "input", backgroundColorForm ).val() } );

        } );

        return $( div );

    }

    return skSettings_pagesetup;

} );
