define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    function skModal_FileStack( graph, selectedType ) {

        /*jshint validthis:true*/
        this.graph = graph;

        var html = require( "text!./skModal_FileStack.html" );

        $( document.body ).append( html );

        var that = this;

        $( ".skFSAddFile", html ).on( "click", function() {

            var type = "";
            if ( selectedType ) {

                type = selectedType;

            }

            window.filepicker.pickMultiple( {
                "mimetype": type,
                "container": "window"
            }, function( Blobs ) {

                Blobs.forEach( function( Blob ) {

                    that.saveFile( Blob, true );

                } );

            } );

        } );

    }

    skModal_FileStack.prototype.graph = null;

    skModal_FileStack.prototype.getNode = function() {

        if ( !this.graph.model.root.filesManager ) {

            this.graph.model.root.filesManager = {};

        }

        return this.graph.model.root.filesManager;

    };

    skModal_FileStack.prototype.createSlug = function( filename ) {

        var tmp = filename.substr( 0, filename.lastIndexOf( "." ) ).replace( /[\W_]+/g, "-" ).toLowerCase();

        // TODO : check if SLUG exists

        return tmp;

    };

    skModal_FileStack.prototype.saveFile = function( blob, isNew ) {

        var file = document.createElement( "file" );
        file.setAttribute( "url", blob.url );
        file.setAttribute( "name", blob.filename );
        file.setAttribute( "type", blob.filetype );
        file.setAttribute( "origin", blob.client );
        file.setAttribute( "slug", this.createSlug( blob.filename ) );
        var d = new Date().getTime();
        file.setAttribute( "added", d );
        file.setAttribute( "modified", d );

        // Adds line to table

        if ( isNew ) {

            $( ".fileStackTable" ).append( this.createTableLine( file ) );

        }

    };

    skModal_FileStack.prototype.createTableLine = function( fileNode ) {

        var slug = fileNode.getAttribute( "slug" );

        return $( "" +
            "<tr>" +
                "<td>" +
                    fileNode.getAttribute( "name" ) +
                "</td>" +
                "<td>" +
                    slug +
                "</td>" +
                "<td>" +
                    ( fileNode.getAttribute( "filetype" ).split( "/" )[ 0 ] === "image" ? ( "<img src='" + fileNode.getAttribute( "url" ) + "'>" ) :
                        "" ) +
                "</td>" +
                "<td>" +
                    "<a class='skFSAction' data-slug='" + slug + "' data-fsaction='shortcode'>Copy shortcode</a>" +
                "</td>" +
                "<td>" +
                    "<a class='skFSAction' data-slug='" + slug + "' data-fsaction='delete'>Delete</a>" +
                "</td>" +
                "<td>" +
                    "<a class='skFSAction' data-slug='" + slug + "' data-fsaction='update'>Update</a>" +
                "</td>" +
                "<td>" +
                    "<a class='skFSAction' data-slug='" + slug + "' data-fsaction='changeslug'>Change slug</a>" +
                "</td>" +
            "</tr>" );

    };

    skModal_FileStack.prototype.listFiles = function() {

        var table = $( ".fileStackTable" );

        // var that = this;

        $( this.getNode() ).each( function( i, file ) {

            table.append( this.createTableLine( file ) );

        } );

        $( "td a", table ).on( "click", function() {

            // var action = $(this).data("fsaction");
            // var slug = $(this).data("slug");

        } );

    };

    return skModal_FileStack;

} );
