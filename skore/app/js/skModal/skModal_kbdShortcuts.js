define( function( require ) {

    "use strict";

    function skModal_kdbShortcuts( navigatorUI ) {

        return navigatorUI.loadModal(
            "skModal_kdbShortcuts",
            require( "text!skModal/skModal_kbdShortcuts.html" )
        );

    }

    return skModal_kdbShortcuts;

} );
