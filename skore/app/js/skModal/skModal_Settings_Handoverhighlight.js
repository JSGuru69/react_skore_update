define( function( require ) {

    "use strict";

    var $ = require( "jquery" ),
        mxConstants = require( "mxConstants" ),
        mxUtils = require( "mxUtils" ),
        skConstants = require( "skConstants" ),
        skShell = require( "skShell" );

    return function skSettings_handoverhighlight( navigatorUI ) {

        var graph = navigatorUI.getCurrentGraph();

        var alreadyOn = " ";

        if ( graph.model.root.handoverhighlight ) {

            alreadyOn = " checked ";

        }

        /*
         * Settings panel in HTML
         *
         */
        /*jshint validthis:true*/
        var div = $( '<div class="panel panel-default">' +
            '<div class="panel-heading">' +
                '<h3 class="panel-title">' +
                    "Handover highlight" +
                "</h3>" +
                '<span class="experimental">Experimental</span>' +
            "</div>" +
            '<div class="panel-body">' +

                /*
                 * Checkbox to enable MRT
                 *
                 */
                '<div class="checkbox">' +
                    "<label>" +
                        '<input aria-describedby="skLHDesc" class="skLineHighlight" type="checkbox" ' + alreadyOn + ">" +
                            "Enable handover highlight" +
                    "</label>" +
                "</div>" +
                '<span id="skLHDesc" class="help-block">' +
                    "<ul>" +
                        "<li>Lines & whyboxes connecting activities performed by different roles will be highlighted.</li>" +
                        "<li>If you have enabled multi resources, " +
                            "only the <strong>first role</strong> of an activity is taken into account for handover.</li>" +
                    "</ul>" +
                "</span>" +
            "</div>" +
        "</div>" );

        /*
         * Highlights a line if necessary
         *
         */

        $( ".skLineHighlight", div ).on( "change", function() {

            console.log( "enable skLineHighlight" );

            if ( $( this ).is( ":checked" ) ) {

                graph.model.root.handoverhighlight = mxUtils.createXmlDocument().createElement( "extension" );
                graph.model.root.handoverhighlight.setAttribute( skConstants.VALUE, "handoverhighlight" );

                if ( !graph.extensions ) {

                   // Initiate the extensions
                    var skExtensions = require( "skExtensions/skExtensions" );

                    graph.extensions = new skExtensions( graph );

                }

                graph.extensions.load_handoverhighlight();

                skShell.skStatus.set( "Handover highlight extension enabled" );

                if ( !DEV && !ELECTRON && ga ) {

                    ga( "send", "event", "SkoreAction", "settingsLineHighlight", "loadLineHighlight" );

                }

            } else {

                graph.model.beginUpdate();

                try {

                    // Gets all the "whybox" and change the style back to normal

                    graph.model.filterDescendants(
                        function( cell ) {

                            // Gets all the EDGES
                            if ( cell.isEdge() && cell.style && mxUtils.getStylename( cell.style ) === skConstants.LINE_HIGHLIGHT ) {

                                graph.setCellStyle(
                                    mxUtils.removeAllStylenames( cell.style ),
                                    [ cell ] );

                            }

                            // Gets all the WHYBOX
                            else if ( cell.isWhybox() ) {

                                graph.setCellStyles( mxConstants.STYLE_STROKECOLOR, null, [ cell ] );

                            }

                        } );

                } finally {

                    graph.model.endUpdate();

                }

                // Disables the handoverHighlight extension
                graph.model.root.handoverhighlight = null;
                delete graph.extensions.loadedExtensions[ skConstants.HANDHOVER_HIGHLIGHT ];
                skShell.ext_lineHighlightLoaded = false;

            }

        } );

        $( ".skSettings_handoverhighlight" ).append( $( div ) );

    };

} );
