
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    /**
     * Constructs a new page setup dialog.
     */
    return function skModal_Settings_Background() {

        console.log( "skModal_Settings_Background" );

        var div = require( "text!./skModal_Settings_Background.html" );

        $( ".skSettings_background" ).empty().append( $( div ) );

        if ( $( document.body ).hasClass( "mainContainerBackground" ) ) {

            $( "#skSettingBg1" ).prop( "checked", true );

        } else {

            $( "#skSettingBg2" ).prop( "checked", true );

        }

        $( "input[name=skSettingBg]" ).on( "change", function() {

            var bgType = $( "input[name=skSettingBg]:checked" ).val();

            switch ( bgType ) {

                case "neutral" :

                    $( document.body ).removeClass( "mainContainerBackground" );

                break;

                case "happy" :

                    $( document.body ).addClass( "mainContainerBackground" );

                break;
            }

            window.localStorage.setItem( "skBg", bgType );

        } );

    };

} );
