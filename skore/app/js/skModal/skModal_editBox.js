
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    var skFormLoader = require( "skGraph/skFormLoader" );
    var modal_edit = require( "text!skModal/skModal_editBox.html" );
    var skConstants = require( "skConstants" );

    function skModal_Editbox( graph ) {

        var modal = $( modal_edit );
        $( document.body ).append( modal );

        // modal from Edit Button
        $( document.body ).on( "click", ".skEditRowButton", function() {

            console.log( "click on edit button of row" );

            var tr = $( this ).closest( "tr" );
            var table = window.currentTable;
            var row = table.row( tr );

            modal.data( "currentcell", row.data().currentCell );
            modal.data( "currentrow", row );
            modal.modal( "show" );

        } );

        modal.on( "hidden.bs.modal", function() {

            console.log( "invalidate row", this );

            $( this ).data( "currentrow" ).invalidate();

        } );
        modal.on( "shown.bs.modal", function() {

            var cell = $( this ).data( "currentcell" );
            /*
             *
             * box
             *
             *
             */

            var boxEditor = new skFormLoader(
                graph,
                cell,
                "whatbox-light",
                true,
                {
                    editable: true,
                    showBtn: true,
                    showBtnRemove: false
                } );

            $( ".skEditBox" ).empty().append( boxEditor.container );

            /*
             *
             * resps
             *
             *
             */

            var respsEditor = new skFormLoader(
                graph,
                cell,
                skConstants.RESPONSIBILITIES,
                true,
                {
                    editable:true,
                    showBtn: true
                }
            );

            $( ".skEditResps" ).empty().append( respsEditor.container );

            /*
             *
             * attachments
             *
             *
             */

            var attsEditor = new skFormLoader(
                graph,
                cell,
                skConstants.ATTACHMENTS,
                true,
                {
                    editable: true,
                    showBtn: true
                } );
            $( ".skEditAtts" ).empty().append( attsEditor.container );

        } );

    }

    return skModal_Editbox;

} );
