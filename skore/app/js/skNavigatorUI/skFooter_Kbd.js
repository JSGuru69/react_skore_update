define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEvent = require( "mxEvent" );
    var mxUtils = require( "mxUtils" );

    /**
     * @constructor
     * keyboard shorcut for edit mode
     *
     *********************/

    function skFooter_Kbd( graphManager ) {

        var li = $( "<li class='skFooter_Kbd'>More keyboard shortcuts</li>" ).attr( "data-skaction", "modal_kbd" );
        $( ".skFooter" ).prepend( li );

        // For dynamic keyboard shortcuts
        /*jshint validthis:true*/
        this.kbdPlaceholder = $( "<span>" );

        $( li ).prepend( this.kbdPlaceholder );

        this.updateKbdShortcut( "canvas" );

        var footer = this;

        graphManager.addListener( "graphEnabled", mxUtils.bind( this, function( ) {

            var enabled = event.getProperty( "enabled" );

            if ( !enabled ) {

                this.updateKbdShortcut( "canvasViewer" );

            }

        } ) );

        graphManager.addListener( "newGraphCreated", function( sender, event ) {

            var graph = event.getProperty( "graph" );

            graph.getSelectionModel().addListener( mxEvent.CHANGE, function( sender, evt ) {

                var cells = evt.getProperty( "removed" );

                if ( cells && cells.length == 1 ) {

                    footer.updateKbdShortcut( "boxSelected" );

                } else if ( cells && cells.length > 1 ) {

                    footer.updateKbdShortcut( "boxesSelected" );

                } else {

                    footer.updateKbdShortcut( "canvas" );

                }

            } );

            graph.addListener( mxEvent.START_EDITING, function( ) {

                footer.updateKbdShortcut( "boxEdit" );

            } );

        } );

    }

    skFooter_Kbd.prototype.kbdPlaceholder = null;

    skFooter_Kbd.prototype.kbdList = {

        canvasViewer: "kbdshortcut for viewer",

        canvas: "" +
            "<span class='kbdshortcut'><kbd>W</kbd> + <i class='fa fa-mouse-pointer'></i> Create Whatbox</span>" +
            "<span class='kbdshortcut'><kbd>Y</kbd> + <i class='fa fa-mouse-pointer'></i> Create Whybox</span>" +
            "<span class='kbdshortcut'><kbd>N</kbd> + <i class='fa fa-mouse-pointer'></i> Create Note</span>" +
            "<span class='kbdshortcut'><kbd>Shift&nbsp;<span>⇧</span></kbd> + Drag <i class='fa fa-mouse-pointer'></i> Selection mode</span>",

        boxSelected: "" +
            "<span class='kbdshortcut'><kbd>Space</kbd> Create next box</span>" +
            "<span class='kbdshortcut'><kbd>Tab&nbsp;<span>⇥</span></kbd> Select next box</span>" +
            "<span class='kbdshortcut'><kbd>Enter&nbsp;<span>↩</span></kbd> Edit text</span>" +
            "<span class='kbdshortcut'><kbd>F</kbd> Bring to <strong>f</strong>ront</span>" +
            "<span class='kbdshortcut'><kbd>B</kbd> Send to <strong>b</strong>ack</span>",

        boxesSelected: "" +
            "<span class='kbdshortcut'><kbd class='skCtrlOrCmd'></kbd> + <kbd>G</kbd> Groups</span>" +
            "<span class='kbdshortcut'><kbd>T</kbd> Align tops (horizontal)</span>" +
            "<span class='kbdshortcut'><kbd>C</kbd> Align centers (vertical)</span>",

        boxEdit: "" +
            "<span class='kbdshortcut'>Use Markdown : <code>**</code>bold<code>**</code></span>" +
            "<span class='kbdshortcut'><code>*</code>italic<code>*</code></span>" +
            "<span class='kbdshortcut'><kbd>Shift&nbsp;<span>⇧</span></kbd> + <kbd>↩</kbd> for line return</span>"
    };

    skFooter_Kbd.prototype.updateKbdShortcut = function updateKbdShortcutF( listCase ) {

        this.kbdPlaceholder.empty();

        if ( this.kbdList[ listCase ] ) {

            this.kbdPlaceholder.append( $( this.kbdList[ listCase ] ) );

        }

    };

    return skFooter_Kbd;

} );
