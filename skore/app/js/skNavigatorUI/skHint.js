/*
 * SkHint
 *
 * usage

    .hey(
      window, //refObject, // html, jquery, cell...
      {
          "title":"your title",
          "content": ["first line", "second line", "etc."]
      },
      null, // direction: "top" "left" null...
      true, //continueButton,
      null // callback... not really a call back actually :s... just a function executed after
    )

 *
 *
 * to write your own "got it button" add this as callback
 *
 *

    function(){
        var that = this;
        this.container$.append(
            $("<div>")
                .html("<a href='#'>Got it</a>").on("click", function(){
                    that.hide();
                    return false;
                })
        )}

 *
 *
 *********************/

define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCell = require( "mxCell" );
    var mxGeometry = require( "mxGeometry" );
    var mxUtils = require( "mxUtils" );

    /**
     * @constructor
     */
    function skHint( navigatorUI ) {

        /*jshint validthis:true*/
        this.navigatorUI = navigatorUI;

        this.container$ = $( '<div class="skHint skInfoBox" style="display:none"></div>' );
        $( document.body ).append( this.container$ );

        // This.container$.draggable();
        this.container$.on( "mousedown", function( e ) {

            $( this ).addClass( "draggable" ).parents().on( "mousemove", function( e ) {

                $( ".draggable" ).offset( {
                    top: e.pageY - $( ".draggable" ).outerHeight() / 2,
                    left: e.pageX - $( ".draggable" ).outerWidth() / 2
                } ).on( "mouseup", function() {

                    $( this ).removeClass( "draggable" );

                } );

            } );
            e.preventDefault();

        } ).on( "mouseup", function() {

            $( ".draggable" ).removeClass( "draggable" );

        } );

    }

    skHint.prototype.container$ = null;
    skHint.prototype.navigatorUI = null;
    skHint.prototype.placeholderForCell = null;

    skHint.prototype.createHTML = function createHTML( obj ) {

        /*
         * Obj {
         *
         *  // title is a string
         *  title:
         *
         *  // content is a mix of string and html content
         *  content: [
         *      "hello <b>Salut</b>",
         *      document.createElement("div"),
         *      "salut"
         *  ]
         * }
         *
         */

        var content = document.createElement( "div" );

        // Title
        // var string = "<div class='hint_title'>" + obj.title + "</div><br>";
        if ( obj.title ) {

            var title = document.createElement( "div" );

            title.className = "hint_title";
            mxUtils.write( title, obj.title );

            content.appendChild( title );

        }

        // Actual content
        if ( obj.content ) {

            obj.content.forEach( function( value ) {

                content.appendChild( document.createElement( "hr" ) );

                // This is text
                if ( typeof( value ) == "string" ) {

                    var tmp = document.createElement( "div" );

                    // MxUtils.write(tmp, value);
                    tmp.innerHTML = value;
                    content.appendChild( tmp );

                }

                // This (probably) already html
                else if ( value instanceof $ ) {

                    $( content ).append( value );

                } else {

                    content.appendChild( value );

                }

            } );

        }

        return content;

    };

    /*
     * Displays a message in the center of the screen
     *
     */

    // FIXME_COMMENTED: heyJude is not used
    // Colin: this is a shortcut method... might be one day
    //noinspection JSUnusedGlobalSymbols
    skHint.prototype.heyJude = function heyJudeF( textObject ) {

        return this.hey( window, textObject, null, true );

    };

    /*
     * Displays a message next to the refObject.
     *
     */

    skHint.prototype.hey = function heyF( refObject, textObject, direction, continueButton, callback ) {

        var that = this;

        // Showing of the box is delayed... it's nicer :)
        setTimeout( function() {

            // Update the text
            that.container$.show().html( "" ).append( that.createHTML( textObject ) );

            /*
             * Positioning
             *
             * mxCell --> cell
             * null --> window
             * html
             * jquery
             * mxGeometry
             *
             */

            // Finds the item to position against
            if ( refObject instanceof mxCell ) {

                var state = that.navigatorUI.getCurrentGraph().view.getState( refObject );

                if ( that.placeholderForCell == null ) {

                    // Create an icon that will support the positioning
                    that.placeholderForCell = document.createElement( "div" );
                    that.placeholderForCell.style.position = "absolute";
                    that.placeholderForCell.style.display = "block";
                    that.placeholderForCell.className = "placeholderForCell";

                    // Despite the element being "on top" of the box, it doesn't react to click
                    that.placeholderForCell.style.pointerEvents = "none";

                    that.navigatorUI.getCurrentGraph().container.appendChild( that.placeholderForCell );

                }

                that.placeholderForCell.style.width = state.width + 20 + "px";
                that.placeholderForCell.style.height = state.height + 20 + "px";
                that.placeholderForCell.style.left = state.x - 10 + "px";
                that.placeholderForCell.style.top = state.y + -10 + "px";

                refObject = $( that.placeholderForCell );

            }

            /*
             * Find the ref object to position against
             *
             */

            // RefObject is null
            else if ( refObject == null ) {

                refObject = $( window );

            } else if ( refObject instanceof mxGeometry ) {

                // Create a 1px that will be used as the refObject
                that.placeholderForCell = $( "<div></div>" ).css( {
                    "position": "absolute",
                    "left": refObject.x + "px",
                    "top": refObject.y + "px",
                    "width": "1px",
                    "height": "1px"

                    // "border": "1px solid red"
                } );
                $( document.body ).append( that.placeholderForCell );

                // Replaces refObject;
                refObject = that.placeholderForCell;

            }

            /*
             * Position the hint
             *
             */

            that.setPosition( refObject, direction );

            if ( continueButton ) {

                console.log( "continueButton" );

                that.container$.append(
                    $( "<div>" )
                        .html( "<a>Click anywhere to continue</a>" )

                );

                $( window ).one( "click", function() {

                    that.hide();

                } );

            } // End if continueButton

            if ( callback ) {

                callback.apply( that, arguments );

            }

        }, 700 );

        return this;

    };

    skHint.prototype.setPosition = function setPositionF( refObject, direction ) {

        // Remove the arrows just in case
        this.container$.removeClass( "skInfoBox_arrow" )
            .removeClass( "skInfoBox_arrow_top" )
            .removeClass( "skInfoBox_arrow_bottom" )
            .removeClass( "skInfoBox_arrow_left" )
            .removeClass( "skInfoBox_arrow_right" );

        /*
         * No direction
         *
         */

        if ( direction == null || direction === "" ) {

            this.container$
                .show()
                .position( {
                    my:"center",
                    at:"center",
                    of:$( refObject ),
                    collision: "fit"
                } );

        }

        /*
         * Directions
         *
         */
         else {

            // For arrows
            this.container$.addClass( "skInfoBox_arrow" );

            /*
             * Right (and left with flip)
             *
             */

            if ( direction == "right" ) {

                this.container$
                    .show()
                    .position( {
                        my:"left center",
                        at:"right center",
                        of:$( refObject ),
                        collision: "flip",
                        using: function( position, data ) {

                            // Data.element : hintbox container
                            // data.target : refObject

                            if (  ( data.element.left + data.element.width ) < ( data.target.left ) ) {

                                // No flip
                                $( this ).addClass( "skInfoBox_arrow_right" );

                            } else {

                                // Flip
                                $( this ).addClass( "skInfoBox_arrow_left" );

                            }
                            $( this ).css( {
                                top: position.top + "px",
                                left: position.left + "px"
                            } );

                        }
                    } );

            }

            /*
             * Left (or right with flip)
             *
             */

            else
            if ( direction == "left" ) {

                this.container$
                    .show()
                    .position( {
                        my:"right center",
                        at:"left-10 center",
                        of:$( refObject ),
                        collision: "flip",
                        using: function( position, data ) {

                            // Data.element : hintbox container
                            // data.target : refObject

                            if (  ( data.element.left ) > ( data.target.left + data.target.width ) ) {

                                // No flip
                                $( this ).addClass( "skInfoBox_arrow_left" );

                            } else {

                                // Flip
                                $( this ).addClass( "skInfoBox_arrow_right" );

                            }
                            $( this ).css( {
                                top: position.top + "px",
                                left: position.left + "px"
                            } );

                        }
                    } );

            }

            /*
             * Top (or bottom with flip)
             *
             */

            else
            if ( direction == "top" ) {

                this.container$
                    .show()
                    .position( {
                        my:"center bottom",
                        at:"center top-10",
                        of:$( refObject ),
                        collision: "flip",
                        using: function( position, data ) {

                            // Data.element : hintbox container
                            // data.target : refObject

                            if (  ( data.element.top ) > ( data.target.top + data.target.height ) ) {

                                // No flip
                                $( this ).addClass( "skInfoBox_arrow_top" );

                            } else {

                                // Flip
                                $( this ).addClass( "skInfoBox_arrow_bottom" );

                            }
                            $( this ).css( {
                                top: position.top + "px",
                                left: position.left + "px"
                            } );

                        }
                    } );

            } // End top

            /*
             * Bottom (or top with flip)
             *
             */

            else
            if ( direction == "bottom" ) {

                this.container$
                    .show()
                    .position( {
                        my:"center top",
                        at:"center bottom+10",
                        of:$( refObject ),
                        collision: "fit flip",
                        using: function( position, data ) {

                            // Data.element : hintbox container
                            // data.target : refObject

                            if (  ( data.element.top + data.element.height ) < ( data.target.top ) ) {

                                // No flip
                                $( this ).addClass( "skInfoBox_arrow_bottom" );

                            } else {

                                // Flip
                                $( this ).addClass( "skInfoBox_arrow_top" );

                            }
                            $( this ).css( {
                                top: position.top + "px",
                                left: position.left + "px"
                            } );

                        }
                    } );

            } // End top

        } // End direction

    }; // End setPositing

    skHint.prototype.hide = function hideF() {

        this.container$.hide();

        if ( this.placeholderForCell ) {

            $( this.placeholderForCell ).remove();

            // This.placeholderForCell.parentElement.removeChild(this.placeholderForCell);

            this.placeholderForCell = null;

        }

    };

    return skHint;

} );
