define( [ "Injector" ], function( Injector ) {

    "use strict";

    var injector = new Injector();

    describe( "skTitle", function() {

        beforeEach( injector.reset );

        var mxCell;
        var mxGraph;
        var skShell;
        var skTitle;

        beforeEach( injector.run(
            function( _mxCell /*mxCell*/,
                      _mxGraph /*mxGraph*/,
                      _skShell /*skShell*/,
                      _skTitle /*skNavigatorUI/skTitle*/) {

                mxCell = _mxCell;
                mxGraph = _mxGraph;
                skShell = _skShell;
                skTitle = _skTitle;

            }
        ) );

        it( "should update itself on root change", function() {

            // Sandbox is defined in jasmine-jquery plugin
            var container = sandbox()[ 0 ];
            var graph = new mxGraph();
            var cell = graph.addCell( new mxCell() );

            skShell.graph = graph;
            var title = new skTitle( skShell, container );

            spyOn( title, "update" );
            graph.getView().setCurrentRoot( cell );
            expect( title.update ).toHaveBeenCalled();

        } );

    } );

} );
