define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );

    /**
     * @constructor
     * id
     *
     *********************/

    function skFooter_Icons( graphManager ) {

        /*jshint validthis:true*/
        this.iconBtn = $( "<li class='skFooter_Icons'>Icons</li>" ).attr( "data-skaction", "modal_icons" );
        $( ".skFooter" ).prepend( this.iconBtn );

        graphManager.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

            var enabled = event.getProperty( "enabled" );
            this.setEnabled( enabled );

        } ) );

    }

    skFooter_Icons.prototype.setEnabled = function( enabled ) {

        this.iconBtn.toggle( enabled );

    };

    skFooter_Icons.prototype.iconBtn = null;

    return skFooter_Icons;

} );
