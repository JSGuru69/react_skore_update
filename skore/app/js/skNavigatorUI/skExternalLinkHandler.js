
define( function( require ) {

    "use strict";

    var mxUtils = require( "mxUtils" );
    var skUtils = require( "skUtils" );

    function skExternalLinkHandler( navigatorUI ) {

        /*jshint validthis:true*/
        this.navigatorUI = navigatorUI;

        document.addEventListener( "click", mxUtils.bind( this, this.supportExternalLinks ), false );

    }

    skExternalLinkHandler.prototype.navigatorUI = null;

    skExternalLinkHandler.prototype.checkDomElement = function( event, element ) {

        var href;

        if ( element.classList.contains( "skExtLink" ) && element.getAttribute( "href" ) ) {

            href = element.getAttribute( "href" );
            skUtils.openURL( href );
            event.preventDefault();

        }

        if ( element.parentElement ) {

            this.checkDomElement( event, element.parentElement );

        }

    };

    skExternalLinkHandler.prototype.supportExternalLinks = function( event ) {

        this.checkDomElement( event, event.target );

    };

    return skExternalLinkHandler;

} );
