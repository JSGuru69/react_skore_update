define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    var mxEvent = require( "mxEvent" );
    var mxEventSource = require( "mxEventSource" );
    var mxEventObject = require( "mxEventObject" );
    var mxClient = require( "mxClient" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );

    var skGraphManager = require( "skGraphManager/skGraphManager" );

    //var skGraphManager = require( "skGraphManager/skNewGraphManager" );

    var skHint = require( "skNavigatorUI/skHint" );
    var skExternalLinkHandler = require( "skNavigatorUI/skExternalLinkHandler" );
    var skModal_Registration = require( "skModal/skModal_Registration" );
    var skNavHistory = require( "skUI/skNavHistory" );
    var skShell = require( "skShell" );
    var skStatus = require( "skNavigatorUI/skStatus" );
    var skModal_Attachments = require( "skModal/skModal_Attachments" );
    var skUtils = require( "skUtils" );
    var skWellDoneMessage = require( "skGraphEditorUI/skWellDoneMessage" );
    var skSearchUI = require( "skNavigatorUI/skSearchUI" );
    var skTopMenu = require( "skUI/skTopMenu" );
    var skModal = require( "skModal/skModal" );
    var skTitle = require( "skUI/skTitle" );

    var skDiagramNavigator = require( "skUI/skDiagramNavigator" );

    if ( WEB_MODE || ELECTRON ) {

        /*
         * FOR CONVERSTION
         *
         * analytics will be handled differently
         *
         *
         */

        var skProcessAnalyticsSwitch = require( "skUI/skProcessAnalyticsSwitch" );

        var skCardshoe = require( "skMenu/skCardshoe" );

    }

    /*
     * FOR CONVERSTION
     *
     * displays the icons on the boxes (enter, attachment, deletes...)
     * need to convert the popover in there to angular menu one day
     *
     */

    require( "skGraph/skGraphIcons" );

    /**
     * @constructor
     */
    function skNavigatorUI() {

        /*jshint validthis:true*/

        mxEventSource.call( this );

        /*
         * FOR CONVERSTION
         *
         * linkHandler is about opening external link in a new window (they have class .skExtLink)
         *
         * so that they are not confused with internal links (those that navigate within a process)
         *
         * conversion: define logic so that external links are opened in new window and internal one are kept inside
         */

        this.extLinkHandler = new skExternalLinkHandler( this );

        /*
         * FOR CONVERSTION
         *
         * all skStatus thing will be replaced by toasts over time
         *
         *
         */

        skShell.skStatus = new skStatus( skShell );

        if ( WEB_MODE || ELECTRON ) {

            this.addListener( "enableSave", function( sender, e ) {

                var enable = e.properties.enable;

                skShell.allowSave = enable;

                if ( enable ) {

                    $( ".warningForNotAllowSave" ).hide();

                    /*
                     * Enable save
                     *
                     * - OS menu
                     * - image export
                     * - export report
                     * -
                     *
                     */

                } else {

                    $( ".warningForNotAllowSave" ).show();

                }

            } );

            /*
             * Warning message on quit in browser
             *
             */

            if ( !DEV && !ELECTRON ) {

                window.onbeforeunload = mxUtils.bind( this, function onbeforeunloadF() {

                    if ( this.getCurrentGraph().documentEdited ) {

                        return "All changes will be lost!";

                    }

                } );

            }

        }

        this.ui = {};

        this.ui.topMenu = new skTopMenu();

        /*
         * FOR CONVERSTION
         *
         * OK THIS IS THE SERIOUS THING STARTING HERE
         *
         *
         */

        this.graphManager = new skGraphManager();

        this.ui.searchUI = new skSearchUI( this.graphManager );

        this.graphManager.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

            var enabled = event.getProperty( "enabled" );

            $( ".formatMenu" ).toggleClass( "enabled", enabled );
            $( ".editMenu" ).toggleClass( "enabled", enabled );
            $( "[data-skaction=editMenu]" ).toggle( enabled );
            $( "[data-skaction=formatMenu]" ).toggle( enabled );

        } ) );
        this.graphManager.addListener( "contentLoaded", mxUtils.bind( this, function( sender, event ) {

            // display the splash screen
            if ( !VIEWER_MODE && event.getProperty( "source" ) !== skConstants.SOURCE_EMBED ) {

                skShell.splash = new ( require( "skModal/skModal_Splash" ) )();

            }

        } ) );

        this.graphManager.addListener( "openModal", mxUtils.bind( this, function( sender, event ) {

            var modalName = event.getProperty( "name" );

            switch ( modalName ) {

                case "print" :

                    var skModal_Print = require( "skModal/skModal_Print" );
                    var modalPrint = new skModal_Print( this );

                    $( modalPrint ).modal( "toggle" );

                break;

                case "justskoreit" :

                    var skModal_JustSkoreIt = require( "skModal/skModal_JustSkoreIt" );
                    var modalJSIT = new skModal_JustSkoreIt( this );

                    $( modalJSIT ).modal( "toggle" );

                break;

                case "settings" :

                    var skModal_Settings = require( "skModal/skModal_Settings" );
                    var modalSettings = new skModal_Settings( this );

                    $( modalSettings ).modal( "toggle" );

                break;

                case "registration" :

                    if ( !this.ui.registration ) {

                        if ( ELECTRON ) {

                            this.ui.registration = require( "skModal/skModal_Registration" );

                        }

                    }

                    $( this.ui.registration ).modal( "toggle" );

                break;

                case "socialexport" :

                    var skModal_SocialExport = require( "skModal/skModal_SocialExport" );
                    var modalSocExp = new skModal_SocialExport( this );

                    $( modalSocExp ).modal( "toggle" );

                break;

                case "kbd" :

                    var skModal_kbd = require( "skModal/skModal_kbdShortcuts" );
                    var modalkbd = new skModal_kbd( this );
                    this.modal.showSideModal( modalkbd );

                break;

                case "formatting" :

                    var skModal_formatting = require( "skModal/skModal_formatting" );
                    var modal_formatting = new skModal_formatting( this );
                    this.modal.showSideModal( modal_formatting );

                break;

                case "icons" :

                    var skModal_Icons = require( "skModal/skModal_Icons" );
                    var modal_icons = new skModal_Icons( this );
                    this.modal.showSideModal( modal_icons, function() {

                        // removes from the DOM because it's quite huge!
                        $( "#skModal_Icons" ).remove();

                        return { allowClose: true };

                    } );

                break;

                case "xml":

                    var skModal_XML = require( "skModal/skModal_XML" );
                    var modalXML = new skModal_XML( this );
                    modalXML.modal( "toggle" );

                break;

                case "stylesheet" :

                    var skModal_Stylesheet = require( "skModal/skModal_Stylesheet" );
                    var modalStylesheet = new skModal_Stylesheet( this );
                    modalStylesheet.data( "loadtype", event.getProperty( "loadtype" ) );
                    modalStylesheet.modal( "show" );

                break;

                case "imageexport" :

                    var skModal_ImageExport = require( "skModal/skModal_ImageExport" );
                    this.modalImageExport = new skModal_ImageExport( this );
                    this.imageExportNow( event.getProperty( "target" ), this.modalImageExport );

                break;

                case "attachments" :

                    var cell = event.getProperty( "cell" );
                    var forceOpen = event.getProperty( "forceOpen" );
                    var atttype = event.getProperty( "atttype" );

                    var modal_attachments = new skModal_Attachments( this );
                    modal_attachments.removeData( [ "cell", "atttype" ] ); // to erase first
                    modal_attachments.data( "cell", cell );
                    modal_attachments.data( "atttype", atttype );

                    if ( forceOpen || $( modal_attachments ).is( ":visible" ) ) {

                        this.modal.showSideModal( modal_attachments, function() {

                            return { allowClose: true };

                        } );

                    }

                break;

            }

        } ) );

        if ( ELECTRON || WEB_MODE ) {

            var skSideMenu = require( "skUI/skSideMenu" );
            this.ui.sideMenu = new skSideMenu( this );

        }

        this.ui.title = new skTitle( this.graphManager );
        this.ui.navHistory = new skNavHistory( this.graphManager );

        if ( ELECTRON || WEB_MODE ) {

            this.ui.cardshoe = new skCardshoe( this.graphManager );

        }
        this.ui.diagramNavigator = new skDiagramNavigator( this );

        /*
         * Now.... dare to load the content
         *
         */

        this.graphManager.init();

        /*
         * keep going...
         *
         */

        this.ui.footer = {};

        if ( WEB_MODE || ELECTRON ) {

            // footer
            var skFooter_Kbd = require( "./skFooter_Kbd" );
            var skFooter_IDs = require( "./skFooter_IDs" );
            var skFooter_Icons = require( "./skFooter_Icons" );
            this.ui.footer.kbd = new skFooter_Kbd( this );
            this.ui.footer.ids = new skFooter_IDs( this.graphManager );
            this.ui.footer.icons = new skFooter_Icons( this.graphManager );

        }

        if ( VIEWER_MODE ) {

            $( ".skNavbar .skSideMenu_print" ).show();
            $( ".skFooter" ).append( $( "<li class='skExtLink' href='https://www.getskore.com'>Made with Skore app</li>" ) );

        }

        this.modal = new skModal( this );

        var source;

        // load a first time like the "viewer"
        if ( VIEWER_MODE ) {

            source = skConstants.SOURCE_EMBED;

        } else {

            source = skConstants.SOURCE_NEW;

        }

        // hide the export image buttons

        if ( WEB_MODE || VIEWER_MODE ) {

            $( "[data-skaction=modal_imageexport]" ).hide();

        }

        if ( WEB_MODE || ELECTRON ) {

            if ( ELECTRON ) {

                this.ui.registration = new skModal_Registration( this );

                var code = "";

                if ( DEV ) {

                    var conf = window.requireNode( "fs-jetpack" ).read(
                        window.requireNode( "electron" ).remote.app.getAppPath() + "/config/env_dev.json",
                        "json" );

                    if ( conf && conf.devKey ) {

                        code = conf.devKey;

                    }

                }

                // Check the licence validity in the main process
                window.requireNode( "electron" ).ipcRenderer.send( "checkLicense", code );

                // Check for updates
                require( "skModal/skModal_CheckUpdate" ).checkOnStartup();

            }

        }

        skShell.wellDoneMessage = new skWellDoneMessage();
        skShell.hint = new skHint( this );

         /*
         * Resizing of the entire grpah
         *
         */

        mxEvent.addListener( window, "resize", mxUtils.bind( this, function() {

            //Noinspection JSPotentiallyInvalidUsageOfThis
            this.refresh();

        } ) );

        // Debounce function refresh

        this.refresh = skUtils.debounce( mxUtils.bind( this, function() {

            // console.log( "refresh UI, debounced!" );
            this.refreshNow();

        } ), 300 );

        // Adds zoom via shift-wheel
        mxEvent.addMouseWheelListener( mxUtils.bind( this, function( evt, up ) {

            var outlineWheel = false;

            if ( mxEvent.isShiftDown( evt ) || outlineWheel ) {

                if ( up ) {

                    //Noinspection JSPotentiallyInvalidUsageOfThis
                    this.getCurrentGraph().zoomIn();

                } else {

                    //Noinspection JSPotentiallyInvalidUsageOfThis
                    this.getCurrentGraph().zoomOut();

                }

                mxEvent.consume( evt );

            }

        } ) ); // End zoom shit wheel

        this.refreshNow();

        /*
         * FOR CONVERSTION
         *
         * only for DEV, it is convenient to have access to the graph varaible in debug
         *
         *
         */

        if ( DEV ) {

            window.navigatorUI = this;
            window.graph = mxUtils.bind( this, function() {

                return this.getCurrentGraph();

            } );
            window.skUtils = require( "skUtils" );
            window.skConstants = require( "skConstants" );
            window.gsc = mxUtils.bind( this, function() {

                return this.getCurrentGraph().getSelectionCell();

            } );
            window.gscval = mxUtils.bind( this, function() {

                var cell = this.getCurrentGraph().getSelectionCell();

                if ( !cell ) {

                    cell = this.getCurrentGraph().setSelectionCells( this.getCurrentGraph().selectAll() )[ 0 ];

                }

                return mxUtils.getPrettyXml( cell.value );

            } );
            window.gss = mxUtils.bind( this, function() {

                return this.getCurrentGraph().view.getState( this.getCurrentGraph().getSelectionCell() );

            } );

            window.getvalue = mxUtils.bind( this, function() {

                return mxUtils.getPrettyXml( this.getCurrentGraph().getSelectionCell().value );

            } );

        }

        // var surveys = new skModal_Surveys( this );
        // surveys.onYoupitralala = null;

        if ( ELECTRON ) {

            this.loadELECTRON();

        }

        if ( ELECTRON || WEB_MODE ) {

            /*
             * toolbar
             *
             */

            // switch
            this.ui.processAnalyticsSwitcher = new skProcessAnalyticsSwitch( this );

            // edit Menu
            $( ".skNavbar .navbar-nav.nav" ).first().append( require( "text!skUI/skEditMenu.html" ) );

            // format menu
            $( ".skNavbar .navbar-nav.nav" ).first().append( require( "text!skUI/skFormatMenu.html" ) );

            this.enableFileDrop();

        }

        if ( ELECTRON ) {

            var config = window.requireNode( "fs-jetpack" ).read(
                window.requireNode( "electron" ).remote.app.getPath( "home" ) + "/skoreFtpForSocialConfig.json", "json" );

            if ( config ) {

                $( "#export" ).append( "" +
                '<p data-skaction="modal_socialexport">' +
                    '<i class="fa fa-comments-o"></i>' +
                        '<span class="side-menu-text">' +
                                " Export for social..." +
                        "</span>" +
                        '<span class="explanation">Upload files ready for sharing on social medias</span>' +
                "</p>" );

            }

            // electron menu
            var a = require( "skMenu/skMenu_desktopSystem" );
            a( this );

        }

        $( document.head ).append( "<style type='text/css'>" +
            ".skCtrlOrCmd:before {" +
                "content:" +
                ( mxClient.IS_MAC ? "'⌘'" : "'Ctrl'" ) +
            "}</style>" );

        /*
         * Background
         *
         */

        if ( window.localStorage.getItem( "skBg" ) === null || window.localStorage.getItem( "skBg" ) === "happy" ) {

            $( ".mainContainer" ).addClass( "mainContainerBackground" );

        }

    } // End class declaration

    mxUtils.extend( skNavigatorUI, mxEventSource );

    /*
     * Spellchecker and drop file
     *
     */

    skNavigatorUI.prototype.loadELECTRON = function() {

        if ( ELECTRON ) {

            var spellchecker  = require( "skGraphEditorUI/skSpellChecker" );
            spellchecker.setEnabled( spellchecker.isEnabled() );

        }

    };

    skNavigatorUI.prototype.graphContainerImage = require( "IMAGE_PATH" ) + "/Skore_BrainStorm_BackgroundImage_lofi.jpg";

    skNavigatorUI.prototype.currentGraph = null;

    skNavigatorUI.prototype.getGraphManager = function() {

        return this.graphManager;

    };

    skNavigatorUI.prototype.getCurrentGraph = function() {

        return this.getGraphManager().getCurrentGraph();

    };

    skNavigatorUI.prototype.initialTopSpacing = 0;

    /**
     * Refreshes the viewport.
     */

    skNavigatorUI.prototype.refreshNow = function refreshNowF( ) {

        // console.log( "refresh UI" );

        /*
 __________________________________________________
[    nav                                  [        ]]      nav     top     always
------------------------------------------[        ]       brdcrmb top     graph
[     ][       ]   graphTabs          [DS][        ]]      graphTab top
[     ][       ]--------------------------[        ]]      DS
[     ][       ][DL]   title breadcrumb   [        ]]      tab     left    graph
[     ][       ][-------------------------[        ]       side    left    graph
[     ][       ][        ][              ][        ]       DL      left    diagramNavigator
[     ][       ][ left   ][              ][ side   ]
[     ][       ][ modal  ][              ][ modal  ]       canvas  middle graph
[ tab ][ side  ][        ][    canvas    ][ right  ]
[     ][       ][        ][              ][        ]       right   right  graph
[     ][       ][        ][              ][        ]
[--------------][        ][              ][        ]       footer  bottom graph
[              ][        ][              ][        ]       footer  bottom graph
[   outline    ][        ][              ][        ]       footer  bottom graph
[              ][        ][              ][        ]       footer  bottom graph
------------------------------------------[        ]
[ footer                                  [        ]]
----------------------------------------------------


        */

        // Get the measures

        var window_h = parseInt( window.innerHeight, 10 );
        var window_w = parseInt( window.innerWidth, 10 );

        var nav_h = $( ".skNavbar" ).outerHeight() || 0;

        var title_h = $( ".skTitle" ).outerHeight() || 0;
        var diagramNavigator_w = $( ".skDiagramNavigator" ).outerWidth() || 0;

        // TODO find alternative to :visible, it's slow
        if ( $( ".forGraphEditor:visible" ).length ) {

            var tab_w = $( ".skTabMenu" ).outerWidth() || 0;
            var side_w = $( ".skSideMenu" ).outerWidth() || 0;

            var sideLeft = $( ".sideModal.sideLeft:visible" );
            var left_w = $( ".modal-content", sideLeft ).outerWidth() || 0;

            var outline_h = $( ".outlineBox" ).outerHeight() || 0;
            var footer_h = $( ".skFooter" ).outerHeight() || 0;

            var sideRight = $( ".sideModal.sideRight:visible" );
            var right_w = $( ".modal-dialog", sideRight ).outerWidth() || 0;

            var skGraphManager_h = $( ".skGraphManager" ).outerHeight() || 0;

            /*
             * Right Panel
             *
             */
            var modal_header_h, modal_footer_h, h;

            if ( sideRight.length ) {

                var modal_header_h1 =  $( ".skAttHeaderTitle", sideRight ).outerHeight() || 0;
                var modal_header_h2 =  $( ".skAttHeaderSwitcher", sideRight ).outerHeight() || 0;
                modal_footer_h =  $( ".modal-footer", sideRight ).outerHeight() || 0;

                modal_header_h = modal_header_h1 + modal_header_h2;

                sideRight.css( {
                    height: "" + ( window_h ) + "px",
                    right: "0"
                } );

                $( ".modal-content", sideRight ).css( {
                    height: "" + ( window_h + 2 ) + "px"
                } );
                $( ".modal-body", sideRight ).css( {
                    "height": ( window_h - modal_header_h - modal_footer_h + 2 ) + "px",
                    "overflow-y": "scroll",
                    "overflow-x": "hidden"
                } );

            }

            /*
             * Left panel
             */

            if ( sideLeft.length ) {

                h = ( window_h -  nav_h - title_h - skGraphManager_h - footer_h + 2 );

                $( ".modal-dialog", sideLeft ).css( {

                    top: "" + ( nav_h + title_h + skGraphManager_h - 1 ) + "px",
                    bottom: "" + ( footer_h - 1 ) + "px",
                    height: "" + ( h ) + "px",
                    left: "" + ( tab_w + side_w ) + "px"

                } );

                modal_header_h =  $( ".modal-header", sideLeft ).outerHeight() || 0;
                modal_footer_h =  $( ".modal-footer", sideLeft ).outerHeight() || 0;

                $( ".modal-content", sideLeft ).css( {

                    height: "" + ( h ) + "px"

                } );

                $( ".modal-body", sideLeft ).css( {
                    "height": "" + ( h -  modal_header_h - modal_footer_h + 2 ) + "px",
                    "overflow-y": "scroll",
                    "overflow-x": "hidden"
                } );

            }

            // Tab menu (left)
            if ( $( ".skTabMenu:visible" ) ) {

                $( ".skTabMenu" ).css( {
                    top: "" + ( nav_h ) + "px"
                } );

            }

            // Side menu (left)

            if ( $( ".skSideMenu:visible" ) ) {

                $( ".skSideMenu" ).css( {
                    top: "" + ( nav_h ) + "px",
                    bottom: "" + ( footer_h + outline_h ) + "px"
                } );

            }

            // outline
            $( ".outlineBox" ).css( {
                left:"" + tab_w + "px",
                width:"" + side_w + "px",
                bottom: "" + footer_h + "px"
            } );

            // diagramNavigator & Breadcrumb

            if ( $( ".skTitle:visible" ) ) {

                $( ".skGraphManager" ).css( {
                    top: "" + ( nav_h ) + "px",
                    left:"" + ( tab_w + side_w ) + "px"
                } );

                $( ".skDiagramNavigator" ).css( {
                    top: "" + ( nav_h + skGraphManager_h ) + "px",
                    left: "" + ( tab_w + side_w ) + "px"
                } );

                $( ".skTitle" ).css( {
                    left: "" + ( tab_w + side_w + diagramNavigator_w ) + "px",
                    top: "" + ( nav_h + skGraphManager_h ) + "px"
                } );

            }

            // Canvas
            $( ".skDiagramContainer" ).css( {
                height: "" + ( window_h - nav_h - title_h - skGraphManager_h - footer_h + 2 ) + "px",
                top:    "" + ( nav_h + title_h + skGraphManager_h - 2 )                       + "px",
                width:  "" + ( window_w - tab_w - side_w - left_w - right_w + 2 ) + "px",
                left:   "" + ( tab_w + side_w + left_w )                          + "px",
                right:  "" + ( right_w )                                          + "px",
                bottom: "" + ( footer_h )                                         + "px"
            } );

        } else {

            $( ".skContainerForAnalytics" ).css( {

                width: "" + window_w + "px",
                height: "" + ( window_h - nav_h ) + "px",
                top: "" + nav_h + "px",
                bottom: "0px"

            } );

        }

        this.graphManager.fireEvent( new mxEventObject( "refresh" ) );

    };

    skNavigatorUI.prototype.loadModal = function( name, html, shown, hidden, callback ) {

        return this.modal.load( name, html, shown, hidden, callback );

    };

    skNavigatorUI.prototype.enableFileDrop = function enableFileDropF() {

        // var graph = this;
        // var container = graph.container;

        var that = this;

        mxEvent.addListener( document, "dragover", function( evt ) {

            evt.stopPropagation();
            evt.preventDefault();

        } );

        mxEvent.addListener( document, "drop", function( evt ) {

            evt.stopPropagation();
            evt.preventDefault();

            if ( ELECTRON ) {

                var ext = evt.dataTransfer.files[ 0 ].name.split( "." ).pop();
                if ( ext === "html" || ext === "skore" ) {

                    that.getGraphManager().openFromFilePath( evt.dataTransfer.files[ 0 ].path );
                    return;

                }

            }

            if ( that.getCurrentGraph().isEnabled() ) {

                var graph = that.getCurrentGraph();

                // Gets drop location point for vertex
                var pt = mxUtils.convertPoint( graph.container, mxEvent.getClientX( evt ), mxEvent.getClientY( evt ) );
                var tr = graph.view.translate;
                var scale = graph.view.scale;
                var x = pt.x / scale - tr.x;
                var y = pt.y / scale - tr.y;

                // Converts local images to data urls
                var filesArray = event.dataTransfer.files;

                for ( var i = 0; i < filesArray.length; i++ )
                {

                    handleDrop( graph, filesArray[ i ], x + i * 10, y + i * 10 );

                }

                skShell.skStatus.setAndDontShowAgain( "Image handling is an experimental feature. <br/>" +
                    "Technical note: the image is stored directly in the xml file (<i>serialised</i> is the technical term)" +
                    " so the file can become very large. <br/>We appreciate feedback on this!", "DONTSHOWAGAINimageDrop" );

            }

        } );

        // Handles each file as a separate insert for simplicity.
        // Use barrier to handle multiple files as a single insert.
        function handleDrop( graph, file, x, y ) {

            console.log( "handle drop" );

            /*

                File.name = name of the file with extension

            */

            if ( file.type.substring( 0, 5 ) == "image" ) {

                var reader = new FileReader();

                reader.onload = function onloadF( e )
                {

                    // Gets size of image for vertex
                    var data = e.target.result;

                    // SVG needs special handling to add viewbox if missing and
                    // find initial size from SVG attributes (only for IE11)
                    if ( file.type.substring( 0, 9 ) == "image/svg" )
                    {

                        var comma = data.indexOf( "," );
                        var svgText = atob( data.substring( comma + 1 ) );
                        var root = mxUtils.parseXml( svgText );

                        // Parses SVG to find width and height
                        if ( root != null )
                        {

                            var svgs = root.getElementsByTagName( "svg" );

                            if ( svgs.length > 0 )
                            {

                                var svgRoot = svgs[ 0 ];
                                var w = parseFloat( svgRoot.getAttribute( "width" ) );
                                var h = parseFloat( svgRoot.getAttribute( "height" ) );

                                // Check if viewBox attribute already exists
                                var vb = svgRoot.getAttribute( "viewBox" );

                                if ( vb == null || vb.length === 0 )
                                {

                                    svgRoot.setAttribute( "viewBox", "0 0 " + w + " " + h );

                                }

                                // Uses width and height from viewbox for
                                // missing width and height attributes
                                else if ( isNaN( w ) || isNaN( h ) )
                                {

                                    var tokens = vb.split( " " );

                                    if ( tokens.length > 3 )
                                    {

                                        w = parseFloat( tokens[ 2 ] );
                                        h = parseFloat( tokens[ 3 ] );

                                    }

                                }

                                w = Math.max( 1, Math.round( w ) );
                                h = Math.max( 1, Math.round( h ) );

                                var value = skUtils.createBoxElement( skConstants.image ); // mxUtils.createXmlDocument().createElement( skConstants.image );

                                data = "data:image/svg+xml," + btoa( mxUtils.getXml( svgs[ 0 ], "\n" ) );
                                var box = graph.insertVertex( null, null, value, x, y, w, h,
                                    "image;shape=image;image=" + data + ";" );

                                console.log( box );

                            }

                        }

                    } else
                    {

                        var img = new Image();

                        img.onload = function onloadF()
                        {

                            var w = Math.max( 1, img.width );
                            var h = Math.max( 1, img.height );

                            // Converts format of data url to cell style value for use in vertex
                            var semi = data.indexOf( ";" );

                            if ( semi > 0 )
                            {

                                data = data.substring( 0, semi ) + data.substring( data.indexOf( ",", semi + 1 ) );

                            }

                            var value = skUtils.createBoxElement( skConstants.image ); // mxUtils.createXmlDocument().createElement( skConstants.image );

                            graph.insertVertex( null, null, value, x, y, w, h, "image;shape=image;image=" + data + ";" );

                        };

                        img.src = data;

                    }

                };

                reader.readAsDataURL( file );

            } // End file type is image

        } // End handle drop

    };

    return skNavigatorUI;

} );
