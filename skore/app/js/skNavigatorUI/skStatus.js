define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );

    /**
     * @constructor
     */
    function skStatus( shell ) {

        /*jshint validthis:true*/

        this.shell = shell || skShell;

        // this.container = $( '<div class="skStatus status" style="display:none;"></div>' );
        // $( document.body ).append( this.container );
        this.container = $( ".skStatus" );

        // Create shortcut method
        this.shell.setStatus = mxUtils.bind( this, function( text ) {

            this.set( text );

        } );

    }

    skStatus.prototype.shell = null;
    skStatus.prototype.container = null;

    skStatus.prototype.getHTML = function getHTMLF( text, className ) {

        return $( '<div class="alert alert-warning alert-dismissible ' + className + '" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            text +
        "</div>" );

    };

    skStatus.prototype.set = function setF( text ) {

        var a = $( this.getHTML( text ) ).appendTo( this.container );
        this.container.show();

        // Var a = $("<div class=''>").html(text).appendTo($(this.container));

        a.fadeOut( 4000, function() {

            $( this ).remove();

        } );

    };

    skStatus.prototype.setAndDontShowAgain = function setAndDontShowAgainF( text, key ) {

        this.setWithButtons( text, key );

    };

    /*
     * Text : text
     * key : key for not showing again
     * customAction : object
     */

    skStatus.prototype.setWithButtons = function setWithButtonsF( text, key, customAction ) {

        console.log( "skStatus.setWithButtons" );

        if ( // No key --> display only the ok button
            key == null || (

            // Or if not in localstorage
            ( localStorage.hasOwnProperty( key ) === false ||

            // Or not set as false
            ( parseInt( localStorage.getItem( key ), 10 ) === false ) ) &&

              //AND not already displayed
              $( "." + key, this.container ).length === 0 &&

              // AND not already displayed in the last 2 minutes
              ( localStorage.getItem( key + "TIMING" ) == null ||
                ( new Date( new Date() - new Date( parseInt( localStorage.getItem( key + "TIMING" ), 10 ) ) ).getSeconds() < 120 ) ) ) ) {

            // Action!

            var messageAndButtonContainer = this.getHTML( text, key );

            var stdFunc = function stdFuncF() {

                // Register the time the user has clicked so we don't bother him too much
                localStorage.setItem( key + "TIMING", new Date().getTime() );

            };

            var myAction = skUtils.extend( {}, { message:"Got it!", icon:null, func:stdFunc }, customAction );

            messageAndButtonContainer.append(
                $( "<div>" +
                    ( key ? '<div class="btn-group" role="group">' : "" ) +

                    ( key ? "" +
                        '<button type="button" class="skGotItForever btn btn-default">' +
                            '<input type="checkbox"> Got it, forever!' +
                        "</button>" : "" ) +

                    '<button type="button" class="skGotIt btn btn-default">' +
                        myAction.message +
                    "</button>" +

                    ( key ? "</div>" : "" ) +

                "</div>" ) );

            // Got it button
            $( ".skGotIt", messageAndButtonContainer ).on( "touchstart click", function() {

                myAction.func();

                // Yeah!
                skShell.wellDoneMessage.hey( this );

                $( messageAndButtonContainer ).fadeOut( 400, function() {

                    $( this ).remove();

                } );

            } );

            // Got it, forever button
            $( ".skGotItForever", messageAndButtonContainer ).on( "click", function() {

                // Check the box
                $( "input", this ).prop( "checked", true );

                // Register that the user don't want to see that anymore
                console.log( "localStorage for skStatus" );
                localStorage.setItem( key, 1 ); // 1 for true

                skShell.wellDoneMessage.hey( this );

                // Fade out
                $( messageAndButtonContainer ).fadeOut( 1000, function() {

                    $( this ).remove();

                } );

            } );

            this.container.show();

            messageAndButtonContainer.appendTo( this.container );
            messageAndButtonContainer.delay( 200 ).fadeIn( 200 );

        }

    };

    skStatus.prototype.setAndAcknowledge = function setAndAcknowledgeF( text ) {

        this.setWithButtons( text, null );

    };

    skStatus.prototype.setAndAction = function setAndActionF( text, actionMessage, actionFunction, icon ) {

        this.setWithButtons( text, null, { "message": actionMessage, "func":actionFunction, "icon":icon } );

    };

    return skStatus;

} );
