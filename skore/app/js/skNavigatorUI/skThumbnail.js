define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var canvg = require( "canvg" );
    var mxOutline = require( "mxOutline" );

    function skThumbnail( ) {

    }

    skThumbnail.prototype.createDataURL = function( graph, cell ) {

        // Create a new outline so we don't mess up with the existing one.

        var tmp = {};
        tmp.t = {};

        tmp.t.outlineTMP = new mxOutline( graph );
        tmp.t.outlineTMPContainer = document.createElement( "div" );
        var imgSrc;

        document.body.appendChild( tmp.t.outlineTMPContainer );
        tmp.t.outlineTMPContainer.style.width = "300px";
        tmp.t.outlineTMPContainer.style.height = "300px";

        // Init the temporary outline
        tmp.t.outlineTMP.init( tmp.t.outlineTMPContainer );

        // Navigate to cell if necessary
        if ( cell != null ) {

            tmp.t.outlineTMP.outline.view.setCurrentRoot( cell );

        }

        // Get the svg...
        tmp.t.g = tmp.t.outlineTMP.outline.getView().drawPane;
        tmp.t.svg = tmp.t.g.viewportElement;

        // Clear the style...
        tmp.t.svg.setAttribute( "style", "" );

        // The svg will be adjusted to fit a square this size
        var viewBoxHeight = "200";
        var viewBoxWidth = "200";

        tmp.t.svg.setAttribute( "width", viewBoxWidth );
        tmp.t.svg.setAttribute( "height", viewBoxHeight );

        // And zoomed to fit in the square
        var bbox = tmp.t.g.getBBox();

        tmp.t.svg.setAttribute( "viewBox", "" + bbox.x + " " + bbox.y + " " + bbox.width + " " + bbox.height );

        // Creates the canvas element
        tmp.t.canvas = document.createElement( "canvas" );

        document.body.appendChild( tmp.t.canvas );
        tmp.t.canvas.setAttribute( "width", viewBoxWidth );
        tmp.t.canvas.setAttribute( "height", viewBoxHeight );

        // Remove the overlay pan
        tmp.t.o = tmp.t.outlineTMP.outline.getView().getOverlayPane();

        tmp.t.o.parentNode.removeChild( tmp.t.o );

        // Convert the svg into the canvas (need to go through a serialize for some reason)
        tmp.t.x = new XMLSerializer();

        // Remove opacity otherwise canvg crashes
        $( "[stroke-opacity], [fill-opacity]", tmp.t.svg ).each( function( index, value ) {

            value.setAttribute( "stroke-opacity", "" );
            value.setAttribute( "fill-opacity", "" );

        } );

        canvg( tmp.t.canvas, tmp.t.x.serializeToString( tmp.t.svg ), {
            ignoreClear: true
        } );

        // Create a dummy canvas with a background, and "paste" our rendered-svg on top
        //create a dummy CANVAS
        tmp.t.destinationCanvas = document.createElement( "canvas" );

        tmp.t.destinationCanvas.width = tmp.t.canvas.width + 20;
        tmp.t.destinationCanvas.height = tmp.t.canvas.height + 20;

        tmp.t.destCtx = tmp.t.destinationCanvas.getContext( "2d" );

        //Create a rectangle with the desired color
        tmp.t.destCtx.fillStyle = "#FFFFFF";
        tmp.t.destCtx.fillRect( 0, 0, tmp.t.canvas.width + 20, tmp.t.canvas.height + 20 );

        //Draw the original canvas onto the destination canvas
        tmp.t.destCtx.drawImage( tmp.t.canvas, 10, 10 );

        // Create the image
        imgSrc = tmp.t.destinationCanvas.toDataURL();

        tmp.t.destCtx.clearRect( 0, 0, tmp.t.destinationCanvas.width, tmp.t.destinationCanvas.height );
        var ctx  = tmp.t.canvas.getContext( "2d" );
        ctx.clearRect( 0, 0, tmp.t.canvas.width, tmp.t.canvas.height );

        // Destroy the objects
        tmp.t.canvas.parentElement.removeChild( tmp.t.canvas );

        // destinationCanvas.parentElement.removeChild( destinationCanvas );
        tmp.t.outlineTMPContainer.parentElement.removeChild( tmp.t.outlineTMPContainer );

        delete tmp.t;

        return imgSrc;

    };

    skThumbnail.prototype.createThumbnail = function( cell ) {

        var imgSrc = this.createDataURL( cell );

        // Convert to base64url
        imgSrc = imgSrc.split( "," )[ 1 ];
        imgSrc = imgSrc.replace( /\+/g, "-" ).replace( /\//g, "_" );

        return {
            image: imgSrc,
            mimeType: "image/png"
        };

    };

    return skThumbnail;

} );
