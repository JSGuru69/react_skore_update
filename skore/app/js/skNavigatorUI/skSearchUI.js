

define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skUtils = require( "skUtils" );
    var mxUtils = require( "mxUtils" );

    function skSearchUI( graphManager ) {

        /*jshint validthis:true*/

        this.graphManager = graphManager;

        /*
        var paramDefault: {
            whatbox: true,
            whybox: true,
            stickynote: true,
            id: false,
            attachments: true,
            responsibilities: true,
            levels: "withchildren",
            exactmatch: false
        }
        */

        var createSearchParam = function() {

            return {
                whatbox: $( "[data-searchboxtype=whatbox]" ).is( ":checked" ),
                whybox: $( "[data-searchboxtype=whybox]" ).is( ":checked" ),
                stickynote: $( "[data-searchboxtype=stickynote]" ).is( ":checked" ),
                id: $( "[data-searchboxtype=id]" ).is( ":checked" ),
                attachments: $( "[data-searchboxtype=attachments]" ).is( ":checked" ),
                responsibilities: $( "[data-searchboxtype=responsibilities]" ).is( ":checked" ),
                levels: $( "input[name=searchScope]:checked" ).val(),
                exactmatch: $( "[data-searchboxtype=exactmatch]" ).is( ":checked" )
            };

        };

        $( ".skMenuFind_match" ).hide();
        $( ".skMenuFind_clear" ).hide();
        $( ".skSearchFilter" ).hide();

        $( ".skMenuFind_input" ).focusin( function() {

            $( this ).select();

        } );

        var searchEngine = graphManager.searchEngine;

        // Clear search
        $( ".skMenuFind_clear" ).on( "click touchstart", function() {

            $( ".skMenuFind_input" ).val( "" );

            searchEngine.clearSearch();

            hideStuffs();

        } );

        var hideStuffs = function() {

            $( ".skMenuFind_match" ).hide();
            $( ".skMenuFind_clear" ).hide();
            $( ".skSearchFilter" ).hide();

        };

        var searchDebounce = skUtils.debounce( mxUtils.bind( this, function() {

            var keyword = $( ".skMenuFind_input" ).val();

            if ( keyword === "" ) {

                hideStuffs();

            } else {

                searchEngine.performSearch( keyword, createSearchParam(), mxUtils.bind( this, this.resultsCallback ) );

            }

        } ), 300 );

        // Performs the search on each key type
        $( ".skMenuFind_input" ).on( "keyup", searchDebounce );

        // Init filter buttons
        $( ".skSearchFilter" ).on( "change", mxUtils.bind( this, function() {

            // Clear previous search so that the search is started again
            searchEngine.clearSearch();
            searchEngine.performSearch( $( ".skMenuFind_input" ).val(), createSearchParam(), mxUtils.bind( this, this.resultsCallback ) );

        } ) );

        graphManager.addListener( "graphEnabled", mxUtils.bind( this, function() {

            searchEngine.performSearch( $( ".skMenuFind_input" ).val(), createSearchParam(), mxUtils.bind( this, this.resultsCallback ) );

        } ) );

        graphManager.addListener( "navigationStarted", function() {

            searchEngine.unhighlight();

        } ) ;

        graphManager.addListener( "navigationDone", mxUtils.bind( this, function() {

            // Clear previous keyword so that the search is started again
            searchEngine.clearSearch();

            searchEngine.performSearch( $( ".skMenuFind_input" ).val(), createSearchParam(), mxUtils.bind( this, this.resultsCallback ) );

        } ) );

        // Prevent the dropdown to close on checkbox change
        $( ".skFilterList li" ).on( "click", function( event ) {

            event.stopPropagation();

        } );

    }

    var thislevel, otherlevel;

    skSearchUI.prototype.resultsCallback = function( keyword, results ) {

        $( ".skMenuFind_clear" ).show();
        $( ".skSearchFilter" ).show();

        // Number of results
        $( ".skMenuFind_match" ).show();
        $( ".skMenuFind_matchResults" ).html( results.length + " found" );
        $( ".skResultList" ).empty();

        var thislevel = null, otherlevel = null;
        if ( !results.length ) {

            $( ".skResultList" ).append( $( "<li><a>No results</a></li>" ) );

        }

        thislevel = null;
        otherlevel = null;

        var that = this;
        results.forEach( function( result ) {

            that.addResultToList( result.cell, !result.cellIsVisible );

        } );

    };

    skSearchUI.prototype.addResultToList = function( cell, other ) {

        console.log( "addResultToList", cell, other );

        var that = this;

        if ( $( ".res-" + cell.id ).length === 0 ) {

            var entry = $( "<li class='res-" + cell.id + "'><a>" + cell.getBoxText( true, true ) + "</a></li>" );
            entry.on( "click", function() {

                that.graphManager.getCurrentGraph().navigateTo( cell, true );

            } );

            if ( other ) {

                if ( !otherlevel ) {

                    otherlevel = $( '<li class="dropdown-header" class="otherlevel" >Another level</li>' );
                    $( ".skResultList" ).append( otherlevel );

                }

                otherlevel.after( entry );

            } else {

                if ( !thislevel ) {

                    thislevel = $( '<li class="dropdown-header" class="thislevel" >On this level</li>' );
                    $( ".skResultList" ).prepend( thislevel );

                }

                thislevel.after( entry );

            }

        }

    };

    skSearchUI.prototype.graphManager = null;
    return skSearchUI;

} );
