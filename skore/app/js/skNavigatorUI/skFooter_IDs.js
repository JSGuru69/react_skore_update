define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );

    /**
     * @constructor
     * id
     *
     *********************/

    function skFooter_IDs( graphManager ) {

        /*jshint validthis:true*/
        var li = $( "<li class='skFooter_IDs'></li>" ).attr( "data-skaction", "showID" );
        $( ".skFooter" ).prepend( li );

        // graphManager.addListener( "newGraph", mxUtils.bind( this, function( sender, event ) {

            // var graph = event.getProperty( "graph" );

        graphManager.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

            li.toggleClass( "on", event.getProperty( "graph" ).showBadgeIcon );

        } ) );

        graphManager.addListener( "boxSelectionChanged", mxUtils.bind( this, function( sender, event ) {

            var cellsAdded = event.getProperty( "cellsAdded" );
            if ( cellsAdded && cellsAdded.length === 0 ) {

                li.hide();

            } else if ( cellsAdded && cellsAdded.length === 1 ) {

                li.show().text( cellsAdded[ 0 ].id );

            } else if ( cellsAdded && cellsAdded.length > 1 ) {

                li.show().text( "• " + cellsAdded.length  + " •" );

            }

        } ) );

        // } ) );

    }

    return skFooter_IDs;

} );
