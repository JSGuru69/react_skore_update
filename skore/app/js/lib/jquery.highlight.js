/*
 * jQuery Highlight plugin
 *
 * Based on highlight v3 by Johann Burkard
 * http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html
 *
 * Code a little bit refactored and cleaned (in my humble opinion).
 * Most important changes:
 *  - has an option to highlight only entire words (wordsOnly - false by default),
 *  - has an option to be case sensitive (caseSensitive - false by default)
 *  - highlight element tag and class names can be specified in options
 *
 * Usage:
 *   // wrap every occurrance of text 'lorem' in content
 *   // with <span class='highlight'> (default options)
 *   $('#content').highlight('lorem');
 *
 *   // search for and highlight more terms at once
 *   // so you can save some time on traversing DOM
 *   $('#content').highlight(['lorem', 'ipsum']);
 *   $('#content').highlight('lorem ipsum');
 *
 *   // search only for entire word 'lorem'
 *   $('#content').highlight('lorem', { wordsOnly: true });
 *
 *   // don't ignore case during search of term 'lorem'
 *   $('#content').highlight('lorem', { caseSensitive: true });
 *
 *   // wrap every occurrance of term 'ipsum' in content
 *   // with <em class='important'>
 *   $('#content').highlight('ipsum', { element: 'em', className: 'important' });
 *
 *   // remove default highlight
 *   $('#content').unhighlight();
 *
 *   // remove custom highlight
 *   $('#content').unhighlight({ element: 'em', className: 'important' });
 *
 *
 * Copyright (c) 2009 Bartek Szopka
 *
 * Licensed under MIT license.
 *
 */

// jQuery.extend({
//     highlight: function (node, re, nodeName, className) {
//         if (node.nodeType === 3) {
//             var match = node.data.match(re);
//             if (match) {
//                 var highlight = document.createElement(nodeName || 'span');
//                 highlight.className = className || 'highlight';
//                 var wordNode = node.splitText(match.index);
//                 wordNode.splitText(match[0].length);
//                 var wordClone = wordNode.cloneNode(true);
//                 highlight.appendChild(wordClone);
//                 wordNode.parentNode.replaceChild(highlight, wordNode);
//                 return 1; //skip added node in parent
//             }
//         } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
//                 !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
//                 !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
//             for (var i = 0; i < node.childNodes.length; i++) {
//                 i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
//             }
//         }
//         return 0;
//     }
// });

// jQuery.fn.unhighlight = function (options) {
//     var settings = { className: 'highlight', element: 'span' };
//     jQuery.extend(settings, options);

//     return this.find(settings.element + "." + settings.className).each(function () {
//         var parent = this.parentNode;
//         parent.replaceChild(this.firstChild, this);
//         parent.normalize();
//     }).end();
// };

// jQuery.fn.highlight = function (words, options) {
//     var settings = { className: 'highlight', element: 'span', caseSensitive: false, wordsOnly: false };
//     jQuery.extend(settings, options);

//     if (words.constructor === String) {
//         words = [words];
//     }
//     words = jQuery.grep(words, function(word, i){
//       return word != '';
//     });
//     words = jQuery.map(words, function(word, i) {
//       return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
//     });
//     if (words.length == 0) { return this; };

//     var flag = settings.caseSensitive ? "" : "i";
//     var pattern = "(" + words.join("|") + ")";
//     if (settings.wordsOnly) {
//         pattern = "\\b" + pattern + "\\b";
//     }
//     var re = new RegExp(pattern, flag);

//     return this.each(function () {
//         jQuery.highlight(this, re, settings.element, settings.className);
//     });
// };


/*

highlight v4

Highlights arbitrary terms.

<http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html>

MIT license.

Johann Burkard
<http://johannburkard.de>
<mailto:jb@eaio.com>

*/

jQuery.fn.highlight = function(pat) {
  function innerHighlight(node, pat) {
    var skip = 0;
    if (node.nodeType == 3) {
      var pos = node.data.toUpperCase().indexOf(pat);
      if (pos >= 0) {
        var spannode = document.createElement('span');
        spannode.className = 'highlight';
        var middlebit = node.splitText(pos);
        var endbit = middlebit.splitText(pat.length);
        var middleclone = middlebit.cloneNode(true);
        spannode.appendChild(middleclone);
        middlebit.parentNode.replaceChild(spannode, middlebit);
        skip = 1;
      }
    } else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
      for (var i = 0; i < node.childNodes.length; ++i) {
        i += innerHighlight(node.childNodes[i], pat);
      }
    }
    return skip;
  }
  return this.length && pat && pat.length ? this.each(function() {
    innerHighlight(this, pat.toUpperCase());
  }) : this;
};

jQuery.fn.unhighlight = function() {
  return this.find("span.highlight").each(function() {
      // node.normalize does not work in IE11 for some reason
      // here is a custom made version
      var parentNode = this.parentNode;
      var text = this.firstChild;
      parentNode.replaceChild(text, this);
      var prev = text.previousSibling;
      if (prev && prev.nodeType !== 3) {
          prev = null;
      }
      var next = text.nextSibling;
      if (next && next.nodeType !== 3) {
          next= null;
      }
      var str = (prev ? prev.data : '') + text.data + (next ? next.data : '');
      if (str) {
          text.data = str;
      } else {
          parentNode.removeChild(text)
      }
      if (prev) {
          parentNode.removeChild(prev)
      }
      if (next) {
          parentNode.removeChild(next)
      }
    //this.parentNode.firstChild.nodeName;
    //with(this.parentNode) {
    //  replaceChild(this.firstChild, this);
    //  normalize();
    //}
  }).end();
};
