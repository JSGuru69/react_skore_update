
/*

findElementsContainingText

selects elements containing a given text
usage: $.("selector").findElementsContainingText("hello");

Colin Claverie


based on highlight v4

<http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html>

MIT license.
Johann Burkard
<http://johannburkard.de>
<mailto:jb@eaio.com>

*/

/*
 * modified "highlight" that returns the list
 *
 */

jQuery.fn.findElementsContainingText = function(pat) {

    var results = [];

    function innerHighlight(node, pat) {
        var skip = 0;
        if (node.nodeType == 3) {
            var pos = node.data.toUpperCase().indexOf(pat);
            if (pos >= 0) {

                results.push(node.parentNode);

                skip = 1;
            }
        } else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += innerHighlight(node.childNodes[i], pat);
            }
        }
        return skip;
    }
    if(this.length && pat && pat.length){

        this.each(function() {
        innerHighlight(this, pat.toUpperCase());
    }) }

    return $(results);
};
