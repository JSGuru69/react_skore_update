
define( function( require ) {

    "use strict";

    var skTemplateDescriptions = require( "skGraph/skTemplateDescriptions" );
    var skFormLoader = require( "skGraph/skFormLoader" );
    var skFormRenderer = require( "skGraph/skFormRenderer" );
    var skConstants = require( "skConstants" );

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );

    var renderAttachments = function( graph, cell, type ) {

        var form;
        switch ( type ) {

            case skConstants.RESPONSIBILITIES :

                form = new skFormLoader(
                    graph,
                    cell,
                    type,
                    true,
                    {
                        editable:true,
                        showBtn: true
                    },
                    function() {

                        delete graph.cellHTMLCache[ cell.id ];
                        graph.updateCellsSize( [ cell ] );

                    }
                );

            break;

            default : // equivalent skConstants.ATTACHMENTS:

                form = new skFormLoader(
                    graph,
                    cell,
                    type,
                    true,
                    {
                        editable: true,
                        showBtn: true
                    } );
                $( ".newAttaButtonGroup" ).show();

            break;
        }

        return form;

    };

    var getAttachmentTypes = function() {

        return Object.keys( skTemplateDescriptions.attachment );

    };

    var createNewAttachment = function( graph, cell, fieldName ) {

        // get the field

        var field = skTemplateDescriptions.attachments[ fieldName ];

        if ( !field ) {

            return;

        }

        var allAttachments = cell.value.querySelector( "attachments" );

        if ( !allAttachments ) {

            allAttachments = mxUtils.createXmlDocument().createElement( "attachments" );
            cell.value.appendChild( allAttachments );

        }

        // crate a value so it knows which editor to pick
        // todo : could be just a string with recognition in the formRenderer
        var tmpValue = mxUtils.createXmlDocument().createElement( field.name );
        allAttachments.appendChild( tmpValue );

        var form = new skFormRenderer(
            graph,
            cell,
            field,
            allAttachments,
            tmpValue,
            false,
            {
                editable:true,
                showBtn: true
            },
            null );

        return form;

    };

    return {
        renderAttachments: renderAttachments,
        getAttachmentTypes: getAttachmentTypes,
        createNewAttachment: createNewAttachment
    };

} );
