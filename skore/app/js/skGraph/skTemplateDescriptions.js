
define( function() {

    "use strict";

	var c = {};

    c.attachment = {};
    c.group = {};
    c.role = {};
    c.stickynote = {};
    c.title = {};
    c.image = {};
    c.whatbox = {};
    c.whybox = {};
    c.responsibility = {};

    c.attachment.text = {
        name:"text", // must be same as object name
        type: "longtext",
        editable: true,
        sortable: true,
        defaulttext: "Additional information",
        title: "Text",
        removable: true,
        multi: {
            addempty: false,
            addbutton: false
        }
    };
    c.attachment.url = {
        name: "url", // must be same as object name
        type: "url",
        sortable: true,
        title: "Web URL link",
        editable: true,
        removable: true,
        multi: {
            addempty: false,
            addbutton: false
        }
    };
    c.attachment.externalcontent = {
        name:"externalcontent", // must be same as object name
        type:"externalcontent",
        title: "External content",
        editable:true,
        sortable: true,
        removable: true,
        multi: {
            addempty: false,
            addbutton: false
        }
    };
    c.attachment.checklist = {
        name:"checklist", // must be same as object name
        type:"checklist",
        title:"Checklist item",
        editable: true,
        sortable: true,
        removable: true,
        multi:{
            addempty: true,
            addbutton: true
        }
    };

    // c.attachment.participants = {
    //     name:"participants",
    //     type:"group",
    //     multi:{
    //         addempty: true,
    //         addbutton: true
    //     },
    //     editable: true,
    //     sortable: true,
    //     removable: true,
    //     fields: [
    //     {
    //         name:"participant",
    //         type:"singleline",
    //         title:"participant of the workshop",
    //         editable: true,
    //         removable: true,
    //         multi:{
    //             addempty: true,
    //             addbutton: true
    //         }
    //     } ]
    // };

	c.whybox.base = {
        fields: [
            {
                name: "text",
                type: "longtext",
                helptext: "formatting",
                defaulttext: "So that?"
            }
        ]
    };

    c.whatbox.base = {
        fields: [
            {
                name:"text",
                type:"longtext",
                helptext: "formatting",
                defaulttext: "What happens?"
            },
            {
                type:"separator",
                name:"whoWhatLine"
            }
        ]
    };

    c.responsibility.base = {
        editable:true,
        fields:[
            {
                name:"responsibility",
                type:"group",
                removable: true,
                sortable: true,
                multi: {
                    addbutton: true,
                    addtext: "Add 1 more role",
                    addempty: true
                },
                fields: [
                    {
                        name:"role",
                        localid:true,
                        type:"singleline",
                        autocomplete: {
                            source:"roles",
                            enrichsource:true
                        },
                        defaulttext: "Who does it?"
                    }
                ]
            }
        ]
    };

    c.responsibility.ratsi = {
        fields: [
            {
                name:"responsibility",
                type:"group",
                removable:true,
                multi: {
                    addbutton: true,
                    addtext: "Add 1 more role",
                    addempty:true
                },
                fields: [
                    {
                        name:"role",
                        localid:true,
                        type:"singleline",
                        autocomplete: {
                            source:"roles",
                            enrichsource:true
                        },
                        defaulttext: "Who does it?"
                    },
                    { name:"tag", value: "R", type:"button", description:"Responsible",
                        style:{ "background-color":"rgba(255, 165, 0, 0.5)" } },
                    { name:"tag", value: "A", type:"button", description:"Authority",
                        style:{ "background-color":"rgba(255, 0, 0, 0.5)" } },
                    { name:"tag", value: "T", type:"button", description:"Task",
                        style:{ "background-color":"rgba(245, 245, 220, 0.5)" } },
                    { name:"tag", value: "S", type:"button", description:"Support" },
                    { name:"tag", value: "I", type:"button", description:"Informed" }
                ]
            }
        ]
    };

    c.responsibility.raci = {
        fields: [
            {

                name:"responsibility",
                type:"group",
                removable:true,
                multi: {
                    addbutton: true,
                    addtext: "Add 1 more role",
                    addempty:true
                },
                fields: [
                    {
                        name:"role",
                        localid:true,
                        type:"singleline",
                        autocomplete: {
                            source:"roles",
                            enrichsource:true
                        },
                        defaulttext: "Who does it?"
                    },
                    { name:"tag", value: "R", type:"button", description:"Responsible",
                        style:{ "background-color":"rgba(255, 165, 0, 0.5)" } },
                    { name:"tag", value: "A", type:"button", description:"Accountable",
                        style:{ "background-color":"rgba(255, 0, 0, 0.5)" } },
                    { name:"tag", value: "C", type:"button", description:"Consulted",
                        style:{ "background-color":"rgba(245, 245, 220, 0.5)" } },
                    { name:"tag", value: "I", type:"button", description:"Informed" }

                ]
            }
        ]
    };

    c.stickynote.base = {
        fields: [
            {
                name: "text",
                type: "longtext",
                helptext: "formatting",
                defaulttext: "A comment"
            }, {
                type: "colorpicker"
            }
        ]
    };

    c.image.base = {
        editable: false
    };

    c.group.base = {
        fields: [
            {
                name: "text",
                type: "singleline",
                defaulttext: "Name your group"
            }
        ]
    };

    c.title.base = {
        fields: [
            {
                name: "text",
                type: "singleline",
                defaulttext: "Name your Skore process"
            }
        ]
    };

    c.role.base = {
        editable: true,
        name:"role",
        type:"singleline",
        fields: [
            {
                name: "comment",
                type: "longtext",
                defaulttext: "Add a comment for the role"
            }
        ]
    };

	return c;

} );
