
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCellState = require( "mxCellState" );
    var mxConstants = require( "mxConstants" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );

    /**
     * @constructor
     * skSearch.js
     *
     * searches the graph (and attachments) and highlights
     * where results are found
     *
     * searched in "visible graph and details view"
     *
     *********************/

    function skSearch( graphManager ) {

        /*jshint validthis:true*/
        this.graphManager = graphManager;

    }

    skSearch.prototype.graphManager = null;
    skSearch.prototype.prevKeyword = "";

    skSearch.prototype.clearSearch = function() {

        this.prevKeyword = "";
        this.performSearch( "" );

    };

    skSearch.prototype.highlightedItems = [];

    skSearch.prototype.performSearch = function performSearchF( keyword, searchParams, callback ) {

    // Core function, triggered at every touch (debounced 300ms)

        var tmp;
        var cells;

        /*
         * Highlights the new search
         *
         */
        if ( keyword && keyword.length ) {

            cells = [];

            // if the entry was a number (i.e. box id)
            // we don't want to search in the restricted scope
            if ( !isNaN( parseInt( keyword, 10 ) ) ) {

                this.prevKeyword = "";

            }

            if ( this.prevKeyword !== "" && keyword.startsWith( this.prevKeyword ) ) {

                cells = [];

                tmp = this.results.map( function( obj ) {

                    return obj.cell;

                } );

                cells = tmp.filter( function( item, i, tmp ) {

                    return i == tmp.indexOf( item );

                } );

            } else {

                // if ( $( "input[name=searchScope]:checked" ).val() === "scopeThisLevel" ) {
                if ( searchParams.levels === "currentonly" ) {

                    tmp = this.graphManager.getCurrentGraph().getDefaultParent().children;
                    if ( tmp ) {

                        cells = tmp.filter( function( cell ) {

                            return cell.isVertex();

                        } );

                    }

                } else {

                    var parent = null;

                    if ( searchParams.levels === "withchildren" ) {

                        parent = this.graphManager.getCurrentGraph().getDefaultParent();

                    }

                    cells = this.graphManager.getCurrentGraph().model.filterDescendants( function( cell ) {

                        return cell.isVertex();

                    }, parent );

                    // Remove the parent
                    if ( parent ) {

                        delete cells[ cells.indexOf( this.graphManager.getCurrentGraph().getDefaultParent() ) ];

                    }

                }

                // now filter by cell type

                cells = $.map( cells, function( cell ) {

                    if ( cell && ( ( cell.isWhatbox() && searchParams.whatbox ) ||
                        ( cell.isWhybox() && searchParams.whybox ) ||
                        ( cell.isStickynote() && searchParams.stickynote ) ) ) {

                        return cell;

                    }

                } );

            }

            this.prevKeyword = keyword;

            // Unhighlight the elements in the current search
            this.unhighlight();

            this.results = [];

            var that = this;

            if ( cells ) {

                keyword = keyword.toLowerCase();

                cells.forEach( mxUtils.bind( this, function( cell ) {

                    // search ID first
                    if ( searchParams.id ) {

                        if ( cell.id.toLowerCase() === keyword ) {

                            this.results.push( {
                                cell: cell,
                                nodeType: "id",
                                nodeValue: keyword
                            } );

                        }

                    }

                    // search in the elemnts

                    if ( searchParams.whatbox || searchParams.whybox || searchParams.stickynote ) {

                        that.searchInNodes(
                            cell.value.querySelectorAll( skConstants.BOX ), keyword, cell, skConstants.BOX, searchParams );

                    }

                    if ( searchParams.responsibilities ) {

                        that.searchInNodes(
                            cell.value.querySelectorAll( skConstants.RESPONSIBILITIES ), keyword, cell, skConstants.RESPONSIBILITIES, searchParams );

                    }

                    if ( searchParams.attachments ) {

                        // RESPONSIBILITY will be filtered out later if necessary
                        that.searchInNodes(
                            cell.value.querySelectorAll( skConstants.ATTACHMENTS ), keyword, cell, skConstants.ATTACHMENTS, searchParams );

                    }

                } ) );

                // run through the results and split if they are visible or not
                this.results.forEach( mxUtils.bind( this, function( result ) {

                    result.cellIsVisible = ( this.graphManager.getCurrentGraph().view.getState( result.cell ) !== null );

                } ) );

                this.highlightAllSearchResult( keyword, this.results, searchParams );

                if ( callback && typeof callback === "function" ) {

                    callback( keyword, this.results, searchParams );

                }

            } else {

                skUtils.reflowStyleCss();

                return;

            }

        }

        // No keywords
        else {

            this.unhighlight();

            this.keyword = "";
            this.prevKeyword = "";

        }

        skUtils.reflowStyleCss();

    };

    skSearch.prototype.unhighlight = function unhighlightF() {

        // Reset cells
        this.highlightedItems.forEach( function( e ) {

            if ( e.state instanceof mxCellState && e.state.shape ) {

                e.state.style[ mxConstants.STYLE_STROKECOLOR ] = skShell.colors.box;
                e.state.shape.apply( e.state );
                $( ".actionIcon", e.state.icon.enterDetail.node ).removeClass( "highlight" );
                e.state.shape.reconfigure();

            }

            if ( e.state instanceof $ ) {

                e.state.removeClass( "highlight" );

            }

        } );

        // Clears the highlighted items
        this.highlightedItems = [];

        // Remove yellow highlight
        $( ".skSearchable" ).unhighlight();

    };

    skSearch.prototype.searchInAttributes = function searchInAttributesF( element, keyword, cell, specialtype, searchParams ) {

        // Search in the attributes
        for ( var i = 0; i < element.attributes.length; i++ ) {

            var valToSearch;

            // only for roles at the moment
            if ( element.nodeName === "role" && element.attributes[ i ].nodeName === "localid" ) {

                valToSearch = skUtils.readValue(
                    this.graphManager.getCurrentGraph().roleManager.roles.querySelector(
                        "role[" + skConstants.LOCAL_ID + "='" + skUtils.readValue( element, skConstants.LOCAL_ID ) + "']" ) );

            } else {

                valToSearch = element.attributes[ i ].value.toLowerCase();

            }

            if ( searchParams.exactmatch ) {

                if ( valToSearch === keyword ) {

                    this.results.push( {
                        cell: cell,
                        nodeType: specialtype,
                        nodeValue: element.attributes[ i ]
                    } );

                }

            } else {

                if ( valToSearch.indexOf( keyword ) >= 0 ) {

                    this.results.push( {
                        cell: cell,
                        nodeType: specialtype,
                        nodeValue: element.attributes[ i ]
                    } );

                }

            }

        }

    };

    skSearch.prototype.searchInNodes = function( nodes, keyword, newCell, specialtype, searchParams ) {

        var that = this;

        $( nodes ).each( function( i, node ) {

            if ( node.nodeType == 1 ) {

                // search in the attributes of the current node
                that.searchInAttributes( node, keyword, newCell, specialtype, searchParams );

                // search in the child nodes
                that.searchInNodes( node.childNodes, keyword, newCell, specialtype, searchParams );

            }

        } );

    };

    // Highlights all cells; only if in lower levels...
    skSearch.prototype.highlightAllSearchResult = function highlightAllSearchResultF( keyword, results, searchParams ) {

        if ( searchParams.responsibilities ) {

            $( ".responsibility" ).highlight( keyword );

        }

        // Text in current view
        if ( searchParams.whatbox ) {

            $( ".whatbox .skSearchable .text" ).highlight( keyword );

        }

        if ( searchParams.whybox ) {

            $( ".whybox .skSearchable .text" ).highlight( keyword );

        }

        if ( searchParams.stickynote ) {

            $( ".stickynote .skSearchable" ).highlight( keyword );

        }

        if ( searchParams.attachments ) {

            $( "#skModal_Attachments .skSearchable" ).highlight( keyword );

        }

        $( ".skTitle.skSearchable" ).highlight( keyword );

        // Var highlight = highlight ? true : false;
        this.highlightedItems = [];
        var that = this;

        console.log( "search results", results.length );

        if ( results != null && results.length > 0 ) {

            // We browse the results (cells, attachment), and see where to highlight i.e. change the state)
            $( results ).each( function( i, result ) {

                var state;

                /*
                 * Highlights only cells (the shape) when in drill down
                 *
                 */

                // If the result is in the drill down (not visible) there is no state
                if ( !result.cellIsVisible ) {

                    var tmp = result.cell;

                    // Search the most relevant cell to highlight
                    while ( that.graphManager.getCurrentGraph().view.getState( tmp ) == null && tmp.parent ) {

                        tmp = tmp.parent;

                    }

                    // Higlights the cell...
                    state = that.graphManager.getCurrentGraph().getView().getState( tmp );

                    if ( state && state.shape != null ) {

                        state.style[ mxConstants.STYLE_STROKECOLOR ] = skShell.colors.BOX_HIGHLIGHT;
                        state.shape.apply( state );

                        $( ".actionIcon", state.icon.enterDetail.node ).addClass( "highlight" );

                        state.shape.redraw();
                        state.shape.reconfigure();
                        that.highlightedItems.push( {
                            cell:tmp,
                            state:state
                        } );

                    }

                }

                // If the cell is visible, we might consider highlighting att
                else if ( result.nodeType == skConstants.ATTACHMENTS ) {

                    console.log( "result is attachment" );
                    state = that.graphManager.getCurrentGraph().getView().getState( result.cell );

                    if ( state != null ) {

                        that.addResultToList( result.cell, false );

                        var domNode = $( ".actionIcon", state.icon.attachment.node ).addClass( "highlight" );

                        that.highlightedItems.push( {
                            cell: result.cell,
                            state:domNode
                        } );

                    }

                }

            } );

        }

    };

    return skSearch;

} );
