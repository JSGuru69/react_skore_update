define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCell = require( "mxCell" );
    var mxEventObject = require( "mxEventObject" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skUtils = require( "skUtils" );
    var skFormLoader = require( "skGraph/skFormLoader" );

    // var skTemplate_Renderer = require( "skGraph/skTemplate_Renderer" );

    mxCell.prototype.getCellType = function() {

        if ( this.isWhatbox() ) {

            return skConstants.whatbox;

        } else if ( this.isWhybox() ) {

            return skConstants.whybox;

        } else if ( this.isStickynote() ) {

            return skConstants.stickynote;

        } else if ( this.isGroup() ) {

            return skConstants.group;

        } else if ( this.isEdge() ) {

            if ( this.style.split( ";" )[ 0 ] === skConstants.LINE_WY2S ) {

                return skConstants.LINE_WY2S;

            } else {

                return "line";

            }

        }
        return "unknown cell type";

    };

    /**
     * Tests if it's a why box
     * @return {boolean} true if the box is a whybox
     */
    mxCell.prototype.isWhybox = function isWhyboxF() {

        return ( this.value && this.value.tagName == skConstants.whybox );

    };

    /**
     * Tests if it's a what box (simple)
     * @return {boolean} true if the box is a what box
     */
    mxCell.prototype.isWhatbox = function isWhatboxF() {

        return ( this.value && this.value.tagName == skConstants.whatbox );

    };

    mxCell.prototype.isWhatboxDetailed = function isWhatboxDetailedF() {

        //FIXME : now there is a value at the root with the extensions ?
        return ( this.value && this.isWhatbox() && this.children && this.children.length > 0 );

    };

    // Adds imageType
    mxCell.prototype.isImage = function isImageF() {

        return ( this.value && this.value.tagName == skConstants.image );

    };

    /**
     * Returns the number of attachments of a box
     * @return {number} The number of attachments
     */
    mxCell.prototype.numberOfAttachments = function numberOfAttachmentsF() {

        return $( "attachments", this.value ).children().length;

    };

    /**
     * Returns the number of surveys of a box
     * @return {number} The number of surveys
     */
    mxCell.prototype.numberOfSurveys = function numberOfSurveysF() {

        var count = 0;

        if ( this.value ) {

            count = this.value.getElementsByTagName( skConstants.SURVEY ).length;

        }

        return count;

    };

    /**
     * Tests if the box is a group
     * @return {boolean} true if the box is a group
     */
    mxCell.prototype.isGroup = function isGroupF() {

        return ( this.value && this.value.tagName == skConstants.group );

    };

    /**
     * Tests if the box is a sticky note
     * @return {string} true if it's a sticky Note
     */
    mxCell.prototype.isStickynote = function isStickynoteF() {

        return ( this.value && this.value.tagName == skConstants.stickynote );

    };

    // mxCell.prototype.graph = null;

    mxCell.prototype.createHtmlContent = function createHtmlContentF( graph ) {

        var id = parseInt( this.id, 10 );
        if ( !graph.cellHTMLCache[ id ] ) {

            console.log( "mxCell.createHtmlContent", id );

            var formLoader = new skFormLoader(
                graph,
                this,
                this.value.tagName,
                true,
                {
                    editable: graph.isEnabled(),
                    showBtn: false
                } );

            var t = "";

            if ( this.value ) {

                t = formLoader.getFullForm();

            }

            if ( graph.labelMode &&  graph.labelMode == "htmlstring" ) {

                t = t.outerHTML;

            } else {

                graph.fireEvent( new mxEventObject( "checkCellLink", "cellHTML", t ) );

            }

            graph.cellHTMLCache[ this.id ] = t;

            return t;

        }

        return graph.cellHTMLCache[ this.id ];

    };

    /*
     * Helper function to get all the attachments
     *
     */

    mxCell.prototype.getAttachmentString = function getAttachmentStringF( graph, markdown ) {

        var attachments = [];
        if ( this.isWhatbox() && this.numberOfAttachments() > 0 ) {

            $( "attachments > *", this.value ).each( function( i, att ) {

                if ( att.nodeName === skConstants.RESPONSIBILITY ) {

                    // get the role id
                    var v = skUtils.readValue(
                        graph.roleManager.getRoleByLocalID(
                            skUtils.readValue(
                                $( "role", att )[ 0 ],
                                skConstants.LOCAL_ID ) ) );

                    if ( markdown ) {

                        v = skUtils.renderMarkdown( v, true );

                    }

                    // add the tags
                    if ( $( "tag", att ).length ) {

                        var tags = [];

                        $( "tag", att ).each( function( j, tag ) {

                            tags.push( skUtils.readValue( tag ) );

                        } );

                        v += " (";
                        v += tags.join( ", " );
                        v += ")";

                    }

                    attachments.push( v );

                } else if ( att.nodeName === skConstants.EXT_CONTENT ) {

                    attachments.push( mxUtils.htmlEntities( skUtils.readValue( att ) ) );

                } else {

                    var t = skUtils.readValue( att );
                    if ( markdown ) {

                        t = skUtils.renderMarkdown( t, true );

                    }

                    attachments.push( t );

                }

            } );

            return attachments.join( skConstants.SEPARATOR );

        } else {

            return "";

        }

    };

    /*
     * Helper function to get the text of a cell
     *
     * markdown : false by default
     * cutTheFat : will remove links, header, formatting, etc. to render inline/one line
     *
     */

    mxCell.prototype.getBoxText = function getBoxTextF( markdown, cutTheFat ) {

        markdown = markdown !== undefined ? markdown : false;
        cutTheFat = cutTheFat !== undefined ? cutTheFat : false ;

        var text = "";

        text = skUtils.readValue( $( "box text", this.value ) ) ;

        if ( markdown ) {

            return skUtils.renderMarkdown( text, cutTheFat );

        }

        return text;

    };

    // returns array of LocalRoleID
    mxCell.prototype.getBoxRoles = function getBoxRolesF( itemNumber, parentValue ) {

        var res = [];
        itemNumber = itemNumber || 0;

        parentValue = parentValue !== undefined ? parentValue : this.value;

        if ( this.value ) {

            if ( itemNumber ) {

                res.push(
                    skUtils.readValue(
                        $( skConstants.ROLE, this.value )[ itemNumber ],
                        skConstants.LOCAL_ID ) );

            } else {

                $( skConstants.ROLE, this.value ).each( function( i, role ) {

                    res.push(
                        skUtils.readValue( role, skConstants.LOCAL_ID ) );

                } );

            }

        }

        return res;

    };

    mxCell.prototype.getInboundsText = function getInboundsTextF() {

        var text = "";

        if ( this.edges && this.edges.length > 0 ) {

            var me = this;

            $( this.edges ).each( function( i, e ) {

                if ( e.target == me ) {

                    // Special treatment for the first one
                    if ( text === "" ) {

                        text += e.source.getBoxText();

                    } else {

                        text += " | " + e.source.getBoxText(); // Fixme constant

                    }

                }

            } );

        }

        return text;

    };

    mxCell.prototype.getOutboundsText = function getOutboundsTextF() {

        var text = "";

        if ( this.edges && this.edges.length > 0 ) {

            var me = this;

            // FIXME : use getOutboundsCells
            $( this.edges ).each( function( i, e ) {

                if ( e.source == me ) {

                    // Special treatment for the first one
                    if ( text === "" ) {

                        text += e.target.getBoxText();

                    } else {

                        text += " | " + e.target.getBoxText(); // Fixme constant

                    }

                }

            } );

        }

        return text;

    };

    /*
     * SIPOC stuffs
     *
     * suppliers : list of resources of the inbound whatbox
     *      (need to go through one or more ybox)
     *
     * suppliersHandoverCount : number of resource of suppliers not same as current box
     *
     *
     * customers / customerCounts : same
     *
     */

    mxCell.prototype.getInboundsCells = function getInboundsCellsF( outbounds ) {

        var cells = [];

        var currentCell = this;

        $( this.edges ).each( function( i, edge ) {

            if ( outbounds ) {

                if ( edge.source == currentCell ) {

                    cells.push( edge.target );

                }

            } else {

                if ( edge.target == currentCell ) {

                    cells.push( edge.source );

                }

            }

        } );

        return cells;

    };

    // FIXME : rename : will actually "jump" to get cells one step ahead (because Y B alternate)
    mxCell.prototype.getInboundsCellsSameType = function getInboundsCellsSameTypeF( outbounds ) {

        var cells;

        cells = this.getInboundsCells( outbounds );

        var cellsSameType = [];

        // Be careful to include only cell of the same type
        // what if we have a mode where there is no "alternative" between Y and W ?

        $( cells ).each( function( i, cell ) {

            cellsSameType = cellsSameType.concat( cell.getInboundsCells( outbounds ) );

        } );

        return cellsSameType;

    };

    mxCell.prototype.getOutboundsCells = function getOutboundsCellsF() {

        return this.getInboundsCells( true );

    };

    mxCell.prototype.getOutboundsCellsSameType = function getOutboundsCellsSameTypeF() {

        return this.getInboundsCellsSameType( true );

    };

    mxCell.prototype.getSuppliers = function getSuppliersF( graph, customers ) {

        var supplierList = [];

        // get all the relevant cells
        var cells = this.getInboundsCellsSameType( customers );

        // get the roles for each cells
        $( cells ).each( function( i, cell ) {

            supplierList = supplierList.concat( cell.getBoxRoles() );

        } );

        return supplierList;

    };

    mxCell.prototype.getCustomers = function getCustomersF() {

        return this.getSuppliers( true );

    };

    mxCell.prototype.getSuppliersUniqueList = function getSuppliersUniqueListF( customers ) {

        return $.distinct( this.getSuppliers( customers ) );

    };

    mxCell.prototype.getCustomersUniqueList = function getCustomersUniqueListF() {

        return this.getSuppliersUniqueList( true );

    };

    mxCell.prototype.getSuppliersUniqueListText = function getSuppliersUniqueListTextF( customers ) {

        return this.getSuppliersUniqueList( customers ).join( skConstants.SEPARATOR );

    };

    mxCell.prototype.getCustomersUniqueListText = function getCustomersUniqueListTextF() {

        return this.getSuppliersUniqueListText( true );

    };

    /*
     * Parents
     *
     *
     *********************/

    mxCell.prototype.getParents = function getParentsF() {

        // Get all the parents in an array
        var parents = [];
        var p = this.getParent();

        while ( p && p.id !== 0 ) {

            parents.unshift( p );

            p = p.getParent();

        }

        // Get all the parent TEXT
        var parentTexts = parents.map( function( cell ) {

            return cell.getBoxText();

        } );

        // Return the object
        return {
            "toString": parentTexts.join( " | " ),
            "cells": parents,
            "count": parents.length
        };

    };

    mxCell.prototype.getSuppliersAndCustomers = function getSuppliersAndCustomersF() {

        return {
            "suppliersUniqueResourcesList": "resource1 | resource2 | resource3",
            "suppliersUniqueResourcesListCount": 3,
            "suppliersUniqueResourcesListWithCount": [
                {
                    "resource": "resource1",
                    "count":3,
                    "cells": [ "mxCell", "mxCell", "mxCell" ]
                },
                {
                    "resource":"resource2",
                    "count":1,
                    "cell": [ "mxCell" ]
                } ],
            "supplierUniqueResources": []

            // Ditto customers

        };

    };

} );
