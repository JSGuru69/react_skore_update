
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    var skExternalContent = {};

    var embedContainerCSS = "<style>" +
            ".embed-container { " +
                "position: relative; " +
                "padding-bottom: 56.25%;" +
                "height: 0;" +
                "overflow: hidden;" +
                "max-width: 100%;" +
                "height: auto;" +
            "}" +
            ".embed-container iframe, .embed-container object, .embed-container embed { " +
                "position: absolute;" +
                "top: 0;" +
                "left: 0;" +
                "width: 100%;" +
                "height: 100%;" +
            "}" +
            "</style>";
    var embedContainerDivOpen = "<div class='embed-container'>";
    var embedContainerDivClose = "</div>";

    // https://github.com/jeffehobbs/embedresponsively

    skExternalContent.youtube = function( youtubeURL ) {

        var youtubeID;
        if ( youtubeURL.length > 28 ) {

            var uri = youtubeURL;
            var queryString = {};
            uri.replace(
                new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
                function( $0, $1, $2, $3 ) {

                    queryString[ $1 ] = $3;

                }
            );
            youtubeID = ( queryString.v );

        } else {

            youtubeID = youtubeURL.substring( 16 );

        }

        var youtubeEmbed = "<iframe src='http://www.youtube.com/embed/" + youtubeID + "' frameborder='0' allowfullscreen></iframe>";
        return $( "<div>" +
                        embedContainerCSS + embedContainerDivOpen + youtubeEmbed + embedContainerDivClose +
                    "</div>" );

    };

    skExternalContent.vimeo = function( vimeoURL ) {

        // var vimeoURL = $("#vimeo-url").val();
        var vimeoID;
        var protocol = vimeoURL.slice( 0, 5 );
        if ( protocol == "https" ) {

            //then the video is served via https, doy
            vimeoID = vimeoURL.substring( 18 );

        } else {

            protocol = "http";
            vimeoID = vimeoURL.substring( 17 );

        }

        var vimeoEmbed = "<iframe " +
                "src='" + protocol + "://player.vimeo.com/video/" + vimeoID + "' " +
                "frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen>" +
            "</iframe>";

        return $( "<div>" +
                embedContainerCSS + embedContainerDivOpen + vimeoEmbed + embedContainerDivClose +
            "</div>" );

    };

    skExternalContent.dailymotion = function( dailymotionURL ) {

        var m;
        var dailymotionID = ( m = dailymotionURL.match( new RegExp( "\/video\/([^_?#]+).*?" ) ) ) ? m[ 1 ] : void 0;
        var dailymotionEmbed = "<iframe src='http://www.dailymotion.com/embed/video/" +
                dailymotionID + "' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";

        return $( "<div>" +
                    embedContainerCSS + embedContainerDivOpen + dailymotionEmbed + embedContainerDivClose +
                "</div>" );

    };

    skExternalContent.googlemaps = function( googlemapsURL ) {

        var escapediFrameURL = googlemapsURL.replace( /\"/g, "'" );
        var escapediFrameURLCode = escapediFrameURL.replace( /</g, "<" );
        var escapediFrameURLCodeFinal = escapediFrameURLCode.replace( />/g, ">" );

        return $( "<div>" +
                    embedContainerCSS + embedContainerDivOpen + escapediFrameURLCodeFinal + embedContainerDivClose +
                "</div>" );

    };

    skExternalContent.image = function( imageURL ) {

        return $( "<img src='" + imageURL + "' class='img-responsive' />" );

    };

    skExternalContent.iframe = function( genericURL ) {

        var escapediFrameURL = genericURL.replace( /\"/g, "'" );
        var escapediFrameURLCode = escapediFrameURL.replace( /</g, "<" );
        var escapediFrameURLCodeFinal = escapediFrameURLCode.replace( />/g, ">" );

        return $( "<div>" +
                    embedContainerCSS + embedContainerDivOpen + escapediFrameURLCodeFinal + embedContainerDivClose +
                "</div>" );

    };

    return skExternalContent;

} );
