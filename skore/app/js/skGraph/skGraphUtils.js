define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skConstants = require( "skConstants" );
    var skUtils = require( "skUtils" );
    var mxRectangle = require( "mxRectangle" );

    /**
     * @typedef {object} synth_boxes_result
     * @property {number} depth
     * @property {Array.<mxCell>} whatboxes
     * @property {number} whatboxesCount
     * @property {Array.<mxCell>} whatboxesDetailed
     * @property {number} whatboxesDetailedCount
     * @property {Array.<mxCell>} whyboxes
     * @property {number} whyboxesCount
     * @property {number} attachmentsCount
     */

    /**
     * @param {mxGraphModel} model
     * @return {Array.<*>}
     */
    var synth_handover = function synth_handoverF( model ) {

        var relations = [];

        console.log( "synth_handover" );

        model.filterDescendants( function( cell ) {

            /*
             [{code, sourceCell, sourceResource, whybox, destinationCell, destinationResource},
             ...
             ]
             */
            var a = cell.highlightLines( false ); // Does not update the style, just looking for the connections

            if ( a !== undefined && a.length > 0 ) {

                $.each( a, function( i, line ) {

                    relations.push( line );

                } );

            }

        } );

        var findRelation = function findRelationF( array, sourceRes, destRes ) {

            var i;

            for ( i = 0; i < array.length; i++ ) {

                if ( array[ i ][ 0 ] == sourceRes && array[ i ][ 1 ] == destRes ) {

                    return array[ i ];

                }

            }

            return null;

        };

        /*
         Sort the relations

         from "relations" :
         [{code, sourceCell, sourceResource, whybox, destinationCell, destinationResource}, ...]

         to "result" :
         [sourceResource, destinationResource, [whyboxes], [sourceCells], [destinationCells] ], ...]
         0                 1                   2           3               4
         */
        var result = [];

        $.each( relations, function( i, e ) {

            //

            // "fetch" the exising relation for source-dest if possible
            // so that it can be "completed"
            var line = findRelation( result, e.sourceResource, e.destinationResource );

            if ( line ) {

                line[ 2 ].push( e.whybox );
                line[ 3 ].push( e.sourceCell );
                line[ 4 ].push( e.destinationCell );

            }

            // Or create the new line
            else {

                result.push( [ e.sourceResource, e.destinationResource, [ e.whybox ], [ e.sourceCell ], [ e.destinationCell ] ] );

            }

        } );

        return result.sort();

    };

    // As handovers in both direction are displayed on the same line we need a way to find the "opposite" relation
    var findRelationOpposite = function findRelationOppositeF( array, source, dest ) {

        var i;

        for ( i = 0 ; i < array.length; i++ ) {

            if ( array[ i ][ 0 ] == source && array[ i ][ 1 ] == dest ) {

                // We mark it as done so it's not counted twice
                array[ i ].done = true;

                return array[ i ];

            }

        }

        return null;

    };

    var getBoxRoles = function( graph, cell ) {

        return cell.getBoxRoles();

    };

    var getBoxRolesString = function( graph, cell, markdown ) {

        var tmpRoles = getBoxRoles( graph, cell );

        var tmpRoles2 = tmpRoles.map( function( r ) {

            var t = skUtils.readValue( graph.roleManager.getRoleByLocalID( r ) );

            if ( markdown ) {

                return skUtils.renderMarkdown( t, true );

            }

            return t;

        } );

        return tmpRoles2.join( skConstants.SEPARATOR );

    };

    var createCellObject = function( cell ) {

        return {
            cell: cell,
            id: cell.id,
            text: cell.getBoxText( false, false ),
            isGroup: cell.isGroup()
        };

    };

    var getBreadcrumbs = function( graph ) {

        var cell = graph.getDefaultParent();

        // Adds the current cell by default;
        var allCells = [ createCellObject( cell ) ];

        // Gathers all the parent cells
        while ( cell.parent && ( cell.parent != graph.model.getRoot() ) ) {

            allCells.unshift( createCellObject( cell.parent ) );
            cell = cell.parent;

        }

        return allCells;

    };

    var checkInternalLinks = function( graph, html ) {

        var warn = false;

        $( ".skGoTo", html ).each( function( i, link ) {

            var dest = "" + $( link ).data( "goto" );

            // check if the cell exists
            var cellID = dest.match( /\d+/ );
            var realCell = graph.model.cells[ cellID ];

            if ( !realCell ) {

                warn = true;

                $( link ).css( { "border-bottom":"1px dashed gray", "display":"inline" } );
                $( link ).attr( "data-toggle", "tooltip" );
                $( link ).attr( "data-placement", "bottom" );
                $( link ).attr( "title", "Link within this process is broken (box " + dest + " does not exist)" );

            }

        } );

        if ( warn ) {

            $( '[data-toggle="tooltip"]', html ).tooltip( { container:"body" } );

        }

    };

    var getAllProcessTextAsHtml = function( graph ) {

        var t = "";

        graph.model.filterDescendants( function( cell ) {

            if ( !cell.isEdge() ) {

                var text = cell.getBoxText();

                if ( text !== "(unnamed)" && text.trim() !== "" ) {

                    t += text + "\n\n";

                    t += getBoxRolesString( graph, cell, true ) + "\n\n";

                    t += cell.getAttachmentString( graph, true ) + "\n\n";

                }

                return true;

            }

        } );

        return skUtils.renderMarkdown( t );

    };

    var getPageSettings = function( graph ) {

        var paperSizes = skConstants.paperSizes;

        var pageSettings = {

            pageView: graph.pageVisible,
            grid: graph.isGridEnabled(),
            pageScale: graph.pageScale,
            backgroundColor: ( graph.background == null || graph.background == "none" ) ? "#ffffff" : graph.background
        };

        var allPaperSizes = Object.keys( paperSizes );
        for ( var i = 0; i < allPaperSizes.length; i++ ) {

            var format = allPaperSizes[ i ];

            if ( graph.pageFormat.width == paperSizes[ format ].width &&
            graph.pageFormat.height == paperSizes[ format ].height ) {

                pageSettings.paperSize = format;
                pageSettings.landscape = false;
                pageSettings.format = new mxRectangle( 0, 0, paperSizes[ format ].width, paperSizes[ format ].height );

            } else if ( graph.pageFormat.width == paperSizes[ format ].height &&
                graph.pageFormat.height == paperSizes[ format ].width ) {

                pageSettings.paperSize = format;
                pageSettings.landscape = true;
                pageSettings.format = new mxRectangle( 0, 0, paperSizes[ format ].width, paperSizes[ format ].height );

            }

        }

        return pageSettings;

    };

    var getPageFormat = function( paperSizeName, landscape ) {

        return new mxRectangle(
            0,
            0,
            landscape ? skConstants.paperSizes[ paperSizeName ].height : skConstants.paperSizes[ paperSizeName ].width,
            landscape ? skConstants.paperSizes[ paperSizeName ].width : skConstants.paperSizes[ paperSizeName ].height
            );

    };

    return {

        synth_handover: synth_handover,
        findRelationOpposite: findRelationOpposite,
        getBoxRolesString: getBoxRolesString,
        checkInternalLinks:checkInternalLinks,
        getBoxRoles: getBoxRoles,
        getBreadcrumbs: getBreadcrumbs,
        getAllProcessTextAsHtml: getAllProcessTextAsHtml,
        getPageFormat: getPageFormat,
        getPageSettings: getPageSettings

    };

} );
