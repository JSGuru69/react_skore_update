define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skShell = require( "skShell" );

    var skTemplateDescriptions = require( "./skTemplateDescriptions" );

    var TEMPLATE = skConstants.TEMPLATE;
    var TEMPLATES  = skConstants.TEMPLATES;
    var VALUE  = skConstants.VALUE;
    var NAME = skConstants.TEMPLATE_NAME;

    function skTemplateManager( graph, existingTemplateManager ) {

        /*jshint validthis:true*/

        // loads the templates for the various type of items
        var that = this;
        this.graph = graph;

        // starts with "base"
        this.currentTemplateForForm = {};
        this.formTypes = Object.keys( skTemplateDescriptions );
        $( this.formTypes ).each( function( i, type ) {

            that.currentTemplateForForm[ type ] = "base";

        } );

        // init the built-in types as well
        $( Object.keys( skTemplateDescriptions ) ).each( function( i, formCategory ) {

            $( Object.keys( skTemplateDescriptions[ formCategory ] ) ).each( function( j, form ) {

                skTemplateDescriptions[ formCategory ][ form ].isBuiltIn = true;

            } );

        } );

        // now we load the custom things
        if ( existingTemplateManager ) {

            graph.model.root.templates = existingTemplateManager;

        }

        // loads the existing templates
        if ( graph.model.root.templates ) {

            /*
                <templates>
                    <template value="whybox" name="base" /> // <-- not saved !! just for the example
                    <template value="whatbox" name="base" />
                    <template value="responsiblity" name="base\ratsi|raci" description="JSON-if-necessary" />
                    ...
                </templates>
            */

            // for each entry in the existing templates of the graph
            $( TEMPLATE, graph.model.root.templates ).each( function( i, template ) {

                // get the type and the template
                var formType = $( template ).attr( VALUE );
                var templateName = $( template ).attr( NAME );

                // check if formType is right
                if ( that.formTypes.indexOf( formType ) > 0 ) {

                    // checks it's part of the "standard" offering
                    // if ( skTemplateDescriptions[ formType ][ templateName ] ) {

                       that.currentTemplateForForm[ formType ] = templateName;

                    // }

                    // it's not part of the standard offering
                    if ( !skTemplateDescriptions[ formType ][ templateName ] ) {

                    // else {

                        // load it in the templates description
                        skTemplateDescriptions[ formType ][ templateName ] = JSON.parse( $( template ).attr( skConstants.TEMPLATE_DESCRIPTION ) );

                    }

                }

            } );

        }

        // nothing found, just make a new init
        else {

            this.initGraph();

        }

    }

    skTemplateManager.prototype.graph = null;
    skTemplateManager.prototype.currentTemplateForForm = null;

    // init / save the forms to the graph
    skTemplateManager.prototype.initGraph = function() {

        var graph = this.graph;
        var that = this;
        var templates = graph.model.root.templates;

        if ( !graph.model.root.templates ) {

            // creates the node
            templates = graph.model.root.templates = mxUtils.createXmlDocument().createElement( TEMPLATES );

        }

        // first check for old file if there is "tagsInUse"
        if ( graph.model.root.tagsInUse ) {

            /*
            <extension value="mrt" type="RATSI" as="tagsInUse">
                <tag value="R" style="css here" name="Responsible"/>
                <tag value="A" style="css here" name="Authority"/>
                <tag value="T" style="css here" name="Task"/>
                <tag value="S" name="Support"/>
                <tag value="I" name="Informed"/>
            </extension>
            */
            var tagsInUse = graph.model.root.tagsInUse;
            var type = tagsInUse.getAttribute( "type" );
            switch ( type ) {
                case "RATSI":
                    that.currentTemplateForForm[ skConstants.RESPONSIBILITY ] = skConstants.RESP_RATSI;
                break;
                case "RACI" :
                    that.currentTemplateForForm[ skConstants.RESPONSIBILITY ] = skConstants.RESP_RACI;
                break;
                case "custom":

                    // this is a "template" for the template description that will be reused
                    var customFields = [ {
                            name:"role",
                            localid:true,
                            type:"singleline",
                            autocomplete: {
                                source:"roles",
                                enrichsource:true
                            },
                            defaulttext: "Who does it?"
                        }

                        // the custom tags will go here
                        /* example :
                        {
                            name:"tag",
                            value: "R",
                            type:"button",
                            description:"Responsible",
                            style:{ "background-color":"rgba(255, 165, 0, 0.5)" }
                        }
                        */
                    ];
                    var customTemplate = {
                        editable:true,
                        fields: [ { name:"responsibility", type:"group", removable:true,
                            multi: { addbutton: true, addtext: "Add 1 more role", addempty:true },
                            fields: customFields } ] };

                    // needs a bit more work...

                    // var desc = $( mxUtils.clone( skTemplateDescriptions.whatbox.multirolesratsi );
                    // desc.fields[ 2 ].fields[ 1 ].fields = [];
                    // var f = desc.fields[ 2 ].fields[ 1 ].fields;
                    $( "tag", tagsInUse ).each( function( i, tag ) {

                        customFields.push( {
                            name:"tag",
                            value: $( tag ).attr( VALUE ),
                            type:"button",
                            description: $( tag ).attr( NAME ),
                            style: {} // todo convert style
                        } );

                    } );

                    // save in the form

                    // make it active
                    that.currentTemplateForForm[ skConstants.RESPONSIBILITY ] = "custom";
                    skTemplateDescriptions[ skConstants.RESPONSIBILITY ].custom = customTemplate;

                break;
            }

            graph.model.root.tagsInUse = null;

        }

        // now, actually write them in the graph for save

        that.formTypes.forEach( function( formType ) {

            var template = $( "" + TEMPLATE + "[" + VALUE + "=" + formType + "]", templates );

            // if default, can remove the template

            var crtTmplte4Form = that.currentTemplateForForm[ formType ];
            if (  crtTmplte4Form == skConstants.TEMPLATE_BASE ) {

                if ( template.length ) {

                    template.remove();

                }

            // } else if ( crtTmplte4Form === "raci" || crtTmplte4Form === "ratsi" ) {

            } else {

                // if other, create the template
                if ( !template.length ) {

                    template = $( "<" + TEMPLATE + " " +
                        VALUE + "='" + formType + "' " +
                        "/>" );
                    $( templates ).append( template );

                }
                if ( template.length === 1 ) {

                    template.attr( NAME, crtTmplte4Form );

                }

                template.removeAttr( skConstants.TEMPLATE_DESCRIPTION );

                // if not built-in, must write the json
                // if ( !skTemplateDescriptions[ formType ][ that.currentTemplateForForm[ formType ] ].isBuiltIn ) {
                if ( that.currentTemplateForForm[ formType ] === "custom" ) {

                    template.attr(
                        skConstants.TEMPLATE_DESCRIPTION,
                        JSON.stringify( skTemplateDescriptions[ formType ][ that.currentTemplateForForm[ formType ] ] ) );

                }

            }

        } );

    };

    skTemplateManager.prototype.validateFormJSON = function( JSONTemplateDescription ) {

        try {

          var c = $.parseJSON( JSONTemplateDescription );
          return c;

        }
        catch ( err ) {

            console.error( err );
            skShell.setStatus( "JSON Form not ok " + err );
            return false;

        }

    };

    skTemplateManager.prototype.setTemplateForForm = function( formType, templateName, customFormJSONDescription ) {

        this.currentTemplateForForm[ formType ] = templateName;

        // standard form unless there is a customFormJSONDescription and it's not built in
        if ( customFormJSONDescription &&
            this.validateFormJSON( customFormJSONDescription ) &&
            templateName === "custom" ) {

            // !skTemplateDescriptions[ formType ][ templateName ].isBuiltIn ) {

            skTemplateDescriptions[ formType ][ templateName ] = JSON.parse( customFormJSONDescription );

        }

        this.initGraph();

    };

    skTemplateManager.prototype.getTemplate = function( type ) {

        return skTemplateDescriptions[ type ][ this.currentTemplateForForm[ type ] ];

    };

    skTemplateManager.prototype.formTypes = null;
    skTemplateManager.prototype.whybox = {};
    skTemplateManager.prototype.whatbox = {};
    skTemplateManager.prototype.group = {};
    skTemplateManager.prototype.stickynote = {};
    skTemplateManager.prototype.title = {};
    skTemplateManager.prototype.role = {};

    return skTemplateManager;

} );
