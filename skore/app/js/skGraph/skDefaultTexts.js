define( function() {

    "use strict";

    return function setupSkDefaultTexts( skShell ) {

        skShell.defaultTexts = {

            // Must match skConstants value
            whatbox: "What happens?",
            resource: "Who does it?",
            whybox: "So that?",
            stickynote: "A comment",
            title: "Name your Skore",
            group: "Name your group",
            line: "",

            GRP_GROUP: "Group these boxes",
            GRP_UNGROUP: "Remove group (ungroup)",

            REL_VISIBLE_ENG: "current view",
            REL_PARENT_ENG: "is the parent",
            REL_GRD_PARENT_ENG: "is a grand parent",
            REL_CHILD_ENG: "is in a detailed view (direct)",
            REL_GRD_CHILD_ENG: "is in a detailed view (2 levels or more)",
            REL_NONE_ENG: "",
            REL_EQUAL_ENG: "current view"
        };

    };

} );
