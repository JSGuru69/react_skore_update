/**
 * Constructs a new graph instance. Note that the constructor does not take a
 * container because the graph instance is needed for creating the UI, which
 * in turn will create the container for the graph. Hence, the container is
 * assigned later in EditorUi.
 */
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCellRenderer = require( "mxCellRenderer" );
    var mxClient = require( "mxClient" );
    var mxConnectionConstraint = require( "mxConnectionConstraint" );
    var mxConstants = require( "mxConstants" );
    var mxEvent = require( "mxEvent" );
    var mxEventObject = require( "mxEventObject" );
    var mxGraph = require( "mxGraph" );
    var mxGraphView = require( "mxGraphView" );
    var mxMouseEvent = require( "mxMouseEvent" );
    var mxPanningHandler = require( "mxPanningHandler" );
    var mxPoint = require( "mxPoint" );
    var mxPolyline = require( "mxPolyline" );
    var mxRectangle = require( "mxRectangle" );
    var mxRectangleShape = require( "mxRectangleShape" );
    var mxShape = require( "mxShape" );
    var mxStencil = require( "mxStencil" );
    var mxStencilRegistry = require( "mxStencilRegistry" );
    var mxText = require( "mxText" );
    var mxUtils = require( "mxUtils" );
    var skCellHighlighter = require( "skNavigator/skCellHighlighter" );
    var skColors = require( "skGraph/skColors" );
    var skConstants = require( "skConstants" );
    var skEdgeSegmentHandler = require( "skGraph/skEdgeSegmentHandler" );
    var skTemplateManager = require( "skGraph/skTemplateManager" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );

    // var skGraphPortClickHandler = require( "skGraph/skGraphPortClickHandler" );
    var skRoleManager = require( "skGraph/skNewRoleManager" );
    var skShell = require( "skShell" );
    var skStylesheet = require( "skStylesheet/skStylesheet" );
    var skUtils = require( "skUtils" );

    /**
     * Boxes
     *
     */
    mxConstants.DEFAULT_FONTFAMILY = "'Open Sans', 'sans-serif'"; // Font for cell
    mxConstants.DEFAULT_FONTSIZE = 15; // Font size
    mxConstants.DEFAULT_VALID_COLOR = skColors.BOX_VALID;
    mxConstants.VERTEX_SELECTION_COLOR = skColors.BOX_SELECTED;
    mxConstants.VERTEX_SELECTION_STROKEWIDTH = skConstants.STROKE_BOX_SELECTED; //3;
    mxConstants.EDGE_SELECTION_STROKEWIDTH = skConstants.STROKE_LINE_selected; //2;
    mxConstants.VERTEX_SELECTION_DASHED = false;

    /**
     * Lines
     *
     */
    mxConstants.EDGE_SELECTION_COLOR = skColors.BOX_SELECTED;
    mxConstants.EDGE_SELECTION_DASHED = false;

    /**
     * Sticky Note
     *
     */
    mxConstants.HANDLE_SIZE = 14;

    /**
     * Other
     *
     */
    mxConstants.GUIDE_COLOR = skColors.GUIDE;
    mxConstants.GUIDE_STROKEWIDTH = 2;
    mxConstants.OUTLINE_COLOR = skColors.OUTLINE_FRAME;

    // Style of the wBoxDetailed
    // Note wBoxDetailed, supports size style
    /**
     * @constructor
     */
    function wBoxDetailed() {

        /*jshint validthis:true*/
        mxShape.call( this );

    }
    mxUtils.extend( wBoxDetailed, mxShape );

    wBoxDetailed.prototype.paintBackground = function paintBackgroundF( c, x, y, w, h ) {

        var stickSize = 20 * this.scale;
        var stickOffset = -5 * this.scale;

        // Draws corners

        // Top left
        c.translate( x, y );

        c.begin();
        c.moveTo( -stickOffset, -stickOffset + stickSize );
        c.lineTo( -stickOffset, -stickOffset );
        c.lineTo( -stickOffset + stickSize, -stickOffset );

        // Top right
        c.moveTo( w + stickOffset - stickSize, -stickOffset );
        c.lineTo( w + stickOffset, -stickOffset );
        c.lineTo( w + stickOffset, -stickOffset + stickSize );

        // C.end();

        // Bottom right
        c.moveTo( w + stickOffset - stickSize, h + stickOffset );
        c.lineTo( w + stickOffset, h + stickOffset );
        c.lineTo( w + stickOffset, h + stickOffset - stickSize );

        // Bottom left
        c.moveTo( -stickOffset, h + stickOffset - stickSize );
        c.lineTo( -stickOffset, h + stickOffset );
        c.lineTo( -stickOffset + stickSize, h + stickOffset );

        // Draws the rectangle
        c.moveTo( 0, 0 );
        c.lineTo( w, 0 );
        c.lineTo( w, h );
        c.lineTo( 0, h );
        c.lineTo( 0, 0 );
        c.close();
        c.end();

        c.stroke();

    };

    mxCellRenderer.registerShape( skConstants.whatboxdetailed, wBoxDetailed );

    // Todo: it is probably better to overwrite mxCellRenderer.prototype.redrawControl
    // or even better create skCellRenderer and overwrite redrawControl there
    // disables the built-in expand / collapse icons because I have my own in skore.ActionButtons
    mxCellRenderer.prototype.getControlBounds = function getControlBoundsF() {

        return new mxRectangle();

    };

    // Todo: investigate the bug
    /**
     * There seem to be a bug in some cases when a flow
     * line is across 2 groups ? Or is it because the line is first
     * in the file ?
     *
     * so I add the first condition, return the shape only if it has a node
     *
     */
    mxCellRenderer.prototype.getShapesForState = function getShapesForStateF( state ) {

        if ( state.shape != null && state.shape.node != null ) {

            return [ state.shape, state.text ];

        } else {

            return [ null, state.text ];

        }

    };

    ( function() {

        // Loads the ybox stencil into the registry
        // jscs:disable maximumLineLength
        var xmlShape = mxUtils.parseXml( '<shapes><shape name="' + skConstants.whybox + '" h="50" w="100" aspect="variable" strokewidth="inherit"><foreground><path><move x="5" y="5"/><arc rx="50" ry="50" x-axis-rotation="0" large-arc-flag="0" sweep-flag="0" x="5" y="45"/></path><stroke/><path><move x="95" y="5"/><arc rx="50" ry="50" x-axis-rotation="0" large-arc-flag="0" sweep-flag="1" x="95" y="45"/></path><stroke/></foreground></shape></shapes>' );

        // jscs:enable maximumLineLength

        var shape = xmlShape.documentElement.firstChild;

        while ( shape != null ) {

            if ( shape.nodeType == mxConstants.NODETYPE_ELEMENT ) {

                mxStencilRegistry.addStencil( shape.getAttribute( "name" ), new mxStencil( shape ) );

            }

            shape = shape.nextSibling;

        }

    }() );

    /**
     * @constructor
     */
    function skGraph( container, model, renderHint, stylesheet, templateManager, roleManager ) {

        /*jshint validthis:true*/

        mxGraph.call( this, container, model, renderHint, stylesheet );

        var graph = this;

        graph.isHtmlLabel = function isHtmlLabelF() {

            return true;

        };

        graph.enabled = false;

        graph.defaultPageVisible = false;
        graph.pageVisible = graph.defaultPageVisible;
        graph.setPanning( true );
        graph.resetViewOnRootChange = false;
        graph.scrollbars = true;
        graph.background = null;
        graph.setConnectable( true );
        graph.setDropEnabled( false );
        graph.setAllowLoops( false );
        graph.setExtendParents( false );
        graph.setExtendParentsOnAdd( false );
        graph.setAllowDanglingEdges( false ); // A line MUST be connected otherwise it disappears
        graph.pageScale = 1;
        graph.view.scale = 1;
        graph.gridEnabled = false;
        graph.graphHandler.guidesEnabled = true;
        graph.foldingEnabled = true;
        graph.setTooltips( false );
        graph.allowAutoPanning = true;
        graph.setConstrainChildren( false ); // ?
        graph.isClipping = false;
        graph.cellHTMLCache = [];
        graph.timerAutoScroll = true;
        graph.showBadgeIcon = false;
        graph.zoomFactor = 1.1;
        graph.alternateEdgeStyle = "elbow=vertical";
        graph.pageBreakColor = skColors.PAGE_BREAK;
        graph.pageFormat = mxConstants.PAGE_FORMAT_A4_LANDSCAPE;
        graph.pageBreaksVisible = graph.pageVisible;
        graph.preferPageSize = graph.pageBreaksVisible;
        graph.enterStopsCellEditing = true;
        graph.labelMode = "htmlnode";

        graph.isWrapping = function isWrappingF() {

            return true;

        };

        mxText.prototype.replaceLinefeeds = false;

        graph.getLabel = function getLabelF( cell ) {

            if ( this.labelsVisible && cell.isVertex() ) {

                return cell.createHtmlContent( this );

            }

        };

        /*
         * Update the cell geometry once edit is done (resize)
         *
         */

        graph.setAutoSizeCells( true );

        graph._returnNewHeight = function _returnNewHeightF( cell ) {

            // Get the label
            //noinspection JSPotentiallyInvalidUsageOfThis
            var labelHTML = cell.createHtmlContent( this );
            var tmp;

            if ( mxUtils.isNode( labelHTML )  ) {

                tmp = labelHTML.cloneNode( true );

            } else {

                tmp = $( labelHTML )[ 0 ];

            }

            tmp.setAttribute( "style",

                "font-size:" + ( this.getCellStyle( cell ).fontSize ) + "px;" +

                // For some reason only 1 time the strokeWidth seems to be removed !
                "width:" + ( cell.geometry.width - this.getCellStyle( cell ).strokeWidth ) + "px;" +
                "vertical-align:top;white-space:normal;text-align:center" );

            document.body.appendChild( tmp );

            // A bit clunky but need to apply the right style
            var newHeight = tmp.offsetHeight;

            if ( false ) {

                $( ".debug" ).remove();

                // Uncomment to debug
                $( tmp ).addClass( "debug" );

            }

            tmp.parentElement.removeChild( tmp );

            return newHeight;

        };

        // autosize cell rewrite to work with resizable instead
        // mxGraph.prototype.isAutoSizeCell = function(cell)
        // {
        //     var state = this.view.getState(cell);
        //     var style = (state != null) ? state.style : this.getCellStyle(cell);

        //     return /* this.isAutoSizeCells() || */ ! (style[mxConstants.STYLE_RESIZABLE] == 1);
        // };

        var graphGetPreferredSizeForCell = graph.getPreferredSizeForCell;
        graph.getPreferredSizeForCell = function getPreferredSizeForCellF( cell ) {

            // console.log( "graph.getPreferredSizeForCell" );

            var result = graphGetPreferredSizeForCell.apply( this, arguments );

            // Var style = this.getCellStyle(cell);

            var newHeight, minHeight;

            if ( !cell.isEdge() ) {

                var state = this.view.getState( cell );
                var style = ( state != null ) ? state.style : this.getCellStyle( cell );

                var w, h;

                if ( style[ mxConstants.STYLE_RESIZABLE ] ) {

                    w = cell.geometry.width;
                    h = cell.geometry.height;

                } else {

                    w = parseInt( style.width, 10 ); // skConstants.YB_WIDTH;
                    h = parseInt( style.height, 10 ); //skConstants.YB_HEIGHT;

                }

                if ( cell.isWhybox() ) {

                    result = new mxRectangle(
                        cell.geometry.x,
                        cell.geometry.y,
                        w,
                        h );

                }

                // For groups we just return the same value
                if ( cell.isGroup() ) {

                    result = new mxRectangle(
                        cell.geometry.x,
                        cell.geometry.y,
                        w,
                        h
                    );

                }

                // For stickynote : if newHeight BIGGER than current, we apply,
                // otherwise, ignore
                if ( cell.isStickynote() ) {

                    console.group();

                    // console.log( "getPreferredSizeForCell", "stickynote", cell.id );

                    newHeight = this._returnNewHeight( cell );

                    // console.log( "getPreferredSizeForCell", "stickynote", newHeight );
                    console.groupEnd();

                    // Resize only if needed
                    // minHeight = skConstants.WB_HEIGHT;
                    newHeight = newHeight > cell.geometry.height ? newHeight : cell.geometry.height;

                    // Resize the cell
                    result = new mxRectangle(
                        cell.geometry.x,
                        cell.geometry.y,
                        w,
                        newHeight );

                }

                // Adjust the size of the whatbox
                if ( cell.isWhatbox() ) {

                    newHeight = this._returnNewHeight( cell );

                    // Applies min heights
                    minHeight = parseInt( style.height, 10 ); //skConstants.WB_HEIGHT;
                    newHeight = newHeight < minHeight ? minHeight : newHeight;

                    // Resize the cell
                    result = new mxRectangle(
                        cell.geometry.x,
                        cell.geometry.y,
                        w, //cell.geometry.width //style.width, //skConstants.WB_WIDTH,
                        newHeight );

                }

            }

            return result;

        };

        /*
         * Hover to highlight the cell
         *
         * ?only if interactive?!
         *********************/

        this.addMouseListener( {

            currentState: null,

            // PreviousStroke: null,
            hoverButtons: null,

            mouseDown: function() {

            },
            mouseMove: function( sender, me ) {

                if ( this.currentState != null && me.getState() == this.currentState && graph.isMouseDown ) {

                    return;

                }

                var tmp = graph.view.getState( me.getCell() );

                // Ignores everything but vertices
                if ( graph.isMouseDown || ( tmp != null && !graph.getModel().isVertex( tmp.cell ) ) ) {

                    tmp = null;

                }

                if ( tmp != this.currentState ) {

                    if ( this.currentState != null ) {

                        this.dragLeave( me.getEvent(), this.currentState );

                    }

                    this.currentState = tmp;

                    if ( this.currentState != null ) {

                        this.dragEnter( me.getEvent(), this.currentState );

                    }

                }

            },
            mouseUp: function() {

            },

            // On hover
            dragEnter: function( evt, state ) {

                // Will redraw the shape with the new color
                if ( state != null ) {

                    // Highlight all cells if graph is enabled OR if disabled and it's not a stickynote
                    if ( graph.isEnabled() || ( !graph.isEnabled() && !state.cell.isStickynote() ) ) {

                        graph.cellHighlighter.toggleHighlightOn( state );

                    }

                    graph.currentCell = state.cell;

                    // Add icons when necessary
                    graph.view.createState( state.cell, true );
                    graph.cellRenderer.redraw( state, true );

                }

            },

            // DragLeave is triggered when the mouse leaves the box
            // AND
            // when the user start dragging the box
            dragLeave: function( evt, state ) {

                if ( state != null && state.shape != null ) {

                    // should destroy the icons when the mouse leaves UNLESS the box is select
                    // if box is selected, icons will be destroyed on select change
                    if ( !( graph.getSelectionCells().length === 1 && graph.getSelectionCell() === graph.currentCell ) ) {

                        graph.cellRenderer.destroyIconsIfNecessary( state );

                    }
                    graph.cellHighlighter.toggleHighlightOff();

                    graph.currentCell = null;

                }

            }
        } ); // End addlistener

        /*
         * Cell highlighter will highlight the cell on hover
         *
         */
        this.cellHighlighter = new skCellHighlighter( this );

        this.visibleAreas = [];

        // Keeps graph container focused on mouse down
        var graphFireMouseEvent = mxGraph.prototype.fireMouseEvent; // Refactor : skore viewer
        this.fireMouseEvent = function fireMouseEventF( evtName ) {

            if ( evtName == mxEvent.MOUSE_DOWN ) {

                //Noinspection JSPotentiallyInvalidUsageOfThis
                this.container.focus();

            }

            graphFireMouseEvent.apply( this, arguments );

        };

        /*
         * REMOVE / DELETE the cells
         *
         *
         *********************/

        this.addListener( mxEvent.CELLS_REMOVED, mxUtils.bind( this, function( sender, evt ) {

            var that = this;

            // Removes from the cache
            $( evt.properties.cells ).each( function( i, e ) {

                if ( !e.isEdge() ) {

                    delete that.cellHTMLCache[ e.id ];

                }

            } );

        } ) );

        this.addListener( "navigateTo", mxUtils.bind( this, function( sender, event ) {

            this.navigateTo( event.getProperty( "cell" ) );

        } ) );

        this.initTemplateManager( templateManager );
        this.initRoleManager( roleManager );

        // listener to check the links within a process and outside

        this.addListener( "checkCellLink", mxUtils.bind( this, function( sender, event ) {

            if ( this.isEnabled() ) {

                skGraphUtils.checkInternalLinks( this, event.getProperty( "cellHTML" ) );

            }

        } ) );

    } // End Graph constructor

    // Graph inherits from mxGraph
    mxUtils.extend( skGraph, mxGraph );

    skGraph.prototype.visibleAreas = null;

    skGraph.prototype.createStylesheet = function createStylesheetF() {

        return new skStylesheet( this );

    };

    skGraph.prototype.cellHTMLCache = null;
    skGraph.prototype.minFitScale = null;
    skGraph.prototype.maxFitScale = null;
    skGraph.prototype.transparentBackground = false;
    skGraph.prototype.scrollTileSize = new mxRectangle( 0, 0, 400, 400 );

    skGraph.prototype.directLinkModeEnabled = false;

    skGraph.prototype.isDirectLinkModeEnabled = function() {

        return this.directLinkModeEnabled;

    };

    // enable if you want to have click on orange dots to create a box
    // skGraph.prototype.createHandlers = function createHandlersF() {

    //     mxGraph.prototype.createHandlers.call( this );

    //     // jshint -W031
    //     new skGraphPortClickHandler( this );

    // };

    skGraph.prototype.createEdgeSegmentHandler = function createEdgeSegmentHandlerF( state ) {

        return new skEdgeSegmentHandler( state );

    };

    /**
     * Used to display constraints when user moves mouse over the cell
     */
    skGraph.prototype.getAllConnectionConstraints = function getAllConnectionConstraintsF( terminal ) {

        // GetConnectionPoint will convert y = 0.5 into 40px
        return terminal.cell.isEdge() ? null : [

            // north
            new mxConnectionConstraint( new mxPoint( 0.5, 0 ), true ),
            new mxConnectionConstraint( new mxPoint( 0.25, 0 ), true ),
            new mxConnectionConstraint( new mxPoint( 0.75, 0 ), true ),

            // east
            new mxConnectionConstraint( new mxPoint( 1, 0.5 ), true ),

            // south
            new mxConnectionConstraint( new mxPoint( 0.5, 1 ), true ),
            new mxConnectionConstraint( new mxPoint( 0.25, 1 ), true ),
            new mxConnectionConstraint( new mxPoint( 0.75, 1 ), true ),

            // west
            new mxConnectionConstraint( new mxPoint( 0, 0.5 ), true )

        ];

    };

    /**
     * Function: getConnectionPoint
     *
     * Returns the nearest point in the list of absolute points or the center
     * of the opposite terminal.
     *
     * Parameters:
     *
     * vertex - <mxCellState> that represents the vertex.
     * constraint - <mxConnectionConstraint> that represents the connection point
     * constraint as returned by <getConnectionConstraint>.
     */
    skGraph.prototype.getConnectionPoint = function getConnectionPointF( vertex, constraint ) {

        var point = null;

        if ( vertex != null && constraint.point != null ) {

            var bounds = this.view.getPerimeterBounds( vertex );

            var dx = constraint.point.x * bounds.width;

            // Connection always starts at 40px on the left and right sides
            var dy = constraint.point.y === 0 || constraint.point.y === 1 ? constraint.point.y * bounds.height : 40 * this.view.scale;

            point = new mxPoint( bounds.x + dx, bounds.y + dy );

            if ( constraint.perimeter ) {

                point = this.view.getPerimeterPoint( vertex, point, false );

            }
            point.x = Math.round( point.x );
            point.y = Math.round( point.y );

            // PHASE 2
            // If the user is trying to move a connected line (edge) we
            // should not display the connection point that gets in the way

            // console.log("getConnectionPoint", vertex, constraint.point, point.x, point.y);

            var selectedLine = this.getSelectionCell();
            if ( selectedLine &&
                selectedLine.isEdge() &&
                    (
                        (
                            selectedLine.source === vertex.cell &&
                            (
                                this.getCellStyle( selectedLine )[ mxConstants.STYLE_EXIT_Y ] === constraint.point.y &&
                                this.getCellStyle( selectedLine )[ mxConstants.STYLE_EXIT_X ] === constraint.point.x

                            )
                        ) ||
                        (
                            selectedLine.target === vertex.cell &&
                            (
                                this.getCellStyle( selectedLine )[ mxConstants.STYLE_ENTRY_Y ] === constraint.point.y &&
                                this.getCellStyle( selectedLine )[ mxConstants.STYLE_ENTRY_X ] === constraint.point.x
                            )
                        )
                    )
                ) {

                    // console.log( "will not display", constraint.point, point.x, point.y );

                    point.DONOTDISPLAY = true;

                    // hide the port
                    // focusIcons[ i ].node.style.display = "none";

            }

        }

        return point;

    };

    skGraph.prototype.getTitle = function getTitleF() {

        if ( this.model.cells[ 1 ].value ) {

            return skUtils.readValue( this.model.cells[ 1 ].value.getElementsByTagName( skConstants.TEXT ) );

        }

        return "untitled";

    };

    /* ⓘ info
     *
     * This basically deals with the navigation withing the diagram
     * as we are not using the default "exitGroup / enterGroup" (but "navigateTo")
     *
     */

    skGraph.prototype.isValidRoot = function isValidRootF( cell ) {

        return cell.id === "1" || cell.isWhatbox();

    };

    var oSetSelectionCell = skGraph.prototype.setSelectionCell;
    skGraph.prototype.setSelectionCell = function setSelectionCellF() {

        if ( this.isEnabled() ) {

            oSetSelectionCell.apply( this, arguments );

        }

    };

    var oSetSelectionCells = skGraph.prototype.setSelectionCells;
    skGraph.prototype.setSelectionCells = function setSelectionCellsF() {

        if ( this.isEnabled() ) {

            oSetSelectionCells.apply( this, arguments );

        }

    };

    /*
     * Navigates to the given cell
     *
     */

    var mxUtilsReplaceTrailingNewlines = mxUtils.replaceTrailingNewlines;
    mxUtils.replaceTrailingNewlines = function replaceTrailingNewlinesF( str, pattern ) {

        if ( str ) {

            return mxUtilsReplaceTrailingNewlines.call( null, str, pattern );

        }

        return str;

    };

    skGraph.prototype.initTemplateManager = function initTemplateManagerF( existingTemplateManager ) {

        this.templateManager = new skTemplateManager( this, existingTemplateManager );

    };

    skGraph.prototype.initRoleManager = function initRoleManagerF( existingRoleManager ) {

        this.roleManager = new skRoleManager( this, existingRoleManager );

    };

    skGraph.prototype.updateCellsSize = function updateCellsSizeF( cells ) {

        var graph = this;

        var update = function( cell ) {

            if ( cell.isVertex() && cell.value ) {

                graph.updateCellSize( cell );

            }

        };

        graph.model.beginUpdate();

        try {

            if ( cells ) {

                cells.forEach( function( cell ) {

                    update( cell );

                } );

            } else {

                graph.model.filterDescendants( function( cell ) {

                    update( cell );

                } );

            }

        } finally {

            graph.model.endUpdate();

        }

        graph.refresh();

    };

    // Draws page breaks only within the page
    skGraph.prototype.updatePageBreaks = function updatePageBreaksF( visible, width, height )
    {

        var scale = this.view.scale;
        var tr = this.view.translate;
        var fmt = this.pageFormat;
        var ps = scale * this.pageScale;
        var pts, pageBreak, i;

        var bounds2 = this.view.getBackgroundPageBounds();

        width = bounds2.width;
        height = bounds2.height;
        var bounds = new mxRectangle( scale * tr.x, scale * tr.y, fmt.width * ps, fmt.height * ps );

        // Does not show page breaks if the scale is too small
        visible = visible && Math.min( bounds.width, bounds.height ) > this.minPageBreakDist;

        var horizontalCount = ( visible ) ? Math.ceil( width / bounds.width ) - 1 : 0;
        var verticalCount = ( visible ) ? Math.ceil( height / bounds.height ) - 1 : 0;
        var right = bounds2.x + width;
        var bottom = bounds2.y + height;

        if ( this.horizontalPageBreaks == null && horizontalCount > 0 )
        {

            this.horizontalPageBreaks = [];

        }

        if ( this.horizontalPageBreaks != null )
        {

            for ( i = 0; i <= horizontalCount; i++ )
            {

                pts = [ new mxPoint( bounds2.x + ( i + 1 ) * bounds.width, bounds2.y ),
                           new mxPoint( bounds2.x + ( i + 1 ) * bounds.width, bottom ) ];

                if ( this.horizontalPageBreaks[ i ] != null )
                {

                    this.horizontalPageBreaks[ i ].points = pts;
                    this.horizontalPageBreaks[ i ].redraw();

                } else
                {

                    pageBreak = new mxPolyline( pts, this.pageBreakColor );
                    pageBreak.dialect = this.dialect;
                    pageBreak.isDashed = this.pageBreakDashed;
                    pageBreak.pointerEvents = false;
                    pageBreak.init( this.view.backgroundPane );
                    pageBreak.redraw();

                    this.horizontalPageBreaks[ i ] = pageBreak;

                }

            }

            for (  i = horizontalCount; i < this.horizontalPageBreaks.length; i++ )
            {

                this.horizontalPageBreaks[ i ].destroy();

            }

            this.horizontalPageBreaks.splice( horizontalCount, this.horizontalPageBreaks.length - horizontalCount );

        }

        if ( this.verticalPageBreaks == null && verticalCount > 0 )
        {

            this.verticalPageBreaks = [];

        }

        if ( this.verticalPageBreaks != null )
        {

            for ( i = 0; i <= verticalCount; i++ )
            {

                pts = [ new mxPoint( bounds2.x, bounds2.y + ( i + 1 ) * bounds.height ),
                           new mxPoint( right, bounds2.y + ( i + 1 ) * bounds.height ) ];

                if ( this.verticalPageBreaks[ i ] != null )
                {

                    this.verticalPageBreaks[ i ].points = pts;
                    this.verticalPageBreaks[ i ].redraw();

                } else
                {

                    pageBreak = new mxPolyline( pts, this.pageBreakColor );
                    pageBreak.dialect = this.dialect;
                    pageBreak.isDashed = this.pageBreakDashed;
                    pageBreak.pointerEvents = false;
                    pageBreak.init( this.view.backgroundPane );
                    pageBreak.redraw();

                    this.verticalPageBreaks[ i ] = pageBreak;

                }

            }

            for ( i = verticalCount; i < this.verticalPageBreaks.length; i++ )
            {

                this.verticalPageBreaks[ i ].destroy();

            }

            this.verticalPageBreaks.splice( verticalCount, this.verticalPageBreaks.length - verticalCount );

        }

    };

    /*
     * former skNavigator.js
     *
     */

    /* ⓘ info
     *
     * This basically deals with the navigation withing the diagram
     * as we are not using the default "exitGroup / enterGroup" (but "navigateTo")
     *
     */

    skGraph.prototype.navigateTo_top = function navigateTo_topF( ) {

        this.navigateTo( this.model.getRoot().children[ 0 ] );

    };

    skGraph.prototype.navigateTo_up = function navigateTo_upF( ) {

        this.navigateTo( this.model.getParent( this.getCurrentRoot() ) );

    };

    /*
     * Makes sure we have some content on the (0,0) page
     *
     */

    skGraph.prototype.resetToOrigin = function resetToOriginF( force, pageSize ) {

        var tx = 0;
        var ty = 0;

        if ( force || this.isEnabled() ) {

            var cells = this.getChildCells( this.getDefaultParent(), true, true );
            var bounds = this.getBoundingBoxFromGeometry( cells, true );

            if ( bounds && bounds.x < 0 && bounds.y < 0 ) {

                var pw = pageSize ? pageSize.width : this.getPageSize().width;
                var ph = pageSize ? pageSize.height : this.getPageSize().height;
                if ( bounds.x > 0 && bounds.x > pw ) {

                    while ( bounds.x > pw ) {

                        bounds.x -= pw;
                        tx -= pw;

                    }

                } else if ( bounds.x < 0 ) {

                    while ( bounds.x < 0 ) {

                        bounds.x += pw;
                        tx += pw;

                    }

                }

                if ( bounds.y > 0 && bounds.y > ph ) {

                    while ( bounds.y > ph ) {

                        bounds.y -= ph;
                        ty -= ph;

                    }

                } else if ( bounds.y < 0 ) {

                    while ( bounds.y < 0 ) {

                        bounds.y += ph;
                        ty += ph;

                    }

                }

                this.moveCells( cells, tx, ty );

            }

        }

        return ( new mxPoint( tx, ty ) );

        // Alternative empiric way (to tweak because it doesn't work as is)

    };

    skGraph.prototype.translateViewer = function translateViewerF( point ) {

        var viewerRectangle = new mxRectangle(
            -this.view.translate.x + this.container.scrollLeft,
            -this.view.translate.y + this.container.scrollTop,
            this.container.clientWidth,
            this.container.clientHeight );

        viewerRectangle.x += point.x;
        viewerRectangle.y += point.y;

        this.scrollRectToVisible( viewerRectangle );

    };

    mxGraphView.prototype.getVisibleArea = function() {

        return {
            scale: this.scale,
            x: this.translate.x,
            y: this.translate.y,
            left: this.graph.container.scrollLeft,
            top: this.graph.container.scrollTop

        };

    };

    mxGraphView.prototype.setVisibleArea = function( object ) {

        this.graph.container.scrollLeft = object.left;
        this.graph.container.scrollTop = object.top;
        this.scaleAndTranslate( object.scale, object.x, object.y );

    };

    skGraph.prototype.navigateToCellHandler = function( dest ) {

        // If modal open, close it
        $( "#skAttachmentsModal" ).modal( "hide" );

        // Ensure / convert dest to string so we can do the rest
        dest = "" + dest;

        // Get the cell id
        var cellID = dest.match( /\d+/ );

        var realCell = this.model.cells[ cellID ];

        if ( realCell ) {

            // Enter detail ? if the dest finished by "d"
            if ( dest[ dest.length - 1 ] === "d" ) {

                this.navigateTo( realCell );

            } else {

                this.navigateTo( realCell, true );

            }

        } else {

            $( event.target ).css( { "border-bottom":"1px dashed gray", "display":"inline" } );
            skShell.skStatus.set( "Destination cell does not exist" );

        }

    };

    // scripts that remembers where you were in the process
    var smartView = false;

    /*
     *
     * navigates INTO destinationCell unless "stayAbove" this will navigate to parent
     *
     * navSource: if this is comming from breadcrumb
     */

    skGraph.prototype.navigateTo = function navigateToF( destinationCell, stayAbove, navSource, centerOnDestination ) {

        // console.log( "skGraph.navigateTo", destinationCell ? destinationCell.id : "null", stayAbove );

        if ( typeof destinationCell === "string" ) {

            destinationCell = this.model.cells[ parseInt( destinationCell, 10 ) ];

        }

        // by default we enter into the cell
        stayAbove = stayAbove || false;

        // essentially, cannot navigate to level 0
        var departingCell = this.getCurrentRoot() ? this.getCurrentRoot() : this.getDefaultParent();

        // foolproof
        destinationCell = destinationCell || this.getSelectionCell();

        var root = this.model.getRoot();

        var originalDestinationCell = destinationCell;

        if ( destinationCell == null ) {

            return;

        }

        // if "stayAbove", go one level up but make sure we are not going at the root
        if ( stayAbove && destinationCell.id !== "1" ) {

            destinationCell = destinationCell.parent;

        }

        // Stop editing
        this.cellEditor.stopEditing( true );

        /*
         *
         * goes "up" until we have a valid root ( groups are not valid roots)
         *
         */

        // Finds the next valid root in the hierarchy
        while (
            destinationCell != root &&
            !this.isValidRoot( destinationCell ) &&
            this.model.getParent( destinationCell ) != root ) {

            destinationCell = this.model.getParent( destinationCell );

        }

        /*
         * Now we have the "defintive" destination cell
         *
         */

        /*
         * bug "counter strike"
         * for some reasons sometimes the entire process is going nuts in the canvas in "negative" territory
         * so here we make sure we are in "positive" territory
         *
         */

        this.translateViewer( this.resetToOrigin() );

        if ( departingCell != destinationCell ) {

            this.fireEvent(
                new mxEventObject(
                    "navigationStarted",
                    "destinationCell", originalDestinationCell,
                    "stayAbove", stayAbove,
                    "graph", this ) );

            /*
             * save the current view so when we drill down again it's back
             * in place
             */

            if ( smartView ) {

                this.visibleAreas[ departingCell.id ] = this.view.getVisibleArea();

            }

            /*
             *
             * change the style of the current cell (if has geometry...)
             *
             */

            if ( !VIEWER_MODE && this.isEnabled() && departingCell.geometry ) {

                // If has children, change the style
                if ( departingCell.children && departingCell.children.length > 0 ) {

                    // Change style but keeps the "custom" style if any
                    skUtils.setCellsStyleName( this.model, [ departingCell ], skConstants.whatboxdetailed );

                    departingCell.geometry.width = departingCell.geometry.width; //skConstants.WB_WIDTH;
                    departingCell.geometry.height = departingCell.geometry.height; //SkConstants.WB_HEIGHT;

                    this.model.setCollapsed( departingCell, true );

                }

                // User has drill down but no content is in it (either none has been created)
                // or content has been deleted
                else {

                    skUtils.setCellsStyleName( this.model, [ departingCell ], skConstants.whatbox );
                    this.model.setCollapsed( departingCell, true );

                }

            }

            /*
             *
             * actually navigates to the cell
             */

            this.clearSelection();

            // Clears the current root if the new root is
            // the model's root or one of the layers.
            if ( destinationCell == root || this.model.getParent( destinationCell ) == root ) {

                this.view.setCurrentRoot( null );

            } else {

                this.view.setCurrentRoot( destinationCell );

            }

            /*
             * same idea as above, just make sure everything is in "positive" territory
             */

            this.translateViewer( this.resetToOrigin() );

            /*
             *
             * Show hints on the first drilldown OR if not shown and in tutorial mode expecting this
             *
             * todo : implement by events
             */

            if ( !window.localStorage.getItem( "skDetailedViewHintHasBeenSeen" ) ||
             ( !window.localStorage.getItem( "skDetailedViewHintHasBeenSeen" ) && skShell.tutorial ) ) {

                if ( skShell.hint ) {

                    skShell.hint.hey(
                        $( ".skTitle li:last-child" ), //RefObject, // html, jquery, cell...
                        {
                            "title":"You entered a Detailed View",
                            "content": [ "To navigate back to where you were, click on the links" ]
                        },
                        "bottom", // Direction: "top" "left" null...
                        true, //ContinueButton,
                        null // Callback... not really a call back actually :s... just a function executed after
                    );

                }

                localStorage.setItem( "skDetailedViewHintHasBeenSeen", true );

                if ( skShell.tutorial ) {

                    skShell.tutorial.stepDone( skShell.tutorial.NAVIGATE );

                }

            }

            /*
             *
             * adjust the view after the navigation if there is a known position
             *
             */

            if ( smartView && navSource == skConstants.NAV_BREADCRUMB && this.visibleAreas[ destinationCell.id ] ) {

                this.view.setVisibleArea( this.visibleAreas[ destinationCell.id ] );

            }

            // Otherwise scrolls to top left
            else {

                // console.log( "no visibleRect, scroll to 0" );

                if ( !VIEWER_MODE ) {

                    this.view.setVisibleArea( {

                        scale: this.view.scale,
                        x: this.container.offsetWidth - skConstants.OFFSET_EDITOR,
                        y: this.container.offsetHeight - skConstants.OFFSET_EDITOR,
                        left: this.container.offsetWidth - skConstants.OFFSET_EDITOR,
                        top: this.container.offsetHeight - skConstants.OFFSET_EDITOR

                    } );

                }

            }

        } // End departing cell same as destination

        /*
         * 7
         *
         * highlight the cell if relevant
         */

        var rel = skUtils.findRelationship( this.getModel(), destinationCell, departingCell );

        // "1 step up" so we highlight the cell
        if ( rel == skConstants.REL_VISIBLE ) {

            this.cellHighlighter.toggleHighlightOn( this.view.getState( departingCell ) );

            this.setSelectionCell( departingCell );

            // this.setSelectionCell(originalDestinationCell);

        }

        // Maybe more than 1 step up... must find the parent
        else if ( rel == skConstants.REL_CHILD ) {

            // We know it's in the children, need to climb up to find the parent

            var tmp = departingCell;

            while ( tmp.parent != destinationCell ) {

                tmp = tmp.parent;

            }

            this.cellHighlighter.toggleHighlightOn( this.view.getState( tmp ) );
            this.setSelectionCell( tmp );

        }

        // No relation
        else {

            this.clearSelection();

            if ( stayAbove  ) {

                this.scrollCellToVisible( originalDestinationCell, centerOnDestination );

                this.cellHighlighter.toggleHighlightOn( this.view.getState( originalDestinationCell ) );
                this.setSelectionCell( originalDestinationCell );

            }

        }

        // no need to trigger this crap if we stayed in the same place

        if ( departingCell !== destinationCell ) {

            this.fireEvent(
                new mxEventObject(
                    "navigationDone",
                    "destinationCell", originalDestinationCell,
                    "stayAbove", stayAbove ) );

        }

    };

    /*
     * Helper function returns the list of all the parents
     *
     ******/

    skGraph.prototype.getParents = function getParentsF( cellAtBottom, cellAtTop ) {

        cellAtTop = cellAtTop || this.getDefaultParent();

        var parents = [];

        var tmp = cellAtBottom.parent;

        while ( tmp != cellAtTop ) {

            parents.unshift( tmp );
            tmp = tmp.parent;

        }

        return parents;

    };

    /*
     * Both functions are only necessary if editor I think
     *
     */

    /**
     * Guesses autoTranslate to avoid another repaint (see below).
     * Works if only the scale of the graph changes or if pages
     * are visible and the visible pages do not change.
     */
    var graphViewValidate = mxGraphView.prototype.validate;
    mxGraphView.prototype.validate = function validateF() {

        if ( this.graph.container != null && mxUtils.hasScrollbars( this.graph.container ) ) {

            var pad = this.graph.getPagePadding();
            var size = this.graph.getPageSize();

            // Updating scrollbars here causes flickering in quirks and is not needed
            // if zoom method is always used to set the current scale on the graph.
            // var tx = this.translate.x;
            // var ty = this.translate.y;
            this.translate.x = pad.x - ( this.x0 || 0 ) * size.width;
            this.translate.y = pad.y - ( this.y0 || 0 ) * size.height;

        }

        graphViewValidate.apply( this, arguments );

    };

    var graphSizeDidChange = mxGraph.prototype.sizeDidChange;
    mxGraph.prototype.sizeDidChange = function sizeDidChangeF() {

        if ( this.container != null && mxUtils.hasScrollbars( this.container ) ) {

            var pages = this.getPageLayout();
            var pad = this.getPagePadding();
            var size = this.getPageSize();

            // Updates the minimum graph size
            var minw = Math.ceil( 2 * pad.x + pages.width * size.width );
            var minh = Math.ceil( 2 * pad.y + pages.height * size.height );

            var min = this.minimumGraphSize;

            // LATER: Fix flicker of scrollbar size in IE quirks mode
            // after delayed call in window.resize event handler
            if ( min == null || min.width != minw || min.height != minh ) {

                this.minimumGraphSize = new mxRectangle( 0, 0, minw, minh );

            }

            // Updates auto-translate to include padding and graph size
            var dx = pad.x - pages.x * size.width;
            var dy = pad.y - pages.y * size.height;

            if ( !this.autoTranslate && ( this.view.translate.x != dx || this.view.translate.y != dy ) ) {

                this.autoTranslate = true;
                this.view.x0 = pages.x;
                this.view.y0 = pages.y;

                // NOTE: THIS INVOKES THIS METHOD AGAIN. UNFORTUNATELY THERE IS NO WAY AROUND THIS SINCE THE
                // BOUNDS ARE KNOWN AFTER THE VALIDATION AND SETTING THE TRANSLATE TRIGGERS A REVALIDATION.
                // SHOULD MOVE TRANSLATE/SCALE TO VIEW.
                var tx = this.view.translate.x;
                var ty = this.view.translate.y;

                this.view.setTranslate( dx, dy );
                this.container.scrollLeft += ( dx - tx ) * this.view.scale;
                this.container.scrollTop += ( dy - ty ) * this.view.scale;

                this.autoTranslate = false;
                return;

            }

            graphSizeDidChange.apply( this, arguments );

            // cell editor repositioning
            if ( this.cellEditor && this.cellEditor.wrapperDiv ) {

                this.cellEditor.positionEditor( this.cellEditor.editingCell );

            }

        }

    };

    // Fits the number of background pages to the graph
    mxGraphView.prototype.getBackgroundPageBounds = function getBackgroundPageBoundsF() {

        var layout = this.graph.getPageLayout();
        var page = this.graph.getPageSize();

        return new mxRectangle( this.scale * ( this.translate.x + layout.x * page.width ),
            this.scale * ( this.translate.y + layout.y * page.height ),
            this.scale * layout.width * page.width,
            this.scale * layout.height * page.height );

    };

    mxGraph.prototype.getPreferredPageSize = function getPreferredPageSizeF( /* bounds, width, height */ ) {

        var pages = this.getPageLayout();
        var size = this.getPageSize();

        return new mxRectangle( 0, 0, pages.width * size.width, pages.height * size.height );

    };

    /**
     * Returns a rectangle describing the position and count of the
     * background pages, where x and y are the position of the top,
     * left page and width and height are the vertical and horizontal
     * page count.
     */
    mxGraph.prototype.getPageLayout = function getPageLayoutF() {

        var size = ( this.pageVisible ) ? this.getPageSize() : this.scrollTileSize;
        var bounds = this.getGraphBounds();

        if ( bounds.width === 0 || bounds.height === 0 ) {

            return new mxRectangle( 0, 0, 1, 1 );

        } else {

            // Computes untransformed graph bounds
            var x = Math.ceil( bounds.x / this.view.scale - this.view.translate.x );
            var y = Math.ceil( bounds.y / this.view.scale - this.view.translate.y );
            var w = Math.floor( bounds.width / this.view.scale );
            var h = Math.floor( bounds.height / this.view.scale );

            var x0 = Math.floor( x / size.width );
            var y0 = Math.floor( y / size.height );
            var w0 = Math.ceil( ( x + w ) / size.width ) - x0;
            var h0 = Math.ceil( ( y + h ) / size.height ) - y0;

            return new mxRectangle( x0, y0, w0, h0 );

        }

    };

    /**
     * Returns the padding for pages in page view with scrollbars.
     */
    mxGraph.prototype.getPagePadding = function getPagePaddingF() {

        // Return new mxPoint(Math.max(0, Math.round((graph.container.offsetWidth - 34) / graph.view.scale)),
        // Math.max(0, Math.round((graph.container.offsetHeight - 34) / graph.view.scale)));

        if ( VIEWER_MODE ) {

            return new mxPoint( 0, 0 );

        }

        // Return new mxPoint( 50, 50 );

        return new mxPoint(
            Math.max( 0, Math.round( ( this.container.offsetWidth - skConstants.OFFSET_EDITOR ) / this.view.scale ) ),
            Math.max( 0, Math.round( ( this.container.offsetHeight - skConstants.OFFSET_EDITOR ) / this.view.scale ) ) );

    };

    /**
     * Returns the size of the page format scaled with the page size.
     */
    mxGraph.prototype.getPageSize = function getPageSizeF() {

        return ( this.pageVisible ) ? new mxRectangle( 0, 0, this.pageFormat.width * this.pageScale,
            this.pageFormat.height * this.pageScale ) : this.scrollTileSize;

    };

    mxGraph.prototype.scrollTileSize = new mxRectangle( 0, 0, 400, 400 );

    mxPanningHandler.prototype.isPanningTrigger = function isPanningTriggerF( me ) {

        var evt = me.getEvent();

        if ( this.graph.isEnabled() && ( skShell.wKeyIsPressed || skShell.yKeyIsPressed || skShell.nKeyIsPressed ) ) {

            // console.log( "w key pressed..." );

            var box = null;

            this.graph.getModel().beginUpdate();

            try {

                var style;

                // Insert whatbox
                if ( skShell.wKeyIsPressed ) {

                    style = this.graph.getStylesheet().styles[ skConstants.whatbox ];

                    box = this.graph.insertVertex(
                        this.graph.getDefaultParent(),
                        null,
                        skUtils.createBoxElement( skConstants.whatbox ),

                        // mxUtils.createXmlDocument().createElement( skConstants.whatbox ),
                        ( me.graphX / this.graph.getView().scale ) -
                            this.graph.getView().translate.x, // X
                        ( me.graphY / this.graph.getView().scale ) - this.graph.getView().translate.y, // Y
                        parseInt( style.width, 10 ), // skConstants.WB_WIDTH,
                        parseInt( style.height, 10 ), // skConstants.WB_HEIGHT,
                        skConstants.whatbox
                    );

                    box.collapsed = true;

                } else if ( skShell.yKeyIsPressed ) {

                    style = this.graph.getStylesheet().styles[ skConstants.whybox ];

                    box = this.graph.insertVertex(
                        this.graph.getDefaultParent(),
                        null,
                        skUtils.createBoxElement( skConstants.whybox ),

                        // mxUtils.createXmlDocument().createElement( skConstants.whybox ),
                        ( me.graphX / this.graph.getView().scale ) -
                            this.graph.getView().translate.x, // X
                        ( me.graphY / this.graph.getView().scale ) - this.graph.getView().translate.y, // Y
                        parseInt( style.width, 10 ), // skConstants.YB_WIDTH,
                        parseInt( style.height, 10 ), // skConstants.YB_HEIGHT,
                        skConstants.whybox
                    );

                } else if ( skShell.nKeyIsPressed ) {

                    box = this.graph.insertVertex(
                        this.graph.getDefaultParent(),
                        null,
                        skUtils.createBoxElement( skConstants.stickynote ),

                        // mxUtils.createXmlDocument().createElement( skConstants.stickynote ),
                        ( me.graphX / this.graph.getView().scale ) -
                            this.graph.getView().translate.x, // X
                        ( me.graphY / this.graph.getView().scale ) - this.graph.getView().translate.y, // Y
                        skConstants.WB_WIDTH,
                        skConstants.WB_HEIGHT,
                        skConstants.stickynote
                    );

                }

            } finally {

                // Updates the display
                this.graph.getModel().endUpdate();

            }

        }

        // Shift is done : selection!
        if ( mxEvent.isShiftDown( evt ) ) {

            return false;

        }

        if ( this.useLeftButtonForPanning && ( this.ignoreCell || me.getState() == null ) ) {

            return true;

        }

        // Configures automatic expand on mouseover
        this.graph.panningHandler.autoExpand = true;

    };

    // Adds panning for the grid with no page view and disabled scrollbars
    var mxGraphPanGraph = mxGraph.prototype.panGraph;
    mxGraph.prototype.panGraph = function panGraphF( dx, dy ) {

        mxGraphPanGraph.apply( this, arguments );

        if ( this.shiftPreview1 != null ) {

            var canvas = this.view.canvas;

            if ( canvas.ownerSVGElement != null ) {

                canvas = canvas.ownerSVGElement;

            }

            var phase = this.gridSize * this.view.scale * this.view.gridSteps;
            var position = -Math.round( phase - mxUtils.mod( this.view.translate.x * this.view.scale + dx, phase ) ) + "px " +
                -Math.round( phase - mxUtils.mod( this.view.translate.y * this.view.scale + dy, phase ) ) + "px";
            canvas.style.backgroundPosition = position;

        }

    };

    // Creates background page shape
    mxGraphView.prototype.createBackgroundPageShape = function createBackgroundPageShapeF( bounds ) {

        return new mxRectangleShape( bounds, "#ffffff", "#cccccc" );

    };

    // Returns the SVG required for painting the background grid.
    mxGraphView.prototype.createSvgGrid = function createSvgGridF( color ) {

        var tmp = this.graph.gridSize * this.scale;

        while ( tmp < this.minGridSize ) {

            tmp *= 2;

        }

        var tmp2 = this.gridSteps * tmp;

        // Small grid lines
        var d = [];

        for ( var i = 1; i < this.gridSteps; i++ ) {

            var tmp3 = i * tmp;
            d.push( "M 0 " + tmp3 + " L " + tmp2 + " " + tmp3 + " M " + tmp3 + " 0 L " + tmp3 + " " + tmp2 );

        }

        // KNOWN: Rounding errors for certain scales (eg. 144%, 121% in Chrome, FF and Safari). Workaround
        // in Chrome is to use 100% for the svg size, but this results in blurred grid for large diagrams.
        var size = tmp2;
        var svg = '<svg width="' + size + '" height="' + size + '" xmlns="' + mxConstants.NS_SVG + '">' +
            '<defs><pattern id="grid" width="' + tmp2 + '" height="' + tmp2 + '" patternUnits="userSpaceOnUse">' +
            '<path d="' + d.join( " " ) + '" fill="none" stroke="' + color + '" opacity="0.2" stroke-width="1"/>' +
            '<path d="M ' + tmp2 + " 0 L 0 0 0 " + tmp2 + '" fill="none" stroke="' + color + '" stroke-width="1"/>' +
            '</pattern></defs><rect width="100%" height="100%" fill="url(#grid)"/></svg>';

        return svg;

    };

    // Uses HTML for background pages (to support grid background image)
    mxGraphView.prototype.validateBackgroundPage = function validateBackgroundPageF() {

        var graph = this.graph;

        if ( graph.container != null && !graph.transparentBackground ) {

            if ( graph.pageVisible ) {

                var bounds = this.getBackgroundPageBounds();

                if ( this.backgroundPageShape == null ) {

                    // Finds first element in graph container
                    var firstChild = graph.container.firstChild;

                    while ( firstChild != null && firstChild.nodeType != mxConstants.NODETYPE_ELEMENT ) {

                        firstChild = firstChild.nextSibling;

                    }

                    if ( firstChild != null ) {

                        this.backgroundPageShape = this.createBackgroundPageShape( bounds );
                        this.backgroundPageShape.scale = 1;

                        // Shadow filter causes problems in outline window in quirks mode. IE8 standards
                        // also has known rendering issues inside mxWindow but not using shadow is worse.
                        this.backgroundPageShape.isShadow = !mxClient.IS_QUIRKS;
                        this.backgroundPageShape.dialect = mxConstants.DIALECT_STRICTHTML;
                        this.backgroundPageShape.init( graph.container );

                        // Required for the browser to render the background page in correct order
                        firstChild.style.position = "absolute";
                        graph.container.insertBefore( this.backgroundPageShape.node, firstChild );
                        this.backgroundPageShape.redraw();

                        this.backgroundPageShape.node.className = "geBackgroundPage";

                        // Adds listener for double click handling on background
                        mxEvent.addListener( this.backgroundPageShape.node, "dblclick",
                            mxUtils.bind( this, function( evt ) {

                                graph.dblClick( evt );

                            } )
                        );

                        // Adds basic listeners for graph event dispatching outside of the
                        // container and finishing the handling of a single gesture
                        mxEvent.addGestureListeners( this.backgroundPageShape.node,
                            mxUtils.bind( this, function( evt ) {

                                graph.fireMouseEvent( mxEvent.MOUSE_DOWN, new mxMouseEvent( evt ) );

                            } ),
                            mxUtils.bind( this, function( evt ) {

                                // Hides the tooltip if mouse is outside container
                                if ( graph.tooltipHandler != null && graph.tooltipHandler.isHideOnHover() ) {

                                    graph.tooltipHandler.hide();

                                }

                                if ( graph.isMouseDown && !mxEvent.isConsumed( evt ) ) {

                                    graph.fireMouseEvent( mxEvent.MOUSE_MOVE, new mxMouseEvent( evt ) );

                                }

                            } ),
                            mxUtils.bind( this, function( evt ) {

                                graph.fireMouseEvent( mxEvent.MOUSE_UP, new mxMouseEvent( evt ) );

                            } )
                        );

                    }

                } else {

                    this.backgroundPageShape.scale = 1;
                    this.backgroundPageShape.bounds = bounds;
                    this.backgroundPageShape.redraw();

                }

            } else if ( this.backgroundPageShape != null ) {

                this.backgroundPageShape.destroy();
                this.backgroundPageShape = null;

            }

            this.validateBackgroundStyles();

        }

    };

    // Defines grid properties
    // jscs:disable maximumLineLength
    mxGraphView.prototype.gridImage = ( mxClient.IS_SVG ) ? "data:image/gif;base64,R0lGODlhCgAKAJEAAAAAAP///8zMzP///yH5BAEAAAMALAAAAAAKAAoAAAIJ1I6py+0Po2wFADs=" :
        require( "IMAGE_PATH" ) + "/grid.gif";

    // jscs:enable maximumLineLength

    mxGraphView.prototype.gridSteps = 4;
    mxGraphView.prototype.gridColor = "#e0e0e0";
    mxGraphView.prototype.minGridSize = 4;

    // Updates the CSS of the background to draw the grid
    mxGraphView.prototype.validateBackgroundStyles = function validateBackgroundStylesF() {

        var graph = this.graph;
        var color = ( graph.background == null || graph.background == mxConstants.NONE ) ? "#ffffff" : graph.background;
        var gridColor = ( this.gridColor != color.toLowerCase() ) ? this.gridColor : "#ffffff";
        var image = "none";
        var position = "";

        if ( graph.isGridEnabled() ) {

            var phase = 10;

            if ( mxClient.IS_SVG ) {

                // Generates the SVG required for drawing the dynamic grid
                image = window.unescape( encodeURIComponent( this.createSvgGrid( gridColor ) ) );

                // Image = ( window.btoa ) ? btoa( image ) : Base64.encode( image, true );
                image = btoa( image );
                image = "url(" + "data:image/svg+xml;base64," + image + ")";
                phase = graph.gridSize * this.scale * this.gridSteps;

            } else {

                // Fallback to grid wallpaper with fixed size
                image = "url(" + this.gridImage + ")";

            }

            var x0 = 0;
            var y0 = 0;

            if ( graph.view.backgroundPageShape != null ) {

                var bds = this.getBackgroundPageBounds();

                x0 = 1 + bds.x;
                y0 = 1 + bds.y;

            }

            // Computes the offset to maintain origin for grid
            position = -Math.round( phase - mxUtils.mod( this.translate.x * this.scale - x0, phase ) ) + "px " +
                -Math.round( phase - mxUtils.mod( this.translate.y * this.scale - y0, phase ) ) + "px";

        }

        var canvas = graph.view.canvas;

        if ( canvas.ownerSVGElement != null ) {

            canvas = canvas.ownerSVGElement;

        }

        if ( graph.view.backgroundPageShape != null ) {

            graph.view.backgroundPageShape.node.style.backgroundPosition = position;
            graph.view.backgroundPageShape.node.style.backgroundImage = image;
            graph.view.backgroundPageShape.node.style.backgroundColor = color;

            canvas.style.backgroundImage = "none";
            canvas.style.backgroundColor = "";

        } else {

            canvas.style.backgroundPosition = position;
            canvas.style.backgroundColor = color;
            canvas.style.backgroundImage = image;

        }

    };

    skGraph.prototype.defaultGraphBackground = "#ffffff";

    skGraph.prototype.updateGraphComponents = function updateGraphComponentsF() {

        if ( this.container != null ) {

            this.view.validateBackground();
            this.container.style.overflow = ( this.scrollbars ) ? "auto" : "hidden";

        }

    };

    var oInit = skGraph.prototype.init;
    skGraph.prototype.init = function initF( container ) {

        oInit.call( this, container );

        // Enables scrollbars and sets cursor style for the container
        this.container.setAttribute( "tabindex", "0" );
        this.container.style.overflow = ( mxClient.IS_TOUCH ) ? "hidden" : "auto";
        this.container.style.cursor = "default";

        this.panningHandler.useLeftButtonForPanning = true;

        // Shows hand cursor while panning
        this.panningHandler.addListener(
            mxEvent.PAN_START,
            mxUtils.bind( this, function() {

                //Noinspection JSPotentiallyInvalidUsageOfThis
                this.container.style.cursor = "pointer";

            } )
        );

        this.panningHandler.addListener(
            mxEvent.PAN_END,
            mxUtils.bind( this, function() {

                // Hide the info popup
                if ( skShell.hint ) {

                    skShell.hint.hide();

                }

                /*
                 * Displays a dontshowagain type of message for pan or select
                 *
                 */

                if ( this.isEnabled() ) {

                    /*
                     * FOR CONVERSTION
                     *
                     * make that a toast
                     *
                     *
                     */

                    skShell.skStatus.setAndDontShowAgain(
                        "For <b>selection square</b>,<br>press <kbd>Shift <span>⇧</span></kbd> at the same time",
                        "DONTSHOWAGAINpanOrSelect" );

                }

            } ) );

        /*
         * Reset line "waypoints" by double clicking
         *
         */

        this.addListener( mxEvent.DOUBLE_CLICK, mxUtils.bind( this, function( sender, evt ) {

            var cell = evt.getProperty( "cell" );

            if ( this.isEnabled() && cell && cell.isEdge() ) {

                this.clearWaypoints( [ cell ] );

            }

            evt.consume();

        } ) );

    };
    skGraph.prototype.initialTopSpacing = 0;
    skGraph.prototype.resetScrollbars = function resetScrollbarsF() {

        // console.log( "skNavigatorUI.resetScrollbars" );

        var bounds;

        if ( mxUtils.hasScrollbars( this.container ) ) {

            if ( this.pageVisible ) {

                var pad = this.getPagePadding();
                this.container.scrollTop = Math.floor( pad.y - this.initialTopSpacing );
                this.container.scrollLeft = Math.floor( Math.min( pad.x, ( this.container.scrollWidth - this.container.clientWidth ) / 2 ) );

            } else {

                bounds = this.getGraphBounds();
                var width = Math.max( bounds.width, this.scrollTileSize.width * this.view.scale );
                var height = Math.max( bounds.height, this.scrollTileSize.height * this.view.scale );
                this.container.scrollTop = Math.floor( Math.max( 0, bounds.y - Math.max( 20, ( this.container.clientHeight - height ) / 4 ) ) );
                this.container.scrollLeft = Math.floor( Math.max( 0, bounds.x - Math.max( 0, ( this.container.clientWidth - width ) / 2 ) ) );

            }

        } else {

            // This code is not actively used since the default for scrollbars is always true
            if ( this.pageVisible ) {

                var b = this.view.getBackgroundPageBounds();
                this.view.setTranslate( Math.floor( Math.max( 0, ( this.container.clientWidth - b.width ) / 2 ) - b.x ),
                    Math.floor( Math.max( 0, ( this.container.clientHeight - b.height ) / 2 ) - b.y ) );

            } else {

                bounds = this.getGraphBounds();
                this.view.setTranslate( Math.floor( Math.max( 0, Math.max( 0, ( this.container.clientWidth - bounds.width ) / 2 ) - bounds.x ) ),
                    Math.floor( Math.max( 0, Math.max( 20, ( this.container.clientHeight - bounds.height ) / 4 ) ) - bounds.y ) );

            }

        }

    };

    skGraph.prototype.setBackgroundColor = function setBackgroundColorF( value ) {

        this.background = value;
        this.fireEvent( new mxEventObject( "graphBackgroundChanged", "graph", this ) );

    };

    return skGraph;

} );
