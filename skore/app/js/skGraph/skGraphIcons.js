
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCellRenderer = require( "mxCellRenderer" );
    var mxConstants = require( "mxConstants" );
    var mxEventObject = require( "mxEventObject" );
    var mxGraph = require( "mxGraph" );
    var mxGraphView = require( "mxGraphView" );
    var mxRectangle = require( "mxRectangle" );
    var mxText = require( "mxText" );
    var skConstants = require( "skConstants" );

    mxGraph.prototype.getIcon = {};

    mxGraph.prototype.getIcon.enterDetail = function getIconEnterDetailF( cell, force ) {

        if (
            cell &&
            cell.isWhatbox() &&
            ( ( force && this.isEnabled() ) || cell.children && cell.children.length ) ) {

            var style = " style='";

            /*
             * Icon gray if no children
             *
             */

             if ( !( cell.children && cell.children.length ) ) {

                style += " color:#808080; ";

             }

            /*
             * Style color if necessary
             *
             */

             else if ( this.getCellStyle( cell )[ skConstants.DETAIL_COLOR ] ) {

                style += " color:" + this.getCellStyle( cell )[ skConstants.DETAIL_COLOR ] + "; ";

             }

            /*
             * Background "hack" if necessary
             *
             */

             var fillColor = this.getCellStyle( cell )[ mxConstants.STYLE_FILLCOLOR ];

             if ( fillColor && fillColor !== "transparent" ) {

                style += "" +
                "background: #ffffff;" +
                "background: -moz-linear-gradient   (top,       " +
                "rgba(255,255,255,0) 0%, rgba(255,255,255,0) 9%, #ffffff 10%, #ffffff 80%, " + fillColor + " 81%, " + fillColor + " 100%);" +
                "background: -webkit-linear-gradient(top,       " +
                "rgba(255,255,255,0) 0%, rgba(255,255,255,0) 9%, #ffffff 10%, #ffffff 80%, " + fillColor + " 81%, " + fillColor + " 100%);" +
                "background: linear-gradient        (to bottom, " +
                "rgba(255,255,255,0) 0%, rgba(255,255,255,0) 9%, #ffffff 10%, #ffffff 80%, " + fillColor + " 81%, " + fillColor + " 100%);";

             }

             style += "'";

             var toi = "<div class='actionIconWrapper'>" +
             "<i " + style + ' class="fa fa-toggle-down actionIcon skBoxActionIcon" ' +
             " >" +
             "</i>" +
             "</div>";

             return toi;

         }

         return null;

    };

    mxGraph.prototype.getIcon.attachment = function getIconAttachmentF( cell, force ) {

        if (
            cell &&
            cell.isWhatbox() &&
            ( ( force && this.isEnabled() ) || cell.numberOfAttachments() ) ) {

            var style = "";

            if ( this.getCellStyle( cell )[ skConstants.ATT_COLOR ] ) {

                style = ' style="color:' + this.getCellStyle( cell )[ skConstants.ATT_COLOR ] + '" ';

            }
            if ( !cell.numberOfAttachments() ) {

                style = ' style="color:#808080" ';

            }

            var toi = "<div " + style + ">" +
            '<i class="fa fa-paperclip actionIconWrapper actionIcon skBoxActionIcon" ' +
            "></i></div>";

            return toi;

        }

    };

    // mxGraph.prototype.getIcon.surveys = function getIconSurveysF( cell, force ) {

    //     if (
    //         cell &&
    //         cell.isWhatbox() &&
    //         ( ( force && this.isEnabled() ) || cell.numberOfSurveys() ) ) {

    //         var style = "";

    //         if ( this.getCellStyle( cell )[ skConstants.SURVEY_COLOR ] ) {

    //             style = ' style="color:' + this.getCellStyle( cell )[ skConstants.SURVEY_COLOR ] + '" ';

    //         }
    //         if ( !cell.numberOfSurveys() ) {

    //             style = ' style="color:#808080" ';

    //         }

    //         var toi = "<div " + style + ">" +
    //         '<i class="fa fa-file-text-o actionIconWrapper actionIcon skBoxActionIcon" ' +
    //         "></i></div>";

    //         return toi;

    //     }

    // };

    mxGraph.prototype.getIcon.remove = function getIconRemoveF( cell ) {

        if ( this.isEnabled() && cell && this.getSelectionCell() === cell && this.currentCell === cell && !cell.isGroup() ) {

            var toi  = "<div>" +
                '<i class="fa fa-trash actionIconWrapper actionIcon actionIconRemove skBoxActionIcon"></i></div>';

            return toi;

        }

    };

    mxGraph.prototype.getIcon.boxnumber = function getIconBoxNumberF( cell ) {

        if ( this.isEnabled() && this.showBadgeIcon && cell && !cell.isEdge() ) {

            return "<div class='badge actionIconBadge'>" + cell.id + "</div>";

        }

    };

    mxGraphView.prototype.createStateIcon = function createStateIconF( icon, state, force ) {

        var currentIcon = this.graph.getIcon[ icon ].call( this.graph, state.cell, force );

        state.icon = state.icon || {};
        if ( state.shape != null && !state.icon[ icon ] && currentIcon != null ) {

            state.icon[ icon ] = new mxText(
                currentIcon,
                new mxRectangle(),
                mxConstants.ALIGN_LEFT,
                mxConstants.ALIGN_TOP );

            // Styles the label
            state.icon[ icon ].dialect = state.shape.dialect;
            state.icon[ icon ].dialect = mxConstants.DIALECT_STRICTHTML;

            // State.icon[icon].wrap = true;
            this.graph.cellRenderer.initializeLabel( state, state.icon[ icon ] );

        }

    };

    // Creates the shape for the attachment / enter details
    var oCreateState = mxGraphView.prototype.createState;

    mxGraphView.prototype.createState = function createStateF( cell, force ) {

        var state = this.getState( cell ) || oCreateState.apply( this, arguments );

        if ( state.cell.geometry != null && !state.cell.geometry.relative ) {

            state.icon = state.icon || {};

            this.createStateIcon( "enterDetail", state, force );

            this.createStateIcon( "attachment", state, force );

            // this.createStateIcon( "surveys", state, force );

            this.createStateIcon( "remove", state, force );

            this.createStateIcon( "boxnumber", state, force );

        }

        return state;

    };

    mxCellRenderer.prototype.redrawIcon = function redrawIconF( icon, state, offset, force ) {

        if ( state.shape != null && state.icon[ icon ] ) {

            var graph = state.view.graph;

            var scale = state.view.getScale();

            var bounds = new mxRectangle( state.x + offset.x, state.y + offset.y, 35, 0 );
            state.icon[ icon ].state = state;
            state.icon[ icon ].value = graph.getIcon[ icon ].call( graph, state.cell, force );
            state.icon[ icon ].scale = scale;
            state.icon[ icon ].bounds = bounds;
            state.icon[ icon ].redraw();

            var dismiss = function() {

                if ( $( event.target ).closest( ".popover" ).length === 0 ) {

                    $( ".popover" ).popover( "hide" );

                }

            };

            $( ".skBoxActionIcon", state.icon[ icon ].node ).parent().off( "click touchend" );

            $( ".skBoxActionIcon", state.icon[ icon ].node ).parent().on( "click touchend", function() {

                if ( icon == "enterDetail" ) {

                    if (

                        // If children (go direct)
                        ( state.cell.children && state.cell.children.length > 0 ) ||

                        // If popover already shown (consider user is ok)
                        $( this ).data( "bs.popover" ) ||

                        // or if user decided not to show message again
                        ( localStorage.hasOwnProperty( "detailedViewPopover" ) &&
                             parseInt( localStorage.getItem( "detailedViewPopover" ), 10 ) === 1 ) ) {

                        state.view.graph.navigateTo( state.cell );

                    } else {

                        console.log( "will show popover" );

                        $( this ).popover( {
                            content: function() {

                                var popoverTrigger = $( this );

                                var buttons = $( "" +
                                    "<div>" +
                                        "<div>" +
                                            '<button style="width:100%; padding-bottom:5px;" class="popBtn popOK btn btn-primary btn-sm">' +
                                                "Create detailed view" +
                                            "</button>" +
                                        "</div>" +
                                        "<div>" +
                                            '<button style="width:100%" class="popBtn popCancel btn btn-default btn-sm">Ignore</button>' +
                                        "</div>" +
                                        '<div class="checkbox">' +
                                            "<label>" +
                                                '<input class="popBtn" type="checkbox" id="detailedViewPopover" >' +
                                                "Don't ask me again" +
                                              "</label>" +
                                        "</div>" +
                                    "</div>" +
                                    "" );

                                $( ".popBtn", buttons ).on( "click", function() {

                                    popoverTrigger.popover( "destroy" );
                                    dismiss();

                                } );

                                $( ".popOK", buttons ).on( "click", function() {

                                    state.view.graph.navigateTo( state.cell );

                                } );

                                $( "#detailedViewPopover", buttons ).on( "change", function() {

                                    localStorage.setItem( "detailedViewPopover", 1 );

                                } );

                                return buttons;

                            },
                            container:"body",
                            placement: "auto top",
                            html: "true"
                        } ).on( "hidden.bs.popover", function() {

                            $( ".skDiagramContainer" ).off( "click", dismiss );

                        } ).on( "shown.bs.popover", function() {

                            $( ".skDiagramContainer" ).one( "click", dismiss );

                        } );

                        $( this ).popover( "show" );

                    }

                }

                if ( icon == "remove" ) {

                    if (

                        // If popover already shown (consider user is ok)
                        $( this ).data( "bs.popover" ) ||

                        // or if user decided not to show message again
                        ( localStorage.hasOwnProperty( "removeViewPopover" ) &&
                             parseInt( localStorage.getItem( "removeViewPopover" ), 10 ) === 1 ) ) {

                        graph.removeCells( [ state.cell ] );

                    } else {

                        $( this ).popover( {

                            content: function() {

                                var popoverTrigger = $( this );

                                var buttons = $( "" +
                                    "<div>" +
                                        "<div>" +
                                            '<button style="width:100%; padding-bottom:5px;" class="popBtn popOK btn btn-primary btn-sm">' +
                                                "Remove box" +
                                            "</button>" +
                                        "</div>" +
                                        "<div>" +
                                            '<button style="width:100%" class="popBtn popCancel btn btn-default btn-sm">Ignore</button>' +
                                        "</div>" +
                                        '<div class="checkbox">' +
                                            "<label>" +
                                                '<input class="popBtn" type="checkbox" id="removeViewPopover" >' +
                                                "Don't ask me again" +
                                              "</label>" +
                                        "</div>" +
                                    "</div>" +
                                    "" );

                                $( ".popBtn", buttons ).on( "click", function() {

                                    popoverTrigger.popover( "destroy" );
                                    dismiss();

                                } );

                                $( ".popOK", buttons ).on( "click", function() {

                                    graph.removeCells( [ state.cell ] );

                                } );

                                $( "#removeViewPopover", buttons ).on( "change", function() {

                                    localStorage.setItem( "removeViewPopover", 1 );

                                } );

                                return buttons;

                            },
                            container:"body",
                            placement: "auto bottom",
                            html: "true"
                        } ).on( "hidden.bs.popover", function() {

                            $( ".skDiagramContainer" ).off( "click", dismiss );

                        } ).on( "shown.bs.popover", function() {

                            $( ".skDiagramContainer" ).one( "click", dismiss );

                        } );

                        $( this ).popover( "show" );

                    }

                }

                if ( icon == "attachment" ) {

                    graph.fireEvent( new mxEventObject( "openAttachment",
                        "cell", state.cell,
                        "forceOpen", true,
                        "atttype", "attachments" ) );

                }

                // if ( icon == "surveys" ) {

                //     console.log( "surveys!" );

                // }

            } );

        }

    };

    // Redraws the shape number after the cell has been moved/resized
    var oRedraw = mxCellRenderer.prototype.redraw;

    mxCellRenderer.prototype.redraw = function redrawF( state, force ) {

        if ( state ) {

            oRedraw.apply( this, arguments );

            state.icon = state.icon || {};

            var scale = state.view.getScale();

            this.redrawIcon( "enterDetail", state, { x:0, y: -18 * scale }, force );

            this.redrawIcon( "attachment", state, { x: state.width - 40 * scale, y: -18 * scale }, force );

            // this.redrawIcon( "surveys", state, { x: state.width - 60 * scale, y: -18 * scale }, force );

            this.redrawIcon( "remove", state, { x:0, y: state.height - 18 * scale }, force );

            this.redrawIcon( "boxnumber", state, { x: 0, y: state.height - 19 * scale }, force );

        }

    };

    // Destroys the shape number
    var mxCellRendererprototypedestroy = mxCellRenderer.prototype.destroy;

    mxCellRenderer.prototype.destroy = function destroyF( state ) {

        mxCellRendererprototypedestroy.apply( this, arguments );

        this.destroyIcons( state );

    };

    mxCellRenderer.prototype.destroyIcons = function destroyIconsF( state ) {

        this.destroyIcon( "enterDetail", state );

        this.destroyIcon( "attachment", state );

        // this.destroyIcon( "surveys", state );

        this.destroyIcon( "remove", state );

        this.destroyIcon( "boxnumber", state );

    };

    mxCellRenderer.prototype.destroyIconsIfNecessary = function destroyIconsIfNecessaryF( state ) {

        if ( state ) {

            if ( state.icon.remove ) {

                this.destroyIcon( "remove", state );

            }

            if ( !state.view.graph.showBadgeIcon && state.icon.boxnumber ) {

                this.destroyIcon( "boxnumber", state );

            }

            if ( state.cell.numberOfAttachments() === 0 ) {

                this.destroyIcon( "attachment", state );

            }

            // if ( state.cell.numberOfSurveys() === 0 ) {

            //     this.destroyIcon( "surveys", state );

            // }

            if ( !state.cell.isWhatboxDetailed() ) {

                this.destroyIcon( "enterDetail", state );

            }

        }

    };

    mxCellRenderer.prototype.destroyIcon = function destroyIconF( icon, state ) {

        if ( state && state.icon && state.icon[ icon ] != null ) {

            state.icon[ icon ].destroy();

            state.icon[ icon ] = null;

        }

    };

    mxCellRenderer.prototype.getShapesForState = function getShapesForStateF( state ) {

        console.log( "mxCellRenderer.getShapesForState" );

        var array = [ state.shape, state.text, state.control ];

        if ( state.icon ) {

            array.push( state.icon.attachment, /* state.icon.surveys, */ state.icon.boxnumber, state.icon.remove, state.icon.enterDetail );

        }

        return array;

    };

} );
