//jshint -W098
define( function( require ) {

    "use strict";

    var mxEdgeSegmentHandler = require( "mxEdgeSegmentHandler" );
    var mxUtils = require( "mxUtils" );

    /**
     * @constructor
     */
    function skEdgeSegmentHandler( state ) {

        /*jshint validthis:true*/
        mxEdgeSegmentHandler.call( this, state );

    }

    mxUtils.extend( skEdgeSegmentHandler, mxEdgeSegmentHandler );

    /**
     * Overriden to clear waypoints
     */
    skEdgeSegmentHandler.prototype.connect = function connectF( edge, terminal, isSource, isClone, me ) {

        var model = this.graph.getModel();

        model.beginUpdate();

        try {

            edge = mxEdgeSegmentHandler.prototype.connect.apply( this, arguments );
            var geo = model.getGeometry( edge );

            if ( geo != null ) {

                geo = geo.clone();
                geo.points = null;

                model.setGeometry( edge, geo );

            }

        }

 finally {

            model.endUpdate();

        }

        return edge;

    };

    return skEdgeSegmentHandler;

} );
