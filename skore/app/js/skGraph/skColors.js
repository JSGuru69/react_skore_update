define( function() {

    "use strict";

    return {

        line: "#067C7E", //"#822563",

        box: "#15518e",
        BOX_VALID: "#00FF00", // Green
        BOX_HOVER: "orange",
        BOX_SELECTED: "#FF00FF", // Pink-ish
        BOX_HIGHLIGHT: "#FFEE88", // Yellow ish

        BOX_GROUP: "#C0C0C0",

        GUIDE: "#e54a4b",
        OUTLINE_FRAME: "#e54a4b",
        PAGE_BREAK: "#C0C0C0",

        HANDOVER: "Purple"

    };

} );
