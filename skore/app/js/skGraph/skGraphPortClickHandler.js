define( function( require ) {

    "use strict";
    var mxEvent = require( "mxEvent" );

    /**
     * @constructor
     */
    function skGraphPortClickHandler( graph ) {

        /*jshint validthis:true*/
        graph.addListener( mxEvent.FIRE_MOUSE_EVENT, this.onMouseEvent );
        graph.addMouseListener( this );

    }

    skGraphPortClickHandler.prototype.onMouseEvent = function onMouseEventF( sender, evt ) {

        // Inside the function `this` refers to a graph
        var evtName = evt.getProperty( "eventName" );

        if ( evtName == mxEvent.MOUSE_UP ) {

            var constraintHandler = this.connectionHandler.constraintHandler;
            var constraint = constraintHandler.currentConstraint;
            var focus = constraintHandler.currentFocus;

            if ( constraint != null && focus != null ) {

                console.log( "skGraphPortClickHandler" );

                var me = evt.getProperty( "event" );

                if ( me.state && me.state.view.graph.getSelectionCell() !== me.state.cell ) {

                    me.ignoreEvent = true;

                } else {

                    me.currentConstraint = constraint;
                    me.currentFocus = focus;

                }

            }

        }

    };

    skGraphPortClickHandler.prototype.mouseDown = function mouseDownF() {

    };

    skGraphPortClickHandler.prototype.mouseMove = function mouseMoveF() {
    };

    skGraphPortClickHandler.prototype.mouseUp = function mouseUpF( sender, me ) {

        console.log( "mouse up" );
        if ( !me.isConsumed() ) {

            if ( me.ignoreEvent ) {

                mxEvent.consume( me.evt );

            } else {

                var constraint = me.currentConstraint;
                var focus = me.currentFocus;

                if ( constraint != null && focus != null ) {

                    // Event was not consumed by neither by connection handler no by edge handler
                    // our turn to create a box on a click
                    this.createBox( constraint, focus );

                    mxEvent.consume( me.evt );

                }

            }

        }

    };

    skGraphPortClickHandler.prototype.createBox = function createBoxF( constraint, state ) {

        // Right
        if ( constraint.point.x == 1 ) {

            state.add_box_after_box_state( null, "right" );

        }

        // Left

        else if ( constraint.point.x === 0 ) {

            state.add_box_after_box_state( null, "left" );

        }

        // Top

        else if ( constraint.point.y === 0 ) {

            state.add_box_after_box_state( null, "top" );

        }

        // Bottom

        else if ( constraint.point.y === 1 ) {

            state.add_box_after_box_state( null, "bottom" );

        }

    };

    return skGraphPortClickHandler;

} );
