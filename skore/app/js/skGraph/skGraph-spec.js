define( [ "Injector", "test/env" ], function( Injector, env ) {

    "use strict";

    var injector = new Injector();

    describe( "skGraph module", function() {

        beforeEach( injector.reset );

        it( "redefines mxConstants", function( done ) {

            var unchanged = {};
            var propsToChange = {
                DEFAULT_FONTFAMILY: true,
                DEFAULT_FONTSIZE: true,
                DEFAULT_VALID_COLOR: true,
                VERTEX_SELECTION_COLOR: true,
                VERTEX_SELECTION_STROKEWIDTH: true,
                EDGE_SELECTION_STROKEWIDTH: true,
                VERTEX_SELECTION_DASHED: true,
                HANDLE_SIZE: true,
                EDGE_SELECTION_COLOR: true,
                EDGE_SELECTION_DASHED: true,
                GUIDE_COLOR: true,
                GUIDE_STROKEWIDTH: true,
                OUTLINE_COLOR: true
            };

            injector.require( [ "mxConstants" ], function( mxConstants ) {

                for ( var p in mxConstants ) {

                    if ( mxConstants.hasOwnProperty( p ) ) {

                        mxConstants[ p ] = unchanged;

                    }

                }
                injector.require( [ "skGraph/skGraph" ], function() {

                    for ( var p in mxConstants ) {

                        if ( mxConstants.hasOwnProperty( p ) ) {

                            var hasChanged = mxConstants[ p ] !== unchanged;

                            if ( p in propsToChange ) {

                                if ( !hasChanged ) {

                                    expect( p ).toBe( "updated" );

                                }

                            } else {

                                if ( hasChanged ) {

                                    expect( p ).toBe( "not updated" );

                                }

                            }

                        }

                    }
                    done();

                } );

            } );

        } );

        it( "defines wBoxDetailed class", function( done ) {

            injector.require( [ "mxCellRenderer" ], function( mxCellRenderer ) {

                expect( mxCellRenderer.prototype.defaultShapes[ "whatboxdetailed" ] ).toBeUndefined();

                injector.require( [ "mxShape", "skGraph/skGraph" ], function( mxShape ) {

                    var wBoxDetailed = mxCellRenderer.prototype.defaultShapes[ "whatboxdetailed" ];

                    expect( wBoxDetailed ).toBeDefined();
                    expect( wBoxDetailed.prototype instanceof mxShape ).toBe( true );

                    expect( wBoxDetailed.prototype.hasOwnProperty( "paintBackground" ) ).toBe( true );

                    // Todo: test wBoxDetailed.prototype.paintBackground

                    done();

                } );

            } );

        } );

        it( "modifies mxCellRenderer class", function( done ) {

            injector.require( [ "mxCellRenderer" ], function( mxCellRenderer ) {

                var getControlBounds = mxCellRenderer.prototype.getControlBounds;
                var getShapesForState = mxCellRenderer.prototype.getShapesForState;

                injector.require( [ "skGraph/skGraph" ], function() {

                    expect( mxCellRenderer.prototype.getControlBounds ).not.toBe( getControlBounds );
                    expect( mxCellRenderer.prototype.getShapesForState ).not.toBe( getShapesForState );
                    done();

                } );

            } );

        } );

        // Todo: test mxCellRenderer.prototype.getShapesForState

        it( "registers `whybox` stencil", function( done ) {

            injector.require( [ "mxStencilRegistry" ], function( mxStencilRegistry ) {

                expect( mxStencilRegistry.getStencil( "whybox" ) ).toBeUndefined();
                injector.require( [ "skGraph/skGraph" ], function() {

                    expect( mxStencilRegistry.getStencil( "whybox" ) ).toBeDefined();
                    done();

                } );

            } );

        } );

    } );

    describe( "skGraph", function() {

        beforeAll( injector.reset );

        var skGraph;
        var skStylesheet;

        beforeAll( injector.run(
            function( _skGraph /*skGraph/skGraph*/,
                      _skStylesheet /*skGraph/skStylesheet*/) {

                skGraph = _skGraph;
                skStylesheet = _skStylesheet;

            }
        ) );

        it( "creates skStylesheet", function() {

            var graph = new skGraph();

            expect( graph.getStylesheet() ).toEqual( jasmine.any( skStylesheet ) );

        } );

    } );

    describe( "skGraph (integration)", function() {

        beforeEach( injector.reset );

        // Create an environment that imitates the real thing
        env.setupEmployeeOnboarding( injector );

        var $;
        var skShell;
        var graph;

        beforeEach( injector.run(
            function( _$ /*jquery*/,
                      _skShell /*skShell*/) {

                $ = _$;
                skShell = _skShell;
                graph = skShell.graph;

            }
        ) );

        function getWhatboxDetailedCell() {

            var cell = graph.getModel().getCell( 231 );

            // Sanity check that correct cell is found
            expect( cell.isWhatboxDetailed() ).toBeTruthy();
            expect( cell.getBoxText() ).toBe( "Prepare new employee arrival" );

            return cell;

        }

        function getGroupCell() {

            var cell = graph.getModel().getCell( 397 );

            // Sanity check that correct cell is found
            expect( cell.isGroup() ).toBeTruthy();
            expect( cell.getBoxText() ).toBe( "Repeat every 6 months" );

            return cell;

        }

        function getGroupParentCell() {

            var cell = graph.getModel().getCell( 397 ).getParent();

            // Sanity check that correct cell is found
            expect( cell.isWhatboxDetailed() ).toBeTruthy();
            expect( cell.getBoxText() ).toBe( "Follow up employee onboarding during the next 100 days" );

            return cell;

        }

        describe( "skGraph.navigateTo", function() {

            var mxEvent;
            var mxMouseEvent;
            var mxRectangle;

            beforeEach( injector.run(
                function( _mxEvent /*mxEvent*/,
                          _mxMouseEvent /*mxMouseEvent*/,
                          _mxRectangle /*mxRectangle*/) {

                    mxEvent = _mxEvent;
                    mxMouseEvent = _mxMouseEvent;
                    mxRectangle = _mxRectangle;

                }
            ) );

            it( "should navigate to the selected cell", function() {

                // Todo: this test fails because graph.getLabel makes a resize call
                // it seems it is unsafe to do a resize during getLabel because it conflicts with an internal state
                graph.setSelectionCell( getWhatboxDetailedCell() );
                graph.navigateTo();
                expect( graph.getCurrentRoot() ).toBe( getWhatboxDetailedCell() );

            } );

            it( "should go `up` until we have a valid root (basically groups are not valid roots)", function() {

                graph.navigateTo( getGroupCell() );
                expect( graph.getCurrentRoot() ).toBe( getGroupParentCell() );

            } );

            it( "should clear search", function() {

                spyOn( skShell.search, "unhighlight" );
                graph.navigateTo( getWhatboxDetailedCell() );
                expect( skShell.search.unhighlight ).toHaveBeenCalled();

            } );

            it( "should save current view", function() {

                var cell = graph.getDefaultParent();

                delete cell.visibleRect;
                graph.navigateTo( getWhatboxDetailedCell() );
                expect( cell.visibleRect ).toBeTruthy();

            } );

            it( "should remove attachment panel", function() {

                // First create an attachment panel by moving mouse over a cell
                var event;
                var cell = getWhatboxDetailedCell();
                var state = graph.getView().getState( cell );

                expect( state ).toBeTruthy();
                expect( skShell.cellActionButtons ).toBeFalsy();
                event = $.Event( "mousemove", { target: graph.container } );
                graph.fireMouseEvent( mxEvent.MOUSE_MOVE, new mxMouseEvent( event, state ) );

                // Attachment icon should appear
                expect( skShell.cellActionButtons ).toBeTruthy();
                expect( skShell.cellActionButtons.icon_attachment ).toBeTruthy();

                // Then click on attachment icon and panel should appear
                expect( graph.attachmentsPanel ).toBeFalsy();
                $( skShell.cellActionButtons.icon_attachment ).simulate( "mouseup" );
                expect( graph.attachmentsPanel ).toBeTruthy();

                // Finally test
                spyOn( graph.attachmentsPanel, "destroy" );
                graph.navigateTo( getWhatboxDetailedCell() );
                expect( graph.attachmentsPanel.destroy ).toHaveBeenCalled();

            } );

            it( "should navigate to the cell", function() {

                var cell = getWhatboxDetailedCell();

                graph.navigateTo( cell );
                expect( graph.getCurrentRoot() ).toBe( cell );

            } );

            it( "should adjust the view after the navigation", function() {

                var cell = getWhatboxDetailedCell();

                spyOn( graph, "scrollRectToVisible" );
                graph.navigateTo( cell );
                expect( graph.scrollRectToVisible ).toHaveBeenCalledWith( jasmine.any( mxRectangle ) );
                cell.visibleRect = new mxRectangle( 0, 0, 100, 100 );
                graph.navigateTo( cell );
                expect( graph.scrollRectToVisible ).toHaveBeenCalledWith( cell.visibleRect );

            } );

            it( "should update the wings", function() {

                var cell = getWhatboxDetailedCell();

                graph.navigateTo( cell );

                // It seems that wings appear only after navigation
                var $leftYBList = $( ".skWingLeft" ).find( ".wingYBList" );
                var $rightYBList = $( ".skWingRight" ).find( ".wingYBList" );

                expect( $leftYBList ).not.toHaveCss( { visibility: "hidden" } );
                expect( $leftYBList.find( ".cs_whybox" ) ).toHaveLength( 1 );
                expect( $leftYBList ).toContainText( "job offer accepted by candidate" );

                expect( $rightYBList ).not.toHaveCss( { visibility: "hidden" } );
                expect( $rightYBList.find( ".cs_whybox" ) ).toHaveLength( 1 );
                expect( $rightYBList ).toContainText( "administrative, IT and onboarding plan ready" );

                // Navigate back to top
                graph.navigateTo( cell.getParent() );
                expect( $leftYBList ).toHaveCss( { visibility: "hidden" } );
                expect( $leftYBList.find( ".cs_whybox" ) ).not.toExist();
                expect( $rightYBList ).toHaveCss( { visibility: "hidden" } );
                expect( $rightYBList.find( ".cs_whybox" ) ).not.toExist();

            } );

            it( "should not update the wings if graph is disabled", function() {

                var cell = getWhatboxDetailedCell();

                graph.setEnabled( false );
                graph.navigateTo( cell );

                // It seems that wings appear only after navigation
                var $leftYBList = $( "#skWingLeft" ).find( ".wingYBList" );
                var $rightYBList = $( "#skWingRight" ).find( ".wingYBList" );

                expect( $leftYBList ).not.toExist();
                expect( $rightYBList ).not.toExist();

            } );

            it( "should update the title", function() {

                graph.navigateTo( getWhatboxDetailedCell() );
                var titles = [];

                $( "a", skShell.title.container ).each( function() {

                    titles.push( $( this ).text() );

                } );
                expect( titles ).toEqual( [ "onboarding a new employee", "Prepare new employee arrival" ] );

            } );

            it( "should refresh the search", function() {

                $( ".skMenuFind_input" ).val( "a" );
                spyOn( skShell.search, "performSearch" );
                graph.navigateTo( getWhatboxDetailedCell() );
                expect( skShell.search.performSearch ).toHaveBeenCalled();
                expect( skShell.search.keyword ).toBe( "a" );

            } );

            it( "should trigger tutorial mode", function() {

                // Initialise tutorial
                expect( skShell.tutorial ).toBeFalsy();
                $( ".skSideMenu_tutorial" ).simulate( "click" );
                expect( skShell.tutorial ).toBeTruthy();

                spyOn( skShell.tutorial, "stepDone" );
                graph.navigateTo( getWhatboxDetailedCell() );
                expect( skShell.tutorial.stepDone ).toHaveBeenCalledWith( skShell.tutorial.NAVIGATE );

            } );

            it( "should call google analytics", function() {

                var prevGa = window.ga;
                var ga = jasmine.createSpy( "ga" );

                window.ga = ga;
                window.DEV = false;
                graph.navigateTo( getWhatboxDetailedCell() );
                window.ga = prevGa;
                window.DEV = true;
                expect( ga ).toHaveBeenCalledWith( "send", "event", "SkoreAction", "actionButton", "navigateTo" );

            } );

        } );

    } );

} );
