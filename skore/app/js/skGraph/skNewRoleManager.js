
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skConstants = require( "skConstants" );
    var mxUtils = require( "mxUtils" );
    var skUtils = require( "skUtils" );

    var LOCAL_ID = skConstants.LOCAL_ID;
    var VALUE = skConstants.VALUE;

    function skNewRoleManager( graph, existingRoleManager ) {

        /*jshint validthis:true*/
        this.graph = graph;

        /*
            <role localid="" remoteid="" value="" description="" />
        */
        this.initModel( existingRoleManager );

    }

    skNewRoleManager.prototype.graph = null;

    // for autocomplete
    skNewRoleManager.prototype.roleList = null;

    skNewRoleManager.prototype.localIDCounter = null;

    // ref to graph.model.root.roles
    skNewRoleManager.prototype.roles = null;

    skNewRoleManager.prototype.initModel = function initModelF( existingRoleManager ) {

        console.log( "initModel" );
        var that = this;
        that.roleList = [];

        // assigns the node to the model
        if ( existingRoleManager ) {

            this.graph.model.root.roles = existingRoleManager;

        }

        // first create the node
        else if ( !this.graph.model.root.roles ) {

            this.graph.model.root.roles = mxUtils.createXmlDocument().createElement( skConstants.ROLES );

        }

        this.roles = this.graph.model.root.roles;

        // get the right counter
        $( skConstants.ROLE, this.roles ).each( function( i, role ) {

            var lid = parseInt( $( role ).attr( LOCAL_ID ), 10 );
            that.localIDCounter = ( lid > that.localIDCounter ? lid : that.localIDCounter );

        } );

        // assign localID to the roles (for old file version)
        $( this.roles.children ).each( function( i, role ) {

            if ( !$( role ).attr( LOCAL_ID ) ) {

                $( role ).attr( LOCAL_ID, that.localIDCounter++ );

            }

        } );

        // check in the model if something is not yet loaded or localID assigned (it has a value)
        this.graph.model.filterDescendants( function( cell ) {

            if ( cell.value ) {

                $( skConstants.ROLE + "[value]", cell.value ).each( function( i, role ) {

                    var candidate = that.getRoleByName( skUtils.readValue( role ) );

                    if ( !candidate ) {

                        candidate = that.addRole( skUtils.readValue( role ) );

                    }

                    // assign localID and remove the value
                    $( role ).attr( LOCAL_ID, $( candidate ).attr( LOCAL_ID ) );
                    $( role ).removeAttr( VALUE );

                } );

            }

        } );

        this.getRoles();

    };

    skNewRoleManager.prototype.deleteRoleByLocalID = function( ID ) {

        this.deleteRole( LOCAL_ID, ID );

    };
    skNewRoleManager.prototype.deleteRoleByName = function( name ) {

        this.deleteRole( VALUE, name );

    };
    skNewRoleManager.prototype.deleteRole = function( key, value ) {

        $( skConstants.ROLE + "[" + key + "='" + value + "']", this.roles ).remove();
        this.roleList = null;
        this.roleList = this.getRoles();

    };

    skNewRoleManager.prototype.addRole = function( name, noReset ) {

        var that = this;

        var candidate = this.getRoleByName( name );

        if ( !candidate ) {

            // var obj = $( "<" + skConstants.ROLE + " />" );
            // obj.attr( VALUE, name );
            // obj.attr( LOCAL_ID, ++that.localIDCounter );
            var obj = document.createElement( skConstants.ROLE );
            obj.setAttribute( VALUE, name );
            obj.setAttribute( LOCAL_ID, ++that.localIDCounter );

            this.roles.appendChild( obj );

            if ( noReset !== true ) {

                // reset roleList
                this.roleList = null;

            }

            return obj;

        } else if ( !$( candidate ).attr( LOCAL_ID ) ) {

            $( candidate[ 0 ] ).attr( LOCAL_ID, ++that.localIDCounter );

        }

        return candidate[ 0 ];

    };
    skNewRoleManager.prototype.getRoleByName = function( name ) {

        return this.getRole( VALUE, name );

    };

    skNewRoleManager.prototype.getRoleByLocalID = function( ID ) {

        return this.getRole( LOCAL_ID, ID );

    };
    skNewRoleManager.prototype.getRoleByRemoteID = function( ID ) {

        return this.getRole( skConstants.REMOTE_ID, ID );

    };

    skNewRoleManager.prototype.getRole = function( key, value ) {

        // backslash are the one characters that are screwing it up

        if ( !value ) {

            value = "";

        }

        var candidate = $( "[" + key + "='" + value.replace( /["\\]/g, "\\$&" ) + "']", this.roles );

        if ( candidate.length && candidate.length > 0 ) {

            return candidate[ 0 ];

        } else {

            return null;

        }

    };

    skNewRoleManager.prototype.setRoleComment = function( role, comment ) {

        $( role ).append( $( "<comment value='" + comment + "' />" ) );

    };

    skNewRoleManager.prototype.getRoles = function() {

        if ( this.roleList && this.roleList.length > 0 ) {

            return this.roleList;

        } else {

            var list = [];

            $( skConstants.ROLE, this.roles ).each( function( i, e ) {

                list.push( {
                    localid: $( e ).attr( LOCAL_ID ),
                    name: $( e ).attr( VALUE )
                } );

            } );

            this.roleList = list;
            return this.roleList;

        }

    };

    skNewRoleManager.prototype.getCommentNode = function( ID ) {

        var role = this.getRole( LOCAL_ID, ID );

        return skUtils.readValue( $( skConstants.ELMT_COMMENT, role ) );

    };

    return skNewRoleManager;

} );
