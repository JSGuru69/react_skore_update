define(
    [ "require",
    "jquery",
    "mxUtils",
    "mxCell",
    "skUtils",
    "colors",
    "mxConstants",
    "skConstants",
    "skGraph/skExternalContent",
    "lib/bootstrap3-typeahead" ],
    function(
        require, $, mxUtils, mxCell, skUtils, allColors, mxConstants, skConstants, externalcontent ) {

    "use strict";

    var counter = 0;

    /*
     * ensures that all the fields of the form are rendered
     *
     */

    function skFormRenderer( graph, cell, fieldDescription, parentValue, value, viewerMode, editableProp, saveCallBack ) {

        console.log( "form", fieldDescription, parentValue, value, viewerMode );

        /*jshint validthis:true*/
        counter = 0;
        this.graph = graph;
        this.cell = cell;
        this.editableProp = editableProp;
        this.fieldDescription = fieldDescription;
        this.originalParentValue = parentValue;
        this.originalValue = value;
        this.parentValue = parentValue;
        this.container = $( "<div />" );
        this.container.addClass( "skForm" );
        this.container.append( this.renderForm( fieldDescription, parentValue, value, viewerMode ) );
        this.saveCallBack = saveCallBack;

    }

    skFormRenderer.prototype.editableProp = null;
    skFormRenderer.prototype.saveCallBack = null;
    skFormRenderer.prototype.fieldDescription = null;
    skFormRenderer.prototype.parentValue = null;
    skFormRenderer.prototype.graph = null;
    skFormRenderer.prototype.cell = null;
    skFormRenderer.prototype.container = null;

    skFormRenderer.prototype.renderForm = function( fieldDescription, parentValue, value, viewerMode ) {

        var that = this;
        var editorMode = !viewerMode; // convenience

        // hides existing popover
        $( ".popover" ).popover( "hide" );

        /*
         * copies the value when editing
         *
         */

        if ( editorMode ) {

            value = value.cloneNode( true );

            this.editedValue = value;

        }

        var container = $( "<div />" );
        container.toggleClass( "viewer", viewerMode );
        container.toggleClass( "skSearchable", viewerMode );
        container.toggleClass( "editor", !viewerMode );

        /*
         * render field in order of the description
         *
         */

        if ( fieldDescription.name ) {

            container.append(
                that.renderField(
                    fieldDescription,
                    parentValue,
                    value,
                    viewerMode ) );

        } else if ( fieldDescription.fields ) {

            container.append(
                that.renderFields(
                    fieldDescription.fields,
                    value,
                    viewerMode
                    ) );

        }

        /*
         * edit button bar
         *
         */

        if ( ( this.editableProp.editable !== false || fieldDescription.editable ) && this.editableProp.showBtn !== false ) {

            console.log( "edit button" );

            var buttonsSuperContainer = $( '<div class="editButtons" />' );
            var buttonsContainer = $( '<div class="btn-group btn-group-xs" role="group" ></div>' );
            container.append( buttonsSuperContainer );
            buttonsSuperContainer.append( buttonsContainer );

            var editButton, cancelButton, saveButton, removeButton, moveUpBtn, moveDownBtn;

            editButton = $( "<button class='btn btn-default'>Edit</button>" ).on( "click", function() {

                container.empty();
                container.append( that.renderForm( fieldDescription, parentValue, value, false ) );

            } );

            saveButton = $( "<button class='btn btn-default'>Save</button>" ).on( "click", function() {

                that.save();

            } );

            if ( this.editableProp.showBtnCancel !== false ) {

                cancelButton = $( "<button class='btn btn-link'>Cancel</button>" ).on( "click", function() {

                    container.empty();
                    container.append( that.renderForm( fieldDescription, parentValue, value, true ) );

                } );

            }

            if ( this.editableProp.showBtnRemove !== false ) {

                removeButton = $( "<button class='btn btn-link'>Remove</button>" ).on( "click", function() {

                    $( that.originalValue ).remove();
                    container.remove();

                } );

            }

            if ( fieldDescription.sortable ) {

                moveUpBtn = $( "<button class='btn btn-default'><i class='fa fa-angle-up'></i></button>" ).on( "click", function() {

                    // move up
                    console.log( "moveUp", that, fieldDescription, value );

                    var currentValue = $( $( this ).parents( ".skForm" ).find( ".skField" )[ 0 ] ).data( "value" );

                    // move the element in the value
                    if ( currentValue && currentValue.previousSibling ) {

                        currentValue.parentElement.insertBefore( currentValue, currentValue.previousSibling );

                    }

                    // move the element in the display
                    if ( $( this ).parents( ".skForm" ).prev( ".skForm" ).length > 0 ) {

                        $( this ).parents( ".skForm" ).insertBefore( $( this ).parents( ".skForm" ).prev( ".skForm" ) );

                    }

                } );

                moveDownBtn = $( "<button class='btn btn-default'><i class='fa fa-angle-down'></i></button>" ).on( "click", function() {

                    // move down
                    console.log( "moveDown", that, fieldDescription, value );

                    var currentValue = $( $( this ).parents( ".skForm" ).find( ".skField" )[ 0 ] ).data( "value" );

                    // move the element in the value
                    if ( currentValue && currentValue.nextSibling ) {

                        currentValue.parentElement.insertBefore( currentValue, currentValue.nextSibling.nextSibling );

                    }

                    // move the element in the display
                    if ( $( this ).parents( ".skForm" ).next( ".skForm" ).length > 0 ) {

                        $( this ).parents( ".skForm" ).insertAfter( $( this ).parents( ".skForm" ).next( ".skForm" ) );

                    }

                } );

            }

            if ( viewerMode ) {

                buttonsContainer.append( moveUpBtn );
                buttonsContainer.append( moveDownBtn );
                buttonsContainer.append( editButton );

            } else {

                if ( cancelButton ) {

                    buttonsContainer.append( cancelButton );

                }

                if ( removeButton ) {

                    buttonsContainer.append( removeButton );

                }

                buttonsContainer.append( saveButton );

            }

        }

        if ( editorMode ) {

            skUtils.makeExpandingArea( $( ".expandingArea", container ) );

        }

        if ( viewerMode && fieldDescription.showOnlySelected ) {

            // now need to filter out which one are visible....
            $( ".skFormFieldInput", container ).each( function( i, field ) {

                if ( $( $( field ).data( "value" ) ).attr( "displayed" ) !== "1" ) {

                    $( field ).remove();

                }

            } );

        }

        container.append( $( "<div class='attSeparator'></div>" ) );

        return container;

    };

    skFormRenderer.prototype.renderFields = function( fields, parentValue, viewerMode, container ) {

        var that = this;
        var editorMode = !viewerMode;

        container = container || $( "<div />" );
        container.addClass( "skFields" );

        fields.forEach( function( field ) {

            /*
             * multi field, fetch values first
             *
             */

            if ( field.multi ) {

                var existingValues = $( field.name, parentValue );

                $( existingValues ).each( function( i, val ) {

                    container.append(
                    that.renderField(
                        field,
                        parentValue,
                        val,
                        viewerMode ) );

                } );

                /*
                 * add empty value
                 *
                 */

                if ( existingValues.length === 0 && field.multi.addempty === true ) {

                    container.append( that.renderField( field, parentValue, mxUtils.createXmlDocument().createElement( field.name ), viewerMode ) );

                }

                /*
                 * add more button
                 *
                 */

                if ( editorMode && field.multi.addbutton ) {

                    var addButton = $( "" +
                        '<a class="btn btn-default btn-xs" style="width:100%;" role="button">' +
                            ( field.multi.addtext || "add 1 more" ) +
                        "</a>" ).on( "click", function() {

                            $( this ).before(
                                that.renderField(
                                    field,
                                    parentValue,
                                    mxUtils.createXmlDocument().createElement( field.name ),
                                    viewerMode ) );

                        } );

                    container.append( addButton );

                }

            }

            /*
             * single field, render direct
             *
             */

            else {

                var val = null;

                /*
                 *  if field is predefined (has value), need to catch it here
                 *
                 */

                if ( parentValue && field.value ) {

                    val = parentValue.querySelector( "" + field.name + "[value='" + field.value + "']" );

                    /*
                     * will create the value in editorMode
                     *
                     */

                    if ( editorMode && !val ) {

                        val = mxUtils.createXmlDocument().createElement( field.name );

                    }

                }

                /*
                 * field is not predefined but we have a parentValue to seach into
                 *
                 */

                else if ( parentValue ) {

                    val = parentValue.querySelector( field.name );

                    /*
                     * create the value if necessary
                     *
                     */

                    if ( editorMode && !val ) {

                        val = mxUtils.createXmlDocument().createElement( field.name );

                    }

                }

                container.append(
                    that.renderField(
                        field,
                        parentValue,
                        val,
                        viewerMode ) );

            }

        } );

        return container;

    };

    var valueSecretPlaceholder = "____I_hope_Nobody_writes__that_as_a_value___";

    /*
     * renders a field
     *
     */

    skFormRenderer.prototype.renderField = function( fieldDescription, parentValue, value, viewerMode ) {

        var editorMode = !viewerMode;
        var that = this;
        var container = $( "<div />" );
        container.addClass( fieldDescription.type );
        container.addClass( fieldDescription.name );
        var localid;

        console.log( "field", parentValue, value );

        /*
         * prepare the value
         *
         */

        var myVal = skUtils.readValue( value );

        // get the localid if existing
        if ( fieldDescription.localid ) {

            localid = skUtils.readValue( value, skConstants.LOCAL_ID );
            myVal = skUtils.readValue(
                $( "" + fieldDescription.name + "[" + skConstants.LOCAL_ID + "='" + localid + "']",
                    this.graph.roleManager.roles ) );

        }

        var myInput, text;

        switch ( fieldDescription.type ){

            case "group":

                // if field is removable BUT IS NOT an editable (in this case it has full blown button)
                if ( fieldDescription.removable && !fieldDescription.editable ) {

                    // container.addClass("skFormFieldInput");
                    myInput = container;
                    myInput.val( valueSecretPlaceholder );

                    $( container ).popover( {
                        container: "body",
                        placement: "auto right",
                        html: "true",
                        trigger:"focus",
                        content: function() {

                            var popoverTrigger = $( this );

                            var buttons = $( "" +
                                "<div>" +
                                    '<button style="padding-bottom:5px;" class="popBtn popDelete btn btn-primary btn-sm">' +
                                        "<i class='fa fa-trash-o'></i>" +
                                    "</button>&nbsp;&nbsp;" +
                                "</div>" );

                            if ( fieldDescription.sortable === true ) {

                                // move up

                                if ( container.prev( "." + fieldDescription.name ).length > 0 ) {

                                    buttons.append( $( "" +
                                        '<button style="padding-bottom:5px;" class="popBtn popUp btn btn-primary btn-sm">' +
                                            "<i class='fa fa-angle-up'></i>" +
                                        "</button>&nbsp;&nbsp;" ).on( "click", function() {

                                            container.insertBefore( container.prev( ".responsibility" ) );

                                        } )
                                    );

                                }

                                // move down

                                if ( container.next( "." + fieldDescription.name ).length > 0 ) {

                                    buttons.append( $( "" +
                                        '<button style="padding-bottom:5px;" class="popBtn popDown btn btn-primary btn-sm">' +
                                            "<i class='fa fa-angle-down'></i>" +
                                        "</button>&nbsp;&nbsp;" ).on( "click", function() {

                                            container.insertAfter( container.next( ".responsibility" ) );

                                        } )
                                    );

                                }

                            }

                            $( ".popBtn", buttons ).on( "click", function() {

                                // popoverTrigger.popover( "destroy" );
                                // $( ".popover" ).popover( "hide" );

                            } );

                            $( ".popDelete", buttons ).on( "click", function() {

                                console.log( "delete", that, parentValue, value );

                                container.remove();
                                value.remove();
                                value = null;

                                popoverTrigger.popover( "destroy" );
                                $( ".popover" ).popover( "hide" );

                            } );

                            return buttons;

                        }
                    } );

                }

            break;

            case "longtext":

                if ( viewerMode ) {

                    text = skUtils.renderMarkdown( myVal );
                    if ( text === "" && fieldDescription.defaulttext ) {

                        text = fieldDescription.defaulttext;
                        container.addClass( "defaulttext" );

                    }
                    container.html( text );

                }

                // editor
                else {

                    container.append( $( "" +
                            '<div class="expandingArea">' +
                                "<pre><span></span><br></pre>" +
                                "<textarea class='form-control selectionAllowed'></textarea>" +
                        "</div>" ) );

                    if ( fieldDescription.helptext === "formatting" ) {

                        container.append( "" +
                            '<div data-skaction="modal_formatting" class="formatButton" >' +
                                "<a>Format text</a>" +
                            "</div>" );

                    }

                    myInput = $( "textarea", container );
                    myInput.attr( "placeholder", fieldDescription.defaulttext || "" );
                    myInput.val( myVal );

                    if ( DEV ) {

                        $( myInput ).on( "keyup", function() {

                            if ( $( this ).val().startsWith( "lorem " ) ) {

                                $( this ).val( "lorem ipsum dolor ![icon](camera-retro) sit amet, \n\n###consectetur " +
                                    "adipisicing \n* elit, \n* sed do eiusmod " +
                                    "tempor incididunt ut " +
                                    "\n* labore **et dolore** magna " +
                                    "\n\naliqua. Ut *enim ad minim veniam*, quis nostrud exercitation." +
                                    "\n\n---\n\n Ullamco laboris " );

                            }

                        } );

                    }

                }
            break;

            case "singleline" :

                if ( viewerMode ) {

                    text = skUtils.renderMarkdown( myVal, true );
                    if ( text === "" && fieldDescription.defaulttext ) {

                        text = fieldDescription.defaulttext;
                        container.addClass( "defaulttext" );

                    }
                    container.html( text );

                    // addon (value before or after )
                    if ( fieldDescription.addon ) {

                        // need to make a proper container first
                        container.addClass( "input-group" );
                        container.addClass( "input-group-sm" );

                        if ( fieldDescription.addon.after ) {

                            container.append( '<span class="input-group-addon">' + fieldDescription.addon.after + "</span>" );

                        }
                        if ( fieldDescription.addon.before ) {

                            container.prepend( '<span class="input-group-addon">' + fieldDescription.addon.before + "</span>" );

                        }

                    }

                } else {

                    myInput = $( '<input type="text" class="form-control input-sm selectionAllowed" >' );
                    myInput.attr( "placeholder", fieldDescription.defaulttext || "" );
                    container.append( myInput );
                    myInput.val( myVal );

                    // autocomplete
                    // if autocomplete
                    if ( fieldDescription.autocomplete ) {

                        var source;

                        if ( fieldDescription.autocomplete.source === "roles" ) {

                            source = that.graph.roleManager.getRoles();

                        } else if ( fieldDescription.autocomplete.source === "allColors" ) {

                            source = allColors;

                        }
                        myInput.attr( "autocomplete", "off" );
                        myInput.typeahead( {
                            autoSelect: false,
                            items:"all",
                            minLength:0,
                            source: source,
                            afterSelect: function( item ) {

                                if ( fieldDescription.localid ) {

                                    $( myInput ).data( "localid", item.localid );

                                }

                            }
                        } );

                        /*
                         * As there is a "conflict" with the default behaviour,
                         * need to do it manually
                         *
                         */

                        myInput.on( "keydown", function( evt ) {

                            if ( evt.keyCode == 13 || evt.keyCode == 9 ) {

                                // TODO find alternative to :visible, it's slow

                                if ( $( ".typeahead .active" ).length &&  $( ".typeahead" ).is( ":visible" ) ) {

                                    $( this ).val( $( ".typeahead .active" ).data( "value" ).name );
                                    if ( fieldDescription.localid ) {

                                        $( this ).data( skConstants.LOCAL_ID,  $( ".typeahead .active" ).data( "value" ).localid );

                                    }

                                    evt.preventDefault();

                                }

                            }

                        } );

                    }

                }

            break;

            case "button" :

                if ( viewerMode ) {

                    if ( Boolean( $( value ).attr( "value" ) && $( value ).attr( "value" ).length ) ) {

                        container = $( "<span>" + fieldDescription.value + "</span>" );
                        container.css( fieldDescription.style || "" );

                    } else {

                        container = null;

                    }

                } else if ( editorMode ) {

                    container = $( '<a class="btn btn-default btn-xs">' + fieldDescription.value + "</a>" );
                    container.toggleClass( "active", Boolean( $( value ).attr( "value" ) && $( value ).attr( "value" ).length ) );
                    myInput = container;

                    myInput.val( myVal );

                    myInput.on( "click", function() {

                        $( this ).toggleClass( "active" );

                        if ( $( this ).hasClass( "active" ) ) {

                            // $( $( this ).data( "value" ) ).attr( "value", fieldDescription.value );
                            $( this ).val( fieldDescription.value );

                        } else {

                            // $( $( this ).data( "value" ) ).removeAttr( "value" );
                            $( this ).val( "" );

                        }

                    } );

                }

            break;

            case "colorpicker":

                if ( editorMode ) {

                    container.append( $( "" +
                        '<div class="colorPicker">' +
                            '<span class="editorSubTitle">Background color</span>' +
                            '<div class="cpList" style="text-align:center;">' +

                            "</div>" +
                        "</div>" ) );

                    var state =  that.graph.view.getState( that.cell );
                    var currentColor;
                    if ( state ) {

                        currentColor = state.style.fillColor || "transparent";

                    } else {

                        currentColor = "transparent";

                    }
                    var recentlyUsed = window.localStorage.getItem( "skore.colors.recentlyUsed" );
                    if ( recentlyUsed ) {

                        recentlyUsed = recentlyUsed.split( "," );

                    } else {

                        recentlyUsed = [ "Red", "Blue", "Aqua", "Lime", "Fuchsia", "Silver" ];

                    }
                    var placeholder = $( ".cpList", container );
                    recentlyUsed.forEach( function( e, i ) {

                        placeholder.append(
                            $( "<input " +
                                'id="skCP' + i + '" ' +
                                'type="radio" ' +
                                'name="color" ' +
                                'value="' + e + '" ' + ( currentColor === e ? "checked" : "" ) +
                                " />" ) );

                        placeholder.append(
                            $( "<label " +
                                'for="skCP' + i + '" ' +
                                'style="background-color:' + e + ' ">' +
                                "</label>" ) );

                    } );
                    placeholder.append( $( "" +
                        '<input id="skYellow" type="radio" name="color" value="#FCF393" ' +
                        ( currentColor === "#FCF393" ? "checked" : "" ) +
                        ">" +
                        '<label for="skYellow" ' +
                            'style="background-color: #FCF393"></label>' +
                        '<input id="skCPNone" type="radio" name="color" value="transparent" ' +
                        ( currentColor === "transparent" ? "checked" : "" ) +
                        "/>" +
                        '<label for="skCPNone" ' +
                            'style="background-image: url(images/grid.gif); background-position-x: 4px; background-position-y: 4px;"></label>' ) );

                    // aucomplete on colors

                    var freeText = that.renderField(
                        {
                            type:"singleline",
                            name:"allColors", // optional in this case
                            autocomplete: {
                                source: "allColors"
                            }
                        }, null, currentColor, viewerMode );

                    container.append( freeText );

                    // don't make it a "myInput" as it already has most properties
                    var freeTextInput = ( freeText.is( ":input" ) ? freeText : $( "input", freeText ) );

                    // Change color in textbox
                    $( "label", container ).on( "click", function() {

                        freeTextInput.val( $( "input#" + $( this ).attr( "for" ) ).val() );

                    } );

                    // hint for save, later
                    freeTextInput.data( "specialtype", "colorpicker" );

                }

            break;

            case "url":

                if ( viewerMode ) {

                    var lnk = $( "<a/>" );
                    lnk.addClass( "skExtLink" );
                    lnk.attr( "href", skUtils.readValue( value, "addr" ) );

                    // var lnk = $( "<a class='skExtLink' " +
                    //     "href='" +  + "></a>" );
                    lnk.html( skUtils.renderMarkdown( skUtils.readValue( value ), true ) );
                    container.append( lnk );

                } else {

                    // dispalyed text (in the "value" attribute)
                    container.append( $( "<label>Displayed text</label>" ) );
                    myInput = $( '<input type="text" placeholder="Enter plain text" class="form-control input-sm selectionAllowed" >' );
                    container.append( myInput );
                    myInput.val( skUtils.readValue( value ) );

                    container.append( $( "<label>URL</label>" ) );
                    var urlInput = $( '<input placeholder="http://" type="text" class="form-control input-sm selectionAllowed" >' );
                    container.append( urlInput );

                    myInput.data( "attributes", [ { key:"addr", input:urlInput } ] );

                    urlInput.val( skUtils.readValue( value, "addr" ) );

                }
            break;

            case skConstants.EXT_CONTENT:

                if ( viewerMode ) {

                    container.append( externalcontent[ skUtils.readValue( value, "contenttype" ) ]( skUtils.readValue( value ) ) );

                } else {

                    container.append( $( "" +
                        '<div class="input-group input-group-sm">' +
                            '<div class="input-group-btn">' +
                                '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" ' +
                                'aria-expanded="false"><span class="contenttype">Youtube</span> <span class="caret"></span></button>' +
                                '<ul class="dropdown-menu">' +
                                    '<li class="dropdown-header">Video</li>' +
                                        '<li><a data-xcontenttype="youtube" data-xcontentdescription="YouTube Page URL">Youtube</a></li>' +
                                        '<li><a data-xcontenttype="vimeo" data-xcontentdescription="Vimeo Page URL">Vimeo</a></li>' +
                                        "<li>" +
                                            '<a data-xcontenttype="dailymotion" data-xcontentdescription="Dailymotion Page URL">DailyMotion</a>' +
                                        "</li>" +
                                    '<li class="dropdown-header">Images</li>' +
                                        '<li><a data-xcontenttype="image" data-xcontentdescription="Image URL">Image URL</a></li>' +
                                    '<li class="dropdown-header">Other</li>' +
                                        "<li>" +
                                            '<a data-xcontenttype="googlemaps" data-xcontentdescription="Google Maps iFrame Embed">' +
                                                "Google Maps" +
                                            "</a></li>" +
                                        '<li><a data-xcontenttype="iframe" data-xcontentdescription="iFrame embed">Generic iFrame</a></li>' +
                                "</ul>" +
                            "</div>" +
                            '<input type="text" class="selectionAllowed form-control">' +
                        "</div>" ) );

                    myInput = $( "input", container );
                    myInput.val( skUtils.readValue( value ) );

                    var label = $( "<label></label>" );
                    container.prepend( label );

                    // select right content style first
                    var ctttype = skUtils.readValue( value, "contenttype" );
                    if ( !ctttype ) {

                        ctttype = "youtube";

                        // add it to the value, this just helps to not confuse the thing
                        $( value ).attr( "contenttype", "youtube" );

                    }

                    $( ".contenttype", container ).text( $( "[data-xcontenttype='" + ctttype + "']", container ).text() );
                    $( ".contenttype", container ).val( ctttype );
                    label.text( $( "[data-xcontenttype='" + ctttype + "']", container ).data( "xcontentdescription" ) );

                    // switch style accordingly
                    $( "[data-xcontenttype]", container ).on( "click", function() {

                        $( ".contenttype", container ).text( $( this ).text() );
                        $( ".contenttype", container ).val( $( this ).data( "xcontenttype" ) );
                        label.text( $( this ).data( "xcontentdescription" ) );

                    } );

                    myInput.data( "attributes", [ { key:"contenttype", input: $( ".contenttype", container ) } ] );

                }
            break;

            case "checklist":

                if ( viewerMode ) {

                    // container = $( "<div>" +
                    container.append( $( "" +
                        '<div class="checkbox">' +
                          "<label>" +
                            '<input type="checkbox" disabled ' +
                                ( skUtils.readValue( value, "status" ) === "1" ? "checked " : "" ) +
                            ">" +
                                skUtils.renderMarkdown( skUtils.readValue( value ), true ) +
                          "</label>" +

                        // "</div>" +
                    "</div>" ) );

                } else {

                    // container = $( "<div data-skcounter='" + ( ++counter ) + "' />" );
                    container.append( "" +
                        '<div class="input-group">' +
                          '<span class="input-group-addon">' +
                            '<input class="selectionAllowed" type="checkbox" ' +
                                ( skUtils.readValue( value, "status" ) === "1" ? "checked " : "" ) +
                            ">" +
                          "</span>" +
                          '<input type="text" class="selectionAllowed form-control">' +
                        "</div>" );

                    myInput = $( "input[type=text]", container );
                    myInput.val( skUtils.readValue( value ) );
                    myInput.data( "attributes", [ { key:"status", input:$( "input[type=checkbox]", container ) } ] );

                }

            break;

            case "separator" :

                if ( viewerMode || ( editorMode && fieldDescription.editable === false ) ) {

                    var style = that.graph.getCellStyle( that.cell );
                    var line = "" + style.whoWhatLine;

                    if ( line && line === "1" ) {

                        container.append( $( "" +
                            "<div style='height:1px; " +
                                "border-bottom:" + style[ mxConstants.STYLE_STROKEWIDTH ] + "px solid " +
                                style[ mxConstants.STYLE_STROKECOLOR ] + "; '>" +
                                "</div>" ) );

                    } else if ( line && line === "css" ) {

                        container.append( $( "<div style='height:1px; " +
                            decodeURIComponent( style.whoWhatLineCSS ) +
                            "'></div>" ) );

                    }

                }

            break;

        } // end switch

        // add relevant class to container (if exist! might not... see "button" type)

        if ( container ) {

            container.addClass( "skField " + fieldDescription.type + " " );
            container.toggleClass( "viewerMode", viewerMode );
            container.toggleClass( "editorMode", editorMode );
            container.attr( "data-skcounter", ++counter );

            if ( viewerMode ) {

                container.data( "value", value ); // usefull to drag and drop attachments

            }

        }

        if ( myInput ) {

            myInput.addClass( "skFormFieldInput" );

            // if the field is sortable, we "detach" it so that it's re-attached at save
            if ( editorMode && fieldDescription.sortable && value.parentElement ) {

                value = value.parentElement.removeChild( value );

            }

            myInput.data( "value", value );
            myInput.data( "parentvalue", parentValue );
            myInput.data( "fielddescription", fieldDescription );

        }

        if ( fieldDescription.fields && container ) {

            container.append(
                that.renderFields(
                    fieldDescription.fields,
                    value,
                    viewerMode,
                    container ) );

        }

        /*
         * special case for responsibility management because they can
         * be shown both on the box and the attachments
         *
         */

        if ( editorMode && fieldDescription.name === "responsibility" && container ) {

            var shownCheckBox = $( "" +
                '<div class="checkbox">' +
                    "<label>" +
                        '<input type="checkbox" ' +
                        ( $( value ).attr( "displayed" ) === "0" ? "" : "checked" ) +
                        ">" +
                        "Show on box" +
                    "</label>" +
                "</div>" );

            container.append( shownCheckBox );

            myInput.data( "attributes", [ { key:skConstants.ATTR_SHOWN, input:$( "input[type=checkbox]", shownCheckBox ) } ] );

        }

        return container;

    };

    skFormRenderer.prototype.save = function( reload ) {

        console.log( "skFormRenderer.save" );

        if ( DEV ) {

            var b = [];
            $( ".skFormFieldInput", this.container ).each( function( i, field ) {

                b.push( [ field, $( field ).data( "parentvalue" ), $( field ).data( "value" ), $( field ).val() ] );

            } );
            console.table( b );

        }

        $( ".skFormFieldInput", this.container ).each( mxUtils.bind( this, function( i, field ) {

            var myVal = $( field ).val();
            var value = $( field ).data( "value" );
            var parentValue = $( field ).data( "parentvalue" );
            var fieldDescription = $( field ).data( "fielddescription" );
            var specialtype = $( field ).data( "specialtype" );

            // remove if empty
            if ( !myVal || myVal.length === 0 ) {

                $( value ).remove();
                value = null;

            }

            // colorPicker
            else if ( specialtype === "colorpicker" ) {

                var color = $( field ).val();
                this.graph.setCellStyles( mxConstants.STYLE_FILLCOLOR, color, [ this.cell ] );
                this.graph.setCellStyles( mxConstants.STYLE_GRADIENTCOLOR, "none", [ this.cell ] );

                if ( color !== "transparent" && color !== "none" && color !== "#FCF393" && skUtils.isValidHtmlColor( color ) ) {

                    // Exists
                    var recentlyUsed = window.localStorage.getItem( "skore.colors.recentlyUsed" );
                    if ( recentlyUsed ) {

                        recentlyUsed = recentlyUsed.split( "," );

                        if ( recentlyUsed.indexOf( color ) > -1 ) {

                            // Remove from where it is
                            recentlyUsed.splice( recentlyUsed.indexOf( color ), 1 );

                            // Add in front
                            recentlyUsed.unshift( color );

                        } else {

                            // Remove last item
                            recentlyUsed.pop();

                            // Add in front
                            recentlyUsed.unshift( color );

                        }

                        // Just make sure we only have 6 items in there (+ yellow + transparent that stay there )
                        recentlyUsed.splice( 6 );

                        // save
                        window.localStorage.setItem( "skore.colors.recentlyUsed", recentlyUsed.toString() );

                    }

                }

            } else {

                /*
                 * do not write value for groups
                 *
                 */

                if ( myVal !== valueSecretPlaceholder ) {

                    //
                    /*
                     * deal with localid / roles / enrichsource
                     *
                     * only for role now
                     *
                     */

                    if (  fieldDescription.localid ) {

                        if ( $( field ).data( skConstants.LOCAL_ID ) ) {

                            $( value ).attr(
                                skConstants.LOCAL_ID,
                                $( field ).data(
                                    skConstants.LOCAL_ID ) );

                        } else {

                            /*
                             * autocomplete enrich source
                             *
                             * only for roles now
                             *
                             */

                            var r = this.graph.roleManager.addRole( $( field ).val() );
                            if ( fieldDescription.localid ) {

                                $( value ).attr( skConstants.LOCAL_ID, $( r ).attr( skConstants.LOCAL_ID ) );

                            }

                        }

                    } else {

                        $( value ).attr( "value", myVal );

                    }

                }

                /*
                 * work on the attributes [{key: string, input: $ }, ...]
                 *
                 */

                 var attributes = $( field ).data( "attributes" );
                 if ( attributes ) {

                    $( attributes ).each( function( i, att ) {

                        var v;
                        if ( $( att.input ).is( ":checkbox" ) ) {

                            v = ( $( att.input ).is( ":checked" ) ? "1" : "0" );

                        } else {

                            v = $( att.input ).val();

                        }
                        $( value ).attr( att.key, v );

                    } );

                }

            }

            if ( value ) {

                if ( !value.parentElement && parentValue ) {

                    parentValue.appendChild( value );

                }

            }

        } ) );

        this.originalValue.parentElement.replaceChild( this.editedValue, this.originalValue );

        // $(this.originalValue).replaceWith($( this.editedValue) );

        this.originalValue = this.editedValue;

        this.container.empty();

        if ( reload !== false ) {

            this.container.append( this.renderForm( this.fieldDescription, this.parentValue, this.editedValue, true ) );

        }

        if ( this.saveCallBack && typeof this.saveCallBack === "function" ) {

            this.saveCallBack.call( this, this.parentValue, this.editedValue );

        }

        return this.parentValue;

    };

    return skFormRenderer;

} );
