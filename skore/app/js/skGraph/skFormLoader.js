define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skConstants = require( "skConstants" );
    var skFormRenderer = require( "skGraph/skFormRenderer" );
    var skTemplateDescriptions = require( "./skTemplateDescriptions" );

    /*
     * will load the right form(s) for the given cell & type...
     *
     */

    function skFormLoader( graph, cell, type, viewerMode, editableProp, saveCallBack ) {

        /*jshint validthis:true*/
        this.container = $( "<div />" );
        this.container.addClass( "skFormContainer" );
        this.container.addClass( type );
        this.forms = [];
        var that = this;
        var template, form;

        type = type || cell.value.tagName.toLowerCase();

        switch ( type.toLowerCase() ) {

            case skConstants.ROLE :

                var roleManager = graph.roleManager;

                // get the right template
                template = graph.templateManager.getTemplate( type );

                // skForm( graph, cell, fieldDescription, parentValue, value, viewerMode, saveCallBack ) {
                form = new skFormRenderer(
                    graph,
                    null,
                    template,
                    roleManager.roles,
                    cell, // which is the role in this case
                    viewerMode,
                    {
                        editable: graph.isEnabled()
                    },
                    saveCallBack );

                that.container.append( form.container );
                that.forms.push( form );

            break;

            case skConstants.ATTACHMENTS :

                // get the attachments
                var parentValue = cell.value.querySelector( skConstants.ATTACHMENTS );

                if ( parentValue ) {

                    var attList = parentValue.children;

                    // create a form per attachments
                    $( attList ).each( function( i, att ) {

                        // skForm( graph, cell, fieldDescription, parentValue, value, viewerMode, saveCallBack ) {
                        var form = new skFormRenderer(
                            graph,
                            cell,
                            skTemplateDescriptions.attachment[ att.tagName.toLowerCase() ],
                            parentValue,
                            att,
                            viewerMode,
                            {
                                editable: graph.isEnabled() && editableProp.editable
                            },
                            saveCallBack );

                        that.forms.push( form );
                        that.container.append( form.container );

                    } );

                }

            break;

            case "whatbox" :

                /*
                 * display the box
                 *
                 */

                template = graph.templateManager.getTemplate( skConstants.whatbox );
                form = new skFormRenderer(
                    graph,
                    cell,
                    template,
                    cell.value,
                    cell.value.querySelector( skConstants.BOX ),
                    viewerMode,
                    {
                        editable:true,
                        showBtn:false
                    },
                    saveCallBack );

                that.forms.push( form );
                that.container.append( form.container );

                /*
                 * and the visible responsibility
                 *
                 */

                template = graph.templateManager.getTemplate( skConstants.RESPONSIBILITY );
                template.showOnlySelected = true; // this will be the flag
                var respsForm = new skFormRenderer(
                    graph,
                    cell,
                    template,
                    cell.value,
                    cell.value.querySelector( skConstants.RESPONSIBILITIES ),
                    viewerMode,
                    {
                        editable: true,
                        showBtn: false
                    },
                    saveCallBack );

                that.forms.push( respsForm );
                that.container.append( respsForm.container );

            break;

            case skConstants.RESPONSIBILITIES :

                /*
                 * display ALL the responsibility (in the attachment panel probably)
                 *
                 */

                // get the right template
                template = graph.templateManager.getTemplate( skConstants.RESPONSIBILITY );

                // skForm( graph, cell, fieldDescription, parentValue, value, viewerMode, saveCallBack ) {
                template.showOnlySelected = false;
                form = new skFormRenderer(
                    graph,
                    cell,
                    template,
                    cell.value,
                    cell.value.querySelector( skConstants.RESPONSIBILITIES ),
                    viewerMode,
                    {
                        editable: true,
                        showBtn: true,
                        showBtnRemove: false,
                        showBtnCancel: false
                    },
                    saveCallBack );

                that.forms.push( form );
                that.container.append( form.container );

            break;

            case "whatbox-light":

                type = "whatbox";

            /* falls through */
            default :

                console.log( "default" );

                // get the right template
                template = graph.templateManager.getTemplate( type );

                // skForm( graph, cell, fieldDescription, parentValue, value, viewerMode, saveCallBack ) {
                form = new skFormRenderer(
                    graph,
                    cell,
                    template,
                    cell.value,
                    cell.value.querySelector( skConstants.BOX ),
                    viewerMode,
                    $.extend( {}, editableProp ),
                    saveCallBack );

                that.container.append( form.container );
                that.forms.push( form );

            break;
        }

        // if ( false ) {

        //     /*
        //      * add the edit button
        //      *
        //      */

        //     var btnContainer = $( '<div class="btn-group btn-group-xs" role="group">' );

        //     var editButton = $( '<button type="button" class="btn btn-default">Edit</button>' );
        //     btnContainer.append( editButton );

        //     editButton.on( "click", function() {

        //         that.forms.forEach( function( form ) {

        //             form.container.empty();
        //             form.container.append(
        //                 form.renderForm(
        //                     form.fieldDescription,
        //                     form.originalParentValue,
        //                     form.originalValue,
        //                     false ) );

        //         } );

        //         // cancel / save
        //         $( editButton ).detach();

        //         //  cancel
        //         var cancelBtn = $( '<button type="button" class="btn btn-default">Cancel</button>' );
        //         var saveBtn = $( "<button class='btn btn-default'>save</button>" );

        //         cancelBtn.on( "click", function() {

        //             that.forms.forEach( function( form ) {

        //                 form.container.empty();
        //                 form.container.append(
        //                     form.renderForm(
        //                         form.fieldDescription,
        //                         form.originalParentValue,
        //                         form.originalValue,
        //                         true ) );

        //             } );

        //             cancelBtn.remove();
        //             saveBtn.remove();
        //             $( btnContainer ).append( editButton );

        //         } );

        //         $( btnContainer ).append( cancelBtn );
        //         $( btnContainer ).append( saveBtn );

        //         //  save
        //         saveBtn.on( "click", function() {

        //             that.save();

        //             cancelBtn.remove();
        //             saveBtn.remove();
        //             $( btnContainer ).append( editButton );

        //         } );

        //     } );

        //     that.container.append( btnContainer );

        // }

    }

    skFormLoader.prototype.forms = null;
    skFormLoader.prototype.container = null;

    skFormLoader.prototype.save = function( reload ) {

        this.forms.forEach( function( form ) {

            form.save( reload );

        } );

    };

    skFormLoader.prototype.getFullForm = function() {

        return this.container[ 0 ];

    };

    return skFormLoader;

} );
