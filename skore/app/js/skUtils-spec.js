define( [ "Injector" ], function( Injector ) {

    "use strict";

    describe( "skUtils.findRelationship", function() {

        var injector = new Injector();

        beforeAll( injector.reset );

        var mxCell;
        var mxGraphModel;
        var skUtils;

        beforeAll( injector.run(
            function( _mxCell /*mxCell*/,
                      _mxGraphModel /*mxGraphModel*/,
                      _skUtils /*skUtils*/) {

                mxCell = _mxCell;
                mxGraphModel = _mxGraphModel;
                skUtils = _skUtils;

            }
        ) );

        // Helper to build cells hierarchy
        // each call to addBox or addGroup adds another level
        function buildModel( model, parent, cells ) {

            function isGroupCell( cell, value ) {

                cell.isGroup = function() {

                    return value;

                };

                return cell;

            }

            if ( model == null ) {

                model = new mxGraphModel();
                isGroupCell( model.getRoot(), false );
                isGroupCell( model.getRoot().getChildAt( 0 ), false );

            }

            if ( parent == null ) {

                parent = model.getRoot().getChildAt( 0 );

            }

            if ( cells == null ) {

                cells = {};

            }

            function createCell() {

                return isGroupCell( new mxCell(), false );

            }

            function createGroupCell() {

                return isGroupCell( new mxCell(), true );

            }
            var self = {
                model: model,
                cells: cells,
                addBox: function( key ) {

                    var child = model.add( parent, createCell() );

                    child.setCollapsed( true );

                    if ( key ) {

                        cells[ key ] = child;

                    }

                    return buildModel( model, child, cells );

                },
                addGroup: function( key ) {

                    var child = model.add( parent, createGroupCell() );

                    if ( key ) {

                        cells[ key ] = child;

                    }

                    return buildModel( model, child, cells );

                },
                nameRoot: function( key ) {

                    cells[ key ] = model.getRoot();

                    return self;

                }
            };

            return self;

        }

        it( "A -> B, should return VISIBLE", function() {

            var b = buildModel()
                .addBox( "A" )
                .addBox( "B" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "VISIBLE" );

        } );

        it( "A -> B -> X, should return VISIBLE", function() {

            var b = buildModel()
                .addBox( "A" )
                .addBox( "B" )
                .addBox();

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "VISIBLE" );

        } );

        it( "A -> group -> B, should return VISIBLE", function() {

            var b = buildModel()
                .addBox( "A" )
                .addGroup()
                .addBox( "B" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "VISIBLE" );

        } );

        it( "A -> group -> B -> X, should return VISIBLE", function() {

            var b = buildModel()
                .addBox( "A" )
                .addGroup()
                .addBox( "B" )
                .addBox();

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "VISIBLE" );

        } );

        it( "A -> groupX -> groupY -> B, should return VISIBLE", function() {

            var b = buildModel()
                .addBox( "A" )
                .addGroup()
                .addGroup()
                .addBox( "B" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "VISIBLE" );

        } );

        // Display root represents a separate diagram
        it( "A -> display root -> B, should return CHILD", function() {

            var b = buildModel()
                .addBox( "A" )
                .addBox()
                .addBox( "B" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "CHILD" );

        } );

        it( "A -> groupX -> groupY -> display root -> groupZ -> B, should return CHILD", function() {

            var b = buildModel()
                .addBox( "A" )
                .addGroup()
                .addGroup()
                .addBox()
                .addGroup()
                .addBox( "B" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "CHILD" );

        } );

        it( "A -> groupX -> groupY -> display root -> groupZ -> B -> X, should return CHILD", function() {

            var b = buildModel()
                .addBox( "A" )
                .addGroup()
                .addGroup()
                .addBox()
                .addGroup()
                .addBox( "B" )
                .addBox();

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "CHILD" );

        } );

        it( "A -> display root -> display root -> B, should return GRANDCHILD", function() {

            var b = buildModel()
                .addBox( "A" )
                .addBox()
                .addBox()
                .addBox( "B" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "GRANDCHILD" );

        } );

        it( "A -> display root -> display root -> B -> X, should return GRANDCHILD", function() {

            var b = buildModel()
                .addBox( "A" )
                .addBox()
                .addBox()
                .addBox( "B" )
                .addBox();

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "GRANDCHILD" );

        } );

        it( "B(group) -> A, should return NONE", function() {

            var b = buildModel()
                .addGroup( "B" )
                .addBox( "A" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "NONE" );

        } );

        it( "B -> A, should return PARENT", function() {

            var b = buildModel()
                .addBox( "B" )
                .addBox( "A" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "PARENT" );

        } );

        it( "B -> group -> A, should return PARENT", function() {

            var b = buildModel()
                .addBox( "B" )
                .addGroup()
                .addBox( "A" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "PARENT" );

        } );

        it( "B -> display root -> A, should return GRANDPARENT", function() {

            var b = buildModel()
                .addBox( "B" )
                .addBox()
                .addBox( "A" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "GRANDPARENT" );

        } );

        it( "B(root) -> A, should return PARENT", function() {

            var b = buildModel()
                .nameRoot( "B" )
                .addBox( "A" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "PARENT" );

        } );

        it( "B(root) -> group -> A, should return PARENT", function() {

            var b = buildModel()
                .nameRoot( "B" )
                .addGroup()
                .addBox( "A" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "PARENT" );

        } );

        it( "B(root) -> display root -> A, should return GRANDPARENT", function() {

            var b = buildModel()
                .nameRoot( "B" )
                .addBox( "X" )
                .addBox( "A" );

            expect( skUtils.findRelationship( b.model, b.cells.A, b.cells.B ) ).toEqual( "GRANDPARENT" );

        } );

    } );

} );
