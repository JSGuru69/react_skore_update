define( function( require ) {

    "use strict";

    var mxCodec = require( "mxCodec" );
    var mxGraph = require( "mxGraph" );
    var mxGraphView = require( "mxGraphView" );
    var mxPoint = require( "mxPoint" );
    var mxRectangle = require( "mxRectangle" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skGraph = require( "skGraph/skGraph" );

    skGraph.prototype.readGraphState = function readGraphStateF( node ) {

        this.gridEnabled = node.getAttribute( "grid" ) != "0";
        this.gridSize = parseFloat( node.getAttribute( "gridSize" ) ) || mxGraph.prototype.gridSize;
        this.graphHandler.guidesEnabled = node.getAttribute( "guides" ) != "0";
        this.setTooltips( node.getAttribute( "tooltips" ) != "0" );
        this.setConnectable( node.getAttribute( "connect" ) != "0" );
        this.connectionArrowsEnabled = node.getAttribute( "arrows" ) != "0";
        this.foldingEnabled = node.getAttribute( "fold" ) != "0";

        this.directLinkModeEnabled = node.getAttribute( "directlinkmode" ) == "1" || false;

        var ps = node.getAttribute( "pageScale" );

        if ( ps != null )
        {

            this.pageScale = ps;

        } else
        {

            this.pageScale = mxGraph.prototype.pageScale;

        }

        var gc = node.getAttribute( "gridColor" );

        if ( gc != null )
        {

            this.view.gridColor = gc.toLowerCase();

        } else
        {

            this.view.gridColor = mxGraphView.prototype.gridColor;

        }

        var pv = node.getAttribute( "page" );

        if ( pv != null )
        {

            this.pageVisible = ( pv == "1" );

        } else
        {

            this.pageVisible = this.defaultPageVisible;

        }

        this.pageBreaksVisible = this.pageVisible;
        this.preferPageSize = this.pageBreaksVisible;

        var pw = node.getAttribute( "pageWidth" );
        var ph = node.getAttribute( "pageHeight" );

        if ( pw != null && ph != null )
        {

            this.pageFormat = new mxRectangle( 0, 0, parseFloat( pw ), parseFloat( ph ) );

        }

        // Loads the persistent state settings
        var bg = node.getAttribute( "background" );

        if ( bg != null && bg.length > 0 )
        {

            this.background = bg;

        } else
        {

            this.background = this.defaultGraphBackground;

        }

        // Loads the zoom
        // var scale = node.getAttribute( "scale" );

        // if ( scale != null ) {

        //     this.view.scale = scale;

        // }

    };

    /**
     * Sets the XML node for the current diagram.
     */

    skGraph.prototype.setGraphXml = function setGraphXmlF( node ) {

        if ( node != null ) {

            var dec = new mxCodec( node.ownerDocument );

            if ( node.nodeName == "mxGraphModel" ) {

                this.model.beginUpdate();

                try {

                    this.model.clear();

                    // This.view.scale = 1;
                    this.readGraphState( node );

                    // No page view in embed /justSkore.it
                    if ( VIEWER_MODE ) {

                        this.pageVisible = false;
                        this.pageBreaksVisible = false;

                        // Not useful when VIEWER_MODE is deployed
                        this.getPagePadding = function getPagePaddingF() {

                            return new mxPoint( 0, 0 );

                        };

                    }

                    this.updateGraphComponents();

                    if ( this.isDirectLinkModeEnabled() ) {

                        this.removeBoxConnectionConstraints();

                    } else {

                        if ( this.isEnabled() ) {

                            this.addBoxConnectionConstraints();

                        }

                    }

                    dec.decode( node, this.getModel() );

                    this.getStylesheet().loadCustomStylesheet( true, false );

                } catch ( e ) {

                    console.error( e );

                } finally {

                    this.model.endUpdate();

                }

                this.resetScrollbars();

            } else {

                throw {

                    message: "Cannot open file",
                    node: node,
                    toString: function() {

                        return this.message;

                    }
                };

            }

        } else {

            this.resetGraph();
            this.model.clear();

            // this.title.update( this.graph );

            this.resetScrollbars();

        }

        // This.graph.view.setCurrentRoot(null);

    }; // End set graph xml

    /**
     * Returns the XML node that represents the current diagram.
     */

    skGraph.prototype.getGraphXml = function getGraphXmlF( ignoreSelection )
    {

        ignoreSelection = ( ignoreSelection != null ) ? ignoreSelection : true;
        var node = null;

        if ( ignoreSelection )
        {

            var enc = new mxCodec( mxUtils.createXmlDocument() );
            node = enc.encode( this.getModel() );

        } else
        {

            node = this.encodeCells( this.getSelectionCells() );

        }

        if ( this.view.translate.x !== 0 || this.view.translate.y !== 0 )
        {

            node.setAttribute( "dx", Math.round( this.view.translate.x * 100 ) / 100 );
            node.setAttribute( "dy", Math.round( this.view.translate.y * 100 ) / 100 );

        }

        node.setAttribute( "grid",      ( this.isGridEnabled() ) ? "1" : "0" );
        node.setAttribute( "gridSize",    this.gridSize );
        node.setAttribute( "guides",    ( this.graphHandler.guidesEnabled ) ? "1" : "0" );
        node.setAttribute( "tooltips",  ( this.tooltipHandler.isEnabled() ) ? "1" : "0" );
        node.setAttribute( "connect",   ( this.connectionHandler.isEnabled() ) ? "1" : "0" );
        node.setAttribute( "arrows",    ( this.connectionArrowsEnabled ) ? "1" : "0" );
        node.setAttribute( "fold",      ( this.foldingEnabled ) ? "1" : "0" );
        node.setAttribute( "page",      ( this.pageVisible ) ? "1" : "0" );
        node.setAttribute( "pageScale",   this.pageScale );
        node.setAttribute( "pageWidth",   this.pageFormat.width );
        node.setAttribute( "pageHeight",  this.pageFormat.height );

        if ( this.isDirectLinkModeEnabled() ) {

            node.setAttribute( "directlinkmode", "1" );

        } else {

            node.removeAttribute( "directlinkmode" );

        }

        if ( this.view.gridColor != mxGraphView.prototype.gridColor )
        {

            node.setAttribute( "gridColor", this.view.gridColor );

        }

        if ( this.background != null )
        {

            node.setAttribute( "background", this.background );

        }

        // add the file Spec version in ELECTRON
        if ( ELECTRON ) {

            var pack = window.requireNode( "fs-jetpack" ).read(
                window.requireNode( "electron" ).remote.app.getAppPath() + "/package.json", "json" );
            node.setAttribute( "skoreFileSpec", pack.skoreFileSpecificationVersion );

        }

        return node;

    };

    // End getgraph xml

    /*
     * Skore function
     *
     */

    mxCodec.prototype.skConvertValue = function skConvertValueF( cell ) {

        try {

        var i, j, box;
        /*
         // original
         <Whatbox id="x" wRes="" wText="This is the title (id=1)">
         <wAtts>
            <wAtt a_t="text" value="att"/>
         </wAtts>
         </Whatbox>

         // new
         <whatbox id="x">
             <resource value="resource"/>
             <text value="text"/>
             <attachment value="my text" type="text">
             <attachment value="skore" type="url" addr="https://www.getskore.com">
         </whatbox>

         // new new (29th sept 2016)
        <whatbox>
            <box>
                <text />
            </box
            <responsibilities>
                <responsibility>
                    <role value="Director">
                    <tag value="A" >
                    <tag value="R">
                </responsibility>
            </responsibilities>
            <attachments>
                <text />
            </attachments>
        </whatbox>

         */

        // Old values to replace
        var text;
        var oldValues = {
            stickyNote_text: "stickyText",
            whatbox_attachments: "wAtts",
            whatbox_attachment: "wAtt",
            resource: "wRes",
            WB_DFLT_TEXT_activity: "wText",
            whybox: "yText",
            whatbox_value: "Whatbox",
            whybox_value: "Whybox",
            stickyNote_value: "stickyNote",
            att_type: "a_t"

        };

        // Create element
        // var node = cell.value;
        var newDoc = mxUtils.createXmlDocument();
        var newValue = null;

        /*
         * WHATBOX + GROUP -- OLD to NEW
         *
         */

        if ( cell.value.nodeName == oldValues.whatbox_value ) {

            if ( cell.style == skConstants.group ) {

                newValue = newDoc.createElement( skConstants.group );

            } else if ( cell.style == skConstants.title ) {

                newValue = newDoc.createElement( skConstants.title );

            } else {

                newValue = newDoc.createElement( skConstants.whatbox );

            }

            // Text
            text = mxUtils.createXmlDocument().createElement( skConstants.TEXT );
            text.setAttribute( skConstants.VALUE, cell.value.getAttribute( oldValues.WB_DFLT_TEXT_activity ) ); // Todo constants
            cell.value.removeAttribute( oldValues.WB_DFLT_TEXT_activity );
            newValue.appendChild( text );

            // Resource
            if ( cell.value.getAttribute( oldValues.resource ) ) {

                var res = mxUtils.createXmlDocument().createElement( skConstants.RESOURCE );

                res.setAttribute( skConstants.VALUE, cell.value.getAttribute( oldValues.resource ) ); // Todo constants
                newValue.appendChild( res );

            }

            cell.value.removeAttribute( oldValues.resource );

            // Attachments
            var allAtts = cell.value.getElementsByTagName( oldValues.whatbox_attachment );

            for ( i = 0; i < allAtts.length; i++ ) {

                var att = mxUtils.createXmlDocument().createElement( skConstants.ATTACHMENT );

                switch ( allAtts[ i ].getAttribute( oldValues.att_type ) ) {

                    case "text" : // Todo constants
                    att.setAttribute( skConstants.ATTR_TYPE, skConstants.text_type );
                    att.setAttribute( skConstants.VALUE, allAtts[ i ].getAttribute( "value" ) );
                    break;

                    case "url": // Todo constants
                    att.setAttribute( skConstants.ATTR_TYPE, skConstants.url_type );
                    att.setAttribute( skConstants.VALUE, allAtts[ i ].getAttribute( "value" ) );
                    att.setAttribute( skConstants.url_address, allAtts[ i ].getAttribute( skConstants.url_address ) );
                    break;
                }

                newValue.appendChild( att );

            }

            // Remove the "old" attachments
            var toDelete = cell.value.getElementsByTagName( oldValues.whatbox_attachments )[ 0 ];

            if ( toDelete ) {

                cell.value.removeChild( toDelete );

            }

            cell.value = null;
            cell.value = newValue;

        }

        /*
         * WHYBOX OLD TO NEW
         */

        else if ( cell.value.nodeName == oldValues.whybox_value ) {

            newValue = newDoc.createElement( skConstants.whybox );

            // Text
            text = mxUtils.createXmlDocument().createElement( skConstants.TEXT );
            text.setAttribute( skConstants.VALUE, cell.value.getAttribute( oldValues.whybox ) ); // Todo constants
            // node.removeAttribute( oldValues.WB_DFLT_TEXT_activity );
            newValue.appendChild( text );

            cell.value = null;
            cell.value = newValue;

        }

        /*
         * STICKYNOTE OLD TO NEW
         */

        else if ( cell.value.nodeName == oldValues.stickyNote_value ) {

            newValue = newDoc.createElement( skConstants.stickynote );

            // Text
            text = mxUtils.createXmlDocument().createElement( skConstants.TEXT );
            text.setAttribute( skConstants.VALUE, cell.value.getAttribute( oldValues.stickyNote_text ) ); // Todo constants
            // node.removeAttribute( oldValues.stickyNote_text );
            newValue.appendChild( text );

            cell.value = null;
            cell.value = newValue;

        }

        /*
         * PHASE 2 : NEW to "NEW NEW"
         *
         */

         /*
         * WHATBOX FROM NEW TO "NEW NEW"
         *
         */

        if ( cell.value.nodeName == skConstants.whatbox && cell.value.querySelector( skConstants.BOX ) == null ) {

            // Second pass (sept 2016)

            /*

            FROM

            <whatbox>
                <text value="..." />
                <resource value="resource">
                  <tag value="S"/>
                  <tag value="T"/>
                </resource>
                <attachment type="text" value="att text"/>
                <attachment type="url" value="skore homepage" addr="https://www.getskore.com"/>
            </whatbox>

            TO

            <whatbox>
                <box>
                    <text value="..." />
                </box>
                <responsibilities>
                    <responsibility displayed="1">
                        <role value="Director" />
                        <tag value="A" />
                        <tag value="R" />
                    </responsibility>
                    <responsibility displayed="1" />
                    <responsibility displayed="1" />
                    <responsibility displayed="1" />
                </responsibilities>
                <attachments>
                    <text value="..." />
                    <url value="..." addr="..." />
                </attachments>

            </

            */

            /*
             * box
             *
             */

            newValue = newDoc.createElement( skConstants.whatbox );

            box = newDoc.createElement( skConstants.BOX );
            newValue.appendChild( box );

            // step 2, move the text
            // will move http://devdocs.io/dom/node/appendchild
            var textTag = cell.value.getElementsByTagName( skConstants.TEXT )[ 0 ];
            if ( textTag ) {

                box.appendChild( textTag );

            }

            /*
             * responsiblities
             *
             */

            var resps = newDoc.createElement( skConstants.RESPONSIBILITIES );
            newValue.appendChild( resps );

            var resources = cell.value.getElementsByTagName( skConstants.RESOURCE );
            for ( i = 0; i < resources.length; i++ ) {

                var resp = newDoc.createElement( skConstants.RESPONSIBILITY );
                resp.setAttribute( skConstants.ATTR_SHOWN, "1" );
                resps.appendChild( resp );

                var role = newDoc.createElement( skConstants.ROLE );
                var roleText = resources[ i ].getAttribute( skConstants.VALUE );
                if ( roleText.length ) {

                    role.setAttribute( skConstants.VALUE, roleText );

                }

                resp.appendChild( role );

                var tags = resources[ i ].getElementsByTagName( skConstants.ELMT_TAG );
                for ( j = 0; j < tags.length; j++ ) {

                    resp.appendChild( tags[ j ] );
                    j--;

                }

            }

            /*
             * ATTACHMENTS
             *
             */

            var attachments = cell.value.getElementsByTagName( skConstants.ATTACHMENT );
            if ( attachments && attachments.length ) {

                var atts = newDoc.createElement( skConstants.ATTACHMENTS );
                newValue.appendChild( atts );

                for ( i = 0; i < attachments.length; i++ ) {

                    var att2 = attachments[ i ];

                    // from <attachment type="text" value="att text"/>
                    // to <text value="..." />>
                    var newAtt = newDoc.createElement( att2.getAttribute( skConstants.ATTR_TYPE ) );

                    // remove type
                    att2.removeAttribute( skConstants.ATTR_TYPE );

                    // copies the other attributes (value, addr)
                    // http://stackoverflow.com/questions/6753362/jquery-how-to-copy-all-the-attributes-of-one-element-and-apply-them-to-another
                    var attributes = Array.prototype.slice.call( att2.attributes );
                    for ( j = 0; j < attributes.length; j++ ) {

                        newAtt.setAttribute( attributes[ j ].nodeName, attributes[ j ].nodeValue );

                    }

                    atts.appendChild( newAtt );

                }

            }

            cell.value = newValue;

        }

        /*
         * other
         *
         */

        else if ( cell.value.querySelector( skConstants.BOX ) == null ) {

            box = newDoc.createElement( skConstants.BOX );
            cell.value.appendChild( box );

            // will move http://devdocs.io/dom/node/appendchild
            box.appendChild( cell.value.getElementsByTagName( skConstants.TEXT )[ 0 ] );

        }

    }
    catch ( err ) {

        console.log( err );

    }

    };

    mxCodec.prototype.skConvertStyle = function skConvertStyleF( cell ) {

        if ( cell.id == 1 ) {

            cell.style = skConstants.title;

            return;

        }

        var oldName = {
            box_style_w: "wBox",
            box_style_y: "yBox",
            box_style_wDetailed: "wBoxDetailed",
            box_style_wGroup: "wBoxGroup",
            box_style_stickyNote: "stickyNote"
        };

        // Stickynotes
        if ( cell.style && cell.style.indexOf( oldName.box_style_stickyNote ) >= 0 ) {

            cell.style = cell.style.replace( oldName.box_style_stickyNote, skConstants.stickynote );

        }

        // Whybox
        else if ( cell.style && cell.style.indexOf( oldName.box_style_y ) >= 0 ) {

            cell.style = skConstants.whybox;

        }

        // Group
        else if ( cell.style && cell.style.indexOf( oldName.box_style_wGroup ) >= 0 ) {

            cell.style = skConstants.group;

        }

        // Detailed
        else if ( cell.style && cell.style.indexOf( oldName.box_style_wDetailed ) >= 0 ) {

            cell.style = skConstants.whatboxdetailed;

        }

        // Whatbox
        else if ( cell.style && cell.style.indexOf( oldName.box_style_w ) >= 0 ) {

            cell.style = skConstants.whatbox;

        }

        /*
         * Lines -- add "port" supports: remove w2y or y2w and add entryX &co
         * todo : for stickies ?
         *
         */

         else if ( cell.isEdge() ) {

            if ( cell.style == skConstants.LINE_WY2S  ) {

                cell.style = "" + skConstants.LINE_WY2S + ";exitX=1;exitY=0.5;entryX=0;entryY=0.5;";

            } else if ( cell.style === "w2y" || cell.style == "y2w" || cell.style == void 0 ) {

                cell.style = "exitX=1;exitY=0.5;entryX=0;entryY=0.5;";

            }

        }

    };

    var mxCodecprototypedecodeCell = mxCodec.prototype.decodeCell;

    mxCodec.prototype.decodeCell = function decodeCellF() {

        var cell = mxCodecprototypedecodeCell.apply( this, arguments );

        if ( cell ) {

            this.skConvertStyle( cell );

            if ( cell.value ) {

                this.skConvertValue( cell );

            }

            // remove visible Rect on load for older file
            if ( cell.visibleRect ) {

                cell.visibleRect = null;

            }

            if ( cell.isWhatbox() ) {

                cell.setCollapsed( true );

            }

        }

        return cell;

    };

} );
