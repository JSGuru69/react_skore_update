
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEvent = require( "mxEvent" );
    var mxUtils = require( "mxUtils" );
    var skUtils = require( "skUtils" );

	function skFileSaver( graphManager ) {

        /*jshint validthis:true*/
        this.graphManager = graphManager;

        this.graphManager.addListener( "newGraphCreated", mxUtils.bind( this, function( sender, event ) {

            this.enableForGraph( event.getProperty( "graph" ) );

        } ) );

        if ( WEB_MODE ) {

            var that = this;
            this.save = skUtils.debounce( function() {

                console.log( "save, debounced" );

                that.saveNow();

            }, 4000 );

        }

	}

    skFileSaver.prototype.graphManager = null;

    skFileSaver.prototype.save = function() {

        return null;

    };
    skFileSaver.prototype.saveNow = function( ) {

        if ( this.graphManager.getCurrentGraph().isEnabled() ) {

            $( ".skSaveNow" ).css( { "color": "" } ).html( "Saving..." );

            window.parent.postMessage( JSON.stringify( {

                name: "saveContent",

                data: {

                    image: "", //(new skThumbnail( )).createDataURL( graph ),

                    xml: mxUtils.getPrettyXml( this.graphManager.getCurrentGraph().getGraphXml() )
                }
            } ), "*" );

        }

    };

    skFileSaver.prototype.sendContentToSave = function( force ) {

        console.log( "sendContentToSave, force : ", force );

        if ( force ) {

            this.saveNow();

        } else {

            this.save();

        }

    };

    skFileSaver.prototype.setUpEnterprise = function() {

        var that = this;

        $( ".skSaveNow" ).show();

        $( ".skSaveNow" ).on( "click", function() {

            that.sendContentToSave( true );

        } );

        function messageListener( event ) {

            console.log( "messageListener : ", event.data );

            if ( event.data === "saveContent" ) {

                that.sendContentToSave( true );

            }

            if ( event.data === "contentSaved" ) {

                $( ".skSaveNow" ).html( "Saved" );

                setTimeout( function() {

                    $( ".skSaveNow" ).html( "Save" );

                }, 500 );

            }

        }

        window.addEventListener( "message", messageListener );

        window.setTimeout( function() {

            console.log( "save every minute... " );

            that.sendContentToSave();

        }, 60000 );

    };

    skFileSaver.prototype.enableForGraph = function( graph ) {

        var that = this;

        /*
         * auto save on change and once per minute
         *
         */

        graph.getModel().addListener( mxEvent.CHANGE, function( sender, evt ) {

            var changes = evt.getProperty( "edit" ).changes;

            if ( changes.length ) {

                console.log( changes );

                $( ".skSaveNow" ).html( "Save" );

                that.sendContentToSave( false );

            }

        } );

    };

	return skFileSaver;

} );
