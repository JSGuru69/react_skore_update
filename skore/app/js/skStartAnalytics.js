define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skAnalytics = require( "skAnalytics/skAnalytics" );
    var skShell = require( "skShell" );
    var skStatus = require( "skNavigatorUI/skStatus" );

    require( "skGraph/skGraphCellUtil" );
    require( "skGraphIO/skGraphIO_xml" );

    var graph;

    // waits for the parent's graph
    window.setupGraphInFrame = function setupGraphInFrameF( graphFromParent, allowSave, ELECTRON, requireNode ) {

        console.log( "setupGraph", graphFromParent );

        skShell.allowSave = allowSave;
        graph = graphFromParent;

        if ( ELECTRON ) {

            window.ELECTRON = true;
            window.requireNode = requireNode;

        }

    };

    // tell the parent we're ready
    window.parent.setupGraphFromParent();

    skShell.allowSave = false;

    if ( ELECTRON || DEV ) {

        skShell.allowSave = true;

    }

    if ( graph ) {

        skShell.analytics = new skAnalytics( graph );
        skShell.analytics.init();

    } else {

        $( ".skAnalyticsLoading" ).html( "no graph received :-( " );

    }

    skShell.skStatus = new skStatus( skShell );

    // window.textEditing = function( evt ) {

    //     var $ = window.jQuery;

    //     evt = evt || window.event;

    //     var allowed = false;

    //     if ( evt ) {

    //         evt = $.event.fix( evt );

    //         allowed = $( evt.target ).hasClass( "selectionAllowed" ) || $( evt.target ).parent( ".selectionAllowed" ).length > 0;

    //     }

    //     if ( ELECTRON ) {

    //         window.requireNode( "electron" ).ipcRenderer.send( "disableWhileTypingText", "allowed", !allowed );

    //     }

    //     return allowed;

    // } ;

    // document.onselectstart = window.textEditing;
    // document.onmousedown = window.textEditing;
    // document.oncontextmenu = window.textEditing;

} );
