define( [ "Injector" ], function( Injector ) {

    "use strict";

    describe( "skStylesheet", function() {

        var injector = new Injector();

        beforeAll( injector.reset );

        var mxEdgeStyle;
        var mxUtils;
        var skStylesheet;

        beforeAll( injector.run(
            function( _mxEdgeStyle /*mxEdgeStyle*/,
                      _mxUtils /*mxUtils*/,
                      _skStylesheet /*skGraph/skStylesheet*/) {

                mxEdgeStyle = _mxEdgeStyle;
                mxUtils = _mxUtils;
                skStylesheet = _skStylesheet;

            }
        ) );

        it( "defines styles", function() {

            function TestingStylesheet() {}
            mxUtils.extend( TestingStylesheet, skStylesheet );

            // Create an object and setup spies
            var stylesheet = new TestingStylesheet();
            var edgeStyle = {};
            var vertexStyle = {};

            spyOn( stylesheet, "putCellStyle" );
            spyOn( stylesheet, "getDefaultEdgeStyle" ).and.returnValue( edgeStyle );
            spyOn( stylesheet, "getDefaultVertexStyle" ).and.returnValue( vertexStyle );

            // Call constructor
            skStylesheet.call( stylesheet );

            expect( stylesheet.putCellStyle ).toHaveBeenCalledWith( "whybox", {
                shape: "whybox",
                verticalAlign: "top"
            } );
            expect( stylesheet.putCellStyle ).toHaveBeenCalledWith( "whatbox", {
                shape: "whatbox"
            } );
            expect( stylesheet.putCellStyle ).toHaveBeenCalledWith( "whatboxdetailed", {
                shape: "whatboxdetailed"
            } );
            expect( stylesheet.putCellStyle ).toHaveBeenCalledWith( "stickynote", {
                fillColor: "#FCF393",
                gradientColor: "#D4CA57",
                strokeColor: "#fff",
                strokeWidth: 0,
                shadow: false,
                opacity: 60,
                fontFamily: "Open Sans, sans-serif",
                verticalAlign: "top",
                align: "left"
            } );
            expect( stylesheet.putCellStyle ).toHaveBeenCalledWith( "group", {
                verticalLabelPosition: "top",
                verticalAlign: "bottom",
                align: "left",
                fillColor: "none",
                strokeColor: "#C0C0C0"
            } );
            expect( stylesheet.putCellStyle ).toHaveBeenCalledWith( "y2w", {
                endArrow: "classic"
            } );
            expect( stylesheet.putCellStyle ).toHaveBeenCalledWith( "w2y", {
                endArrow: "none",
                endFill: "0"
            } );
            expect( stylesheet.putCellStyle ).toHaveBeenCalledWith( "wy2s", {
                endArrow: "none",
                endFill: "0",
                strokeColor: "#067C7E",
                strokeWidth: 2,
                opacity: 50,
                edgeStyle: mxEdgeStyle.OrthConnector,
                curved: true,
                exitPerimeter: true,
                entryX: null,
                entryY: null,
                exitX: null,
                exitY: null
            } );

            expect( edgeStyle ).toEqual( {
                rounded: true,
                edgeStyle: mxEdgeStyle.OrthConnector,
                strokeColor: "#067C7E",
                labelBackgroundColor: "white",
                fontColor: "black",
                fontSize: "10",
                endSize: "11",
                strokeWidth: 1,
                exitX: 1,
                exitY: 0,
                exitDx: 0,
                exitDy: 40,
                exitPerimeter: 0,
                entryX: 0,
                entryY: 0,
                entryDx: 0,
                entryDy: 40,
                entryPerimeter: 0
            } );

            expect( vertexStyle ).toEqual( {
                fillColor: "transparent",
                strokeWidth: 1,
                verticalAlign: "top"
            } );

        } );

    } );

} );
