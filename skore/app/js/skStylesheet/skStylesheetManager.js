/**
 * Constructs a new report dialog
 */
define( function( require ) {

    "use strict";

    var skConstants = require( "skConstants" );
    var skUtils = require( "skUtils" );

    /**
     * @constructor
     */
    function skStylesheetManager( stylesheetNode ) {

        /*
         * "internal" role manager is reset every time
         *
         */

        /*jshint validthis:true*/
        this.stylesheetNode = stylesheetNode;
        this.cellsType = [];

        this._nameToNode = [];

        this.loadExisting();

    }

    skStylesheetManager.prototype.loadExisting = function loadExistingF() {

        // First, loads the existing roles into the array
        var styles = this.stylesheetNode.getElementsByTagName( skConstants.style );

        for ( var i = 0, l = styles.length; i < l; i += 1 ) {

            var node = styles[ i ];
            var cellType = skUtils.readValue( node );

            // Should it be trimmed?

            if ( !this.getStyleNode( cellType ) ) {

                // Adds to arrays
                this.cellsType.push( cellType );
                this._nameToNode[ cellType ] = node;

            }

            // Else duplicate in xml, how to handle that?

        }

    };
    skStylesheetManager.prototype.stylesheetNode = null;
    skStylesheetManager.prototype.cellsType = null;

    skStylesheetManager.prototype.getStyleNode = function getStyleNodeF( cellType ) {

        return Object.prototype.hasOwnProperty.call( this._nameToNode, cellType ) ? this._nameToNode[ cellType ] : null;

    };

    skStylesheetManager.prototype.getStyle = function getStyleF( cellType ) {

        var styleNode = this.getStyleNode( cellType );

        if ( styleNode ) {

            return styleNode.getAttribute( skConstants.styleTokens );

        } else {

            return "";

        }

    };

    /*
     * Add style for the cell
     *
     */

    skStylesheetManager.prototype.addStyle = function addStyleF( cellType, style ) {

        console.log( "skStylesheetManager.addStyle", cellType, style );

        var node = this.getStyleNode( cellType );

        if ( !cellType ) {

            return;

        }

        if ( !node ) {

            // Adds to array
            this.cellsType.push( cellType );

            // Adds to model
            // var stylesheet = this.initModel();

            node = this.stylesheetNode.appendChild( this.stylesheetNode.ownerDocument.createElement( skConstants.style ) );
            node.setAttribute( skConstants.VALUE, cellType );

            // Add to hash
            this._nameToNode[ cellType ] = node;

        }

        if ( !style || style.trim() === "" ) {

            style = "";

        }

        node.setAttribute( skConstants.styleTokens, style );

        return node;

    };

    skStylesheetManager.prototype.removeStyle = function removeStyleF( cellType ) {

        // Remove from array
        var ind = this.cellsType.indexOf( cellType );

        if ( ind > -1 ) {

            this.cellsType.splice( ind, 1 );

        }

        var node = this.getStyleNode( cellType );

        if ( node ) {

            // Remove from the model
            node.parentNode.removeChild( node );
            delete this._nameToNode[ cellType ];

        }

    };

    return skStylesheetManager;

} );
