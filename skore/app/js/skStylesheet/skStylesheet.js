
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxConstants = require( "mxConstants" );
    var mxEdgeStyle = require( "mxEdgeStyle" );
    var mxEventObject = require( "mxEventObject" );
    var mxStylesheet = require( "mxStylesheet" );
    var mxUtils = require( "mxUtils" );
    var skColors = require( "skGraph/skColors" );
    var skConstants = require( "skConstants" );
    var skStylesheetsManager = require( "skStylesheet/skStylesheetsManager" );

    function skStylesheet( graph ) {

        /*jshint validthis:true*/
        this.graph = graph;
        mxStylesheet.call( this );

        this.resetStyleSheet();

    }

    mxUtils.extend( skStylesheet, mxStylesheet );

    skStylesheet.prototype.graph = null;
    skStylesheet.prototype.resetStyleSheet = function resetStyleSheetF() {

        /*
         * Boxes
         *
         */
        var style;

        style = this.getDefaultVertexStyle();
        style[ mxConstants.STYLE_FILLCOLOR ] = "transparent";
        style[ mxConstants.STYLE_STROKEWIDTH ] = skConstants.STROKE_BOX;
        style[ mxConstants.STYLE_VERTICAL_ALIGN ] = mxConstants.ALIGN_TOP;
        style[ mxConstants.STYLE_OPACITY ] = 100;
        style[ skConstants.ATT_COLOR ] = "#067C7E";
        style[ skConstants.DETAIL_COLOR ] = "#067C7E";
        style[ mxConstants.STYLE_STROKECOLOR ] = "#6482B9";
        style[ mxConstants.STYLE_FONTCOLOR ] = "#774400";
        style[ mxConstants.STYLE_RESIZABLE ] = false;
        style[ mxConstants.STYLE_DASHED ] = "0";

        // style[ mxConstants.LINK_COLOR ] = "#008cba";
        style[ mxConstants.STYLE_SPACING ] = 0;
        style[ mxConstants.STYLE_FONTSIZE ] = 13;

        // style[ mxConstants.STYLE_LABEL_WIDTH ] = "186";
        style.whoWhatLine = "0"; // line between what and who

        /*
         * Why box
         *
         */

        var yBoxStyle = {};

        yBoxStyle[ mxConstants.STYLE_SHAPE ] = skConstants.whybox;
        yBoxStyle[ mxConstants.STYLE_VERTICAL_ALIGN ] = mxConstants.ALIGN_TOP;

        yBoxStyle[ mxConstants.STYLE_OVERFLOW ] = "visible"; // problem : by default the text will not be v-centered because no height
        // yBoxStyle[ mxConstants.STYLE_OVERFLOW ] = "fill"; // problem: text overflow will be hidden
        // yBoxStyle[ mxConstants.STYLE_OVERFLOW ] = "fill"; // problem: text overflow will be hidden

        yBoxStyle[ skConstants.WIDTH ] = skConstants.YB_WIDTH;
        yBoxStyle[ skConstants.HEIGHT ] = skConstants.YB_HEIGHT;
        var s = $.extend( {}, style, yBoxStyle );

        this.putCellStyle( skConstants.whybox, s );
        this.putCellStyle( "sys-" + skConstants.whybox, s );
        s = null;

        /*
         * What Box
         *
         */

        var wBoxStyle = {};

        wBoxStyle[ mxConstants.STYLE_SHAPE ] = skConstants.whatbox;
        wBoxStyle[ mxConstants.STYLE_OVERFLOW ] = "fill";
        wBoxStyle[ skConstants.WIDTH ] = skConstants.WB_WIDTH;
        wBoxStyle[ skConstants.HEIGHT ] = skConstants.WB_HEIGHT;
        wBoxStyle[ mxConstants.STYLE_ROUNDED ] = "0";

        s = $.extend( {}, style, wBoxStyle );
        this.putCellStyle( skConstants.whatbox, s );
        this.putCellStyle( "sys-" + skConstants.whatbox, s );
        s = null;

        var wBoxDetailedStyle = {};

        wBoxDetailedStyle[ mxConstants.STYLE_SHAPE ] = skConstants.whatbox;
        s = $.extend( {}, style, wBoxDetailedStyle );
        this.putCellStyle( skConstants.whatboxdetailed, s );
        this.putCellStyle( "sys-" + skConstants.whatboxdetailed, s );
        s = null;

        /*
         * Sticky Note
         *
         */

        var stickyNoteStyle = {};

        stickyNoteStyle[ mxConstants.STYLE_FILLCOLOR ] = "#FCF393";

        // StickyNoteStyle[mxConstants.STYLE_GRADIENTCOLOR] = "#D4CA57";
        stickyNoteStyle[ mxConstants.STYLE_STROKECOLOR ] = "transparent";
        stickyNoteStyle[ mxConstants.STYLE_STROKEWIDTH ] = 1;
        stickyNoteStyle[ mxConstants.STYLE_SHADOW ] = false;
        stickyNoteStyle[ mxConstants.STYLE_OPACITY ] = 60;
        stickyNoteStyle[ mxConstants.STYLE_RESIZABLE ] = true;
        stickyNoteStyle[ mxConstants.STYLE_ALIGN ] = mxConstants.ALIGN_LEFT;
        s = $.extend( {}, style, stickyNoteStyle );
        this.putCellStyle( skConstants.stickynote, s );
        this.putCellStyle( "sys-" + skConstants.stickynote, s );

        /// style of the group
        var wBoxGroup = {};

        wBoxGroup[ mxConstants.STYLE_VERTICAL_LABEL_POSITION ] = mxConstants.ALIGN_TOP;
        wBoxGroup[ mxConstants.STYLE_VERTICAL_ALIGN ] = mxConstants.ALIGN_BOTTOM;
        wBoxGroup[ mxConstants.STYLE_ALIGN ] = mxConstants.ALIGN_LEFT;
        wBoxGroup[ mxConstants.STYLE_FILLCOLOR ] = "transparent";
        wBoxGroup[ mxConstants.STYLE_STROKECOLOR ] = skColors.BOX_GROUP;
        wBoxGroup[ mxConstants.STYLE_STROKEWIDTH ] = 1;
        wBoxGroup[ mxConstants.STYLE_FONTSIZE ] = 20;
        wBoxGroup[ mxConstants.STYLE_RESIZABLE ] = true;

        s = $.extend( {}, style, wBoxGroup );
        this.putCellStyle( skConstants.group, s );
        this.putCellStyle( "sys-" + skConstants.group, s );

        /*
         * images
         *
         */

        var wBImage = {};
        wBImage[ mxConstants.STYLE_RESIZABLE ] = 1;

        s = $.extend( {}, style, wBImage );
        this.putCellStyle( skConstants.image, s );
        this.putCellStyle( "sys-" + skConstants.image, s );

        /*
         * Lines
         *
         */

        style = this.getDefaultEdgeStyle();
        style[ mxConstants.STYLE_ROUNDED ] = "1";
        style[ mxConstants.STYLE_EDGE ] = mxEdgeStyle.OrthConnector;
        style[ mxConstants.STYLE_STROKECOLOR ] = skColors.line;
        style[ mxConstants.STYLE_LABEL_BACKGROUNDCOLOR ] = "white";
        style[ mxConstants.STYLE_FONTCOLOR ] = "black";

        // style[ mxConstants.STYLE_FONTSIZE ] = "10";
        style[ mxConstants.STYLE_ENDSIZE ] = 11;
        style[ mxConstants.STYLE_STROKEWIDTH ] = skConstants.STROKE_LINE;
        style[ mxConstants.STYLE_OPACITY ] = 100;
        style[ mxConstants.STYLE_DASHED ] = "0";

        style[ mxConstants.STYLE_ENDFILL ] = "0";
        style[ mxConstants.STYLE_ENDARROW ] = mxConstants.ARROW_CLASSIC;

        style[ mxConstants.STYLE_EXIT_PERIMETER ] = "0"; // Does NOT look for the "shortest connection"

        style[ mxConstants.STYLE_ENTRY_PERIMETER ] = "0"; // Does NOT look for the "shortest connection"
        this.putCellStyle( "sys-line", style );

        // Style of the line wy2s (what/why to stickyNote)
        var wy2sStyle = {};

        wy2sStyle[ mxConstants.STYLE_ENDARROW ] = "none";
        wy2sStyle[ mxConstants.STYLE_ENDFILL ] = "0";
        wy2sStyle[ mxConstants.STYLE_DASHED ] = "0";

        wy2sStyle[ mxConstants.STYLE_STROKECOLOR ] = skColors.line; // "#FCF393";
        wy2sStyle[ mxConstants.STYLE_STROKEWIDTH ] = 2;

        wy2sStyle[ mxConstants.STYLE_OPACITY ] = 50;
        wy2sStyle[ mxConstants.STYLE_EDGE ] = mxEdgeStyle.OrthConnector;
        wy2sStyle[ mxConstants.STYLE_CURVED ] = "1";
        wy2sStyle[ mxConstants.STYLE_EXIT_PERIMETER ] = "1";

        this.putCellStyle( skConstants.LINE_WY2S, wy2sStyle );
        this.putCellStyle( "sys-" + skConstants.LINE_WY2S, wy2sStyle );

    };

    // returns the style of the given type, if cells is specified this will be taken into account
    skStylesheet.prototype.buildCustomStyle = function buildCustomStyleF( cellType, styleSheetName, cells ) {

        // Builds the custom style
        var customStyleUsed = "";

        if ( cells ) {

            if ( cells[ 0 ].isEdge() ) {

                customStyleUsed = "defaultEdge;" + cells[ 0 ].style + ";";

            } else {

                customStyleUsed = cells[ 0 ].style + ";";

            }

        } else {

            customStyleUsed = cellType + ";" + this.graph.stylesheetsManager.getStyle( styleSheetName, cellType );

        }

        // If ( cellType == "line" || cellType == skConstants.LINE_WY2S) {}

        var myDefault = this.styles[ "sys-" + cellType ];

        return this.getCellStyle( customStyleUsed, myDefault );

    };

    /*
     * function to load the stylesheet to use in the current graph
     *
     * - if NO CONTENT (blank canvas)
     *      - WEB : load system stylehseet (i.e. do nothing)
     *      - ELECTRON : load saved stylesheet if any, or system
     * - if CONTENT
     *      - has stylesheet "tag"
     *          - load system and override with stylesheet in the file
     *      - has no stylesheet tag
     *          - load system stylehseet
     *
     */

    skStylesheet.prototype.customHasBeenLoaded = false;

    skStylesheet.prototype.loadCustomStylesheet = function loadCustomStylesheetF( content, forceRefresh ) {

        // To avoid a round trip
        if ( this.customHasBeenLoaded ) {

            this.resetStyleSheet();

        }

        console.log( "skStylesheet.loadCustomStylesheet", content );

        this.graph.stylesheetsManager = new skStylesheetsManager( this.graph );

        // skShell.StylesheetsManager = new skStylesheetsManager( graph );

        // NO content + ELECTRON = load saved stylesheet
        if ( ELECTRON && !content ) {

            var fsj = window.requireNode( "fs-jetpack" );
            var app = window.requireNode( "electron" ).remote.app;
            var userDataDir = fsj.cwd( app.getPath( "userData" ) );
            var savedStylesheet = userDataDir.read( "stylesheets.xml" );

            if ( savedStylesheet ) {

                var xml = mxUtils.parseXml( savedStylesheet );
                var node =  mxUtils.importNode( xml, xml.getElementsByTagName( "stylesheets" )[ 0 ], true );

                this.graph.model.root.stylesheets = node;

                this.graph.stylesheetsManager = new skStylesheetsManager( this.graph );

                // skShell.StylesheetsManager = new skStylesheetsManager( graph );

            }

        }

        // if ( ELECTRON && force === true ) {

        //     // Try to load the system one

        //     var fsj = window.requireNode( "fs-jetpack" );
        //     var app = window.requireNode( "electron" ).remote.app;
        //     var userDataDir = fsj.cwd( app.getPath( "userData" ) );
        //     var stylesheet = userDataDir.read( "stylesheets.xml" );

        //     if ( stylesheet ) {

        //         var xml = mxUtils.parseXml( stylesheet );
        //         var node =  mxUtils.importNode( xml, xml.getElementsByTagName( "stylesheets" )[ 0 ], true );

        //         graph.model.root.stylesheets = node;

        //         skShell.StylesheetsManager = new skStylesheetsManager( graph );

        //     } else if ( force === true ) {

        //         console.log( "no saved stylesheet found" );

        //         // SkShell.setStatus("No saved stylesheet found");

        //     }

        // }

        // Wb + wbdetailed
        this.putCellStyle( skConstants.whatbox, this.buildCustomStyle( skConstants.whatbox, "default", null ) );
        this.putCellStyle( skConstants.whatboxdetailed, this.buildCustomStyle( skConstants.whatbox, "default", null ) );

        // Yb
        this.putCellStyle( skConstants.whybox, this.buildCustomStyle( skConstants.whybox, "default", null ) );

        //Line
        this.putDefaultEdgeStyle( this.buildCustomStyle( "line", "default", null ) );

        // Sticky
        this.putCellStyle( skConstants.stickynote, this.buildCustomStyle( skConstants.stickynote, "default", null ) );

        // Wy2s
        this.putCellStyle( skConstants.LINE_WY2S, this.buildCustomStyle( skConstants.LINE_WY2S, "default", null ) );

        this.putCellStyle( skConstants.group, this.buildCustomStyle( skConstants.group, "default", null ) );

        if ( this.graph && forceRefresh === true ) {

            this.graph.refresh();

        }

        this.customHasBeenLoaded = true;

    };

    var applyStyleToLine = function applyStyleToLineF( line, styleString ) {

        var currentStyle = line.style;

        // Keep the stylename + entryX & co.
        var toKeep = "";

        if ( currentStyle !== null ) {

            var tokens = currentStyle.split( ";" );

            for ( var j = 0; j < tokens.length; j++ ) {

                // Keeps the stylename (no "=") and the entryX & co.

                if ( tokens[ j ].indexOf( "=" ) == -1 ||
                     tokens[ j ].trim() !== "" && (
                     tokens[ j ].indexOf( "_" ) >= 0 ||
                     tokens[ j ].indexOf( "entry" ) >= 0 ||
                     tokens[ j ].indexOf( "exit" ) >= 0 ) ) {

                    toKeep += tokens[ j ] + ";";

                }

            }

        }

        line.style = "" + toKeep + ";" + styleString;

    };

    var applyStyleToStickyNote = function( cell, styleString ) {

        var currentStyle = cell.style;

        // Keep the stylename + entryX & co.
        var toKeep = "";

        if ( currentStyle !== null ) {

            var tokens = currentStyle.split( ";" );

            for ( var j = 0; j < tokens.length; j++ ) {

                // Keeps the stylename and the entryX & co.

                if ( tokens[ j ].trim() !== "" && tokens[ j ].indexOf( mxConstants.STYLE_FILLCOLOR ) >= 0 ) {

                    toKeep += tokens[ j ] + ";";

                }

            }

        }

        cell.style = skConstants.stickynote + ";" + toKeep + ";" + styleString;

    };

    skStylesheet.prototype.applyStyleToCells = function( cells, styleAsString ) {

        cells.forEach( function( cell ) {

            if ( cell.isWhatboxDetailed() ) {

                cell.style = skConstants.whatboxdetailed + ";" + styleAsString;

            } else if ( cell.isEdge() ) {

                applyStyleToLine( cell, styleAsString );

            } else if ( cell.isStickynote() ) {

                applyStyleToStickyNote( cell, styleAsString );

            } else {

                cell.style = cell.getCellType() + ";" + styleAsString;

            }

        } );

        this.graph.refresh();

    };

    skStylesheet.prototype.saveStyleSheet = function( styleStringForCells, styleSheetName, cellsToChange ) {

        styleSheetName = styleSheetName || "default";

        var cellTypes = Object.keys( styleStringForCells );

        var that = this;

        var stylesheetsManager =  this.graph.stylesheetsManager; //this.navigatorUI.getCurrentGraph().stylesheetsManager;

        // first per cells
        cellTypes.forEach( function( cellType ) {

            var systemStyleSheet = that.getCellStyle( "sys-" + cellType );
            var styleObj = styleStringForCells[ cellType ];

            // prepare the strings to be saved

            var styleStringRaw = ""; // will contain everything to be written on relevant cells
            var styleString = ""; // will contain only difference to be written on graph when relevant
            var styleAttrs = Object.keys( styleObj );
            styleAttrs.forEach( function( attr ) {

                styleStringRaw += attr + "=" + styleObj[ attr ] + ";";

                if ( systemStyleSheet[ attr ] && styleObj[ attr ] !== systemStyleSheet[ attr ] ) {

                    styleString += attr + "=" + styleObj[ attr ] + ";";

                }

            } );

            // writes only for the cells (when cellsToChange is there)
            if ( cellsToChange && Object.keys( cellsToChange ).includes( cellType ) && cellsToChange[ cellType ].length ) {

                that.applyStyleToCells( cellsToChange[ cellType ], styleStringRaw );

            }

            // writes the stylesheet
            else {

                // we save in the actual style so the current graph is fine
                var s = $.extend( {}, systemStyleSheet[ cellType ], styleObj );
                switch ( cellType ) {

                    case "line":

                        that.putCellStyle( "defaultEdge", s );

                    break;
                    case skConstants.whatbox:

                        // special case for whatbox we must write for WHATBOXDETAILED as well
                        // and for the normal one (so no "break;" statement)
                        that.putCellStyle( skConstants.whatboxdetailed, s );

                    /* falls through */
                    default:

                        that.putCellStyle( cellType, s );

                    break;
                }

                stylesheetsManager.updateStyleSheet( styleSheetName, cellType, styleString );

            }

        } );

        this.graph.fireEvent( new mxEventObject( "styleSheetUpdated", "graph", this.graph ) );

    };

    return skStylesheet;

} );
