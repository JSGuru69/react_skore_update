
/**
 * Constructs a new report dialog
 */
define( function( require ) {

    "use strict";

    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skStylesheetManager = require( "skStylesheet/skStylesheetManager" );
    var skUtils = require( "skUtils" );

    /**
     * @constructor
     */
    function skStylesheetsManager( graph ) {

        console.log( "new skStylesheets (plural) Manager", graph );

        /*
         * "internal" stylesheets (plural) manager is reset every time
         *
         */

        /*jshint validthis:true*/
        this.graph = graph;

        this.stylesheets = [];

        this._nameToNode = [];

        this.loadExisting();

    }

    skStylesheetsManager.prototype.stylesheets = null;
    skStylesheetsManager.prototype.graph = null;

    skStylesheetsManager.prototype.loadExisting = function loadExistingF() {

        var styleSheets = this.initModel().getElementsByTagName( skConstants.stylesheet );

        for ( var i = 0, l = styleSheets.length; i < l; i += 1 ) {

            var stylesheetName = skUtils.readValue( styleSheets[ i ] );

            this.stylesheets.push( stylesheetName );
            this._nameToNode[ stylesheetName ] = styleSheets[ i ];
            var ss = new skStylesheetManager( styleSheets[ i ] );

            ss = null;

        }

    };

    skStylesheetsManager.prototype.initModel = function initModelF() {

        if ( !this.graph.model.root.stylesheets ) {

            this.graph.model.root.stylesheets = mxUtils.createXmlDocument().createElement( skConstants.stylesheets );

        }

        return this.graph.model.root.stylesheets;

    };

    skStylesheetsManager.prototype._getStylesheetNode = function _getStylesheetNodeF( stylesheetName ) {

        return Object.prototype.hasOwnProperty.call( this._nameToNode, stylesheetName ) ? this._nameToNode[ stylesheetName ] : null;

    };

    /*
     * Adds the role to the Array AND the model (if does not exist)
     *
     */

    skStylesheetsManager.prototype.addStylesheet = function addStylesheetF( stylesheetName ) {

        // Trim trailing space for consistency
        // roleName = roleName.trim();

        if ( !stylesheetName ) {

            return;

        }

        var node = this._getStylesheetNode( stylesheetName );

        if ( !node ) {

            // Adds to array
            this.stylesheets.push( stylesheetName );

            // Adds to model
            var stylesheets = this.initModel();

            node = stylesheets.appendChild( stylesheets.ownerDocument.createElement( skConstants.stylesheet ) );
            node.setAttribute( skConstants.VALUE, stylesheetName );

            // Node.setAttribute("style", style);

            // Add to hash
            this._nameToNode[ stylesheetName ] = node;

        }

        return new skStylesheetManager( node );

    };

    skStylesheetsManager.prototype.removeStylesheet = function removeStyleF( stylesheetName ) {

        // Remove from array
        var ind = this.stylesheets.indexOf( stylesheetName );

        if ( ind > -1 ) {

            this.stylesheets.splice( ind, 1 );

        }

        var node = this.getSylesheetNode( stylesheetName );

        if ( node ) {

            // Remove from the model
            node.parentNode.removeChild( node );
            delete this._nameToNode[ stylesheetName ];

        }

    };

    /*
     * For styleSheet
     *
     */

    skStylesheetsManager.prototype.updateStyleSheet = function updateStyleSheetF( stylesheetName, cellType, style ) {

        if ( !stylesheetName || !cellType ) {

            console.log( "stylesheet name / celltype needed", stylesheetName, cellType );

            return;

        }

        // Find the stylesheet
        var ss = this.addStylesheet( stylesheetName );

        // Find & update the style for cell type
        return ss.addStyle( cellType, style );

        //Done

    };

    skStylesheetsManager.prototype.getStyle = function getStyleF( stylesheetName, cellType ) {

        // Find the stylesheet
        var ss = this.addStylesheet( stylesheetName );

        // Find the style
        return ss.getStyle( cellType );

    };

    return skStylesheetsManager;

} );
