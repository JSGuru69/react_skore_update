
define( function( ) {

    "use strict";

    var c = {};

    c.OFFSET_EDITOR = 34;

    /*
     * Types of box
     *
     */

    c.title = "title";
    c.whatbox = "whatbox";
    c.whatboxdetailed = "whatboxdetailed";
    c.whybox = "whybox";
    c.stickynote = "stickynote";
    c.group = "group";
    c.highlightStyle = "cellhihglight";
    c.line = "line";
    c.image = "image";

    c.FLEX_ARROW = "flexArrow";

    /*
     * Types of lines
     *
     */

    c.LINE_WY2S = "wy2s";
    c.LINE_HIGHLIGHT = "linehighlight";
    c.ATT_COLOR = "attachmentColor";
    c.SURVEY_COLOR = "surveyColor";
    c.DETAIL_COLOR = "detailColor";
    c.LINK_COLOR = "linkColor";

    c.START_WIDTH = "startWidth";
    c.END_WIDTH = "endWidth";

    /*
     * Elements & attributes making the boxes
     *
     */

    c.TEXT = "text";
    c.BOX = "box";
    c.ATTACHMENTS = "attachments";
    c.ATTACHMENT = "attachment";
    c.SURVEY = "survey";
    c.RESOURCE = "resource";
    c.ROLE = "role";
    c.RESPONSIBILITIES = "responsibilities";
    c.RESPONSIBILITY = "responsibility";
    c.ATTR_SHOWN = "displayed";

    c.VALUE = "value";
    c.ATTR_TYPE = "type";

    /*
     * Styles
     *
     */

    c.WIDTH = "width";
    c.HEIGHT = "height";

    c.WB_WIDTH = 190;
    c.WB_HEIGHT = 80;

    c.YB_WIDTH = 120;
    c.YB_HEIGHT = 80;

    c.DIST_W2Y = 25;
    c.DIST_Y2W = 40;

    c.DIST_LINES = 80;

    c.STROKE_BOX = 1;

    // c.STROKE_BOX_HOVER = 2;
    c.STROKE_BOX_SELECTED = 2;
    c.STROKE_LINE = 1;
    c.STROKE_LINE_selected = 4;

    /*
     * Relation between cells
     *
     */

    c.REL_VISIBLE = "VISIBLE";
    c.REL_PARENT = "PARENT";
    c.REL_GRD_PARENT = "GRANDPARENT";
    c.REL_CHILD = "CHILD";
    c.REL_GRD_CHILD = "GRANDCHILD";
    c.REL_NONE = "NONE";
    c.REL_EQUAL = "SameSame";

    /*
     * Attachment
     *
     */

    c.url_type = "url";
    c.text_type = "text";
    c.url_address = "addr";
    c.gdrive_type = "gdrive";
    c.gdrive_address = "addr";

    /*
     * forms
     *
     */

    c.TEMPLATES = "templates";
    c.TEMPLATE = "template";
    c.TEMPLATE_NAME = "name";
    c.TEMPLATE_BASE = "base";
    c.TEMPLATE_DESCRIPTION = "description";
    c.TEMPLATE_CUSTOM = "custom";

    c.RESP_RACI = "raci";
    c.RESP_RATSI = "ratsi";

    c.EXT_CONTENT = "externalcontent";

    /*
     * Extensions
     *
     */

    c.ELMT_TAG = "tag";

    c.HANDHOVER_HIGHLIGHT = "handoverhighlight";
    c.MULTI_RESOURCE_TAG = "mrt";

    // For roles
    c.ROLES = "roles";
    c.ROLE = "role";
    c.ELMT_COMMENT = "comment";

    c.SOURCE_EMBED = "embed";
    c.SOURCE_JUSTSKOREIT = "justskoreit";
    c.SOURCE_FILE = "file";
    c.SOURCE_GDRIVE = "gdrive";
    c.SOURCE_NEW = "new";
    c.SOURCE_DEMOCONTENT = "demo";
    c.SOURCE_XMLEDIT = "xml";
    c.SRC_ENT = "SAAS";

    c.stylesheets = "stylesheets";
    c.stylesheet = "stylesheet";

    c.styleTokens = "styleTokens";
    c.style = "style";

    c.JUSTSKOREIT = "justSkoreIt";

    /*
     * NAVIGATION SOURCE
     *
     */

    c.NAV_BREADCRUMB = "navbreadcrumb";

    /*
     * ROLE MANAGER
     *
     */

    c.LOCAL_ID = "localid";
    c.REMOTE_ID = "remoteid";

    /*
     * just skore it
     *
     */

    c.justSkoreItBaseUrl = "https://justskore.it/?embed";
    c.justSkoreItBasUrlProd = c.justSkoreItBaseUrl;

    if ( DEV ) {

        c.justSkoreItBaseUrl = "http://localhost.me:8080/app/?embed";

    }

    /*
     * report
     *
     */

    c.SEPARATOR = " | ";

    /*
     *
     * page sizes
     *
     *
     */

    c.paperSizes = {
        A4: {
            name: "A4",
            displayName: "A4",
            width: 826,
            height: 1169

            // sizePortrait: mxConstants.PAGE_FORMAT_A4_PORTRAIT,
            // sizeLandscape: mxConstants.PAGE_FORMAT_A4_LANDSCAPE
        },
        A3: {
            name: "A3",
            displayName: "A3",
            width: 1169,
            height: 1652

            // sizePortrait: new mxRectangle( 0, 0, 1169, 1652 ),
            // sizeLandscape: new mxRectangle( 0, 0, 1652, 1169 )
        },
        Letter: {
            name: "Letter",
            displayName: "Letter (8.5 x 11)",
            width: 850,
            height: 1100
        },
        Legal: {
            name: "Legal",
            displayName: "Legal (8.5 x 14)",
            width: 850,
            height: 1403
        },
        Tabloid: {
            name: "Tabloid",
            displayName: "Tabloid / Ledger (11 x 17)",
            width: 1097,
            height: 1703
        }
    };

    return c;

} );
