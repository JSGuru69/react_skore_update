define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEvent = require( "mxEvent" );
    var overview = require( "./skAnalyticsOverview_page" );
    var reportPage = require( "./skAnalyticsReport_page" );
    var skAnalyticsRoles_page = require( "./skAnalyticsRoles_page" );
    var skGraphData = require( "./skGraphData" );
    var modal_edit = require( "skModal/skModal_editBox" );

    function skAnalytics( graph ) {

        console.log( "new skAnalytics" );

        /*jshint validthis:true*/
        var that = this;

        that.graph = graph;
        this.graphData = new skGraphData( graph );

        if ( DEV ) {

            window.graph = graph;
            window.analytics = this;

        }

        // $( document.body ).append( $( require( "text!./skAnalytics.html" ) ) );

        // Document.getElementsByClassName( "skAnalytics" )[ 0 ].onmousedown = window.textEditing;
        // document.getElementsByClassName( "skAnalytics" )[ 0 ].onselectstart = window.textEditing;

        this.modal_edit = new modal_edit( graph );

        mxEvent.disableContextMenu( document.getElementsByClassName( "skAnalytics" )[ 0 ] );

        $( document.body ).append( $( require( "text!./skAnalyticsRoles_roleDesc_modal.html" ) ) );

        $( ".skAnalyticsNavBtn" ).on( "click", function() {

            // UI stuffs
            $( ".skAnalyticsTab" ).hide();
            $( ".skAnalyticsNavBtn" ).removeClass( "active" );
            $( this ).addClass( "active" );

            // Shows the correct one
            var tabtoopen = $( this ).data( "tabtoopen" );

            $( "." + tabtoopen ).show( 0, function() {

                switch ( tabtoopen ) {

                    case "skAnalyticsRoles" :

                        that.skAnalyticsRoles_page = new skAnalyticsRoles_page( that.graph );

                        break;

                    case "skAnalyticsReporting" :

                        if ( that.reportPage ) {

                            that.reportPage.loadReport( that.graph );

                        } else {

                            that.reportPage = new reportPage( that.graph );

                        }

                        break;

                    default :

                        that.overview = new overview( that.graph, that.graphData );

                        break;

                }

            } ); // End show

        } ); // End click on nav button

    }

    skAnalytics.prototype.graph = null;
    skAnalytics.prototype.graphData = null;
    skAnalytics.prototype.roles = null;
    skAnalytics.prototype.reportPage = null;
    skAnalytics.prototype.overview = null;

    skAnalytics.prototype.init = function initF( ) {

        console.log( "skAnalytics.init" );

        this.overview = new overview( this.graph, this.graphData );

        $( ".skAnalyticsLoading" ).fadeOut().remove();

    };

    return skAnalytics;

} );
