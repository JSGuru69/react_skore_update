
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    /**
     * @constructor
     */
    function skAnalyticsReport_customizer( myReport ) {

        /*
         * Box type
         *
         */

        var ul = $( ".skCustoBoxTypes" );

        ul.empty().html( "<li class='list-group-item'>" +
            "<h4 class='list-group-item-heading'>Filter box type</h4><p class='list-group-item-text'>from the graph</p></li>" );
        var li = $( '<li class="list-group-item">' );

        ul.append( li );

        $.each( myReport.boxType, function( e ) {

            li.append( $( '<div class="checkbox" id="boxType-' + e + '" >' +
              "<label>" +
                '<input type="checkbox" value="" ' + ( myReport.boxType[ e ] ? "checked" : "" ) + ">" +
                e +
              "</label>" +
            "</div>" ) );

        } );

        $( ".checkbox", li ).on( "change", function() {

            myReport.boxType[ this.id.split( "-" )[ 1 ] ] = $( $( "input[type=checkbox]", $( this ) )[ 0 ] ).context.checked;

        } );

        /*
         * Columns
         *
         */

        ul = $( ".skCustoColumns" );
        ul.empty().html( "<li class='list-group-item'><h4 class='list-group-item-heading'>Select columns</h4></li>" );

        li = $( '<li class="list-group-item">' );
        ul.append( li );

        $.each( myReport.columnsProperties, function( i, e ) {

            if ( e.isSelectable.call( myReport ) ) {

                li.append( $( '<div class="checkbox" id="column-' + e.data + '" >' +
                    '<label data-toggle="tooltip" data-container="body" data-placement="bottom" title="' + e.description + '">' +
                        '<input type="checkbox" ' + ( myReport.columns[ e.data ] === true ? "checked" : "" ) + ">" +
                        e.title +
                    "</label>" +
                "</div>" ) );

            }

        } );

        $( ".checkbox", li ).on( "change", function() {

            var checkbox = this;
            var checked = $( $( "input[type=checkbox]", $( this ) )[ 0 ] ).context.checked;

            myReport.columns[ checkbox.id.split( "-" )[ 1 ] ] = checked;

            $( '.skCustoColumns [data-toggle="tooltip"]' ).tooltip( "hide" );

        } );

        /*
         * Relations
         *
         */

        // Ul = $("<div class='list-group'>");
        // d.append(ul);

        // Ul.append($("<li class='list-group-item'><h4>Relation to current view <small></small></h4></li>"));
        ul = $( ".skCustoRelations" );
        ul.empty().html( "<li class='list-group-item'><h4>Relation to current view <small></small></h4></li>" );
        $.each( myReport.relations, function( e ) {

            ul.append( $( "<div class='list-group-item'>" +
                "<div class='checkbox' id='relation-" + e + "' >" +
                    "<label><input type='checkbox' " + ( myReport.relations[ e ] ? "checked" : "" ) + "  >" + e + "</label></div></div>" ) );

        } );

        $( ".checkbox", ul ).on( "change", function() {

            myReport.relations[ this.id.split( "-" )[ 1 ] ] = $( $( "input[type=checkbox]", $( this ) )[ 0 ] ).context.checked;

            // MyReport.update();
            $( '.skCustoColumns [data-toggle="tooltip"]' ).tooltip( "hide" );

        } );

        $( '.skCustoColumns [data-toggle="tooltip"]' ).tooltip();

    }

    return skAnalyticsReport_customizer;

} );
