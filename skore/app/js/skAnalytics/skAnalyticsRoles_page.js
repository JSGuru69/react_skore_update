
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skAnalyticsRoles_roleDesc = require( "skAnalytics/skAnalyticsRoles_roleDesc" );
    var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );
    var skConstants = require( "skConstants" );

    /**
     * @constructor
     */
    function skAnalyticsRoles( graph ) {

        var rolesList = $( ".skRoleListPlaceholder" );

        rolesList.empty();

        var roleDesc = $( ".skRoleDesc" );

        roleDesc.empty();

        var openRoleDesc = function openRoleDescF( item, role ) {

            // Open thing for the role description
            $( "a", rolesList ).removeClass( "active" );
            $( item ).addClass( "active" );

            $( roleDesc ).empty();
            skShell.dialog_role = new skAnalyticsRoles_roleDesc( graph, role );
            $( roleDesc ).append( skShell.dialog_role );

        };

        // We sort the roles by alphabetical order
        // var sortedRoles = graph.roleManager.roles.sort( function( a, b ) {
        var sortedRoles = graph.roleManager.roleList.sort( function( a, b ) {

            return a.name.toLowerCase().localeCompare( b.name.toLowerCase() );

        } );

        // var whatboxesForRole = graphData.getBigData().whatboxesForRole;

        $.each( sortedRoles, function( i, role ) {

            // var wb = whatboxesForRole[ role ];

            // var classN = ( role === roleName ? "active" : "" );

            var myRole = $( "<a class='list-group-item'>" )
                    .append( $( "<span class='roleName'>" + skUtils.renderMarkdown( role.name, true ) + "</span>" ) )
                    .append( $( "<span class='badge' data-toggle='tooltip' data-placement='bottom' " +
                        "title='Count of activities where this role is assigned'>" +
                        ( role.boxes ? role.boxes.length : "0" ) +
                        "</span>" ) )
                    .on( "click", function() {

                        openRoleDesc( this, role );

                    } );

            $( rolesList ).append( myRole );

        } );

        // Get the first role if none is set

        if ( $( rolesList ).children()[ 0 ] ) {

            $( $( rolesList ).children()[ 0 ] ).click();

        } else {

            $( ".skRoleDesc" ).empty().html( "Select or add a role in the list on the left." );

        }

        if ( graph.isEnabled() ) {

            // Button: add a role
            rolesList.append( $( "<li class='list-group-item'><button id='skAddRoleButton' class='btn btn-default'>Add a role</button></li>" ) );

            $( "#skAddRoleButton", rolesList ).on( "click", function() {

                var myLi = $( this ).parent();

                // Hides the button
                $( this ).hide();

                // Adds the form instead
                var addRoleForm = $( "<div class='input-group'>" +
                                        "<input aria-describedby='newRoleHelpBlock' " +
                                            "type='text' id='skNewRoleInput' class='form-control selectionAllowed' " +
                                            "placeholder='New role name...'>" +
                                        "<span class='input-group-btn'>" +
                                            "<button id='skButtonAddRole' class='btn btn-default' type='button'>Add</button>" +
                                        "</span>" +
                                    "</div>" +
                                    '<span id="newRoleHelpBlock" class="help-block">Enter one role, or several roles separated by a comma.</span>' +
                                    "<p>" +
                                        "<a id='skButtonCancelNewRole'>Cancel</a>" +
                                    "</p>" );

                $( myLi ).append( addRoleForm );
                $( "#skNewRoleInput", addRoleForm ).focus();

                // Link event to button
                $( "#skButtonAddRole" ).on( "click", function() {

                    var roleNames = $( "#skNewRoleInput" ).val().split( "," );

                    $( roleNames ).each( function( i, roleName ) {

                        roleName = roleName.trim();

                        // And in the roleManager
                        var newlyCreatedRole = graph.roleManager.addRole( roleName, true );

                        // hack, but that does the job
                        var roleFull = {
                            localid: skUtils.readValue( newlyCreatedRole, skConstants.LOCAL_ID ),
                            name: skUtils.readValue( newlyCreatedRole ),
                            boxes: []
                        };
                        graph.roleManager.roleList.push( roleFull );

                        // Insert the new role in the list
                        var newLi = '<a class="list-group-item">' +
                                '<span class="roleName"></span>' +
                                '<span class="badge" ' +
                                    'data-toggle="tooltip" ' +
                                    'data-placement="bottom" ' +
                                    'title="Count of activities where this role is assigned">0</span>' +
                                "</a>";

                        $( ".roleName", newLi ).text( roleName );

                        newLi.insertBefore( $( myLi ) );

                        newLi.on( "click", function() {

                            openRoleDesc( this, roleFull );

                        } );

                        // As this is created manually we add a comment
                        graph.roleManager.setRoleComment( newlyCreatedRole, "Role added via role manager" );

                        // Removes the form
                        $( addRoleForm ).remove();

                        // Shows the button again
                        $( "#skAddRoleButton" ).show();

                    } );

                } );

                // Link cancel button
                $( "#skButtonCancelNewRole" ).on( "click", function() {

                    // Removes the form
                    $( addRoleForm ).remove();

                    // Shows the button again
                    $( "#skAddRoleButton" ).show();

                } );

            } );

        }

        // Enable the tooltips
        $( '[data-toggle="tooltip"]', rolesList ).tooltip();

        // filter
        $( "#skRoleFilter" ).on( "keyup", function() {

            var elem = $( ".skRoleListPlaceholder" ).findElementsContainingText( $( this ).val() );

            if ( elem.length ) {

                // Hide all
                $( ".skRoleListPlaceholder .list-group-item" ).hide();

                // Just show the ones we want
                elem.show();

                // Display numbers
                $( ".skRoleResult" ).html( "" + elem.length + " role(s) found" );

            }

            // No results
            else {

                // Show all
                $( ".skRoleListPlaceholder .list-group-item" ).show();

                // Hide results
                $( ".skRoleResult" ).html( "" );

            }

        } );

        var countNotUsed = graph.roleManager.roleList.filter( function( role ) {

            return role.boxes.length === 0;

        } );

        $( ".skUnusedRolesCount" ).text( "" + countNotUsed.length );

    }

    return skAnalyticsRoles;

} );
