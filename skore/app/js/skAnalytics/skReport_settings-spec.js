
define( [ "Injector" ], function( Injector ) {

    "use strict";

    describe( "skReport_settings", function() {

        var injector = new Injector();

        // Tests do not modify injected dependencies
        // for that reason it is enough to reset injector and get dependencies once
        beforeAll( injector.reset );

        var mxUtils, skShell, skReport_settings, skShellCopy;

        beforeAll( injector.run(
            function( _mxUtils /*mxUtils*/,
                      _skShell /*skShell*/,
                      _skReport_settings /*skAnalytics/skReport_settings*/) {

                mxUtils = _mxUtils;
                skShell = _skShell;
                skReport_settings = _skReport_settings;
                skShellCopy = {
                    arrayToString: skShell.arrayToString,
                    readValue: skShell.readValue
                };

            }
        ) );

        beforeEach( function() {

            // It is much faster to clear skShell between tests then to call injector.reset
            for ( var p in skShell ) {

                if ( skShell.hasOwnProperty( p ) ) {

                    delete skShell[ p ];

                }

            }
            skShell.arrayToString = skShellCopy.arrayToString;
            skShell.readValue = skShellCopy.readValue;

        } );

        describe( "skReport_settings.columnsProperties", function() {

            var columns, column;

            beforeEach( function() {

                var settings = new skReport_settings();

                columns = settings.columnsProperties;
                column = null;

            } );

            function getColumn( data ) {

                for ( var i = 0, l = columns.length; i < l; i += 1 ) {

                    if ( columns[ i ].data === data ) {

                        return columns[ i ];

                    }

                }

                return null;

            }

            function columnDefinition( title ) {

                return jasmine.objectContaining( {
                    title: title,

                    // Todo: isSelectable is not used ?
                    //isSelectable: jasmine.any(Function),
                    included: jasmine.any( Function ),
                    getter: jasmine.any( Function )
                } );

            }

            describe( "`cell` column", function() {

                beforeEach( function() {

                    column = getColumn( "cell" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Cell" ) );

                } );

                it( "should return cell itself", function() {

                    var cell = { cell: true };

                    expect( column.getter( cell ) ).toBe( cell );

                } );

            } );

            describe( "`id` column", function() {

                beforeEach( function() {

                    column = getColumn( "id" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "ID" ) );

                } );

                it( "should return cell.id", function() {

                    var cell = { id: "a" };

                    expect( column.getter( cell ) ).toBe( "a" );

                } );

            } );

            describe( "`type` column", function() {

                beforeEach( function() {

                    column = getColumn( "type" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Type" ) );

                } );

                describe( "`what` report", function() {

                    beforeEach( function() {

                        skShell.reportProperties = { reportType: "what" };

                    } );

                    it( "should handle `anything`", function() {

                        var cell = { style: "anything" };

                        expect( column.getter( cell ) ).toBe( "anything" );

                    } );

                    it( "should handle `anything;a=b`", function() {

                        var cell = { style: "anything;a=b;c=d" };

                        expect( column.getter( cell ) ).toBe( "anything | b" );

                    } );

                    it( "should handle `whatboxdetailed`", function() {

                        var cell = { style: "whatboxdetailed" };

                        expect( column.getter( cell ) ).toBe( "whatbox | with details" );

                    } );

                    it( "should handle `whatboxdetailed;a=b`", function() {

                        var cell = { style: "whatboxdetailed;a=b" };

                        // Todo: bug? should it be `whatbox | b | with details` instead of `whatboxdetailed | b`?
                        expect( column.getter( cell ) ).toBe( "whatboxdetailed | b" );

                    } );

                    it( "should handle `cellhihglight`", function() {

                        var cell = { style: "cellhihglight" };

                        expect( column.getter( cell ) ).toBe( "whybox | handover" );

                    } );

                    it( "should handle `cellhihglight;a=b`", function() {

                        var cell = { style: "cellhihglight;a=b" };

                        // Todo: bug? should it be `whybox | b | handover` instead of `cellhihglight | b`?
                        expect( column.getter( cell ) ).toBe( "cellhihglight | b" );

                    } );

                } );

                describe( "`att` report", function() {

                    beforeEach( function() {

                        skShell.reportProperties = { reportType: "att" };

                    } );

                    it( "should return the type of attachment", function() {

                        spyOn( skShell, "readValue" ).and.callThrough();
                        var attachment = mxUtils.parseXml( '<attachment value="a" type="b"/>' ).documentElement;

                        expect( column.getter( null, null, attachment ) ).toBe( "b" );
                        expect( skShell.readValue ).toHaveBeenCalledWith( attachment, "type" );

                    } );

                } );

                describe( "all other reports", function() {

                    beforeEach( function() {

                        skShell.reportProperties = { reportType: "the rest" };

                    } );

                    it( "should return undefined", function() {

                        // Todo: bug?
                        expect( column.getter() ).toBeUndefined();

                    } );

                } );

            } );

            describe( "`text` column", function() {

                beforeEach( function() {

                    column = getColumn( "text" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Text" ) );

                } );

                it( "should return result of cell.getBoxText call", function() {

                    var text = "box text";
                    var cell = jasmine.createSpyObj( "cell", [ "getBoxText" ] );

                    cell.getBoxText.and.returnValue( text );
                    expect( column.getter( cell ) ).toBe( text );
                    expect( cell.getBoxText ).toHaveBeenCalled();

                } );

            } );

            describe( "`roles` column", function() {

                beforeEach( function() {

                    column = getColumn( "roles" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Role(s)" ) );

                } );

                it( "should return space if cell is not whatbox", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "isWhatbox" ] );

                    cell.isWhatbox.and.returnValue( false );
                    expect( column.getter( cell ) ).toBe( " " );

                } );

                it( "should return cell.getBoxResource if cell is whatbox, and report type is not `who`", function() {

                    skShell.reportProperties = { reportType: "not who" };
                    var resource = "resource";
                    var cell = jasmine.createSpyObj( "cell", [ "isWhatbox", "getBoxResource" ] );

                    cell.isWhatbox.and.returnValue( true );
                    cell.getBoxResource.and.returnValue( resource );
                    expect( column.getter( cell ) ).toBe( resource );

                } );

                it( "should return cell.getBoxResource if cell is whatbox, report type is `who`, and resource is undefined", function() {

                    skShell.reportProperties = { reportType: "who" };
                    var resource = "resource";
                    var cell = jasmine.createSpyObj( "cell", [ "isWhatbox", "getBoxResource" ] );

                    cell.isWhatbox.and.returnValue( true );
                    cell.getBoxResource.and.returnValue( resource );
                    expect( column.getter( cell ) ).toBe( resource );

                } );

                it( "should read and return resource value if cell is whatbox and report type is `who`", function() {

                    skShell.reportProperties = { reportType: "who" };
                    spyOn( skShell, "readValue" ).and.callThrough();
                    var resource = mxUtils.parseXml( '<resource value="a"/>' ).documentElement;
                    var cell = jasmine.createSpyObj( "cell", [ "isWhatbox" ] );

                    cell.isWhatbox.and.returnValue( true );
                    expect( column.getter( cell, null, resource ) ).toBe( "a" );

                } );

            } );

            describe( "`relation` column", function() {

                beforeEach( function() {

                    column = getColumn( "relation" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Location" ) );

                } );

                it( "should return result of findRelationship call", function() {

                    var cell = { cell: 1 };
                    var parent = { parent: 2 };
                    var relation = { relation: 3 };

                    skShell.graph = { findRelationship: jasmine.createSpy( "findRelationship" ) };
                    skShell.graph.findRelationship.and.returnValue( relation );
                    expect( column.getter( cell, parent ) ).toBe( relation );
                    expect( skShell.graph.findRelationship ).toHaveBeenCalledWith( parent, cell );

                } );

            } );

            describe( "`inbound` column", function() {

                beforeEach( function() {

                    column = getColumn( "inbound" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Inbound box(es)" ) );

                } );

                it( "should return result of getInboundsText call", function() {

                    var text = "inbound text";
                    var cell = jasmine.createSpyObj( "cell", [ "getInboundsText" ] );

                    cell.getInboundsText.and.returnValue( text );
                    expect( column.getter( cell ) ).toBe( text );
                    expect( cell.getInboundsText ).toHaveBeenCalled();

                } );

            } );

            describe( "`outbound` column", function() {

                beforeEach( function() {

                    column = getColumn( "outbound" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Outbound box(es)" ) );

                } );

                it( "should return result of getOutboundsText call", function() {

                    var text = "outbound text";
                    var cell = jasmine.createSpyObj( "cell", [ "getOutboundsText" ] );

                    cell.getOutboundsText.and.returnValue( text );
                    expect( column.getter( cell ) ).toBe( text );
                    expect( cell.getOutboundsText ).toHaveBeenCalled();

                } );

            } );

            describe( "`attachments` column", function() {

                beforeEach( function() {

                    column = getColumn( "attachments" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Attachments" ) );

                } );

                it( "should return result of getAttachmentString call if report type is not `att`", function() {

                    skShell.reportProperties = { reportType: "not att" };
                    var text = "attachment";
                    var cell = jasmine.createSpyObj( "cell", [ "getAttachmentString" ] );

                    cell.getAttachmentString.and.returnValue( text );
                    expect( column.getter( cell ) ).toBe( text );
                    expect( cell.getAttachmentString ).toHaveBeenCalled();

                } );

                it( "should read and return attachment value if report type is `att`", function() {

                    skShell.reportProperties = { reportType: "att" };
                    spyOn( skShell, "readValue" ).and.callThrough();
                    var attachment = mxUtils.parseXml( '<attachment value="a"/>' ).documentElement;

                    expect( column.getter( null, null, attachment ) ).toBe( "a" );
                    expect( skShell.readValue ).toHaveBeenCalledWith( attachment );

                } );

            } );

            describe( "`directParent` column", function() {

                beforeEach( function() {

                    column = getColumn( "directParent" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Parent (whatbox)" ) );

                } );

                it( "should return result of cell.parent.getBoxText()", function() {

                    var text = "parent text";
                    var cell = { parent: jasmine.createSpyObj( "cell", [ "getBoxText" ] ) };

                    cell.parent.getBoxText.and.returnValue( text );
                    expect( column.getter( cell ) ).toBe( text );
                    expect( cell.parent.getBoxText ).toHaveBeenCalled();

                } );

            } );

            describe( "`suppliers` column", function() {

                beforeEach( function() {

                    column = getColumn( "suppliers" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Suppliers" ) );

                } );

                it( "should return result of cell.getSuppliersUniqueList() separated by | (no elements)", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getSuppliersUniqueList" ] );

                    cell.getSuppliersUniqueList.and.returnValue( [] );
                    expect( column.getter( cell ) ).toBe( "" );

                } );

                it( "should return result of cell.getSuppliersUniqueList() separated by | (1 element)", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getSuppliersUniqueList" ] );

                    cell.getSuppliersUniqueList.and.returnValue( [ "a" ] );
                    expect( column.getter( cell ) ).toBe( "a" );

                } );

                it( "should return result of cell.getSuppliersUniqueList() separated by | (many elements)", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getSuppliersUniqueList" ] );

                    cell.getSuppliersUniqueList.and.returnValue( [ "a", "b" ] );
                    expect( column.getter( cell ) ).toBe( "a | b" );

                } );

            } );

            describe( "`supplierscount` column", function() {

                beforeEach( function() {

                    column = getColumn( "supplierscount" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "No. of handover from suppliers" ) );

                } );

                it( "should return empty string if length of cell.getSuppliersUniqueList() is zero", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getSuppliersUniqueList" ] );

                    cell.getSuppliersUniqueList.and.returnValue( [] );
                    expect( column.getter( cell ) ).toBe( "" );

                } );

                it( "should return length of cell.getSuppliersUniqueList()", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getSuppliersUniqueList" ] );

                    cell.getSuppliersUniqueList.and.returnValue( [ "a", "b" ] );
                    expect( column.getter( cell ) ).toBe( 2 );

                } );

            } );

            describe( "`customers` column", function() {

                beforeEach( function() {

                    column = getColumn( "customers" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "Customers" ) );

                } );

                it( "should return result of cell.getCustomersUniqueList() separated by | (no elements)", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getCustomersUniqueList" ] );

                    cell.getCustomersUniqueList.and.returnValue( [] );
                    expect( column.getter( cell ) ).toBe( "" );

                } );

                it( "should return result of cell.getCustomersUniqueList() separated by | (1 element)", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getCustomersUniqueList" ] );

                    cell.getCustomersUniqueList.and.returnValue( [ "a" ] );
                    expect( column.getter( cell ) ).toBe( "a" );

                } );

                it( "should return result of cell.getCustomersUniqueList() separated by | (many elements)", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getCustomersUniqueList" ] );

                    cell.getCustomersUniqueList.and.returnValue( [ "a", "b", "c" ] );
                    expect( column.getter( cell ) ).toBe( "a | b | c" );

                } );

            } );

            describe( "`customerscount` column", function() {

                beforeEach( function() {

                    column = getColumn( "customerscount" );

                } );

                it( "should exist", function() {

                    expect( column ).toEqual( columnDefinition( "No. of handover to customers" ) );

                } );

                it( "should return empty string if length of cell.getCustomersUniqueList() is zero", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getCustomersUniqueList" ] );

                    cell.getCustomersUniqueList.and.returnValue( [] );
                    expect( column.getter( cell ) ).toBe( "" );

                } );

                it( "should return length of cell.getCustomersUniqueList()", function() {

                    var cell = jasmine.createSpyObj( "cell", [ "getCustomersUniqueList" ] );

                    cell.getCustomersUniqueList.and.returnValue( [ "a", "b", "c" ] );
                    expect( column.getter( cell ) ).toBe( 3 );

                } );

            } );

        } );

        describe( "skReport_settings.getColumns", function() {

            var settings;

            beforeEach( function() {

                settings = new skReport_settings();

                // Clear columnsProperties, so getColumns does not include columns based on columnsProperties
                settings.columnsProperties = [];

            } );

            beforeEach( function() {

                var xml = "<extension>" +
                    '<tag value="R" name="Responsible"/>' +
                    '<tag value="A" name="Authority"/>' +
                    '<tag value="T" name="Task"/>' +
                    "</extension>";

                skShell.graph = { model: { root: { tagsInUse: mxUtils.parseXml( xml ).documentElement } } };

            } );

            function columnDefinition( col ) {

                col.isSelectable = col.included = col.getter = jasmine.any( Function );

                return col;

            }

            it( "should return columns based on `skShell.graph.model.root.tagsInUse` if report type is `who`", function() {

                settings.reportType = "who";
                var columns = settings.getColumns();

                expect( columns ).toEqual( [
                    columnDefinition( { data: "R", title: "Responsible" } ),
                    columnDefinition( { data: "A", title: "Authority" } ),
                    columnDefinition( { data: "T", title: "Task" } )
                ] );

                var resourceXml = '<resource><tag value="R"/><tag value="A"/></resource>';
                var resource = mxUtils.parseXml( resourceXml ).documentElement;

                expect( columns[ 0 ].getter( null, null, resource ) ).toEqual( "R" );
                expect( columns[ 1 ].getter( null, null, resource ) ).toEqual( "A" );
                expect( columns[ 2 ].getter( null, null, resource ) ).toEqual( "" );

            } );

            it( "should not create columns if report type is not `who`", function() {

                settings.reportType = "not who";
                var columns = settings.getColumns();

                expect( columns ).toEqual( [] );

            } );

        } );

    } );

} );
