
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skConstants = require( "skConstants" );
    var skUtils = require( "skUtils" );
    var mxCellPath = require( "mxCellPath" );

    function skGraphData( graph ) {

        console.log( "new skGraphData" );

        /*jshint validthis:true*/
        this.graph = graph;
        this.model = graph.model;

    }

    skGraphData.prototype.graph = null;
    skGraphData.prototype.model = null;
    skGraphData.prototype.bigData = null;
    skGraphData.prototype.getBigData = function( forceRefresh ) {

        if ( !forceRefresh && this.bigData ) {

            return this.bigData;

        }

        // Whatboxes & depth & attachments
        var depth = 0;
        var attachmentsCount = 0;

        var whatboxes = [];
        var whatboxesDetailed = [];
        var whyboxes = [];

        // var whatboxesForRole = [];

        var that = this;

        // initiate the role manager
        if ( !forceRefresh || !that.graph.roleManager.roleList ) {

            that.graph.roleManager.getRoles();

        }
        that.graph.roleManager.roleList.forEach( function( role ) {

            role.boxes = [];

        } );

        this.model.filterDescendants( function( cell ) {

            depth = Math.max( mxCellPath.create( cell ).split( "." ).length, depth );

            attachmentsCount += cell.numberOfAttachments();

            if ( cell.isWhatbox() ) {

                whatboxes.push( cell );

                if ( cell.value ) {

                    $( skConstants.ROLE, cell.value ).each( function( i, role ) {

                        // find role in the role list
                        var r = that.graph.roleManager.roleList.find( function( role2 ) {

                            return role2.localid == skUtils.readValue( role, skConstants.LOCAL_ID );

                        } );

                        if ( r ) {

                            r.boxes.push( cell );

                        }

                    } );

                }

            }

            if ( cell.isWhatboxDetailed() ) {

                whatboxesDetailed.push( cell );

            }

            if ( cell.isWhybox() ) {

                whyboxes.push( cell );

            }

        } );

        this.bigData =  {
            depth: depth - 1, // -1 because we make it match the number of "levels" in the title

            whatboxes: whatboxes,
            whatboxesCount: whatboxes.length,

            whatboxesDetailed: whatboxesDetailed,
            whatboxesDetailedCount: whatboxesDetailed.length,

            whatboxesForRole: that.graph.roleManager.roleList,

            whyboxes: whyboxes,
            whyboxesCount: whyboxes.length,

            attachmentsCount: attachmentsCount
        };

        return this.bigData;

    };

    /*
     * gets an array per role of all the whatboxes
     *
     */

    skGraphData.prototype.getWhatboxesForRole = function( forceRefresh ) {

        return this.getBigData( forceRefresh ).whatboxesForRole;

    };

    /*
     * returns the top 10 / 15 of all roles
     * array index = 0 -> most used roles, etc.
     *
     */

    skGraphData.prototype.topRoles = null;
    skGraphData.prototype.getTopRoles = function( forceRefresh ) {

        if ( !forceRefresh && this.topRoles ) {

            return this.topRoles;

        }

        var whatboxesForRole = this.getWhatboxesForRole( forceRefresh );

        whatboxesForRole.sort( function( a, b ) {

            return b.boxes.length - a.boxes.length;

        } );

        // var keys = Object.keys( whatboxesForRole );

        // var topRoles = [];

        // // get them in a first array
        // $.each( keys, function( i, role ) {

        //     topRoles.push( {
        //         "role": role,
        //         "count": whatboxesForRole[ role ].length } );

        // } );

        // // sort
        // topRoles = topRoles.sort( function( a, b ) {

        //     return b.count - a.count; // We sort from max to min

        // } );

        // this.topRoles = topRoles;

        return whatboxesForRole;

    };

    return skGraphData;

} );
