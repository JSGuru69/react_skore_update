
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skConstants = require( "skConstants" );
    var mxUtils = require( "mxUtils" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );
    var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );
    var skFormLoader = require( "skGraph/skFormLoader" );

    /**
     * @constructor
     */
    function skAnalyticsRoles_roleDesc( graph, role ) {

        console.log( "create role desc", role );
        var textForClipboard = "";

        /*jshint validthis:true*/
        var that = this;
        this.graph = graph;

        var d = $( "<div>" );

        var clipBoardButton;
        var roleComment;

        // Adds export to clipboard button
        if ( ELECTRON ) {

            clipBoardButton = $( '<div class="skIgnoreForCopy pull-right">' +
                '<div class="btn-group">' +
                    '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" ' +
                    'aria-expanded="false">' +
                    'Copy to clipboard<span class="caret"></span>' +
                    "</button>" +
                    '<ul class="dropdown-menu">' +
                        '<li><a class="textCp">Text (markdown) to clipboard</a></li>' +
                        '<li><a class="htmlCp">HTML to clipboard (best for MS Word)</a></li>' +

                    "</ul>" +
                "</div>" +
            "</div>" );

            $( ".textCp", clipBoardButton ).on( "click", function() {

                window.requireNode( "electron" ).clipboard.writeText( textForClipboard.replace( "%%SKTOBEREPLACED%%", roleComment ) );
                skShell.setStatus( "Exported to your Clipboard!<br/>You can paste the content anywhere." );

            } );
            $( ".htmlCp", clipBoardButton ).on( "click", function() {

                var roleDesc = $( ".skRoleDesc" )[ 0 ].cloneNode( true );
                $( ".skIgnoreForCopy", roleDesc ).remove();
                $( ".btn-group", roleDesc ).remove();

                var t = $( roleDesc ).html();
                window.requireNode( "electron" ).clipboard.writeHtml( t );
                skShell.setStatus( "Exported to your Clipboard!<br/>You can paste the content anywhere." );

            } );

            d.append( clipBoardButton );

        }

        /*
         * Off we go to build up the role description
         *
         */

        var t = $( "<h2>Role description for <span class='skModalRolesManagerLabel'></span></h2>" );

        $( ".skModalRolesManagerLabel", t ).html( skUtils.renderMarkdown( mxUtils.htmlEntities( role.name ), true ) );
        d.append( t );

        textForClipboard += "# " + role.name + "\n";
        textForClipboard += "(Generated : " + new Date() + ")\n";

        var cloud;

        if ( $.jQCloud ) {

            cloud = $( "<div style='width:650px; height:200px'>" );

            d.append( cloud );

        }

        /*
         * render comment in the role
         *
         */

        var v = this.graph.roleManager.getRoleByName( role.name );

        // form with callback function
        var myForm = new skFormLoader(
            this.graph,
            v,
            "role",
            true,
            {
                editable:true,
                showBtnRemove: false
            },
            mxUtils.bind( this, function( parentValue, value ) {

                var rName = skUtils.renderMarkdown( skUtils.readValue( value ), true );
                $( ".skModalRolesManagerLabel" ).html( rName );
                $( ".skRoleListPlaceholder .active .roleName" ).html( rName );

                this.graph.roleManager.roleList = null;
                this.graph.roleManager.roleList = this.graph.roleManager.getRoles();
                $( "#skModalRolesManager" ).data( "rolehasbeendeletedorupdated", true );

            } )
        );

        // var template_Renderer = new Template_Renderer( this.graph, "role", null, function( newValue ) {
        // } );
        // var formPlaceholder = template_Renderer.makeForm( null, this.graph.roleManager.roles, v, true );

        d.append( myForm.getFullForm() );

        /*
         * Role (custom) comment / notes
         *
         */

        if ( skShell.viewerMode ) {

            $( ".skNotesButton", d ).hide();

            if ( ELECTRON ) {

                $( clipBoardButton ).hide();

            }

        } else {

            $( ".skNotesButton", d ).show();

            if ( roleComment === null || roleComment === "" ) {

                $( ".skNotesButton", d ).html( "Add notes for the role" );

            }

        }

        textForClipboard += "\nNotes for the role:\n" + "%%SKTOBEREPLACED%%" + "\n\n";

        /*
         * Overview of activities
         *
         * (and attachments as we're at it already...)
         */

        d.append( $( "<h3>All work activities</h3>" ) );

        textForClipboard += "## All activities\n";

        var ul = $( "<ul class='list-group'>" );

        d.append( ul );

        // Gets all the relevant activities
        var wbWithAttachment = [];

        if ( !role.boxes || role.boxes.length === 0 ) {

            var ite = $( "<li class='list-group-item'><em>This role is not used in the map.</em></li>" );

            textForClipboard += "\nThis role is not used in the map.";

            ul.append( ite );

            /*
             * delete button
             *
             */

            if ( roleComment == null || roleComment === "" ) {

                d.append( $( '<button type="button" class="btn btn-danger btn-sm">Remove role</button>' ).on( "click", function() {

                    // Remove from resource manager
                    graph.roleManager.deleteRoleByLocalID( role.localid );

                    // Remove from list on the left (not the prettiest way though...)
                    $( ".skRoleListPlaceholder .active" ).remove();

                    // If in roles manager, we empty the right side panel
                    $( ".skRoleDesc" ).empty().html( "Select or add a role in the list on the left." );

                    // If in modal, we close the modal
                    $( "#skModalRolesManager" ).data( "rolehasbeendeletedorupdated", true );
                    $( "#skModalRolesManager" ).modal( "hide" );

                } ) );

            }

        } else {

            // var allWords = [];

            $.each( role.boxes, function( i, cell ) {

                // for jqcloud
                // var punctRE = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#\$%&\(\)\*\+,\-\.\/:;<=>\?@\[\]\^_`\{\|\}~]/g;
                // var spaceRE = /\s+/g;
                // var str = cell.getBoxText();
                // str.replace( punctRE, "" ).replace( spaceRE, " " );
                // allWords = allWords.concat( str.split( " " ) );

                var textCell = cell.getBoxText();
                var ite;

                if ( textCell.trim() !== "" ) {

                    ite = $( "<li class='list-group-item'>" + skUtils.renderMarkdown( textCell, true ) + "</li>" );

                } else {

                    ite = $( "<li class='list-group-item'>" + "(unnamed)" + "</li>" );

                }

                textForClipboard += "\n" + cell.getBoxText();

                ul.append( ite );

                // If multi resources -- add the tags
                // if ( graph.model.root.tagsInUse ) {

                    console.log( "tags In Use" );

                    // For each tags of the whatbox and this resource
                    that.getResourceTagsNodes( role, cell ).forEach( function( tagNode ) {

                        var currentTag = skUtils.readValue( tagNode );

                        // Find the style of the tag
                        ite.append( $( "" +
                            "<span class='" + skConstants.ELMT_TAG + "' >" +
                                currentTag +
                            "</span>" ) );
                        textForClipboard += " [" + currentTag + "]";

                    } );

                // }

                if ( cell.numberOfAttachments() ) {

                    wbWithAttachment.push( cell );
                    ite.append( $( "<span>( <span class='tag'>" + cell.numberOfAttachments() + " attachment(s)</span> )</span>" ) );
                    textForClipboard += " (" + cell.numberOfAttachments() + " attachments)";

                }

            } );

            // For cloud
            // if ( $.jQCloud ) {

            //     // Create the cloud
            //     var allWordsDistinct = $.distinct( allWords );
            //     var allWordsNew = [];

            //     $.each( allWordsDistinct, function( i, e ) {

            //         // jscs:disable maximumLineLength
            //         var wordsToIgnore = [ "a", "abaft", "abeam", "aboard", "about", "above", "absent", "across", "afore", "after", "against", "along", "alongside", "amid", "amidst", "among", "amongst", "an", "anenst", "apropos", "apud", "around", "as", "aside", "astride", "at", "athwart", "atop", "barring", "before", "behind", "below", "beneath", "beside", "besides", "between", "beyond", "but", "by", "chez", "circa", "concerning", "despite", "down", "during", "except", "excluding", "excl.", "failing", "following", "for", "forenenst", "from", "given", "in", "including", "inc.", "inside", "into", "like", "mid", "midst", "minus", "modulo", "near", "next", "notwithstanding", "o'", "of", "off", "on", "onto", "opposite", "out", "outside", "over", "pace", "past", "per", "plus", "pro", "qua", "regarding", "round", "sans", "save", "since", "than", "the", "through", "thru", "throughout", "thruout", "till", "times", "to", "toward", "towards", "under", "underneath", "unlike", "until", "unto", "up", "upon", "versus", "vs.", "v.", "via", "vice", "vis-à-vis", "with", "within ", "without", "worth" ];

            //         // jscs:enable maximumLineLength

            //         if ( e.length > 2 && wordsToIgnore.indexOf( e ) < 0 ) {

            //             allWordsNew.push( {
            //                 "text": e.toLowerCase(),
            //                 "weight": 1
            //             } );

            //         }

            //     } );

            //     $( cloud ).jQCloud( allWordsNew, {
            //         autoResize: true,
            //         delay: 50
            //     } );

            // }

        }

        /*
         * Overview of handovers
         *
         */

        if ( graph.model.root.handoverhighlight ) {

            var relations = skGraphUtils.synth_handover( graph.model );

            // Filter only relevant relations
            var relationFiltered = [];

            $.each( relations, function( i, e ) {

                if ( e[ 0 ] == role.localid || e[ 1 ] == role.localid ) {

                    relationFiltered.push( e );

                }

            } );

            console.log( "relationFiltered" );

            if ( relationFiltered.length > 0 ) {

                textForClipboard += "\n## Handovers (" + relationFiltered.length + ")\n";
                d.append( $( '<h3>Handovers <span class="badge">' + relationFiltered.length + "</span></h3>" ) );

                d.append( $( "<h4>From suppliers</h4>" ) );
                textForClipboard += "\n### From Supplier\n";

                ul = $( "<ul class='list-group'>" );
                d.append( ul );

                var i, fullName;

                for ( i = 0; i < relationFiltered.length; i++ ) {

                    if ( relationFiltered[ i ][ 1 ] == role.localid ) {

                        fullName = skUtils.readValue( graph.roleManager.getRoleByLocalID( relationFiltered[ i ][ 0 ] ) );
                        ul.append( $( "<li class='list-group-item'>" +
                            skUtils.renderMarkdown( fullName, true ) +
                            " (" + relationFiltered[ i ][ 3 ].length + ")</li>" ) );
                        textForClipboard += "" + fullName + " (" + relationFiltered[ i ][ 3 ].length + ")\n";

                    }

                }

                d.append( $( '<h4 class="panel-title">To customers</span></h4>' ) );

                // D.append($("<h3>To customers : </h3>"));
                textForClipboard += "\n### To customers\n";

                ul = $( "<ul class='list-group'>" );
                d.append( ul );

                for ( i = 0; i < relationFiltered.length; i++ ) {

                    if ( relationFiltered[ i ][ 0 ] == role.localid ) {

                        fullName = skUtils.readValue( graph.roleManager.getRoleByLocalID( relationFiltered[ i ][ 1 ] ) );
                        ul.append( $( "<li class='list-group-item'>" +
                            skUtils.renderMarkdown( fullName, true ) +
                            " (" + relationFiltered[ i ][ 4 ].length + ")</li>" ) );
                        textForClipboard += "" + fullName + " (" + relationFiltered[ i ][ 4 ].length + ")\n";

                    }

                }

            } // End if relations

        } // End handovers

        /*
         * attachments
         *
         */

        if ( wbWithAttachment.length ) {

            d.append( $( "<h3>Attachments</h3>" ) );
            textForClipboard += "\n\n## Attachments\n";

            // var myRenderer = new Template_Renderer( this.graph, "attachments" );

            $.each( wbWithAttachment, function( i, cell ) {

                var form = new skFormLoader(
                    that.graph,
                    cell,
                    "attachments",
                    true,
                    {
                        editable: false
                    } );
                d.append( $( "<H4>For box : " + skUtils.renderMarkdown( cell.getBoxText(), true ) + "</h4>" ) );
                d.append( form.getFullForm() ); //myRenderer.makeForm( null, null, cell.value.querySelector( "attachments" ), true ) );

                textForClipboard += "\n### " + cell.getBoxText() + "\n";

                // for the copy to text
                $( $( "attachments", cell.value ).children() ).each(
                    function( i, att ) {

                        if ( att.nodeName === "url" ) {

                            textForClipboard += "[" + skUtils.readValue( att ) + "](" + skUtils.readValue( att ) + ")";

                        } else {

                            textForClipboard += skUtils.readValue( att );

                        }

                        textForClipboard += "\n";

                    } );

                // ul = $( "<ul class='list-group'>" );
                // d.append( ul );

                // $( "attachment", cell.value ).each( function( i, atts ) {

                //     $(atts).each(function(j, att){

                //     })

                //     // If URL we convert it to markdown so it gets "marked"
                //     if ( att.getAttribute( "type" ) === skConstants.url_type ) {

                //         textForAtt = "[" + skUtils.renderMarkdown( skUtils.readValue( att ) ) + "]" +
                //         "(" + skUtils.readValue( att, skConstants.url_address ) + ")";

                //     } else {

                //         textForAtt = skUtils.renderMarkdown( skUtils.readValue( att ) );

                //     }

                //     var lineToAdd = $( "<li class='list-group-item'>" + skUtils.renderMarkdown( textForAtt )  + "</li>" );

                //     ul.append( lineToAdd );

                //     $( lineToAdd ).on( "click", "a", function( event ) {

                //         console.log( "click on a -- att " );
                //         event.stopPropagation();
                //         graph.handleConnectionLinks( event );

                //     } );

                // } );

            } );

        }

        return d;

    }

    skAnalyticsRoles_roleDesc.prototype.graph = null;

    skAnalyticsRoles_roleDesc.prototype.getCommentNode = function getCommentNodeF( roleName ) {

        // this.graph.roleManager.getCommentNode( roleName );
        return $( "role[value='" + roleName + "'] comment", this.graph.roleManager.roles );

    };

    skAnalyticsRoles_roleDesc.prototype.setRoleComment = function setRoleCommentF( roleName, comment ) {

        return this.graph.roleManager.setRoleComment( roleName, comment );

    };

    /*
     * TODO : get tag style from form renderer
     *
     */

    // skAnalyticsRoles_roleDesc.prototype.getTagStyle = function getTagStyleF( currentTag ) {

    //     var tagNodes = this.graph.model.root.tagsInUse.getElementsByTagName( skConstants.ELMT_TAG );

    //     for ( var i = 0, l = tagNodes.length; i < l; i += 1 ) {

    //         var n = tagNodes[ i ];

    //         if ( n.getAttribute( skConstants.VALUE ) === currentTag ) {

    //             return n.getAttribute( "style" ) || "";

    //         }

    //     }

    //     return "";

    // };

    skAnalyticsRoles_roleDesc.prototype.nodeListToArray = function nodeListToArrayF( nl ) {

        var l = nl.length;
        var arr = new Array( l );

        for ( var i = 0; i < l; i += 1 ) {

            arr[ i ] = nl[ i ];

        }

        return arr;

    };

    skAnalyticsRoles_roleDesc.prototype.getResourceTagsNodes = function getResourceTagsNodesF( role, cell ) {

        var res = [];
        var roles = $( "role[" + skConstants.LOCAL_ID + "=" + role.localid + "]", cell.value );

        roles.each( function( i, role ) {

            var resp = $( role ).parent();
            $( "tag", resp ).each( function( i, tag ) {

                res.push( tag );

            } );

        } );

        return res;

        // $("resource[value='roleName'], cell.value).find("tag").toArray()
        // var resourceNodes = cell.value ? cell.value.getElementsByTagName( skConstants.ROLE ) : [];

        // for ( var i = 0, l = resourceNodes.length; i < l; i += 1 ) {

        //     var n = resourceNodes[ i ];

        //     if ( n.getAttribute( skConstants.VALUE ) === roleName ) {

        //         return this.nodeListToArray( n.getElementsByTagName( skConstants.ELMT_TAG ) );

        //     }

        // }

        // return [];

    };

    return skAnalyticsRoles_roleDesc;

} );
