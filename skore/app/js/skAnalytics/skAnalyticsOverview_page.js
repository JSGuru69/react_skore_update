
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skAnalyticsRoles_roleDesc = require( "./skAnalyticsRoles_roleDesc" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );
    var skUtils = require( "skUtils" );

    function skAnalyticsOverview( graph, graphData ) {

        /*jshint validthis:true*/
        this.graph = graph;
        this.graphData = graphData;
        var that = this;

        var bigData = this.graphData.getBigData( true );

        // var graphUtils = skGraphUtils.synth_boxes( graph.model );

        /*
         * Activities panel
         *
         */

        if ( bigData.whatboxesCount === 0 ) {

            $( ".skEmptyMessage" ).show();

        } else {

            $( ".skEmptyMessage" ).hide();

        }

        $( ".skActivityCount" ).text( bigData.whatboxesCount );
        $( ".skDetailedViewCount" ).text( bigData.whatboxesDetailedCount );
        $( ".skNotDetailedViewCount" ).text( ( bigData.whatboxesCount - bigData.whatboxesDetailedCount ) );
        $( ".skMaxDepth" ).text( bigData.depth );
        $( ".skAttachmentsCount" ).text( bigData.attachmentsCount );

        /*
         * Roles panel
         *
         */

        /*jshint validthis:true*/
        this.createRoleList();

        /*
         * Role description in modal
         *
         */

        // On open
        $( "#skModalRolesManager" ).on( "shown.bs.modal", function( event ) {

            var rt = event.relatedTarget;
            var button = $( rt ); // Button that triggered the modal

            $( ".skJobDescPlaceholder" ).empty();
            $( ".skJobDescPlaceholder" ).append( new skAnalyticsRoles_roleDesc(
                graph,
                button.data( "role" ) ) );

            // Cosmetic hack...
            $( "#skModalRolesManagerLabel" ).html( skUtils.renderMarkdown( button.data( "role" ).name, true ) );
            $( "#skModalRolesManager h2" ).hide();

        } );

        // On close the modal
        $( "#skModalRolesManager" ).on( "hidden.bs.modal", function( /*event*/) {

            // To make sure it's clean...
            $( ".skJobDescPlaceholder" ).empty();

            // Refresh the list of roles if one has been deleted
            if ( $( "#skModalRolesManager" ).data( "rolehasbeendeletedorupdated" ) === true ) {

                that.createRoleList( true );

            }

        } );

        /*
         * Handover panel
         *
         */

        if ( graph.model.root.handoverhighlight ) {

            // Hide notice
            $( ".skHHMRTWarning" ).hide();
            $( ".skHandoverPanel" ).show();

            var handovers = skGraphUtils.synth_handover( graph.model );

            // if ( graph.model.root.tagsInUse ) {

            $( ".skHHMRTWarning" ).show().html( "Only the <strong>first role</strong> of an activity is taken into account for handover." );

            var ul = $( ".skHHList" );

            ul.empty();

            // Start creating the html
            $.each( handovers, function( i, relation ) {

                if ( !relation.done ) {

                    var relOpposite = skGraphUtils.findRelationOpposite( handovers, relation[ 1 ], relation[ 0 ] );

                    var countO = 0;

                    if ( relOpposite != null ) {

                        countO = relOpposite[ 2 ].length;

                    }

                    // Html

                    ul.append( that.createRelation( relation[ 0 ], relation[ 2 ].length, relation[ 1 ], countO ) );

                }

            } );

        }

        // If extension is disabled
        else {

            $( ".skHandoverPanel" ).hide();

        }

    }

    skAnalyticsOverview.prototype.graph = null;
    skAnalyticsOverview.prototype.graphData = null;

    skAnalyticsOverview.prototype.createRoleList = function createRoleListF( forceRefresh ) {

        // var graph = this.graph;

        // Fixme : count
        // Gets the role, find the activity counts and sort
        var roleListWithBoxes = this.graphData.getTopRoles( forceRefresh );

        // get count
        var count = roleListWithBoxes.filter( function( role ) {

            return role.boxes.length > 0;

        } );

        $( ".skUniqueRolesCount" ).text( count.length );

        if ( roleListWithBoxes.length > 0 ) {

            $( ".skRolesUsage" ).show();

            var roleListe = $( ".skRolesList" );

            roleListe.empty();

            // Gets max 10 roles... if there is more than 15
            var max = 10;

            if ( roleListWithBoxes.length > 15 ) {

                $( ".skRoleListCountHeader" ).html( "Top 10 roles " );
                max = 10;

            } else {

                $( ".skRoleListCountHeader" ).html( "Roles usage " );
                max = roleListWithBoxes.length;

            }

            // Display them...
            for ( var i = 0; i < max; i++ ) {

                var container = $( "" +
                    "<p>" +
                        '<span class="leftCounter" >' +
                            roleListWithBoxes[ i ].boxes.length +
                        "</span>" +
                    "</p>" );

                var link = $( '<a data-toggle="modal" data-target="#skModalRolesManager">' +
                    skUtils.renderMarkdown( mxUtils.htmlEntities( roleListWithBoxes[ i ].name ), true ) + "</a>" );

                $( link ).data( "role", roleListWithBoxes[ i ] );

                $( container ).append( link );

                roleListe.append( container );

            }

            // Finishes off the count if necessary
            if ( count.length > max ) {

                roleListe.append( $( "<p>" +
                    "10 of " + roleListWithBoxes.length + " roles displayed" +
                "</p>" ) );

            }

        } else {

            $( ".skRolesUsage" ).hide();
            $( ".skRolesList" ).empty();

        }

    };

    skAnalyticsOverview.prototype.createRelation = function createRelationF( resource1, count1, resource2, count2 ) {

        var li = $( "" +
            '<li class="list-group-item">' +
                '<span class="handoverDetail">' +
                    ' <span class="roleForHandovers">' +
                        '<a data-toggle="modal" data-target="#skModalRolesManager" class="skR1">' +
                            skUtils.renderMarkdown(
                                mxUtils.htmlEntities(
                                    skUtils.readValue( this.graph.roleManager.getRoleByLocalID( resource1 ) ) ), true ) +
                        "</a> " +
                        ( ( count1 > 0 ) ? ' <span class="badge"> ' + count1 + " ⇀ </span> " : " " ) +
                    " </span> " +
                    '<span class="roleforHandover"> ' +
                        ( ( count2 > 0 ) ? ' <span  class="badge"> ↽ ' + count2 + " </span> " : " " ) +
                        '<a data-toggle="modal" data-target="#skModalRolesManager" class="skR2">' +
                            skUtils.renderMarkdown(
                                mxUtils.htmlEntities(
                                    skUtils.readValue(
                                        this.graph.roleManager.getRoleByLocalID( resource2 ) ) ), true ) +
                        "</a>" +
                    "</span>" +
                "</span>" +
            "</li>" );

        $( ".skR1", li ).data( "role", this.graph.roleManager.roleList.find( function( role ) {

 return resource1[ 0 ] == role.localid;

 } ) );
        $( ".skR2", li ).data( "role", this.graph.roleManager.roleList.find( function( role ) {

 return resource2[ 0 ] == role.localid;

 } ) );

        return li;

    };

    skAnalyticsOverview.prototype.shell = null;

    return skAnalyticsOverview;

} );
