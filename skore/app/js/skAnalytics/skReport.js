
/**
 * Constructs a new report to be inserted in a page
 *
 * focus on the "data tables" aspect of it (and filling it up)
 *
 * receives parameter...
 *
 * idea is to have as much as the configuration outside of this thing
 */

// Swith to another way of defining this because of datatables

define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );
    var skUtils = require( "skUtils" );

    /**
     * @constructor
     */
    function skReport( myReportContainer, reportType, graph ) {

        /*jshint validthis:true*/
        this.graph = graph;
        this.model = this.graph.model;
        var that = this;

        // initiate from saved values in localStorage
        var savedColumns = JSON.parse( window.localStorage.getItem( "skReportColumns" ) ) || {};

        Object.keys( this.columns ).forEach( function( column ) {

            that.columns[ column ] = ( savedColumns[ column ] != null ? savedColumns[ column ] : that.columns[ column ] );

        } );

        this.init( myReportContainer, reportType );

    }

    skReport.prototype.init = function initF( myReportContainer, reportType ) {

        /*
         * Placeholder data tables
         *
         *
         *********************/

        this.reportContainer = document.createElement( "div" );
        myReportContainer.innerHTML = "";
        myReportContainer.appendChild( this.reportContainer );

        this.reportPlaceholder = document.createElement( "table" );
        this.reportPlaceholder.className = "display";
        this.reportPlaceholder.setAttribute( "width", "100%" );
        this.reportPlaceholder.id = "table0";
        this.reportContainer.appendChild( this.reportPlaceholder );

        /*
         * Off we go...
         *
         */

        reportType = reportType || "what";

        this.update( reportType );

    };

    skReport.prototype.reportContainer = null;
    skReport.prototype.reportPlaceholder = null;

    skReport.prototype.columns = null;

    skReport.prototype.model = null;
    skReport.prototype.graph = null;
    skReport.prototype.dataTable = null;

    skReport.prototype.update = function updateF( reportType ) {

        if ( reportType ) {

            this.reportType = reportType;

        }

        // Kills and recreate the table
        if ( this.reportPlaceholder.childNodes.length !== 0 ) {

            this.reportPlaceholder.parentElement.remove( this.reportPlaceholder );
            this.reportPlaceholder = document.createElement( "table" );
            this.reportPlaceholder.className = "display";
            this.reportPlaceholder.id = "table0";
            this.reportContainer.appendChild( this.reportPlaceholder );

        }

        if ( this.dataTable ) {

            this.dataTable.destroy();

        }

        // Get the data to display

        var results = this.getData();

        this.dataTable = $( this.reportPlaceholder ).DataTable( {
            data: results.result,
            retrieve: true,
            columns: results.columns,
            paging: false,

            // Column reordering
            // dom: 'Rlrtip',
            dom: "irtip",
            buttons: [

                // Do not change order!!!
                // "copyHtml5",
                // "excelHtml5",
                // "csvHtml5",
                // "pdfHtml5"
                "copy",
                "excel",
                "csv",
                "pdf"
            ]
        } );

        this.dataTable.draw();

        // $.fn.dataTable.tables( {visible: true, api: true} ).
        this.dataTable.columns.adjust();

        window.currentTable = this.dataTable;

    }; // End update

    skReport.prototype.getData = function getDataF() {

        var dataResult = [];
        var that = this;

        var currentParent = this.graph.getDefaultParent();

        /* ⓘ info
         *
         * first we filter the cells (data)
         * and second the relation (scope)
         */

        var columnsProp = this.getColumns();

        var relation, myTableLine, candidate;

        this.model.filterDescendants( function( cell ) {

            myTableLine = {};
            candidate = false;

            // Filters by type
            if ( that.boxType.whatbox && cell.isWhatbox() ) {

                candidate = true;

            } else if ( that.boxType.whybox && cell.isWhybox() ) {

                candidate = true;

            } else if ( that.boxType.stickynote && cell.isStickynote() ) {

                candidate = true;

            }

            // Filters by scope
            if ( candidate ) {

                relation = skUtils.findRelationship( that.graph.getModel(), currentParent, cell );

                if (
                    ( that.relations.REL_PARENT       && relation == skConstants.REL_PARENT        ) ||
                    ( that.relations.REL_GRD_PARENT  && relation == skConstants.REL_GRD_PARENT   ) || // Beware parent/grandparent
                    ( that.relations.REL_CHILD        && relation == skConstants.REL_CHILD         ) ||
                    ( that.relations.REL_GRD_CHILD   && relation == skConstants.REL_GRD_CHILD    ) || // Beware child / grandchild thing
                    ( that.relations.REL_VISIBLE      && relation == skConstants.REL_VISIBLE       ) ||
                    ( that.relations.REL_NONE         && relation == skConstants.REL_NONE          ) ||
                    ( that.relations.REL_EQUAL        && relation == skConstants.REL_EQUAL         ) ) {

                    // Now we have all the cells
                    // to the columns...

                    if ( that.reportType == "who" ) {

                        var resps = $( skConstants.RESPONSIBILITY, cell.value );

                        // Iterate on resources first
                        $.each( resps, function( j, resp ) {

                            myTableLine = {};

                            // Fill the standard columns
                            $.each( columnsProp, function( i, column ) {

                                if ( column.isIncluded.call( that ) ) {

                                    myTableLine[ column.data ] = column.getter.call( that, cell, currentParent, resp );

                                }

                            } );

                            // Adds to table
                            dataResult.push( myTableLine );

                        } );

                    } else if ( that.reportType == "what" ) {

                        $.each( columnsProp, function( i, column ) {

                            if ( column.isIncluded.call( that ) ) {

                                myTableLine[ column.data ] = column.getter.call( that, cell, currentParent );

                            }

                        } );

                        myTableLine.currentCell = cell;

                        // Adds to table
                        dataResult.push( myTableLine );

                    } else if ( that.reportType == "att" ) {

                        // Get the attachment (ignore cells without attachments)
                        $.each( $( skConstants.ATTACHMENTS, cell.value ), function( i, atts ) {

                            $( atts ).children().each( function( j, att ) {

                                myTableLine = {};
                                $.each( columnsProp, function( i, column ) {

                                    myTableLine[ column.data ] = column.getter.call( that, cell, currentParent, att );

                                } );

                                // Adds to table
                                dataResult.push( myTableLine );

                            } );

                        } );

                    }

                    return true;

                }

            }

            // Probably an edge (groups are whatbox...)
            return false;

        } );

        if ( console.table ) {

            console.table( dataResult );

        }

        // This.columns = columnsProp;

        var returnObject = {
            "result": dataResult,
            "columns": columnsProp
        };

        return returnObject;

    }; // End getData

    skReport.prototype.reportType = "what";
    skReport.prototype.possibleReportType = [ "what", "who", "att" ];

    skReport.prototype.boxType = {
        whatbox: true,
        whybox: true,
        stickynote: false
    };

    /*
     * Columns and if they are included by default
     *
     */

    skReport.prototype.columns = {
        id: true,
        type: true,
        text: true,
        textnomarkdown: false,
        roles: true,
        rolesnomarkdown: false,
        relation: true,
        role: true,
        rolenotes: false,
        inbound: false,
        outbound: false,
        attachments: true,
        attachmentsnomarkdown: false,
        parent: true,
        suppliers: false,
        supplierscount: false,
        customers: false,
        customerscount: false,
        style: true

    };

    // SkReport.prototype.loadTags = {

    //     REL_PARENT : true,
    //     REL_GRD_PARENT : true,
    //     REL_CHILD : true,
    //     REL_GRD_CHILD : true,
    //     REL_VISIBLE : true,
    //     REL_NONE : true,
    //     REL_EQUAL : true

    // };

    skReport.prototype.relations = {
        REL_PARENT: true,
        REL_GRD_PARENT: true,
        REL_CHILD: true,
        REL_GRD_CHILD: true,
        REL_VISIBLE: true,
        REL_NONE: true,
        REL_EQUAL: true
    };

    skReport.prototype.getColumns = function getColumnsF() {

        var newColumns = [];
        var that = this;

        // if ( that.reportType == "what") {

        // }

        // Adds columns from mrt extension
        if ( that.reportType == "who" /*&& this.graph.model.root.tagsInUse */) {

            var myTags = [];

            // ok we go and fetch it manually
            // todo simplify
            var resp = that.graph.templateManager.getTemplate( "responsibility" ).fields;
            if ( resp && resp.length ) {

                myTags = skUtils.getObjects( resp, "name", "tag" );

                $( myTags ).each( function( i, tag ) {

                    if ( tag.value && tag.value.length && !that.columns.hasOwnProperty( tag.value ) ) {

                        that.columns[ tag.value ] = true;
                        that.columnsProperties.push( {
                            data: tag.value,
                            title: tag.description, //skUtils.readValue( tag, "name" ),
                            isSelectable: function() {

                                return this.reportType == "who";

                            },
                            isIncluded: function() {

                                return ( this.reportType == "who" && this.columns[ tag.value ] );

                            },
                            getter: function( cell, currentParent, resp ) {

                                // todo ici
                                var currentTag = $( "" + tag.name + "[value=" + tag.value + "]", resp ) ;

                                return skUtils.readValue( currentTag );

                            }
                        } );

                    }

                } );

            }

            // that.columnsPropertiesForMRT();

        }

        $.each( this.columnsProperties, function( i, column ) {

            if ( column.isIncluded.call( that ) ) {

                newColumns.push( column );

            }

        } );

        return newColumns;

    };

    // skReport.prototype.columnsPropertiesForMRT = function columnsPropertiesForMRTF() {

    //     var that = this;
    //     var tags;

    //     var resp = skUtils.getObjects( this.graph.formManager.getForm( "whatbox" ), "name", "responsibility" );
    //     if ( resp.length ) {

    //         tags = skUtils.getObjects( resp, "name", "tag" );

    //     }

    //     // if ( that.reportType == "who" && getObjects(resp, "name", "tag")) {
    //     if ( that.reportType == "who" && resp.length && tags.length ) {

    //         // For each tag of the tagsInUse
    //         // $.each( $( this.graph.model.root.tagsInUse ).children(), function( i, tag ) {
    //         $( tags ).each( function( i, tag ) {

    //         // $.each( $( this.graph.model.root.tagsInUse ).children(), function( i, tag ) {

    //             var roleTag = tag.value;

    //             // Add it if it not there yet
    //             if ( roleTag && roleTag.length && !that.columns.hasOwnProperty( roleTag ) ) {

    //                 that.columns[ roleTag ] = true;

    //                 that.columnsProperties.push( {
    //                     data: roleTag,
    //                     title: tag.description, //skUtils.readValue( tag, "name" ),
    //                     isSelectable: function() {

    //                         return this.reportType == "who";

    //                     },
    //                     isIncluded: function() {

    //                         return ( this.reportType == "who" && this.columns[ roleTag ] );

    //                     },
    //                     getter: function( cell, currentParent, resp ) {

    //                         // todo ici
    //                         var currentTag = $( "" + tag.name + "[value=" + tag.value + "]", resp ) ;

    //                         return skUtils.readValue( currentTag );

    //                     }
    //                 } );

    //             }

    //         } );

    //     }

    // };

    skReport.prototype.columnsProperties = [
        {
            data: "cell",
            title: "Cell",
            isSelectable: function() {

                return false;

            },
            isIncluded: function() {

                return this.columns.cell;

            },
            getter: function( cell ) {

                return cell;

            }
        },
        {
            data: "id",
            title: "ID",
            description: "ID of the cell",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.id;

            },
            getter: function( cell ) {

                return cell.id;

            }
        },
        {

            // Box type
            data: "type",
            title: "Type",
            description: "Shows box type (whatbox, whybox, stickynote) and some attributes; or attachment type",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.type;

            },
            getter: function( cell, currentParent, attachment ) {

                if ( this.reportType == "what" || this.reportType == "who" ) {

                    var typeString = "none";

                    var rawType = cell.style.split( ";" );

                    typeString = rawType[ 0 ];

                    // Whybox for handover
                    if ( cell.isWhybox() ) {

                        var handover = cell.style.match( "handover=1" );

                        if ( handover ) {

                            typeString = skConstants.whybox + " | handover";

                        }

                    }

                    if ( cell.isWhatboxDetailed() ) {

                        typeString = skConstants.whatbox + " | with details";

                    }

                    return typeString;

                } else if ( this.reportType == "att" ) {

                    // Return the type of attachment
                    return attachment.nodeName; //skUtils.readValue( attachment, "type" );

                }

            }
        },
        {

            // Whatbox text
            data: "text",
            title: "Text",
            description: "Main text of the current box",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.text;

            },
            getter: function( cell ) {

                return cell.getBoxText( true, true );

            }
        },
        {

            // Whatbox text
            data: "textnomarkdown",
            title: "Text (no formatting)",
            description: "Text without markdown rendering",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.textnomarkdown;

            },
            getter: function( cell ) {

                return cell.getBoxText();

            }
        },
        {

            // Role or roles
            data: "roles",
            title: "Role(s)",
            description: "Roles of the whatbox (joined & separated by " + skConstants.SEPARATOR + ").",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.roles;

            },
            getter: function( cell, currentParent, resp ) {

                if ( cell.isWhatbox() ) {

                    if ( this.reportType == "who" ) {

                        if ( resp ) {

                            var role = $( "" + skConstants.ROLE, resp )[ 0 ];
                            if ( role ) {

                                return skUtils.renderMarkdown(
                                    skUtils.readValue(
                                        this.graph.roleManager.getRoleByLocalID( $( role ).attr( "localid" ) ) ), true );

                            }

                        }

                    }

                    return skGraphUtils.getBoxRolesString( this.graph, cell, true );

                }

                return " ";

            }
        },
        {

            // Role or roles
            data: "rolesnomarkdown",
            title: "Role(s) (no formatting)",
            description: "Roles without markdown rendering",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.rolesnomarkdown;

            },
            getter: function( cell, currentParent, resp ) {

                if ( cell.isWhatbox() ) {

                    if ( this.reportType == "who" ) {

                        if ( resp ) {

                            var role = $( "" + skConstants.ROLE, resp );
                            if ( role ) {

                                return skUtils.readValue( role );

                            }

                        }

                    }

                    // return skGraphUtils.getBoxRoles(this.graph, cell);
                    var tmpRoles = skGraphUtils.getBoxRoles( this.graph, cell );
                    var tmpRoles2 = tmpRoles.map( function( r ) {

                        return skUtils.readValue( this.graph.roleManager.getRoleByLocalID( r ) );

                    } );

                    return tmpRoles2.join( skConstants.SEPARATOR );

                }

                return " ";

            }
        },
        {

            // Role or roles
            data: "rolenotes",
            title: "Notes for the role",
            description: "Notes for the role",
            isSelectable: function() {

                return this.reportType == "who";

            },
            isIncluded: function() {

                return this.columns.rolenotes;

            },
            getter: function( cell, currentParent, resp ) {

                if ( this.reportType == "who" ) {

                    if ( resp ) {

                        var role = skUtils.readValue( $( "" + skConstants.ROLE, resp )[ 0 ], skConstants.LOCAL_ID );
                        var commentNode = this.graph.roleManager.getCommentNode( role );

                        console.log( role, commentNode );

                        return skUtils.readValue( commentNode );

                    }

                }

                return " ";

            }
        },
        {

            // Level
            data: "relation",
            title: "Location",
            description: "Position of current box vs. current view",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.relation;

            },
            getter: function( cell, currentParent ) {

                return skUtils.findRelationship( this.graph.getModel(), currentParent, cell );

            }
        },
        {
            data: "inbound",
            title: "Inbound box(es)",
            description: "List inbound boxes text (joined & separated by | ). For unique relationship see the handover report",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.inbound;

            },
            getter: function( cell ) {

                return skUtils.renderMarkdown( cell.getInboundsText(), true );

            }
        },
        {
            data: "outbound",
            title: "Outbound box(es)",
            description: "List of outbound boxes text (joined & separated by | ). For unique relationship see the handover report",
            isSelectable: function() {

            return true;

        },
            isIncluded: function() {

                return this.columns.outbound;

            },
            getter: function( cell ) {

                return skUtils.renderMarkdown( cell.getOutboundsText(), true );

            }
        },
        {

            // Attachments
            data: "attachments",
            title: "Attachments",
            description: "Attachments to current whatbox (joined & separated by | ). Open attachment report to display one line per attachment",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.attachments;

            },
            getter: function( cell, currentParent, attachment ) {

                if ( this.reportType == "att" ) {

                    if ( attachment.nodeName === skConstants.RESPONSIBILITY ) {

                        // get the role id
                        var v = skUtils.readValue(
                                    this.graph.roleManager.getRoleByLocalID(
                                        skUtils.readValue(
                                            $( "role", attachment )[ 0 ],
                                            skConstants.LOCAL_ID ) ) );

                        v = skUtils.renderMarkdown( v, true );

                        // add the tags
                        if ( $( "tag", attachment ).length ) {

                            var tags = [];

                            $( "tag", attachment ).each( function( j, tag ) {

                                tags.push( skUtils.readValue( tag ) );

                            } );

                            v += " (";
                            v += tags.join( ", " );
                            v += ")";

                        }

                        return v;

                    } else if ( attachment.nodeName === skConstants.EXT_CONTENT ) {

                        return mxUtils.htmlEntities( skUtils.readValue( attachment ) );

                    }

                    return skUtils.renderMarkdown( skUtils.readValue( attachment ), false );

                }

                return cell.getAttachmentString( this.graph, true );

            }
        },
        {

            // Attachments
            data: "attachmentsnomarkdown",
            title: "Attachments (no formatting)",
            description: "Attachments without markdown rendering",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.attachmentsnomarkdown;

            },
            getter: function( cell, currentParent, attachment ) {

                if ( this.reportType == "att" ) {

                    return mxUtils.htmlEntities( skUtils.readValue( attachment ) );

                }

                return cell.getAttachmentString( this.graph, false );

            }
        },
        {
            data: "directParent",
            title: "Parent (whatbox)",
            description: "Show the (unique) parent of the current box. Parent of top level is the title of the Skore",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.directParent;

            },
            getter: function( cell ) {

                return skUtils.renderMarkdown( cell.parent.getBoxText(), true );

            }
        },
        {
            data: "suppliers",
            title: "Suppliers",
            description: "Roles of the inbound whatboxes when different than current role (joined & separated by | ). " +
                "See handover and role report for different kind of analytics.",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.suppliers;

            },
            getter: function( cell ) {

                var resList = cell.getSuppliersUniqueList();

                var that = this;
                var res = resList.map( function( r ) {

                    return skUtils.renderMarkdown( skUtils.readValue( that.graph.roleManager.getRoleByLocalID( r ) ), true );

                } );

                return res.join( skConstants.SEPARATOR );

            }
        },
        {
            data: "supplierscount",
            title: "No. of handover from suppliers",
            description: "Number of different suppliers (each counted only once)",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.supplierscount;

            },
            getter: function( cell ) {

                var resList = cell.getSuppliersUniqueList();

                return resList.length > 0 ? resList.length : "";

            }
        },
        {
            data: "customers",
            title: "Customers",
            description: "Roles of the next (outbound) whatbox (joined & separated by | ). " +
                "See handover and role report for different kind of analytics",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.customers;

            },
            getter: function( cell ) {

                var resList = cell.getCustomersUniqueList();

                var that = this;
                var res = resList.map( function( r ) {

                    return skUtils.renderMarkdown( skUtils.readValue( that.graph.roleManager.getRoleByLocalID( r ) ), true );

                } );

                return res.join( skConstants.SEPARATOR );

            }
        },
        {
            data: "customerscount",
            title: "No. of handover to customers",
            description: "Number of different customers (each counted only once)",
            isSelectable: function() {

                return true;

            },
            isIncluded: function() {

                return this.columns.customerscount;

            },
            getter: function( cell ) {

                var resList = cell.getCustomersUniqueList();

                return resList.length > 0 ? resList.length : "";

            }
        },
        {
            data: "rolenotes",
            title: "Role notes",
            description: "Notes added to the role",
            isSelectable: function() {

                if ( this.reportType === "who" ) {

                    return true;

                } else {

                    return false;

                }

            },
            isIncluded: function() {

                return this.columns.rolenotes;

            },
            getter: function( cell ) {

                return skUtils.renderMarkdown(
                        skUtils.readValue(
                            this.graph.roleManager.getCommentNode(
                                cell.getBoxRoles()[ 0 ] ) ), true );

            }
        },
        {
            data: "style",
            title: "Custom style",
            description: "Custom style applied to a box",
            isSelectable: function() {

                return true;

            },
            isIncluded:function() {

                return this.columns.style;

            },
            getter: function( cell ) {

                var returnString = "";
                var style = cell.style.split( ";" );

                style.forEach( function( e ) {

                    var s = e.split( "=" );

                    // Cleanup unused properties
                    if ( s.length === 2 && s[ 1 ] !== "" ) {

                        returnString += e + "; ";

                    }

                } );

                return returnString;

            }
        },
        {
            data: "editButton",
            title: "Actions",
            description: "Action buttons",
            isSelectable: function() {

                return false;

            },
            isIncluded: function() {

                return true;

            },
            getter: function() {

                return "<button class='btn btn-default skEditRowButton'>Edit</button>";

            }
        }
    ]; // End columns

    return skReport;

} );
