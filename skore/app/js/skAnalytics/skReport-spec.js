
define( [ "Injector" ], function( Injector ) {

    "use strict";

    describe( "skReport", function() {

        var injector = new Injector();

        beforeAll( injector.reset );

        var mxUtils, skShell, skReport, skShellCopy;

        beforeAll( injector.run(
            function( _mxUtils /*mxUtils*/,
                      _skShell /*skShell*/,
                      _skReport /*skAnalytics/skReport*/) {

                mxUtils = _mxUtils;
                skShell = _skShell;
                skReport = _skReport;
                skShellCopy = {
                    arrayToString: skShell.arrayToString,
                    readValue: skShell.readValue
                };

            }
        ) );

        beforeEach( function() {

            // It is much faster to clear skShell between tests then to call injector.reset
            for ( var p in skShell ) {

                if ( skShell.hasOwnProperty( p ) ) {

                    delete skShell[ p ];

                }

            }
            skShell.arrayToString = skShellCopy.arrayToString;
            skShell.readValue = skShellCopy.readValue;

        } );

        function constFn( value ) {

            return function() {

                return value;

            };

        }

        function MockCell( valueAsXml ) {

            return {
                value: mxUtils.parseXml( valueAsXml ).documentElement,
                isWhatbox: constFn( false ),
                isWhybox: constFn( false ), isStickynote: constFn( false )
            };

        }

        function WhatboxCell( valueAsXml ) {

            var self = MockCell( valueAsXml );

            self.isWhatbox = constFn( true );

            return self;

        }

        function WhyboxCell( valueAsXml ) {

            var self = MockCell( valueAsXml );

            self.isWhybox = constFn( true );

            return self;

        }

        function StickynoteCell( valueAsXml ) {

            var self = MockCell( valueAsXml );

            self.isStickynote = constFn( true );

            return self;

        }

        function MockModel( /*...cells*/) {

            var args = arguments;

            return {
                filterDescendants: function( fn ) {

                    for ( var i = 0, l = args.length; i < l; i += 1 ) {

                        fn( args[ i ] );

                    }

                }
            };

        }

        describe( "skReport.getData", function() {

            var report;

            beforeEach( function() {

                // Override constructor to prevent DOM code running
                var TestingReport = function() {
                };

                mxUtils.extend( TestingReport, skReport );
                report = new TestingReport();

            } );

            beforeEach( function() {

                // Setup mocks
                report.model = new MockModel(
                    WhatboxCell( '<whatbox><resource value="what 1"/><resource value="what 2"/></whatbox>' ),
                    WhyboxCell( '<whybox><resource value="why 1"/><resource value="why 2"/></whybox>' ),
                    StickynoteCell( '<stickynote><resource value="sticky 1"/><resource value="sticky 2"/></stickynote>' )
                );
                report.graph = skShell.graph = {
                    getDefaultParent: constFn( {} ),
                    findRelationship: constFn( "PARENT" )
                };
                skShell.reportProperties = {
                    relations: { relParent: true },
                    getColumns: constFn( [ {
                        data: "value attribute",
                        included: constFn( true ),
                        getter: function( cell, parent, resource ) {

                            return resource.getAttribute( "value" );

                        }
                    } ] )
                };

            } );

            describe( '"who" report type', function() {

                beforeEach( function() {

                    skShell.reportProperties.reportType = "who";

                } );

                it( "it should include resources from whatbox", function() {

                    skShell.reportProperties.boxType = { whatbox: true };

                    var data = report.getData();

                    expect( report.columns ).toEqual( [ jasmine.objectContaining( { data: "value attribute" } ) ] );
                    expect( data ).toEqual( {
                        result: [
                            { "value attribute": "what 1" },
                            { "value attribute": "what 2" }
                        ],
                        columns: [ jasmine.objectContaining( { data: "value attribute" } ) ]
                    } );

                } );

                it( "it should include resources from whybox", function() {

                    skShell.reportProperties.boxType = { whybox: true };

                    var data = report.getData();

                    expect( report.columns ).toEqual( [ jasmine.objectContaining( { data: "value attribute" } ) ] );
                    expect( data ).toEqual( {
                        result: [
                            { "value attribute": "why 1" },
                            { "value attribute": "why 2" }
                        ],
                        columns: [ jasmine.objectContaining( { data: "value attribute" } ) ]
                    } );

                } );

                it( "it should include resources from sticky", function() {

                    skShell.reportProperties.boxType = { stickynote: true };

                    var data = report.getData();

                    expect( report.columns ).toEqual( [ jasmine.objectContaining( { data: "value attribute" } ) ] );
                    expect( data ).toEqual( {
                        result: [
                            { "value attribute": "sticky 1" },
                            { "value attribute": "sticky 2" }
                        ],
                        columns: [ jasmine.objectContaining( { data: "value attribute" } ) ]
                    } );

                } );

            } );

        } );

    } );

    describe( "skReport (integration)", function() {

        var injector = new Injector();

        beforeEach( injector.reset );

        var $;
        var mxCodec;
        var mxUtils;
        var skGraph;
        var skShell;
        var skReport;
        var skRolesManager;

        beforeEach( injector.run(
            function( _$ /*jquery*/,
                      _mxCodec /*mxCodec*/,
                      _mxUtils /*mxUtils*/,
                      _skGraph /*skGraph/skGraph*/,
                      _skReport /*skAnalytics/skReport*/,
                      _skRolesManager /*skGraphEditor/skRolesManager*/,
                      _skShell /*skShell*/) {

                $ = _$;
                mxCodec = _mxCodec;
                mxUtils = _mxUtils;
                skGraph = _skGraph;
                skReport = _skReport;
                skRolesManager = _skRolesManager;
                skShell = _skShell;

            }
        ) );

        // This module extends mxCell prototype, required for reports
        beforeEach( injector.run( [ "skGraph/skGraphCell" ], function() {} ) );

        var $container, report;

        beforeEach( function() {

            $container = $( "<div></div>" );
            document.body.appendChild( $container[ 0 ] );

        } );

        afterEach( function() {

            if ( report && report.dataTable ) {

                report.dataTable.destroy();

            }
            $container.remove();

        } );

        it( "should create a report", injector.run( [ "text!fixtures/employee-onboarding.skore" ], function( content ) {

            // Minimum setup to produce a report
            skShell.graph = new skGraph();
            skShell.rolesManager = new skRolesManager();

            var doc = mxUtils.parseXml( content );
            var dec = new mxCodec( doc );

            dec.decode( doc.documentElement, skShell.graph.getModel() );

            report = new skReport( $container[ 0 ] );

            // Sanity checks
            var $table = $container.find( "table.dataTable" );

            expect( $table ).toExist();
            expect( $table.find( "thead th" ) ).toHaveLength( 6 ); // 6 columns
            expect( $table.find( "tbody tr" ) ).toHaveLength( 8 ); // 8 rows

            // Todo: define exactly the expected result of running a report against employee-onboarding.skore
            // create several examples / different reports

        } ) );

    } );

} );
