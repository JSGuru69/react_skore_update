
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skModal_ReportCustomizer = require( "./skModal_ReportCustomizer" );
    var skModal_ReportCustomizerHtml = require( "text!./skModal_ReportCustomizer.html" );
    var skReport = require( "./skReport" );
    var skShell = require( "skShell" );

    function skAnalyticsReport( graph ) {

        console.log( "skAnalyticsReport" );

        /*jshint validthis:true*/
        this.graph = graph;

        this.loadReport( "what" );

    }

    skAnalyticsReport.prototype.report = null;

    skAnalyticsReport.prototype.loadReport = function loadReportF( reportType ) {

        // Finds the active tab
        reportType = reportType || $( ".skReportTabs .active a" ).data( "reporttype" ); // Extract info from data-* attributes

        /*jshint validthis:true*/
        this.report = new skReport(
            $( ".skModalReportPlaceholder" )[ 0 ],
            reportType,
            this.graph );

        this.initUI( this.report );

    };

    skAnalyticsReport.prototype.initUI = function initUIF( report ) {

        $( ".reportButton" ).off( "click" );

        $( ".warningForNotAllowSave" ).toggle( !skShell.allowSave );

        /*
         * Export button
         *
         */

        var saveReport = function saveReportF( i ) {

            report.dataTable.buttons( i ).trigger();

        };

        // Order:
        // 'copyHtml5',
        // 'excelHtml5',
        // 'csvHtml5',
        // 'pdfHtml5'
        $( ".skReportClipboard" ).off( "click" );
        $( ".skReportClipboard" ).on( "click", function() {

            saveReport( 0 );

        } );

        $( ".skReportSaveAs" ).off( "click" );
        $( ".skReportSaveAs" ).on( "click", function() {

            saveReport( 1 );

        } );

        $( ".skReportSaveAsCSV" ).off( "click" );
        $( ".skReportSaveAsCSV" ).on( "click", function() {

            saveReport( 2 );

        } );

        $( ".skReportSaveAsPDF" ).off( "click" );
        $( ".skReportSaveAsPDF" ).on( "click", function() {

            saveReport( 3 );

        } );

        /*
         * Tabs to switch report type
         *
         */

        $( ".skReportTabs a" ).off( "click" );
        $( ".skReportTabs a" ).on( "click", function( e ) {

            console.log( "report tabs clicked " );

            e.preventDefault();

            var reporttype = $( this ).data( "reporttype" );

            // Ui for tabs
            $( ".skReportTabs li" ).removeClass( "active" );
            $( this ).parent().addClass( "active" );

            // Explanation
            $( ".skReportExpl" ).hide();
            $( ".skExpl-" + reporttype ).show();

            // Reset search
            $( ".skSearchInReport" ).val( "" );

            report.update( reporttype );

        } );

        /*
         * Search
         *
         */

        $( ".skSearchInReport" ).off( "keyup" );
        $( ".skSearchInReport" ).on( "keyup", function() {

            console.log( "keyup", $( this ).val() );

            report.dataTable.search( $( this ).val() ).draw();

            // Fixme add highlight plugin to highlight the search resutls

        } );

        /*
         * Customizer modal
         *
         */

        console.log( "append analytics report " );

        if ( $( "#skModal_ReportCustomizer" ).length === 0 ) {

            $( document.body ).append( $( skModal_ReportCustomizerHtml ) );

        }

        $( "#skModal_ReportCustomizer" ).off( "shown.bs.modal" );
        $( "#skModal_ReportCustomizer" ).on( "shown.bs.modal", function( /*event*/) {

            skShell.skModal_ReportCustomizer = new skModal_ReportCustomizer( report );

        } );

        $( "#skModal_ReportCustomizer" ).off( "hide.bs.modal" );
        $( "#skModal_ReportCustomizer" ).on( "hide.bs.modal", function( /*event*/) {

            window.localStorage.setItem( "skReportColumns", JSON.stringify( report.columns ) );
            report.update();

        } );

   };

   return skAnalyticsReport;

} );
