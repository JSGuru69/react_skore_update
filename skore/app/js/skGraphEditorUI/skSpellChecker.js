
define( function() {

    "use strict";

    // https://www.npmjs.com/package/electron-spell-check-provider

    var selection;
    var wasDisabled;

    var resetSelection = function resetSelectionF() {

        selection = {
            isMisspelled: false,
            spellingSuggestions: []
        };

    };

    var contexMenuListener = function contexMenuListenerF( e ) {

        if ( ELECTRON ) {

            // Only show the context menu in text editors.
            if ( !e.target.closest( 'textarea, input, [contenteditable="true"]' ) ) {

                return;

            }

            var buildEditorContextMenu = window.requireNode( "electron" ).remote.require( "electron-editor-context-menu" );
            var menu = buildEditorContextMenu( selection );

            // The 'contextmenu' event is emitted after 'selectionchange' has fired but possibly before the
            // visible selection has changed. Try to wait to show the menu until after that, otherwise the
            // visible selection will update after the menu dismisses and look weird.

            setTimeout( function() {

                menu.popup( window.requireNode( "electron" ).remote.getCurrentWindow() );

            }, 30 );

        }

    };

    function pause() {

        wasDisabled = isEnabled();
        setEnabled( false );

    }

    function resume() {

        setEnabled( wasDisabled );

    }

    function isEnabled() {

        if ( ELECTRON ) {

            if ( localStorage.hasOwnProperty( "spellcheckenabled" ) === false || (
                localStorage.hasOwnProperty( "spellcheckenabled" ) === true  &&
                parseInt( localStorage.getItem( "spellcheckenabled" ), 10 ) === 1 ) ) {

                return true;

            } else {

                return false;

            }

        }

    }

    function setEnabled( enable ) {

        if ( ELECTRON ) {

            resetSelection();

            if ( enable ) {

                localStorage.setItem( "spellcheckenabled", 1 );

                // Reset the selection when clicking around, before the spell-checker runs and the context menu shows.
                window.removeEventListener( "mousedown", resetSelection );
                window.addEventListener( "mousedown", resetSelection );

                var SpellCheckProvider = window.requireNode( "electron-spell-check-provider" );
                window.requireNode( "electron" ).webFrame.setSpellCheckProvider(
                    "en-US",
                    true,
                    new SpellCheckProvider( "en-US" ).on( "misspelling", function( suggestions ) {

                        // Prime the context menu with spelling suggestions _if_ the user has selected text. Electron
                        // may sometimes re-run the spell-check provider for an outdated selection e.g. if the user
                        // right-clicks some misspelled text and then an image.
                        if ( window.getSelection().toString() ) {

                            selection.isMisspelled = true;

                            // Take the first three suggestions if any.
                            selection.spellingSuggestions = suggestions.slice( 0, 3 );

                        }

                    } )
                );

                window.removeEventListener( "contextmenu", contexMenuListener );
                window.addEventListener( "contextmenu", contexMenuListener );

            } else {

                localStorage.setItem( "spellcheckenabled", 0 );

                window.removeEventListener( "mousedown", resetSelection );

                // fake spell check returning true all the time
                window.requireNode( "electron" ).webFrame.setSpellCheckProvider( "en-US", false, {

                    spellCheck: function() {

                        return true;

                    }

                } );

                window.removeEventListener( "contextmenu", contexMenuListener );

            }

        }

    }

    return {
        isEnabled: isEnabled,
        pause: pause,
        resume: resume,
        setEnabled: setEnabled
    };

} );
