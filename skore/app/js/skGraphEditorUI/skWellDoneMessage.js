define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCell = require( "mxCell" );

        // SkShell = require("skShell");

    /**
     * @constructor
     */
    function skWellDoneMessage() {

        /*jshint validthis:true*/

        this.init();

    }

    skWellDoneMessage.prototype.box = null;
    skWellDoneMessage.prototype.shell = null;
    skWellDoneMessage.prototype.hintContainer$ = null;

    skWellDoneMessage.prototype.init = function initF() {

        this.box = document.createElement( "div" );
        this.box.className = "wellDone";
        this.box.style.position = "absolute";
        document.body.appendChild( this.box );

    };

    skWellDoneMessage.prototype.setHintContainer = function setHintContainerF( container$ ) {

        this.hintContainer$ = container$;

    };

    skWellDoneMessage.prototype.clearHintContainer = function clearHintContainerF() {

        this.hintContainer$ = null;

    };

    skWellDoneMessage.prototype.hey = function heyF( element, text ) {

        // Var that = this;

        /*
         * Find the element
         *
         */

        var element$ = null;

        // Cell
        if ( element instanceof mxCell ) {

            element$ = $( "#box" + element.id );

            if ( element$.length === 0 ) {

                element$ = $( window.document.body );

            }

        }

        // If empty, probably the hintContainer
        else if ( element == null && this.hintContainer$ != null ) {

            element$ = this.hintContainer$;

        }

        // If really empty
        else if ( element == null ) {

            element$ = $( window.document.body );

        }

        // If any object...
        else if ( !( element instanceof $ ) ) {

            element$ = $( element );

        } else {

            element$ = element;

        }

        // Pick a random message
        // displays it
        // animate the box
        var t = text || this.messages[ Math.floor( Math.random() * this.messages.length ) ];

        var b = $( this.box )
            .show()
            .html( t )
            .position( {
                my: "center center",
                at: "center center",
                of: element$
            } );

        if ( navigator.userAgent.indexOf( "MSIE 9" ) >= 0 ) {

            b.fadeOut( 1000 );

        } else {

            b.addClass( "wellDone fadeOutUp animated" );

        }

        window.setTimeout( function() {

            b.hide();
            b.removeClass( "fadeOutUp" );
            b.removeClass( "animated" );

        }, 1000 );

    };

    skWellDoneMessage.prototype.messages = [
        "+1",
        "Ace!",
        "Applaudable!",
        "Astonishing!",
        "Awesome!",
        "Beaut!",
        "Bonzer!",
        "Bosting!",
        "Bravo!",
        "Brilliant!",
        "Class!",
        "Congratulations!",
        "Cool!",
        "Extraordinary!",
        "Fab!",
        "Fabulous!",
        "Fantastic!",
        "First class!",
        "First rate!",
        "Formidable!",
        "Ganz richtig! (That's German)",
        "Geil! (That's German)",
        "Genau! (That's German)",
        "Genious!",
        "Grand!",
        "Great",
        "Impressive!",
        "Marvellous!",
        "Matchless!",
        "Mighty!",
        "Neat!",
        "Outstanding!",
        "Phenomenal!",
        "Prodigious!",
        "Rad!",
        "Sensational!",
        "Skore!",
        "Smart!",
        "Smashing!",
        "Spiffing!",
        "Sublime!",
        "Super",
        "Superb!",
        "Swell!",
        "Terrific!",
        "Tip top!",
        "Well done!",
        "Wicked!",
        "Wonderful!",
        "Yeah!"
    ];

    return skWellDoneMessage;

} );
