define( [ "Injector" ], function( Injector ) {

    "use strict";

    var injector = new Injector();

    describe( "skMenu_wings", function() {

        beforeEach( injector.reset );

        var mxCell;
        var mxGraph;
        var skMenu;
        var skShell;

        beforeEach( injector.run(
            function( _mxCell /*mxCell*/,
                      _mxGraph /*mxGraph*/,
                      _skMenu /*skNavigatorUI/skMenu*/,
                      _skShell /*skShell*/) {

                mxCell = _mxCell;
                mxGraph = _mxGraph;
                skMenu = _skMenu;
                skShell = _skShell;
                skMenu.prototype.bindButton = jasmine.createSpy( "bindButton" );

            }
        ) );

        // Require skMenu_wings to update skMenu prototype
        // todo: skMenu_wings should probably be made independent of skMenu
        beforeEach( injector.run( [ "skGraphEditorUI/skMenu_wings" ], function() {} ) );

        it( "should update itself on root change", function() {

            var graph = new mxGraph();
            var cell = graph.addCell( new mxCell() );

            skShell.graph = graph;
            var menu = new skMenu( skShell, {} );

            spyOn( menu, "updateInputsAndOutputsBoxes" );
            graph.getView().setCurrentRoot( cell );
            expect( menu.updateInputsAndOutputsBoxes ).toHaveBeenCalled();
            expect( skMenu.prototype.bindButton ).toHaveBeenCalled();

        } );

        it( "should not update itself if graph is disabled", function() {

            var graph = new mxGraph();

            graph.setEnabled( false );
            var cell = graph.addCell( new mxCell() );

            skShell.graph = graph;
            var menu = new skMenu( skShell, {} );

            spyOn( menu, "updateInputsAndOutputsBoxes" );
            graph.getView().setCurrentRoot( cell );
            expect( menu.updateInputsAndOutputsBoxes ).not.toHaveBeenCalled();

        } );

    } );

} );
