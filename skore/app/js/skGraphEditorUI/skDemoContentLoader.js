
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skShell = require( "skShell" );

    function skDemoContent( navigatorUI ) {

        /*jshint validthis:true*/

        // var that = this;

        var demoContent = [

            {
                title: "Employee Onboarding",
                fileName: "EmployeeOnboarding.skore",
                skore: require( "text!demoContent/EmployeeOnboarding.skore" ),
                description: "HR - getting ready for a new employee arrival",
                isPublic: true
            },
            {
                title: "Training company process",
                fileName: "training_company_process.skore",
                skore: require( "text!demoContent/training_company_process.skore" ),
                description: "Process for a company delivering training courses",
                isPublic: true
            },
            {
                title: "Design Process",
                fileName: "DesignProcess.skore",
                skore: require( "text!demoContent/DesignProcess.skore" ),
                description: "Design & build a new feature",
                isPublic: true
            },
            {
                title: "Transformation Approach",
                fileName: "TransformationApproach.skore",
                skore: require( "text!demoContent/TransformationApproach.skore" ),
                description:"Understand the current situation, define and implement a strategy",
                isPublic: true
            },
            {
                title: "New Product Development",
                fileName: "npd.skore",
                skore: require( "text!demoContent/npd.skore" ),
                description: "A process from a wikipedia article",
                isPublic: true
            },
            {
                title: "Projet Rouge",
                fileName: "projet-rouge.skore",
                skore: require( "text!demoContent/projet-rouge.skore" ),
                description: "",
                isPublic: false
            },
            {
                title: "full feature test ",
                fileName: "fullFeaturesTests.skore",
                skore: require( "text!demoContent/fullFeaturesTests.skore" ),
                description: "",
                isPublic: false
            },
            {
                title: "Skore of Skore app",
                fileName: "SkoreOfSkoreApp.skore",
                skore: require( "text!demoContent/SkoreOfSkoreApp.skore" ),
                description: "Skore of all user journeys in Skore",
                isPublic: false
            },
            {
                title: "Test navigation",
                fileName: "skNavigationTest.skore",
                skore: require( "text!demoContent/skNavigationTest.skore" ),
                description: "Test the navigation in skore",
                isPublic: false
            },
            {
                title: "RACI Conversion",
                fileName: "RACIFileConversion.skore",
                skore: require( "text!demoContent/RACIFileConversion.skore" ),
                description: "Test opening old RACI file",
                isPublic: false
            },
            {
                title: "RATSI Conversion",
                fileName: "RATSIFileConversion.skore",
                skore: require( "text!demoContent/RATSIFileConversion.skore" ),
                description: "Test opening old RATSI file",
                isPublic: false
            },
            {
                title: "Custom MRT Conversion",
                fileName: "CustomFileConversion.skore",
                skore: require( "text!demoContent/CustomFileConversion.skore" ),
                description: "Test opening old Custom MRT file",
                isPublic: false
            },
            {
                title: "Test SEARCH",
                fileName: "test-search.skore",
                skore: require( "text!demoContent/test-search.skore" ),
                description: "Test search feature",
                isPublic: false
            },
            {
                title: "Attachments test",
                fileName: "attachmentstests.skore",
                skore: require( "text!demoContent/attachmentstests.skore" ),
                description: "Test ATTACHMENTS feature",
                isPublic: false
            }
        ];

        var firstExample = $( ".skSideMenu_loadExample" );

        var makeLink = function makeLinkF( e ) {

            if ( DEV || e.isPublic ) {

                $( "<p><i class='fa fa-map-o'></i> &nbsp;" +
                    e.title +
                    '<span class="explanation">' + e.description + "</span>" +
                "</p>" ).on( "click", function() {

                    // SkShell.currentFilePath = "localOnly";
                    var tmpFP = "localOnly";

                    if ( ELECTRON ) {

                        var app = window.requireNode( "electron" ).remote.app;

                        if ( DEV ) {

                            tmpFP = "" + app.getAppPath() + "/js/demoContent/" + e.fileName;

                        } else {

                            tmpFP = app.getPath( "temp" ) + "com.the-skore.skore/" + e.fileName ;

                            // Copy the file out of Electron
                            skShell.copyFileOutOfElectron(
                                "js/demoContent/" + e.fileName,
                                tmpFP );

                        }

                        navigatorUI.graphManager.openFromFilePath( tmpFP );

                    } else {

                        navigatorUI.graphManager.openGraph( {
                            xml:e.skore,
                            source: "demo",
                            filePath:skShell.currentFilePath } );

                    }

                } ).insertAfter( firstExample );

            }

        };

        for ( var i = 0; i < demoContent.length; i++ ) {

            makeLink( demoContent[ i ] );

        }

    }

    return skDemoContent;

} );
