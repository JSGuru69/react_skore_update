
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxClient = require( "mxClient" );
    var mxClipboard = require( "mxClipboard" );
    var mxCodec = require( "mxCodec" );
    var mxEvent = require( "mxEvent" );
    var mxGraph = require( "mxGraph" );
    var mxGraphModel = require( "mxGraphModel" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );

    // Var mxResources = require("mxResources");

    return function setupClipboard( graphManager ) {

        var codec, model;

        /*
         * Inspired from clipboard.html example
         *
         */

        // Public helper method for shared clipboard.
        mxClipboard.cellsToString = function cellsToStringF( cells )
        {

            console.log( "skClipboard", "cellsToString" );

            codec = new mxCodec();
            model = new mxGraphModel();
            var parent = model.getChildAt( model.getRoot(), 0 );

            for ( var i = 0; i < cells.length; i++ )
            {

                model.add( parent, cells[ i ] );

            }

            // need to add the roles from the role manager in the "value"
            model.filterDescendants( function( cell ) {

                var roleCandidate = $( "role", cell.value );
                if ( roleCandidate.length > 0 ) {

                    $( roleCandidate ).each( function( j, role ) {

                        var lid = $( role ).attr( skConstants.LOCAL_ID );
                        $( role ).attr( skConstants.VALUE, skUtils.readValue( graphManager.getCurrentGraph().roleManager.getRoleByLocalID( lid ) ) );
                        $( role ).removeAttr( skConstants.LOCAL_ID );

                    } );

                }

            } );

            return mxUtils.getXml( codec.encode( model ) );

        };

        // Focused but invisible textarea during control or meta key events
        var textInput = document.createElement( "textarea" );

        mxUtils.setOpacity( textInput, 0 );
        textInput.setAttribute( "class", "selectionAllowed skClipboard" );
        textInput.style.width = "1px";
        textInput.style.height = "1px";
        var restoreFocus = false;
        var gs = mxGraph.prototype.gridSize;
        var lastPaste = null;
        var dx = 0;
        var dy = 0;

        // resets offset on root change
        graphManager.addListener( "navigationDone", function() {

            // dx = -gs;
            dx = 0;

            // dy = -gs;
            dy = 0;

        } );

        // Workaround for no copy event in IE/FF if empty
        textInput.value = " ";

        // Shows a textare when control/cmd is pressed to handle native clipboard actions
        mxEvent.addListener( document, "keydown", function( evt )
        {

            // No dialog visible
            var source = mxEvent.getSource( evt );

            if ( $( document.body ).hasClass( "modal-open" ) ) {

                console.log( "modal open" );

                return;

            }

            var graph = graphManager.getCurrentGraph();

            if ( graph && graph.isEnabled() && !graph.isMouseDown && !graph.isEditing() && !window.textEditing( evt ) && source.nodeName != "INPUT" )
            {

                if ( evt.keyCode == 224 /* FF */ || ( !mxClient.IS_MAC && evt.keyCode == 17 /* Control */) || ( mxClient.IS_MAC && evt.keyCode == 91 /* Meta */) )
                {

                    console.log( "skClipboard", "keydown", restoreFocus );

                    // Cannot use parentNode for check in IE
                    // if (!restoreFocus)
                    // {
                        // skShell.copy_selectMe();
                        // Avoid autoscroll but allow handling of events
                        textInput.style.position = "absolute";
                        textInput.style.left = ( graph.container.scrollLeft + 10 ) + "px";
                        textInput.style.top = ( graph.container.scrollTop + 10 ) + "px";
                        graphManager.getCurrentGraph().container.appendChild( textInput );

                        restoreFocus = true;
                        textInput.focus();
                        textInput.select();

                    // }

                    console.info( "textinput ready" );

                }

            }

        } );

        $( document ).on( "keyup", function( event ) {

            var evt = $( event )[ 0 ];

            if ( $( evt.target ).hasClass( "skClipboard" ) && restoreFocus && ( evt.keyCode == 224 /* FF */ || evt.keyCode == 17 /* Control */ ||
                evt.keyCode == 91 /* Meta */) )
            {

                restoreFocus = false;

                if ( !graphManager.getCurrentGraph().isEditing() )
                {

                    graphManager.getCurrentGraph().container.focus();

                }

                textInput.parentNode.removeChild( textInput );

            }

        } );

        // Inserts the XML for the given cells into the text input for copy
        skShell.copyCells = function copyCellsF( graph, cells ) {

            console.log( "skClipboard", "copyCells" );

            if ( cells.length > 0 )
            {

                var clones = graph.cloneCells( cells );

                // Checks for orphaned relative children and makes absolute
                for ( var i = 0; i < clones.length; i++ )
                {

                    var state = graph.view.getState( cells[ i ] );

                    if ( state != null )
                    {

                        var geo = graph.getCellGeometry( clones[ i ] );

                        if ( geo != null && geo.relative )
                        {

                            geo.relative = false;
                            geo.x = state.x / state.view.scale - state.view.translate.x;
                            geo.y = state.y / state.view.scale - state.view.translate.y;

                        }

                    }

                }

                textInput.value = mxClipboard.cellsToString( clones );

            }

            textInput.select();
            lastPaste = textInput.value;

            return textInput.value;

        };

        // Handles copy event by putting XML for current selection into text input
        mxEvent.addListener( textInput, "copy", mxUtils.bind( this, function( ) {

            console.log( "skClipboard", "copy" );

            var graph = graphManager.getCurrentGraph();

            if ( graph.isEnabled() && !graph.isSelectionEmpty() ) {

                skShell.copyCells( graph, mxUtils.sortCells( graph.model.getTopmostCells( graph.getSelectionCells() ) ) );
                dx = 0;
                dy = 0;

            }

        } ) );

        // Handles cut event by removing cells putting XML into text input
        mxEvent.addListener( textInput, "cut", mxUtils.bind( this, function( ) {

            console.log( "skClipboard", "cut" );

            var graph = graphManager.getCurrentGraph();

            if ( graph.isEnabled() && !graph.isSelectionEmpty() ) {

                skShell.copyCells( graph, graph.removeCells() );
                dx = -gs;
                dy = -gs;

            }

        } ) );

        // Merges XML into existing graph and layers
        var importXml = function importXmlF( graph, xml, dx, dy )
        {

            console.log( "skClipboard", "importXml" );

            dx = ( dx != null ) ? dx : 0;
            dy = ( dy != null ) ? dy : 0;
            var cells = [];

            try
            {

                var doc = mxUtils.parseXml( xml );
                var node = doc.documentElement;

                if ( node != null )
                {

                    model = new mxGraphModel();
                    codec = new mxCodec( node.ownerDocument );

                    codec.decode( node, model );

                    var childCount = model.getChildCount( model.getRoot() );
                    var targetChildCount = graph.model.getChildCount( graph.model.getRoot() );

                    // Fixme : colin : change the parent... it always inserts in top level

                    // Merges existing layers and adds new layers
                    graph.model.beginUpdate();

                    try
                    {

                        var children;

                        for ( var i = 0; i < childCount; i++ )
                        {

                            var parent = model.getChildAt( model.getRoot(), i );

                            // Adds cells to existing layers if not locked
                            if ( targetChildCount > i )
                            {

                                // Inserts into active layer if only one layer is being pasted
                                var target = ( childCount == 1 ) ? graph.getDefaultParent() : graph.model.getChildAt( graph.model.getRoot(), i );

                                if ( !graph.isCellLocked( target ) )
                                {

                                    children = model.getChildren( parent );
                                    cells = cells.concat( graph.importCells( children, dx, dy, target ) );

                                }

                            } else
                            {

                                // Delta is non cascading, needs separate move for layers
                                parent = graph.importCells( [ parent ], 0, 0, graph.model.getRoot() )[ 0 ];
                                children = graph.model.getChildren( parent );
                                graph.moveCells( children, dx, dy );
                                cells = cells.concat( children );

                            }

                        }

                        // re-align the roles in the new world
                        graph.roleManager.initModel();

                    }

                    finally
                    {

                        graph.model.endUpdate();
                        codec = null;
                        model = null;

                    }

                }

            }

            catch ( e )
            {

                // This.handleError(e, mxResources.get('invalidOrMissingFile'));
                throw e;

            }

            return cells;

        };

        // Parses and inserts XML into graph
        skShell.pasteText = function pasteTextF( graph, text ) {

            console.info( "skClipboard", "pasteText" );

            skShell.setStatus( "Pasting... this can take a while..." );

            var xml = mxUtils.trim( text );

            // Not used ? // Colin
            // var x = graph.container.scrollLeft / graph.view.scale - graph.view.translate.x;
            // var y = graph.container.scrollTop / graph.view.scale - graph.view.translate.y;

            if ( xml.length > 0 )
            {

                if ( lastPaste != xml )
                {

                    lastPaste = xml;
                    dx = 0;
                    dy = 0;

                } else
                {

                    dx += gs;
                    dy += gs;

                }

                // Standard paste via control-v
                if ( xml.substring( 0, 14 ) == "<mxGraphModel>" )
                {

                    var spellcheck;

                    // disable spellcheckr
                    if ( ELECTRON ) {

                        spellcheck = require( "skGraphEditorUI/skSpellChecker" );
                        spellcheck.pause();

                    }

                    graph.setSelectionCells( importXml( graph, xml, dx, dy ) );

                    setTimeout( function() {

                        graph.scrollCellToVisible( graph.getSelectionCell() );

                    }, 100 );

                    if ( ELECTRON ) {

                        spellcheck.resume();

                    }

                    skShell.setStatus( "Paste done!" );

                }

            }

        };

        // Cross-browser function to fetch text from paste events
        var extractGraphModelFromEvent = function extractGraphModelFromEventF( evt )
        {

            console.log( "skClipboard", "extractGraphModelFromEvent" );
            var data = null;

            if ( evt != null )
            {

                var provider = ( evt.dataTransfer != null ) ? evt.dataTransfer : evt.clipboardData;

                if ( provider != null )
                {

                    if ( document.documentMode == 10 || document.documentMode == 11 )
                    {

                        data = provider.getData( "Text" );

                    } else
                    {

                        data = ( mxUtils.indexOf( provider.types, "text/html" ) >= 0 ) ? provider.getData( "text/html" ) : null;

                        if ( mxUtils.indexOf( provider.types, "text/plain" && ( data == null || data.length === 0 ) ) )
                        {

                            data = provider.getData( "text/plain" );

                        }

                    }

                }

            }

            return data;

        };

        // Handles paste event by parsing and inserting XML
        mxEvent.addListener( textInput, "paste", function( evt )
        {

            var graph = graphManager.getCurrentGraph();

            console.log( "skClipboard", "paste" );

            if ( $( evt.target ).hasClass( "skClipboard" ) ) {

                console.info( "textinput pasted" );

                // Clears existing contents before paste - should not be needed
                // because all text is selected, but doesn't hurt since the
                // actual pasting of the new text is delayed in all cases.
                textInput.value = "";

                if ( graph.isEnabled() && !graph.isEditing() )
                {

                    var xml = extractGraphModelFromEvent( evt );

                    if ( xml !== null && xml.length > 0 )
                    {

                        skShell.pasteText( graph, xml );

                    } else
                    {

                        // Timeout for new value to appear
                        window.setTimeout( mxUtils.bind( this, function()
                        {

                            skShell.pasteText( graph, textInput.value );

                        } ), 0 );

                    }

                }

                textInput.select();

            }

        } );

    };

} );
