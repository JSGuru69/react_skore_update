define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCell = require( "mxCell" );
    var mxClient = require( "mxClient" );
    var mxEvent = require( "mxEvent" );
    var skHint = require( "skNavigatorUI/skHint" );
    var skShell = require( "skShell" );
    var skWellDoneMessage = require( "skGraphEditorUI/skWellDoneMessage" );

     var panListener = function() {

        skShell.tutorial.stepDone( skShell.tutorial.SCREEN_PANNED );

    };

    var scrollListener = function() {

        skShell.tutorial.stepDone( skShell.tutorial.SCREEN_PANNED );

    };

    var removeListener = function() {

        skShell.tutorial.stepDone( skShell.tutorial.BOX_DELETED_WITH_BUTTON );

    };

    var moveListener = function( event, sender ) {

        // Hack : to prevent the event to fired twice we do a check
        if ( skShell.tutorial.expectedCode && this.view.getState( sender.getProperty( "cells" )[ 0 ] ) ) {

            skShell.tutorial.stepDone( skShell.tutorial.BOX_MOVED_ON_CANVAS, { "cell": sender.getProperty( "cells" )[ 0 ] } );

        }

    };

    var droppedListener = function( sender, event ) {

        skShell.tutorial.stepDone(
            skShell.tutorial.BOX_DROPPED_FROM_CARDSHOE,
            { "cell": event.getProperty( "cells" )[ 0 ] } );

    };

    var navigationListener = function() {

        skShell.tutorial.stepDone( skShell.tutorial.NAVIGATE );

    };

    var selectionChangeListener = function( sender, event ) {

        var cellsAdded = event.getProperty( "removed" ); // yeah, known bug...

        if ( cellsAdded.length == 1 && cellsAdded[ 0 ].isWhatbox() ) {

            skShell.tutorial.stepDone(
                skShell.tutorial.BOX_SELECTED,
                { cell: cellsAdded[ 0 ] }
            );

        }

    };

    /**
     *
     * This is where we have all the code for the step-by-step
     * tutorial in the software
     *
     * Each step are described in the switch()
     *
     */
    /**
     * @constructor
     */
    function skGraphEditorTutorial( navigatorUI, doNotStart ) {

        /*jshint validthis:true*/

        // SkShell = shell || skShell;
        this.graph = navigatorUI.getCurrentGraph();
        this.navigatorUI = navigatorUI;

        var stopButton = $( '<li class="stopTutorialButton" style="display:none"><a><i class="fa fa-close"></i> End tutorial</a></li>' );

        $( ".skWingMenuLeft .navbar-nav" ).show().append( stopButton );
        stopButton.fadeIn( 1000 );

        var that = this;

        stopButton.on( "click", function() {

            that.end();

        } );

        // Hint container
        if ( skShell.hint == null ) {

            skShell.hint = new skHint( this.graph );

        }

        // Well done message
        if ( skShell.wellDoneMessage == null ) {

            skShell.wellDoneMessage = new skWellDoneMessage();

        }
        skShell.wellDoneMessage.setHintContainer( skShell.hint.container$ );

        this.randomActionDone = [];

        if ( !doNotStart ) {

            $( ".skStopTutorial" ).show();
            this.goToStep( 1 );

        }

        this.graph.panningHandler.addListener( mxEvent.PAN, panListener );
        this.graph.panningHandler.addListener( mxEvent.PAN_END, panListener );

        this.graph.addListener( "navigationDone", navigationListener );

        this.graph.addListener( mxEvent.MOVE_CELLS, moveListener );
        this.graph.addListener( mxEvent.CELLS_REMOVED, removeListener );
        mxEvent.addListener( this.graph.container, "scroll", scrollListener );

        this.graph.getSelectionModel().addListener( mxEvent.CHANGE, selectionChangeListener );

        navigatorUI.graphManager.addListener( "droppedListener", droppedListener );

    }

    skGraphEditorTutorial.prototype.graph = null;
    skGraphEditorTutorial.prototype.navigatorUI = null;

    skGraphEditorTutorial.prototype.end = function endF() {

        if ( this.icon_for_hint_box_position !== null ) {

            this.icon_for_hint_box_position.parentNode.removeChild( this.icon_for_hint_box_position );
            this.icon_for_hint_box_position = null;

        }

        // Well done
        this.displayWellDoneMessage( $( window ), "Bye bye" );
        skShell.wellDoneMessage.clearHintContainer();

        // End tutorial button
        // this.endTutorialButton.destroy();
        $( ".stopTutorialButton" ).hide();

        // Actual hint
        skShell.hint.hide();

        if ( !DEV && !ELECTRON && ga ) {

            ga( "send", "event", "SkoreAction", "tutorial", "endTutorial", this.currentStep );

        }

        // Skore_tutorialMode = false;
        this.currentStep = 0;
        skShell.tutorial = null;

        localStorage.setItem( "DONTSHOWAGAINtutorial", true );

        this.graph.panningHandler.removeListener( panListener );
        this.graph.removeListener( moveListener );
        this.graph.removeListener( removeListener );
        this.graph.removeListener( navigationListener );
        mxEvent.removeListener( this.graph.container, "scroll", scrollListener );
        this.graph.getSelectionModel().removeListener( selectionChangeListener );
        this.navigatorUI.graphManager.removeListener( droppedListener );

    };

    skGraphEditorTutorial.prototype.graph = null;

    skGraphEditorTutorial.prototype.hintContainer$ = null;

    /*****
     *
     * CODE sent by the various events to tell when an action is done
     *
     *****/

    skGraphEditorTutorial.prototype.any = "any";
    skGraphEditorTutorial.prototype.CREATE_TAB_OPENED = "CREATE_TAB_OPENED";
    skGraphEditorTutorial.prototype.BOX_DROPPED_FROM_CARDSHOE = "BOX_DROPPED_FROM_CARDSHOE";
    skGraphEditorTutorial.prototype.BOX_MOVED_ON_CANVAS = "BOX_MOVED_ON_CANVAS";
    skGraphEditorTutorial.prototype.BOX_CREATED_WITH_DND_FROM_CONNECT_ARROW = "BOX_CREATED_WITH_DND_FROM_CONNECT_ARROW";
    skGraphEditorTutorial.prototype.BOX_CREATED_WITH_SPACE_BAR = "BOX_CREATED_WITH_SPACE_BAR";
    skGraphEditorTutorial.prototype.BOX_DELETED_WITH_BUTTON = "BOX_DELETED_WITH_BUTTON";
    skGraphEditorTutorial.prototype.BOX_TAB_PRESSED = "BOX_TAB_PRESSED";
    skGraphEditorTutorial.prototype.STOP_EDITING = "STOP_EDITING";
    skGraphEditorTutorial.prototype.NEXT_Y_BOX_IS_CREATED = "NEXT_Y_BOX_IS_CREATED";
    skGraphEditorTutorial.prototype.W_BOX_IS_CREATED = "W_BOX_IS_CREATED";
    skGraphEditorTutorial.prototype.SCREEN_PANNED = "SCREEN_PANNED";
    skGraphEditorTutorial.prototype.TUT_NEXT_PRESSED = "TUT_NEXT_PRESSED";
    skGraphEditorTutorial.prototype.ATT_PANEL_OPEN = "ATT_PANEL_OPEN";
    skGraphEditorTutorial.prototype.ATT_PANEL_CLOSED = "ATT_PANEL_CLOSED";
    skGraphEditorTutorial.prototype.NAVIGATE = "NAVIGATE";
    skGraphEditorTutorial.prototype.LINE_RESIZE_LESSONLEARNT = "LINE_RESIZE_LESSONLEARNT";
    skGraphEditorTutorial.prototype.BOX_SELECTED = "BOX_SELECTED";

    /*
     * End CODE
     */

    skGraphEditorTutorial.prototype.currentStep = 1;
    skGraphEditorTutorial.prototype.dataForNextStep = null;
    skGraphEditorTutorial.prototype.callback = null;
    skGraphEditorTutorial.prototype.expectedCode = null;
    skGraphEditorTutorial.prototype.justWaitForNextAction = false;
    skGraphEditorTutorial.prototype.randomActionDone = null;
    skGraphEditorTutorial.prototype.randomActionCounter = 0;

    skGraphEditorTutorial.prototype.icon_for_hint_box_position = null;

    skGraphEditorTutorial.prototype.updateHintBox = function updateHintBoxF( refObject, text, direction, continueButton, callback ) {

        skShell.hint.hey( refObject, text, direction, continueButton, callback );

    };

    skGraphEditorTutorial.prototype.displayWellDoneMessage = function displayWellDoneMessageF( $element, text ) {

        skShell.wellDoneMessage.hey( $element, text );

    };

    skGraphEditorTutorial.prototype.hideHint = function hideHintF() {

        skShell.hint.hide();

    };

    skGraphEditorTutorial.prototype.goToNextStep = function goToNextStepF() {

        this.currentStep++;
        this.goToStep( this.currentStep );

    };

    skGraphEditorTutorial.prototype.stepDone = function stepDoneF( codeSentByEvent, args ) {

        // Expected Code or awaited code ?
        if ( this.justWaitForNextAction ) {

            // Hides hint
            this.hideHint();

            // Saves the arguments
            this.dataForNextStep = args || [];

            console.log( "will go to : " + codeSentByEvent );

            if ( this.randomActionCounter == 3 ) {

                this.goToStep( codeSentByEvent );

                this.randomActionCounter = 0;

            } else {

                this.randomActionCounter++;

            }

            /*
             * JustWaitForNextAction = goes to the particular step...
             *
             */

            // If (this.justWaitForNextAction != null ) {

            //     This.goToStep( this.codeSentByEvent );
            // }

        } else if ( this.expectedCode === this.any || this.expectedCode === codeSentByEvent ) {

            // Hides hint
            this.hideHint();

            // Congratulates
            if ( args && args.cell instanceof mxCell ) {

                this.displayWellDoneMessage( args.cell );

            } else {

                this.displayWellDoneMessage( window );

            }

            // Saves the arguments
            this.dataForNextStep = args || [];

            // Applies the function with the arguments
            if ( this.callback ) {

                // Call back the function
                this.callback( args );

            }

            /*
             * ExpectedCode = goes to the next step
             *
             */

            else if ( this.currentStep > 0 ) {

                this.goToNextStep();

            }

            if ( !DEV && !ELECTRON && ga ) {

                ga( "send", "event", "SkoreAction", "tutorial", "nextStep", this.currentStep );

            }

        }

    };

    skGraphEditorTutorial.prototype.goToStep = function goToStepF( stepToGoTo ) {

        var model,
            cellFound, cell, currentCell,
            text;

        var that = this;

        this.currentStep = stepToGoTo;

        console.log( "case : " + stepToGoTo );

        switch ( stepToGoTo ) {

        case 1:

            // Check in which tab we are
            if ( $( ".skNavTabs .active" ).find( "a" ).attr( "href" ) !== "#create" ) {

                text = {
                    title: "Go to the Create tab",
                    content: [ "to start adding elements on the canvas" ]
                };

                var createTab = $( ".skNavTabs" ).find( "a[href=#create]" );

                createTab.one( "click", function() {

                    skShell.tutorial.stepDone( skShell.tutorial.CREATE_TAB_OPENED );

                } );

                this.updateHintBox( createTab, text, "left" );
                this.expectedCode = this.CREATE_TAB_OPENED;

            } else {

                this.goToNextStep();

            }

        break;

        case 2: // Shows hint to drag & drop from cardshoe

            // Sets the text
            text = {
                title: "add a box to the canvas",
                content: [ "Drag a box, drop it on the canvas" ]
            };

            this.updateHintBox( $( ".cardshoe-box-wb" ).parent(), text, "left" );

            // Sets the code that is expected
            this.expectedCode = this.BOX_DROPPED_FROM_CARDSHOE;

            break;

        case 3 : // Makes sure we are editing a cell

            // Checks if we are already in edit mode
            if ( this.graph.cellEditor.editingCell !== undefined ) {

                this.goToNextStep();

            }

            // Not in edit mode, suggest user to do so
            else if ( this.graph.getSelectionCell() !== undefined ) {

                // Need to find a cell first...
                cellFound = null;
                model = this.graph.getModel();

                // Will find the last whatbox created
                // which is probably the one the user
                // just created while following the steps
                model.filterDescendants( function( cell ) {

                    if ( cell.isWhatbox() ) {

                        cellFound = cell;

                    }

                } );

                if ( cellFound ) {

                    // We have a cell, now suggest to enter the edit mode
                    this.dataForNextStep = cellFound;

                    this.updateHintBox(
                        $( cellFound ), {
                            title: "This is a whatobx",
                            content: [
                                "A Whatbox describes WHAT needs to be done",
                                "Click to edit the description"

                                // $("<div>Press <kbd>Tab&nbsp;⇥</kbd> to continue</div>")
                            ]
                        },
                        "bottom" );

                    this.expectedCode = this.BOX_ENTERED_EDIT_MODE;

                }

            }

            break;

        case 4:

            /*
             * Editing a whatbox
             * enter the what
             *
             */

            this.dataForNextStep.cell = this.graph.getSelectionCell();
            this.dataForNextStep.firstInput = this.graph.cellEditor.firstInput.inputField;

            this.updateHintBox(
                this.dataForNextStep.cell, {
                    title: "Describe the activity",
                    content: [
                        "Look at the example",
                        $( "<div>Press <kbd>Tab&nbsp;⇥</kbd> to continue</div>" )
                    ]
                },
                "bottom" );

            // Changes the text
            this.dataForNextStep.firstInput.value = "Make a list of images needed for website";

            // 2. sets the code that is expected
            this.expectedCode = this.BOX_TAB_PRESSED;

            // /*
            //  * Move the box with drag and drop
            //  *
            //  */

            // This.updateHintBox(
            //     this.dataForNextStep.cell,
            //     {
            //         title: "move box around",
            //         content: [ "Select and move the box" ]
            //     },
            //     "bottom");

            // This.expectedCode = this.BOX_MOVED_ON_CANVAS;

            break;

        case 5 :

            /*
             * Enter edit who
             *
             */

            var field = this.graph.cellEditor.currentInputs[ this.graph.cellEditor.currentInputs.length - 1 ].inputField;

            this.updateHintBox(

                // This.graph.cellEditor.editingCell,
                field,
                {
                    title: "Who is responsible for that?",
                    content: [
                        "Look at the example",
                        $( "<div>Press <kbd>Tab&nbsp;⇥</kbd> or <kbd>Enter&nbsp;↩</kbd> to continue</div>" )
                    ]
                },
                "bottom" );

            // Changes the text
            $( field )
                .prop( "disabled", false )
                .val( "Marketing coordinator" );

            $( field ).focus( function() {

                var _this = this;

                setTimeout( function() {

                    _this.select();

                }, 10 );

            } );

            // 2. sets the code that is expected
            this.expectedCode = this.STOP_EDITING;
            this.dataForNextStep.cell = this.graph.getSelectionCell();

            break;

        case 6:

            /*
             * Now must be on a whybox
             *
             */

            var firstYbox = [];

            // Finds a whybox from the current cell

            currentCell = this.dataForNextStep.cell ? this.dataForNextStep.cell : this.graph.getSelectionCell();

            // Var currentCell = this.dataForNextStep.cell;
            $( currentCell.edges ).each( function( i, edge ) {

                if ( edge.target.isWhybox() ) {

                    firstYbox.push( edge.target );

                    return false;

                }

            } );

            if ( firstYbox.length > 0 ) {

                this.dataForNextStep.cell = firstYbox[ 0 ];
                this.goToNextStep();

            }

            // Can't find a whybox, create one...
            else {

                this.expectedCode = this.NEXT_Y_BOX_IS_CREATED;

                this.updateHintBox(
                    currentCell,
                    {
                        title: "Create a Whybox to continue...",
                        content:[ //Fixme : new icon
                            $( "<span>Drag " +
                                "<span class='fa-stack' style='color:orange' >" +
                                "<i class='fa fa-circle-thin fa-stack-2x'></i>" +
                                "<i class='fa fa-circle fa-stack-1x'></i>" +
                            "</span>" +
                            "then drop close by,<br/>or press <kbd>Space</kbd></span>" )
                        ]
                    },
                    "bottom" );

            }

            break;

        case 7: // Enter Edit on the WHY

            // Check if we are not already in edit mode
            if ( this.graph.editingCell ) {

                this.dataForNextStep.cell = this.graph.editingCell;
                this.goToNextStep();

            }

            // Not in editing mode...
            else {

                this.updateHintBox(
                    this.dataForNextStep.cell,
                    {
                        title: "once the activity is done",
                        content: [
                            "Edit the state",
                            $( "<div>Click, press <kbd>Enter&nbsp;↩</kbd><br/>or <kbd>F2</kbd></div>" )
                        ]
                    },
                    "bottom" );

                var g = this.graph;
                setTimeout( function() {

                    g.setSelectionCell( that.dataForNextStep.cell );

                }, 100 );

                // Changes the text

                // 2. sets the code that is expected
                this.expectedCode = this.BOX_ENTERED_EDIT_MODE;

            }

            // // 3. sets the callback function
            // this.callback = function callbackF(hash) {
            //     that.displayWellDoneMessage(hash.cell);
            //     that.dataForNextStep = hash;
            // };

            break;

        case 8:

            // Edit the whybox text for real

            this.updateHintBox(
                $( this.graph.cellEditor.firstInput.inputField ),
                {
                    title: "Why is this done?",
                    content: [
                        $( "<span>Expected state once the activity is completed.</span>" ),

                        // "Look at the example",
                        $( "<div>Press <kbd>Tab&nbsp;⇥</kbd> or <kbd>Enter&nbsp;↩</kbd> to validate</div>" )
                    ]
                },
                "left" );

            this.graph.cellEditor.firstInput.inputField.value = "list shared with marketing agency for feedback";
            this.dataForNextStep.cell = this.graph.cellEditor.editingCell;

            // 2. sets the code that is expected
            this.expectedCode = this.BOX_TAB_PRESSED;

            // // 3. sets the callback function
            // this.callback = function callbackF(hash) {
            //     that.displayWellDoneMessage(hash.cell);
            //     that.dataForNextStep = hash;
            // };
            break;

        case 9:

            /*
             * Drag the arrow to create more boxes
             *
             */

            // Find the right cell to display the message
            cell = [];
            $( this.dataForNextStep.cell.edges ).each( function( i, edge ) {

                if ( edge.target.isVertex() ) {

                    cell.push( edge.target );

                    return false;

                }

            } );

            if ( cell.length > 0 ) {

                this.dataForNextStep.cell = cell[ 0 ];
                var t = this;
                setTimeout( function() {

                    t.graph.setSelectionCell( t.dataForNextStep.cell );

                }, 100 );

            }

            this.updateHintBox(
                this.dataForNextStep.cell,
                {
                    title: "To create more boxes...",
                    content: [

                        // "Hover over the box",
                        $( "<div>Drag the circle " +
                            "<span class='fa-stack' style='color:orange' >" +
                                "<i class='fa fa-circle-thin fa-stack-2x'></i>" +
                                "<i class='fa fa-circle fa-stack-1x'></i>" +
                            "</span>" +
                            "and drop it close by...</div>" )
                    ]
                },
                "bottom"
            );

            this.expectedCode = this.BOX_CREATED_WITH_DND_FROM_CONNECT_ARROW;

            break;

        case 10: // Create box with space

            /*
             * Only for desktop & non-touch
             *
             */

            if ( !mxClient.IS_TOUCH ) {

                // 1. sets the text
                text = {
                    title: "Want to be faster?",
                    content: [

                        // $("<div>Learn some keyboard shorcuts!</div>"),
                        $( "<div>Select a box and press <kbd>Space</kbd> to create more...</div>" )
                    ]
                };
                this.updateHintBox( this.dataForNextStep.cell, text, "bottom" );

                this.expectedCode = this.BOX_CREATED_WITH_SPACE_BAR;

            }

            break;

        case 11:

            this.updateHintBox( $( ".skNavTabs" ).find( "a[href=#learn]" ),
                {
                    title: "Keep going...",
                    content:[ "You can load examples from the learn tab..." ]
                },
                "left",
                true );

            this.justWaitForNextAction = true;
            this.expectedCode = "any";

            break;

        case this.BOX_CREATED_WITH_SPACE_BAR:

            if ( this.randomActionDone.indexOf( this.BOX_CREATED_WITH_SPACE_BAR ) == -1 ) {

                text = {
                    title: "Oh, you like speed ?",
                    content: [
                        $( "<div>Learn more keyboard shortcuts</div>" )
                    ]
                };
                this.updateHintBox( $( ".skFooter_Kbd" ), text, "top", true, function() {

                    // Use the call back to reposition the hint box.
                    $( ".skHint" ).position( { my: "middle bottom-10", at:"left top", of:$( ".skFooter_Kbd" ) } );

                } );

                this.randomActionDone.push( this.BOX_CREATED_WITH_SPACE_BAR );

            }

            break;

        case this.ATT_PANEL_OPEN :

            if ( this.randomActionDone.indexOf( this.ATT_PANEL_OPEN ) == -1 ) {

                $( ".skAttPanelTutorial" ).show();

                this.expectedCode = this.any;

                this.randomActionDone.push( this.ATT_PANEL_OPEN );

            }

            break;

        case this.ATT_PANEL_CLOSED:

            if ( this.randomActionDone.indexOf( this.ATT_PANEL_CLOSED ) == -1 ) {

                // $(".skAttPanelTutorial").hide();

                this.expectedCode = this.any;

                this.randomActionDone.push( this.ATT_PANEL_CLOSED );

            }

            break;

        case this.BOX_SELECTED:

            if ( this.randomActionDone.indexOf( this.BOX_SELECTED + "DETAILEDVIEW" ) == -1 ) {

                // Do something

                this.updateHintBox(
                    this.dataForNextStep.cell,
                    {
                    title: "Need to detail a step?",
                    content: [
                        $( "<div>Hover over box & click on the <span aria-hidden='true' class='fa fa-toggle-down'></span> to enter</div>" )
                    ]
                },
                "left" );

                this.expectedCode = this.NAVIGATE;

                this.randomActionDone.push( this.BOX_SELECTED + "DETAILEDVIEW" );

                break;

            } else if ( this.randomActionDone.indexOf( this.BOX_SELECTED + "_ADDNOTE" ) == -1 ) {

                this.updateHintBox(
                    $( ".CSSN" ),
                    {
                    title: "Need to write notes?",
                    content: [
                        "Try adding one on the canvas" ]
                },
                "bottom" );

                this.expectedCode = this.any;

                this.randomActionDone.push( this.BOX_SELECTED + "_ADDNOTE" );

                break;

            }

        // Case 8: // edit

        //     Var theCell = null;
        //     // finds a whatbox
        //     if ( this.dataForNextStep != null ) {

        //         TheCell = this.dataForNextStep;

        //     } else  {
        //         cellsFound = [];

        //         Model = this.graph.getModel();
        //         model.filterDescendants(function(cell) {

        //             If (cell.isWhatbox()) {
        //                 cellsFound.push(cell);
        //                 return true;
        //             }
        //         });

        //         TheCell = cellsFound[0];

        //     }

        //     If ( theCell) {

        //         This.graph.scrollCellToVisible(theCell);

        //         This.updateHintBox(
        //             theCell,
        //             {
        //                 title: "Edit content",
        //                 content: [ "Double click on the box to edit it"]
        //             },
        //             "bottom" );

        //         This.graph.setSelectionCell(theCell);

        //         // 2. sets the code that is expected
        //         this.expectedCode = this.BOX_ENTERED_EDIT_MODE;
        //     }
        //     else {
        //         console.log("no whatbox found");
        //     }

        //     Break;

        // Case 9: // actually edit the text

        //     // this.updateHintBox(
        //     //     $(this.dataForNextStep.firstInput), {
        //     //         title: "Enter the WHAT",
        //     //         content: [
        //     //             "A Whatbox describes WHAT is done",
        //     //             "Look at the example",
        //     //             $("<div>Press <kbd>Tab&nbsp;⇥</kbd> to continue</div>")
        //     //         ]
        //     //     },
        //     //     "left" );

        //     // // changes the text
        //     // this.dataForNextStep.firstInput.value = "Create marketing materials";

        //     // // 2. sets the code that is expected
        //     // this.expectedCode = this.BOX_TAB_PRESSED;

        //     Break;

        // Case 10: // edit the WHO

        //     // var field = $(".editor_who_input.cell_" + this.dataForNextStep.cell.id);

        //     // finds the field, we trust the last one in the list is a resource editor...
        //     // var field = this.graph.cellEditor.currentInputs[ this.graph.cellEditor.currentInputs.length-1 ].inputField;
        //     // this.updateHintBox(
        //     //     $(field),
        //     //     {
        //     //         title: "Enter the WHO",
        //     //         content: [
        //     //             "Who does this action",
        //     //             "Look at the example",
        //     //             $("<div>Press <kbd>Tab&nbsp;⇥</kbd> or <kbd>Enter&nbsp;↩</kbd> to continue</div>")
        //     //         ]
        //     //     },
        //     //     "left" );

        //     // // changes the text
        //     // $(field)
        //     //     .prop("disabled", false)
        //     //     .val("Marketing coordinator");

        //     // $(field).focus(function() {
        //     //     var _this = this;
        //     //     setTimeout(function() {
        //     //         _this.select();
        //     //     }, 10);
        //     // });

        //     // // 2. sets the code that is expected
        //     // this.expectedCode = this.BOX_TAB_PRESSED;

        //     // break;

        // Case 11 : // find a why box; or create one

        // Case "LOAD_A_SKORE":
        // case 15 : // load a skore

        //     // resets switch to a "real" number if LOAD_A_SKORE has been used
        //     this.currentStep = 15;

        //     // if (! this.tutorialHasStartedAutomaticallyAtStart ) {

        //     //     // warns user this will clear content
        //     //     this.updateHintBox({
        //     //         $(window),
        //     //         {
        //     //             title: "INFO",
        //     //             content: [
        //     //                 "This will clear the existing content",
        //     //             ]
        //     //         },
        //     //         null,
        //     //         true,
        //     //         function(){
        //     //             this.container$.append($("<a>Stop!</a>");
        //     //             var a = document.createElement("a");
        //     //             mxUtils.write(a, "Stop!");
        //     //             mxEvent.addGestureListeners(
        //     //                 a,
        //     //                 null,
        //     //                 null,

        //     //             );
        //     //         }

        //     //     })

        //     // }

        //     If (that.graph.scoreBoxActionsBtns) {
        //         that.graph.scoreBoxActionsBtns.destroy();
        //     }

        //     Var doc = ""
        //     skShell.setGraphXml(doc.documentElement);

        //     ContinueButton = document.createElement("a");
        //     mxUtils.write(continueButton, "Close tutorial");
        //     mxEvent.addGestureListeners(
        //         continueButton,
        //         null,
        //         null,
        //         mxUtils.bind(this, function(){
        //             // this.stepDone(that.TUT_NEXT_PRESSED);
        //             this.end();
        //         })
        //     );

        //     This.updateHintBox(
        //         $(window),
        //         {
        //             title: "Demo Skore",
        //             content: [
        //                 "Onboarding an employee",
        //                 "Take some time to study... ",
        //                 "You will notice some new things",
        //                 continueButton
        //             ]
        //         });

        //     // 2. sets the code that is expected
        //     this.expectedCode = this.TUT_NEXT_PRESSED;

        //     Break;

        // Case 19: // shows off details

        //     // find a cell with attachment
        //     // finds a whatbox
        //     cellsFound = [];
        //     model = this.graph.getModel();
        //     model.filterDescendants(function(cell) {

        //         If (cell.isWhatboxDetailed()) {
        //             cellsFound.push(cell);
        //             return true;
        //         }
        //     });

        //     Cell = cellsFound[0];

        //     This.updateHintBox(
        //         cell,
        //         {
        //             title: "Detailed view",
        //             content: [
        //                 "This Whatbox has further details",
        //                 "You can see because of the 'wings' on the box",
        //                 $("<div>Hover over box & click on the <span aria-hidden='true' class='actionIcon icon-enter'></span> to view</div>")
        //             ]
        //         },
        //         "left");

        //     // 2. sets the code that is expected
        //     this.expectedCode = this.NAVIGATE;

        //     Break;

        // Case 19: // in the detailed view

        //     ContinueButton = document.createElement("a");
        //     mxUtils.write(continueButton, "Continue...");
        //     mxEvent.addGestureListeners(
        //         continueButton,
        //         null,
        //         null,
        //         mxUtils.bind(this, function(){
        //             this.stepDone(that.TUT_NEXT_PRESSED);
        //         })
        //     );

        //     This.updateHintBox(
        //         $(".skTitle"),
        //         {
        //             title: "Detailed view",
        //             content: [
        //                 "Here you are in a Whatbox",
        //                 "Look, the title is what the Whatbox is called",
        //                 continueButton
        //             ]
        //         },
        //         "top");

        //     // 2. sets the code that is expected
        //     this.expectedCode = this.TUT_NEXT_PRESSED;

        //     Break;

        // Case 20: // wings

        //     This.updateHintBox(
        //         $(".wingsInputs"),
        //          {
        //             title: "Reuse Whyboxes",
        //             content: [
        //                 "You can reuse the same Whyboxes you had connected to the high level Whatbox",
        //                 "Drag & drop one to the canvas"
        //             ]
        //         },
        //         "left");

        //     // 2. sets the code that is expected
        //     // this.expectedCode = this.YBOX_CREATED_FROM_WING;
        //     this.expectedCode = this.BOX_DROPPED_FROM_CARDSHOE;

        //     Break;

        // Case 21: // exit level up

        //     This.updateHintBox(
        //         $(";skTitleContainer"),
        //         {
        //             title: "Detailed view",
        //             content: [ "Click on the higher level name to go back" ]
        //         },
        //         "top");

        //     // 2. sets the code that is expected
        //     this.expectedCode = this.NAVIGATE;

        //     Break;

        // Case 22: // generic
        // case 23: // generic
        // case 24: // generic

           /* // 1 text
            var text = {
                title: "",
                action1: "",
                action2: ""
            }

            this.updateHintBox($(window), text, null, false)

            $(".f5m_continent")
                .append(
                    $("<div>")
                        .html("<br><a href='.'>Continue</a>")
                        .on("click", function(){
                            that.stepDone(that.TUT_NEXT_PRESSED);
                        })
                );



            // 2. sets the code that is expected
            this.expectedCode = this.TUT_NEXT_PRESSED;

            // // 3. sets the callback function
            // this.callback = function callbackF(hash) {
            //     that.displayWellDoneMessage(hash.cell)
            //     this.dataForNextStep = hash;
            // };

            break;*/

        // Case 25:

        //     Var learnButton = document.createElement("span");
        //     learnButton.innerHTML = "Visit <a>getSkore.com/learn</a> for more wisdom";
        //     mxEvent.addGestureListeners(learnButton, null, null, function(){
        //         window.open("https://www.getskore.com/learn");
        //     });

        //     CloseButton = document.createElement("a");
        //     mxUtils.write(closeButton, "Close tutorial");
        //     mxEvent.addGestureListeners(closeButton, null, null, mxUtils.bind(this, function(){
        //         this.end();
        //     }));

        //     // //refObject, text, direction, animate, continueButton, callback
        //     this.updateHintBox(
        //         $(window),
        //         {
        //             title: "the end",
        //             content: [
        //                 "Skores are better shared with other's'!",
        //                 learnButton,
        //                 closeButton
        //             ]
        //         });

        //     //     that.hintContainer$
        //     //         .append(
        //     //             $("<div>")
        //     //                 .html("<hr>Visit the <a href='.'>the-skore.com/learn</a> for more")
        //     //                 .on("click", function(){
        //     //                     window.open("http://www.the-skore.com/learn")
        //     //                 })
        //     //         )
        //     //         .append(
        //     //             $("<div>")
        //     //                 .html("<br><hr><a href='#'>Close</a>")
        //     //                 .on("click", function(){
        //     //                     that.end();
        //     //                 })
        //     //         ).append(
        //     //             $("<div>")
        //     //                 .html("<br><hr>You can create a new Skores from the file menu.")
        //     //                 .on("click", function(){
        //     //                     that.end();
        //     //                 })
        //     //         );

        //     // });

        //     Break;

        }

    };

    return skGraphEditorTutorial;

} );
