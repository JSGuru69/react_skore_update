define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxArrowConnector = require( "mxArrowConnector" );
    var mxCell = require( "mxCell" );
    var mxCellRenderer = require( "mxCellRenderer" );
    var mxConstants = require( "mxConstants" );
    var mxEvent = require( "mxEvent" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skShell = require( "skShell" );
    var skColors = require( "skGraph/skColors" );
    var skUtils = require( "skUtils" );

    return function setupHandoverHighlight( graph ) {

        /*
         * Second trial with flex Arrow
         *
         */

        // Generic arrow
        function FlexArrowShape() {

            mxArrowConnector.call( this );
            this.spacing = 0;

        }

        mxUtils.extend( FlexArrowShape, mxArrowConnector );
        FlexArrowShape.prototype.defaultWidth = 2;
        FlexArrowShape.prototype.defaultArrowWidth = 10;

        FlexArrowShape.prototype.getStartArrowWidth = function getStartArrowWidthF() {

            return this.getEdgeWidth() + mxUtils.getNumber( this.style, skConstants.START_WIDTH, this.defaultArrowWidth );

        };

        FlexArrowShape.prototype.getEndArrowWidth = function getEndArrowWidthF() {

            return this.getEdgeWidth() + mxUtils.getNumber( this.style, skConstants.END_WIDTH, this.defaultArrowWidth );

        };

        FlexArrowShape.prototype.getEdgeWidth = function getEdgeWidthF() {

            return mxUtils.getNumber( this.style, "width", this.defaultWidth ) + Math.max( 0, this.strokewidth - 1 );

        };

        // Registers the link shape
        mxCellRenderer.prototype.defaultShapes[ skConstants.FLEX_ARROW ] = FlexArrowShape;

        // Shortcut to tell the extension is loaded
        skShell.ext_lineHighlightLoaded = true;

        var highlightColor = skColors.HANDOVER;
        /*
         * Get the color
         *
         */
        if ( graph.model.root.handoverhighlight ) {

            var highlightColorSetup = skUtils.readValue( graph.model.root.handoverhighlight, "color" );

            // If hasn't been saved...
            if ( highlightColorSetup === "" ) {

                graph.model.root.handoverhighlight.setAttribute( "color", skColors.HANDOVER );

            } else {

                highlightColor = highlightColorSetup;

            }

        }

        /*
         * Register the style for the highlighted lines and whybox
         *
         */

        var highlightedLineStyle = {};

        highlightedLineStyle[ mxConstants.STYLE_ENDSIZE ] = 5;
        highlightedLineStyle[ mxConstants.STYLE_STROKECOLOR ] = highlightColor;
        highlightedLineStyle[ skConstants.END_WIDTH ] = 8;
        highlightedLineStyle[ mxConstants.STYLE_VERTICAL_ALIGN ] = mxConstants.ALIGN_TOP;
        highlightedLineStyle[ mxConstants.STYLE_SHAPE ] = skConstants.FLEX_ARROW;

        graph.getStylesheet().putCellStyle( skConstants.LINE_HIGHLIGHT, highlightedLineStyle );

        /*
         * Function called on the whybox to see which lines to be highlgiht
         *
         */

         mxCell.prototype.highlightLines = function highlightLinesF( actuallyUpdateStyle ) {

            if ( this && !this.isWhybox() ) {

                // Console.log("not a whybox");

                return;

            }

            actuallyUpdateStyle = actuallyUpdateStyle && skShell.ext_lineHighlightLoaded;

            /*
             * To understand variable names... this = 3 = currentWhybox
             * ---> [ 1 ] --2--> ( 3 ) --4--> [ 5 ] --->
             *                     ^
             *               currentWhybox
             *********************/

            // FIXME : use mxCell properties instead ?
            var lines2 = [],
            lines4 = [],
            currentWhybox = this,
            connections = [];

            // Find all the edges and reset the style
            $( currentWhybox.edges ).each( function( i, edge ) {

                if ( edge.source == currentWhybox ) {

                    lines4.push( edge );

                } else if ( edge.target == currentWhybox ) {

                    lines2.push( edge );

                }

            } );

            if ( actuallyUpdateStyle ) {

                // Reset style of lines
                var tmp = lines4.concat( lines2 );

                $( tmp ).each( function( i, edge ) {

                    graph.setCellStyle(
                        mxUtils.removeAllStylenames( edge.style ),
                        [ edge ] );

                } );

                // Box
                graph.setCellStyles( mxConstants.STYLE_STROKECOLOR, null, [ this ] );
                graph.setCellStyles( "handover", null, [ this ] );

            }

            var box1resource, box5resource;

            // Now we start reviewing the connections...
            $( lines2 ).each( function( i, edge2 ) {

                // We take the FIRST resource of the box to compute the handovers
                box1resource = edge2.source.getBoxRoles( 0 )[ 0 ];

                $( lines4 ).each( function( i, edge4 ) {

                    // We take the FIRST resource of the box to compute the handovers
                    box5resource = edge4.target.getBoxRoles( 0 )[ 0 ];

                    if ( box1resource != box5resource ) {

                        // Console.log("highlight lines");

                        if ( actuallyUpdateStyle ) {

                            skUtils.setCellsStyleName( graph.model, [ edge2, edge4 ], skConstants.LINE_HIGHLIGHT );

                            graph.setCellStyles( mxConstants.STYLE_STROKECOLOR, highlightColor, [ currentWhybox ] );
                            graph.setCellStyles( "handover", "1", [ currentWhybox ] );

                        }

                        connections.push( {
                            "code": "" + edge2.source.id + " " + currentWhybox.id + " " + edge4.target.id,
                            "sourceCell": edge2.source,
                            "sourceResource": box1resource,
                            "whybox": currentWhybox,
                            "destinationCell": edge4.target,
                            "destinationResource": box5resource
                        } );

                    }

                } );

            } );

            // Console.table(connections);

            // Return $.distinct(connections);
            return connections;

        };

        /*
         * Now, browse all lines and highlight them if necessary
         *
         * (one off)
         */

        var filter = function filterF( cell ) {

            return cell.isWhybox();

        };

        graph.model.beginUpdate();

        try {

            var yb = graph.model.filterDescendants( filter );

            for ( var i = 0; i < yb.length; i++ ) {

                yb[ i ].highlightLines( true );

            }

        }

        finally {

            graph.model.endUpdate();

        }

        /*
         * Supercharge event : on new line connection
         *
         */

        graph.addListener( mxEvent.CELL_CONNECTED, function( sender, event ) {

            // The event is triggerd twice per connection, we do only once.
            if ( !event.getProperty( "source" ) ) {

                // FIXME : use mxCell properties

                // Tries the 2...
                event.getProperty( "edge" ).source.highlightLines( true );// .highlightMeIfNecessary();
                event.getProperty( "edge" ).target.highlightLines( true );// .highlightMeIfNecessary();

            }

        } );  // End cell_connected event

        /*
         * On resource change
         *
         */

         graph.addListener( mxEvent.LABEL_CHANGED, function( event, sender ) {

            // FIXME : do it only if value has changed ?
            // var value = sender.getProperty("value");
            // var old = sender.getProperty("old");
            var cell = sender.getProperty( "cell" );

            $( cell.edges ).each( function( i, edge ) {

                if ( edge.source.isWhybox() ) {

                    edge.source.highlightLines( true );

                } else if ( edge.target.isWhybox() ) {

                    edge.target.highlightLines( true );

                }

            } );

        } );

        /*
         * On line delete
         *
         */

         graph.addListener( mxEvent.CELLS_REMOVED, function( event, sender ) {

            // Console.log("CELLS_REMOVED");

            var cells = sender.getProperty( "cells" );

            // FIXME : use mxCell

            // Take all other lines connected to the cell and recalculate
            $( cells ).each( function( i, cell ) {

                if ( cell.isWhatbox ) {

                    // Console.log("remove whatbox");
                    $( cell.edges ).each( function( i, edge ) {

                        if ( edge.source.isWhybox() ) {

                            edge.source.highlightLines( true );

                        } else if ( edge.target.isWhybox() ) {

                            edge.target.highlightLines( true );

                        }

                    } );

                }

                if ( cell.isEdge ) {

                    // Remove line
                    if ( cell.source && cell.source.isWhybox() ) {

                        cell.source.highlightLines( true );

                    } else if ( cell.target && cell.target.isWhybox() ) {

                        cell.target.highlightLines( true );

                    }

                }

            } );

        } );

};

} );
