
define( function( require ) {

	"use strict";

	var $ = require( "jquery" );
	var mxConstants = require( "mxConstants" );
	var mxEvent = require( "mxEvent" );
	var mxOutline = require( "mxOutline" );
	var mxUtils = require( "mxUtils" );

	// var skGraph = require( "skGraph/skGraph" );

	function skOutline( graph ) {

		/*jshint validthis:true*/
		this.source = graph;
		this.border = 0;

		var container = $( "#skOutline" ).empty();
		container.empty();
		this.init(  container[ 0 ] );
		this.isOutline = true;

		/*jshint validthis:true*/
		mxEvent.addListener( window, "resize", mxUtils.bind( this, function() {

			this.update();

		} ) );

		graph.addListener( "refreshOutline", mxUtils.bind( this, function() {

			this.update();

		} ) );

        if ( this.outline.dialect == mxConstants.DIALECT_SVG ) {

			var that = this;

			mxEvent.addMouseWheelListener( function( evt, up ) {

				var outlineWheel = false;
				var source = mxEvent.getSource( evt );

				while ( source != null ) {

					if ( source == that.outline.view.canvas.ownerSVGElement ) {

						outlineWheel = true;
						break;

					}

					source = source.parentNode;

				}

				if ( outlineWheel ) {

					if ( up ) {

						graph.zoomIn();

					} else {

						graph.zoomOut();

					}

					mxEvent.consume( evt );

				}

			} );

		}

		this.update();

		return this;

	}

	var outlineCreateGraph = mxOutline.prototype.createGraph;
	mxOutline.prototype.createGraph = function createGraphF( container ) {

		this.container = container;

		var g = outlineCreateGraph.apply( this, arguments );

		// var g = new skGraph( container, this.source.getModel(), this.graphRenderHint, this.source.getStylesheet() );
		g.foldingEnabled = false;
		g.autoScroll = false;
		g.gridEnabled = false;
		g.pageScale = this.source.pageScale;
		g.pageFormat = this.source.pageFormat;
		g.background = this.source.background;
		g.pageVisible = this.source.pageVisible;

		var current = mxUtils.getCurrentStyle( this.source.container );
		container.style.backgroundColor = current.backgroundColor;

		return g;

	};

	mxUtils.extend( skOutline, mxOutline );

	// skOutline.prototype.update = function( revalidate ) {

	// 	this.outline.pageScale = this.source.pageScale;
	// 	this.outline.pageFormat = this.source.pageFormat;
	// 	this.outline.pageVisible = this.source.pageVisible;
	// 	this.outline.background = this.source.background;

	// 	var current = mxUtils.getCurrentStyle( this.source.container );
	// 	this.container.style.backgroundColor = current.backgroundColor;

	// 	if ( this.source.view.backgroundPageShape != null && this.outline.view.backgroundPageShape != null )
	// 	{

	// 		this.outline.view.backgroundPageShape.fill = this.source.view.backgroundPageShape.fill;

	// 	}

	// 	mxOutline.prototype.update.call( this, revalidate );

	// };

	skOutline.prototype.outline = null;

	return skOutline;

} );
