define( function( require ) {

    "use strict";

    var mxClient = require( "mxClient" );
    var mxEvent = require( "mxEvent" );
    var mxKeyHandler = require( "mxKeyHandler" );
    var mxUtils = require( "mxUtils" );
    var skShell = require( "skShell" );

    /**
     * @constructor
     * keyboard event handler
     *
     *********************/

    function skGraphKeyboardHandler( graph, actions ) {

        /*jshint validthis:true*/

        var keyHandler = new mxKeyHandler( graph, graph.container );

        // Routes command-key to control-key on Mac
        keyHandler.isControlDown = function isControlDownF( evt ) {

            return mxEvent.isControlDown( evt ) || ( mxClient.IS_MAC && evt.metaKey );

        };

        var keyHandleEscape = keyHandler.escape;

        keyHandler.escape = function escapeF() {

            /* ⓘ todo
             *
             * escape goes up a level if in a drill down
             */

            console.log( "escape pressed" );

            // SkShell.menu.hideDialog();
            graph.cellEditor.stopEditing( true );
            keyHandleEscape.apply( this, arguments );

        };

        // Helper function to move cells with the cursor keys
        function nudge( keyCode ) {

            if ( !graph.isSelectionEmpty() ) {

                var dx = 0;
                var dy = 0;

                if ( keyCode == 37 ) {

                    dx = -1;

                } else if ( keyCode == 38 ) {

                    dy = -1;

                } else if ( keyCode == 39 ) {

                    dx = 1;

                } else if ( keyCode == 40 ) {

                    dy = 1;

                }

                graph.moveCells( graph.getSelectionCells(), dx, dy );

                //Graph.scrollCellVisible(graph.getSelectionCell()); // score disabled; don't know why it was there ? but doesn't seem to work

            }

        }

        // Binds keystrokes to actions

        var bindAction = mxUtils.bind( this, function( code, control, key, shift ) {

            var action = actions.get( key );

            if ( action != null ) {

                var f = function fF() {

                    if ( action.enabled ) {

                        action.funct( arguments );

                    }

                };

                if ( control ) {

                    if ( shift ) {

                        keyHandler.bindControlShiftKey( code, f );

                    } else {

                        keyHandler.bindControlKey( code, f );

                    }

                } else {

                    if ( shift ) {

                        keyHandler.bindShiftKey( code, f );

                    } else {

                        keyHandler.bindKey( code, f );

                    }

                }

            }

        } );

        skShell.keyBindAction = bindAction;

        // Page up : exit details
        // keyHandler.bindKey(33, function() { graph.exitGroup(); }); // Page Up
        keyHandler.bindKey( 33, function() {

            actions.get( "exitGroup" ).funct.apply();

        } ); // Page Up

        // Page down: enter details
        keyHandler.bindKey( 34, function() {

            actions.get( "enterGroup" ).funct.apply();

        } );

        // Home: go to top level
        keyHandler.bindKey( 36, function() {

            graph.home();

        } );

        // End : refresh
        keyHandler.bindKey( 35, function() {

            graph.refresh();

        } );

        // Arrows : nudge
        keyHandler.bindKey( 37, function() {

            nudge( 37 );

        } ); // Left arrow
        keyHandler.bindKey( 38, function() {

            nudge( 38 );

        } ); // Up arrow
        keyHandler.bindKey( 39, function() {

            nudge( 39 );

        } ); // Right arrow
        keyHandler.bindKey( 40, function() {

            nudge( 40 );

        } ); // Down arrow

        // Enter / F2 : start edit
        keyHandler.bindKey( 13, function() {

            graph.startEditingAtCell();

        } );
        keyHandler.bindKey( 113, function() {

            graph.startEditingAtCell();

        } );

        // "f" brings to front
        bindAction( 70, false, "bringFront" );

        // "b" send to bottom / send to back
        bindAction( 66, false, "sendBack" );

        // "t" aligns top
        bindAction( 84, false, "alignTop" );

        // "l" aligns top
        bindAction( 76, false, "alignLeft" );

        // "r" aligns right
        bindAction( 82, false, "alignRight" );

        // "c" aligns center
        bindAction( 67, false, "alignCenter" );

        // // "s" opens stylesheet
        bindAction( 83, false, "modal_stylesheet" );

        // "v" distributes space out vertically
        bindAction( 86, false, "distributeVertical" );

        // "h" distributes space out horizontally
        bindAction( 72, false, "distributeHorizontal" );

        // // "x" modal xml
        bindAction( 88, false, "modal_xml" );

        // Delete (backspace or del)
        bindAction( 46, false, "delete" );
        bindAction( 8, false, "delete" );

        // + - = : zoom
        bindAction( 107, false, "zoomIn" );
        bindAction( 109, false, "zoomOut" );
        bindAction( 187, false, "zoomReset" );

        // Ctrl+A, Z, Y, X, C, V, D : standard edit
        bindAction( 90, true, "undo" ); // Ctrl+Z
        bindAction( 89, true, "redo" ); // Ctrl+Y
        bindAction( 68, true, "duplicate" ); // Ctrl+D

        // Copy / paste / cut handled in skClipboard
        // bindAction(67, true, 'copy'); // Ctrl+C
        // bindAction(88, true, 'cut'); // Ctrl+X
        // bindAction(86, true, 'paste'); // Ctrl+V

        bindAction( 71, true, "group" ); // Ctrl+G
        bindAction( 65, true, "selectAll" ); // Ctrl+A

        // "p" for print
        bindAction( 80, true, "modal_print" );

        //BindAction(71, true, 'grid', true); // Ctrl+Shift+G

        bindAction( 85, true, "unGroup" ); // Ctrl+U

        bindAction( 75, true, "toggleStickynotes" ); // Ctrl+K

        // SPACE : new box
        bindAction( 32, false, "createNextBox" );
        bindAction( 32, false, "createPreviousBox", true ); // Shift space
        bindAction( 32, true, "createNextBoxNewLine" ); // Ctrl space

        // TAB : navigates
        bindAction( 9, false, "selectNextBox" );
        bindAction( 9, false, "selectPreviousBox", true ); // Shift tab

        /*
         * W and y + click to add a whatbox/whybox
         *
         */

        // W : 87
        keyHandler.bindKey( 87, function() {

            if ( skShell.wKeyIsPressed ) {

                window.clearTimeout( skShell.wKeyIsPressed );
                skShell.wKeyIsPressed = null;

            }
            skShell.wKeyIsPressed = window.setTimeout( function() {

                skShell.wKeyIsPressed = null;

            }, 600 );

        } );

        // Y : 89
        keyHandler.bindKey( 89, function() {

            if ( skShell.yKeyIsPressed ) {

                window.clearTimeout( skShell.yKeyIsPressed );
                skShell.yKeyIsPressed = null;

            }
            skShell.yKeyIsPressed = window.setTimeout( function() {

                skShell.yKeyIsPressed = null;

            }, 600 );

        } );

        // N : 78
        keyHandler.bindKey( 78, function() {

            if ( skShell.nKeyIsPressed ) {

                window.clearTimeout( skShell.nKeyIsPressed );
                skShell.nKeyIsPressed = null;

            }
            skShell.nKeyIsPressed = window.setTimeout( function() {

                skShell.nKeyIsPressed = null;

            }, 600 );

        } );

        return keyHandler;

    }

    return skGraphKeyboardHandler;

} );
