define( function( require ) {

    "use strict";

    var setupSkDefaultTexts = require( "skGraph/skDefaultTexts" );
    var skColors = require( "skGraph/skColors" );
    var skConstants = require( "skConstants" );

    var shell = {};

    /*
     * Helpers
     *
     */

    shell.urlParams = null;

    shell.readUrlParams = function readUrlParamsF( url ) {

        this.urlParams = {};
        var idx = url.lastIndexOf( "?" );

        if ( idx > 0 ) {

 // There are parameters

            var params = url.substring( idx + 1 ).split( "&" );

            // Browse the parameters
            for ( var i = 0; i < params.length; i++ ) {

                // There
                idx = params[ i ].indexOf( "=" );

                if ( idx > 0 ) {

                    this.urlParams[ params[ i ].substring( 0, idx ) ] = params[ i ].substring( idx + 1 );

                } else {

                    // By default it's justSkoreIt... sort of hack
                    this.urlParams[ skConstants.JUSTSKOREIT ] = params[ i ];

                }

            }

        }

        //Return this.urlParams;

    };

    // Used to enable w/y + click to add a box on graph
    shell.wKeyIsPressed = null;
    shell.yKeyIsPressed = null;
    shell.nKeyIsPressed = null;

    shell.colors = skColors;
    setupSkDefaultTexts( shell );

    if ( DEV ) {

        window.skShell = shell;

    }

     /*
     * Helper function to have the conte
     *
     */

    shell.copyFileOutOfElectron = function skCopyFileOutOfElectronF( sourceInAsarArchive, destOutsideAsarArchive ) {

        if ( ELECTRON ) {

            var fs = window.requireNode( "fs" );
            var fsj = window.requireNode( "fs-jetpack" );
            var app = window.requireNode( "electron" ).remote.app;

            if ( fs.existsSync( app.getAppPath() + "/" + sourceInAsarArchive ) ) {

              // File will be copied
              if ( fs.statSync( app.getAppPath() + "/" + sourceInAsarArchive ).isFile() ) {

                  fsj.write( destOutsideAsarArchive, fs.readFileSync( app.getAppPath() + "/" + sourceInAsarArchive ) );

              }

              // Dir is browsed
              else if ( fs.statSync( app.getAppPath() + "/" + sourceInAsarArchive ).isDirectory() ) {

                var that = this;

                  fs.readdirSync( app.getAppPath() + "/" + sourceInAsarArchive ).forEach( function( fileOrFolderName ) {

                     that.copyFileOutOfElectron( sourceInAsarArchive + "/" + fileOrFolderName, destOutsideAsarArchive + "/" + fileOrFolderName );

                  } );

              }

            }

        }

    };

    return shell;

} );
