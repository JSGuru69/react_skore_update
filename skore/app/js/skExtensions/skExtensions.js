
define( function( require ) {

    "use strict";

    var skConstants = require( "skConstants" );

    // var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );

    function skExtensions( graph ) {

        /*jshint validthis: true */
        this.graph = graph;
        this.loadedExtensions = [];

    }

    skExtensions.prototype.loadedExtensions = null;
    skExtensions.prototype.graph = null;
    skExtensions.prototype.loadExistingExtensions = function loadExistingExtensionsF() {

        console.log( "load existing extensions" );

        // Loads extension if necessary

        // FIXME use model.root ???

        // Get the xml
        var node = this.graph.getGraphXml();

        // Find the extensions
        var exts = node.getElementsByTagName( "extension" );

        if ( exts.length > 0 ) {

            for ( var i = 0; i < exts.length; i++ ) {

                var ext = exts[ i ];

                // Load the extension
                this.load( ext );

            }

        }

    };

    skExtensions.prototype.load = function loadF( node ) {

        var extensionName = skUtils.readValue( node );

        // Check if extension exists
        if ( !this.loadedExtensions.hasOwnProperty( extensionName ) &&
            this.loadedExtensions[ extensionName ] !== false ) {

            console.log( "loading extension : " + extensionName );
            this[ "load_" + extensionName ]();

            // This.loadedExtensions[extensionName] = true;

        }

    };

    skExtensions.prototype.load_mrt = function load_mrtF() {

        // if ( !this.loadedExtensions[ skConstants.MULTI_RESOURCE_TAG ] ) {

        //     require( "skGraphEditor/skEditor" );
        //     // require( "skGraphEditor/skEditor_resource" );

        //     // This one just defines skEditor_tag
        //     require( "skGraphEditor/skEditor_tag" );

        //     // These change prototypes, therefore they return function to call
        //     require( "skGraphEditor/skEditor_multiresource" )();
        //     require( "skGraphEditor/skEditor_resourcetag" )();
        //     require( "skGraph/skBox_tag" )();
        //     require( "skGraph/skBox_multiresource" )();

        //     this.loadedExtensions[ skConstants.MULTI_RESOURCE_TAG ] = true;

        //     if ( skShell.setStatus ) {

        //         skShell.setStatus( "Extension Multi-resources & tags loaded" );

        //     }

        //     console.log( "loading extension : mrt loaded" );

        // }

    };

    skExtensions.prototype.load_handoverhighlight = function load_handoverhighlightF() {

        if ( !this.loadedExtensions[ skConstants.HANDHOVER_HIGHLIGHT ] ) {

            require( "skGraphEditorUI/skHandoverHighlight" )( this.graph );
            this.loadedExtensions[ skConstants.HANDHOVER_HIGHLIGHT ] = true;

            console.log( "loading extension : handoverhighlight loaded" );

        }

    };

    return skExtensions;

} );
