define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skShell = require( "skShell" );

    function licenseAvailable( license ) {

        var canSave = false;

        if ( license instanceof Error ) {

            $( ".skRegistrationDetailsTitle" ).text( "Not logged in." );

        } else {

            var email = license.email;

            if ( license.paid ) {

                console.log( "paid license" );

                // Enable features in export
                // skShell.exporter.initTheRest();

                $( ".skRegistrationDetails" ).hide();

                skShell.allowSave = false;
                canSave = true;

            }

            // Check demo period
            else if ( !license.paid && ( new Date( new Date() - new Date( license.first_visit ) ) / ( 1000 * 60 * 60 * 24 ) ) < 31 ) {

                var daysLeft = Math.floor( 31 - ( new Date( new Date() - new Date( license.first_visit ) ) / ( 1000 * 60 * 60 * 24 ) ) );
                var status = "Skore app for Google Drive : Demo (" + daysLeft + " days remaining)";

                skShell.footer.addTab( status, null, null );

                $( ".skDemoDaysLeft div" ).html( "<p>Demo: " + daysLeft + " days remaining.</p>" +
                    "<p><span class='label label-danger'>When demo expires, you cannot save anymore.</span></p>" );

                $( ".skRegistrationDetailsTitle" ).text( "Subscribe" );

                skShell.allowSave = true;
                canSave = true;

            }

            //Otherwise, demo is expired
            else {

                // Colors the button
                $( ".skRegistrationDetailsButton" ).addClass( "btn-warning" );
                $( ".skRegistrationDetailsTitle" ).text( "Demo expired. Please subscribe now." );
                $( ".skDemoDaysLeft div" ).html( "<p><span class='label label-danger'>Demo Expired, you cannot save anymore.</span></p>" +
                    "<p>Purchasing a license will help us make the best software.</p>" );
                $( ".skSideMenu_save" ).off( "click" ).addClass( "noLink" );

                skShell.allowSave = false;

            }

            $( ".skgDriveLoggedInAs" ).html( "Logged in as " + email );

            console.log( "here.... gdrive" );

        }

        return canSave;

    }

    return licenseAvailable;

} );
