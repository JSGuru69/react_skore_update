/* globals gapi, google */
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    var undef;

    var appId, clientId, scopes, apiKey;

    if ( DEV ) {

        // Development
        appId = "818013595605";
        clientId = "818013595605-est4i1o6c1482n4filotol0gc1d02aeg.apps.googleusercontent.com";
        scopes = [ "email", "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive.install" ];
        apiKey = "AIzaSyCfLXveaYLXS2nuRBmOFXzFb4D_BMkMLjg";

    } else {

        // Production
        appId = "564831058403";
        clientId = "564831058403-lvomu2gnslps95s4a9natmqufcjaa4e4.apps.googleusercontent.com";
        scopes = [ "email", "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive.install" ];
        apiKey = "AIzaSyDi8QbMOEsfYx95nyDGoyOJ4VMStoP8CuI";

    }

    function inherit( child, parent ) {

        function F() {
        }
        F.prototype = parent.prototype;
        child.prototype = new F();
        child.prototype.constructor = child;

    }

    function CustomError( message ) {

        if ( Error.captureStackTrace ) {

            Error.captureStackTrace( this, CustomError );

        } else {

            var stack = Error().stack;

            if ( stack ) {

                this.stack = stack;

            }

        }

        if ( message ) {

            this.message = String( message );

        }

    }
    inherit( CustomError, Error );
    CustomError.prototype.name = "CustomError";

    function DriveError( code, reason, message ) {

        CustomError.call( this, message );
        this.code = code;
        this.reason = reason;

    }
    inherit( DriveError, CustomError );
    DriveError.prototype.name = "DriveError";

    function Timeout() {

        CustomError.call( this );

    }
    inherit( Timeout, CustomError );
    Timeout.prototype.name = "Timeout";
    Timeout.prototype.message = "Timeout";

    function ConcurrentSave( lastModifyingUserName ) {

        CustomError.call( this );

        if ( lastModifyingUserName ) {

            this.message = "Save failed, the file has been modified by " + lastModifyingUserName;

        } else {

            this.message = "Save failed, the file has been modified by somebody else";

        }

    }
    inherit( ConcurrentSave, CustomError );
    ConcurrentSave.prototype.name = "ConcurrentSave";

    function driveResolve( defer ) {

        return function( resp ) {

            defer.resolve( resp.result );

        };

    }

    function driveError( defer ) {

        return function( resp ) {

            var result = resp && resp.result,
                error = result && result.error,
                code = error && error.code,
                errors = error && error.errors,
                e0 = errors && errors[ 0 ],
                reason = e0 && e0.reason,
                message = e0 && e0.message;

            defer.reject( new DriveError( code, reason, message ) );

        };

    }

    function timeoutError( defer ) {

        return function() {

            if ( defer.state() === "pending" ) {

                defer.reject( new Timeout() );

            }

        };

    }

    function argCopy( source ) {

        var index = -1;
        var length = source.length;

        //Noinspection JSPotentiallyInvalidConstructorUsage
        var array = Array( length );

        while ( ++index < length ) {

            array[ index ] = source[ index ];

        }

        return array;

    }

    // Returns a random integer between min (included) and max (excluded)
    // Using Math.round() will give you a non-uniform distribution!
    function getRandomInt( min, max ) {

        return Math.floor( Math.random() * ( max - min ) ) + min;

    }

    function retry( fn ) {

        return function() {

            var d = $.Deferred(), args = argCopy( arguments ), nextDelay = 1000, that = this, authTried = false;
            var tryIt = function() {

                fn.apply( that, args ).then( d.resolve, function( e ) {

                    console.log( "retry(" + fn.name + "): " + e.message );

                    if ( e instanceof DriveError ) {

                        var code = e.code, reason = e.reason,
                            isAuthError = code === 401 && ( reason === "authError" || reason === "required" ),
                            isRateLimit = code === 403 && ( reason === "rateLimitExceeded" || reason === "userRateLimitExceeded" ),
                            isNotFound = code === 404 && reason === "notFound";

                        if ( ( isRateLimit || isNotFound ) && nextDelay <= 16000 ) {

                            setTimeout( tryIt, nextDelay + getRandomInt( 0, 1001 ) );
                            nextDelay *= 2;

                            return;

                        }

                        if ( isAuthError && !authTried ) {

                            authTried = true;
                            authorize( true ).then( tryIt, function() {

                                d.reject( e );

                            } );

                            return;

                        }

                    }
                    d.reject( e );

                } );

            };

            tryIt();

            return d.promise();

        };

    }

    /**
     * @external Promise
     * @see {@link http://api.jquery.com/Types/#Promise Promise}
     */

    /**
     * @param fileId
     * @return {Promise}
     */
    var downloadMetadata = retry( function downloadMetadata( fileId ) {

        var d = $.Deferred();

        gapi.client.drive.files.get( { fileId: fileId } ).then( driveResolve( d ), driveError( d ) );

        return d.promise();

    } );

    /**
     * @param fileId
     * @return {Promise}
     */
    var downloadContent = retry( function downloadContent( fileId ) {

        var d = $.Deferred();

        gapi.client.drive.files.get( { fileId: fileId, alt: "media" } ).then( function( resp ) {

            try {

                /*global escape*/
                d.resolve( decodeURIComponent( escape( resp.body ) ) ); // Convert from utf8

            }

 catch ( e ) {

                d.reject( new DriveError( 0, "invalidFormat", e.message ) );

            }

        }, driveError( d ) );

        return d.promise();

    } );

    /**
     * Promise will be resolved with (metadata, content), download in parallel
     * @param fileId
     * @return {Promise}
     */
    function openFile( fileId ) {

        return $.when( downloadMetadata( fileId ), downloadContent( fileId ) );

    }

    function addExt( name ) {

        return /\.skore$/.test( name ) ? name : name + ".skore";

    }

    var btoa = window.btoa ||

        function( input ) {

            //jshint bitwise:false
            var B64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

            var position = 0,
                out = [],
                o1, o2, o3,
                e1, e2, e3, e4;

            while ( position < input.length ) {

                o1 = input.charCodeAt( position++ );
                o2 = input.charCodeAt( position++ );
                o3 = input.charCodeAt( position++ );

                // 111111 112222 222233 333333
                e1 = o1 >> 2;
                e2 = ( ( o1 & 0x3 ) << 4 ) | ( o2 >> 4 );
                e3 = ( ( o2 & 0xf ) << 2 ) | ( o3 >> 6 );
                e4 = o3 & 0x3f;

                if ( position === input.length + 2 ) {

                    e3 = 64;
                    e4 = 64;

                } else if ( position === input.length + 1 ) {

                    e4 = 64;

                }

                out.push(
                    B64_ALPHABET.charAt( e1 ),
                    B64_ALPHABET.charAt( e2 ),
                    B64_ALPHABET.charAt( e3 ),
                    B64_ALPHABET.charAt( e4 ) );

            }

            return out.join( "" );

        };

    function b64( str ) {

        /*global unescape*/
        return btoa( unescape( encodeURIComponent( str ) ) );

    }

    function generateBoundary() {

        // http://stackoverflow.com/questions/2071257/generating-multipart-boundary
        // jshint bitwise:false
        var chars = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
            charsLen = chars.length,
            count = ( Math.random() * 11 | 0 ) + 30, // A random size from 30 to 40
            result = "";

        while ( count-- ) {

            result += chars.charAt( Math.random() * charsLen | 0 );

        }

        return result;

    }

    function injectETag( file, fn ) {

        return function() {

            var d = $.Deferred(), args = argCopy( arguments ), etag = file && file.etag, that = this, numOfAttempts = 0;
            var tryIt = function() {

                fn.apply( that, [ etag ].concat( args ) ).then( d.resolve, function( e ) {

                    console.log( "concurrent(" + fn.name + "): " + e.message );
                    numOfAttempts += 1;

                    if ( e instanceof DriveError ) {

                        var code = e.code, reason = e.reason;

                        if ( file && code === 412 && reason === "conditionNotMet" ) {

                            downloadMetadata( file.id ).then( function( latestFile ) {

                                if ( numOfAttempts < 5 && latestFile.md5Checksum === file.md5Checksum ) {

                                    // Something has changed (sharing?), but not a content
                                    // retry save with a new etag
                                    etag = latestFile.etag;
                                    setTimeout( tryIt, 1000 );

                                    return;

                                }
                                d.reject( new ConcurrentSave( latestFile.lastModifyingUserName ) );

                            }, function() {

                                // DownloadMetadata call failed, so we do not know who modified the file
                                d.reject( new ConcurrentSave() );

                            } );

                            return;

                        }

                    }
                    d.reject( e );

                } );

            };

            tryIt();

            return d.promise();

        };

    }

    /**
     * @param file
     * @param name
     * @param content
     * @param thumbnail
     * @return {Promise}
     */
    function saveFile( file, name, content, thumbnail ) {

        name = addExt( name );

        var boundary = generateBoundary();
        var delimiter = "\r\n--" + boundary + "\r\n";
        var closeDelim = "\r\n--" + boundary + "--";

        var contentType = "application/vnd.google.drive.ext-type.skore";
        var metadata = {
            title: name,
            mimeType: contentType,
            thumbnail: thumbnail || undef
        };

        var base64Data = b64( content );
        var mixedRequestBody =
            delimiter +
            "Content-Type: application/json\r\n\r\n" +
            JSON.stringify( metadata ) +
            delimiter +
            "Content-Type: " + contentType + "\r\n" +
            "Content-Transfer-Encoding: base64\r\n" +
            "\r\n" +
            base64Data +
            closeDelim;

        var headers = {
            "Content-Type": 'multipart/mixed; boundary="' + boundary + '"'
        };

        return injectETag( file, retry( function save( etag ) {

            if ( etag ) {

                headers[ "If-Match" ] = etag;

            }
            var request = gapi.client.request( {
                path: "/upload/drive/v2/files" + ( file ? "/" + file.id : "" ),
                method: file ? "PUT" : "POST",
                params: {
                    uploadType: "multipart"
                },
                headers: headers,
                body: mixedRequestBody } );

            var d = $.Deferred();

            request.then( driveResolve( d ), driveError( d ) );

            return d.promise();

        } ) )();

    }

    /**
     * @return {Promise}
     */
    function showPicker() {

        var picker = google.picker;
        var d = $.Deferred();

        new picker.PickerBuilder()
            .enableFeature( picker.Feature.NAV_HIDDEN )
            .setAppId( appId )
            .setOAuthToken( getAuthToken() ) // Todo fail if auth.getToken is null
            .setDeveloperKey( apiKey )
            .setCallback( function( data ) {

                var action = data[ picker.Response.ACTION ];

                if ( action === picker.Action.PICKED ) {

                    d.resolve( data[ picker.Response.DOCUMENTS ][ 0 ] );

                } else if ( action === picker.Action.CANCEL ) {

                    d.reject();

                }

            } )
            .setOrigin( window.location.protocol + "//" + window.location.host )
            .addView( new picker.DocsView( picker.ViewId.DOCS )
                .setSelectFolderEnabled( false )
                .setIncludeFolders( false )
                .setMode( picker.DocsViewMode.LIST )
                .setQuery( ".skore" ) )
            .setTitle( "Select a Skore" )
            .setSize( 600, 450 )
            .build()
            .setVisible( true );

        return d.promise();

    }

    var shareClient;

    function showShare( file ) {

        if ( !shareClient ) {

            shareClient = new gapi.drive.share.ShareClient( appId );

        }
        shareClient.setItemIds( file.id );
        shareClient.showSettingsDialog();

    }

    /**
     * @return {Promise}
     */
    var checkLicense = retry( function checkLicense() {

        var d = $.Deferred();

        gapi.client.skore.license().then( driveResolve( d ), driveError( d ) );

        return d.promise();

    } );

    /**
     * @return {Promise}
     */
    function authorize( isImmediate ) {

        var authParams = {
            client_id: clientId,
            scope: scopes,
            immediate: !!isImmediate
        };

        var d = $.Deferred();

        gapi.auth.authorize( authParams, function( authResult ) {

            if ( authResult && !authResult.error ) {

                d.resolve();

            } else {

                d.reject( authResult && authResult.error );

            }

        } );

        return d.promise();

    }

    /**
     * @return token or null
     */
    function getAuthToken() {

        var token = gapi.auth.getToken();

        return ( token && token.access_token ) || null;

    }

    function parseState() {

        var state = /[\?&]state=([^&#]*)/.exec( location.search );

        state = state && decodeURIComponent( state[ 1 ].replace( /\+/g, " " ) );
        state = state ? JSON.parse( state ) : {};
        var ids = state.ids, fileId = ids && ids[ 0 ], action = state.action;

        return {
            fileId: fileId,
            folderId: state.folderId,
            action: action,
            userId: state.userId,
            isNew: action !== "open" && !fileId,
            isOpen: action === "open" && !!fileId
        };

    }

    function loadGapi() {

        var d = $.Deferred();

        if ( window.gapi ) {

            d.resolve();

        } else {

            setTimeout( timeoutError( d ), 30000 );
            window.skDriveOnGapiClientLoad = d.resolve;
            var a = document.createElement( "script" ),
                m = document.getElementsByTagName( "script" )[ 0 ];

            a.async = 1;
            a.src = "https://apis.google.com/js/client.js?onload=skDriveOnGapiClientLoad";
            m.parentNode.insertBefore( a, m );

        }

        return d.promise();

    }

    function loadApi( name, version, root ) {

        var d = $.Deferred();

        if ( gapi.client[ name ] ) {

            d.resolve();

        } else {

            setTimeout( timeoutError( d ), 30000 );
            gapi.client.load( name, version, d.resolve, root );

        }

        return d.promise();

    }

    function loadDriveApi() {

        return loadApi( "drive", "v2" );

    }

    function loadLicenseApi() {

        return loadApi( "skore", "v1", "https://the-skore-backend.appspot.com/_ah/api" );

    }

    function loadPickerApi() {

        var d = $.Deferred();

        if ( window.google && window.google.picker ) {

            d.resolve();

        } else {

            setTimeout( timeoutError( d ), 30000 );
            gapi.load( "picker", d.resolve );

        }

        return d.promise();

    }

    function loadShareApi() {

        var d = $.Deferred();

        if ( gapi.drive && gapi.drive.share ) {

            d.resolve();

        } else {

            setTimeout( timeoutError( d ), 30000 );
            gapi.load( "drive-share", d.resolve );

        }

        return d.promise();

    }

    function callAuth() {

        var d = $.Deferred();

        if ( getAuthToken() ) {

            d.resolve();

        } else {

            // Always resolve, even when authorise call is rejected
            authorize( true ).then( d.resolve, d.resolve );

        }

        return d.promise();

    }

    /**
     * When init is resolved all apis have been loaded and auth has been called with immediate: true
     * call getAuthToken and proceed accordingly
     */
    function init() {

        return loadGapi().then( function() {

            return $.when( loadDriveApi(), loadLicenseApi(), loadPickerApi(),  loadShareApi(), callAuth() );

        } );

    }

    return {
        init: init,
        open: openFile,
        save: saveFile,
        checkLicense: checkLicense,
        showPicker: showPicker,
        showShare:showShare,
        authorize: authorize,
        getAuthToken: getAuthToken,
        urlState: parseState()
    };

} );
