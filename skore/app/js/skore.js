
( function( mx ) {

    "use strict";

    function makeMx( mod ) {

        return function() {

            return window[ mod ];

        };

    }

    for ( var i = 0, l = mx.length; i < l; i += 1 ) {

        define( mx[ i ], makeMx( mx[ i ] ) );

    }

}( [
    "mxArrowConnector",
    "mxCell",
    "mxCellEditor",
    "mxCellMarker",
    "mxCellOverlay",
    "mxCellPath",
    "mxCellRenderer",
    "mxCellState",
    "mxClient",
    "mxClipboard",
    "mxCodec",
    "mxCompactTreeLayout",
    "mxConnectionConstraint",
    "mxConnectionHandler",
    "mxConstants",
    "mxConstraintHandler",
    "mxDivResizer",
    "mxEdgeHandler",
    "mxEdgeSegmentHandler",
    "mxEdgeStyle",
    "mxEvent",
    "mxEventObject",
    "mxEventSource",
    "mxGeometry",
    "mxGraph",
    "mxGraphModel",
    "mxGraphView",
    "mxImage",
    "mxImageShape",
    "mxKeyHandler",
    "mxLayoutManager",
    "mxMorphing",
    "mxMouseEvent",
    "mxMultiplicity",
    "mxOutline",
    "mxPanningHandler",
    "mxPoint",
    "mxPolyline",
    "mxPrintPreview",
    "mxRectangle",
    "mxRectangleShape",
    "mxRubberband",
    "mxShape",
    "mxStencil",
    "mxStencilRegistry",
    "mxStylesheet",
    "mxText",
    "mxUndoManager",
    "mxUtils",
    "mxVertexHandler"
] ) );

// need to setup global vars
// mxBasePath
// mxImageBasePath
// mxLanguage
// mxDefaultLanguage
// mxLoadResources
// mxLoadStylesheets
// mxResourceExtension

define( "jquery", function() {

    "use strict";
    return window.jQuery;

} );

define( "marked", function() {

    "use strict";
    return window.marked;

} );

define( "canvg", function() {

    "use strict";
    return window.canvg;

} );

define( "colors", function() {

    "use strict";

    //jscs:disable maximumLineLength

    return [ { name: "IndianRed", htmlcode:"#CD5C5C" }, { name: "LightCoral", htmlcode:"#F08080" }, { name: "Salmon", htmlcode:"#FA8072" }, { name: "DarkSalmon", htmlcode:"#E9967A" }, { name: "LightSalmon", htmlcode:"#FFA07A" }, { name: "Crimson", htmlcode:"#DC143C" }, { name: "Red", htmlcode:"#FF0000" }, { name: "FireBrick", htmlcode:"#B22222" }, { name: "DarkRed", htmlcode:"#8B0000" }, { name: "Pink", htmlcode:"#FFC0CB" }, { name: "LightPink", htmlcode:"#FFB6C1" }, { name: "HotPink", htmlcode:"#FF69B4" }, { name: "DeepPink", htmlcode:"#FF1493" }, { name: "MediumVioletRed", htmlcode:"#C71585" }, { name: "PaleVioletRed", htmlcode:"#DB7093" }, { name: "LightSalmon", htmlcode:"#FFA07A" }, { name: "Coral", htmlcode:"#FF7F50" }, { name: "Tomato", htmlcode:"#FF6347" }, { name: "OrangeRed", htmlcode:"#FF4500" }, { name: "DarkOrange", htmlcode:"#FF8C00" }, { name: "Orange", htmlcode:"#FFA500" }, { name: "Gold", htmlcode:"#FFD700" }, { name: "Yellow", htmlcode:"#FFFF00" }, { name: "LightYellow", htmlcode:"#FFFFE0" }, { name: "LemonChiffon", htmlcode:"#FFFACD" }, { name: "LightGoldenrodYellow", htmlcode:"#FAFAD2" }, { name: "PapayaWhip", htmlcode:"#FFEFD5" }, { name: "Moccasin", htmlcode:"#FFE4B5" }, { name: "PeachPuff", htmlcode:"#FFDAB9" }, { name: "PaleGoldenrod", htmlcode:"#EEE8AA" }, { name: "Khaki", htmlcode:"#F0E68C" }, { name: "DarkKhaki", htmlcode:"#BDB76B" }, { name: "Lavender", htmlcode:"#E6E6FA" }, { name: "Thistle", htmlcode:"#D8BFD8" }, { name: "Plum", htmlcode:"#DDA0DD" }, { name: "Violet", htmlcode:"#EE82EE" }, { name: "Orchid", htmlcode:"#DA70D6" }, { name: "Fuchsia", htmlcode:"#FF00FF" }, { name: "Magenta", htmlcode:"#FF00FF" }, { name: "MediumOrchid", htmlcode:"#BA55D3" }, { name: "MediumPurple", htmlcode:"#9370DB" }, { name: "BlueViolet", htmlcode:"#8A2BE2" }, { name: "DarkViolet", htmlcode:"#9400D3" }, { name: "DarkOrchid", htmlcode:"#9932CC" }, { name: "DarkMagenta", htmlcode:"#8B008B" }, { name: "Purple", htmlcode:"#800080" }, { name: "Indigo", htmlcode:"#4B0082" }, { name: "SlateBlue", htmlcode:"#6A5ACD" }, { name: "DarkSlateBlue", htmlcode:"#483D8B" }, { name: "MediumSlateBlue", htmlcode:"#7B68EE" }, { name: "GreenYellow", htmlcode:"#ADFF2F" }, { name: "Chartreuse", htmlcode:"#7FFF00" }, { name: "LawnGreen", htmlcode:"#7CFC00" }, { name: "Lime", htmlcode:"#00FF00" }, { name: "LimeGreen", htmlcode:"#32CD32" }, { name: "PaleGreen", htmlcode:"#98FB98" }, { name: "LightGreen", htmlcode:"#90EE90" }, { name: "MediumSpringGreen", htmlcode:"#00FA9A" }, { name: "SpringGreen", htmlcode:"#00FF7F" }, { name: "MediumSeaGreen", htmlcode:"#3CB371" }, { name: "SeaGreen", htmlcode:"#2E8B57" }, { name: "ForestGreen", htmlcode:"#228B22" }, { name: "Green", htmlcode:"#008000" }, { name: "DarkGreen", htmlcode:"#006400" }, { name: "YellowGreen", htmlcode:"#9ACD32" }, { name: "OliveDrab", htmlcode:"#6B8E23" }, { name: "Olive", htmlcode:"#808000" }, { name: "DarkOliveGreen", htmlcode:"#556B2F" }, { name: "MediumAquamarine", htmlcode:"#66CDAA" }, { name: "DarkSeaGreen", htmlcode:"#8FBC8F" }, { name: "LightSeaGreen", htmlcode:"#20B2AA" }, { name: "DarkCyan", htmlcode:"#008B8B" }, { name: "Teal", htmlcode:"#008080" }, { name: "Aqua", htmlcode:"#00FFFF" }, { name: "Cyan", htmlcode:"#00FFFF" }, { name: "LightCyan", htmlcode:"#E0FFFF" }, { name: "PaleTurquoise", htmlcode:"#AFEEEE" }, { name: "Aquamarine", htmlcode:"#7FFFD4" }, { name: "Turquoise", htmlcode:"#40E0D0" }, { name: "MediumTurquoise", htmlcode:"#48D1CC" }, { name: "DarkTurquoise", htmlcode:"#00CED1" }, { name: "CadetBlue", htmlcode:"#5F9EA0" }, { name: "SteelBlue", htmlcode:"#4682B4" }, { name: "LightSteelBlue", htmlcode:"#B0C4DE" }, { name: "PowderBlue", htmlcode:"#B0E0E6" }, { name: "LightBlue", htmlcode:"#ADD8E6" }, { name: "SkyBlue", htmlcode:"#87CEEB" }, { name: "LightSkyBlue", htmlcode:"#87CEFA" }, { name: "DeepSkyBlue", htmlcode:"#00BFFF" }, { name: "DodgerBlue", htmlcode:"#1E90FF" }, { name: "CornflowerBlue", htmlcode:"#6495ED" }, { name: "MediumSlateBlue", htmlcode:"#7B68EE" }, { name: "RoyalBlue", htmlcode:"#4169E1" }, { name: "Blue", htmlcode:"#0000FF" }, { name: "MediumBlue", htmlcode:"#0000CD" }, { name: "DarkBlue", htmlcode:"#00008B" }, { name: "Navy", htmlcode:"#000080" }, { name: "MidnightBlue", htmlcode:"#191970" }, { name: "Cornsilk", htmlcode:"#FFF8DC" }, { name: "BlanchedAlmond", htmlcode:"#FFEBCD" }, { name: "Bisque", htmlcode:"#FFE4C4" }, { name: "NavajoWhite", htmlcode:"#FFDEAD" }, { name: "Wheat", htmlcode:"#F5DEB3" }, { name: "BurlyWood", htmlcode:"#DEB887" }, { name: "Tan", htmlcode:"#D2B48C" }, { name: "RosyBrown", htmlcode:"#BC8F8F" }, { name: "SandyBrown", htmlcode:"#F4A460" }, { name: "Goldenrod", htmlcode:"#DAA520" }, { name: "DarkGoldenrod", htmlcode:"#B8860B" }, { name: "Peru", htmlcode:"#CD853F" }, { name: "Chocolate", htmlcode:"#D2691E" }, { name: "SaddleBrown", htmlcode:"#8B4513" }, { name: "Sienna", htmlcode:"#A0522D" }, { name: "Brown", htmlcode:"#A52A2A" }, { name: "Maroon", htmlcode:"#800000" }, { name: "White", htmlcode:"#FFFFFF" }, { name: "Snow", htmlcode:"#FFFAFA" }, { name: "Honeydew", htmlcode:"#F0FFF0" }, { name: "MintCream", htmlcode:"#F5FFFA" }, { name: "Azure", htmlcode:"#F0FFFF" }, { name: "AliceBlue", htmlcode:"#F0F8FF" }, { name: "GhostWhite", htmlcode:"#F8F8FF" }, { name: "WhiteSmoke", htmlcode:"#F5F5F5" }, { name: "Seashell", htmlcode:"#FFF5EE" }, { name: "Beige", htmlcode:"#F5F5DC" }, { name: "OldLace", htmlcode:"#FDF5E6" }, { name: "FloralWhite", htmlcode:"#FFFAF0" }, { name: "Ivory", htmlcode:"#FFFFF0" }, { name: "AntiqueWhite", htmlcode:"#FAEBD7" }, { name: "Linen", htmlcode:"#FAF0E6" }, { name: "LavenderBlush", htmlcode:"#FFF0F5" }, { name: "MistyRose", htmlcode:"#FFE4E1" }, { name: "Gainsboro", htmlcode:"#DCDCDC" }, { name: "LightGrey", htmlcode:"#D3D3D3" }, { name: "Silver", htmlcode:"#C0C0C0" }, { name: "DarkGray", htmlcode:"#A9A9A9" }, { name: "Gray", htmlcode:"#808080" }, { name: "DimGray", htmlcode:"#696969" }, { name: "LightSlateGray", htmlcode:"#778899" }, { name: "SlateGray", htmlcode:"#708090" }, { name: "DarkSlateGray", htmlcode:"#2F4F4F" }, { name:"Black", htmlcode:"#000000" } ];

    //jscs:enable maximumLineLength

} );

define( "IMAGE_PATH", "images" );

requirejs.config( {
    baseUrl: "js",
    paths: {
        root: "../",
        text: "lib/text"
    }
} );

requirejs( [ "skStart" ] );
