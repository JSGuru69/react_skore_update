define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxClipboard = require( "mxClipboard" );
    var mxConstants = require( "mxConstants" );
    var mxEvent = require( "mxEvent" );
    var mxEventObject = require( "mxEventObject" );
    var mxGeometry = require( "mxGeometry" );
    var mxUtils = require( "mxUtils" );
    var skAction = require( "./skAction" );
    var skConstants = require( "skConstants" );
    var skShell = require( "skShell" );

    /**
     * @constructor
     */
    function skActions( graphManager ) {

        /*jshint validthis:true*/

        this.actions = {};
        this.graphManager = graphManager;

        // this.navigatorUI = navigatorUI;

        var that = this;

        /*
         * listener at document lever
         *
         */

        $( document.body, "[data-skaction]" ).on( "click", function( event ) {

            var act = that.get( $( event.target ).closest( "[data-skaction]" ).data( "skaction" ) );

            if ( act != null ) {

                event.stopPropagation();

                var funct = act.funct;

                if ( funct !== null ) {

                    funct( event );

                }

                mxEvent.consume( event );

                // TODO : insert analytics here

            }

        } );

        this.init( graphManager );
        this.loadActions();

    }

    skActions.prototype.actions = null;
    skActions.prototype.graphManager = null;

    skActions.prototype.init = function initF( graphManager ) {

         /*
         * will assign listeners to the new graph so that the buttons react (enabled/disabled)
         * accordingly
         *
         */

        graphManager.addListener( "newGraphCreated", mxUtils.bind( this, function( sender, event ) {

            var graph = event.getProperty( "graph" );

            graph.getSelectionModel().addListener( mxEvent.CHANGE, mxUtils.bind( this, function() {

                var cells = graph.getSelectionCells();
                var multiCells = cells.length > 1 ;
                var oneCell = cells.length == 1 ;

                var that = this;

                Object.keys( this.actions ).forEach(
                    function( action ) {

                        if ( that.actions.hasOwnProperty( action ) ) {

                            var currentAction = that.get( action );

                            if ( currentAction.requireOneCell && currentAction.requireMultiCells ) {

                                currentAction.setEnabled( oneCell || multiCells );

                            } else {

                                if ( currentAction.requireMultiCells ) {

                                    currentAction.setEnabled( multiCells );

                                }
                                if ( currentAction.requireOneCell ) {

                                    currentAction.setEnabled( oneCell );

                                }

                            }

                        }

                    } );

                // special case for groups

                if ( cells.length === 1 && cells[ 0 ].isGroup() ) {

                    this.get( "unGroup" ).setEnabled( true );

                } else {

                    this.get( "unGroup" ).setEnabled( false );

                }

                // special case for paste

                if ( mxClipboard.cells != null ) {

                    console.log( "enable paste" );

                    //Noinspection JSPotentiallyInvalidUsageOfThis
                    this.get( "paste" ).setEnabled( true );

                }

            } ) );

        } ) );

        // this.navigatorUI.addListener( "enableAction", mxUtils.bind( this, function( sender, event ) {

        //     this.actions.get( event.properties.name ).setEnabled( event.properties.enable );

        // } ) );

    };

    // Register a new action
    skActions.prototype.addAction = function addActionF( key, requireOneCell, requireMultiCells, funct ) {

        return this.put( key, new skAction( key, funct, requireOneCell, requireMultiCells ) );

    };

    /**
     * Registers the given action under the given name.
     */
    skActions.prototype.put = function putF( name, action ) {

        this.actions[ name ] = action;

        return action;

    };

    // skActions.prototype.fireAction = function fireActionF(name) {

    // };

    skActions.prototype.get = function getF( name ) {

        return this.actions[ name ];

    };

    skActions.prototype.loadActions = function loadActions() {

        var curGrph = mxUtils.bind( this, function() {

            return this.graphManager.getCurrentGraph();

        } );

        var actions = this;

        /*
         * On-graph actions
         *
         */

        this.addAction(
            "home",
            false, false,
            function() {

                curGrph().home();

            } );

        this.addAction(
            "exitGroup",
            true, false,
            function( ) {

                curGrph().navigateTo();

            } );

        this.addAction(
            "enterGroup",
            true, false,
            function( cell ) {

                curGrph().navigateTo( cell );

            } );

        this.addAction(
            "undo",
            false, false,
            function() {

                curGrph().undoManager.undo();

            } );

        this.addAction(
            "redo",
            false, false,
            function() {

                curGrph().undoManager.redo();

            } );

        this.addAction(
            "enableGraph",
            false, false,
            function() {

                // var currentState = curGrph().isEnabled();
                curGrph().setEnabled( true );

            } );

        /*
         * Create the next box in the flow
         *
         */

        // space
        this.addAction(
            "createNextBox",
            true, false,
            function() {

                if ( arguments[ 0 ] && arguments[ 0 ][ 0 ].toString() === "[object KeyboardEvent]" ) {

                    mxEvent.consume( arguments[ 0 ] );

                }

                window.setTimeout( function() {

                    // Gets the current cell
                    var currentCells = curGrph().getSelectionCells();
                    var currentCell = currentCells[ 0 ];

                    // If only one box is selected AND this is actually a box
                    if ( currentCells.length == 1 && currentCell.isVertex() && !currentCell.isStickynote() ) {

                        // Get current state
                        var currentState = curGrph().getView().getState( currentCell );

                        // Create the next box
                        var newBox = currentState.add_box_after_box_state( null, "right", null, true );

                        // Select the next box
                        curGrph().setSelectionCell( newBox );

                        if ( skShell.tutorial ) {

                            skShell.tutorial.stepDone( skShell.tutorial.BOX_CREATED_WITH_SPACE_BAR, { cell: newBox } );

                        } // End tutorial

                    }

                }, 10 );

            } ); // CreateNext Box

        /*
         * Create previous box in the flow
         *
         */

        this.addAction(
            "createPreviousBox",
            true, false,
            function() {

                if ( arguments[ 0 ] && arguments[ 0 ][ 0 ].toString() === "[object KeyboardEvent]" ) {

                    mxEvent.consume( arguments[ 0 ] );

                }

                window.setTimeout( function() {

                    var currentCells = curGrph().getSelectionCells();
                    var currentCell = currentCells[ 0 ];

                    // If only one box is selected AND this is actually a box
                    if ( currentCells.length == 1 && currentCell.isVertex ) {

                        // Get current state
                        var currentState = curGrph().getView().getState( currentCell );

                        // Create the next box
                        var newBox = currentState.add_box_after_box_state( null, "left", null, true );

                        // Select the next box
                        curGrph().setSelectionCell( newBox );

                    }

                }, 10 );

            } ); // CreatePreviousBox

        // Triggered on ctrl + space when only one box is selected
        // creates a new box on the next line (80px margin...)
        this.addAction(
            "createNextBoxNewLine",
            true, false,
            function() {

                if ( arguments[ 0 ] && arguments[ 0 ][ 0 ].toString() === "[object KeyboardEvent]" ) {

                    mxEvent.consume( arguments[ 0 ] );

                }

                window.setTimeout( function() {

                    var currentCells = curGrph().getSelectionCells();
                    var currentCell = currentCells[ 0 ];

                    // If only one box is selected AND this is actually a box
                    if ( currentCells.length == 1 && currentCell.isVertex ) {

                        // Get current state
                        var currentState = curGrph().getView().getState( currentCell );

                        // Calculates the coordinates where to drop the next box
                        var newGeo = new mxGeometry();

                        newGeo.x = 15;
                        newGeo.y = currentCell.geometry.y + currentCell.geometry.height + skConstants.DIST_LINES;

                        // Create the next box
                        var newBox = currentState.add_box_after_box_state( newGeo, "right", null, true ); // True to create the line

                        // Select the next box
                        curGrph().setSelectionCell( newBox );

                    }

                }, 10 );

            } ); // CreateNextBoxNewLine

        /*
         * Select with tab
         *
         */

        var siblingsToVisit = {
            cells: [],
            nextItem: 0,
            isRolling: false,
            timer: null
        };

        var selectPreviousBoxWithTab = function selectPreviousBoxWithTabF() {

            selectBoxWithTab( true );

        };

        var selectBoxWithTab = function selectBoxWithTabF( backwards ) {

            if ( typeof backwards === "boolean" ) {

                backwards = backwards;

            } else {

                backwards = false;

            }

            console.log( "tab pressed" );

            function startTimer() {

                siblingsToVisit.timer = window.setTimeout( function() {

                    if ( siblingsToVisit.isRolling ) {

                        siblingsToVisit.isRolling = false;

                    }

                    // Kills timer
                    window.clearTimeout( siblingsToVisit.timer );
                    siblingsToVisit.timer = null;

                }, 500 );

            }

            if ( curGrph().getSelectionCells().length ) {

                /*
                 * Are we "rolling" or just navigating
                 *
                 */

                // Not rolling
                if ( !siblingsToVisit.isRolling ) {

                    console.log( "not rolling" );

                    // Resets the table
                    siblingsToVisit.cells.length = 0;

                    var currentCell = curGrph().getSelectionCells()[ 0 ];

                    // Get the next cells
                    $( currentCell.edges ).each( function( i, edge ) {

                        if ( backwards ) {

                            if ( edge.source.isVertex && edge.target == currentCell ) {

                                siblingsToVisit.cells.push( edge.source );

                            }

                        } else {

                            // First, find all the cells to highlight
                            if ( edge.target.isVertex && edge.source == currentCell ) {

                                siblingsToVisit.cells.push( edge.target );

                            }

                        }

                    } );

                    // No cells, ends
                    if ( siblingsToVisit.cells.length === 0 ) {

                        actions.get( "createNextBox" ).funct();

                        return true;

                    }

                    // Default behaviour : goes to the next cell
                    curGrph().setSelectionCell( siblingsToVisit.cells[ 0 ] );

                    // Only one cell to go to, ends
                    if ( siblingsToVisit.cells.length == 1 /*&& currentCell.isVertex*/) {

                        console.log( "goes to next cell" );

                        return true;

                    }

                    // More cells to visit, start rolling!
                    else {

                        console.log( "more cells to visit" );

                        siblingsToVisit.nextItem = 0;
                        siblingsToVisit.isRolling = true;
                        siblingsToVisit.nextItem = ( siblingsToVisit.nextItem + 1 ) % siblingsToVisit.cells.length;

                        // Start timer that will "disable" the rolling more in 500ms.
                        startTimer();

                    }

                } // End not rolling...

                // We ARE rolling !
                else {

                    curGrph().setSelectionCell( siblingsToVisit.cells[ siblingsToVisit.nextItem ] );
                    siblingsToVisit.nextItem = ( siblingsToVisit.nextItem + 1 ) % siblingsToVisit.cells.length;
                    window.clearTimeout( siblingsToVisit.timer );
                    startTimer();

                } // End rolling

            }

        };

        // Will select the (first of the) next box on press of tab
        this.addAction(
            "selectNextBox",
            true, false,
            selectBoxWithTab );
        this.addAction(
            "selectPreviousBox",
            true, false,
            selectPreviousBoxWithTab );

        /*
         * Edit actions
         *
         */

        this.addAction(
            "cut",
            true, true,
            function() {

                mxClipboard.cut( curGrph() );

            } );

        this.addAction(
            "copy",
            true, true,
            function() {

                console.log( "copy" );
                mxClipboard.copy( curGrph() );
                actions.get( "paste" ).setEnabled( true );

            } );

        this.addAction(
            "paste",
            false, false,
            function() {

                console.log( "paste (editing : " + curGrph().isEditing() + ")" );

                if ( !curGrph().isEditing() ) {

                    mxClipboard.paste( curGrph() );

                }

            } ); // At the start it's disabled

        this.addAction(
            "delete",
            true, true,
            function() {

                curGrph().removeCells();

            } );

        this.addAction(
            "duplicate",
            true, true,
            function() {

                var s = curGrph().gridSize;

                curGrph().setSelectionCells( curGrph().moveCells( curGrph().getSelectionCells(),
                    s,
                    s,
                    true ) );

            } );

        this.addAction(
            "selectAll",
            false, false,
            function() {

                curGrph().selectAll();

            } );

        this.addAction(
            "group",
            true, true,
            function() {

                curGrph().setSelectionCell( curGrph().groupCells( null, 15 ) );

            } );

        this.addAction(
            "unGroup",
            true, false,
            function() {

                curGrph().setSelectionCells( curGrph().ungroupCells() );

            } );

        this.addAction(
            "alignTop",
            false, true,
            function() {

                curGrph().alignCells( mxConstants.ALIGN_TOP );

            } );

        this.addAction(
            "alignLeft",
            false, true,
            function() {

                curGrph().alignCells( mxConstants.ALIGN_LEFT );

            } );
        this.addAction(
            "alignRight",
            false, true,
            function() {

                curGrph().alignCells( mxConstants.ALIGN_RIGHT );

            } );

        this.addAction(
            "alignCenter",
            false, true,
            function() {

                curGrph().alignCells( mxConstants.ALIGN_CENTER );

            } );

        this.addAction(
            "bringFront",
            true, true,
            function() {

                curGrph().alignCells( mxConstants.ALIGN_TOP );

            } );

        this.addAction(
            "sendBack",
            true, true,
            function() {

                curGrph().orderCells( true );

            } );

        this.addAction(
            "bringFront",
            true, true,
            function() {

                curGrph().orderCells( false );

            } );

        this.addAction(
            "distributeHorizontal",
            false, true,
            function() {

                curGrph().distributeCells( true );

            }
        );

        this.addAction(
            "distributeVertical",
            false, true,
            function() {

                curGrph().distributeCells( false );

            }
        );

    this.addAction(
        "modal_print",
        false, false,
        function() {

            actions.openModal( "print" );

        } );

    this.addAction(
        "modal_justskoreit",
        false, false,
        function() {

            actions.openModal( "justskoreit" );

        } );

    this.addAction(
        "modal_settings",
        false, false,
        function() {

            actions.openModal( "settings" );

        } );

    this.addAction(
        "modal_registration",
        false, false,
        function() {

            actions.openModal( "registration" );

        } );

        this.addAction(
            "modal_socialexport",
            false, false,
            function() {

                actions.openModal( "socialexport" );

            } );

        this.addAction(
            "modal_imageexport",
            false, false,
            function( event ) {

                var target = "preview";

                if ( event ) {

                    target = $( event.toElement ).closest( "[data-target]" ).data( "target" );

                }

                actions.openModal( "imageexport", "target", target );

                // $( modalImageExport ).modal( "toggle" );

            } );

        /*
         * ZOOM
         *
         */

        this.addAction(
            "zoomIn",
            false, false,
            function() {

                curGrph().zoomIn();

            } );

        this.addAction(
            "zoomFit",
            false, false,
            function() {

                curGrph().fit( 10 );

            } );

        this.addAction(
            "zoomOut",
            false, false,
            function() {

                curGrph().zoomOut();

            } );

        this.addAction(
            "zoomReset",
            false, false,
            function() {

                curGrph().zoomTo( 1 );

            } );

        this.addAction(
            "zoomFitPageWidth",
            false, false,
            function() {

                var fmt = curGrph().pageFormat;
                var ps = curGrph().pageScale;
                var cw = curGrph().container.clientWidth - 0;

                var scale = Math.floor( 20 * cw / fmt.width / ps ) / 20;
                curGrph().zoomTo( scale );

                if ( mxUtils.hasScrollbars( curGrph().container ) )
                {

                    var pad = curGrph().getPagePadding();
                    curGrph().container.scrollLeft = Math.min( pad.x * curGrph().view.scale,
                        ( curGrph().container.scrollWidth - curGrph().container.clientWidth ) / 2 );

                }

        } );

        /*
         * Toggle sticky note
         *
         */

        var that = this;

        this.toggledStickyNotes = [];
        this.addAction(
            "toggleStickynotes",
            false, false,
            function() {

                console.log( "toggle" );

                // Show stickynotes
                if ( that.toggledStickyNotes.length > 0 ) {

                    curGrph().toggleCells(
                        true,
                        that.toggledStickyNotes,
                        true );

                    that.toggledStickyNotes = [];

                }

                // Hide the stickynotes
                else {

                    // Gets all the stickyNotes
                    var cells = curGrph().model.filterDescendants( function( cell ) {

                        return cell.isStickynote();

                    } );

                    // Hides them
                    that.toggledStickyNotes = curGrph().toggleCells(
                        false, // Display?
                        cells, // Array of cells
                        true //IncludeEdges
                    );

                }

            } );

        this.addAction(
            "showID",
            false, false,
            function() {

                $( ".skFooter_IDs" ).toggleClass( "on" );
                curGrph().showBadgeIcon = !curGrph().showBadgeIcon;
                curGrph().refresh();

            } );

        if ( !VIEWER_MODE ) {

            // keyboard shortcut side modal

            this.addAction(
                "modal_kbd",
                false, false,
                function() {

                   actions.openModal( "kbd" );

                }
            );

            // formatting help side modal
            this.addAction(
                "modal_formatting",
                false, false,
                function() {

                    actions.openModal( "formatting" );

                }
            );

            // icon panel side modal
            this.addAction(
                "modal_icons",
                false, false,
                function() {

                    actions.openModal( "icons" );

                }
            );

            // xml modal
            this.addAction(
                "modal_xml",
                false, false,
                function() {

                    actions.openModal( "xml" );

                }
            );

            // stylesheet modal
            this.addAction(
                "modal_stylesheet",
                false, false,
                function( evt ) {

                    var loadtype = "stylesheet";

                    if ( evt.target ) {

                        var closest = $( evt.target.closest( "[data-loadtype]" ) );

                        if ( closest ) {

                            loadtype = closest.data( "loadtype" );

                        }

                    } else {

                        if ( curGrph().getSelectionCells().length > 0 ) {

                            loadtype = "stylecell";

                        }

                    }

                    actions.openModal( "stylesheet", "loadtype", loadtype );

                }
            );

        }

    };

    skActions.prototype.openModal = function( name, arg1Def, arg1Val, arg2Def, arg2Val, arg3Def, arg3Val ) {

        this.graphManager.fireEvent( new mxEventObject( "openModal",
            "graph", this.graphManager.getCurrentGraph(),
            "name", name,
            arg1Def, arg1Val,
            arg2Def, arg2Val,
            arg3Def, arg3Val ) );

    };

    return skActions;

} );
