define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEventSource = require( "mxEventSource" );
    var mxUtils = require( "mxUtils" );

    /**
     * @constructor
     * Constructs a new action for the given parameters.
     */
    function skAction( label, funct, requireOneCell, requireMultiCells ) {

        /*jshint validthis:true*/

        mxEventSource.call( this );

        this.label = label;
        this.funct = funct;
        this.enabled = true;
        this.requireMultiCells = requireMultiCells || false;
        this.requireOneCell = requireOneCell || false;

    }

    // Action inherits from mxEventSource
    mxUtils.extend( skAction, mxEventSource );

    skAction.prototype.setEnabled = function setEnabledF( value ) {

        if ( this.enabled != value ) {

            this.enabled = value;

            $( "[data-skaction=" + this.label + "]" ).toggleClass( "disabled", !this.enabled );

            if ( ELECTRON ) {

                // console.log( "electron enableMenuItem", this.label, this.enabled );
                window.requireNode( "electron" ).ipcRenderer.send( "enableMenuItem", this.label, this.enabled );

            }

        }

    };

    return skAction;

} );
