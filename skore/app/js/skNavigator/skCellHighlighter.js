define( function( require ) {

    "use strict";

    var mxConstants = require( "mxConstants" );
    var mxUtils = require( "mxUtils" );
    var skShell = require( "skShell" );

    /*
     * Highlight a cell... it's different than selection because it's purely cosmetic and ephemeral
     *
     * todo : should work with a group of cells so it can be used in other things like search results
     *
     */

    /**
     * @constructor
     */
    function skCellHighlighter( graph ) {

        /*jshint validthis:true*/
        this.graph = graph;

    }

    skCellHighlighter.prototype.previousStyle = null;
    skCellHighlighter.prototype.currentState = null;
    skCellHighlighter.prototype.graph = null;

    skCellHighlighter.prototype.toggleHighlightOn = function toggleHighlightOnF( state ) {

        this.toggleHighlightOff();

        if ( !state || !state.shape ) {

            return;

        }

        console.log( "skCellHighlighter.toggleHighlightOn" );

        this.previousStyle = state.style;
        this.currentState = state;

        if ( state ) {

            state.style = mxUtils.clone( state.style );

            // state.style[ mxConstants.STYLE_STROKEWIDTH ] = skConstants.STROKE_BOX_HOVER; //3;
            state.style[ mxConstants.STYLE_STROKECOLOR ] = skShell.colors.BOX_HOVER;
            state.shape.apply( state );

            // state.shape.reconfigure();
            state.shape.redraw();

            // Redraws the icon to make sure they are "on top"

            // Var refresh = function refreshF(icon){

            //     State.view.graph.cellRenderer.destroyIcon(icon, state );
            //     state.view.graph.view.createStateIcon(icon, state, true );
            //     state.view.graph.cellRenderer.redrawIcon(icon, state, true );

            // }

            // If ( state.icon && state.icon.attachment ) {

            //     Refresh("attachment");
            // }

            // If ( state.icon && state.icon.enterDetail ) {

            //     Refresh("enterDetail");

            // }

            // If ( state.icon && state.icon.remove ) {

            //     Refresh("remove");

            // }

        }

    };

    skCellHighlighter.prototype.toggleHighlightOff = function toggleHighlightOffF( state ) {

        state = state || this.currentState;

        if ( state && state.shape ) {

            state.style = mxUtils.clone( this.previousStyle );

            // Get the right style
            var graphS = this.graph.getStylesheet();
            var s = graphS.getCellStyle( state.cell.style, graphS.getDefaultVertexStyle() );

            state.style[ mxConstants.STYLE_STROKEWIDTH ] = s[ mxConstants.STYLE_STROKEWIDTH ]; // SkConstants.STROKE_BOX; //1;
            state.style[ mxConstants.STYLE_STROKECOLOR ] = s[ mxConstants.STYLE_STROKECOLOR ];
            state.shape.apply( state );
            state.shape.redraw();

            // state.shape.reconfigure();

        }

        this.currentState = null;
        this.previousStyle = null;

    };

    return skCellHighlighter;

} );
