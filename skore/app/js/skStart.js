
/*
 * this is the starting point, where the UI is created
 *
 */

define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skNavigatorUI = require( "skNavigatorUI/skNavigatorUI" );
    var skShell = require( "skShell" );

    skShell.nav = new skNavigatorUI();

    if ( ELECTRON && DEV ) {

        // open the dev tools of ELECTRON
        var bw = window.requireNode( "electron" ).remote.getCurrentWindow();
        bw.openDevTools();

    }

    /*
     * animation used in the title bar so that when going INSIDE a box the menu appears from the right
     *
     */

    window.jQuery.fn.extend( {

        animateCss: function( animationName ) {

            var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
            $( this ).addClass( "animated " + animationName ).one( animationEnd, function() {

                $( this ).removeClass( "animated " + animationName );

            } );

        }
    } );

    /*
     * this is the function i'm using
     *
     */

    window.textEditing = function( evt ) {

        var $ = window.jQuery;

        evt = evt || window.event;

        var allowed = false;

        if ( evt ) {

            evt = $.event.fix( evt );

            allowed = $( evt.target ).hasClass( "selectionAllowed" ) || $( evt.target ).parent( ".selectionAllowed" ).length > 0;

        }

        if ( ELECTRON ) {

            window.requireNode( "electron" ).ipcRenderer.send( "disableWhileTypingText", "allowed", !allowed );

        }

        return allowed;

    } ;

    document.onselectstart = window.textEditing;
    document.onmousedown = window.textEditing;
    document.oncontextmenu = window.textEditing;

    /*
     * next, look at skNavigatorUI
     *
     */

} );
