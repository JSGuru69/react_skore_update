
define( function( require ) {

    "use strict";

    var mxUtils = require( "mxUtils" );

    function skProcessLinkHandler( graphManager ) {

        /*jshint validthis:true*/
        this.graphManager = graphManager;

        document.body.addEventListener( "click", mxUtils.bind( this, this.supportLinks ), false );

    }

    skProcessLinkHandler.prototype.graphManager = null;

    skProcessLinkHandler.prototype.checkDomElement = function( event, element ) {

        if ( element.classList.contains( "skGoTo" ) ) {

            if ( !this.graphManager.getCurrentGraph().isEditing() ) {

                var goTo = element.getAttribute( "data-goto" );

                if ( goTo ) {

                    this.graphManager.navigateTo( goTo );

                }

            }

            event.preventDefault();

        } else if ( element.parentElement ) {

            this.checkDomElement( event, element.parentElement );

        }

    };

    skProcessLinkHandler.prototype.supportLinks = function( event ) {

        this.checkDomElement( event, event.target );

    };

    return skProcessLinkHandler;

} );
