
/**
 * my module is skGraphManager module
 * @module skGraphManager/skGraphManager
 */

define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCell = require( "mxCell" );
    var mxEvent = require( "mxEvent" );
    var mxEventObject = require( "mxEventObject" );
    var mxEventSource = require( "mxEventSource" );
    var mxGeometry = require( "mxGeometry" );
    var mxUtils = require( "mxUtils" );
    var skActions = require( "skNavigator/skActions" );
    var skConstants = require( "skConstants" );
    var skExtensions = require( "skExtensions/skExtensions" );
    var skFileSaver = require( "skGraphIO/skFileSaver" );
    var skGraph = require( "skGraph/skGraph" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );
    var skKeyboardHandler = require( "skGraphEditorUI/skKeyboardHandler" );
    var skOutline = require( "skGraphEditorUI/skOutline" );
    var skProcessLinkHandler = require( "skGraphManager/skProcessLinkHandler" );
    var skAttachments = require( "skGraph/skAttachments" );
    var skSearch = require( "skGraph/skSearch" );
    var skShell = require( "skShell" );
    require( "skGraph/skGraphCellUtil" );
    var counter = 0;

    if ( !VIEWER_MODE ) {

        require( "skGraphEditor/skGraphEditor" );
        require( "skGraphEditor/skGraphEditorCellEditor" );

    }
    var skUtils = require( "skUtils" );
    require( "skGraphIO/skGraphIO_xml" );

    /**
     * Identifier for a graph within the graphManager
     * @typedef {string|number|skGraph} graphID
     */

    /**
     * @constructor skGraphManager
     * @param {string} container - optional container in which the graphManager will create its graphs. Default is document.body
     * @returns {graphManager} the created graphManager to be stored by the outside UI
     */
    function skGraphManager( container ) {

        /*jshint validthis:true*/

        // this.navigatorUI = navigatorUI;
        this.container = container ? $( container ) : $( document.body );

        this.actions = new skActions( this );
        this.linkHandler = new skProcessLinkHandler( this );

        this.graphs = {};

        if ( VIEWER_MODE ) {

            this.defaultGraphEnabled = false;

        } else {

            this.defaultGraphEnabled = true;

        }

        this.tabsEnabled = false;

        if ( WEB_MODE || ELECTRON ) {

            this.clipboard = require( "skGraphEditorUI/skClipboard" )( this );

        }

        if ( this.tabsEnabled ) {

            this.initTabs();

        }

        this.fileSaver = new skFileSaver( this );

        this.searchEngine = new skSearch( this );

        /*
         * undomanager
         *
         */
        if ( ELECTRON || WEB_MODE ) {

            var undo = this.actions.get( "undo" );
            var redo = this.actions.get( "redo" );

            this.undoListener = function undoListenerF( undoMgr ) {

                undo.setEnabled( undoMgr.canUndo() );
                redo.setEnabled( undoMgr.canRedo() );

            };

        }

        /*
         *
         * refresh
         *
         *
         */

        this.addListener( "refresh", mxUtils.bind( this, function() {

            this.getCurrentGraph().sizeDidChange();

            // this.getCurrentGraph().resetScrollbars();

        } ) );

        /*
         * open a blank process
         *
         */

        // this.openGraph( { source:"new" } );

        // page settings changed

        this.addListener( "pageSettingsChanged", mxUtils.bind( this, function( sender, event ) {

            var graph = event.getProperty( "graph" );

            this.setPageSettings( graph, event.getProperty( "settings" ) );
            graph.fireEvent( new mxEventObject( "refreshOutline", "graph", graph ) );

        } ) );

    }

    mxUtils.extend( skGraphManager, mxEventSource );

    /**
     * Init the graphManager
     *
     */
    skGraphManager.prototype.init = function() {

        if ( ELECTRON ) {

            // will trigger loadFileContent
            this.getDataFromDesktop( );

        } else {

            this.getGraphXmlDataFromSource( );

        }

        /*
         *
         * tabs
         *
         *
         */

        if ( this.tabsEnabled && $( ".skGraphManager" ).length === 0 ) {

            var mgr = $( '<ol class="skGraphManager navigators breadcrumb forGraphEditor">' +
                            '<li><a class="newBlankGraph"><i class="fa fa-plus"></i></a></li>' +
                        "</ol>" );

            var that = this;

            // Insert after the navbar
            $( document.body ).prepend( mgr );

            $( ".newBlankGraph", mgr ).on( "click", function() {

                that.openGraph( {
                    source:"new " } );

            } );

            this.addListener( "graphEnabled", function( sender, event ) {

                var graphID = event.getProperty( "graph" ).id;
                $( ".skGraphManager li" ).removeClass( "active" );
                $( ".skGraphManager li[data-graphid='" + graphID + "']" ).addClass( "active" );

            } );

        }

    };

    skGraphManager.prototype.tabsEnabled = null;

    skGraphManager.prototype.defaultGraphEnabled = null;
    skGraphManager.prototype.graphs = null;
    skGraphManager.prototype.currentGraph = null;

    /**
     * @function
     * @name initTabs
     * @description init the tabs for the graph Manager (optional)
     * @param {graphID}
     */
    skGraphManager.prototype.initTabs = function initTabsF( graphID ) {

        if ( this.graphs[ graphID ] !== undefined ) {

            var graph = this.graphs[ graphID ];

            $( ".skGraphManager .active" ).removeClass( "active" );
            var tab = $( "<li class='active'><a>" + "untitled" + "</a></li>" );
            $( ".skGraphManager" ).prepend( tab );

            tab.attr( "data-graphid", graphID );

            var that = this;
            tab.on( "click", function() {

                that.setCurrentGraph( graph );

                // tab will change with event

            } );

            graph.addListener( "contentLoaded", function() {

                console.log( "contentLoaded, set the tab title" );
                $( "a", tab ).text( graph.getTitle() );

                // that.setCurrentGraph( graph );

            } );

            graph.addListener( "updateTitle", function() {

                $( "a", tab ).text( graph.getTitle() );

            } );

        }

    };

    /*
     * OPEN and Create
     *
     *
     */

    /**
     * @function
     * @name openGraph
     * @description Opens a graph in the graph manager.
     * @param {object} graphObject - describes the graph to be opened
     * @param {string} graphObject.xml -
     * @param {string} graphObject.source -
     * @param {string} graphObject.filePath -
     * @param {string} id -
     * @param {string} cellID - the cell code on which to focus once the process is opened
     * @param {boolean} enabled - should the graph be enabled on open (default: false)
     * @param {boolean} makeCurrent - should the graph be made current (default: true)
     * @param {graphID} graphID - optional the graph to be overriden if
     * @returns {skGraph} - the created graph
     */
    skGraphManager.prototype.openGraph = function openGraphF( graphObject, cellID, enabled, makeCurrent, graphID ) {

        enabled = enabled || this.defaultGraphEnabled;
        makeCurrent = ( makeCurrent === false ? false : true ); // default true

        var graph;

        if ( graphID ) {

            graph = this.getGraphFromID( graphID );

        }

        // if ( typeof graphObject === "string" && makeCurrent && this.graphs[ graphObject ] !== undefined ) {

        //     this.setCurrentGraph( this.graphs[ graphObject ] );

        // } else {

        graph = this.getCurrentGraph();

        if ( !graph ) {

            graph = this.createGraph();
            graph.id = graphObject.id || counter++;
            this.graphs[ graph.id ] = graph;

        } else if ( graphObject.source === "demo" && Object.keys( graph.model.cells ).length > 2 ) {

            skShell.setStatus( "Process must be empty to load a demo content" );

            return;

        }

        var content = false;

        if ( graphObject.xml && graphObject.xml !== "" ) {

            skShell.setStatus( "Loading process..." );

            var data = skUtils.zapGremlins( mxUtils.trim( graphObject.xml ) );
            var doc =  mxUtils.parseXml( data );
            content = true;
            var graphXml = "";

            /*
             * 1) load from HTML
             *
             */

            if ( doc.documentElement.tagName === "html" ) {

                // Test script type="application/skore"

                // Fixme : probably needs more granular test ?
                if ( doc.documentElement.getElementsByTagName( "script" ).length > 0 &&
                        doc.documentElement.getElementsByTagName( "script" )[ 0 ].getAttribute( "type" ) == "application/skore" ) {

                    graphXml = doc.documentElement.getElementsByTagName( "script" )[ 0 ].firstChild;

                } else {

                    console.log( "invalid file" );

                    graphObject.source = "invalidHTML";
                    skShell.setStatus( "Not a valid Skore HTML file.<br/>Only file created in Skore app (desktop) can be opened here." );
                    content = false;

                }

            }

            /*
             * 2) ready data
             *
             */

            else {

                graphXml = doc.documentElement;

            }

            graph.model.beginUpdate();

            try {

                graph.setGraphXml( graphXml, graphObject.source );
                graph.initTemplateManager();
                graph.initRoleManager();

            } catch ( e ) {

                console.error( e );

            } finally {

                graph.model.endUpdate();

            }

            if ( ELECTRON ) {

                var bw = window.requireNode( "electron" ).remote.getCurrentWindow();

                if ( content ) {

                    skShell.currentFilePath = graphObject.filePath;

                    if ( graphObject.filePath ) {

                        window.requireNode( "electron" ).remote.app.addRecentDocument( graphObject.filePath );

                        bw.setTitle( graphObject.filePath.split( "/" ).pop() );

                    } else {

                        bw.setTitle( graph.getTitle() );

                    }

                }

                graph.setDocumentEdited( false );

            }

            if ( !graph.extensions ) {

                graph.extensions = new skExtensions( graph );

            }

            graph.extensions.loadExistingExtensions();

            /*
             * After loading the file...
             *
             */

            // Recomputes the size of the cells for the current view... the rest will be done later
            if ( graph.view ) {

                // reset the form manager

                graph.cellHTMLCache = [];

                // graph.updateCellsSize( true );

            }

        } else {

            // Stylesheet(s)
            // If there is no content, we force loading the default stylesheet

            console.log( "will load stylesheet" );
            graph.getStylesheet().loadCustomStylesheet( false );

        }

        // this.graphs.push( graph );

        if ( makeCurrent ) {

            this.setCurrentGraph( graph );

        }

        graph.view.validate();

        // this.navigatorUI.refreshNow( graph );
        this.fireEvent( new mxEventObject( "refresh" ) );

        graph.fireEvent( new mxEventObject( "contentLoaded", "graph", graph, "source", graphObject.source ) );
        graph.fireEvent( new mxEventObject( "refreshOutline" ) );
        graph.setEnabled( enabled );

        graph.resetScrollbars();

        // }

        return graph;

    }; // end openGraph

    /**
     * @function
     * @name isGraphOpened
     * @description Returns true if the graph is opened in the graphManager (might be opened but not visible!)
     * @param {graphID} - optional graph to test. Default is the current graph
     * @returns {boolean}
     */
    skGraphManager.prototype.isGraphOpened = function isGraphOpenedF( graphID ) {

        var graph = this.getGraphFromID( graphID );

        return ( graph !== null );

    };

    /**
     * @function
     * @name closeGraph
     * @description Close the given graph from the graphManager
     * @param {graphID}
     */
    skGraphManager.prototype.closeGraph = function closeGraphF( graphID ) {

        var graph = this.getGraphFromID( graphID );

        if ( graph ) {

            $( graph.container ).remove();
            this.graphs[ graph.id ] = null;

            this.fireEvent( new mxEventObject( "graphClosed", "graph", graph ) );

        }

    };

    /*
     *
     * VISIBLE
     *
     *
     */

    /**
     * @function
     * @name  setGraphVisible
     * @description Make the given graph VISIBLE. Triggers <graphVisible>
     * @param {graphID} graphID - optional graph to test. Default is the current graph
     * @param {boolean} visible - true if the graph should be made visible. False for hidden. Default is true.
     * @returns {skGraph} - the graph that has been made visible / hidden
     */
    skGraphManager.prototype.setGraphVisible = function( graphID, visible ) {

        var graph = this.getGraphFromID( graphID );

        // hide all the other graphs
        $( ".skDiagramContainer" ).hide();

        // show the right one (or not)
        visible = ( visible === false ? false : true );
        $( graph.container ).toggle( visible );

        this.fireEvent( new mxEventObject( "graphVisible", "graph", graph, "visible", visible ) );

        return graph;

    };

    /**
     * @function
     * @name isGraphVisible
     * @description Test if the given graph is visible.
     * @param {graphID} - optional graph to test. Default is the current graph
     * @returns {skGraph} - the graph in question
     */
    skGraphManager.prototype.isGraphVisible = function( graphID ) {

        var graph = this.getGraphFromID( graphID );

        $( graph.container ).is( ":visible" );

        return graph;

    };

    /*
     *
     * make current
     *
     *
     */

    /**
     * Test if the given graph is current.
     * @param {graphID} - optional graph to test. Default is the current graph
     * @returns {boolean} - true if current
     */
    skGraphManager.prototype.isCurrentGraph = function( graphID ) {

        var graph = this.getGraphFromID( graphID );
        if ( graph === this.currentGraph ) {

            return true;

        }

        return false;

    };

    /**
     * Test if the given graph is visible. triggers <graphEnabled>
     * @param {graphID} - optional graph to test. Default is the current graph
     * @returns {skGraph} - the graph in question
     */
    skGraphManager.prototype.setCurrentGraph  = function( graphID ) {

        var graph = this.getGraphFromID( graphID );

        if ( graph ) {

            this.setGraphVisible( graph );

            this.currentGraph = graph;

            this.fireEvent( new mxEventObject( "graphEnabled", "graph", graph, "enabled", graph.isEnabled() ) );

        }

        return graph;

    };

    /**
     * Returns the current graph
     * @returns {skGraph} - the graph in question
     */
    skGraphManager.prototype.getCurrentGraph  = function() {

        return this.currentGraph;

    };

    /*
     *
     * enable
     *
     *
     */

    /**
     * Test if the given graph is enabled (can be edited). Triggers <graphEnabled>
     * @param {graphID} - optional graph to test. Default is the current graph
     * @returns {boolean} - true if graph is enabled
     */
    skGraphManager.prototype.isGraphEnabled = function( graphID ) {

        var graph = this.getGraphFromID( graphID );

        return ( graph && graph.isEnabled() );

    };

    /**
     * make the graph enabled. will fire <graphEnabled> via <skGraph.setEnabled>
     * @param {graphID} - optional graph to test. Default is the current graph
     * @param {enabled} - optional, true if to make enabled. Default is true.
     * @returns {skGraph} - the graph in question
     */
    skGraphManager.prototype.setGraphEnabled = function enableGraphF( graphID, enabled ) {

        var graph = this.getGraphFromID( graphID );
        enabled = ( enabled !== false ? true : false );

        if ( graph ) {

            graph.setEnabled( enabled ); // will fire event

        }

    };

    /*
     * crate a graph
     *
     *
     */

    /**
     * internal function to create a new empty graph. Triggers <newGraphCreated> events
     * @returns {skGraph} - the graph in question
     */
    skGraphManager.prototype.createGraph = function createGraphF() {

        var graph = new skGraph();

        /*
         *
         * creates the container and attach to container
         *
         */

        var graphContainer = document.createElement( "div" );
        graphContainer.classList.add( "skDiagramContainer", "forGraphEditor" );
        this.container.append( graphContainer );

        // loads the graph in the container
        graph.init( graphContainer );

        /*
         * will setup all actions with a "newGraphCreated" listener
         *
         */

        this.fireEvent( new mxEventObject( "newGraphCreated", "graph", graph ) );
        this.keyHandler = new skKeyboardHandler( graph, this.actions );

        /*
         * relays events
         *
         */

        graph.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

            var graph = event.getProperty( "graph" );

            this.fireEvent( new mxEventObject( "graphEnabled",
                "graph", graph,
                "enabled", event.getProperty( "enabled" ),
                "box", "" + graph.id + "." + graph.getDefaultParent().id + "d" ) );

        } ) );
        graph.addListener( "navigationStarted", mxUtils.bind( this, function( sender, event ) {

            this.fireEvent( new mxEventObject( "navigationStarted",
                "graph", event.getProperty( "graph" ),
                "destinationCell", event.getProperty( "destinationCell" ),
                "stayAbove", event.getProperty( "stayAbove" )
                ) );

        } ) );
        graph.addListener( "navigationDone", mxUtils.bind( this, function( sender, event ) {

            this.fireEvent( new mxEventObject( "navigationDone",
                "graph", graph,
                "destinationCell", event.getProperty( "destinationCell" ),
                "stayAbove", event.getProperty( "stayAbove" )
                ) );

        } ) );
        graph.addListener( "contentLoaded", mxUtils.bind( this, function( ) {

            this.fireEvent( new mxEventObject( "contentLoaded",
                "graph", graph
                ) );

        } ) );

        graph.getSelectionModel().addListener( mxEvent.CHANGE, mxUtils.bind( this, function( sender, evt ) {

            this.fireEvent( new mxEventObject( "boxSelectionChanged",
                "graph", graph,
                "cellsRemoved", evt.getProperty( "added" ), // yes it's correct
                "cellsAdded", evt.getProperty( "removed" ) // still correct
                ) );

        } ) );

        /*
         * events
         *
         *
         */

        graph.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

            console.log( "skGraphManager listener skNewRoleManager" );

            var enabled = event.getProperty( "enabled" );
            var graph = event.getProperty( "graph" );

            /*
             * undo manager and button sync
             *
             */

            if ( enabled ) {

                if ( graph.undoManager ) {

                    graph.undoManager.addListener( mxEvent.ADD, this.undoListener );
                    graph.undoManager.addListener( mxEvent.UNDO, this.undoListener );
                    graph.undoManager.addListener( mxEvent.REDO, this.undoListener );
                    graph.undoManager.addListener( mxEvent.CLEAR, this.undoListener );

                    // Updates the button states once
                    this.undoListener( graph.undoManager );

                }

            } else {

                if ( graph.undoManager ) {

                    graph.undoManager.removeListener( this.undoListener );

                }

            }

            // in all case we remake the outline
            this.outline = new skOutline( graph );

        } ) );

        graph.addListener( "openAttachment", mxUtils.bind( this, function( sender, event ) {

            this.actions.openModal( "attachments",
                "cell", event.getProperty( "cell" ),
                "forceOpen", event.getProperty( "forceOpen" ),
                "atttype", event.getProperty( "atttype" ) );

        } ) );

        if ( !VIEWER_MODE ) {

            graph.initGraphEditor();

        }
        this.outline = new skOutline( graph );

        return graph;

    };

    skGraphManager.prototype.getDataFromJustSkoreIt = function( id ) {

        var that = this;

        return $.ajax( {
            url: "https://api.github.com/gists/" + id + "?callback",
            type: "GET",

            dataType: "jsonp",

            success: function( data ) {

                that.openGraph( {
                    xml:decodeURIComponent( data.data.files[ "gist.skore" ].content ),
                    source: skConstants.SOURCE_JUSTSKOREIT
                } );

            }
        } );

    };

    skGraphManager.prototype.getDataFromDesktop = function() {

        if ( ELECTRON ) {

            var that = this;

            var filePath = window.requireNode( "electron" ).ipcRenderer.sendSync( "readyToLoadFileContent" );

            if ( filePath && filePath !== "" ) {

                var data = window.requireNode( "fs-jetpack" ).read( filePath );

                if ( data ) {

                    that.openGraph( {
                        xml:data,
                        source:skConstants.SOURCE_FILE,
                        filePath: filePath
                    } );

                } else {

                    console.log( "can't read file" );

                }

            } else {

                that.openGraph( {
                    xml: "",
                    source: "noSource"
                } );

            }

        }

    };

    skGraphManager.prototype.getGraphXmlDataFromSource = function( ) {

        console.log( "getGraphXmlDataFromSource" );

        var that = this;

        if ( !ELECTRON ) {

            // Read the url to see if there is a skore to load
            skShell.readUrlParams( window.location.href );

            if ( window.location.search === "?embed" ) {

                console.log( "load embed" );

                // setup the listener
                window.addEventListener( "message", function( event ) {

                    that.openGraph( {
                        xml: event.data.trim(),
                        source: skConstants.SOURCE_EMBED
                    } );

                } );

                window.parent.postMessage( "ready", "*" );

            }

            if ( skShell.urlParams[ skConstants.JUSTSKOREIT ] != null ) {

                console.log( "load jutskore it" );

                that.getDataFromJustSkoreIt( skShell.urlParams[ skConstants.JUSTSKOREIT ] );

            } else if ( ( VIEWER_MODE || WEB_MODE ) &&
            ( skShell.urlParams.source === "skEnterprise" ||
              skShell.urlParams.source === "skEnterpriseViewer" ) ) {

                var url = skShell.urlParams.url;

                // <iframe src="demo/index.html?source=skEnterprise&url=http://skoreappdev.com/backend/api/web/&amp;processID=93" ng-src="demo/index.html?url=http://skoreappdev.com/backend/api/web/&amp;processID=93" class="se-app-iframe ng-scope"></iframe>

                if ( url ) {

                    $.ajax( {
                        url: url,
                        type: "GET"

                    } ).done( function( data ) {

                        that.openGraph( {
                            xml: data.graphXml,
                            source: skConstants.SRC_ENT
                        } );

                    } ).error( function( err ) {

                        console.log( err );

                    } );

                } else {

                    skShell.setStatus( "Need process ID, mate." );

                }

            } else {

                that.openGraph( {
                    xml: "",
                    source: skConstants.SOURCE_NEW
                } );

            }

        }

    };

    skGraphManager.prototype.openFromFilePath = function openFromFilePathF( filePath ) {

        var graph = this.getCurrentGraph();

        if ( graph.getDefaultParent().children !== null && graph.getDefaultParent().children.length ) {

            console.log( "there are children, beware! will load " + filePath );
            window.requireNode( "electron" ).ipcRenderer.send( "newskorewindow", filePath );

        } else {

            // TODO
            var fsj = window.requireNode( "fs-jetpack" );
            var data = fsj.read( filePath );

            this.openGraph( {
                xml:data,
                source:skConstants.SOURCE_FILE,
                filePath:filePath } );

        }

    };

    skGraphManager.prototype.container = null;

    /*
     *
     * API Utils
     *
     */

    /**
     * @function
     * @name getGraphFromID
     * @params {graphID} - optional ID
     * @returns {skGraph} - the graph matching or the currentgraph if no ID is specified
     */

    skGraphManager.prototype.getGraphFromID = function( graphID ) {

        if ( graphID instanceof skGraph ) {

            return graphID;

        } else if ( typeof graphID === "string" ) {

            if ( this.graphs[ graphID ] !== undefined ) {

                return this.graphs[ graphID ];

            }

        } else if ( graphID == null ) {

            return this.getCurrentGraph();

        }

        return null;

    };

    skGraphManager.prototype.getTitle = function( graphID ) {

        var graph = this.getGraphFromID( graphID );

        return graph.getTitle();

    };

    /*
     *
     * selection cells
     *
     */

    skGraphManager.prototype.getSelectionCells = function( graphID ) {

        var graph = this.getGraphFromID( graphID );

        if ( graph ) {

            return graph.getSelectionCells();

        }

    };

    /*
     * NAVIGATION
     *
     *
     */

    skGraphManager.prototype.navigateTo = function( cellCode ) {

        /*
         * check if we should stay in the same process
         *
         */

        if ( cellCode.indexOf( "." ) == -1 ) {

            this.getCurrentGraph().navigateToCellHandler( cellCode );

        } else {

            cellCode = cellCode.split( "." );

            var graph = this.getGraphFromID( cellCode[ 0 ] );

            // graph is opened, we assume we can just navigate to it
            if ( this.isGraphOpened( graph ) ) {

                this.setGraphVisible( graph );
                graph.navigateToCellHandler( cellCode[ 1 ] );

            }

            // if graph is not opened
            else {

                // the graph is not opened,
                this.fireEvent( new mxEventObject( "requestGraphOpen", "graphID", cellCode[ 0 ], "cellCode", cellCode[ 1 ] ) );

            }

        }

    };

    /*
     *
     * CARDSHOE
     *
     *
     */

    var createDragElement = function( type ) {

        var dragElement = $( "<div class='icon_dragged icon_" + type + "_dragged'>" +
                "<div class='icon_dragged_inner'>Drop me on the canvas</div>" +
            "</div>" );

        return dragElement[ 0 ];

    };

    skGraphManager.prototype.makeDropFunction = function( type, valueTextToReuse ) {

        return mxUtils.bind( this, function( graph, evt, target, x, y ) {

            var style = graph.getStylesheet().styles[ type ];

            var value;

            if ( typeof valueTextToReuse === "object" ) {

                value = valueTextToReuse;

            } else if ( typeof valueTextToReuse === "string" ) {

                value = skUtils.createBoxElement( type );
                $( "box", value ).append( $( "<text value='" + valueTextToReuse + "'></text>" ) );
                $( "text", value ).attr( "value", valueTextToReuse );

            } else {

                value = skUtils.createBoxElement( type );

            }

            var w = isNaN( parseInt( style.width, 10 ) ) ? skConstants.WB_WIDTH : parseInt( style.width, 10 );
            var h = isNaN( parseInt( style.height, 10 ) ) ? skConstants.WB_HEIGHT : parseInt( style.height, 10 );

            // Creates the box
            var box = new mxCell(
                value,
                new mxGeometry( 0, 0, w, h ),
                type );

            box.vertex = true;
            var boxes = graph.importCells( [ box ], x, y, target );

            if ( boxes !== null && boxes.length > 0 ) {

                graph.scrollCellToVisible( boxes[ 0 ] );
                graph.setSelectionCells( boxes );

                this.fireEvent( new mxEventObject( "boxDropped",
                    "graph", graph,
                    "cells", boxes ) );

                // Enter editmode except if dropped from cardshoe
                if ( !valueTextToReuse ) {

                    graph.cellEditor.startEditing( boxes[ 0 ] );
                    setTimeout( function() {

                     $( $( "textarea, input[type=text]", graph.cellEditor.wrapperDiv )[ 0 ] ).select();

                }, 100 );

                }

            }

        } );

    };

    skGraphManager.prototype.makeCardshoeBoxDraggable = function( element, type, valueTextToReuse ) {

        return mxUtils.makeDraggable(
            element,
            mxUtils.bind( this, this.getCurrentGraph ),
            this.makeDropFunction( type, valueTextToReuse ),
            createDragElement( type ),
            null, null, true, true );

    };

    /**
     * get the page settings for the given process
     * @param {graphID}
     * @returns {object} see below for structure
     */
    skGraphManager.prototype.getIOWhyboxes = function getIOWhyboxes( cell ) {

        var result = {
            inputs: [],
            outputs: []
        };

        if ( cell.edges ) {

            // fetch inputs / outputs
            cell.edges.forEach( function( edge ) {

                if ( edge.target.isWhybox() && edge.source === cell ) {

                    result.outputs.push( edge.target );

                } else if ( edge.source.isWhybox() && edge.target === cell )  {

                    result.inputs.push( edge.source );

                }

            } );

        }

        return result;

    };

    /*
     *
     * page settings
     *
     *
     */

    /**
     * get the page settings for the given process
     * @param {graphID}
     * @returns {object} see below for structure
     */
    skGraphManager.prototype.getPageSettings = function( graphID ) {

        var graph = this.getGraphFromID( graphID );

        return skGraphUtils.getPageSettings( graph );

    };

    /**
     * set the page settings for the given process
     * @param {graphID}
     * @param {object} pageSettings - JSON type object
     * @param {boolean} pageSettings.pageView
     * @param {boolean} pageSettings.grid
     * @param {double} pageSettings.pageScale
     * @param {string} pageSettings.backgroundColor
     * @param {string} pageSettings.paperSize - can be "A4", "A3", "Letter", "Legal", "Tabloid"
     * @param {boolean} pageSettings.landscape - whether it should be landscape or not (portrait)
     * @returns {skGraph} the changed graph
     */
    skGraphManager.prototype.setPageSettings = function( graphID, pageSettings ) {

        var graph = this.getGraphFromID( graphID );

        var currentPageSettings = skGraphUtils.getPageSettings( graph );

        var newPageSettings = $.extend( {}, currentPageSettings, pageSettings );

        var hasScrollbars, tx, ty;

        // only usefull when pageView changed?
        if ( currentPageSettings.pageView !== pageSettings.pageView ) {

            hasScrollbars = mxUtils.hasScrollbars( graph.container );
            tx = 0;
            ty = 0;

            if ( hasScrollbars )
            {

                tx = graph.view.translate.x * graph.view.scale - graph.container.scrollLeft;
                ty = graph.view.translate.y * graph.view.scale - graph.container.scrollTop;

            }

        }

        graph.pageVisible = newPageSettings.pageView;
        graph.pageBreaksVisible = newPageSettings.pageView;
        graph.preferPageSize = newPageSettings.pageView;

        graph.setGridEnabled( newPageSettings.grid );
        graph.pageScale = newPageSettings.pageScale;
        graph.background = newPageSettings.backgroundColor;

        graph.pageFormat = skGraphUtils.getPageFormat( newPageSettings.paperSize, newPageSettings.landscape );

        graph.refresh();
        graph.updateGraphComponents();
        graph.view.validateBackground();
        graph.sizeDidChange();

        if ( currentPageSettings.pageView !== pageSettings.pageView ) {

            // Workaround for possible handle offset
            if ( hasScrollbars ) {

                var cells = graph.getSelectionCells();
                graph.clearSelection();
                graph.setSelectionCells( cells );

            }

            if ( hasScrollbars && graph.container ) {

                graph.container.scrollLeft = graph.view.translate.x * graph.view.scale - tx;
                graph.container.scrollTop = graph.view.translate.y * graph.view.scale - ty;

            }

        }

        this.fireEvent( new mxEventObject( "graphBackgroundChanged", "graph", this ) );

        return graph;

    };

    skGraphManager.prototype.getStyleForCell = function( graphID, cellType, styleSheetName, cells ) {

        var graph = this.getGraphFromID( graphID );
        cellType = cellType || "whatbox";
        styleSheetName = styleSheetName || "default";

        if ( graph ) {

            return graph.getStylesheet().buildCustomStyle( cellType, styleSheetName, cells );

        }

        return null;

    };

    /**
     * @function
     * @name setStyleSheet
     * @param {graphID} - as usual
     * @param {object} styleObject - the object output by the modal
     * @param {string} styleSheetName - the name of the stylesheet. Default is "default" (only supported so far)
     * @param {array} cells - the save to change the style (otherwise it will be for the graph)
     */
    skGraphManager.prototype.setStyleSheet = function( graphID, styleObject, styleSheetName, cells ) {

        var graph = this.getGraphFromID( graphID );

        if ( graph ) {

            graph.getStylesheet().saveStyleSheet( styleObject, null, cells );

        }

    };

    /**
     * @function
     * @name getXmlForGraph
     * @description get the XML for the given graph
     * @param {graphID} as usual
     * @pretty {boolean} true if output pretty XML, default is false
     */
    skGraphManager.prototype.getXmlForGraph = function( graphID, pretty ) {

        var graph = this.getGraphFromID( graphID );

        var xml = graph.getGraphXml();
        if ( pretty ) {

            return mxUtils.getPrettyXml( xml );

        }
        return xml;

    };

    /**
     * @function
     * @name triggerGraphAction
     * @description will trigger the action for the current graph. Not possible to trigger for another graph at the moment
     * @param {graphID} can be left null
     * @param {string} actionName - action to trigger. see skActions.js for list
     */

    skGraphManager.prototype.triggerGraphAction = function( graphID, actionName ) {

        var graph = this.getGraphFromID();
        if ( graph ) {

            var act = this.actions.get( actionName );

            if ( act !== null && act.funct !== null ) {

                act.funct();

            }

        }

    };

    /*
     *
     * ATTACHMENTS
     *
     *
     */

    /**
     * @function
     * @name renderAttachments
     * @return {object} form - form.container to be inserted where you want
     */

    skGraphManager.prototype.renderAttachments = function( graph, cell, type ) {

        return skAttachments.renderAttachments( cell, type );

    };

    /**
     * @function
     * @name getAttachmentTypes
     * @returns {array} of string attachments type
     */
    skGraphManager.prototype.getAttachmentTypes = function() {

        return skAttachments.getAttachmentTypes();

    };

    /**
     * @function
     * @name createNewAttachment
     * @param {graphID} as usual
     * @param {mxCell} cell - mandatory
     * @returns {object} form - the attachmetn object. form.container to be inserted where you want
     */
    skGraphManager.prototype.createNewAttachment = function( graphID, cell, attachmentType ) {

        var graph = this.getGraphFromID( graphID );
        return skAttachments.createNewAttachment( graph, cell, attachmentType );

    };

    skGraphManager.prototype.searchEngine = null;

    return skGraphManager;

} );
