
/**
 * my module is skGraphManager module
 * @module skGraphManager/skGraphManager
 */
/* jshint ignore:start */

define( "skGraphManager/skGraphManager", [ "require", "skGraphEditorUI/skClipboard", "jquery", "mxCell", "mxEvent", "mxEventObject", "mxEventSource", "mxGeometry", "mxUtils", "skNavigator/skActions", "skConstants", "skExtensions/skExtensions", "skGraphIO/skFileSaver", "skGraph/skGraph", "skGraph/skGraphUtils", "skGraphEditorUI/skKeyboardHandler", "skGraphEditorUI/skOutline", "skGraphManager/skProcessLinkHandler", "skGraph/skAttachments", "skGraph/skSearch", "skShell", "skGraph/skGraphCellUtil", "skGraphEditor/skGraphEditor", "skGraphEditor/skGraphEditorCellEditor", "skUtils", "skGraphIO/skGraphIO_xml" ], function( require ) {

    "use strict";

    function skGraphManager( container ) {

        this.container = $( container ? container : document.body ), this.actions = new skActions( this ),
            this.linkHandler = new skProcessLinkHandler( this ), this.graphs = {}, this.defaultGraphEnabled = !0,
            this.tabsEnabled = !1, this.clipboard = require( "skGraphEditorUI/skClipboard" )( this ),
            this.tabsEnabled && this.initTabs(), this.fileSaver = new skFileSaver( this ), this.searchEngine = new skSearch( this );
        var undo = this.actions.get( "undo" ),
            redo = this.actions.get( "redo" );
        this.undoListener = function( undoMgr ) {

            undo.setEnabled( undoMgr.canUndo() ), redo.setEnabled( undoMgr.canRedo() );

        }, this.addListener( "refresh", mxUtils.bind( this, function() {

            this.getCurrentGraph().sizeDidChange();

        } ) ), this.addListener( "pageSettingsChanged", mxUtils.bind( this, function( sender, event ) {

            var graph = event.getProperty( "graph" );
            this.setPageSettings( graph, event.getProperty( "settings" ) ), graph.fireEvent( new mxEventObject( "refreshOutline", "graph", graph ) );

        } ) );

    }
    var $ = require( "jquery" ),
        mxCell = require( "mxCell" ),
        mxEvent = require( "mxEvent" ),
        mxEventObject = require( "mxEventObject" ),
        mxEventSource = require( "mxEventSource" ),
        mxGeometry = require( "mxGeometry" ),
        mxUtils = require( "mxUtils" ),
        skActions = require( "skNavigator/skActions" ),
        skConstants = require( "skConstants" ),
        skExtensions = require( "skExtensions/skExtensions" ),
        skFileSaver = require( "skGraphIO/skFileSaver" ),
        skGraph = require( "skGraph/skGraph" ),
        skGraphUtils = require( "skGraph/skGraphUtils" ),
        skKeyboardHandler = require( "skGraphEditorUI/skKeyboardHandler" ),
        skOutline = require( "skGraphEditorUI/skOutline" ),
        skProcessLinkHandler = require( "skGraphManager/skProcessLinkHandler" ),
        skAttachments = require( "skGraph/skAttachments" ),
        skSearch = require( "skGraph/skSearch" ),
        skShell = require( "skShell" );
    require( "skGraph/skGraphCellUtil" );
    var counter = 0;
    require( "skGraphEditor/skGraphEditor" ), require( "skGraphEditor/skGraphEditorCellEditor" );
    var skUtils = require( "skUtils" );
    require( "skGraphIO/skGraphIO_xml" ), mxUtils.extend( skGraphManager, mxEventSource ),
        skGraphManager.prototype.init = function() {

            if ( this.getGraphXmlDataFromSource(), this.tabsEnabled && 0 === $( ".skGraphManager" ).length ) {

                var mgr = $( '<ol class="skGraphManager navigators breadcrumb forGraphEditor"><li><a class="newBlankGraph"><i class="fa fa-plus"></i></a></li></ol>' ),
                    that = this;
                $( document.body ).prepend( mgr ), $( ".newBlankGraph", mgr ).on( "click", function() {

                    that.openGraph( {
                        source: "new "
                    } );

                } ), this.addListener( "graphEnabled", function( sender, event ) {

                    var graphID = event.getProperty( "graph" ).id;
                    $( ".skGraphManager li" ).removeClass( "active" ), $( ".skGraphManager li[data-graphid='" + graphID + "']" ).addClass( "active" );

                } );

            }

        }, skGraphManager.prototype.tabsEnabled = null, skGraphManager.prototype.defaultGraphEnabled = null,
        skGraphManager.prototype.graphs = null, skGraphManager.prototype.currentGraph = null,
        skGraphManager.prototype.initTabs = function( graphID ) {

            if ( void 0 !== this.graphs[ graphID ] ) {

                var graph = this.graphs[ graphID ];
                $( ".skGraphManager .active" ).removeClass( "active" );
                var tab = $( "<li class='active'><a>untitled</a></li>" );
                $( ".skGraphManager" ).prepend( tab ), tab.attr( "data-graphid", graphID );
                var that = this;
                tab.on( "click", function() {

                    that.setCurrentGraph( graph );

                } ), graph.addListener( "contentLoaded", function() {

                    $( "a", tab ).text( graph.getTitle() );

                } ), graph.addListener( "updateTitle", function() {

                    $( "a", tab ).text( graph.getTitle() );

                } );

            }

        }, skGraphManager.prototype.openGraph = function( graphObject, cellID, enabled, makeCurrent, graphID ) {

            enabled = enabled || this.defaultGraphEnabled, makeCurrent = makeCurrent === !1 ? !1 : !0;
            var graph;
            if ( graphID && ( graph = this.getGraphFromID( graphID ) ), graph = this.getCurrentGraph() ) {

                if ( "demo" === graphObject.source && Object.keys( graph.model.cells ).length > 2 ) return void skShell.setStatus( "Process must be empty to load a demo content" );

            } else graph = this.createGraph(), graph.id = graphObject.id || counter++, this.graphs[ graph.id ] = graph;
            var content = !1;
            if ( graphObject.xml && "" !== graphObject.xml ) {

                skShell.setStatus( "Loading process..." );
                var data = skUtils.zapGremlins( mxUtils.trim( graphObject.xml ) ),
                    doc = mxUtils.parseXml( data );
                content = !0;
                var graphXml = "";
                "html" === doc.documentElement.tagName ? doc.documentElement.getElementsByTagName( "script" ).length > 0 && "application/skore" == doc.documentElement.getElementsByTagName( "script" )[ 0 ].getAttribute( "type" ) ? graphXml = doc.documentElement.getElementsByTagName( "script" )[ 0 ].firstChild : ( graphObject.source = "invalidHTML",
                    skShell.setStatus( "Not a valid Skore HTML file.<br/>Only file created in Skore app (desktop) can be opened here." ),
                    content = !1 ) : graphXml = doc.documentElement, graph.model.beginUpdate();
                try {

                    graph.setGraphXml( graphXml, graphObject.source ), graph.initTemplateManager(), graph.initRoleManager();

                } catch ( e ) {} finally {

                    graph.model.endUpdate();

                }
                graph.extensions || ( graph.extensions = new skExtensions( graph ) ), graph.extensions.loadExistingExtensions(),
                    graph.view && ( graph.cellHTMLCache = [] );

            } else graph.getStylesheet().loadCustomStylesheet( !1 );
            return makeCurrent && this.setCurrentGraph( graph ), graph.view.validate(), this.fireEvent( new mxEventObject( "refresh" ) ),
                graph.fireEvent( new mxEventObject( "contentLoaded", "graph", graph, "source", graphObject.source ) ),
                graph.fireEvent( new mxEventObject( "refreshOutline" ) ), graph.setEnabled( enabled ),
                graph.resetScrollbars(), graph;

        }, skGraphManager.prototype.isGraphOpened = function( graphID ) {

            var graph = this.getGraphFromID( graphID );
            return null !== graph;

        }, skGraphManager.prototype.closeGraph = function( graphID ) {

            var graph = this.getGraphFromID( graphID );
            graph && ( $( graph.container ).remove(), this.graphs[ graph.id ] = null, this.fireEvent( new mxEventObject( "graphClosed", "graph", graph ) ) );

        }, skGraphManager.prototype.setGraphVisible = function( graphID, visible ) {

            var graph = this.getGraphFromID( graphID );
            return $( ".skDiagramContainer" ).hide(), visible = visible === !1 ? !1 : !0, $( graph.container ).toggle( visible ),
                this.fireEvent( new mxEventObject( "graphVisible", "graph", graph, "visible", visible ) ),
                graph;

        }, skGraphManager.prototype.isGraphVisible = function( graphID ) {

            var graph = this.getGraphFromID( graphID );
            return $( graph.container ).is( ":visible" ), graph;

        }, skGraphManager.prototype.isCurrentGraph = function( graphID ) {

            var graph = this.getGraphFromID( graphID );
            return graph === this.currentGraph ? !0 : !1;

        }, skGraphManager.prototype.setCurrentGraph = function( graphID ) {

            var graph = this.getGraphFromID( graphID );
            return graph && ( this.setGraphVisible( graph ), this.currentGraph = graph, this.fireEvent( new mxEventObject( "graphEnabled", "graph", graph, "enabled", graph.isEnabled() ) ) ),
                graph;

        }, skGraphManager.prototype.getCurrentGraph = function() {

            return this.currentGraph;

        }, skGraphManager.prototype.isGraphEnabled = function( graphID ) {

            var graph = this.getGraphFromID( graphID );
            return graph && graph.isEnabled();

        }, skGraphManager.prototype.setGraphEnabled = function( graphID, enabled ) {

            var graph = this.getGraphFromID( graphID );
            enabled = enabled !== !1 ? !0 : !1, graph && graph.setEnabled( enabled );

        }, skGraphManager.prototype.createGraph = function() {

            var graph = new skGraph(),
                graphContainer = document.createElement( "div" );
            return graphContainer.classList.add( "skDiagramContainer", "forGraphEditor" ), this.container.append( graphContainer ),
                graph.init( graphContainer ), this.fireEvent( new mxEventObject( "newGraphCreated", "graph", graph ) ),
                this.keyHandler = new skKeyboardHandler( graph, this.actions ), graph.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

                    var graph = event.getProperty( "graph" );
                    this.fireEvent( new mxEventObject( "graphEnabled", "graph", graph, "enabled", event.getProperty( "enabled" ), "box", "" + graph.id + "." + graph.getDefaultParent().id + "d" ) );

                } ) ), graph.addListener( "navigationStarted", mxUtils.bind( this, function( sender, event ) {

                    this.fireEvent( new mxEventObject( "navigationStarted", "graph", event.getProperty( "graph" ), "destinationCell", event.getProperty( "destinationCell" ), "stayAbove", event.getProperty( "stayAbove" ) ) );

                } ) ), graph.addListener( "navigationDone", mxUtils.bind( this, function( sender, event ) {

                    this.fireEvent( new mxEventObject( "navigationDone", "graph", graph, "destinationCell", event.getProperty( "destinationCell" ), "stayAbove", event.getProperty( "stayAbove" ) ) );

                } ) ), graph.addListener( "contentLoaded", mxUtils.bind( this, function() {

                    this.fireEvent( new mxEventObject( "contentLoaded", "graph", graph ) );

                } ) ), graph.getSelectionModel().addListener( mxEvent.CHANGE, mxUtils.bind( this, function( sender, evt ) {

                    this.fireEvent( new mxEventObject( "boxSelectionChanged", "graph", graph, "cellsRemoved", evt.getProperty( "added" ), "cellsAdded", evt.getProperty( "removed" ) ) );

                } ) ), graph.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

                    var enabled = event.getProperty( "enabled" ),
                        graph = event.getProperty( "graph" );
                    enabled ? graph.undoManager && ( graph.undoManager.addListener( mxEvent.ADD, this.undoListener ),
                            graph.undoManager.addListener( mxEvent.UNDO, this.undoListener ), graph.undoManager.addListener( mxEvent.REDO, this.undoListener ),
                            graph.undoManager.addListener( mxEvent.CLEAR, this.undoListener ), this.undoListener( graph.undoManager ) ) : graph.undoManager && graph.undoManager.removeListener( this.undoListener ),
                        this.outline = new skOutline( graph );

                } ) ), graph.addListener( "openAttachment", mxUtils.bind( this, function( sender, event ) {

                    this.actions.openModal( "attachments", "cell", event.getProperty( "cell" ), "forceOpen", event.getProperty( "forceOpen" ), "atttype", event.getProperty( "atttype" ) );

                } ) ), graph.initGraphEditor(), this.outline = new skOutline( graph ), graph;

        }, skGraphManager.prototype.getDataFromJustSkoreIt = function( id ) {

            var that = this;
            return $.ajax( {
                url: "https://api.github.com/gists/" + id + "?callback",
                type: "GET",
                dataType: "jsonp",
                success: function( data ) {

                    that.openGraph( {
                        xml: decodeURIComponent( data.data.files[ "gist.skore" ].content ),
                        source: skConstants.SOURCE_JUSTSKOREIT
                    } );

                }
            } );

        }, skGraphManager.prototype.getDataFromDesktop = function() {}, skGraphManager.prototype.getGraphXmlDataFromSource = function() {

            var that = this;
            if ( skShell.readUrlParams( window.location.href ), "?embed" === window.location.search && ( window.addEventListener( "message", function( event ) {

                    that.openGraph( {
                        xml: event.data.trim(),
                        source: skConstants.SOURCE_EMBED
                    } );

                } ), window.parent.postMessage( "ready", "*" ) ), null != skShell.urlParams[ skConstants.JUSTSKOREIT ] ) that.getDataFromJustSkoreIt( skShell.urlParams[ skConstants.JUSTSKOREIT ] );
            else if ( "skEnterprise" === skShell.urlParams.source || "skEnterpriseViewer" === skShell.urlParams.source ) {

                var url = skShell.urlParams.url;
                url ? $.ajax( {
                    url: url,
                    type: "GET"
                } ).done( function( data ) {

                    that.openGraph( {
                        xml: data.graphXml,
                        source: skConstants.SRC_ENT
                    } );

                } ).error( function( err ) {} ) : skShell.setStatus( "Need process ID, mate." );

            } else that.openGraph( {
                xml: "",
                source: skConstants.SOURCE_NEW
            } );

        }, skGraphManager.prototype.openFromFilePath = function( filePath ) {

            var graph = this.getCurrentGraph();
            if ( null !== graph.getDefaultParent().children && graph.getDefaultParent().children.length ) window.requireNode( "electron" ).ipcRenderer.send( "newskorewindow", filePath );
            else {

                var fsj = window.requireNode( "fs-jetpack" ),
                    data = fsj.read( filePath );
                this.openGraph( {
                    xml: data,
                    source: skConstants.SOURCE_FILE,
                    filePath: filePath
                } );

            }

        }, skGraphManager.prototype.container = null, skGraphManager.prototype.getGraphFromID = function( graphID ) {

            if ( graphID instanceof skGraph ) return graphID;
            if ( "string" == typeof graphID ) {

                if ( void 0 !== this.graphs[ graphID ] ) return this.graphs[ graphID ];

            } else if ( null == graphID ) return this.getCurrentGraph();
            return null;

        }, skGraphManager.prototype.getTitle = function( graphID ) {

            var graph = this.getGraphFromID( graphID );
            return graph.getTitle();

        }, skGraphManager.prototype.getSelectionCells = function( graphID ) {

            var graph = this.getGraphFromID( graphID );
            return graph ? graph.getSelectionCells() : void 0;

        }, skGraphManager.prototype.navigateTo = function( cellCode ) {

            if ( -1 == cellCode.indexOf( "." ) ) this.getCurrentGraph().navigateToCellHandler( cellCode );
            else {

                cellCode = cellCode.split( "." );
                var graph = this.getGraphFromID( cellCode[ 0 ] );
                this.isGraphOpened( graph ) ? ( this.setGraphVisible( graph ), graph.navigateToCellHandler( cellCode[ 1 ] ) ) : this.fireEvent( new mxEventObject( "requestGraphOpen", "graphID", cellCode[ 0 ], "cellCode", cellCode[ 1 ] ) );

            }

        };
    var createDragElement = function( type ) {

        var dragElement = $( "<div class='icon_dragged icon_" + type + "_dragged'><div class='icon_dragged_inner'>Drop me on the canvas</div></div>" );
        return dragElement[ 0 ];

    };
    return skGraphManager.prototype.makeDropFunction = function( type, valueTextToReuse ) {

        return mxUtils.bind( this, function( graph, evt, target, x, y ) {

            var value, style = graph.getStylesheet().styles[ type ];
            "object" == typeof valueTextToReuse ? value = valueTextToReuse : "string" == typeof valueTextToReuse ? ( value = skUtils.createBoxElement( type ),
                $( "box", value ).append( $( "<text value='" + valueTextToReuse + "'></text>" ) ), $( "text", value ).attr( "value", valueTextToReuse ) ) : value = skUtils.createBoxElement( type );
            var w = isNaN( parseInt( style.width, 10 ) ) ? skConstants.WB_WIDTH : parseInt( style.width, 10 ),
                h = isNaN( parseInt( style.height, 10 ) ) ? skConstants.WB_HEIGHT : parseInt( style.height, 10 ),
                box = new mxCell( value, new mxGeometry( 0, 0, w, h ), type );
            box.vertex = !0;
            var boxes = graph.importCells( [ box ], x, y, target );
            null !== boxes && boxes.length > 0 && ( graph.scrollCellToVisible( boxes[ 0 ] ), graph.setSelectionCells( boxes ),
                this.fireEvent( new mxEventObject( "boxDropped", "graph", graph, "cells", boxes ) ),
                valueTextToReuse || ( graph.cellEditor.startEditing( boxes[ 0 ] ), setTimeout( function() {

                    $( $( "textarea, input[type=text]", graph.cellEditor.wrapperDiv )[ 0 ] ).select();

                }, 100 ) ) );

        } );

    }, skGraphManager.prototype.makeCardshoeBoxDraggable = function( element, type, valueTextToReuse ) {

        return mxUtils.makeDraggable( element, mxUtils.bind( this, this.getCurrentGraph ), this.makeDropFunction( type, valueTextToReuse ), createDragElement( type ), null, null, !0, !0 );

    }, skGraphManager.prototype.getIOWhyboxes = function( cell ) {

        var result = {
            inputs: [],
            outputs: []
        };
        return cell.edges && cell.edges.forEach( function( edge ) {

            edge.target.isWhybox() && edge.source === cell ? result.outputs.push( edge.target ) : edge.source.isWhybox() && edge.target === cell && result.inputs.push( edge.source );

        } ), result;

    }, skGraphManager.prototype.getPageSettings = function( graphID ) {

        var graph = this.getGraphFromID( graphID );
        return skGraphUtils.getPageSettings( graph );

    }, skGraphManager.prototype.setPageSettings = function( graphID, pageSettings ) {

        var hasScrollbars, tx, ty, graph = this.getGraphFromID( graphID ),
            currentPageSettings = skGraphUtils.getPageSettings( graph ),
            newPageSettings = $.extend( {}, currentPageSettings, pageSettings );
        if ( currentPageSettings.pageView !== pageSettings.pageView && ( hasScrollbars = mxUtils.hasScrollbars( graph.container ),
                tx = 0, ty = 0, hasScrollbars && ( tx = graph.view.translate.x * graph.view.scale - graph.container.scrollLeft,
                    ty = graph.view.translate.y * graph.view.scale - graph.container.scrollTop ) ), graph.pageVisible = newPageSettings.pageView,
            graph.pageBreaksVisible = newPageSettings.pageView, graph.preferPageSize = newPageSettings.pageView,
            graph.setGridEnabled( newPageSettings.grid ), graph.pageScale = newPageSettings.pageScale,
            graph.background = newPageSettings.backgroundColor, graph.pageFormat = skGraphUtils.getPageFormat( newPageSettings.paperSize, newPageSettings.landscape ),
            graph.refresh(), graph.updateGraphComponents(), graph.view.validateBackground(),
            graph.sizeDidChange(), currentPageSettings.pageView !== pageSettings.pageView ) {

            if ( hasScrollbars ) {

                var cells = graph.getSelectionCells();
                graph.clearSelection(), graph.setSelectionCells( cells );

            }
            hasScrollbars && graph.container && ( graph.container.scrollLeft = graph.view.translate.x * graph.view.scale - tx,
                graph.container.scrollTop = graph.view.translate.y * graph.view.scale - ty );

        }
        return this.fireEvent( new mxEventObject( "graphBackgroundChanged", "graph", this ) ),
            graph;

    }, skGraphManager.prototype.getStyleForCell = function( graphID, cellType, styleSheetName, cells ) {

        var graph = this.getGraphFromID( graphID );
        return cellType = cellType || "whatbox", styleSheetName = styleSheetName || "default",
            graph ? graph.getStylesheet().buildCustomStyle( cellType, styleSheetName, cells ) : null;

    }, skGraphManager.prototype.setStyleSheet = function( graphID, styleObject, styleSheetName, cells ) {

        var graph = this.getGraphFromID( graphID );
        graph && graph.getStylesheet().saveStyleSheet( styleObject, null, cells );

    }, skGraphManager.prototype.getXmlForGraph = function( graphID, pretty ) {

        var graph = this.getGraphFromID( graphID ),
            xml = graph.getGraphXml();
        return pretty ? mxUtils.getPrettyXml( xml ) : xml;

    }, skGraphManager.prototype.triggerGraphAction = function( graphID, actionName ) {

        var graph = this.getGraphFromID();
        if ( graph ) {

            var act = this.actions.get( actionName );
            null !== act && null !== act.funct && act.funct();

        }

    }, skGraphManager.prototype.renderAttachments = function( graph, cell, type ) {

        return skAttachments.renderAttachments( cell, type );

    }, skGraphManager.prototype.getAttachmentTypes = function() {

        return skAttachments.getAttachmentTypes();

    }, skGraphManager.prototype.createNewAttachment = function( graphID, cell, attachmentType ) {

        var graph = this.getGraphFromID( graphID );
        return skAttachments.createNewAttachment( graph, cell, attachmentType );

    }, skGraphManager.prototype.searchEngine = null, skGraphManager;

} );
/* jshint ignore:end */