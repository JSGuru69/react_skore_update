define( function( require ) {

    "use strict";

    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var marked = require( "marked" );
    var $ = require( "jquery" );

    function isDisplayRoot( model, cell ) {

        return !cell.isGroup() || model.isLayer( cell );

    }

    /*
     * Return relationship between cellA and cellB
     *
     * possibilities:
     *
     * VISIBLE : B is direct child of A and visible when focus on A
     * CHILD : B is a direct child of A but not visible (same branch)
     * GRANDCHILD : B is a of a child child of A but not visible (same branch)
     * NONE : A and B are not in the same branch
     *
     * PARENT : B is parent of A (same branch)
     * GRANDPARENT : B is on the same level as a direct parent of A
     *          They have grandparent in comment
     *
     *********************/

    // Todo: this function is probably a good candidate to extend mxGraphModel

    // Given cellA is a current display root, find how cellB relates to cellA
    function findRelationship( model, cellA, cellB ) {

        var numOfValidRootsBetweenCells;
        var tmp;

        var root = model.getRoot();

        if ( cellA == cellB ) {

            return skConstants.REL_EQUAL;

        }

        if ( isDisplayRoot( model, cellA ) ) {

            // Move up from cellB until cellA is found
            numOfValidRootsBetweenCells = 0;
            tmp = cellB.parent;

            while ( tmp != null && tmp != cellA ) {

                if ( isDisplayRoot( model, tmp ) ) {

                    numOfValidRootsBetweenCells += 1;

                }
                tmp = tmp.parent;

            }

            if ( tmp == cellA ) {

                if ( root == cellA ) {

                    numOfValidRootsBetweenCells -= 1;

                }

                if ( numOfValidRootsBetweenCells <= 0 ) {

                    return skConstants.REL_VISIBLE;

                }

                if ( numOfValidRootsBetweenCells === 1 ) {

                    return skConstants.REL_CHILD;

                }

                return skConstants.REL_GRD_CHILD;

            }

        }

        if ( isDisplayRoot( model, cellB ) ) {

            // Move up from cellA until cellB is found
            numOfValidRootsBetweenCells = 0;
            tmp = cellA.parent;

            while ( tmp != null && tmp != cellB ) {

                if ( isDisplayRoot( model, tmp ) ) {

                    numOfValidRootsBetweenCells += 1;

                }
                tmp = tmp.parent;

            }

            if ( tmp == cellB ) {

                if ( root == cellB ) {

                    numOfValidRootsBetweenCells -= 1;

                }

                if ( numOfValidRootsBetweenCells <= 0 ) {

                    return skConstants.REL_PARENT;

                }

                return skConstants.REL_GRD_PARENT;

            }

        }

        return skConstants.REL_NONE;

    }

    /**
     * Function: setCellsStyleName
     *
     * Replace stylename in the given cells
     *
     * Parameters:
     *
     * model - <mxGraphModel> to execute the transaction in.
     * cells - Array of <mxCells> to be updated.
     * stylename - New stylename
     */
    function setCellsStyleName( model, cells, stylename ) {

        if ( cells != null && cells.length > 0 ) {

            model.beginUpdate();

            try {

                for ( var i = 0; i < cells.length; i++ ) {

                    if ( cells[ i ] != null ) {

                        var style = model.getStyle( cells[ i ] );

                        style = mxUtils.removeAllStylenames( style );

                        if ( stylename ) {

                            // Make sure that stylename is at front
                            if ( style.length === 0 ) {

                                style = stylename;

                            } else {

                                style = stylename + ";" + style;

                            }

                        }
                        model.setStyle( cells[ i ], style );

                    }

                }

            } finally {

                model.endUpdate();

            }

        }

    }

    function zapGremlins( text ) {

        var checked = [];

        for ( var i = 0; i < text.length; i++ ) {

            var code = text.charCodeAt( i );

            // Removes all control chars except TAB, LF and CR
            if ( code >= 32 || code == 9 || code == 10 || code == 13 ) {

                checked.push( text.charAt( i ) );

            }

        }

        return checked.join( "" );

    }

    function reflowStyleCss() {

        var style = document.createElement( "style" );
        document.body.appendChild( style );
        document.body.removeChild( style );

    }

    /*
     * Remove markdown "heavy" formatting
     *
     */

    function cutTheFatNow( text ) {

        return text

            // Links
            .replace( /<a\b[^>]*>/g, "&lt;" ).replace( /<\/a>/g, "&gt;" )

            // Paragraphs
            .replace( /<p\b[^>]*>/g, "" ).replace( /<\/p>/g, " " )

            // Headings
            .replace( /<h[0-9]*\b[^>]*>/g, "" ).replace( /<\/h[0-9]*>/g, " " )

            // Icons, make sure they are "small" by removing fa-lg or fa-2x & co.
            .replace( /fa-lg|fa-[0-9]x/g, "" )

            // Lists
            .replace( /<ul\b[^>]*>/g, "" ).replace( /<\/ul>/g, " " )
            .replace( /<li\b[^>]*>/g, "" ).replace( /<\/li>/g, " " )
            .replace( /<ol\b[^>]*>/g, "" ).replace( /<\/ol>/g, " " )

            // Hr
            .replace( "<hr>", "---" )

            // line return
            .replace( /\n/g, " " );

    }

    var renderer = new marked.Renderer();

    // var oImage = renderer.image;
    renderer.image = function imageF( href, title, text ) {

        if ( text.startsWith( "icon" ) ) {

            var classText = "fa ";

            if ( href.startsWith( "l-" ) ) {

                classText += " lnrf lnr-" + href.substring( 2 );

            } else {

                classText += " faf fa-" + href;

            }

            // Add other classes at the end
            var textTmp = text.split( " " );
            var style = "";

            textTmp.forEach( function( e ) {

                if ( e === "fw" ||
                    e === "ul" ||
                    e === "li" ||
                    e === "border" ||
                    e === "pull-right" ||
                    e === "pull-left" ||
                    e === "spin" ||
                    e === "pulse" ||
                    e === "lg" ||
                    e === "2x" ||
                    e === "3x" ||
                    e === "4x" ||
                    e === "5x" ||
                    e === "rotate-90" ||
                    e === "rotate-180" ||
                    e === "rotate-270" ||
                    e === "flip-horizontal" ||
                    e === "flip-vertical"
                    ) {

                        classText += " fa-" + e;

                } else if ( e.indexOf( "#" ) >= 0 ) {

                    style = ' style="color:' + e + '"';

                } else if ( isValidHtmlColor( e.toLowerCase() ) ) {

                    style = ' style="color:' + e.toLowerCase() + '"';

                }

            } );

            return '<i class="' + classText + ' "' + style + " ></i>";

        }

        // var tmp = oImage.apply( this, arguments );

        var out = '<img src="' + href + '" alt="' + text + '"';

            if ( text.indexOf( "noresize" ) == -1 ) {

                out += ' class="img-responsive" ';

            }

            if ( title ) {

                out += ' title="' + title + '"';

            }
        out += ">";

        return out;

    };

    function renderMarkdown( text, cutTheFat ) {

        cutTheFat = cutTheFat !== undefined ? cutTheFat : false ;

        if ( text !== " " ) {

            text = marked( text, { renderer: renderer } );

            if ( cutTheFat ) {

                text = cutTheFatNow( text );

            }

        }

        return text;

    }

    /*
     Node : null
     node : []
     node : <node value="">
     node : [<node value="">]
     node : [<node value="">, <node value="">, ...]

     */

    function readValue( node, attribute ) {

        attribute = attribute || skConstants.VALUE;

        if ( node == null ) {

            return "";

        }

        if ( typeof node === "string" ) {

            return node;

        }

        // Normal item
        if ( node.tagName ) {

            return node.getAttribute( attribute ) || "";

        }

        // Array with several items
        if ( node.length > 0 ) {

            return node[ 0 ].getAttribute( attribute ) || "";

        }

        return "";

    }

    function openURL( url ) {

        if ( ELECTRON ) {

            window.requireNode( "electron" ).shell.openExternal( url );

        } else {

            window.open( url );

        }

    }

    function unHtmlEntities( s, newline ) {

        s = s || "";

        // S = s.replace(/&/g,'&amp;'); // 38 26
        s = s.replace( "/&amp;/g", "&" ); // 38 26
        s = s.replace( "/&quot;/g", '"' ); // 34 22
        s = s.replace( "/&#39;/g", "'" ); // 39 27
        s = s.replace( "/&lt;/g", "<" ); // 60 3C
        s = s.replace( "/&gt;/g", ">" ); // 62 3E

        if ( newline === null || newline ) {

            s = s.replace( "/&#xa;/g", "\n" );

        }

        return s;

    }

    // Extend the first Object to have the properties of the following objects (priority to the last)
    function extend() {

        // For each object in the arguments list
        for ( var i = 1; i < arguments.length; i++ ) {

            // For each key in the given object
            for ( var key in arguments[ i ] ) {

                // If the object has the key, it will replace the first object
                if ( arguments[ i ].hasOwnProperty( key ) ) {

                    arguments[ 0 ][ key ] = arguments[ i ][ key ];

                }

            }

        }

        return arguments[ 0 ];

    }

    function isValidHtmlColor( color ) {

        var div = document.createElement( "div" );
        div.style.borderColor = "";
        div.style.borderColor = color;
        if ( div.style.borderColor.length === 0 ) {

            return false;

        }
        return true;

    }

    // See https://davidwalsh.name/javascript-debounce-function

    function debounce( func, wait, immediate ) {

        var timeout;

        return function() {

            var context = this, args = arguments;

            var later = function laterF() {

                timeout = null;
                if ( !immediate ) {

                    func.apply( context, args );

                }

            };

            var callNow = immediate && !timeout;

            clearTimeout( timeout );
            timeout = setTimeout( later, wait );

            if ( callNow ) {

                func.apply( context, args );

            }

        };

    }

    function makeExpandingArea( containers ) {

        $( containers ).each( function( i, container ) {

            var area = container.querySelector( "textarea" );
            var span = container.querySelector( "span" );

            if ( area.addEventListener ) {

                area.addEventListener( "input", function() {

                    span.textContent = area.value;

                }, false );

                span.textContent = area.value;

            } else if ( area.attachEvent ) {

                // IE8 compatibility
                area.attachEvent( "onpropertychange", function() {

                    span.innerText = area.value;

                } );

                span.innerText = area.value;

            }

            // Enable extra CSS
            container.className += " active ";

            // container.querySelector( 'pre' ).appendChild(document.createElement( 'br' ));

        } );

    }

    function isEmptyString( str ) {

        if ( typeof str == "undefined" || !str || str.length === 0 || str === "" ||
            !/[^\s]/.test( str ) || /^\s*$/.test( str ) || str.replace( /\s/g, "" ) === "" ) {

            return true;

        } else {

            return false;

        }

    }

    // alternative name : findInObject
    // http://stackoverflow.com/questions/4992383/use-jquerys-find-on-json-object
    function getObjects( obj, key, val ) {

        var objects = [];
        for ( var i in obj ) {

            if ( !obj.hasOwnProperty( i ) ) {

                continue;

            }
            if ( typeof obj[ i ] == "object" ) {

                objects = objects.concat( getObjects( obj[ i ], key, val ) );

            } else if ( i == key && obj[ key ] == val ) {

                objects.push( obj );

            }

        }
        return objects;

    }

    function createBoxElement( boxType ) {

        var type = mxUtils.createXmlDocument().createElement( boxType );
        var box = mxUtils.createXmlDocument().createElement( skConstants.BOX );
        type.appendChild( box );

        if ( boxType === skConstants.whatbox ) {

            var resps = mxUtils.createXmlDocument().createElement( skConstants.RESPONSIBILITIES );
            type.appendChild( resps );

            var resp = mxUtils.createXmlDocument().createElement( skConstants.RESPONSIBILITY );
            resp.setAttribute( skConstants.ATTR_SHOWN, "1" );
            resps.appendChild( resp );

            resp.appendChild( mxUtils.createXmlDocument().createElement( skConstants.ROLE ) );

        }

        return type;

    }

    // var areas = document.querySelectorAll('.expandingArea');
    // var l = areas.length;
    // while (l--) {
    //     makeExpandingArea(areas[l]);
    // }

    return {
        createBoxElement: createBoxElement,
        debounce: debounce,
        extend: extend,
        findRelationship: findRelationship,
        getObjects: getObjects,
        isEmptyString: isEmptyString,
        isValidHtmlColor: isValidHtmlColor,
        makeExpandingArea: makeExpandingArea,
        openURL:openURL,
        readValue: readValue,
        reflowStyleCss: reflowStyleCss,
        renderMarkdown: renderMarkdown,
        setCellsStyleName: setCellsStyleName,
        unHtmlEntities: unHtmlEntities,
        zapGremlins: zapGremlins
    };

} );
