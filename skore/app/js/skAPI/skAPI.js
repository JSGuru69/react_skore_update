
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var URLEndPoint = "";
    var skShell = require( "skShell" );

    function skAPI( navigatorUI ) {

        /*jshint validthis:true*/
        this.navigatorUI = navigatorUI;

    }
    skAPI.prototype.navigatorUI = null;

    skAPI.prototype.workspace = null;

    // dummy variables
    var dateStamp;
    var userID;

    // error handling, function definition at bottom
    var handleError, handle403;

    /*
     * EXAMPLE Data Structure
     *
     *
     *********************/

    // meta data for a process (summary, no specific version)
    var processMetaData = {
        processID:          199,    // int
        slug:               "",     // string
        processName:        "",     // string
        versions: {
            last:                   12, // int, total nb of versions
            hasBeenPublished:       true, // bool, true if published version exists at all (current or previous)
            lastPublished:          10, // int, last published version
            currentlyPublishedDate: dateStamp, // date of last published version
            lastVersionDate:        dateStamp, // date of the last version saved (not necessarily the latest published)
            lastChangeBy:           userID     // user ID who made the last chanages
        },
        accessRights: {             // it's a summary of access rights
            canEdit:       "",     // string "everyone" || "justme" || "restricted"
            canView:       ""      // same
        },
        share: {
            sharedPublicly: true,   // true if shared publicly
            url:            ""      // URL string for sharing
        }
    };

    // meta data for a process (FOR A GIVEN VERSION)
    var processMetaDataVersion = {
        reUse:  "processMetaData",

        // and add...
        version:            11,         // int, current version requested
        isLastPublished:    false,      // true if THIS is the currently published version (shorthand, could be computed from metadata)
        isLast:             false,      // if if this is the current last version available (shorthand)
        versionTitle:       "",         // string: "{version number}" or "last" or "lastpublished" when appropriate (shorthand)
        changeBy:           userID,     // string: user ID
        date:               dateStamp   // date of version

    };

    // process data FOR A GIVEN VERSION
    var processDataVersion = {
        reUse: "processMetaDataVersion",

        // and add...
        xml: ""             // xml string
    };

    // returns an array (ordered) of all the versions of a given process
    var processMetaDataVersions = {

    //                        ^ with plural

        reUse: "processMetaData",

        // and add...

        processVersions: [ // array of all the versions
            {
                reUse: "processMetaDataVersion"
            },
            {
                reUse: "processMetaDataVersion" // etc.
            }
        ]
    };

    /*
     * process of the current workspace
     *
     * only processes the user can see !
     */

    var workspaceProcessList = {

        count:  10,     // int, total count of processes THE CURRENT USER CAN SEE (===processes.length)
        processes: [
            {
                reUse: "processMetaData"
            },
            {
                reUse: "processMetaData" // etc.
            }
        ]
    };

    // user object

    var user = {
        displayName: "",    // string
        userID:     139,    // int, unique userID
        email:      ""      // string, user email
        // one day we'll need picture, last login, etc.
    };

    /// end /// example data structures

    /*
     * LOGIN
     *
     * with oauth
     *
     *********************/

    var setTokenCookie = function( token, isRefreshToken ) {

        // in localStorage for now
        // later: cookies, best with expiration date and user can delete cookies...
        if ( token ) {

            window.localStorage.set( "skAppToken", token.access_token );

        }
        if ( isRefreshToken ) {

            window.localStorage.set( "skAppRefreshToken", token.refresh_token );

        }

    };

    var getTokenCookie = function( refresh ) {

        if ( refresh ) {

            window.localStorage.get( "skAppRefreshToken" );

        }

        return window.localStorage.get( "skAppToken" );

    };

    var token;

    skAPI.prototype.getToken = function() {

        return "Bearer " + token;

    };

    skAPI.prototype.setToken = function( token ) {

        token = token;

    };

    /*
     * login
     *
     */

    // helper function
    skAPI.prototype.showLoginModal = null;
    skAPI.prototype.showWorkspacePicker = null;

    skAPI.prototype.checkLogin = function( userEmail, password, workspace ) {

        var existingToken = this.getTokenCookie( false );

        var loginData;

        if ( !existingToken ) {

            loginData = {

                grant_type: "password",

                // not sure what we need for us, client_id will probably vary if user on skoreapp / dev / preprod / desktop pc / desktop mac ... ?
                client_id: "N29zyJ1lafXKDvWrllcxrb5ulOt2N6IxoGoe7Vdf",
                client_secret: "G1clyY9tfyt0kWMqGueIhHDLWa4iBEpJ0W88c",
                username: userEmail,
                password: password

            };

        } else {

            // there is a token, we will refresh it
            loginData = {
                grant_type: "refresh",
                refresh_token: this.getTokenCookie( true )
            };

        }

        var that = this;
        var myUrl = URLEndPoint + "oauth/token/";
        $.ajax( {

            url: myUrl,
            data: loginData,
            type:"POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            dataType: "html"

        } )
        .done( function( data ) {

            setTokenCookie( JSON.parse( data ) );
            that.setToken( getTokenCookie( false ) );

            if ( !workspace ) {

                that.getMyUserDetail().done( function( data ) {

                    that.showWorkspacePicker( data );

                } );

            }

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "GET", myUrl, handleError( thrownError ) );

            if ( xhr.status == 401 ) {

                // 401 == expired
                that.showLoginModal();

            }

        } );

    };

    skAPI.prototype.setWorkspaceSlugOrID = function( workspaceSlugOrID ) {

        this.workspace = workspaceSlugOrID;

    };

    skAPI.prototype.getWorkspaceSlugOrID = function() {

        return this.workspace;

    };

   /*
    * PROCESS
    * GET / PUT / POST
    *
    *********************/

    /*
     * get process xml (given version id applicable)
     *
     * param: id: the process id / slug
     * version: version ID || "last" || "lastpublished" || all
     * metaDataOnly: true if we only want the metadata
     */

    skAPI.prototype.getProcess = function( processSlugOrID, version, metaDataOnly ) {

        version = version || "last";

        /*
         * about versions...
         *
         * if user is "can edit" && version = int
         *      --> returns the version ID, if version does not exists, default to last
         *
         * if user is "can edit" && !version
         * if user is "can edit" && id == "last"
         *      --> returns last
         *
         * if user is "can edit" && id == "lastpublished"
         *      --> returns last published version (might not be the last!)
         *
         * if user is "can view" && !version
         * if user is "can view" && version == "last"
         * if user is "can view" && version == "lastpublished"
         *      --> returns LAST PUBLISHED version in all cases case
         *
         * if user === "can view" && version == int
         *      --> returns version of lastpublish if can't find version
         *
         * if user is "can edit" && version == "all"
         *      --> returns object processVersions
         *
         */

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/" + processSlugOrID + "/";

        return $.ajax( {
            type: "GET",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType:"json",
            data: {
                version: version, // see above
                metadata: ( metaDataOnly || ( version == "all" ? true : false ) )
            }
        } ).done( function( data ) {

            if ( !metaDataOnly && version !== "all" ) {

                data = processDataVersion;

            }

            if (   metaDataOnly && version !== "all" ) {

                data = processMetaDataVersion;

            }
            if (    metaDataOnly && version === "all" ) {

                data = processMetaDataVersions;

            }

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "GET", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403( false );

            }

        } );

    };

    /*
     * save the process, a bit as existing
     *
     */

    // save the current proces to the workspace
    // arg: process: the process ID (int) or slug
    // slug: optional, would change the slug
    // title: optional, would change the title
    skAPI.prototype.saveProcess = function( processSlugOrID, slug, title ) {

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/process/" + processSlugOrID + "/";

        return $.ajax( {
            type: "PUT",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            data: {
                processName: title, // does not change if not mentioned
                processSlug: slug,  // does not change if not mentioned
                xml: this.navigatorUI.getCurrentGraph().getXml()
            }
        } ).done( function( data ) {

            data = processMetaData;

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "PUT", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403( true );

            }

        } );

    }; // end save

    /*
     * CREATE a process
     *
     * UI used to create a new process FROM the graph interface
     *
     * processName: mandatory
     * slug: optional, created on server side if empty string
     * xml: optional
     * set default access rights on server (can Edit for current user)
     */

    skAPI.prototype.createProcess = function( processName, slug, xml ) {

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/processes/";

        var that = this;

        return $.ajax( {

            type:"POST",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType: "json",
            data: {
                processName: processName,
                slug: ( slug ? slug : "" )
            }
        } ).done( function( data ) {

            data = processMetaData;  // version 0 I assume

            if ( xml ) {

                that.saveProcess( data.processID, xml );

            }

        } );

    };

    /*
     * WORKSPACE
     *
     * get the list of all processes in a workspace
     *
     * UI: the user will be able to open process from the graph editor
     *
     * ONLY PROCESSES the user CAN VIEW / CAN EDIT
     */

    skAPI.prototype.getAllProcesses = function() {

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/processes/";

        return $.ajax( {
            type:"GET",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType:"json"

        } ).done( function( data ) {

            data = workspaceProcessList;

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "GET", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403();

            }

        } );

    };

    /*
     * ACCESS RIGTHS
     *
     * GET, PUT
     *
     *********************/

    /*
     *
     * check access rights for the given process (can view or can edit)
     *
     * CURRENT USER MUST "CAN EDIT" to run this
     *
     * userID: "me" (default) || a userID (int) || "all"
     * "canView" true by default, list of canView
     * "canEdit" true by default, list of canEdit
     */

    skAPI.prototype.checkAccessRights = function( processSlugOrID, userID, canView, canEdit ) {

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/" + processSlugOrID + "/rights/";

        return $.ajax( {
            type:"GET",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType:"json",
            data: {
                userID: ( userID ? userID : "me" ),
                canView: ( canView === undefined ? true : canView ),
                canEdit: ( canEdit === undefined ? true : canEdit )
            }
        } ).done( function( data ) {

            if ( userID === "me" || userID === "one userID" ) {

                data = {
                    reUse: processMetaData,

                    // and ...
                    reUse2: user,

                    // and add...
                    canEdit: false,     // int, true if can Edit
                    canView: true       // int, true if can View
                };

            } else {

                data = {

                    reUse:  "processMetaData",

                    // and... if canEdit

                    canEdit: [
                        {
                            reUse: "user"
                        },
                        {
                            reUse: "user" // etc.
                        }
                    ],

                    // and... if canView

                    canView: [
                        {
                            reUse: "user" // user (see below)
                        },
                        {
                            reUse: "user" // etc.
                        }
                    ]
                };

            }

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "GET", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403( false );

            }

        } );

    };

    /*
     * ACCESS RIGHTS
     *
     * set Rights for given user id. USER MUST BE "CAN EDIT" to do that, throw 403 otherwise
     *
     * userID: userID integer or comma separated list of userID (example: 212,322,33 )
     * param: canView: true if can view (false by default)
     * param: canEdit: true if can edit (false by default)
     * if both false, remove access rights!
     */

    skAPI.prototype.setRight = function( processID, userID, canView, canEdit ) {

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/" + processID + "/right/";

        return $.ajax( {
            type:"PUT",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType: "json",
            data: {
                userID: userID,
                canView: canView,   // int
                canEdit: canEdit    // int
            }
        } ).done( function( data ) {

            if ( userID === 112  ) {

                // just one user

                data = {
                    reUse: "see above for one user"
                };

            } else {

                // multi user (a comma-separated list was provided)

                data = {
                    reUse: "see above for multi user" // need to think what to do when errors on some userID
                };

            }

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "PUT", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403( false );

            }

        } );

    };

    /*
     * USERS
     *
     *
     *********************/

    skAPI.prototype.getUserByID = function( userID ) {

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/users/" + userID + "/";

        return $.ajax( {
            type:"GET",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType: "json"
        } ).done( function( data ) {

            data = {
                reUse: user  // reUse user structure
            };

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "GET", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403( false );

            }

        } );

    };

    skAPI.prototype.getMyUserDetail = function() {

        var myUrl = URLEndPoint + "/me/";

        return $.ajax( {
            type:"GET",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType:"json"

        } ).done( function( data ) {

            data = {
                reUse: "user",
                workspaces: [ // object with workspaces the user can access (member or owner)
                    {
                        displayName: "",       // string, workspace name
                        slug: "",       // string, workspace slug
                        isOwner: true // true if owner
                    },
                    {
                        reUse: "above" // etc.
                    }
                ]
            };

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "GET", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403( false );

            }

        } );

    };

    /*
     * IMAGES
     *
     */

    skAPI.prototype.getImages = function() {

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/images/";
        return $.ajax( {
            type:"GET",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType:"json"
        } ).done( function( data ) {

            data = {
                count: 1212,        // count of images
                images: {
                    reUse: "image"  // see image object below
                }
            };

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "GET", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403( false );

            }

        } );

    };

    skAPI.prototype.getImage = function( imageID ) {

        var myUrl = URLEndPoint + "/" + this.getWorkspaceSlugOrID() + "/images/" + imageID + "/";

        return $.ajax( {
            type:"GET",
            url: myUrl,
            headers: {
                authorization: this.getToken()
            },
            dataType:"json"
        } ).done( function( data ) {

            data = {
                imageID: 1292,          // int, ID of the image
                url: "https://...", // string, the actual URL of the image
                width: 100,         // int, width in px
                height: 100         // int, height in px
            };

        } ).error( function( xhr, ajaxOptions, thrownError ) {

            console.log( "GET", myUrl, handleError( thrownError ) );

            if ( xhr.status == "403" ) {

                handle403( false );

            }

        } );

    };

    /*
     * handle errors
     *
     */

    handle403 = function( wasPUTRequest ) {

        // check the access rights again

        // show login modal if necessary, try again after logged in again.

        if ( wasPUTRequest ) {

            // inform user that the "write" did not commit
            skShell.setStatus( "error access rights to write" );

        }

    };

    handleError = function( err, def ) {

        var msg = def;

        if ( err.data ) {

            var keys = this.getObjectKeys( err.data );
            if ( keys.length ) {

                msg = err.data[ keys[ 0 ] ][ 0 ];

            } else {

                msg = def;

            }

        } else {

            msg = def;

        }

        return msg;

    };

} );

