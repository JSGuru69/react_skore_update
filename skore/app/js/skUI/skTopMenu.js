
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    function skTopMenu() {

        /*
         * Html
         *
         */

        var myMenu = $( require( "text!./skTopMenu.html" ) );

        $( document.body ).prepend( myMenu );

        /*
         * toggles the zoom icons
         *
         */

        $( ".skMenuView" ).hover( function() {

            $( ".zoomOff" ).toggle();
            $( ".zoomOn" ).toggle();

        } );

    }

    return skTopMenu;

} );
