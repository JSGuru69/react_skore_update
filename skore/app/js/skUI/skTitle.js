define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEvent = require( "mxEvent" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skUtils = require( "skUtils" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );

    /**
     * @constructor
     */
    function skTitle( graphManager ) {

        console.log( "skTitle" );

        if ( $( ".skTitle" ).length === 0 ) {

            var tit = $( require( "text!./skTitle.html" ) );

            // Insert after the navbar
            $( document.body ).prepend( tit );

        }

        /*jshint validthis:true*/
        this.container = $( ".skTitle" );

        this.graphManager = graphManager;
        var that = this;

        graphManager.addListener( "newGraphCreated", mxUtils.bind( that, function( sender, event ) {

            this.initForGraph( event.getProperty( "graph" ) );

        } ) );

        graphManager.addListener( "navigationDone", mxUtils.bind( that, function( sender, event ) {

            this.update( event.getProperty( "graph" ) );

        } ) );

        graphManager.addListener( "graphEnabled", mxUtils.bind( that, function( sender, event ) {

            this.update( event.getProperty( "graph" ) );

        } ) );

        graphManager.addListener( "contentLoaded", mxUtils.bind( that, function( sender, event ) {

            this.update( event.getProperty( "graph" ) );

        } ) );

        graphManager.addListener( "upadteTitle", mxUtils.bind( that, function( sender, event ) {

            this.update( event.getProperty( "graph" ) );

        } ) );

    }

    /*
     * Update title
     *
     * the event will be linked only to the "first" breadcrumb
     * this is used by skTitle but only loaded here in "edit" mode
     */
    skTitle.prototype.updateTitleOnClick = function updateTitleOnClickF() {

        console.log( "ready to update title" );

        if ( this.graphManager.getCurrentGraph().model.root == this.graphManager.getCurrentGraph().getDefaultParent().parent ) {

            if ( !this.graphManager.getCurrentGraph().cellEditor.textarea ) {

                this.graphManager.getCurrentGraph().cellEditor.init();
                this.graphManager.getCurrentGraph().cellEditor.textarea.style.display = "none";

            }

            this.graphManager.getCurrentGraph().cellEditor.startEditing( this.graphManager.getCurrentGraph().getDefaultParent() );

        }

    };

    skTitle.prototype.graphManager = null;

    skTitle.prototype.initForGraph = function( graph ) {

        console.log( "skTitle, initForGraph" );

        graph.getView().addListener( mxEvent.DOWN, mxUtils.bind( this, function() {

            this.update();

        } ) );

        graph.getView().addListener( mxEvent.UP, mxUtils.bind( this, function() {

            this.update();

        } ) );

        graph.addListener( "updateTitle", mxUtils.bind( this, function() {

            this.update();

        } ) );

    };

    // Will update the title
    skTitle.prototype.update = function updateF( myGraph ) {

        myGraph = myGraph || this.graphManager.getCurrentGraph();

        console.log( "skTitle.update" );

        var beforeUpdateCount = $( "li", this.container ).length;

        // Empties the title container just for the fun
        this.container.empty();

        var allCells = skGraphUtils.getBreadcrumbs( myGraph );

        var activeCell = null;
        var i;

        // Creates the breadcrumb
        for ( i = 0; i < allCells.length; i++ ) {

            activeCell = allCells[ i ];

            var text = skUtils.renderMarkdown( activeCell.text, true );

            if ( text === "" ) {

                if ( i === 0 ) {

                    text = "Click here to name your process";

                } else {

                    text = "(unnamed)";

                }

            } else if ( text === " " ) {

                // Adds non breaking space so we could still click on it if we want to
                text = "&nbsp;";

            }

            var tmp = $( "<li><a>" + text + "</a></li>" );

            this.container.append( tmp );
            var link = $( "a", tmp );

            // If only one breadcrumb displayed
            if ( allCells.length == 1 && myGraph.isEnabled() ) {

                link.css( { "cursor":"text" } );

                $( link ).on( "click", mxUtils.bind( this, this.updateTitleOnClick ) );

            } else {

                // If group OR last item, special class+no link
                if ( i + 1 === allCells.length || activeCell.isGroup ) {

                    link.addClass( "isGroup" );

                } else {

                    this.attachNavigationAction( myGraph, activeCell.cell, link[ 0 ] );

                }

            }

        } // End for loop

        if ( allCells.length >= beforeUpdateCount + 1 ) {

            $( "li:last", this.container ).animateCss( "bounceInRight" );

        }

    };

    skTitle.prototype.attachNavigationAction = function attachNavigationActionF( myGraph, cell, span ) {

        console.log( span.innerHTML + " will navigate to... " + cell.id );

        //Noinspection JSPotentiallyInvalidUsageOfThis
        mxEvent.addGestureListeners( span, null, null, mxUtils.bind( this, function() {

            console.log( "attachNavigationAction", span, cell.id );

            myGraph.navigateTo( cell, false, skConstants.NAV_BREADCRUMB );

        } ) );

    };

    skTitle.prototype.hide = function hideF() {

        this.container.css( "display", "none" );

    };

    skTitle.prototype.show = function showF() {

        this.container.css( "display", "" );

    };

    return skTitle;

} );
