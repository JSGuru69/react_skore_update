
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    // var mxEvent = require( "mxEvent" );
    // var mxUtils = require( "mxUtils" );
    var skUtils = require( "skUtils" );
    var skConstants = require( "skConstants" );

    function skDiagramNavigator( graphManager ) {

        console.log( "skDiagramNavigator" );

        $( document.body ).prepend( $( require( "text!./skDiagramNavigator.html" ) ) );

        /*jshint validthis:true*/
        this.graphManager = graphManager;
        var that = this;

        graphManager.addListener( "graphEnabled", function() {

            that.tree = null;

        } );

        graphManager.addListener( "navigationDone", function() {

            that.tree = null;

        } ) ;

        this.initPopover( $( ".skDiagramNavigator" ) );

    }

    skDiagramNavigator.prototype.graphManager = null;
    skDiagramNavigator.prototype.treeview = null;
    skDiagramNavigator.prototype.tree = null;

    skDiagramNavigator.prototype.dismiss = function( event ) {

        console.log( "dismiss" );

        if ( $( event.target ).closest( ".popover" ).length === 0 ) {

            $( ".popover" ).popover( "hide" );

        }

    };

    skDiagramNavigator.prototype.initPopover = function( btn ) {

        var that = this;

        $( btn ).popover( {

            title: "Diagram Navigator",
            placement: "bottom",
            container: "body",
            html: true,
            trigger: "focus click",

            content: function() {

                return $( "<div id='treeview'>Loading...</div>" );

            },
            template: '<div class="popover" role="tooltip">' +
                '<h3 class="popover-title"></h3>' +
                '<div class="popover-content"></div></div>'

        } ).on( "shown.bs.popover", function() {

            console.log( "popover shown" );

            $( ".tooltip" ).tooltip( "hide" );

            var treeviewDom = $( "#treeview" ).treeview( {

                data: that.getTree( ),
                enableLinks: true,
                showBorder: false,
                onhoverColor: "black",

                showIcon: false,
                icon: " ",

                selectedIcon: " ",
                backColor: "inherit",
                selectedBackColor: "#50DC93",

                showCheckbox: true,
                uncheckedIcon: " ",
                checkedIcon: " ",

                collapseIcon: "fa fa-chevron-down",
                expandIcon: "fa fa-chevron-right",

                onNodeSelected: function( event, node ) {

                    console.log( "onNodeSelected", event, node );

                    that.graphManager.getCurrentGraph().navigateTo( node.cell );
                    that.refreshTooltip();

                },
                onNodeExpanded: function() {

                    that.refreshTooltip();

                },
                onNodeCollapsed: function() {

                    that.refreshTooltip();

                }

            } );

            that.treeview = treeviewDom.treeview( true );

            // Find node to be revealed
            var node = that.treeview.findNodes( that.graphManager.getCurrentGraph().getDefaultParent().id, "g", "cell.id" )[ 0 ];
            that.treeview.revealNode( node );
            that.treeview.selectNode( node, { silent:true } );
            that.treeview.expandNode( node, { silent:true } );

            // "check" the parents, so they'll appear in bold with css
            var parent = that.treeview.getParent( node );
            while ( parent ) {

                that.treeview.checkNode( parent );
                parent = that.treeview.getParent( parent );

            }

            console.log( "dismiss on click - event" );

            $( ".skDiagramContainer" ).one( "click", that.dismiss );

            that.refreshTooltip();

            $( ".popover-content" ).css( {
                "max-height": "" + ( parseInt( window.innerHeight, 10 ) - 130 ) + "px"
            } );

        } ).on( "hidden.bs.popover", function() {

            console.log( "hidden" );

            $( ".skDiagramContainer" ).off( "click", that.dismiss );
            $( ".tooltip" ).tooltip( "hide" );

        } );

    };

    skDiagramNavigator.prototype.refreshTooltip = function() {

        $( ".tooltip" ).tooltip( "hide" );

        var that = this;

        setTimeout( function() {

            $( ".treeview .node-treeview" ).tooltip( {
                container:"body",
                placement: "right",
                html: true,
                title: function() {

                    var cell = that.treeview.getNode( $( this ).data( "nodeid" ) ).cell;
                    var type = skConstants.BOX;
                    if ( cell.isGroup() ) {

                        type = "group";

                    }
                    return "" + skUtils.renderMarkdown( cell.getBoxText(), true ) + "<br/>(" + type + " id : " + cell.id + ")";

                }
            } );

        }, 100 );

    };

    skDiagramNavigator.prototype.getTree = function() {

        if ( !this.tree ) {

            this.tree = this.getCellsTree();

        }

        return this.tree;

    };

    skDiagramNavigator.prototype.getCellsTree = function( root ) {

        var graph = this.graphManager.getCurrentGraph();

        root = root || graph.model.root.children[ 0 ];

        console.log( "cell.id", root.id );

        var tree = [];
        var obj;

        if ( root.children || root == graph.getDefaultParent() ) {

            var text = skUtils.renderMarkdown( root.getBoxText(), true );
            if ( text === "" ) {

                text = "(unnamed)";

            }

            var selectable = true;
            if ( root.isGroup() ) {

                selectable = false;

            }
            obj = {

                text: text,
                cell: root,
                selectable: selectable

            };

            tree.push( obj );

            // Visits the children of the cell
            var childCount = graph.model.getChildCount( root );

            var childTree = [];

            for ( var i = 0; i < childCount; i++ ) {

                var child = graph.model.getChildAt( root, i );
                childTree = childTree.concat( this.getCellsTree( child ) );

            }

            if ( childTree.length ) {

                obj.nodes = childTree;

            }

        }

        return tree;

    };

    return skDiagramNavigator;

} );
