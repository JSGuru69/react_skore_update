
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEventObject = require( "mxEventObject" );
    var skShell = require( "skShell" );

    /*
     * Top menu for editor : edit menu +  map / analytics switch + cardshoe
     *
     */

    function skProcessAnalyticsSwitch( navigatorUI ) {

        var isSideMenuMini = false;

        /*
         * Switch map / analytics
         *
         */

        var switchButtons = $( "" +
            '<li class="active"><a class="skMapButton">Work processes</a></li>' +
            '<li><a class="skProcessAnalytics">Analytics</a></li>' +
            "" );

        $( ".skNavbar .navbar-nav.nav" ).first().append( switchButtons );

        $( ".skMapButton", switchButtons ).on( "click", function() {

            // If not already selected
            if ( !$( this ).parent().hasClass( "active" ) ) {

                // Button states
                $( this ).parent().addClass( "active" );
                $( ".skProcessAnalytics" ).parent().removeClass( "active" );

                // Remove the Analytics properly
                $( ".skContainerForAnalytics" ).empty().hide();

                $( ".forGraphEditor" ).show();
                $( ".skTitle" ).show();
                if ( isSideMenuMini ) {

                    $( ".canBeMini" ).addClass( "mini" ).hide();

                } else {

                    $( ".canBeMini" ).removeClass( "mini" ).show();

                }

                // $( document.body ).addClass( "bodyFullSize" );

                var graph = navigatorUI.getCurrentGraph();

                graph.cellHTMLCache = [];
                graph.view.invalidate();
                graph.view.refresh();
                graph.fireEvent( new mxEventObject( "updatetitle" ) );

                // graph.title.update();
                navigatorUI.refresh();
                graph.sizeDidChange();
                graph.fireEvent( new mxEventObject( "refreshoutline" ) );

                if ( ELECTRON ) {

                    window.requireNode( "electron" ).ipcRenderer.send( "enableGraphMenu", true );

                }

            }

        } );

        $( ".skProcessAnalytics", switchButtons ).on( "click", function() {

            // If not already select
            if ( !$( this ).parent().hasClass( "active" ) ) {

                $( this ).parent().addClass( "active" );
                $( ".skMapButton" ).parent().removeClass( "active" );

                $( ".skTitle" ).hide();

                // Hide the graph & stuffs
                $( ".forGraphEditor" ).hide();
                if ( $( ".canBeMini" ).hasClass( "mini" ) ) {

                    isSideMenuMini = true;

                } else {

                    isSideMenuMini = false;

                }

                var iframe;

                iframe = $( "<iframe name='analytics' src='analytics.html' frameborder='0' style='overflow:hidden;height:100%;width:100%'" +
                    "height='100%' width='100%' />" );

                window.setupGraphFromParent = function setupGraphFromParentF() {

                    var frame = window.frames.analytics;
                    frame.setupGraphInFrame( navigatorUI.getCurrentGraph(), skShell.allowSave, ELECTRON, window.requireNode );

                };

                var window_h = parseInt( window.innerHeight, 10 );
                var window_w = parseInt( window.innerWidth, 10 );
                var nav_h = $( ".skNavbar" ).outerHeight() || 0;

                $( ".skContainerForAnalytics" )
                    .empty().show()
                    .css( {
                        position: "fixed",
                        width: "" + window_w + "px",
                        height: "" + ( window_h - nav_h ) + "px",
                        top: "" + nav_h + "px",
                        bottom: "0px"

                    } )
                    .append( iframe );

                $( iframe ).load( function() {

                    console.log( "iframe loaded" );

                } );

                if ( ELECTRON ) {

                    window.requireNode( "electron" ).ipcRenderer.send( "enableGraphMenu", false );

                }

            }

        } );

    }

    return skProcessAnalyticsSwitch;

} );
