
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxEventObject = require( "mxEventObject" );
    var mxUtils = require( "mxUtils" );

    function skNavHistory( graphManager ) {

        console.log( "new skNavHistory" );

        /*jshint validthis:true*/
        this.graphManager = graphManager;

        graphManager.addListener( "newGraphCreated", mxUtils.bind( this, function( event, sender ) {

            var graph = sender.getProperty( "graph" );

            this.initForGraph( graph );
            this.buildUI();

        } ) );

    }

    skNavHistory.prototype.initForGraph = function( graph ) {

        graph.addListener( "navigationDone", mxUtils.bind( this, function( sender, evt ) {

            this.addHistory(
                graph,
                evt.getProperty( "destinationCell" ),
                evt.getProperty( "stayAbove" ) );

        } ) );

        this.init( graph );

    };

    // var initDone = false;

    skNavHistory.prototype.init = function initF( graph ) {

        // if ( !initDone ) {

            this.history = [ graph.getDefaultParent() ];
            this.currentPosition = 0;

            $( ".skNavBack" ).addClass( "disabled" );

            $( ".skNavForward" ).addClass( "disabled" );

            // initDone = true;

        // }

    };

    skNavHistory.prototype.buildUI = function buildUIF() {

        console.log( "skNavHistory.buildUI" );

        var that = this;

        $( ".skNavBack" ).off( "click" );
        $( ".skNavBack" ).on( "click", function() {

            console.log( "click nav button back" );
            that.graphManager.fireEvent( new mxEventObject( "navigateTo", "cell", that.history[ that.currentPosition - 1 ] ) );

        } );

        $( ".skNavForward" ).off( "click" );
        $( ".skNavForward" ).on( "click", function() {

            that.graphManager.fireEvent( new mxEventObject( "navigateTo", "cell", that.history[ that.currentPosition + 1 ] ) );

        } );

        $( '.skNavbar [data-toggle="tooltip"]' ).tooltip();

    };

    skNavHistory.prototype.history = null;
    skNavHistory.prototype.graph = null;
    skNavHistory.prototype.nextButton = null;
    skNavHistory.prototype.graphManager = null;

    skNavHistory.prototype.refreshButtons = function refreshButtonsF() {

        console.log( "refreshButtons", this.history, this.currentPosition );

        if ( this.currentPosition > 0 ) {

            $( ".skNavBack" ).removeClass( "disabled" );

        } else {

            $( ".skNavBack" ).addClass( "disabled" );

        }

        if ( this.currentPosition + 1 < this.history.length ) {

            $( ".skNavForward" ).removeClass( "disabled" );

        } else {

            $( ".skNavForward" ).addClass( "disabled" );

        }

    };

    skNavHistory.prototype.addHistory = function addHistoryF( graph, destinationCell, stayAbove ) {

        // No cell ? you are at the top, takes the default name
        if ( !destinationCell ) {

            destinationCell = graph.getDefaultParent();

        }

        if ( stayAbove ) {

            destinationCell = destinationCell.parent;

        }

        // Destination is "back"
        if ( this.history[ this.currentPosition - 1 ] === destinationCell ) {

            --this.currentPosition;

        }

        // Destination is "next"
        else if ( this.history[ this.currentPosition + 1 ] === destinationCell ) {

            ++this.currentPosition;

        }

        // "flush" the next steps and add if not the same
        else if ( this.history[ this.currentPosition ] !== destinationCell ) {

            this.history.splice( this.currentPosition + 1, this.history.length - this.currentPosition - 1, destinationCell );
            this.currentPosition = this.history.length - 1;

        }

        /*
        History : [ a ][ b ][ c ][ d ][ e ][ f ][ g ][ h ]
        length  :   0    1    2    3    4    5    6    7
        current :             *

        add("d") --> current = 3
        add("b") --> current = 1
        add("f") --> current = 3, 3F, flush the rest
        */

        this.refreshButtons();

        console.log( "addHistory", this.currentPosition, history, destinationCell );

    };

    return skNavHistory;

} );
