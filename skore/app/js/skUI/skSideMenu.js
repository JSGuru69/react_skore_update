define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skGraphEditorTutorial = require( "skGraphEditorUI/skGraphEditorTutorial" );
    var skShell = require( "skShell" );

    function skSideMenu( navigatorUI ) {

        /*jshint validthis:true*/
        this.navigatorUI = navigatorUI;
        var that = this;

        $( document.body ).append( $( require( "text!./skSideMenu.html" ) ) );

        if ( !VIEWER_MODE ) {

            var skDemoContent = require( "skGraphEditorUI/skDemoContentLoader" );

            this.demoContent = new skDemoContent( this.navigatorUI );

        }

        // Whenever we hover over a menu item that has a submenu
        $( "li.parent" ).on( "mouseover", function() {

            var $menuItem = $( this );
            var $submenuWrapper = $( "> .wrapper", $menuItem );

            // Grab the menu item's position relative to its positioned parent
            var menuItemPos = $menuItem.position();

            // Place the submenu in the correct position relevant to the menu item
            $submenuWrapper.css( {
              top: menuItemPos.top,
              left: menuItemPos.left + Math.round( $menuItem.outerWidth() * 0.85 )
            } );

        } );

        /*
         * LEARN
         *
         * common to all versions
         *********************/

        // Tutorial
        $( ".skSideMenu_tutorial" ).on( "click", function() {

            skShell.tutorial = new skGraphEditorTutorial( that.navigatorUI );

        } );

        this.navigatorUI.graphManager.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

            var enabled = event.getProperty( "enabled" );
            $( ".graphenablebtn i" ).toggleClass( "fa-unlock", !enabled );
            $( ".graphenablebtn i" ).toggleClass( "fa-lock", enabled );

        } ) );

        /*
         * Export social
         *
         */

        // if ( ELECTRON ) {

        //     this.socialExp = ( require( "skModal/skModal_socialExport" ) )( this.navigatorUI );

        // }

        /*
         * Error log is attachd to menu
         *
         */

        if ( DEV ) {

            $( ".topLevel" ).append( $( "" +
            '<li class="sideMenuTitle">DEV</li>' +
            '<li class="parent">' +
                'Log <span class="subMenuAvailable"> &gt; </span>' +
                '<div class="wrapper">' +
                    '<ul id="skLogWrapper">' +
                    "</ul>" +
                "</div>" +
            "</li>" ) );

            $( "#skLogWrapper" ).append( $( "#skLog" ) );

        }

        /*
         * version number
         *
         */

        var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" ];
        var date = "", version = "";

        if ( ELECTRON ) {

            var pack = window.requireNode( "fs-jetpack" ).read(
                window.requireNode( "electron" ).remote.app.getAppPath() + "/package.json", "json" );

            date = new Date( pack.versionDate );

            version = pack.version;

            $( ".skVersionNumber" ).html( "" +
                "v"  + version +
                " (" + date.getDate() + " " + monthNames[ date.getMonth() ] + " " + date.getFullYear() + ")" );

        } else {

            $.ajax( "package.json", {

                success: function( data ) {

                    date = new Date( data.versionDate );
                    version = data.version;
                    $( ".skVersionNumber" ).html( "" +
                        "v"  + version +
                        " (" + date.getDate() + " " + monthNames[ date.getMonth() ] + " " + date.getFullYear() + ")" );

                }

            } );

        }

        /*
         * MiniMaxi
         *
         */

        $( ".skMiniMaxiMenu" ).on( "click", function() {

            // Icons
            $( "i", this ).toggleClass( "fa-angle-double-up" );
            $( "i", this ).toggleClass( "fa-angle-double-down" );

            $( ".canBeMini" ).toggleClass( "mini" ).toggle();

            that.navigatorUI.refreshNow();

        } );

        $( ".skSideMenu_license .sideMenuText" ).html( "Read license (Skore app Web demo)" );

    } // end constr

    return skSideMenu;

} );
