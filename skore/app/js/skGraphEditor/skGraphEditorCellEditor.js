
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCellEditor = require( "mxCellEditor" );
    var mxEventObject = require( "mxEventObject" );
    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var skShell = require( "skShell" );
    var skFormLoader = require( "skGraph/skFormLoader" );

    mxCellEditor.prototype.autoSize = false;

    // Note that some fields might have their "own" save mecanism

    mxCellEditor.prototype.positionEditor = function positionEditorF( cell ) {

        if ( this.wrapperDiv ) {

            this.wrapperDiv.style.left = ( this.graph.view.getState( cell ).x - 1 ) + "px";
            this.wrapperDiv.style.position = "absolute";
            this.wrapperDiv.style.top = ( this.graph.view.getState( cell ).y ) + "px";

            // editor min width
            if ( cell.value && cell.value.tagName && cell.value.tagName !== "title" ) {

                var wsys = this.graph.getStylesheet().styles[ "sys-" + cell.value.tagName ].width || skConstants.WB_WIDTH + 20;

                var w = Math.max( cell.geometry.width + 2, wsys );

                console.log( "edit width", w );

                this.wrapperDiv.style.minWidth =  w + "px";

            }

        }

    };

    var mxCellEditorStartEditing = mxCellEditor.prototype.startEditing;
    mxCellEditor.prototype.startEditing = function startEditingF( cell, trigger ) {

        if ( cell.isImage() ) {

            skShell.setStatus( "image cannot be changed. Please import a new one." );
            return;

        }

        // console.log("startEditing");

        // Launches the default editor
        mxCellEditorStartEditing.apply( this, arguments );

        $( ".skBoxActionIcon" ).hide();

        // Prevents the zoom menu to interfere

        // Saves all inputs created to create the key down handlers
        // this.currentInputs = [];
        var that = this;

        // Creates our own text editor:
        if ( this.graph.isHtmlLabel( cell ) ) {

            this.myForm = new skFormLoader( this.graph, cell, null, false, false );

            // Hides the "normal" textarea... it's still there on the screen but not visible
            this.textarea.style.display = "none";

            // Creates editor (common stuff for all type of boxes)
            this.wrapperDiv = document.createElement( "div" );
            this.wrapperDiv.className = "cellEditor";

            // Matches the zoom of the graph if graph bigger
            if ( this.graph.view.scale > 1 ) {

                this.wrapperDiv.style[ "-webkit-transform-origin" ] = "top left";
                this.wrapperDiv.style[ "-webkit-transform" ] = "scaleX(" + this.graph.view.scale + ") scaleY(" + this.graph.view.scale + ")";

            }

            // Duplicate the value
            // cell.newValue = cell.value.cloneNode( true );

            if ( this.graph.getDefaultParent() !== cell ) {

                this.positionEditor( cell );
                $( this.graph.container ).append( this.wrapperDiv );

            } else {

                $( ".skTitle" ).children().hide();
                $( ".skTitle" ).append( this.wrapperDiv );

            }

            $( this.wrapperDiv ).append( this.myForm.getFullForm() );

            var selectMe = function( area ) {

                if ( area ) {

                    window.setTimeout( function() {

                        area.focus();
                        area.select();

                    }, 10 );

                }

            };

            // select the input on which the user has clicked via the fieldname
            if ( trigger ) {

                var targetCounter = $( trigger.toElement ).data( "skcounter" );
                if ( !targetCounter ) {

                    targetCounter  = $( $( trigger.toElement ).parents( "[data-skcounter]" ) ).data( "skcounter" );

                }
                if ( targetCounter ) {

                    selectMe( $( "[data-skcounter=" + targetCounter + "] textarea, " +
                                 "[data-skcounter=" + targetCounter + "] input",
                                 this.wrapperDiv )[ 0 ] );

                } else {

                    selectMe( $( "textarea, input[type=text]", this.wrapperDiv ).first() );

                }

            } else {

                selectMe( $( "textarea, input[type=text]", this.wrapperDiv ).first() );

            }

            // on tab / enter on the last field
            $( "textarea, input[type=text]", this.wrapperDiv ).last().on( "keydown", function( event ) {

                var keyCode = event.keyCode || event.which;

                // enter = 13
                // tab = 9;
                if ( keyCode == 13 || keyCode == 9 ) {

                    event.preventDefault();
                    that.graph.cellEditor.stopEditing( !that.graph.isInvokesStopCellEditing() );

                }

                // Tab
                if ( skShell.tutorial && keyCode == 9  ) {

                    skShell.tutorial.stepDone( skShell.tutorial.BOX_TAB_PRESSED, {
                        "cell": cell,
                        "field": $( this )[ 0 ]
                    } );

                }

            } );

            // if not stikcynote
            if ( !cell.isStickynote() ) {

                $( "textarea", this.wrapperDiv ).on( "keydown", function( event ) {

                    var keyCode = event.keyCode || event.which;

                    // enter = 13
                    // tab = 9;
                    if ( keyCode == 13 && !event.shiftKey ) {

                        event.preventDefault();
                        that.graph.cellEditor.stopEditing( !that.graph.isInvokesStopCellEditing() );

                    }

                } );

            }

            if ( skShell.tutorial ) {

                // FIXME_FIXED: BOX_ENTERED_EDIT_MODE is not defined
                skShell.tutorial.stepDone( skShell.tutorial.BOX_ENTERED_EDIT_MODE, {
                    "cell": cell,
                    "firstInput": $( "textarea, input[type=text]", this.wrapperDiv ).first()[ 0 ]
                } );

            }

        }

    };

    /*
     * Helper function
     *
     */

    // What happens when we stop editing
    // var mxCellEditorStopEditing = mxCellEditor.prototype.stopEditing;
    mxCellEditor.prototype.stopEditing = function stopEditingF( cancel ) {

        cancel = cancel || false;

        if ( this.editingCell !== null ) {

            if ( this.textNode !== null ) {

                this.textNode.style.visibility = "visible";
                this.textNode = null;

            }

            if ( !cancel /* && this.isModified() */ ) {

                this.graph.model.beginUpdate();

                try {

                    // Resets the cache
                    delete this.graph.cellHTMLCache[ this.editingCell.id ];

                    // replace the "box" thing
                    // var v = this.editingCell.value.cloneNode( true );
                    var v = this.editingCell.value; //.cloneNode( true );
                    // if ( v.querySelector( "box" ) ) {

                        // v.querySelector( "box" ).remove();

                    // }
                    // v.appendChild( this.myForm.save() );
                    this.myForm.save( false );

                    this.graph.labelChanged( this.editingCell, v, this.trigger );

                    this.graph.cellRenderer.redraw( this.graph.view.getState( this.editingCell ), true, true );

                    // Todo : could be not all the time...
                    if ( this.editingCell.value && this.editingCell.value.tagName == skConstants.title ) {

                        this.graph.fireEvent( new mxEventObject( "updateTitle", "graph", this.graph ) );

                        if ( ELECTRON ) {

                            if ( skShell.currentFilePath == void 0 ) {

                                var bw = window.requireNode( "electron" ).remote.getCurrentWindow();

                                bw.setTitle( this.graph.getTitle() );

                            }

                        }

                    }

                } finally {

                    this.graph.model.endUpdate();

                }

            }

            // Has been canceled
            else {

                console.log( "editing canceled" );

            }

            $( ".skTitle" ).children().show();
            this.trigger = null;
            this.textarea.blur();
            $( this.textarea ).remove();

            // this.textarea = null;

        }

        // Removes the editor
        if ( this.wrapperDiv != null ) {

            if ( this.wrapperDiv.parentNode ) {

                $( this.wrapperDiv ).remove();

            }
            this.wrapperDiv = null;

        }

        $( ".skBoxActionIcon" ).show();

        if ( skShell.tutorial ) {

            skShell.tutorial.stepDone( skShell.tutorial.STOP_EDITING, {
                "cell": this.editingCell

            } );

        }

        this.editingCell = null;

        // window.setTimeout( function() {

        //     document.body.click();

        // }, 10 );

        window.setTimeout( mxUtils.bind( this, function() {

            this.graph.container.focus();

        } ), 10 );

    };

    var mxCellEditorFocusLost = mxCellEditor.prototype.focusLost;

    mxCellEditor.prototype.focusLost = function focusLostF() {

        if ( !this.graph.isHtmlLabel( this.getEditingCell() ) ) {

            mxCellEditorFocusLost.apply( this, arguments );

        }

    };

} );
