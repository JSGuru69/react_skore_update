define( [ "Injector" ], function( Injector ) {

    "use strict";

    describe( "skRolesManager", function() {

        var injector = new Injector();

        beforeEach( injector.reset );

        var mxUtils, skShell, skRolesManager;

        beforeEach( injector.run(
            function( _mxUtils /*mxUtils*/,
                      _skShell /*skShell*/,
                      _skRolesManager /*skGraphEditor/skRolesManager*/) {

                mxUtils = _mxUtils;
                skShell = _skShell;
                skRolesManager = _skRolesManager;

            }
        ) );

        beforeEach( function() {

            // Mock dependency
            skShell.graph = { model: { root: {} } };
            skShell.graph.model.filterDescendants = jasmine.createSpy( "filterDescendants" );

        } );

        describe( "constructor", function() {

            it( "should saves the roles in the model when created", function() {

                expect( skShell.graph.model.root.roles ).toBeUndefined();

                //jshint -W031
                new skRolesManager();
                expect( skShell.graph.model.filterDescendants ).toHaveBeenCalled();

                expect( skShell.graph.model.root.roles ).toBeDefined();
                expect( mxUtils.getXml( skShell.graph.model.root.roles ) ).toBe( "<roles/>" );

            } );

        } );

        describe( "skRolesManager.prototype.exists", function() {

        } );

        describe( "skRolesManager.prototype.add", function() {

        } );

        describe( "skRolesManager.prototype.addAsset", function() {

        } );

        describe( "skRolesManager.prototype.refreshModel", function() {

        } );

        describe( "skRolesManager.prototype.addRole", function() {

            it( "should append a trimmed role if it does not exists", function() {

                var rolesXml = "<roles/>";

                skShell.graph.model.root.roles = mxUtils.parseXml( rolesXml ).documentElement;
                var manager = new skRolesManager();

                manager.addRole( " lorem" );
                expect( mxUtils.getXml( skShell.graph.model.root.roles ) ).toBe( '<roles><role value="lorem"/></roles>' );

            } );

            it( "should not append a role if a trimmed version exists", function() {

                var rolesXml = '<roles><role value="lorem"/></roles>';

                skShell.graph.model.root.roles = mxUtils.parseXml( rolesXml ).documentElement;
                var manager = new skRolesManager();

                manager.addRole( " lorem" );
                expect( mxUtils.getXml( skShell.graph.model.root.roles ) ).toBe( '<roles><role value="lorem"/></roles>' );

            } );

        } );

    } );

} );
