define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxCellMarker = require( "mxCellMarker" );
    var mxCellState = require( "mxCellState" );
    var mxClient = require( "mxClient" );
    var mxConnectionConstraint = require( "mxConnectionConstraint" );
    var mxConnectionHandler = require( "mxConnectionHandler" );
    var mxConstants = require( "mxConstants" );
    var mxConstraintHandler = require( "mxConstraintHandler" );
    var mxEdgeHandler = require( "mxEdgeHandler" );
    var mxEvent = require( "mxEvent" );
    var mxEventObject = require( "mxEventObject" );
    var mxGraph = require( "mxGraph" );

    // var mxImage = require( "mxImage" );
    var mxMultiplicity = require( "mxMultiplicity" );
    var mxPoint = require( "mxPoint" );
    var mxRubberband = require( "mxRubberband" );
    var mxShape = require( "mxShape" );
    var mxStencilRegistry = require( "mxStencilRegistry" );
    var mxUndoManager = require( "mxUndoManager" );
    var mxUtils = require( "mxUtils" );
    var mxVertexHandler = require( "mxVertexHandler" );
    var skConstants = require( "skConstants" );
    var skGraph = require( "skGraph/skGraph" );
    var skShell = require( "skShell" );
    var skUtils = require( "skUtils" );

    skGraph.prototype.isCellEditable = function isCellEditableF( cell ) {

        return ( this.isEnabled() && !cell.isEdge() );

    };

    /*
     * Editor : move the cells
     *
     * overwrites the move cells function so it takes the Ybox with it
     * only when only one whatbox is selected
     *
     * only if there is ONE whybox
     */

    skGraph.prototype.isCellMovable = function isCellMovableF() {

        return this.isEnabled();

    };

    var mxGraphMoveCells = mxGraph.prototype.moveCells;

    skGraph.prototype.moveCells = function moveCellsF( cells ) {

        // If one box is selected and it's a whatbox
        if ( cells.length == 1 && ( cells[ 0 ].isWhatbox() ) ) {

            // var potentialYbox = [];
            var myCell = cells[ 0 ];

            // moves along all the "single" whybox

            // all the edges of the whatbox
            if ( myCell.edges ) {

                for ( var i = 0 ; i < myCell.edges.length; i++ ) {

                    // check if the cell is "single" (has only this cell as input)
                    var edge = myCell.edges[ i ];

                    if ( edge.target == myCell && edge.source.edges.length == 1 ) {

                        cells.push( edge.source );

                    } else if ( edge.source == myCell && edge.target.edges.length == 1 ) {

                        cells.push( edge.target );

                    }

                }

                // cells = cells.concat( potentialYbox );
                // if ( potentialYbox.length == 1 ) {

                //     cells.push( potentialYbox[ 0 ] );

                // }

            }

        }

        // Applies default function
        return mxGraphMoveCells.apply( this, arguments );

    };

    skGraph.prototype.was = {};
    skGraph.prototype.setEnabled = function( enabled ) {

        console.log( "skGraph.setEnabled", enabled );

        this.enabled = enabled;

        if ( this.isPrintPreview !== true ) {

            if ( this.enabled ) {

                this.pageVisible = this.was.pageVisible || true;
                this.pageBreaksVisible = this.was.pageBreaksVisible || true;

                /*
                 * Set the initial title
                 *
                 */

                // todo : must be the first child if there is only one child. think about comment layer
                if ( !this.getDefaultParent().value ) {

                    var value = skUtils.createBoxElement( skConstants.title ); //mxUtils.createXmlDocument().createElement( skConstants.title );
                    var text = mxUtils.createXmlDocument().createElement( skConstants.TEXT );
                    value.querySelector( skConstants.BOX ).appendChild( text );
                    this.getDefaultParent().value = value;

                    // this.getDefaultParent().setBoxText( skShell.defaultTexts.title );

                }

                if ( !this.undoManager ) {

                    this.createUndoManager();

                } else {

                    this.undoManager.clear();

                }

            } else {

                this.was.pageVisible = this.pageVisible;
                this.pageVisible =  false;
                this.was.pageBreaksVisible = this.pageBreaksVisible;
                this.pageBreaksVisible = false;

            }

            /*
             * RUBBERBAND for selection
             *
             *
             *********************/

            if ( enabled ) {

                var rubberband = new mxRubberband( this );

                this.getRubberband = function getRubberbandF() {

                    return rubberband;

                };

            } else {

                this.getRubberband = null;

            }

            this.updateGraphComponents();
            this.refresh();
            this.resetScrollbars();

            if ( this.isOutline !== true ) {

                this.fireEvent( new mxEventObject( "graphEnabled", "enabled", enabled, "graph", this ) );

            }

        }

    };

    skGraph.prototype.initGraphEditor = function initGraphEditorF( ) {

        /*
         * SELECTION Change
         *
         * what happens when the selection (by rubberband or ctrl click) changes
         */

        // Remove JSPotentiallyInvalidUsageOfThis after refactoring
        this.getSelectionModel().addListener( mxEvent.CHANGE, mxUtils.bind( this, function( sender, evt ) {

            var graph = this;

            // console.log( "graph.model.mxEvent.CHANGE : ", "added : ",  evt.getProperty( "added" ), "removed : ",  evt.getProperty( "removed" ) );

            $( ".box" ).removeClass( "editable" );

            // Remove highlight when select
            graph.cellHighlighter.toggleHighlightOff();

            var cellsRemovedFromSelection = evt.getProperty( "added" ); // yeah, known bug...
            if ( cellsRemovedFromSelection && cellsRemovedFromSelection.length ) {

                cellsRemovedFromSelection.forEach( function( cell ) {

                    graph.cellRenderer.destroyIconsIfNecessary( graph.view.getState( cell ) );

                } );

            }

            var cellsAdded = evt.getProperty( "removed" ); // yeah, known bug...
            if ( cellsAdded && cellsAdded.length  ) {

                for ( var i = 0; i < cellsAdded.length; i++ ) {

                    graph.cellRenderer.destroyIconsIfNecessary( graph.view.getState( cellsAdded[ i ] ) );

                }

            } else {

                cellsAdded = [];

            }

            // Remove the boxIcons from the cell that were removed
            // var cells = evt.getProperty( "added" ); // Don't know why it's the property "added" !
            // var cells = this.cells;

            if ( cellsAdded ) {

                /*
                 * No box selected && sk_touch
                 *
                 */

                if ( cellsAdded.length === 0 ) {

                    // $( ".skFooter_IDs" ).hide();
                    $( ".skSideMenu [data-loadtype=stylecell]" ).addClass( "disabled" );
                    $( ".box-pluralise" ).html( "" );

                }

                /*
                 * Only one box selected
                 *
                 */

                if ( cellsAdded.length == 1 ) {

                    // $( ".skFooter_IDs" ).show().text( cellsAdded[ 0 ].id );
                    $( ".skSideMenu [data-loadtype=stylecell]" ).removeClass( "disabled" );
                    $( ".box-pluralise" ).html( "" );

                    /*
                     * Box icons
                     *
                     */

                    graph.view.createState( cellsAdded[ 0 ], true );
                    graph.cellRenderer.redraw( graph.view.getState( cellsAdded[ 0 ] ), true );

                    /*
                     * make box editable
                     *
                     */

                    $( "#box" + cellsAdded[ 0 ].id ).addClass( "editable" );

                    if ( $( "#skModal_Attachments" ).length ) {

                        graph.fireEvent( new mxEventObject( "openAttachment", "cell", cellsAdded[ 0 ] ) );

                    }

                }

                /*
                 * Editor : group button
                 *
                 * more than one box is selected
                 */
                if ( graph.isEnabled() && cellsAdded.length > 1 ) {

                    // $( ".skFooter_IDs" ).show().text( "• " + cellsAdded.length  + " •" );

                    // todo move that to action.setEnabled with multiCells
                    $( ".skSideMenu [data-loadtype=stylecell]" ).removeClass( "disabled" );
                    $( ".box-pluralise" ).html( "es" );

                }

            }

        } ) ); // End selection change

        // Updates modified state if graph changes
        this.getModel().addListener( mxEvent.CHANGE, mxUtils.bind( this, function() {

            //Noinspection JSPotentiallyInvalidUsageOfThis
            this.setDocumentEdited( this.isEnabled() && true );

        } ) );

        this.addListener( "graphBackgroundChanged", mxUtils.bind( this, function() {

            this.getView().validateBackground();

        } ) );

        /*
         * Set the constraints
         *
         */

        this.connectionHandler.addListener( mxEvent.CONNECT, mxUtils.bind( this, function( sender, evt ) {

            var graph = this;

            var edge = evt.getProperty( "cell" );
            var source = graph.getModel().getTerminal( edge, true );
            var target = graph.getModel().getTerminal( edge, false );
            var target2, source2;

            // Check if the target cell already exists
            // if not, apply default constraint
            if ( !graph.view.getState( target ) ) {

                // top
                if ( sender.sourceConstraint.point.y === 0 ) {

                    // first need to invert the arrow
                    target2 = edge.target;
                    source2 = edge.source;
                    edge.target = source2;
                    edge.source = target2;

                    // source box
                    graph.setConnectionConstraint(
                        edge,
                        source,
                        false,
                        new mxConnectionConstraint(
                            new mxPoint(
                                sender.sourceConstraint.point.x,
                                0 ) ) );

                    // destination (new) box
                    graph.setConnectionConstraint(
                        edge,
                        target,
                        true,
                        new mxConnectionConstraint(
                            new mxPoint(
                                0.5, //sender.sourceConstraint.point.x,
                                1 ) ) );

                }

                // right
                else if ( sender.sourceConstraint.point.x === 1 && sender.sourceConstraint.point.y === 0.5 ) {

                    graph.setConnectionConstraint( edge, source, true, sender.sourceConstraint );
                    graph.setConnectionConstraint( edge, target, false, new mxConnectionConstraint( new mxPoint( 0, 0.5 ) ) );

                }

                // bottom
                else if ( sender.sourceConstraint.point.y === 1 ) {

                    // source (existing)
                    // graph.setConnectionConstraint( edge, source, true, sender.sourceConstraint );
                    graph.setConnectionConstraint(
                        edge,
                        source,
                        true,
                        new mxConnectionConstraint(
                            new mxPoint(
                                sender.sourceConstraint.point.x,
                                sender.sourceConstraint.point.y
                                ) )
                         );

                    // new box
                    graph.setConnectionConstraint(
                        edge,
                        target,
                        false,
                        new mxConnectionConstraint(
                            new mxPoint(
                                0.5,
                                0 ) ) );

                }

                // left
                else if ( sender.sourceConstraint.point.x === 0 && sender.sourceConstraint.point.y === 0.5 ) {

                    // first need to invert the arrow
                    target2 = edge.target;
                    source2 = edge.source;
                    edge.target = source2;
                    edge.source = target2;

                    graph.setConnectionConstraint( edge, source, true, new mxConnectionConstraint( new mxPoint( 1, 0.5 ) ) );
                    graph.setConnectionConstraint( edge, target, false, new mxConnectionConstraint( new mxPoint( 0, 0.5 ) ) );

                }

            }

            // if the user has dropped the line ON the box (not on a port)
            // must register the edge entryY and entryX in the style
            if ( edge.style.indexOf( mxConstants.STYLE_ENTRY_Y ) === -1 ) {

                edge.style = mxUtils.addStylename( edge.style, mxConstants.STYLE_ENTRY_Y + "=0.5" );
                edge.style = mxUtils.addStylename( edge.style, mxConstants.STYLE_ENTRY_X + "=0" );

            }

        } ) );

         /*
         * Set the style of the line when necessary
         *
         */

        this.addListener( mxEvent.CELL_CONNECTED, mxUtils.bind( this, function( sender, event ) {

            var graph = this;

            var edge = event.getProperty( "edge" );
            var source = event.getProperty( "source" );
            var terminal = event.getProperty( "terminal" );

            // console.log( "CELL_CONNECTED", "terminal", terminal, "edge", edge, "source", source );

            var otherEnd = edge.getTerminal( !source );
            var stylename = otherEnd && otherEnd.isStickynote() || terminal.isStickynote() ? skConstants.LINE_WY2S : "";

            skUtils.setCellsStyleName( graph.model, [ edge ], stylename );

            // Puts the lines to the back (z-index)
            graph.orderCells( true, [ event.getProperty( "edge" ) ] );

        } ) );

        /*
         * Preventing w2w and y2y connections
         *
         */

        this.addBoxConnectionConstraints = mxUtils.bind( this, function() {

            var graph = this;

            graph.removeBoxConnectionConstraints();

            // Source node does not want any incoming connections
            graph.multiplicities.push( new mxMultiplicity(
                true, // source
                skConstants.whatbox, //type
                null, // attr
                null, // attr value
                0, // min edge
                "n", // max edge
                [ skConstants.whybox, skConstants.stickynote, skConstants.group ], // opposite
                "", // count error
                "You need to insert a whybox first" //type error
                ) );

            graph.multiplicities.push( new mxMultiplicity(
                true, skConstants.whybox, null, null, 0, "n", [ skConstants.whatbox, skConstants.stickynote, skConstants.group ],
                "", "You need to insert a whatbox first" ) );

        } );

        this.removeBoxConnectionConstraints = mxUtils.bind( this, function() {

            this.multiplicities = [];

        } );

        this.addBoxConnectionConstraints();

        // Tap & hold brings up context menu.
        // Tolerance slightly below graph tolerance is better.
        this.connectionHandler.tapAndHoldTolerance = 16;

        // Score: Implements the connect preview
        this.connectionHandler.createEdgeState = mxUtils.bind( this, function createEdgeStateF() {

            var graph = this;

            var edge = graph.createEdge( null, null, null, null, null );

            return new mxCellState( graph.view, edge, graph.getCellStyle( edge ) );

        } );

        /*
         * Reset line "waypoints" by double clicking
         *
         */

        this.addListener( mxEvent.DOUBLE_CLICK, mxUtils.bind( this, function( sender, evt ) {

            var cell = evt.getProperty( "cell" );

            if ( cell && cell.isEdge() ) {

                this.clearWaypoints( [ cell ] );

            }

            evt.consume();

        } ) );

        this.addListener( "styleSheetUpdated", mxUtils.bind( this, function() {

            this.cellHTMLCache = [];
            this.updateCellsSize();
            this.refresh();

        } ) );

    }; // End skGraphEditor

    // Score: Enables orthogonal connect preview in IE
    mxConnectionHandler.prototype.movePreviewAway = false;

    /**
     * Enhance mxConstraintHandler
     */

     // orange dot image
    // mxConstraintHandler.prototype.pointImage = new mxImage( "images/connectorPoint.png", 15, 15 );

    // ignore event on control / command
    mxConstraintHandler.prototype.isEventIgnored = function( me ) {

        if ( mxEvent.isControlDown( me.evt ) || ( mxClient.IS_MAC && me.evt.metaKey ) ) {

            return true;

        }

        return false;

    };

    // ignore current dots
    var mxConstraintHandlerUpdate = mxConstraintHandler.prototype.update;
    mxConstraintHandler.prototype.update = function updateF( /* me, source */) {

        mxConstraintHandlerUpdate.apply( this, arguments );
        var focusIcons = this.focusIcons;

        if ( focusIcons != null && focusIcons.length ) {

            for ( var i = 0, l = focusIcons.length; i < l; i += 1 ) {

                focusIcons[ i ].node.style.cursor = "pointer";

                if ( this.focusPoints[ i ].DONOTDISPLAY ) {

                    focusIcons[ i ].node.style.display = "none";

                }

            }

        }

    };

    /*
     * BOX creation with drag and drop
     *
     *
     *********************/

    mxCellState.prototype.add_box_after_box_state = function add_box_after_box_stateF( dropPoint, positionForNewBox, sourceConstraint, createLine ) {

        var distance,
            width,
            height,
            value,
            box,
            style_box,

            // style_line,
            x = ( dropPoint ? dropPoint.x : null ),
            y = ( dropPoint ? dropPoint.y : null ),
            graph = this.view.graph;

        var styleWB = graph.getStylesheet().styles[ skConstants.whatbox ];
        var styleYB = graph.getStylesheet().styles[ skConstants.whybox ];

        if ( this.cell.isWhybox() ) {

            // The whatbox
            distance = positionForNewBox === "left" ? skConstants.DIST_W2Y : skConstants.DIST_Y2W;
            width = parseInt( styleWB.width, 10 );
            height = parseInt( styleWB.height, 10 );
            value = skUtils.createBoxElement( skConstants.whatbox );
            style_box = skConstants.whatbox;

        } else if ( this.cell.isWhatbox() || this.cell.isGroup() ) {

            distance = positionForNewBox === "left" ? skConstants.DIST_Y2W : skConstants.DIST_W2Y;
            width = parseInt( styleYB.width, 10 );
            height = parseInt( styleYB.height, 10 );
            value = skUtils.createBoxElement( skConstants.whybox );
            style_box = skConstants.whybox;

        } else {

            return null;

        }

        // No drop point specified, let's just put the activity where we think is best
        // if there is already an activity there, we move the activity below
        // if ( !dropPoint && positionForNewBox ) {

        //     switch (positionForNewBox){

        //         case "top":

        //             x = west + this.cell.geometry.width / 2 - width / 2;
        //             y = north - distance - height;

        //         break;
        //         case "right":

        //             y = this.origin.y;
        //             x = east + distance;

        //         break;
        //         case "left":

        //             x = west - distance - width;
        //             y = this.origin.y;

        //         break;
        //         case "bottom":

        //             x = west + this.cell.geometry.width / 2 - width / 2;
        //             y = south + distance;

        //         break;
        //     }

        //     // Initial calculation
        //     x = this.origin.x + this.cell.geometry.width + distance;

        //     y = this.avoid_superimpose();

        // }

        // // if dropPoint &&
        // else
        if ( sourceConstraint ) {

            // align the box if this is dropped in the "close" area (which is FACTOR times the area around the cell)

            var scp = sourceConstraint.point;

            if ( scp.y === 0 ) {

                positionForNewBox = "top";

            } else if ( scp.x === 1 ) {

                positionForNewBox = "right";

            } else if ( scp.y === 1 ) {

                positionForNewBox = "bottom";

            } else if ( scp.x === 0 ) {

                positionForNewBox = "left";

            }

        }

        var FACTOR = 1.5;
        var east = this.origin.x + this.cell.geometry.width;
        var farEast = east + FACTOR * this.cell.geometry.width;
        var west = this.origin.x;
        var farWest = west - FACTOR * this.cell.geometry.width;
        var north = this.origin.y;
        var farNorth = north - FACTOR * this.cell.geometry.height;
        var south = this.origin.y + this.cell.geometry.height;
        var farSouth = south + FACTOR * this.cell.geometry.height;

        switch ( positionForNewBox ) {

            case "top":
                if ( !dropPoint || ( dropPoint.x > west && dropPoint.x < east ) ) {

                    // coridor

                    x = west + this.cell.geometry.width / 2 - width / 2;

                    if ( !dropPoint || ( dropPoint.y < north && dropPoint.y > farNorth ) ) {

                        y = north - distance - height;

                    }

                }
            break;

            case "right":
                if ( !dropPoint || ( dropPoint.y > north && dropPoint.y < south ) ) {

                    // corridor
                    y = this.origin.y;

                    if ( !dropPoint ||  ( dropPoint.x > east && dropPoint.x < farEast ) ) {

                        // area
                        x = east + distance;

                    }

                }
            break;

            case "bottom":
                if ( !dropPoint || ( dropPoint.x > west && dropPoint.x < east ) ) {

                    // corridor
                    x = west + this.cell.geometry.width / 2 - width / 2;

                    if ( !dropPoint || ( dropPoint.y > south && dropPoint.y < farSouth ) ) {

                        y = south + distance;

                    }

                }
            break;

            case "left":
                if ( !dropPoint || ( dropPoint.y > north && dropPoint.y < south ) ) {

                    x = west - distance - width;

                    if ( !dropPoint || ( dropPoint.x < west && dropPoint.x > farWest ) ) {

                        y = this.origin.y;

                    }

                }
            break;
        }

            // Y (horizontal)

            // The user probably wants the box to be aligned on the same line (same y) if it's dropped on the right hand side
            // if ( dropPoint.y > this.origin.y &&
            //     ( dropPoint.y < ( this.origin.y + this.cell.geometry.height ) ) ) {

            //     y = this.origin.y;

            // } else {

            //     // Mouse position = left middle of the box:
            //     y = dropPoint.y - ( height / 2 );

            // }

        // }

        // If it's not "too far" on x the user probably wants the standard distance
        // if ( dropPoint ) {

        //     if ( dropPoint.x > ( this.origin.x + ( this.cell.geometry.width ) ) &&
        //         dropPoint.x < this.origin.x + ( this.cell.geometry.width * 2 ) ) {

        //         x = this.origin.x + this.width + distance;

        //     } else {

        //         x = dropPoint.x;

        //     }

        // }

        // In this case we place the new box before
        // if ( positionForNewBox === "left" ) {

        //     x = this.origin.x - distance - width;
        //     y = this.avoid_superimpose( true );

        // }

        // The current x and y are calculated relative to the state which doesn't take
        // into acount the parent cell and its position on the graph
        // so in a group it recalculates the x and y with offset
        if ( this.cell.parent != graph.getDefaultParent() ) {

            var p = this.cell.parent;

            x -= p.geometry.x;
            y -= p.geometry.y;

        }

        /*
         * Commits change
         *
         */
        graph.getModel().beginUpdate();

        try {

            // Insert box
            box = graph.insertVertex( this.cell.parent,
                null,
                value,
                x,
                y,
                width,
                height,
                style_box );

            // Set the box as collapsed from the start

            if ( box.style_box === skConstants.whatbox ) {

                box.collapsed = true;

            }

            var myEdge;
            value = null;
            var style_line = "line"; //graph.getStylesheet().getDefaultEdgeStyle();

            if ( createLine === true ) {

                switch ( positionForNewBox ) {

                    case "left" :
                        myEdge = graph.insertEdge( this.cell.parent, null, value, box, this.cell, style_line );
                        graph.setConnectionConstraint( myEdge, box, true, new mxConnectionConstraint( new mxPoint( 1, 0.5 ), true ) );
                        graph.setConnectionConstraint( myEdge, this.cell, false, new mxConnectionConstraint( new mxPoint( 0, 0.5 ), true ) );

                    break;

                    case "top" :
                        myEdge = graph.insertEdge( this.cell.parent, null, value, box, this.cell, style_line );
                        graph.setConnectionConstraint( myEdge, this.cell, true, new mxConnectionConstraint( new mxPoint( 0.5, 1 ), true ) );
                        graph.setConnectionConstraint( myEdge, box, false, new mxConnectionConstraint( new mxPoint( 0.5, 0 ), true ) );
                    break;

                    case "right" :
                        myEdge = graph.insertEdge( this.cell.parent, null, value, this.cell, box, style_line );
                        graph.setConnectionConstraint( myEdge, this.cell, true, new mxConnectionConstraint( new mxPoint( 1, 0.5 ), true ) );
                        graph.setConnectionConstraint( myEdge, box, false, new mxConnectionConstraint( new mxPoint( 0, 0.5 ), true ) );
                        break;

                    case "bottom" :
                        myEdge = graph.insertEdge( this.cell.parent, null, value, this.cell, box, style_line );
                        graph.setConnectionConstraint( myEdge, this.cell, true, new mxConnectionConstraint( new mxPoint( 0.5, 1 ), true ) );
                        graph.setConnectionConstraint( myEdge, box, false, new mxConnectionConstraint( new mxPoint( 0.5, 0 ), true ) );

                    break;

                }

            }

        } finally {

            // Updates the display
            graph.getModel().endUpdate();

        }

        if ( skShell.tutorial ) {

            skShell.tutorial.stepDone(
                skShell.tutorial.BOX_CREATED_WITH_DND_FROM_CONNECT_ARROW,
                { cell:box } );

            if ( box.isWhybox() ) {

                skShell.tutorial.stepDone(
                    skShell.tutorial.NEXT_Y_BOX_IS_CREATED,
                    { cell: box }
                );

            } else if ( box.isWhatbox() ) {

                skShell.tutorial.stepDone(
                    skShell.tutorial.W_BOX_IS_CREATED,
                    { cell: box }
                );

            }

        }

        // Scroll to visible / last created cell
        graph.scrollCellToVisible( box );

        return box;

    };

    mxCellState.prototype.avoid_superimpose = function avoid_superimposeF( before ) {

        var state = this;
        var view = this.view;
        var y = state.origin.y;

        // We don't want several boxes to overlap so we need to do a few checks first
        if ( state.cell.edges && state.cell.edges.length > 0 ) {

            var cells_to_avoid = [];

            $( state.cell.edges ).each( function( i, e ) {

                if ( before ) {

                    if ( e.target == state.cell ) {

                        cells_to_avoid.push( e.source );

                    }

                } else {

                    if ( e.source == state.cell ) {

                        cells_to_avoid.push( e.target );

                    }

                }

            } );

            // For each "next" cells,
            // put the cell below (y += distance)
            if ( cells_to_avoid.length > 0 ) {

                $( cells_to_avoid ).each( function( i, e ) {

                    if ( y == view.getState( e ).origin.y ) {

                        // Y is calculated from the middle of the box so we do /2
                        y += e.geometry.height / 2 + skConstants.DIST_LINES;

                    }

                } );

            }

        }

        return y;

    };

    /*
     * this is where we trigger the editing at the second click
     *
     */

    var mxGraphSelectCellForEvent = mxGraph.prototype.selectCellForEvent;
    mxGraph.prototype.selectCellForEvent = function( cell, evt ) {

        if ( !this.isToggleEvent( evt ) && this.isCellSelected( cell ) && this.getSelectionCount() === 1 ) {

            this.startEditingAtCell( cell, evt );

        }  else {

            mxGraphSelectCellForEvent.apply( this, arguments );

        }

    };

    // Enables the "create Target" when line is dropped in the white area
    mxConnectionHandler.prototype.createTarget = true;

    // line always sticks to left

    var mxConnectionHandlerupdateEdgeState = mxConnectionHandler.prototype.updateEdgeState;
    mxConnectionHandler.prototype.updateEdgeState = function( current, constraint ) {

        if ( !constraint ) {

            constraint = new mxConnectionConstraint( new mxPoint( 0, 0.5 ), true );

        }

        mxConnectionHandlerupdateEdgeState.call( this, current, constraint );

    };

    // When a new line (edge) is created with "drag and drop the ball", the line is NOT selected
    mxConnectionHandler.prototype.select = false;

    // Function fired up when the above is true
    mxConnectionHandler.prototype.createTargetVertex = function createTargetVertexF( evt, sourceCell ) {

        // console.log( "createTargetVertex" );

        var graph = this.graph;

        var new_box = null;

        new_box = graph.view.getState( sourceCell ).add_box_after_box_state( this.graph.getPointForEvent( evt ), null, this.sourceConstraint );

        mxEvent.consume( evt );

        return new_box;

    };

    // Increases default rubberband opacity (default is 20)
    mxRubberband.prototype.defaultOpacity = 30;

    /*
     * BOX SELECTION
     *
     *
     *********************/

    // Creates the shape of the selection frame for y-box and groups (so it matches the borders)
    mxVertexHandler.prototype.createSelectionShape = function createSelectionShapeF( bounds ) // Refactor : skore in graph editor
    {

        var key = this.state.style[ mxConstants.STYLE_SHAPE ];
        var stencil = mxStencilRegistry.getStencil( key );
        var shape = null;

        // console.log( "mxVertexHandler.createSelectionShape", this.state.cell.id, stencil );

        if ( stencil != null ) {

            shape = new mxShape( stencil );
            shape.apply( this.state );

        } else {

            shape = new this.state.shape.constructor();

        }

        shape.outline = true;
        shape.bounds = bounds;
        shape.stroke = this.getSelectionColor(); //MxConstants.HANDLE_STROKECOLOR;
        shape.strokewidth = this.getSelectionStrokeWidth();
        shape.isDashed = this.isSelectionDashed();
        shape.isShadow = false;

        return shape;

    }; // End create selection shape

    /*
     * GROUPING cells
     *
     *
     *********************/

    var graphCreateGroupCell = mxGraph.prototype.createGroupCell;

    mxGraph.prototype.createGroupCell = function createGroupCellF() {

        var group = graphCreateGroupCell.apply( this, arguments );

        group.setStyle( skConstants.group );
        group.value = skUtils.createBoxElement( skConstants.group ); //mxUtils.createXmlDocument().createElement( skConstants.group ); // SkShell.editorDefaults.whatbox_properties.cloneNode(true);
        group.setConnectable( true );

        return group;

    };

    /*
     * SELECTION, change the original function so that cells are selected when touched (not when encompassed)
     *
     */

    mxGraph.prototype.getCells = function getCellsF( x, y, width, height, parent, result ) {

        result = ( result != null ) ? result : [];

        if ( width > 0 || height > 0 ) {

            var right = x + width;
            var bottom = y + height;
            var left = x;
            var top = y;

            parent = parent || this.getDefaultParent();

            if ( parent != null ) {

                var childCount = this.model.getChildCount( parent );

                for ( var i = 0; i < childCount; i++ ) {

                    var cell = this.model.getChildAt( parent, i );
                    var state = this.view.getState( cell );

                    if ( this.isCellVisible( cell ) && state != null ) {

                        var stateXPlusW = state.x + state.width;
                        var stateYplusH = state.y + state.height;

                        // @formatter:off
                        if ( // Normal behaviour : the cell must be completely IN the selection box
                            ( state.x >= x &&
                            state.y >= y &&
                            stateXPlusW <= right &&
                            stateYplusH <= bottom ) ||

                            // Extended : only on touch of the cell (does not need to be totally selected)
                            ( cell.isVertex() && (

                                    // Case 1 : cell overlapping on left
                                    (
                                        left <= stateXPlusW &&
                                        left >= state.x &&
                                        top <= state.y &&
                                        bottom >= stateYplusH
                                    ) ||

                                    // Case 2 : cell overlaping top left corner
                                    (
                                        left <= stateXPlusW &&
                                        left >= state.x &&
                                        top >= state.y &&
                                        top <= stateYplusH
                                    ) ||

                                    // Case 3 : cell overlaping top
                                    (
                                        left <= state.x &&
                                        right >= stateXPlusW &&
                                        top >= state.y &&
                                        top <= stateYplusH
                                    ) ||

                                    // Case 4 : cell overlaping top right corner
                                    (
                                        left >= state.x &&
                                        left <= stateXPlusW &&
                                        bottom >= state.y &&
                                        bottom <= stateYplusH
                                    ) ||

                                    // Case 5 : etc, etc.
                                    (
                                        right >= state.x &&
                                        right <= stateXPlusW &&
                                        top <= state.y &&
                                        bottom >= stateYplusH
                                    ) ||

                                    // Case 6
                                    (
                                        right >= state.x &&
                                        right <= stateXPlusW &&
                                        bottom >= state.y &&
                                        bottom <= stateYplusH

                                    ) ||

                                    // Case 7
                                    (
                                        left <= state.x &&
                                        right >= stateXPlusW &&
                                        bottom >= state.y &&
                                        bottom <= stateYplusH
                                    ) ||

                                    // Case 8
                                    (
                                        right >= state.x &&
                                        right <= stateXPlusW &&
                                        top >= state.y &&
                                        top <= stateYplusH
                                    )
                                )
                            )
                        ) {

                            // Todo : for each "qualified" cell, look into the visible children as well
                            result.push( cell );

                        } else
                        {

                            this.getCells( x, y, width, height, cell, result );

                        }

                        // @formatter:on

                    }

                }

            }

        }

        return result;

    };

    /*
     * Drag & drop to create cells / ConnectionHandler
     *
     *
     *********************/

    // enable cell marker to be able to drag a line anywhere on a box
    mxCellMarker.prototype.enabled = true;

    // disable the "intersects" calculation so that click on a box does NOT
    // starts creation of a line
    mxCellMarker.prototype.intersects = function() {

        return false;

    };

    /*
     * constraints
     *
     */

    mxConnectionHandler.prototype.factoryMethod = function factoryMethodF() {

        return;

    };

    /*
     * clearwaypoints clear way points
     *
     */

    skGraph.prototype.clearWaypoints = function( myCells ) {

        var cells = myCells || this.getSelectionCells();

        if ( cells != null ) {

            this.getModel().beginUpdate();

            try {

                for ( var i = 0; i < cells.length; i++ ) {

                    var cell = cells[ i ];

                    if ( this.getModel().isEdge( cell ) ) {

                        var geo = this.getCellGeometry( cell );

                        if ( geo != null ) {

                            geo = geo.clone();
                            geo.points = null;
                            this.getModel().setGeometry( cell, geo );

                        }

                    }

                }

            }

            finally {

                this.getModel().endUpdate();

            }

        }

    };

    // When a new line (edge) is created with "drag and drop the ball", the line is NOT selected
    mxConnectionHandler.prototype.select = false;

    /*
     * distribute cells
     *
     */

    mxGraph.prototype.distributeCells = function( horizontal, cells ) {

        var i;

        if ( cells == null ) {

            cells = this.getSelectionCells();

        }

        if ( cells != null && cells.length > 1 ) {

            var vertices = [];
            var max = null;
            var min = null;

            for ( i = 0; i < cells.length; i++ ) {

                if ( this.getModel().isVertex( cells[ i ] ) ) {

                    var state = this.view.getState( cells[ i ] );

                    if ( state != null ) {

                        var tmp = ( horizontal ) ? state.getCenterX() : state.getCenterY();
                        max = ( max != null ) ? Math.max( max, tmp ) : tmp;
                        min = ( min != null ) ? Math.min( min, tmp ) : tmp;

                        vertices.push( state );

                    }

                }

            }

            if ( vertices.length > 2 ) {

                vertices.sort( function( a, b ) {

                    return ( horizontal ) ? a.x - b.x : a.y - b.y;

                } );

                var t = this.view.translate;
                var s = this.view.scale;

                min = min / s - ( ( horizontal ) ? t.x : t.y );
                max = max / s - ( ( horizontal ) ? t.x : t.y );

                this.getModel().beginUpdate();

                try {

                    var dt = ( max - min ) / ( vertices.length - 1 );
                    var t0 = min;

                    for ( i = 1; i < vertices.length - 1; i++ ) {

                        var geo = this.getCellGeometry( vertices[ i ].cell );
                        t0 += dt;

                        if ( geo != null ) {

                            geo = geo.clone();

                            if ( horizontal ) {

                                geo.x = Math.round( t0 - geo.width / 2 );

                            } else {

                                geo.y = Math.round( t0 - geo.height / 2 );

                            }

                            this.getModel().setGeometry( vertices[ i ].cell, geo );

                        }

                    }

                }
                finally {

                    this.getModel().endUpdate();

                }

            }

        }

        return cells;

    };

    /**
     * Creates and returns a new undo manager.
     */
    skGraph.prototype.createUndoManager = function createUndoManagerF() {

        var graph = this;
        var undoManager = new mxUndoManager();

        this.undoListener = function undoListenerF( sender, evt ) {

            undoManager.undoableEditHappened( evt.getProperty( "edit" ) );

        };

        // Installs the command history
        var listener = mxUtils.bind( this, function( ) {

            this.undoListener.apply( this, arguments );

        } );

        graph.getModel().addListener( mxEvent.UNDO, listener );
        graph.getView().addListener( mxEvent.UNDO, listener );

        // Keeps the selection in sync with the history
        var undoHandler = function undoHandlerF( sender, evt ) {

            var cand = graph.getSelectionCellsForChanges( evt.getProperty( "edit" ).changes );
            var model = graph.getModel();
            var cells = [];

            for ( var i = 0; i < cand.length; i++ ) {

                if ( ( model.isVertex( cand[ i ] ) || model.isEdge( cand[ i ] ) ) && graph.view.getState( cand[ i ] ) != null ) {

                    cells.push( cand[ i ] );
                    graph.cellHTMLCache[ parseInt( cand[ i ].id, 10 ) ] = null;

                }

            }

            graph.setSelectionCells( cells );

        };

        undoManager.addListener( mxEvent.UNDO, undoHandler );
        undoManager.addListener( mxEvent.REDO, undoHandler );

        graph.undoManager = undoManager;

    };

    skGraph.prototype.documentEdited = false;

    skGraph.prototype.setDocumentEdited = function setDocumentEditedF( value ) {

        this.documentEdited = value;

        if ( ELECTRON ) {

            if ( window.requireNode( "electron" ).remote.process.platform === "darwin"  )  {

                window.requireNode( "electron" ).remote.getCurrentWindow().setDocumentEdited( value );

            }

        }

    };

    /*
     * Resizeing the lines
     *
     *
     *
     *
     */

    mxEdgeHandler.prototype.removeEnabled = true;
    mxEdgeHandler.prototype.mergeRemoveEnabled = true;
    mxEdgeHandler.prototype.dblClickRemoveEnabled = true;
    mxEdgeHandler.prototype.straightRemoveEnabled = true;

    var cp = mxEdgeHandler.prototype.changePoints; // Refactor : in graph editor
    mxEdgeHandler.prototype.changePoints = function changePointsF() {

        cp.apply( this, arguments );

        // Use skStatus
        skShell.skStatus.setAndDontShowAgain(
            "Line path changed!<br/><hr>To reset to original path,<br/>" +
                "<span style='border-bottom:black double'>double click</span> on one of the handles.",
            "DONTSHOWAGAINlineHasChanged" );

    };

} );

