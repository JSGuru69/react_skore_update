
define( function( require ) {

    "use strict";

    var mxUtils = require( "mxUtils" );
    var skConstants = require( "skConstants" );
    var $ = require( "jquery" );
    var skUtils = require( "skUtils" );

    function skCardshoe( graphManager ) {

        /*jshint validthis:true*/
        console.log( "skCardshoe.init_cardshoe" );

        this.graphManager = graphManager;

        /*
         * Adds the cardshoe
         *
         */

        $( '.cardshoe [data-toggle="tooltip"]' ).tooltip();

        this.dragsource_wb = this.graphManager.makeCardshoeBoxDraggable( $( ".skSideMenu .cardshoe-box-wb" )[ 0 ], skConstants.whatbox );
        this.dragsource_yb = this.graphManager.makeCardshoeBoxDraggable( $( ".skSideMenu .cardshoe-box-yb" )[ 0 ], skConstants.whybox );
        this.dragsource_sn = this.graphManager.makeCardshoeBoxDraggable( $( ".skSideMenu .cardshoe-box-sn" )[ 0 ], skConstants.stickynote );

        this.setEnabled( false );

        this.graphManager.addListener( "graphEnabled", mxUtils.bind( this, function( sender, event ) {

            this.setEnabled( event.getProperty( "enabled" ) );

        } ) );

        /*
         * updates list of inputs / outputs
         *
         */

        this.graphManager.addListener( "navigationDone", mxUtils.bind( this,  function( sender, event ) {

            this.buildIOList( event.getProperty( "destinationCell" ) );

        } ) );

    }

    skCardshoe.prototype.dragsource_yb = null;
    skCardshoe.prototype.dragsource_wb = null;
    skCardshoe.prototype.dragsource_sn = null;

    skCardshoe.prototype.buildIOList = function( destinationCell ) {

        var that = this;
        $( ".skWingIOList" ).empty();
        $( ".skWhyBoxIOList" ).hide();

        var ioList = this.graphManager.getIOWhyboxes( destinationCell );

        var yb =  '<div class="cardshoe">' +
            '<span class="cardshoe-box cardshoe-box-yb" data-toggle="tooltip" data-placement="bottom" ' +
            'title="Drag &amp; drop on the canvas">' +
                '<span class="cardshoe-box-text">' +
                "</span>" +
            "</span>" +
        "</div>";

        var y;

        var createList = function( list, placeholder ) {

            var listIO = $( "ul", placeholder );
            listIO.empty();

            if ( list.length ) {

                $( placeholder ).show();

                listIO.empty();
                list.forEach( function( cell ) {

                    y = $( yb );
                    $( ".cardshoe-box-text", y ).html( skUtils.renderMarkdown( cell.getBoxText(), true ) );
                    listIO.append( y );

                    that.additional.push( that.graphManager.makeDraggable( y[ 0 ], skConstants.whybox, cell.value.cloneNode( true ) ) );

                } );

            } else {

                $( placeholder ).hide();
                listIO.empty();

            }

        };

        this.additional = [];
        createList( ioList.inputs, $( ".skWhyBoxInputsList" ) );
        createList( ioList.outputs, $( ".skWhyBoxOutputsList" ) );

    };

    skCardshoe.prototype.getCurrentGraph = function() {

        return this.graphManager.getCurrentGraph();

    };

    skCardshoe.prototype.additional = null;
    skCardshoe.prototype.setEnabled = function( enabled ) {

        this.dragsource_wb.setEnabled( enabled );
        this.dragsource_yb.setEnabled( enabled );
        this.dragsource_sn.setEnabled( enabled );
        if ( this.additional ) {

            this.additional.forEach( function( source ) {

                source.setEnabled( enabled );

            } );

        }
        $( ".cardshoe" ).toggleClass( "disabled", !enabled );

    };

    return skCardshoe;

} );
