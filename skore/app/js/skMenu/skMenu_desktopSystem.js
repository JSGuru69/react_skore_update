
/*
* Used by appshell to execute commands from OS Menu
*
*
*********************/

define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var mxUtils = require( "mxUtils" );
    var skConstants =  require( "skConstants" );
    var skGraphEditorTutorial = require( "skGraphEditorUI/skGraphEditorTutorial" );
    var skGraphUtils = require( "skGraph/skGraphUtils" );
    var skShell = require( "skShell" );
    var skThumbnail = require( "skNavigatorUI/skThumbnail" );

    return function setupMenuDesktop( navigatorUI ) {

        console.log( "skMenu.setupMenuDesktop" );

        var ipcRenderer = window.requireNode( "electron" ).ipcRenderer;

        var actions = navigatorUI.graphManager.actions;

        var prepareContentToSave = function prepareContentToSaveF( saveType, silent, event, actionToDoAfter ) {

            var filePath = skShell.currentFilePath;
            var currentTitle = navigatorUI.getCurrentGraph().getTitle();
            var data = mxUtils.getXml( navigatorUI.getCurrentGraph().getGraphXml() );
            var htmlServer = skConstants.justSkoreItBaseUrl;
            var thumbnail;
            var fulltext = "";

            if ( saveType === "html" || saveType === "social" || ( filePath && filePath.split( "/" ).pop().split( "." ).pop() === "html" ) ) {

                thumbnail = ( new skThumbnail() ).createDataURL( navigatorUI.getCurrentGraph() );
                fulltext = skGraphUtils.getAllProcessTextAsHtml( navigatorUI.getCurrentGraph() );

            }

            var params = JSON.stringify( {

                filePath: filePath,
                currentTitle: currentTitle,
                data: data,
                thumbnail: thumbnail,
                actionToDoAfter: actionToDoAfter,
                htmlServer: htmlServer,
                fulltext: fulltext,
                silent: silent,
                saveType: saveType

            } );

            if ( event ) {

                event.sender.send( "hereAreTheThingsToSave", params );

            } else {

                return ipcRenderer.sendSync( "hereAreTheThingsToSave", params );

            }

            thumbnail = null;

        };

        // Handles "native" copy / paste / cut
        $( document ).on( "copy paste cut", function( event ) {

            console.log( "native edit menu : " + $( event )[ 0 ].type );

        } );

        $( document ).on( "copy", function( event ) {

            console.log( "event document copy" );

            var graph = navigatorUI.getCurrentGraph();

            if ( graph.isEnabled() && !window.textEditing( event ) && !graph.isSelectionEmpty() ) {

                window.requireNode( "electron" ).clipboard
                    .writeText(
                        skShell.copyCells(
                            graph,
                            mxUtils.sortCells(
                                graph.model.getTopmostCells(
                                    graph.getSelectionCells() ) ) ) );

            }

        } );

        // Handle this one only when used from the menu.
        // when using keyboard shortcut it's already under control
        $( document ).on( "paste", function( event ) {

            // We only handle if triggered from menu, i.e. target is body
            if ( event.target === document.body ) {

                console.info( "desktop paste" );

                var xml = window.requireNode( "electron" ).clipboard.readText();

                if ( xml !== null && xml.length > 0 ) {

                    skShell.pasteText( navigatorUI.getCurrentGraph(), xml );

                }

            }

        } );
        $( document ).on( "cut", function( event ) {

            var graph = navigatorUI.getCurrentGraph();

            if ( event.target === document.body ) {

                if ( graph.isEnabled() && !window.textEditing( event ) && !graph.isSelectionEmpty() ) {

                    window.requireNode( "electron" ).clipboard
                        .writeText(
                            skShell.copyCells(
                                graph,
                                graph.removeCells() ) ) ;

                }

            }

        } );

        console.log( "set beforeunload" );

        window.addEventListener( "beforeunload", function( e ) {

            console.log( "onbeforeunload" );

            if ( DEV ) {

                e.returnValue = undefined;

            } else {

                e.preventDefault();

                if ( !navigatorUI.documentEdited ) {

                    e.returnValue = undefined;

                } else {

                    // Focus on the window (if you're closing several window)
                    var bw = window.requireNode( "electron" ).remote.BrowserWindow.getFocusedWindow();
                    bw.focus();

                    var handleRes = function handleResF( res ) {

                        switch ( res ) {

                            case 0 :

                                var res2 = prepareContentToSave( null, null, null, "closeMeNow" );

                                if ( res2 === "closeMeNow" ) {

                                    e.returnValue = undefined;

                                } else if ( res2 === "dontClose" ) {

                                    e.returnValue = false;

                                }

                            break;

                            case 1 :

                                // Cancel
                                e.returnValue = false;
                                break;

                            case 2 :

                                // Don't save
                                e.returnValue = undefined;
                                break;

                        } // End switch

                    }; // End handleRes;

                    var res = window.requireNode( "electron" ).remote.dialog.showMessageBox(

                        // Bw,
                        {
                            type:"question",
                            message: "Do you want to save ?",
                            buttons:[ "Save", "Cancel", "Don't save" ],
                            cancelId: 1
                        }
                    );

                    handleRes( res );

                } // End else (document has been edited)

            }

        } );

        console.log( "setup executeCommandFromOSMenu" );

        ipcRenderer.on( "executeCommandFromOSMenu", function( event, command ) {

          console.log( "executeCommandFromOSMenu", command );

          switch ( command ) {

            case "update" :

                require( "skModal/skModal_CheckUpdate" )( "menu" );

            break;

            case "open" :

                navigatorUI.getGraphManager().openFromFilePath( arguments[ 2 ] );

            break;

            case "tutorial" :

                if ( skShell.tutorial ) {

                    skShell.tutorial.end();
                    skShell.tutorial = null;

                } else {

                    skShell.tutorial = new skGraphEditorTutorial( navigatorUI.getCurrentGraph() );

                }

            break;

            case "modal_print" :
            case "modal_imageexport" :
            case "modal_registration" :

            case "undo" :
            case "redo" :
            case "delete" :
            case "zoomIn" :
            case "zoomOut" :
            case "zoomReset" :
            case "zoomFit" :
            case "enterGroup" :
            case "exitGroup" :
            case "toggleStickynotes" :
            case "group" :
            case "unGroup" :
            case "alignTop" :
            case "alignCenter" :
            case "sendBack" :
            case "bringFront" :

                if ( !navigatorUI.getCurrentGraph().isEditing() ) {

                    var action = actions.get( command );

                    if ( action && action.enabled ) {

                        action.funct();

                        return true;

                    }

                }

            break;

            } // End switch;

        } ); // End ipc

        ipcRenderer.on( "giveMeStuffToSave", function( event, type, silent ) {

            console.log( "skMenu.giveMeStuffToSave" );

            prepareContentToSave( type, silent, event );

        } );

        ipcRenderer.on( "fileSaved", function( event, status, filePath, mode ) {

            if ( status === "ok" ) {

                skShell.setStatus( filePath + " saved" );

                if ( mode !== "social" ) {

                    skShell.currentFilePath = filePath;
                    var w = window.requireNode( "electron" ).remote.getCurrentWindow();

                    w.setTitle( filePath.split( "/" ).pop() );

                    navigatorUI.getCurrentGraph().setDocumentEdited( false );

                }

            } else {

                skShell.setStatus( "Error" );
                skShell.skStatus.setWithButtons( "Sorry, there is an error saving " + filePath + ", " + status +
                    ". Please try to save as... and use another name." );

            }

        } );

    };// End function

} );
