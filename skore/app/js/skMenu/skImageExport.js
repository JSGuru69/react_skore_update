
define( function( require ) {

    "use strict";

    var $ = require( "jquery" );

    return function setupImageExport() {

        console.log( "setupImageExport" );

        if ( ELECTRON ) {

            var remote = window.requireNode( "electron" ).remote;
            var app = remote.app;
            var fsj = window.requireNode( "fs-jetpack" );

            var skGraph = require( "skGraph/skGraph" );
            var skMenu = require( "./skMenu" );
            var skShell = require( "skShell" );

            skMenu.prototype.imageExportNow = function imageExportNowF( whereToSave ) {

                var thisMenu = this;

                this.initImageExportModal();

                console.log( "imageExportNow" );

                if ( thisMenu.navigatorUI.graph.getSelectionCells().length ||
                    ( thisMenu.navigatorUI.graph.getDefaultParent().children !== null &&
                        thisMenu.navigatorUI.graph.getDefaultParent().children.length ) ) {

                    // Create the page
                    var fullHTML = require( "text!root/template_imageExport.html" );

                    var res = this.create_html_string_for_image_export( thisMenu.navigatorUI.graph );

                    fullHTML = fullHTML.replace( "{%svg%}", res.html );

                    // Write the html file that will be imageExport

                    /*
                     * Get the bloody html and css in the temp directory
                     *
                     */

                    skShell.copyFileOutOfElectron(
                        "template_imageExport.html",
                        app.getPath( "temp" ) + "com.the-skore.skore/template_imageExport.html" );
                    skShell.copyFileOutOfElectron(
                        "styles",
                        app.getPath( "temp" ) + "com.the-skore.skore/styles" );

                    var userDataDir = fsj.cwd( app.getPath( "temp" ) + "com.the-skore.skore" );
                    var stateStoreFile = "template_imageExport.html";

                    userDataDir.writeAsync( stateStoreFile, fullHTML, { atomic: true } ).then( function( error ) {

                        if ( error ) {

                            throw error;

                        } else {

                            console.log( "ready for screenshot" );

                            var BrowserWindow = window.requireNode( "electron" ).remote.BrowserWindow;

                            var bw = new BrowserWindow( {
                                x: 0,
                                y: 0,
                                width: parseInt( res.width, 10 ),
                                height: parseInt( res.height, 10 ),
                                show: false,
                                frame: false,

                                nodeIntegration: false,
                                transparent: false,
                                enableLargerThanScreen: true,
                                skipTaskbar: true,
                                directWrite: true
                            } );

                            bw.loadURL( "file://" + userDataDir.cwd() + "/" + stateStoreFile );

                            bw.webContents.on( "did-finish-load", function() {

                                window.setTimeout( function() {

                                    bw.capturePage( {
                                        x: 0,
                                        y: 0,
                                        width: parseInt( res.width, 10 ),
                                        height: parseInt( res.height, 10 )
                                    }, function( image ) {

                                        // SkShell.tmpImageBase64 = image.toDataURL();
                                        skShell.tmpImage = image;

                                        if ( whereToSave == "preview" ) {

                                            // Create Modal to display the image
                                            $( "#skModalImageExport" ).modal( "show", image.size );

                                        } else if ( whereToSave == "social" ) {

                                            return image.size;

                                        } else {

                                            thisMenu.saveImage( whereToSave );

                                        }

                                        window.setTimeout( function() {

                                            bw.close();

                                        }, 100 );

                                    } );

                                }, 100 );

                            } );

                        }

                    } ); // End writeFile

                } else {

                    skShell.setStatus( "No content to export as image" );

                }

            };

            skMenu.prototype.create_html_string_for_image_export = function create_html_string_for_image_exportF( graphToExport ) {

                console.log( "create_html_string_for_image_export" );

                // FIXME: handle the case when there are no cells at all

                /* ⓘ info
                 *
                 * for the rendering to work, i  need to create a new container
                 * need to make the container visible so that it generates the
                 * box with the right size (otherwise, foreignobject have width and height of 1)
                 *
                 */

                var container = document.createElement( "div" );

                container.id = "newGraph";
                container.style.position = "absolute";

                // Container.style.display = "none";
                container.style.top = "100%";
                document.body.appendChild( container );

                var newGraph = new skGraph( container, null, null, this.navigatorUI.graph.getStylesheet() );

                newGraph.labelMode = "htmlstring";

                if ( graphToExport.background !== "#ffffff" ) {

                    newGraph.background = graphToExport.background;

                }

                // Get the selected cells
                var cells = graphToExport.getSelectionCells();

                if ( cells.length ) {

                    $( ".skSelectedCellsOnly" ).text( "Only selected cells used in image." );

                }

                // If not; get all the cells
                if ( !cells.length ) {

                    $( ".skSelectedCellsOnly" ).empty().hide();
                    cells = graphToExport.model.getChildren( graphToExport.getDefaultParent() );

                }

                cells = newGraph.cloneCells( cells );

                newGraph.addCells( cells );

                // Shift the cells
                var bounds = newGraph.getBoundingBoxFromGeometry( cells, true );

                newGraph.moveCells( cells, -bounds.x + 15, -bounds.y + 15 );

                newGraph.refresh();

                // Does some work on the container to make our lives easier
                var svg = container.getElementsByTagName( "svg" )[ 0 ];

                svg.style.minWidth = "";
                svg.style.minHeight = "";

                var result = {
                    html:   container.innerHTML,
                    height: bounds.height + 30,
                    width:  bounds.width + 30
                };

                console.log( "w", bounds.width, "h", bounds.height );

                container.innerHTML = "";
                document.body.removeChild( container );
                newGraph = null;

                return result;

            };

            skMenu.prototype.initImageExportModal_loaded = false;

            skMenu.prototype.initImageExportModal = function initImageExportModalF() {

                if ( !this.initImageExportModal_loaded ) {

                    var thisMenu = this;

                    this.initImageExportModal_loaded = true;
                    /*
                     * Init modal that will display the image export
                     *
                     */

                    var template = $( require( "text!skModal/skModal_ImageExport.html" ) );

                    $( document.body ).append( template );

                    // Unloaded
                    $( "#skModalImageExport" ).on( "hidden.bs.modal", function() {

                        // SkShell.tmpImageBase64 = null;
                        skShell.tmpImage = null;

                    } );

                    // Loaded
                    $( "#skModalImageExport" ).on( "shown.bs.modal", function() {

                        // Var size = event.relatedTarget;

                        var img = $( "<img style='box-shadow:0 0 14px #8E8E8E;' class='center-block img-responsive' src='" +
                            skShell.tmpImage.toDataURL() + "' />" );

                        $( ".skImageExportContainer", template ).append( img );

                        window.setTimeout( function() {

                            $( ".skImageDetails" ).html( "" +
                                "width : " + skShell.tmpImage.getSize().width + "px, " +
                                "height : " + skShell.tmpImage.getSize().height + "px " +
                                "<br/>(previewed image above may not be original size)" );

                        }, 100 );

                        $( ".skImageExportButton", template ).toggleClass( "disabled", !skShell.allowSave );

                    } );

                    // Unloaded
                    $( "#skModalImageExport" ).on( "hidden.bs.modal", function() {

                        $( ".skImageExportContainer", template ).empty();

                        // SkShell.tmpImageBase64 = null;
                        skShell.tmpImage = null;

                    } );

                    /*
                     * Init buttons sideMenu & modal
                     *
                     */

                    $( ".skSideMenu_image" ).on( "click", function() {

                        skShell.wellDoneMessage.hey( this );

                        console.log( "click skSideMenu_image" );
                        thisMenu.imageExportNow( $( this ).data( "target" ) );

                    } );

                    $( ".skImageExportButton", template ).on( "click", function() {

                        thisMenu.saveImage( $( this ).data( "target" ) );

                    } );

                }

            };
            skMenu.prototype.saveFile = function saveFileF( path ) {

                fsj.writeAsync(
                    path,
                    skShell.tmpImage.toPng() ) //New Buffer.Buffer( skShell.tmpImageBase64, "base64" ) )
                .then(
                    function( err ) {

                        if ( err ) {

                            throw err;

                        } else {

                            if ( path.startsWith( "\\" ) ) {

                                skShell.skStatus.set( "Image saved :-)" );

                            } else {

                                skShell.skStatus.setAndAction( "Image saved :-)", "Open containing folder", function() {

                                    window.requireNode( "electron" ).shell.showItemInFolder( path );

                                } );

                            }

                            // SkShell.setStatus("Image saved :) <br/>" + path);

                        }

                    }
                );

            };

            skMenu.prototype.saveImage = function saveImageF( whereToSave ) {

                if ( skShell.allowSave ) {

                    var thisMenu = this;

                    if ( whereToSave == "clipboard" ) {

                        // Create the NativeImage
                        // var nat = remote.require( "native-image" );

                        // Var img = nat.createFromDataUrl("data:image/png;base64," + skShell.tmpImageBase64);
                        // var img = nat.createFromBuffer( new Buffer.Buffer( skShell.tmpImageBase64, "base64" ) );

                        // Copy to clipbard
                        var clipboard = window.requireNode( "electron" ).clipboard;

                        // Bug may 2016... seems to not work directly??

                        // clipboard.writeImage( skShell.tmpImage );

                        // Alternative :

                        clipboard.writeImage(
                            window.requireNode( "electron" ).nativeImage
                            .createFromDataURL( skShell.tmpImage.toDataURL() )
                        );

                        skShell.setStatus( "image in clipboard, ready to be pasted!" );

                    } else if ( whereToSave == "desktop" || whereToSave == "file" ) {

                        var path;

                        var tmpD = ( new Date().toString().substr( 0, 24 ) + ").png" ).replace( /:/gi, "-" );

                        if ( whereToSave === "desktop" ) {

                            path = remote.app.getPath( "userDesktop" ) + "/" + "Skore (" + tmpD;
                            thisMenu.saveFile( path );

                        } else if ( whereToSave === "file" ) {

                            var dialog = remote.require( "dialog" );
                            var name = "Skore (" + tmpD;

                            dialog.showSaveDialog(
                            {
                                title: name,
                                defaultPath: name,
                                filters: [
                                    { name: "Image", extensions: [ "png" ] }
                                ]
                            },
                            function( fileName ) {

                                if ( fileName === undefined ) {

                                    return;

                                }
                                thisMenu.saveFile( fileName );

                            } );

                        }

                    }

                } else {

                    skShell.setStatus( "need license to save." );

                }

            };

        }

    };

} );
