define( function( require ) {

    "use strict";

    var $ = require( "jquery" );
    var skShell = require( "skShell" );
    var skDrive = require( "skDrive/skDrive" );

    return function setupMenuGDrive() {

        $( ".forGDriveVersion" ).show();
        $( ".skgDriveFileLink" ).on( "click", function( event ) {

            event.stopPropagation();

        } );

        // Open for google
        $( ".skSideMenu_open .explanation" ).html( "Open from your Google Drive" );
        $( ".skSideMenu_open" ).on( "click", function() {

            $( ".skSideMenu" ).hide();

            skDrive.showPicker().done( function( file ) {

                skShell.openFromGoogleDrive( file.id );

            } );

            if ( !DEV && !ELECTRON && ga ) {

                ga( "send", "event", "SkoreAction", "sideBar", "GDrive Open" );

            }

        } );

        $( ".skSideMenu_save .explanation" ).html( "Force save to your Google Drive." );
        $( ".skSideMenu_save" ).on( "click", function() {

            skShell.saveToGoogleDrive();

            if ( !DEV && !ELECTRON && ga ) {

                ga( "send", "event", "SkoreAction", "sideBar", "GDrive Save" );

            }

        } );

        // Share with google drive
        $( ".skSideMenu_share_gDrive" ).on( "click", function() {

            if ( skShell.gdriveFile ) {

                skDrive.showShare( skShell.gdriveFile );

            } else {

                skShell.skStatus.set( "You need to save the file a first time." );

            }

        } );

        $( ".skSideMenu_license .sideMenuText" ).html( "Read license (Skore app for Google Drive)" );

        var skMenu = require( "./skMenu" );
        /*
         * Google drive things
         *
         *
         *********************/

        skMenu.prototype.updateSaveButton_ = function( file ) {

            console.log( "update save button" );

            if ( file ) {

                var filename = file.title;

                // Title
                $( ".skgDriveFileName" ).text( "File name : " + filename );

                // Date saved

                var now = new Date();
                var lastUpdateDate = new Date( file.modifiedDate );
                var diff = now - lastUpdateDate;

                // Less than 5 seconds
                if ( diff < 5000 ) {

                    $( ".skgDriveLastSaved" ).text( "Saved seconds ago" );

                }

                // Less than a minute
                else if ( diff < 60000 ) {

                    $( ".skgDriveLastSaved" ).text( "Saved a minute ago" );

                }

                // Less than 5 minutes
                else if ( diff < 300000 ) {

                    $( ".skgDriveLastSaved" ).text( "Saved less than 5 minutes ago" );

                } else {

                    $( ".skgDriveLastSaved" ).text( "Saved on " + lastUpdateDate.toString() );

                }

                // Text
                // $(".skDriveMenuSaveText").html('Last changes save : <span class="driveMenuSaveTime">' + lastUpdate + '</span>');

                // Need to change the style for cursor to be "normal"
                // not a hand --> there is nothing to click on
                //$("#skDriveMenuSave").addClass("saveOnDrive");

                // Button is always enabled at the moment
                // FIXME: what if the latest save operation failed, how can user retry save if button is disabled
                // FIXME: always save into localstorage, if network goes down user will not lose the work

            } else {

                // Title
                $( ".skDriveMenuSaveDescription" ).text( "Save Skore in Google Drive" );

                // Filename:
                $( ".skDriveMenuFileName" ).text( this.shell.graph.model.getRoot().children[ 0 ].getBoxText() );

                // Subtitle
                $( ".skDriveMenuSaveText" ).text( "Click here to save a first time." );

            }

        };

    };

} );
