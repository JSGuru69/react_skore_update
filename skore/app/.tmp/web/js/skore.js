define('text',{
    load: function(id) {
        throw new Error("Dynamic load not allowed: " + id);
    }
});
define('text!skModal/skModal_Splash.html',[],function () { return '<div class="modal fade" id="skModal_Splash" tabindex="-1" role="dialog" aria-labelledby="skModal_SplashLabel" data-backdrop="false" aria-hidden="true">\n\t<div class="modal-dialog modal-lg">\n\t\t<div class="modal-content">\n\t\t\t<div class="modal-header">\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\t\t\t\t<h4 class="modal-title" id="skModal_SplashLabel">Welcome to Skore App</h4>\n\t\t\t</div>\n\t\t\t<div class="modal-body">\n\t\t\t\t<div class="container-fluid">\n\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-md-6">\n\t\t\t\t\t\t\t<p>&nbsp;</p>\n\t\t\t\t\t\t\t<p class="text-center">\n\t\t\t\t\t\t\t\t<button class="btn btn-primary skStartTutorial">Start tutorial</button>\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t<p class="text-center">\n\t\t\t\t\t\t\t\tGuides you to create your first process\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="col-md-6">\n\t\t\t\t\t\t\t<p>&nbsp;</p>\n\t\t\t\t\t\t\t<p class="text-center">\n\t\t\t\t\t\t\t\t<button class="skCloseForNow btn btn-default">\n\t\t\t\t\t\t\t\t\tSkip tutorial\n\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t</p>\n\n\t\t\t\t\t\t\t<p class="text-center">\n\t\t\t\t\t\t\t\tAnd start using Skore app...\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t<p>&nbsp;</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n';});
define('skNavigatorUI/skHint',['require','jquery','mxCell','mxGeometry','mxUtils'],function(require) {
    "use strict";
    function skHint(navigatorUI) {
        this.navigatorUI = navigatorUI, this.container$ = $('<div class="skHint skInfoBox" style="display:none"></div>'), 
        $(document.body).append(this.container$), this.container$.on("mousedown", function(e) {
            $(this).addClass("draggable").parents().on("mousemove", function(e) {
                $(".draggable").offset({
                    top: e.pageY - $(".draggable").outerHeight() / 2,
                    left: e.pageX - $(".draggable").outerWidth() / 2
                }).on("mouseup", function() {
                    $(this).removeClass("draggable");
                });
            }), e.preventDefault();
        }).on("mouseup", function() {
            $(".draggable").removeClass("draggable");
        });
    }
    var $ = require("jquery"), mxCell = require("mxCell"), mxGeometry = require("mxGeometry"), mxUtils = require("mxUtils");
    return skHint.prototype.container$ = null, skHint.prototype.navigatorUI = null, 
    skHint.prototype.placeholderForCell = null, skHint.prototype.createHTML = function(obj) {
        var content = document.createElement("div");
        if (obj.title) {
            var title = document.createElement("div");
            title.className = "hint_title", mxUtils.write(title, obj.title), content.appendChild(title);
        }
        return obj.content && obj.content.forEach(function(value) {
            if (content.appendChild(document.createElement("hr")), "string" == typeof value) {
                var tmp = document.createElement("div");
                tmp.innerHTML = value, content.appendChild(tmp);
            } else value instanceof $ ? $(content).append(value) : content.appendChild(value);
        }), content;
    }, skHint.prototype.heyJude = function(textObject) {
        return this.hey(window, textObject, null, !0);
    }, skHint.prototype.hey = function(refObject, textObject, direction, continueButton, callback) {
        var that = this;
        return setTimeout(function() {
            if (that.container$.show().html("").append(that.createHTML(textObject)), refObject instanceof mxCell) {
                var state = that.navigatorUI.getCurrentGraph().view.getState(refObject);
                null == that.placeholderForCell && (that.placeholderForCell = document.createElement("div"), 
                that.placeholderForCell.style.position = "absolute", that.placeholderForCell.style.display = "block", 
                that.placeholderForCell.className = "placeholderForCell", that.placeholderForCell.style.pointerEvents = "none", 
                that.navigatorUI.getCurrentGraph().container.appendChild(that.placeholderForCell)), 
                that.placeholderForCell.style.width = state.width + 20 + "px", that.placeholderForCell.style.height = state.height + 20 + "px", 
                that.placeholderForCell.style.left = state.x - 10 + "px", that.placeholderForCell.style.top = state.y + -10 + "px", 
                refObject = $(that.placeholderForCell);
            } else null == refObject ? refObject = $(window) : refObject instanceof mxGeometry && (that.placeholderForCell = $("<div></div>").css({
                position: "absolute",
                left: refObject.x + "px",
                top: refObject.y + "px",
                width: "1px",
                height: "1px"
            }), $(document.body).append(that.placeholderForCell), refObject = that.placeholderForCell);
            that.setPosition(refObject, direction), continueButton && (that.container$.append($("<div>").html("<a>Click anywhere to continue</a>")), 
            $(window).one("click", function() {
                that.hide();
            })), callback && callback.apply(that, arguments);
        }, 700), this;
    }, skHint.prototype.setPosition = function(refObject, direction) {
        this.container$.removeClass("skInfoBox_arrow").removeClass("skInfoBox_arrow_top").removeClass("skInfoBox_arrow_bottom").removeClass("skInfoBox_arrow_left").removeClass("skInfoBox_arrow_right"), 
        null == direction || "" === direction ? this.container$.show().position({
            my: "center",
            at: "center",
            of: $(refObject),
            collision: "fit"
        }) : (this.container$.addClass("skInfoBox_arrow"), "right" == direction ? this.container$.show().position({
            my: "left center",
            at: "right center",
            of: $(refObject),
            collision: "flip",
            using: function(position, data) {
                data.element.left + data.element.width < data.target.left ? $(this).addClass("skInfoBox_arrow_right") : $(this).addClass("skInfoBox_arrow_left"), 
                $(this).css({
                    top: position.top + "px",
                    left: position.left + "px"
                });
            }
        }) : "left" == direction ? this.container$.show().position({
            my: "right center",
            at: "left-10 center",
            of: $(refObject),
            collision: "flip",
            using: function(position, data) {
                data.element.left > data.target.left + data.target.width ? $(this).addClass("skInfoBox_arrow_left") : $(this).addClass("skInfoBox_arrow_right"), 
                $(this).css({
                    top: position.top + "px",
                    left: position.left + "px"
                });
            }
        }) : "top" == direction ? this.container$.show().position({
            my: "center bottom",
            at: "center top-10",
            of: $(refObject),
            collision: "flip",
            using: function(position, data) {
                data.element.top > data.target.top + data.target.height ? $(this).addClass("skInfoBox_arrow_top") : $(this).addClass("skInfoBox_arrow_bottom"), 
                $(this).css({
                    top: position.top + "px",
                    left: position.left + "px"
                });
            }
        }) : "bottom" == direction && this.container$.show().position({
            my: "center top",
            at: "center bottom+10",
            of: $(refObject),
            collision: "fit flip",
            using: function(position, data) {
                data.element.top + data.element.height < data.target.top ? $(this).addClass("skInfoBox_arrow_bottom") : $(this).addClass("skInfoBox_arrow_top"), 
                $(this).css({
                    top: position.top + "px",
                    left: position.left + "px"
                });
            }
        }));
    }, skHint.prototype.hide = function() {
        this.container$.hide(), this.placeholderForCell && ($(this.placeholderForCell).remove(), 
        this.placeholderForCell = null);
    }, skHint;
});
define('skGraph/skDefaultTexts',[],function() {
    "use strict";
    return function(skShell) {
        skShell.defaultTexts = {
            whatbox: "What happens?",
            resource: "Who does it?",
            whybox: "So that?",
            stickynote: "A comment",
            title: "Name your Skore",
            group: "Name your group",
            line: "",
            GRP_GROUP: "Group these boxes",
            GRP_UNGROUP: "Remove group (ungroup)",
            REL_VISIBLE_ENG: "current view",
            REL_PARENT_ENG: "is the parent",
            REL_GRD_PARENT_ENG: "is a grand parent",
            REL_CHILD_ENG: "is in a detailed view (direct)",
            REL_GRD_CHILD_ENG: "is in a detailed view (2 levels or more)",
            REL_NONE_ENG: "",
            REL_EQUAL_ENG: "current view"
        };
    };
});
define('skGraph/skColors',[],function() {
    "use strict";
    return {
        line: "#067C7E",
        box: "#15518e",
        BOX_VALID: "#00FF00",
        BOX_HOVER: "orange",
        BOX_SELECTED: "#FF00FF",
        BOX_HIGHLIGHT: "#FFEE88",
        BOX_GROUP: "#C0C0C0",
        GUIDE: "#e54a4b",
        OUTLINE_FRAME: "#e54a4b",
        PAGE_BREAK: "#C0C0C0",
        HANDOVER: "Purple"
    };
});
define('skConstants',[],function() {
    "use strict";
    var c = {};
    return c.OFFSET_EDITOR = 34, c.title = "title", c.whatbox = "whatbox", c.whatboxdetailed = "whatboxdetailed", 
    c.whybox = "whybox", c.stickynote = "stickynote", c.group = "group", c.highlightStyle = "cellhihglight", 
    c.line = "line", c.image = "image", c.FLEX_ARROW = "flexArrow", c.LINE_WY2S = "wy2s", 
    c.LINE_HIGHLIGHT = "linehighlight", c.ATT_COLOR = "attachmentColor", c.SURVEY_COLOR = "surveyColor", 
    c.DETAIL_COLOR = "detailColor", c.LINK_COLOR = "linkColor", c.START_WIDTH = "startWidth", 
    c.END_WIDTH = "endWidth", c.TEXT = "text", c.BOX = "box", c.ATTACHMENTS = "attachments", 
    c.ATTACHMENT = "attachment", c.SURVEY = "survey", c.RESOURCE = "resource", c.ROLE = "role", 
    c.RESPONSIBILITIES = "responsibilities", c.RESPONSIBILITY = "responsibility", c.ATTR_SHOWN = "displayed", 
    c.VALUE = "value", c.ATTR_TYPE = "type", c.WIDTH = "width", c.HEIGHT = "height", 
    c.WB_WIDTH = 190, c.WB_HEIGHT = 80, c.YB_WIDTH = 120, c.YB_HEIGHT = 80, c.DIST_W2Y = 25, 
    c.DIST_Y2W = 40, c.DIST_LINES = 80, c.STROKE_BOX = 1, c.STROKE_BOX_SELECTED = 2, 
    c.STROKE_LINE = 1, c.STROKE_LINE_selected = 4, c.REL_VISIBLE = "VISIBLE", c.REL_PARENT = "PARENT", 
    c.REL_GRD_PARENT = "GRANDPARENT", c.REL_CHILD = "CHILD", c.REL_GRD_CHILD = "GRANDCHILD", 
    c.REL_NONE = "NONE", c.REL_EQUAL = "SameSame", c.url_type = "url", c.text_type = "text", 
    c.url_address = "addr", c.gdrive_type = "gdrive", c.gdrive_address = "addr", c.TEMPLATES = "templates", 
    c.TEMPLATE = "template", c.TEMPLATE_NAME = "name", c.TEMPLATE_BASE = "base", c.TEMPLATE_DESCRIPTION = "description", 
    c.TEMPLATE_CUSTOM = "custom", c.RESP_RACI = "raci", c.RESP_RATSI = "ratsi", c.EXT_CONTENT = "externalcontent", 
    c.ELMT_TAG = "tag", c.HANDHOVER_HIGHLIGHT = "handoverhighlight", c.MULTI_RESOURCE_TAG = "mrt", 
    c.ROLES = "roles", c.ROLE = "role", c.ELMT_COMMENT = "comment", c.SOURCE_EMBED = "embed", 
    c.SOURCE_JUSTSKOREIT = "justskoreit", c.SOURCE_FILE = "file", c.SOURCE_GDRIVE = "gdrive", 
    c.SOURCE_NEW = "new", c.SOURCE_DEMOCONTENT = "demo", c.SOURCE_XMLEDIT = "xml", c.SRC_ENT = "SAAS", 
    c.stylesheets = "stylesheets", c.stylesheet = "stylesheet", c.styleTokens = "styleTokens", 
    c.style = "style", c.JUSTSKOREIT = "justSkoreIt", c.NAV_BREADCRUMB = "navbreadcrumb", 
    c.LOCAL_ID = "localid", c.REMOTE_ID = "remoteid", c.justSkoreItBaseUrl = "https://justskore.it/?embed", 
    c.justSkoreItBasUrlProd = c.justSkoreItBaseUrl, c.SEPARATOR = " | ", c.paperSizes = {
        A4: {
            name: "A4",
            displayName: "A4",
            width: 826,
            height: 1169
        },
        A3: {
            name: "A3",
            displayName: "A3",
            width: 1169,
            height: 1652
        },
        Letter: {
            name: "Letter",
            displayName: "Letter (8.5 x 11)",
            width: 850,
            height: 1100
        },
        Legal: {
            name: "Legal",
            displayName: "Legal (8.5 x 14)",
            width: 850,
            height: 1403
        },
        Tabloid: {
            name: "Tabloid",
            displayName: "Tabloid / Ledger (11 x 17)",
            width: 1097,
            height: 1703
        }
    }, c;
});
define('skShell',['require','skGraph/skDefaultTexts','skGraph/skColors','skConstants'],function(require) {
    "use strict";
    var setupSkDefaultTexts = require("skGraph/skDefaultTexts"), skColors = require("skGraph/skColors"), skConstants = require("skConstants"), shell = {};
    return shell.urlParams = null, shell.readUrlParams = function(url) {
        this.urlParams = {};
        var idx = url.lastIndexOf("?");
        if (idx > 0) for (var params = url.substring(idx + 1).split("&"), i = 0; i < params.length; i++) idx = params[i].indexOf("="), 
        idx > 0 ? this.urlParams[params[i].substring(0, idx)] = params[i].substring(idx + 1) : this.urlParams[skConstants.JUSTSKOREIT] = params[i];
    }, shell.wKeyIsPressed = null, shell.yKeyIsPressed = null, shell.nKeyIsPressed = null, 
    shell.colors = skColors, setupSkDefaultTexts(shell), shell.copyFileOutOfElectron = function(sourceInAsarArchive, destOutsideAsarArchive) {
    }, shell;
});
define('skGraphEditorUI/skWellDoneMessage',['require','jquery','mxCell'],function(require) {
    "use strict";
    function skWellDoneMessage() {
        this.init();
    }
    var $ = require("jquery"), mxCell = require("mxCell");
    return skWellDoneMessage.prototype.box = null, skWellDoneMessage.prototype.shell = null, 
    skWellDoneMessage.prototype.hintContainer$ = null, skWellDoneMessage.prototype.init = function() {
        this.box = document.createElement("div"), this.box.className = "wellDone", this.box.style.position = "absolute", 
        document.body.appendChild(this.box);
    }, skWellDoneMessage.prototype.setHintContainer = function(container$) {
        this.hintContainer$ = container$;
    }, skWellDoneMessage.prototype.clearHintContainer = function() {
        this.hintContainer$ = null;
    }, skWellDoneMessage.prototype.hey = function(element, text) {
        var element$ = null;
        element instanceof mxCell ? (element$ = $("#box" + element.id), 0 === element$.length && (element$ = $(window.document.body))) : element$ = null == element && null != this.hintContainer$ ? this.hintContainer$ : null == element ? $(window.document.body) : element instanceof $ ? element : $(element);
        var t = text || this.messages[Math.floor(Math.random() * this.messages.length)], b = $(this.box).show().html(t).position({
            my: "center center",
            at: "center center",
            of: element$
        });
        navigator.userAgent.indexOf("MSIE 9") >= 0 ? b.fadeOut(1e3) : b.addClass("wellDone fadeOutUp animated"), 
        window.setTimeout(function() {
            b.hide(), b.removeClass("fadeOutUp"), b.removeClass("animated");
        }, 1e3);
    }, skWellDoneMessage.prototype.messages = [ "+1", "Ace!", "Applaudable!", "Astonishing!", "Awesome!", "Beaut!", "Bonzer!", "Bosting!", "Bravo!", "Brilliant!", "Class!", "Congratulations!", "Cool!", "Extraordinary!", "Fab!", "Fabulous!", "Fantastic!", "First class!", "First rate!", "Formidable!", "Ganz richtig! (That's German)", "Geil! (That's German)", "Genau! (That's German)", "Genious!", "Grand!", "Great", "Impressive!", "Marvellous!", "Matchless!", "Mighty!", "Neat!", "Outstanding!", "Phenomenal!", "Prodigious!", "Rad!", "Sensational!", "Skore!", "Smart!", "Smashing!", "Spiffing!", "Sublime!", "Super", "Superb!", "Swell!", "Terrific!", "Tip top!", "Well done!", "Wicked!", "Wonderful!", "Yeah!" ], 
    skWellDoneMessage;
});
define('skGraphEditorUI/skGraphEditorTutorial',['require','jquery','mxCell','mxClient','mxEvent','skNavigatorUI/skHint','skShell','skGraphEditorUI/skWellDoneMessage'],function(require) {
    "use strict";
    function skGraphEditorTutorial(navigatorUI, doNotStart) {
        this.graph = navigatorUI.getCurrentGraph(), this.navigatorUI = navigatorUI;
        var stopButton = $('<li class="stopTutorialButton" style="display:none"><a><i class="fa fa-close"></i> End tutorial</a></li>');
        $(".skWingMenuLeft .navbar-nav").show().append(stopButton), stopButton.fadeIn(1e3);
        var that = this;
        stopButton.on("click", function() {
            that.end();
        }), null == skShell.hint && (skShell.hint = new skHint(this.graph)), null == skShell.wellDoneMessage && (skShell.wellDoneMessage = new skWellDoneMessage()), 
        skShell.wellDoneMessage.setHintContainer(skShell.hint.container$), this.randomActionDone = [], 
        doNotStart || ($(".skStopTutorial").show(), this.goToStep(1)), this.graph.panningHandler.addListener(mxEvent.PAN, panListener), 
        this.graph.panningHandler.addListener(mxEvent.PAN_END, panListener), this.graph.addListener("navigationDone", navigationListener), 
        this.graph.addListener(mxEvent.MOVE_CELLS, moveListener), this.graph.addListener(mxEvent.CELLS_REMOVED, removeListener), 
        mxEvent.addListener(this.graph.container, "scroll", scrollListener), this.graph.getSelectionModel().addListener(mxEvent.CHANGE, selectionChangeListener), 
        navigatorUI.graphManager.addListener("droppedListener", droppedListener);
    }
    var $ = require("jquery"), mxCell = require("mxCell"), mxClient = require("mxClient"), mxEvent = require("mxEvent"), skHint = require("skNavigatorUI/skHint"), skShell = require("skShell"), skWellDoneMessage = require("skGraphEditorUI/skWellDoneMessage"), panListener = function() {
        skShell.tutorial.stepDone(skShell.tutorial.SCREEN_PANNED);
    }, scrollListener = function() {
        skShell.tutorial.stepDone(skShell.tutorial.SCREEN_PANNED);
    }, removeListener = function() {
        skShell.tutorial.stepDone(skShell.tutorial.BOX_DELETED_WITH_BUTTON);
    }, moveListener = function(event, sender) {
        skShell.tutorial.expectedCode && this.view.getState(sender.getProperty("cells")[0]) && skShell.tutorial.stepDone(skShell.tutorial.BOX_MOVED_ON_CANVAS, {
            cell: sender.getProperty("cells")[0]
        });
    }, droppedListener = function(sender, event) {
        skShell.tutorial.stepDone(skShell.tutorial.BOX_DROPPED_FROM_CARDSHOE, {
            cell: event.getProperty("cells")[0]
        });
    }, navigationListener = function() {
        skShell.tutorial.stepDone(skShell.tutorial.NAVIGATE);
    }, selectionChangeListener = function(sender, event) {
        var cellsAdded = event.getProperty("removed");
        1 == cellsAdded.length && cellsAdded[0].isWhatbox() && skShell.tutorial.stepDone(skShell.tutorial.BOX_SELECTED, {
            cell: cellsAdded[0]
        });
    };
    return skGraphEditorTutorial.prototype.graph = null, skGraphEditorTutorial.prototype.navigatorUI = null, 
    skGraphEditorTutorial.prototype.end = function() {
        null !== this.icon_for_hint_box_position && (this.icon_for_hint_box_position.parentNode.removeChild(this.icon_for_hint_box_position), 
        this.icon_for_hint_box_position = null), this.displayWellDoneMessage($(window), "Bye bye"), 
        skShell.wellDoneMessage.clearHintContainer(), $(".stopTutorialButton").hide(), skShell.hint.hide(), 
        ga && ga("send", "event", "SkoreAction", "tutorial", "endTutorial", this.currentStep), 
        this.currentStep = 0, skShell.tutorial = null, localStorage.setItem("DONTSHOWAGAINtutorial", !0), 
        this.graph.panningHandler.removeListener(panListener), this.graph.removeListener(moveListener), 
        this.graph.removeListener(removeListener), this.graph.removeListener(navigationListener), 
        mxEvent.removeListener(this.graph.container, "scroll", scrollListener), this.graph.getSelectionModel().removeListener(selectionChangeListener), 
        this.navigatorUI.graphManager.removeListener(droppedListener);
    }, skGraphEditorTutorial.prototype.graph = null, skGraphEditorTutorial.prototype.hintContainer$ = null, 
    skGraphEditorTutorial.prototype.any = "any", skGraphEditorTutorial.prototype.CREATE_TAB_OPENED = "CREATE_TAB_OPENED", 
    skGraphEditorTutorial.prototype.BOX_DROPPED_FROM_CARDSHOE = "BOX_DROPPED_FROM_CARDSHOE", 
    skGraphEditorTutorial.prototype.BOX_MOVED_ON_CANVAS = "BOX_MOVED_ON_CANVAS", skGraphEditorTutorial.prototype.BOX_CREATED_WITH_DND_FROM_CONNECT_ARROW = "BOX_CREATED_WITH_DND_FROM_CONNECT_ARROW", 
    skGraphEditorTutorial.prototype.BOX_CREATED_WITH_SPACE_BAR = "BOX_CREATED_WITH_SPACE_BAR", 
    skGraphEditorTutorial.prototype.BOX_DELETED_WITH_BUTTON = "BOX_DELETED_WITH_BUTTON", 
    skGraphEditorTutorial.prototype.BOX_TAB_PRESSED = "BOX_TAB_PRESSED", skGraphEditorTutorial.prototype.STOP_EDITING = "STOP_EDITING", 
    skGraphEditorTutorial.prototype.NEXT_Y_BOX_IS_CREATED = "NEXT_Y_BOX_IS_CREATED", 
    skGraphEditorTutorial.prototype.W_BOX_IS_CREATED = "W_BOX_IS_CREATED", skGraphEditorTutorial.prototype.SCREEN_PANNED = "SCREEN_PANNED", 
    skGraphEditorTutorial.prototype.TUT_NEXT_PRESSED = "TUT_NEXT_PRESSED", skGraphEditorTutorial.prototype.ATT_PANEL_OPEN = "ATT_PANEL_OPEN", 
    skGraphEditorTutorial.prototype.ATT_PANEL_CLOSED = "ATT_PANEL_CLOSED", skGraphEditorTutorial.prototype.NAVIGATE = "NAVIGATE", 
    skGraphEditorTutorial.prototype.LINE_RESIZE_LESSONLEARNT = "LINE_RESIZE_LESSONLEARNT", 
    skGraphEditorTutorial.prototype.BOX_SELECTED = "BOX_SELECTED", skGraphEditorTutorial.prototype.currentStep = 1, 
    skGraphEditorTutorial.prototype.dataForNextStep = null, skGraphEditorTutorial.prototype.callback = null, 
    skGraphEditorTutorial.prototype.expectedCode = null, skGraphEditorTutorial.prototype.justWaitForNextAction = !1, 
    skGraphEditorTutorial.prototype.randomActionDone = null, skGraphEditorTutorial.prototype.randomActionCounter = 0, 
    skGraphEditorTutorial.prototype.icon_for_hint_box_position = null, skGraphEditorTutorial.prototype.updateHintBox = function(refObject, text, direction, continueButton, callback) {
        skShell.hint.hey(refObject, text, direction, continueButton, callback);
    }, skGraphEditorTutorial.prototype.displayWellDoneMessage = function($element, text) {
        skShell.wellDoneMessage.hey($element, text);
    }, skGraphEditorTutorial.prototype.hideHint = function() {
        skShell.hint.hide();
    }, skGraphEditorTutorial.prototype.goToNextStep = function() {
        this.currentStep++, this.goToStep(this.currentStep);
    }, skGraphEditorTutorial.prototype.stepDone = function(codeSentByEvent, args) {
        this.justWaitForNextAction ? (this.hideHint(), this.dataForNextStep = args || [], 
        3 == this.randomActionCounter ? (this.goToStep(codeSentByEvent), this.randomActionCounter = 0) : this.randomActionCounter++) : (this.expectedCode === this.any || this.expectedCode === codeSentByEvent) && (this.hideHint(), 
        args && args.cell instanceof mxCell ? this.displayWellDoneMessage(args.cell) : this.displayWellDoneMessage(window), 
        this.dataForNextStep = args || [], this.callback ? this.callback(args) : this.currentStep > 0 && this.goToNextStep(), 
        ga && ga("send", "event", "SkoreAction", "tutorial", "nextStep", this.currentStep));
    }, skGraphEditorTutorial.prototype.goToStep = function(stepToGoTo) {
        var model, cellFound, cell, currentCell, text, that = this;
        switch (this.currentStep = stepToGoTo, stepToGoTo) {
          case 1:
            if ("#create" !== $(".skNavTabs .active").find("a").attr("href")) {
                text = {
                    title: "Go to the Create tab",
                    content: [ "to start adding elements on the canvas" ]
                };
                var createTab = $(".skNavTabs").find("a[href=#create]");
                createTab.one("click", function() {
                    skShell.tutorial.stepDone(skShell.tutorial.CREATE_TAB_OPENED);
                }), this.updateHintBox(createTab, text, "left"), this.expectedCode = this.CREATE_TAB_OPENED;
            } else this.goToNextStep();
            break;

          case 2:
            text = {
                title: "add a box to the canvas",
                content: [ "Drag a box, drop it on the canvas" ]
            }, this.updateHintBox($(".cardshoe-box-wb").parent(), text, "left"), this.expectedCode = this.BOX_DROPPED_FROM_CARDSHOE;
            break;

          case 3:
            void 0 !== this.graph.cellEditor.editingCell ? this.goToNextStep() : void 0 !== this.graph.getSelectionCell() && (cellFound = null, 
            model = this.graph.getModel(), model.filterDescendants(function(cell) {
                cell.isWhatbox() && (cellFound = cell);
            }), cellFound && (this.dataForNextStep = cellFound, this.updateHintBox($(cellFound), {
                title: "This is a whatobx",
                content: [ "A Whatbox describes WHAT needs to be done", "Click to edit the description" ]
            }, "bottom"), this.expectedCode = this.BOX_ENTERED_EDIT_MODE));
            break;

          case 4:
            this.dataForNextStep.cell = this.graph.getSelectionCell(), this.dataForNextStep.firstInput = this.graph.cellEditor.firstInput.inputField, 
            this.updateHintBox(this.dataForNextStep.cell, {
                title: "Describe the activity",
                content: [ "Look at the example", $("<div>Press <kbd>Tab&nbsp;⇥</kbd> to continue</div>") ]
            }, "bottom"), this.dataForNextStep.firstInput.value = "Make a list of images needed for website", 
            this.expectedCode = this.BOX_TAB_PRESSED;
            break;

          case 5:
            var field = this.graph.cellEditor.currentInputs[this.graph.cellEditor.currentInputs.length - 1].inputField;
            this.updateHintBox(field, {
                title: "Who is responsible for that?",
                content: [ "Look at the example", $("<div>Press <kbd>Tab&nbsp;⇥</kbd> or <kbd>Enter&nbsp;↩</kbd> to continue</div>") ]
            }, "bottom"), $(field).prop("disabled", !1).val("Marketing coordinator"), $(field).focus(function() {
                var _this = this;
                setTimeout(function() {
                    _this.select();
                }, 10);
            }), this.expectedCode = this.STOP_EDITING, this.dataForNextStep.cell = this.graph.getSelectionCell();
            break;

          case 6:
            var firstYbox = [];
            currentCell = this.dataForNextStep.cell ? this.dataForNextStep.cell : this.graph.getSelectionCell(), 
            $(currentCell.edges).each(function(i, edge) {
                return edge.target.isWhybox() ? (firstYbox.push(edge.target), !1) : void 0;
            }), firstYbox.length > 0 ? (this.dataForNextStep.cell = firstYbox[0], this.goToNextStep()) : (this.expectedCode = this.NEXT_Y_BOX_IS_CREATED, 
            this.updateHintBox(currentCell, {
                title: "Create a Whybox to continue...",
                content: [ $("<span>Drag <span class='fa-stack' style='color:orange' ><i class='fa fa-circle-thin fa-stack-2x'></i><i class='fa fa-circle fa-stack-1x'></i></span>then drop close by,<br/>or press <kbd>Space</kbd></span>") ]
            }, "bottom"));
            break;

          case 7:
            if (this.graph.editingCell) this.dataForNextStep.cell = this.graph.editingCell, 
            this.goToNextStep(); else {
                this.updateHintBox(this.dataForNextStep.cell, {
                    title: "once the activity is done",
                    content: [ "Edit the state", $("<div>Click, press <kbd>Enter&nbsp;↩</kbd><br/>or <kbd>F2</kbd></div>") ]
                }, "bottom");
                var g = this.graph;
                setTimeout(function() {
                    g.setSelectionCell(that.dataForNextStep.cell);
                }, 100), this.expectedCode = this.BOX_ENTERED_EDIT_MODE;
            }
            break;

          case 8:
            this.updateHintBox($(this.graph.cellEditor.firstInput.inputField), {
                title: "Why is this done?",
                content: [ $("<span>Expected state once the activity is completed.</span>"), $("<div>Press <kbd>Tab&nbsp;⇥</kbd> or <kbd>Enter&nbsp;↩</kbd> to validate</div>") ]
            }, "left"), this.graph.cellEditor.firstInput.inputField.value = "list shared with marketing agency for feedback", 
            this.dataForNextStep.cell = this.graph.cellEditor.editingCell, this.expectedCode = this.BOX_TAB_PRESSED;
            break;

          case 9:
            if (cell = [], $(this.dataForNextStep.cell.edges).each(function(i, edge) {
                return edge.target.isVertex() ? (cell.push(edge.target), !1) : void 0;
            }), cell.length > 0) {
                this.dataForNextStep.cell = cell[0];
                var t = this;
                setTimeout(function() {
                    t.graph.setSelectionCell(t.dataForNextStep.cell);
                }, 100);
            }
            this.updateHintBox(this.dataForNextStep.cell, {
                title: "To create more boxes...",
                content: [ $("<div>Drag the circle <span class='fa-stack' style='color:orange' ><i class='fa fa-circle-thin fa-stack-2x'></i><i class='fa fa-circle fa-stack-1x'></i></span>and drop it close by...</div>") ]
            }, "bottom"), this.expectedCode = this.BOX_CREATED_WITH_DND_FROM_CONNECT_ARROW;
            break;

          case 10:
            mxClient.IS_TOUCH || (text = {
                title: "Want to be faster?",
                content: [ $("<div>Select a box and press <kbd>Space</kbd> to create more...</div>") ]
            }, this.updateHintBox(this.dataForNextStep.cell, text, "bottom"), this.expectedCode = this.BOX_CREATED_WITH_SPACE_BAR);
            break;

          case 11:
            this.updateHintBox($(".skNavTabs").find("a[href=#learn]"), {
                title: "Keep going...",
                content: [ "You can load examples from the learn tab..." ]
            }, "left", !0), this.justWaitForNextAction = !0, this.expectedCode = "any";
            break;

          case this.BOX_CREATED_WITH_SPACE_BAR:
            -1 == this.randomActionDone.indexOf(this.BOX_CREATED_WITH_SPACE_BAR) && (text = {
                title: "Oh, you like speed ?",
                content: [ $("<div>Learn more keyboard shortcuts</div>") ]
            }, this.updateHintBox($(".skFooter_Kbd"), text, "top", !0, function() {
                $(".skHint").position({
                    my: "middle bottom-10",
                    at: "left top",
                    of: $(".skFooter_Kbd")
                });
            }), this.randomActionDone.push(this.BOX_CREATED_WITH_SPACE_BAR));
            break;

          case this.ATT_PANEL_OPEN:
            -1 == this.randomActionDone.indexOf(this.ATT_PANEL_OPEN) && ($(".skAttPanelTutorial").show(), 
            this.expectedCode = this.any, this.randomActionDone.push(this.ATT_PANEL_OPEN));
            break;

          case this.ATT_PANEL_CLOSED:
            -1 == this.randomActionDone.indexOf(this.ATT_PANEL_CLOSED) && (this.expectedCode = this.any, 
            this.randomActionDone.push(this.ATT_PANEL_CLOSED));
            break;

          case this.BOX_SELECTED:
            if (-1 == this.randomActionDone.indexOf(this.BOX_SELECTED + "DETAILEDVIEW")) {
                this.updateHintBox(this.dataForNextStep.cell, {
                    title: "Need to detail a step?",
                    content: [ $("<div>Hover over box & click on the <span aria-hidden='true' class='fa fa-toggle-down'></span> to enter</div>") ]
                }, "left"), this.expectedCode = this.NAVIGATE, this.randomActionDone.push(this.BOX_SELECTED + "DETAILEDVIEW");
                break;
            }
            if (-1 == this.randomActionDone.indexOf(this.BOX_SELECTED + "_ADDNOTE")) {
                this.updateHintBox($(".CSSN"), {
                    title: "Need to write notes?",
                    content: [ "Try adding one on the canvas" ]
                }, "bottom"), this.expectedCode = this.any, this.randomActionDone.push(this.BOX_SELECTED + "_ADDNOTE");
                break;
            }
        }
    }, skGraphEditorTutorial;
});
define('skModal/skModal_Splash',['require','text!skModal/skModal_Splash.html','jquery','skGraphEditorUI/skGraphEditorTutorial','skShell'],function(require) {
    "use strict";
    function skModal_Splash(navigatorUI) {
        var tutKey = "DONTSHOWAGAINtutorial";
        (localStorage.hasOwnProperty(tutKey) === !1 || parseInt(localStorage.getItem(tutKey), 10) === !1) && window.setTimeout(function() {
            var m = $(require("text!skModal/skModal_Splash.html"));
            $(document.body).append(m), $("#skModal_Splash").modal("show"), $(".skStartTutorial", m).on("click", function() {
                $("#skModal_Splash").modal("hide"), skShell.tutorial = new skGraphEditorTutorial(navigatorUI), 
                ga && ga("send", "event", "SkoreAction", "welcomeScreen", "Start Tutorial");
            }), $(".skCloseForNow", m).on("click", function() {
                $("#skModal_Splash").modal("hide"), window.localStorage.setItem(tutKey, !0);
            });
        }, 2e3);
    }
    var $ = require("jquery"), skGraphEditorTutorial = require("skGraphEditorUI/skGraphEditorTutorial"), skShell = require("skShell");
    return skModal_Splash;
});
define('text!skModal/skModal_Print.html',[],function () { return '<div class="modal fade" id="skModal_Print" tabindex="-1" role="dialog" aria-labelledby="skModalPrintLabel" aria-hidden="true">\n\t<div class="modal-dialog modal-lg">\n\t\t<div class="modal-content">\n\t\t\t<div class="modal-header">\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\t\t\t\t<h4 class="modal-title" id="skModalPrintLabel">Print</h4>\n\t\t\t</div>\n\t\t\t<div class="modal-body">\n\n\t\t\t\t<div class="container-fluid">\n\t\t\t\t\t<form class="form-horizontal">\n\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t<label for="skPrintWhat" class="col-sm-2 control-label">Print</label>\n\t\t\t\t\t\t\t<div class="col-sm-10 ">\n\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t<input id="skPrintWhatProcess" type="checkbox" checked> Processes\n\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t<input id="skPrintWhatAtt" type="checkbox"> Attachments\n\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<fieldset id=\'skPageSettings\'>\n\t\t\t\t\t\t</fieldset>\n\t\t\t\t\t\t<fieldset id=\'skPosterSettings\'>\n\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t<label for="skPaperPosterNbPages" class="col-sm-2 control-label">Print process pages</label>\n\t\t\t\t\t\t\t\t<div class="col-sm-10">\n\t\t\t\t\t\t\t\t\t<div class="radio">\n\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t<input name="skPrintPage" class="selectionAllowed skPageAsIS" type="radio" checked>\n\t\t\t\t\t\t\t\t\t\t\tAs page view\n\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class="radio">\n\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t<input name="skPrintPage" class="selectionAllowed skPageFitOne" type="radio">\n\t\t\t\t\t\t\t\t\t\t\tFit all on one page\n\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class="radio">\n\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t<input name="skPrintPage" class="selectionAllowed skPagePoster" type="radio">\n\t\t\t\t\t\t\t\t\t\t\tPoster Print\n\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t\t\t\t<input type="number" class="selectionAllowed form-control" id="skPaperPosterNbPages" value="1" min="1" size="6" disabled>\n\t\t\t\t\t\t\t\t\t\t<div class="input-group-addon">pages</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</fieldset>\n\t\t\t\t\t</form>\n\t\t\t\t</div>\n\t\t\t\t<fieldset id=\'skPrintScopeSet\' >\n\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t<label class="col-sm-2 control-label" style="text-align:right;">\n\t\t\t\t\t\t\tScope\n\t\t\t\t\t\t</label>\n\t\t\t\t\t\t<div class="col-sm-10">\n\t\t\t\t\t\t\t<div class="radio">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type="radio" name="skPrintScope" id="skPrintScope1" value="skCurrentLevel" checked>\n\t\t\t\t\t\t\t\t\tThis level only\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class="radio">\n\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t<input type="radio" name="skPrintScope" id="skPrintScope2" value="skAllLevels">\n\t\t\t\t\t\t\t\t\tThis level and detailed views\n\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</fieldset>\n\t\t\t</div>\n\t\t\t<div class="modal-footer">\n\n\t\t\t\t<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\n\t\t\t\t<button type="button" class="btn btn-primary skModalPrintPreview">Preview &amp; print</button>\n\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n';});
define('skUtils',['require','mxUtils','skConstants','marked','jquery'],function(require) {
    "use strict";
    function isDisplayRoot(model, cell) {
        return !cell.isGroup() || model.isLayer(cell);
    }
    function findRelationship(model, cellA, cellB) {
        var numOfValidRootsBetweenCells, tmp, root = model.getRoot();
        if (cellA == cellB) return skConstants.REL_EQUAL;
        if (isDisplayRoot(model, cellA)) {
            for (numOfValidRootsBetweenCells = 0, tmp = cellB.parent; null != tmp && tmp != cellA; ) isDisplayRoot(model, tmp) && (numOfValidRootsBetweenCells += 1), 
            tmp = tmp.parent;
            if (tmp == cellA) return root == cellA && (numOfValidRootsBetweenCells -= 1), 0 >= numOfValidRootsBetweenCells ? skConstants.REL_VISIBLE : 1 === numOfValidRootsBetweenCells ? skConstants.REL_CHILD : skConstants.REL_GRD_CHILD;
        }
        if (isDisplayRoot(model, cellB)) {
            for (numOfValidRootsBetweenCells = 0, tmp = cellA.parent; null != tmp && tmp != cellB; ) isDisplayRoot(model, tmp) && (numOfValidRootsBetweenCells += 1), 
            tmp = tmp.parent;
            if (tmp == cellB) return root == cellB && (numOfValidRootsBetweenCells -= 1), 0 >= numOfValidRootsBetweenCells ? skConstants.REL_PARENT : skConstants.REL_GRD_PARENT;
        }
        return skConstants.REL_NONE;
    }
    function setCellsStyleName(model, cells, stylename) {
        if (null != cells && cells.length > 0) {
            model.beginUpdate();
            try {
                for (var i = 0; i < cells.length; i++) if (null != cells[i]) {
                    var style = model.getStyle(cells[i]);
                    style = mxUtils.removeAllStylenames(style), stylename && (style = 0 === style.length ? stylename : stylename + ";" + style), 
                    model.setStyle(cells[i], style);
                }
            } finally {
                model.endUpdate();
            }
        }
    }
    function zapGremlins(text) {
        for (var checked = [], i = 0; i < text.length; i++) {
            var code = text.charCodeAt(i);
            (code >= 32 || 9 == code || 10 == code || 13 == code) && checked.push(text.charAt(i));
        }
        return checked.join("");
    }
    function reflowStyleCss() {
        var style = document.createElement("style");
        document.body.appendChild(style), document.body.removeChild(style);
    }
    function cutTheFatNow(text) {
        return text.replace(/<a\b[^>]*>/g, "&lt;").replace(/<\/a>/g, "&gt;").replace(/<p\b[^>]*>/g, "").replace(/<\/p>/g, " ").replace(/<h[0-9]*\b[^>]*>/g, "").replace(/<\/h[0-9]*>/g, " ").replace(/fa-lg|fa-[0-9]x/g, "").replace(/<ul\b[^>]*>/g, "").replace(/<\/ul>/g, " ").replace(/<li\b[^>]*>/g, "").replace(/<\/li>/g, " ").replace(/<ol\b[^>]*>/g, "").replace(/<\/ol>/g, " ").replace("<hr>", "---").replace(/\n/g, " ");
    }
    function renderMarkdown(text, cutTheFat) {
        return cutTheFat = void 0 !== cutTheFat ? cutTheFat : !1, " " !== text && (text = marked(text, {
            renderer: renderer
        }), cutTheFat && (text = cutTheFatNow(text))), text;
    }
    function readValue(node, attribute) {
        return attribute = attribute || skConstants.VALUE, null == node ? "" : "string" == typeof node ? node : node.tagName ? node.getAttribute(attribute) || "" : node.length > 0 ? node[0].getAttribute(attribute) || "" : "";
    }
    function openURL(url) {
        window.open(url);
    }
    function unHtmlEntities(s, newline) {
        return s = s || "", s = s.replace("/&amp;/g", "&"), s = s.replace("/&quot;/g", '"'), 
        s = s.replace("/&#39;/g", "'"), s = s.replace("/&lt;/g", "<"), s = s.replace("/&gt;/g", ">"), 
        (null === newline || newline) && (s = s.replace("/&#xa;/g", "\n")), s;
    }
    function extend() {
        for (var i = 1; i < arguments.length; i++) for (var key in arguments[i]) arguments[i].hasOwnProperty(key) && (arguments[0][key] = arguments[i][key]);
        return arguments[0];
    }
    function isValidHtmlColor(color) {
        var div = document.createElement("div");
        return div.style.borderColor = "", div.style.borderColor = color, 0 === div.style.borderColor.length ? !1 : !0;
    }
    function debounce(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this, args = arguments, later = function() {
                timeout = null, immediate || func.apply(context, args);
            }, callNow = immediate && !timeout;
            clearTimeout(timeout), timeout = setTimeout(later, wait), callNow && func.apply(context, args);
        };
    }
    function makeExpandingArea(containers) {
        $(containers).each(function(i, container) {
            var area = container.querySelector("textarea"), span = container.querySelector("span");
            area.addEventListener ? (area.addEventListener("input", function() {
                span.textContent = area.value;
            }, !1), span.textContent = area.value) : area.attachEvent && (area.attachEvent("onpropertychange", function() {
                span.innerText = area.value;
            }), span.innerText = area.value), container.className += " active ";
        });
    }
    function isEmptyString(str) {
        return "undefined" != typeof str && str && 0 !== str.length && "" !== str && /[^\s]/.test(str) && !/^\s*$/.test(str) && "" !== str.replace(/\s/g, "") ? !1 : !0;
    }
    function getObjects(obj, key, val) {
        var objects = [];
        for (var i in obj) obj.hasOwnProperty(i) && ("object" == typeof obj[i] ? objects = objects.concat(getObjects(obj[i], key, val)) : i == key && obj[key] == val && objects.push(obj));
        return objects;
    }
    function createBoxElement(boxType) {
        var type = mxUtils.createXmlDocument().createElement(boxType), box = mxUtils.createXmlDocument().createElement(skConstants.BOX);
        if (type.appendChild(box), boxType === skConstants.whatbox) {
            var resps = mxUtils.createXmlDocument().createElement(skConstants.RESPONSIBILITIES);
            type.appendChild(resps);
            var resp = mxUtils.createXmlDocument().createElement(skConstants.RESPONSIBILITY);
            resp.setAttribute(skConstants.ATTR_SHOWN, "1"), resps.appendChild(resp), resp.appendChild(mxUtils.createXmlDocument().createElement(skConstants.ROLE));
        }
        return type;
    }
    var mxUtils = require("mxUtils"), skConstants = require("skConstants"), marked = require("marked"), $ = require("jquery"), renderer = new marked.Renderer();
    return renderer.image = function(href, title, text) {
        if (text.startsWith("icon")) {
            var classText = "fa ";
            classText += href.startsWith("l-") ? " lnrf lnr-" + href.substring(2) : " faf fa-" + href;
            var textTmp = text.split(" "), style = "";
            return textTmp.forEach(function(e) {
                "fw" === e || "ul" === e || "li" === e || "border" === e || "pull-right" === e || "pull-left" === e || "spin" === e || "pulse" === e || "lg" === e || "2x" === e || "3x" === e || "4x" === e || "5x" === e || "rotate-90" === e || "rotate-180" === e || "rotate-270" === e || "flip-horizontal" === e || "flip-vertical" === e ? classText += " fa-" + e : e.indexOf("#") >= 0 ? style = ' style="color:' + e + '"' : isValidHtmlColor(e.toLowerCase()) && (style = ' style="color:' + e.toLowerCase() + '"');
            }), '<i class="' + classText + ' "' + style + " ></i>";
        }
        var out = '<img src="' + href + '" alt="' + text + '"';
        return -1 == text.indexOf("noresize") && (out += ' class="img-responsive" '), title && (out += ' title="' + title + '"'), 
        out += ">";
    }, {
        createBoxElement: createBoxElement,
        debounce: debounce,
        extend: extend,
        findRelationship: findRelationship,
        getObjects: getObjects,
        isEmptyString: isEmptyString,
        isValidHtmlColor: isValidHtmlColor,
        makeExpandingArea: makeExpandingArea,
        openURL: openURL,
        readValue: readValue,
        reflowStyleCss: reflowStyleCss,
        renderMarkdown: renderMarkdown,
        setCellsStyleName: setCellsStyleName,
        unHtmlEntities: unHtmlEntities,
        zapGremlins: zapGremlins
    };
});
define('skGraph/skGraphUtils',['require','jquery','skConstants','skUtils','mxRectangle'],function(require) {
    "use strict";
    var $ = require("jquery"), skConstants = require("skConstants"), skUtils = require("skUtils"), mxRectangle = require("mxRectangle"), synth_handover = function(model) {
        var relations = [];
        model.filterDescendants(function(cell) {
            var a = cell.highlightLines(!1);
            void 0 !== a && a.length > 0 && $.each(a, function(i, line) {
                relations.push(line);
            });
        });
        var findRelation = function(array, sourceRes, destRes) {
            var i;
            for (i = 0; i < array.length; i++) if (array[i][0] == sourceRes && array[i][1] == destRes) return array[i];
            return null;
        }, result = [];
        return $.each(relations, function(i, e) {
            var line = findRelation(result, e.sourceResource, e.destinationResource);
            line ? (line[2].push(e.whybox), line[3].push(e.sourceCell), line[4].push(e.destinationCell)) : result.push([ e.sourceResource, e.destinationResource, [ e.whybox ], [ e.sourceCell ], [ e.destinationCell ] ]);
        }), result.sort();
    }, findRelationOpposite = function(array, source, dest) {
        var i;
        for (i = 0; i < array.length; i++) if (array[i][0] == source && array[i][1] == dest) return array[i].done = !0, 
        array[i];
        return null;
    }, getBoxRoles = function(graph, cell) {
        return cell.getBoxRoles();
    }, getBoxRolesString = function(graph, cell, markdown) {
        var tmpRoles = getBoxRoles(graph, cell), tmpRoles2 = tmpRoles.map(function(r) {
            var t = skUtils.readValue(graph.roleManager.getRoleByLocalID(r));
            return markdown ? skUtils.renderMarkdown(t, !0) : t;
        });
        return tmpRoles2.join(skConstants.SEPARATOR);
    }, createCellObject = function(cell) {
        return {
            cell: cell,
            id: cell.id,
            text: cell.getBoxText(!1, !1),
            isGroup: cell.isGroup()
        };
    }, getBreadcrumbs = function(graph) {
        for (var cell = graph.getDefaultParent(), allCells = [ createCellObject(cell) ]; cell.parent && cell.parent != graph.model.getRoot(); ) allCells.unshift(createCellObject(cell.parent)), 
        cell = cell.parent;
        return allCells;
    }, checkInternalLinks = function(graph, html) {
        var warn = !1;
        $(".skGoTo", html).each(function(i, link) {
            var dest = "" + $(link).data("goto"), cellID = dest.match(/\d+/), realCell = graph.model.cells[cellID];
            realCell || (warn = !0, $(link).css({
                "border-bottom": "1px dashed gray",
                display: "inline"
            }), $(link).attr("data-toggle", "tooltip"), $(link).attr("data-placement", "bottom"), 
            $(link).attr("title", "Link within this process is broken (box " + dest + " does not exist)"));
        }), warn && $('[data-toggle="tooltip"]', html).tooltip({
            container: "body"
        });
    }, getAllProcessTextAsHtml = function(graph) {
        var t = "";
        return graph.model.filterDescendants(function(cell) {
            if (!cell.isEdge()) {
                var text = cell.getBoxText();
                return "(unnamed)" !== text && "" !== text.trim() && (t += text + "\n\n", t += getBoxRolesString(graph, cell, !0) + "\n\n", 
                t += cell.getAttachmentString(graph, !0) + "\n\n"), !0;
            }
        }), skUtils.renderMarkdown(t);
    }, getPageSettings = function(graph) {
        for (var paperSizes = skConstants.paperSizes, pageSettings = {
            pageView: graph.pageVisible,
            grid: graph.isGridEnabled(),
            pageScale: graph.pageScale,
            backgroundColor: null == graph.background || "none" == graph.background ? "#ffffff" : graph.background
        }, allPaperSizes = Object.keys(paperSizes), i = 0; i < allPaperSizes.length; i++) {
            var format = allPaperSizes[i];
            graph.pageFormat.width == paperSizes[format].width && graph.pageFormat.height == paperSizes[format].height ? (pageSettings.paperSize = format, 
            pageSettings.landscape = !1, pageSettings.format = new mxRectangle(0, 0, paperSizes[format].width, paperSizes[format].height)) : graph.pageFormat.width == paperSizes[format].height && graph.pageFormat.height == paperSizes[format].width && (pageSettings.paperSize = format, 
            pageSettings.landscape = !0, pageSettings.format = new mxRectangle(0, 0, paperSizes[format].width, paperSizes[format].height));
        }
        return pageSettings;
    }, getPageFormat = function(paperSizeName, landscape) {
        return new mxRectangle(0, 0, landscape ? skConstants.paperSizes[paperSizeName].height : skConstants.paperSizes[paperSizeName].width, landscape ? skConstants.paperSizes[paperSizeName].width : skConstants.paperSizes[paperSizeName].height);
    };
    return {
        synth_handover: synth_handover,
        findRelationOpposite: findRelationOpposite,
        getBoxRolesString: getBoxRolesString,
        checkInternalLinks: checkInternalLinks,
        getBoxRoles: getBoxRoles,
        getBreadcrumbs: getBreadcrumbs,
        getAllProcessTextAsHtml: getAllProcessTextAsHtml,
        getPageFormat: getPageFormat,
        getPageSettings: getPageSettings
    };
});
define('text!skModal/skModal_Settings_PageSetup.html',[],function () { return '<div class="panel panel-default">\n\t<div class="panel-heading">\n\t\t<h3 class="panel-title">Page Setup</h3>\n\t</div>\n\t<div class="panel-body">\n\t\t<form class="form-horizontal" onsubmit="return false">\n\n\t\t\t<!-- pageView -->\n\t\t\t<div class="form-group" data-skparam="pageView">\n\t\t\t\t<label for="skPageView" class="col-sm-4 control-label">Page View</label>\n\t\t\t\t<div class="col-sm-8 ">\n\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t<input id="skPageView" type="checkbox" >\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<!-- grid -->\n\n\t\t\t<div class="form-group" data-skparam="grid">\n\t\t\t\t<label for="skGrid" class="col-sm-4 control-label">Grid</label>\n\t\t\t\t<div class="col-sm-8 ">\n\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t<input id="skGrid" type="checkbox" >\n\t\t\t\t\t\t</label>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<!-- page scale -->\n\n\t\t\t<fieldset data-skparam="pageScale">\n\t\t\t\t<div class="form-group" >\n\t\t\t\t\t<label for="skPageScaleInput" class="col-sm-4 control-label">Page Scale</label>\n\t\t\t\t\t<div class="col-sm-8">\n\t\t\t\t\t\t<div class="input-group input-group-sm">\n\t\t\t\t\t\t\t<span class="input-group-btn">\n\t\t\t\t\t\t\t\t<button class="skPageScaleBtn skMinus btn btn-default" type="button">-</button>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t<input class="form-control" type="text" id="skPageScaleInput" readonly value="\n\t\t\t\t\t\t\tarseInt( graph.pageScale * 100, 10\n\t\t\t\t\t\t\t%">\n\t\t\t\t\t\t\t<span class="input-group-btn">\n\t\t\t\t\t\t\t\t<button class="skPageScaleBtn skPlus btn btn-default" type="button">+</button>\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<span id="helpBlock" class="help-block">The higher the more boxes on a page.</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</fieldset>\n\n\t\t\t<!-- paper size -->\n\n\t\t\t<fieldset data-skparam="paperSize">\n\t\t\t\t<div class="form-group" >\n\t\t\t\t\t<label for="skPaperSizeSelect" class="col-sm-4 control-label">Paper size</label>\n\t\t\t\t\t<div class="col-sm-8">\n\t\t\t\t\t\t<select class="selectionAllowed form-control input-sm" id="skPaperSizeSelect">\n\t\t\t\t\t\t</select>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</fieldset>\n\n\t\t\t<!-- landscape -->\n\n\t\t\t<fieldset data-skparam="landscape">\n\t\t\t\t<div class="form-group" >\n\t\t\t\t\t<label for="skLandscapeCheckBox" class="col-sm-4 control-label">Landscape</label>\n\t\t\t\t\t<div class="col-sm-8 ">\n\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<input id="skLandscapeCheckBox" type="checkbox">\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</fieldset>\n\n\t\t\t<!-- background color -->\n\n\t\t\t<div class="form-group" data-skparam="backgroundColor">\n\t\t\t\t<label for="skPageColor" class="col-sm-4 control-label">Page background color</label>\n\t\t\t\t<div class="col-sm-8">\n\t\t\t\t\t<div class="input-group input-group-sm">\n\t\t\t\t\t\t<input class="form-control selectionAllowed" type="text" id="skPageColor" value="white">\n\t\t\t\t\t\t<span class="input-group-btn">\n\t\t\t\t\t\t\t<button class="skApplyColor btn btn-default" type="button">Apply</button>\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</form>\n\t</div>\n</div>\n';});
!function(root, factory) {
    "use strict";
    "function" == typeof define && define.amd ? define('lib/bootstrap3-typeahead',[ "jquery" ], function($) {
        return factory($);
    }) : factory(root.jQuery);
}(this, function($) {
    "use strict";
    var Typeahead = function(element, options) {
        this.$element = $(element), this.options = $.extend({}, $.fn.typeahead.defaults, options), 
        this.matcher = this.options.matcher || this.matcher, this.sorter = this.options.sorter || this.sorter, 
        this.select = this.options.select || this.select, this.autoSelect = "boolean" == typeof this.options.autoSelect ? this.options.autoSelect : !0, 
        this.highlighter = this.options.highlighter || this.highlighter, this.render = this.options.render || this.render, 
        this.updater = this.options.updater || this.updater, this.displayText = this.options.displayText || this.displayText, 
        this.source = this.options.source, this.delay = this.options.delay, this.$menu = $(this.options.menu), 
        this.$appendTo = this.options.appendTo ? $(this.options.appendTo) : null, this.shown = !1, 
        this.listen(), this.showHintOnFocus = "boolean" == typeof this.options.showHintOnFocus ? this.options.showHintOnFocus : !1, 
        this.afterSelect = this.options.afterSelect, this.addItem = !1;
    };
    Typeahead.prototype = {
        constructor: Typeahead,
        select: function() {
            var val = this.$menu.find(".active").data("value");
            if (this.$element.data("active", val), this.autoSelect || val) {
                var newVal = this.updater(val);
                newVal || (newVal = ""), this.$element.val(this.displayText(newVal) || newVal).change(), 
                this.afterSelect(newVal);
            }
            return this.hide();
        },
        updater: function(item) {
            return item;
        },
        setSource: function(source) {
            this.source = source;
        },
        show: function() {
            var scrollHeight, pos = $.extend({}, this.$element.position(), {
                height: this.$element[0].offsetHeight
            });
            scrollHeight = "function" == typeof this.options.scrollHeight ? this.options.scrollHeight.call() : this.options.scrollHeight;
            var element;
            return element = this.shown ? this.$menu : this.$appendTo ? this.$menu.appendTo(this.$appendTo) : this.$menu.insertAfter(this.$element), 
            element.css({
                top: pos.top + pos.height + scrollHeight,
                left: pos.left
            }).show(), this.shown = !0, this;
        },
        hide: function() {
            return this.$menu.hide(), this.shown = !1, this;
        },
        lookup: function(query) {
            if ("undefined" != typeof query && null !== query ? this.query = query : this.query = this.$element.val() || "", 
            this.query.length < this.options.minLength && !this.options.showHintOnFocus) return this.shown ? this.hide() : this;
            var worker = $.proxy(function() {
                $.isFunction(this.source) ? this.source(this.query, $.proxy(this.process, this)) : this.source && this.process(this.source);
            }, this);
            clearTimeout(this.lookupWorker), this.lookupWorker = setTimeout(worker, this.delay);
        },
        process: function(items) {
            var that = this;
            return items = $.grep(items, function(item) {
                return that.matcher(item);
            }), items = this.sorter(items), items.length || this.options.addItem ? (items.length > 0 ? this.$element.data("active", items[0]) : this.$element.data("active", null), 
            this.options.addItem && items.push(this.options.addItem), "all" == this.options.items ? this.render(items).show() : this.render(items.slice(0, this.options.items)).show()) : this.shown ? this.hide() : this;
        },
        matcher: function(item) {
            var it = this.displayText(item);
            return ~it.toLowerCase().indexOf(this.query.toLowerCase());
        },
        sorter: function(items) {
            for (var item, beginswith = [], caseSensitive = [], caseInsensitive = []; item = items.shift(); ) {
                var it = this.displayText(item);
                it.toLowerCase().indexOf(this.query.toLowerCase()) ? ~it.indexOf(this.query) ? caseSensitive.push(item) : caseInsensitive.push(item) : beginswith.push(item);
            }
            return beginswith.concat(caseSensitive, caseInsensitive);
        },
        highlighter: function(item) {
            var len, leftPart, middlePart, rightPart, strong, html = $("<div></div>"), query = this.query, i = item.toLowerCase().indexOf(query.toLowerCase());
            if (len = query.length, 0 === len) return html.text(item).html();
            for (;i > -1; ) leftPart = item.substr(0, i), middlePart = item.substr(i, len), 
            rightPart = item.substr(i + len), strong = $("<strong></strong>").text(middlePart), 
            html.append(document.createTextNode(leftPart)).append(strong), item = rightPart, 
            i = item.toLowerCase().indexOf(query.toLowerCase());
            return html.append(document.createTextNode(item)).html();
        },
        render: function(items) {
            var that = this, self = this, activeFound = !1, data = [], _category = that.options.separator;
            return $.each(items, function(key, value) {
                key > 0 && value[_category] !== items[key - 1][_category] && data.push({
                    __type: "divider"
                }), !value[_category] || 0 !== key && value[_category] === items[key - 1][_category] || data.push({
                    __type: "category",
                    name: value[_category]
                }), data.push(value);
            }), items = $(data).map(function(i, item) {
                if ("category" == (item.__type || !1)) return $(that.options.headerHtml).text(item.name)[0];
                if ("divider" == (item.__type || !1)) return $(that.options.headerDivider)[0];
                var text = self.displayText(item);
                return i = $(that.options.item).data("value", item), i.find("a").html(that.highlighter(text, item)), 
                item.htmlcode && i.css({
                    "background-color": item.htmlcode
                }), text == self.$element.val() && (i.addClass("active"), self.$element.data("active", item), 
                activeFound = !0), i[0];
            }), this.autoSelect && !activeFound && (items.filter(":not(.dropdown-header)").first().addClass("active"), 
            this.$element.data("active", items.first().data("value"))), this.$menu.html(items), 
            this;
        },
        displayText: function(item) {
            return "undefined" != typeof item && "undefined" != typeof item.name && item.name || item;
        },
        next: function(event) {
            var active = this.$menu.find(".active").removeClass("active"), next = active.next();
            next.length || (next = $(this.$menu.find("li")[0])), next.addClass("active");
        },
        prev: function(event) {
            var active = this.$menu.find(".active").removeClass("active"), prev = active.prev();
            prev.length || (prev = this.$menu.find("li").last()), prev.addClass("active");
        },
        listen: function() {
            this.$element.on("focus", $.proxy(this.focus, this)).on("blur", $.proxy(this.blur, this)).on("keypress", $.proxy(this.keypress, this)).on("input", $.proxy(this.input, this)).on("keyup", $.proxy(this.keyup, this)), 
            this.eventSupported("keydown") && this.$element.on("keydown", $.proxy(this.keydown, this)), 
            this.$menu.on("click", $.proxy(this.click, this)).on("mouseenter", "li", $.proxy(this.mouseenter, this)).on("mouseleave", "li", $.proxy(this.mouseleave, this));
        },
        destroy: function() {
            this.$element.data("typeahead", null), this.$element.data("active", null), this.$element.off("focus").off("blur").off("keypress").off("input").off("keyup"), 
            this.eventSupported("keydown") && this.$element.off("keydown"), this.$menu.remove();
        },
        eventSupported: function(eventName) {
            var isSupported = eventName in this.$element;
            return isSupported || (this.$element.setAttribute(eventName, "return;"), isSupported = "function" == typeof this.$element[eventName]), 
            isSupported;
        },
        move: function(e) {
            if (this.shown) switch (e.keyCode) {
              case 9:
              case 13:
              case 27:
                e.preventDefault();
                break;

              case 38:
                if (e.shiftKey) return;
                e.preventDefault(), this.prev();
                break;

              case 40:
                if (e.shiftKey) return;
                e.preventDefault(), this.next();
            }
        },
        keydown: function(e) {
            this.suppressKeyPressRepeat = ~$.inArray(e.keyCode, [ 40, 38, 9, 13, 27 ]), this.shown || 40 != e.keyCode ? this.move(e) : this.lookup();
        },
        keypress: function(e) {
            this.suppressKeyPressRepeat || this.move(e);
        },
        input: function(e) {
            this.lookup(), e.preventDefault();
        },
        keyup: function(e) {
            switch (e.keyCode) {
              case 40:
              case 38:
              case 16:
              case 17:
              case 18:
                break;

              case 9:
              case 13:
                if (!this.shown) return;
                this.select();
                break;

              case 27:
                if (!this.shown) return;
                this.hide();
            }
            e.preventDefault();
        },
        focus: function(e) {
            this.focused || (this.focused = !0, this.options.showHintOnFocus && this.lookup(""));
        },
        blur: function(e) {
            this.focused = !1, !this.mousedover && this.shown && this.hide();
        },
        click: function(e) {
            e.preventDefault(), this.select(), this.$element.focus(), this.hide();
        },
        mouseenter: function(e) {
            this.mousedover = !0, this.$menu.find(".active").removeClass("active"), $(e.currentTarget).addClass("active");
        },
        mouseleave: function(e) {
            this.mousedover = !1, !this.focused && this.shown && this.hide();
        }
    };
    var old = $.fn.typeahead;
    $.fn.typeahead = function(option) {
        var arg = arguments;
        return "string" == typeof option && "getActive" == option ? this.data("active") : this.each(function() {
            var $this = $(this), data = $this.data("typeahead"), options = "object" == typeof option && option;
            data || $this.data("typeahead", data = new Typeahead(this, options)), "string" == typeof option && data[option] && (arg.length > 1 ? data[option].apply(data, Array.prototype.slice.call(arg, 1)) : data[option]());
        });
    }, $.fn.typeahead.defaults = {
        source: [],
        items: 8,
        menu: '<ul class="typeahead dropdown-menu" role="listbox"></ul>',
        item: '<li><a class="dropdown-item" href="#" role="option"></a></li>',
        minLength: 1,
        scrollHeight: 0,
        autoSelect: !0,
        afterSelect: $.noop,
        addItem: !1,
        delay: 0,
        separator: "category",
        headerHtml: '<li class="dropdown-header"></li>',
        headerDivider: '<li class="divider" role="separator"></li>'
    }, $.fn.typeahead.Constructor = Typeahead, $.fn.typeahead.noConflict = function() {
        return $.fn.typeahead = old, this;
    }, $(document).on("focus.typeahead.data-api", '[data-provide="typeahead"]', function(e) {
        var $this = $(this);
        $this.data("typeahead") || $this.typeahead($this.data());
    });
});
define('skModal/skModal_Settings_Pagesetup',[ "require", "jquery", "colors", "mxEventObject", "mxUtils", "skShell", "skGraph/skGraphUtils", "skConstants", "text!skModal/skModal_Settings_PageSetup.html", "lib/bootstrap3-typeahead" ], function(require, $, colors, mxEventObject, mxUtils, skShell, skGraphUtils, skConstants, html) {
    "use strict";
    function skSettings_pagesetup(formParams, pageSettings, graph, callback) {
        pageSettings = pageSettings || skGraphUtils.getPageSettings(graph);
        var div = $(html), pageViewForm = $("[data-skparam='pageView']", div);
        pageViewForm.toggle(!formParams || formParams && formParams.pageView), $("input", pageViewForm).attr("checked", pageSettings.grid).on("change", function() {
            callback({
                pageView: $(this).is(":checked")
            }), enableOtherPageSettings($(this).is(":checked"));
        });
        var enableOtherPageSettings = function(enabled) {
            $(pageScaleForm).attr("disabled", enabled ? null : "disabled"), $(paperSizeForm).attr("disabled", enabled ? null : "disabled"), 
            $(landscapeForm).attr("disabled", enabled ? null : "disabled");
        };
        enableOtherPageSettings(pageSettings.pageViewForm);
        var gridForm = $("[data-skparam='grid']", div);
        gridForm.toggle(!formParams || formParams && formParams.grid), $("input", gridForm).attr("checked", pageSettings.grid).on("change", function() {
            callback({
                grid: $(this).is(":checked")
            });
        });
        var pageScaleForm = $("[data-skparam='pageScale']", div);
        pageScaleForm.toggle(!formParams || formParams && formParams.pageScale);
        var scale = parseInt(100 * pageSettings.pageScale, 10);
        $("input", pageScaleForm).val(scale + " %"), $(".skPageScaleBtn", pageScaleForm).on("click", function(event) {
            scale += $(event.target).hasClass("skMinus") ? -10 : 10, $("input", pageScaleForm).val("" + scale + " %"), 
            callback({
                pageScale: scale / 100
            });
        });
        var paperSizeForm = $("[data-skparam='paperSize']", div);
        paperSizeForm.toggle(!formParams || formParams && formParams.paperSize);
        for (var paperSizeInput = $("select", paperSizeForm), i = 0; i < Object.keys(skConstants.paperSizes).length; i++) {
            var name = Object.keys(skConstants.paperSizes)[i], option = $("<option>" + skConstants.paperSizes[name].displayName + "</option>");
            option.attr("value", skConstants.paperSizes[name].name), option.attr("selected", pageSettings.paperSize === skConstants.paperSizes[name].name ? !0 : !1), 
            paperSizeInput.append(option);
        }
        paperSizeInput.on("change", function() {
            callback({
                paperSize: $(this).val()
            });
        });
        var landscapeForm = $("[data-skparam='landscape']", div);
        landscapeForm.toggle(!formParams || formParams && formParams.landscape), $("input", landscapeForm).attr("checked", pageSettings.landscape), 
        $("input", landscapeForm).on("change", function() {
            callback({
                landscape: $(this).is(":checked")
            });
        });
        var backgroundColorForm = $("[data-skparam='backgroundColor']", div);
        backgroundColorForm.toggle(!formParams || formParams && formParams.backgroundColor);
        var currentBgColor = pageSettings.backgroundColor;
        return currentBgColor.startsWith("#") && colors.some(function(e) {
            return e.htmlcode == currentBgColor.toUpperCase() ? (currentBgColor = e.name, !0) : void 0;
        }), $("input", backgroundColorForm).val(currentBgColor).typeahead({
            autoSelect: !1,
            items: "all",
            minLength: 0,
            source: colors
        }), $("button", backgroundColorForm).on("click", function() {
            callback({
                backgroundColor: $("input", backgroundColorForm).val()
            });
        }), $(div);
    }
    return skSettings_pagesetup;
});
define('skNavigator/skCellHighlighter',['require','mxConstants','mxUtils','skShell'],function(require) {
    "use strict";
    function skCellHighlighter(graph) {
        this.graph = graph;
    }
    var mxConstants = require("mxConstants"), mxUtils = require("mxUtils"), skShell = require("skShell");
    return skCellHighlighter.prototype.previousStyle = null, skCellHighlighter.prototype.currentState = null, 
    skCellHighlighter.prototype.graph = null, skCellHighlighter.prototype.toggleHighlightOn = function(state) {
        this.toggleHighlightOff(), state && state.shape && (this.previousStyle = state.style, 
        this.currentState = state, state && (state.style = mxUtils.clone(state.style), state.style[mxConstants.STYLE_STROKECOLOR] = skShell.colors.BOX_HOVER, 
        state.shape.apply(state), state.shape.redraw()));
    }, skCellHighlighter.prototype.toggleHighlightOff = function(state) {
        if (state = state || this.currentState, state && state.shape) {
            state.style = mxUtils.clone(this.previousStyle);
            var graphS = this.graph.getStylesheet(), s = graphS.getCellStyle(state.cell.style, graphS.getDefaultVertexStyle());
            state.style[mxConstants.STYLE_STROKEWIDTH] = s[mxConstants.STYLE_STROKEWIDTH], state.style[mxConstants.STYLE_STROKECOLOR] = s[mxConstants.STYLE_STROKECOLOR], 
            state.shape.apply(state), state.shape.redraw();
        }
        this.currentState = null, this.previousStyle = null;
    }, skCellHighlighter;
});
define('skGraph/skEdgeSegmentHandler',['require','mxEdgeSegmentHandler','mxUtils'],function(require) {
    "use strict";
    function skEdgeSegmentHandler(state) {
        mxEdgeSegmentHandler.call(this, state);
    }
    var mxEdgeSegmentHandler = require("mxEdgeSegmentHandler"), mxUtils = require("mxUtils");
    return mxUtils.extend(skEdgeSegmentHandler, mxEdgeSegmentHandler), skEdgeSegmentHandler.prototype.connect = function(edge, terminal, isSource, isClone, me) {
        var model = this.graph.getModel();
        model.beginUpdate();
        try {
            edge = mxEdgeSegmentHandler.prototype.connect.apply(this, arguments);
            var geo = model.getGeometry(edge);
            null != geo && (geo = geo.clone(), geo.points = null, model.setGeometry(edge, geo));
        } finally {
            model.endUpdate();
        }
        return edge;
    }, skEdgeSegmentHandler;
});
define('skGraph/skTemplateDescriptions',[],function() {
    "use strict";
    var c = {};
    return c.attachment = {}, c.group = {}, c.role = {}, c.stickynote = {}, c.title = {}, 
    c.image = {}, c.whatbox = {}, c.whybox = {}, c.responsibility = {}, c.attachment.text = {
        name: "text",
        type: "longtext",
        editable: !0,
        sortable: !0,
        defaulttext: "Additional information",
        title: "Text",
        removable: !0,
        multi: {
            addempty: !1,
            addbutton: !1
        }
    }, c.attachment.url = {
        name: "url",
        type: "url",
        sortable: !0,
        title: "Web URL link",
        editable: !0,
        removable: !0,
        multi: {
            addempty: !1,
            addbutton: !1
        }
    }, c.attachment.externalcontent = {
        name: "externalcontent",
        type: "externalcontent",
        title: "External content",
        editable: !0,
        sortable: !0,
        removable: !0,
        multi: {
            addempty: !1,
            addbutton: !1
        }
    }, c.attachment.checklist = {
        name: "checklist",
        type: "checklist",
        title: "Checklist item",
        editable: !0,
        sortable: !0,
        removable: !0,
        multi: {
            addempty: !0,
            addbutton: !0
        }
    }, c.whybox.base = {
        fields: [ {
            name: "text",
            type: "longtext",
            helptext: "formatting",
            defaulttext: "So that?"
        } ]
    }, c.whatbox.base = {
        fields: [ {
            name: "text",
            type: "longtext",
            helptext: "formatting",
            defaulttext: "What happens?"
        }, {
            type: "separator",
            name: "whoWhatLine"
        } ]
    }, c.responsibility.base = {
        editable: !0,
        fields: [ {
            name: "responsibility",
            type: "group",
            removable: !0,
            sortable: !0,
            multi: {
                addbutton: !0,
                addtext: "Add 1 more role",
                addempty: !0
            },
            fields: [ {
                name: "role",
                localid: !0,
                type: "singleline",
                autocomplete: {
                    source: "roles",
                    enrichsource: !0
                },
                defaulttext: "Who does it?"
            } ]
        } ]
    }, c.responsibility.ratsi = {
        fields: [ {
            name: "responsibility",
            type: "group",
            removable: !0,
            multi: {
                addbutton: !0,
                addtext: "Add 1 more role",
                addempty: !0
            },
            fields: [ {
                name: "role",
                localid: !0,
                type: "singleline",
                autocomplete: {
                    source: "roles",
                    enrichsource: !0
                },
                defaulttext: "Who does it?"
            }, {
                name: "tag",
                value: "R",
                type: "button",
                description: "Responsible",
                style: {
                    "background-color": "rgba(255, 165, 0, 0.5)"
                }
            }, {
                name: "tag",
                value: "A",
                type: "button",
                description: "Authority",
                style: {
                    "background-color": "rgba(255, 0, 0, 0.5)"
                }
            }, {
                name: "tag",
                value: "T",
                type: "button",
                description: "Task",
                style: {
                    "background-color": "rgba(245, 245, 220, 0.5)"
                }
            }, {
                name: "tag",
                value: "S",
                type: "button",
                description: "Support"
            }, {
                name: "tag",
                value: "I",
                type: "button",
                description: "Informed"
            } ]
        } ]
    }, c.responsibility.raci = {
        fields: [ {
            name: "responsibility",
            type: "group",
            removable: !0,
            multi: {
                addbutton: !0,
                addtext: "Add 1 more role",
                addempty: !0
            },
            fields: [ {
                name: "role",
                localid: !0,
                type: "singleline",
                autocomplete: {
                    source: "roles",
                    enrichsource: !0
                },
                defaulttext: "Who does it?"
            }, {
                name: "tag",
                value: "R",
                type: "button",
                description: "Responsible",
                style: {
                    "background-color": "rgba(255, 165, 0, 0.5)"
                }
            }, {
                name: "tag",
                value: "A",
                type: "button",
                description: "Accountable",
                style: {
                    "background-color": "rgba(255, 0, 0, 0.5)"
                }
            }, {
                name: "tag",
                value: "C",
                type: "button",
                description: "Consulted",
                style: {
                    "background-color": "rgba(245, 245, 220, 0.5)"
                }
            }, {
                name: "tag",
                value: "I",
                type: "button",
                description: "Informed"
            } ]
        } ]
    }, c.stickynote.base = {
        fields: [ {
            name: "text",
            type: "longtext",
            helptext: "formatting",
            defaulttext: "A comment"
        }, {
            type: "colorpicker"
        } ]
    }, c.image.base = {
        editable: !1
    }, c.group.base = {
        fields: [ {
            name: "text",
            type: "singleline",
            defaulttext: "Name your group"
        } ]
    }, c.title.base = {
        fields: [ {
            name: "text",
            type: "singleline",
            defaulttext: "Name your Skore process"
        } ]
    }, c.role.base = {
        editable: !0,
        name: "role",
        type: "singleline",
        fields: [ {
            name: "comment",
            type: "longtext",
            defaulttext: "Add a comment for the role"
        } ]
    }, c;
});
define('skGraph/skTemplateManager',['require','jquery','mxUtils','skConstants','skShell','./skTemplateDescriptions'],function(require) {
    "use strict";
    function skTemplateManager(graph, existingTemplateManager) {
        var that = this;
        this.graph = graph, this.currentTemplateForForm = {}, this.formTypes = Object.keys(skTemplateDescriptions), 
        $(this.formTypes).each(function(i, type) {
            that.currentTemplateForForm[type] = "base";
        }), $(Object.keys(skTemplateDescriptions)).each(function(i, formCategory) {
            $(Object.keys(skTemplateDescriptions[formCategory])).each(function(j, form) {
                skTemplateDescriptions[formCategory][form].isBuiltIn = !0;
            });
        }), existingTemplateManager && (graph.model.root.templates = existingTemplateManager), 
        graph.model.root.templates ? $(TEMPLATE, graph.model.root.templates).each(function(i, template) {
            var formType = $(template).attr(VALUE), templateName = $(template).attr(NAME);
            that.formTypes.indexOf(formType) > 0 && (that.currentTemplateForForm[formType] = templateName, 
            skTemplateDescriptions[formType][templateName] || (skTemplateDescriptions[formType][templateName] = JSON.parse($(template).attr(skConstants.TEMPLATE_DESCRIPTION))));
        }) : this.initGraph();
    }
    var $ = require("jquery"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skShell = require("skShell"), skTemplateDescriptions = require("./skTemplateDescriptions"), TEMPLATE = skConstants.TEMPLATE, TEMPLATES = skConstants.TEMPLATES, VALUE = skConstants.VALUE, NAME = skConstants.TEMPLATE_NAME;
    return skTemplateManager.prototype.graph = null, skTemplateManager.prototype.currentTemplateForForm = null, 
    skTemplateManager.prototype.initGraph = function() {
        var graph = this.graph, that = this, templates = graph.model.root.templates;
        if (graph.model.root.templates || (templates = graph.model.root.templates = mxUtils.createXmlDocument().createElement(TEMPLATES)), 
        graph.model.root.tagsInUse) {
            var tagsInUse = graph.model.root.tagsInUse, type = tagsInUse.getAttribute("type");
            switch (type) {
              case "RATSI":
                that.currentTemplateForForm[skConstants.RESPONSIBILITY] = skConstants.RESP_RATSI;
                break;

              case "RACI":
                that.currentTemplateForForm[skConstants.RESPONSIBILITY] = skConstants.RESP_RACI;
                break;

              case "custom":
                var customFields = [ {
                    name: "role",
                    localid: !0,
                    type: "singleline",
                    autocomplete: {
                        source: "roles",
                        enrichsource: !0
                    },
                    defaulttext: "Who does it?"
                } ], customTemplate = {
                    editable: !0,
                    fields: [ {
                        name: "responsibility",
                        type: "group",
                        removable: !0,
                        multi: {
                            addbutton: !0,
                            addtext: "Add 1 more role",
                            addempty: !0
                        },
                        fields: customFields
                    } ]
                };
                $("tag", tagsInUse).each(function(i, tag) {
                    customFields.push({
                        name: "tag",
                        value: $(tag).attr(VALUE),
                        type: "button",
                        description: $(tag).attr(NAME),
                        style: {}
                    });
                }), that.currentTemplateForForm[skConstants.RESPONSIBILITY] = "custom", skTemplateDescriptions[skConstants.RESPONSIBILITY].custom = customTemplate;
            }
            graph.model.root.tagsInUse = null;
        }
        that.formTypes.forEach(function(formType) {
            var template = $("" + TEMPLATE + "[" + VALUE + "=" + formType + "]", templates), crtTmplte4Form = that.currentTemplateForForm[formType];
            crtTmplte4Form == skConstants.TEMPLATE_BASE ? template.length && template.remove() : (template.length || (template = $("<" + TEMPLATE + " " + VALUE + "='" + formType + "' />"), 
            $(templates).append(template)), 1 === template.length && template.attr(NAME, crtTmplte4Form), 
            template.removeAttr(skConstants.TEMPLATE_DESCRIPTION), "custom" === that.currentTemplateForForm[formType] && template.attr(skConstants.TEMPLATE_DESCRIPTION, JSON.stringify(skTemplateDescriptions[formType][that.currentTemplateForForm[formType]])));
        });
    }, skTemplateManager.prototype.validateFormJSON = function(JSONTemplateDescription) {
        try {
            var c = $.parseJSON(JSONTemplateDescription);
            return c;
        } catch (err) {
            return skShell.setStatus("JSON Form not ok " + err), !1;
        }
    }, skTemplateManager.prototype.setTemplateForForm = function(formType, templateName, customFormJSONDescription) {
        this.currentTemplateForForm[formType] = templateName, customFormJSONDescription && this.validateFormJSON(customFormJSONDescription) && "custom" === templateName && (skTemplateDescriptions[formType][templateName] = JSON.parse(customFormJSONDescription)), 
        this.initGraph();
    }, skTemplateManager.prototype.getTemplate = function(type) {
        return skTemplateDescriptions[type][this.currentTemplateForForm[type]];
    }, skTemplateManager.prototype.formTypes = null, skTemplateManager.prototype.whybox = {}, 
    skTemplateManager.prototype.whatbox = {}, skTemplateManager.prototype.group = {}, 
    skTemplateManager.prototype.stickynote = {}, skTemplateManager.prototype.title = {}, 
    skTemplateManager.prototype.role = {}, skTemplateManager;
});
define('skGraph/skNewRoleManager',['require','jquery','skConstants','mxUtils','skUtils'],function(require) {
    "use strict";
    function skNewRoleManager(graph, existingRoleManager) {
        this.graph = graph, this.initModel(existingRoleManager);
    }
    var $ = require("jquery"), skConstants = require("skConstants"), mxUtils = require("mxUtils"), skUtils = require("skUtils"), LOCAL_ID = skConstants.LOCAL_ID, VALUE = skConstants.VALUE;
    return skNewRoleManager.prototype.graph = null, skNewRoleManager.prototype.roleList = null, 
    skNewRoleManager.prototype.localIDCounter = null, skNewRoleManager.prototype.roles = null, 
    skNewRoleManager.prototype.initModel = function(existingRoleManager) {
        var that = this;
        that.roleList = [], existingRoleManager ? this.graph.model.root.roles = existingRoleManager : this.graph.model.root.roles || (this.graph.model.root.roles = mxUtils.createXmlDocument().createElement(skConstants.ROLES)), 
        this.roles = this.graph.model.root.roles, $(skConstants.ROLE, this.roles).each(function(i, role) {
            var lid = parseInt($(role).attr(LOCAL_ID), 10);
            that.localIDCounter = lid > that.localIDCounter ? lid : that.localIDCounter;
        }), $(this.roles.children).each(function(i, role) {
            $(role).attr(LOCAL_ID) || $(role).attr(LOCAL_ID, that.localIDCounter++);
        }), this.graph.model.filterDescendants(function(cell) {
            cell.value && $(skConstants.ROLE + "[value]", cell.value).each(function(i, role) {
                var candidate = that.getRoleByName(skUtils.readValue(role));
                candidate || (candidate = that.addRole(skUtils.readValue(role))), $(role).attr(LOCAL_ID, $(candidate).attr(LOCAL_ID)), 
                $(role).removeAttr(VALUE);
            });
        }), this.getRoles();
    }, skNewRoleManager.prototype.deleteRoleByLocalID = function(ID) {
        this.deleteRole(LOCAL_ID, ID);
    }, skNewRoleManager.prototype.deleteRoleByName = function(name) {
        this.deleteRole(VALUE, name);
    }, skNewRoleManager.prototype.deleteRole = function(key, value) {
        $(skConstants.ROLE + "[" + key + "='" + value + "']", this.roles).remove(), this.roleList = null, 
        this.roleList = this.getRoles();
    }, skNewRoleManager.prototype.addRole = function(name, noReset) {
        var that = this, candidate = this.getRoleByName(name);
        if (!candidate) {
            var obj = document.createElement(skConstants.ROLE);
            return obj.setAttribute(VALUE, name), obj.setAttribute(LOCAL_ID, ++that.localIDCounter), 
            this.roles.appendChild(obj), noReset !== !0 && (this.roleList = null), obj;
        }
        return $(candidate).attr(LOCAL_ID) || $(candidate[0]).attr(LOCAL_ID, ++that.localIDCounter), 
        candidate[0];
    }, skNewRoleManager.prototype.getRoleByName = function(name) {
        return this.getRole(VALUE, name);
    }, skNewRoleManager.prototype.getRoleByLocalID = function(ID) {
        return this.getRole(LOCAL_ID, ID);
    }, skNewRoleManager.prototype.getRoleByRemoteID = function(ID) {
        return this.getRole(skConstants.REMOTE_ID, ID);
    }, skNewRoleManager.prototype.getRole = function(key, value) {
        value || (value = "");
        var candidate = $("[" + key + "='" + value.replace(/["\\]/g, "\\$&") + "']", this.roles);
        return candidate.length && candidate.length > 0 ? candidate[0] : null;
    }, skNewRoleManager.prototype.setRoleComment = function(role, comment) {
        $(role).append($("<comment value='" + comment + "' />"));
    }, skNewRoleManager.prototype.getRoles = function() {
        if (this.roleList && this.roleList.length > 0) return this.roleList;
        var list = [];
        return $(skConstants.ROLE, this.roles).each(function(i, e) {
            list.push({
                localid: $(e).attr(LOCAL_ID),
                name: $(e).attr(VALUE)
            });
        }), this.roleList = list, this.roleList;
    }, skNewRoleManager.prototype.getCommentNode = function(ID) {
        var role = this.getRole(LOCAL_ID, ID);
        return skUtils.readValue($(skConstants.ELMT_COMMENT, role));
    }, skNewRoleManager;
});
define('skStylesheet/skStylesheetManager',['require','skConstants','skUtils'],function(require) {
    "use strict";
    function skStylesheetManager(stylesheetNode) {
        this.stylesheetNode = stylesheetNode, this.cellsType = [], this._nameToNode = [], 
        this.loadExisting();
    }
    var skConstants = require("skConstants"), skUtils = require("skUtils");
    return skStylesheetManager.prototype.loadExisting = function() {
        for (var styles = this.stylesheetNode.getElementsByTagName(skConstants.style), i = 0, l = styles.length; l > i; i += 1) {
            var node = styles[i], cellType = skUtils.readValue(node);
            this.getStyleNode(cellType) || (this.cellsType.push(cellType), this._nameToNode[cellType] = node);
        }
    }, skStylesheetManager.prototype.stylesheetNode = null, skStylesheetManager.prototype.cellsType = null, 
    skStylesheetManager.prototype.getStyleNode = function(cellType) {
        return Object.prototype.hasOwnProperty.call(this._nameToNode, cellType) ? this._nameToNode[cellType] : null;
    }, skStylesheetManager.prototype.getStyle = function(cellType) {
        var styleNode = this.getStyleNode(cellType);
        return styleNode ? styleNode.getAttribute(skConstants.styleTokens) : "";
    }, skStylesheetManager.prototype.addStyle = function(cellType, style) {
        var node = this.getStyleNode(cellType);
        if (cellType) return node || (this.cellsType.push(cellType), node = this.stylesheetNode.appendChild(this.stylesheetNode.ownerDocument.createElement(skConstants.style)), 
        node.setAttribute(skConstants.VALUE, cellType), this._nameToNode[cellType] = node), 
        style && "" !== style.trim() || (style = ""), node.setAttribute(skConstants.styleTokens, style), 
        node;
    }, skStylesheetManager.prototype.removeStyle = function(cellType) {
        var ind = this.cellsType.indexOf(cellType);
        ind > -1 && this.cellsType.splice(ind, 1);
        var node = this.getStyleNode(cellType);
        node && (node.parentNode.removeChild(node), delete this._nameToNode[cellType]);
    }, skStylesheetManager;
});
define('skStylesheet/skStylesheetsManager',['require','mxUtils','skConstants','skStylesheet/skStylesheetManager','skUtils'],function(require) {
    "use strict";
    function skStylesheetsManager(graph) {
        this.graph = graph, this.stylesheets = [], this._nameToNode = [], this.loadExisting();
    }
    var mxUtils = require("mxUtils"), skConstants = require("skConstants"), skStylesheetManager = require("skStylesheet/skStylesheetManager"), skUtils = require("skUtils");
    return skStylesheetsManager.prototype.stylesheets = null, skStylesheetsManager.prototype.graph = null, 
    skStylesheetsManager.prototype.loadExisting = function() {
        for (var styleSheets = this.initModel().getElementsByTagName(skConstants.stylesheet), i = 0, l = styleSheets.length; l > i; i += 1) {
            var stylesheetName = skUtils.readValue(styleSheets[i]);
            this.stylesheets.push(stylesheetName), this._nameToNode[stylesheetName] = styleSheets[i];
            var ss = new skStylesheetManager(styleSheets[i]);
            ss = null;
        }
    }, skStylesheetsManager.prototype.initModel = function() {
        return this.graph.model.root.stylesheets || (this.graph.model.root.stylesheets = mxUtils.createXmlDocument().createElement(skConstants.stylesheets)), 
        this.graph.model.root.stylesheets;
    }, skStylesheetsManager.prototype._getStylesheetNode = function(stylesheetName) {
        return Object.prototype.hasOwnProperty.call(this._nameToNode, stylesheetName) ? this._nameToNode[stylesheetName] : null;
    }, skStylesheetsManager.prototype.addStylesheet = function(stylesheetName) {
        if (stylesheetName) {
            var node = this._getStylesheetNode(stylesheetName);
            if (!node) {
                this.stylesheets.push(stylesheetName);
                var stylesheets = this.initModel();
                node = stylesheets.appendChild(stylesheets.ownerDocument.createElement(skConstants.stylesheet)), 
                node.setAttribute(skConstants.VALUE, stylesheetName), this._nameToNode[stylesheetName] = node;
            }
            return new skStylesheetManager(node);
        }
    }, skStylesheetsManager.prototype.removeStylesheet = function(stylesheetName) {
        var ind = this.stylesheets.indexOf(stylesheetName);
        ind > -1 && this.stylesheets.splice(ind, 1);
        var node = this.getSylesheetNode(stylesheetName);
        node && (node.parentNode.removeChild(node), delete this._nameToNode[stylesheetName]);
    }, skStylesheetsManager.prototype.updateStyleSheet = function(stylesheetName, cellType, style) {
        if (stylesheetName && cellType) {
            var ss = this.addStylesheet(stylesheetName);
            return ss.addStyle(cellType, style);
        }
    }, skStylesheetsManager.prototype.getStyle = function(stylesheetName, cellType) {
        var ss = this.addStylesheet(stylesheetName);
        return ss.getStyle(cellType);
    }, skStylesheetsManager;
});
define('skStylesheet/skStylesheet',['require','jquery','mxConstants','mxEdgeStyle','mxEventObject','mxStylesheet','mxUtils','skGraph/skColors','skConstants','skStylesheet/skStylesheetsManager'],function(require) {
    "use strict";
    function skStylesheet(graph) {
        this.graph = graph, mxStylesheet.call(this), this.resetStyleSheet();
    }
    var $ = require("jquery"), mxConstants = require("mxConstants"), mxEdgeStyle = require("mxEdgeStyle"), mxEventObject = require("mxEventObject"), mxStylesheet = require("mxStylesheet"), mxUtils = require("mxUtils"), skColors = require("skGraph/skColors"), skConstants = require("skConstants"), skStylesheetsManager = require("skStylesheet/skStylesheetsManager");
    mxUtils.extend(skStylesheet, mxStylesheet), skStylesheet.prototype.graph = null, 
    skStylesheet.prototype.resetStyleSheet = function() {
        var style;
        style = this.getDefaultVertexStyle(), style[mxConstants.STYLE_FILLCOLOR] = "transparent", 
        style[mxConstants.STYLE_STROKEWIDTH] = skConstants.STROKE_BOX, style[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP, 
        style[mxConstants.STYLE_OPACITY] = 100, style[skConstants.ATT_COLOR] = "#067C7E", 
        style[skConstants.DETAIL_COLOR] = "#067C7E", style[mxConstants.STYLE_STROKECOLOR] = "#6482B9", 
        style[mxConstants.STYLE_FONTCOLOR] = "#774400", style[mxConstants.STYLE_RESIZABLE] = !1, 
        style[mxConstants.STYLE_DASHED] = "0", style[mxConstants.STYLE_SPACING] = 0, style[mxConstants.STYLE_FONTSIZE] = 13, 
        style.whoWhatLine = "0";
        var yBoxStyle = {};
        yBoxStyle[mxConstants.STYLE_SHAPE] = skConstants.whybox, yBoxStyle[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP, 
        yBoxStyle[mxConstants.STYLE_OVERFLOW] = "visible", yBoxStyle[skConstants.WIDTH] = skConstants.YB_WIDTH, 
        yBoxStyle[skConstants.HEIGHT] = skConstants.YB_HEIGHT;
        var s = $.extend({}, style, yBoxStyle);
        this.putCellStyle(skConstants.whybox, s), this.putCellStyle("sys-" + skConstants.whybox, s), 
        s = null;
        var wBoxStyle = {};
        wBoxStyle[mxConstants.STYLE_SHAPE] = skConstants.whatbox, wBoxStyle[mxConstants.STYLE_OVERFLOW] = "fill", 
        wBoxStyle[skConstants.WIDTH] = skConstants.WB_WIDTH, wBoxStyle[skConstants.HEIGHT] = skConstants.WB_HEIGHT, 
        wBoxStyle[mxConstants.STYLE_ROUNDED] = "0", s = $.extend({}, style, wBoxStyle), 
        this.putCellStyle(skConstants.whatbox, s), this.putCellStyle("sys-" + skConstants.whatbox, s), 
        s = null;
        var wBoxDetailedStyle = {};
        wBoxDetailedStyle[mxConstants.STYLE_SHAPE] = skConstants.whatbox, s = $.extend({}, style, wBoxDetailedStyle), 
        this.putCellStyle(skConstants.whatboxdetailed, s), this.putCellStyle("sys-" + skConstants.whatboxdetailed, s), 
        s = null;
        var stickyNoteStyle = {};
        stickyNoteStyle[mxConstants.STYLE_FILLCOLOR] = "#FCF393", stickyNoteStyle[mxConstants.STYLE_STROKECOLOR] = "transparent", 
        stickyNoteStyle[mxConstants.STYLE_STROKEWIDTH] = 1, stickyNoteStyle[mxConstants.STYLE_SHADOW] = !1, 
        stickyNoteStyle[mxConstants.STYLE_OPACITY] = 60, stickyNoteStyle[mxConstants.STYLE_RESIZABLE] = !0, 
        stickyNoteStyle[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_LEFT, s = $.extend({}, style, stickyNoteStyle), 
        this.putCellStyle(skConstants.stickynote, s), this.putCellStyle("sys-" + skConstants.stickynote, s);
        var wBoxGroup = {};
        wBoxGroup[mxConstants.STYLE_VERTICAL_LABEL_POSITION] = mxConstants.ALIGN_TOP, wBoxGroup[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_BOTTOM, 
        wBoxGroup[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_LEFT, wBoxGroup[mxConstants.STYLE_FILLCOLOR] = "transparent", 
        wBoxGroup[mxConstants.STYLE_STROKECOLOR] = skColors.BOX_GROUP, wBoxGroup[mxConstants.STYLE_STROKEWIDTH] = 1, 
        wBoxGroup[mxConstants.STYLE_FONTSIZE] = 20, wBoxGroup[mxConstants.STYLE_RESIZABLE] = !0, 
        s = $.extend({}, style, wBoxGroup), this.putCellStyle(skConstants.group, s), this.putCellStyle("sys-" + skConstants.group, s);
        var wBImage = {};
        wBImage[mxConstants.STYLE_RESIZABLE] = 1, s = $.extend({}, style, wBImage), this.putCellStyle(skConstants.image, s), 
        this.putCellStyle("sys-" + skConstants.image, s), style = this.getDefaultEdgeStyle(), 
        style[mxConstants.STYLE_ROUNDED] = "1", style[mxConstants.STYLE_EDGE] = mxEdgeStyle.OrthConnector, 
        style[mxConstants.STYLE_STROKECOLOR] = skColors.line, style[mxConstants.STYLE_LABEL_BACKGROUNDCOLOR] = "white", 
        style[mxConstants.STYLE_FONTCOLOR] = "black", style[mxConstants.STYLE_ENDSIZE] = 11, 
        style[mxConstants.STYLE_STROKEWIDTH] = skConstants.STROKE_LINE, style[mxConstants.STYLE_OPACITY] = 100, 
        style[mxConstants.STYLE_DASHED] = "0", style[mxConstants.STYLE_ENDFILL] = "0", style[mxConstants.STYLE_ENDARROW] = mxConstants.ARROW_CLASSIC, 
        style[mxConstants.STYLE_EXIT_PERIMETER] = "0", style[mxConstants.STYLE_ENTRY_PERIMETER] = "0", 
        this.putCellStyle("sys-line", style);
        var wy2sStyle = {};
        wy2sStyle[mxConstants.STYLE_ENDARROW] = "none", wy2sStyle[mxConstants.STYLE_ENDFILL] = "0", 
        wy2sStyle[mxConstants.STYLE_DASHED] = "0", wy2sStyle[mxConstants.STYLE_STROKECOLOR] = skColors.line, 
        wy2sStyle[mxConstants.STYLE_STROKEWIDTH] = 2, wy2sStyle[mxConstants.STYLE_OPACITY] = 50, 
        wy2sStyle[mxConstants.STYLE_EDGE] = mxEdgeStyle.OrthConnector, wy2sStyle[mxConstants.STYLE_CURVED] = "1", 
        wy2sStyle[mxConstants.STYLE_EXIT_PERIMETER] = "1", this.putCellStyle(skConstants.LINE_WY2S, wy2sStyle), 
        this.putCellStyle("sys-" + skConstants.LINE_WY2S, wy2sStyle);
    }, skStylesheet.prototype.buildCustomStyle = function(cellType, styleSheetName, cells) {
        var customStyleUsed = "";
        customStyleUsed = cells ? cells[0].isEdge() ? "defaultEdge;" + cells[0].style + ";" : cells[0].style + ";" : cellType + ";" + this.graph.stylesheetsManager.getStyle(styleSheetName, cellType);
        var myDefault = this.styles["sys-" + cellType];
        return this.getCellStyle(customStyleUsed, myDefault);
    }, skStylesheet.prototype.customHasBeenLoaded = !1, skStylesheet.prototype.loadCustomStylesheet = function(content, forceRefresh) {
        this.customHasBeenLoaded && this.resetStyleSheet(), this.graph.stylesheetsManager = new skStylesheetsManager(this.graph);
        this.putCellStyle(skConstants.whatbox, this.buildCustomStyle(skConstants.whatbox, "default", null)), 
        this.putCellStyle(skConstants.whatboxdetailed, this.buildCustomStyle(skConstants.whatbox, "default", null)), 
        this.putCellStyle(skConstants.whybox, this.buildCustomStyle(skConstants.whybox, "default", null)), 
        this.putDefaultEdgeStyle(this.buildCustomStyle("line", "default", null)), this.putCellStyle(skConstants.stickynote, this.buildCustomStyle(skConstants.stickynote, "default", null)), 
        this.putCellStyle(skConstants.LINE_WY2S, this.buildCustomStyle(skConstants.LINE_WY2S, "default", null)), 
        this.putCellStyle(skConstants.group, this.buildCustomStyle(skConstants.group, "default", null)), 
        this.graph && forceRefresh === !0 && this.graph.refresh(), this.customHasBeenLoaded = !0;
    };
    var applyStyleToLine = function(line, styleString) {
        var currentStyle = line.style, toKeep = "";
        if (null !== currentStyle) for (var tokens = currentStyle.split(";"), j = 0; j < tokens.length; j++) (-1 == tokens[j].indexOf("=") || "" !== tokens[j].trim() && (tokens[j].indexOf("_") >= 0 || tokens[j].indexOf("entry") >= 0 || tokens[j].indexOf("exit") >= 0)) && (toKeep += tokens[j] + ";");
        line.style = "" + toKeep + ";" + styleString;
    }, applyStyleToStickyNote = function(cell, styleString) {
        var currentStyle = cell.style, toKeep = "";
        if (null !== currentStyle) for (var tokens = currentStyle.split(";"), j = 0; j < tokens.length; j++) "" !== tokens[j].trim() && tokens[j].indexOf(mxConstants.STYLE_FILLCOLOR) >= 0 && (toKeep += tokens[j] + ";");
        cell.style = skConstants.stickynote + ";" + toKeep + ";" + styleString;
    };
    return skStylesheet.prototype.applyStyleToCells = function(cells, styleAsString) {
        cells.forEach(function(cell) {
            cell.isWhatboxDetailed() ? cell.style = skConstants.whatboxdetailed + ";" + styleAsString : cell.isEdge() ? applyStyleToLine(cell, styleAsString) : cell.isStickynote() ? applyStyleToStickyNote(cell, styleAsString) : cell.style = cell.getCellType() + ";" + styleAsString;
        }), this.graph.refresh();
    }, skStylesheet.prototype.saveStyleSheet = function(styleStringForCells, styleSheetName, cellsToChange) {
        styleSheetName = styleSheetName || "default";
        var cellTypes = Object.keys(styleStringForCells), that = this, stylesheetsManager = this.graph.stylesheetsManager;
        cellTypes.forEach(function(cellType) {
            var systemStyleSheet = that.getCellStyle("sys-" + cellType), styleObj = styleStringForCells[cellType], styleStringRaw = "", styleString = "", styleAttrs = Object.keys(styleObj);
            if (styleAttrs.forEach(function(attr) {
                styleStringRaw += attr + "=" + styleObj[attr] + ";", systemStyleSheet[attr] && styleObj[attr] !== systemStyleSheet[attr] && (styleString += attr + "=" + styleObj[attr] + ";");
            }), cellsToChange && Object.keys(cellsToChange).includes(cellType) && cellsToChange[cellType].length) that.applyStyleToCells(cellsToChange[cellType], styleStringRaw); else {
                var s = $.extend({}, systemStyleSheet[cellType], styleObj);
                switch (cellType) {
                  case "line":
                    that.putCellStyle("defaultEdge", s);
                    break;

                  case skConstants.whatbox:
                    that.putCellStyle(skConstants.whatboxdetailed, s);

                  default:
                    that.putCellStyle(cellType, s);
                }
                stylesheetsManager.updateStyleSheet(styleSheetName, cellType, styleString);
            }
        }), this.graph.fireEvent(new mxEventObject("styleSheetUpdated", "graph", this.graph));
    }, skStylesheet;
});
define('skGraph/skGraph',['require','jquery','mxCellRenderer','mxClient','mxConnectionConstraint','mxConstants','mxEvent','mxEventObject','mxGraph','mxGraphView','mxMouseEvent','mxPanningHandler','mxPoint','mxPolyline','mxRectangle','mxRectangleShape','mxShape','mxStencil','mxStencilRegistry','mxText','mxUtils','skNavigator/skCellHighlighter','skGraph/skColors','skConstants','skGraph/skEdgeSegmentHandler','skGraph/skTemplateManager','skGraph/skGraphUtils','skGraph/skNewRoleManager','skShell','skStylesheet/skStylesheet','skUtils','IMAGE_PATH'],function(require) {
    "use strict";
    function wBoxDetailed() {
        mxShape.call(this);
    }
    function skGraph(container, model, renderHint, stylesheet, templateManager, roleManager) {
        mxGraph.call(this, container, model, renderHint, stylesheet);
        var graph = this;
        graph.isHtmlLabel = function() {
            return !0;
        }, graph.enabled = !1, graph.defaultPageVisible = !1, graph.pageVisible = graph.defaultPageVisible, 
        graph.setPanning(!0), graph.resetViewOnRootChange = !1, graph.scrollbars = !0, graph.background = null, 
        graph.setConnectable(!0), graph.setDropEnabled(!1), graph.setAllowLoops(!1), graph.setExtendParents(!1), 
        graph.setExtendParentsOnAdd(!1), graph.setAllowDanglingEdges(!1), graph.pageScale = 1, 
        graph.view.scale = 1, graph.gridEnabled = !1, graph.graphHandler.guidesEnabled = !0, 
        graph.foldingEnabled = !0, graph.setTooltips(!1), graph.allowAutoPanning = !0, graph.setConstrainChildren(!1), 
        graph.isClipping = !1, graph.cellHTMLCache = [], graph.timerAutoScroll = !0, graph.showBadgeIcon = !1, 
        graph.zoomFactor = 1.1, graph.alternateEdgeStyle = "elbow=vertical", graph.pageBreakColor = skColors.PAGE_BREAK, 
        graph.pageFormat = mxConstants.PAGE_FORMAT_A4_LANDSCAPE, graph.pageBreaksVisible = graph.pageVisible, 
        graph.preferPageSize = graph.pageBreaksVisible, graph.enterStopsCellEditing = !0, 
        graph.labelMode = "htmlnode", graph.isWrapping = function() {
            return !0;
        }, mxText.prototype.replaceLinefeeds = !1, graph.getLabel = function(cell) {
            return this.labelsVisible && cell.isVertex() ? cell.createHtmlContent(this) : void 0;
        }, graph.setAutoSizeCells(!0), graph._returnNewHeight = function(cell) {
            var tmp, labelHTML = cell.createHtmlContent(this);
            tmp = mxUtils.isNode(labelHTML) ? labelHTML.cloneNode(!0) : $(labelHTML)[0], tmp.setAttribute("style", "font-size:" + this.getCellStyle(cell).fontSize + "px;width:" + (cell.geometry.width - this.getCellStyle(cell).strokeWidth) + "px;vertical-align:top;white-space:normal;text-align:center"), 
            document.body.appendChild(tmp);
            var newHeight = tmp.offsetHeight;
            return tmp.parentElement.removeChild(tmp), newHeight;
        };
        var graphGetPreferredSizeForCell = graph.getPreferredSizeForCell;
        graph.getPreferredSizeForCell = function(cell) {
            var newHeight, minHeight, result = graphGetPreferredSizeForCell.apply(this, arguments);
            if (!cell.isEdge()) {
                var w, h, state = this.view.getState(cell), style = null != state ? state.style : this.getCellStyle(cell);
                style[mxConstants.STYLE_RESIZABLE] ? (w = cell.geometry.width, h = cell.geometry.height) : (w = parseInt(style.width, 10), 
                h = parseInt(style.height, 10)), cell.isWhybox() && (result = new mxRectangle(cell.geometry.x, cell.geometry.y, w, h)), 
                cell.isGroup() && (result = new mxRectangle(cell.geometry.x, cell.geometry.y, w, h)), 
                cell.isStickynote() && (newHeight = this._returnNewHeight(cell), newHeight = newHeight > cell.geometry.height ? newHeight : cell.geometry.height, 
                result = new mxRectangle(cell.geometry.x, cell.geometry.y, w, newHeight)), cell.isWhatbox() && (newHeight = this._returnNewHeight(cell), 
                minHeight = parseInt(style.height, 10), newHeight = minHeight > newHeight ? minHeight : newHeight, 
                result = new mxRectangle(cell.geometry.x, cell.geometry.y, w, newHeight));
            }
            return result;
        }, this.addMouseListener({
            currentState: null,
            hoverButtons: null,
            mouseDown: function() {},
            mouseMove: function(sender, me) {
                if (null == this.currentState || me.getState() != this.currentState || !graph.isMouseDown) {
                    var tmp = graph.view.getState(me.getCell());
                    (graph.isMouseDown || null != tmp && !graph.getModel().isVertex(tmp.cell)) && (tmp = null), 
                    tmp != this.currentState && (null != this.currentState && this.dragLeave(me.getEvent(), this.currentState), 
                    this.currentState = tmp, null != this.currentState && this.dragEnter(me.getEvent(), this.currentState));
                }
            },
            mouseUp: function() {},
            dragEnter: function(evt, state) {
                null != state && ((graph.isEnabled() || !graph.isEnabled() && !state.cell.isStickynote()) && graph.cellHighlighter.toggleHighlightOn(state), 
                graph.currentCell = state.cell, graph.view.createState(state.cell, !0), graph.cellRenderer.redraw(state, !0));
            },
            dragLeave: function(evt, state) {
                null != state && null != state.shape && ((1 !== graph.getSelectionCells().length || graph.getSelectionCell() !== graph.currentCell) && graph.cellRenderer.destroyIconsIfNecessary(state), 
                graph.cellHighlighter.toggleHighlightOff(), graph.currentCell = null);
            }
        }), this.cellHighlighter = new skCellHighlighter(this), this.visibleAreas = [];
        var graphFireMouseEvent = mxGraph.prototype.fireMouseEvent;
        this.fireMouseEvent = function(evtName) {
            evtName == mxEvent.MOUSE_DOWN && this.container.focus(), graphFireMouseEvent.apply(this, arguments);
        }, this.addListener(mxEvent.CELLS_REMOVED, mxUtils.bind(this, function(sender, evt) {
            var that = this;
            $(evt.properties.cells).each(function(i, e) {
                e.isEdge() || delete that.cellHTMLCache[e.id];
            });
        })), this.addListener("navigateTo", mxUtils.bind(this, function(sender, event) {
            this.navigateTo(event.getProperty("cell"));
        })), this.initTemplateManager(templateManager), this.initRoleManager(roleManager), 
        this.addListener("checkCellLink", mxUtils.bind(this, function(sender, event) {
            this.isEnabled() && skGraphUtils.checkInternalLinks(this, event.getProperty("cellHTML"));
        }));
    }
    var $ = require("jquery"), mxCellRenderer = require("mxCellRenderer"), mxClient = require("mxClient"), mxConnectionConstraint = require("mxConnectionConstraint"), mxConstants = require("mxConstants"), mxEvent = require("mxEvent"), mxEventObject = require("mxEventObject"), mxGraph = require("mxGraph"), mxGraphView = require("mxGraphView"), mxMouseEvent = require("mxMouseEvent"), mxPanningHandler = require("mxPanningHandler"), mxPoint = require("mxPoint"), mxPolyline = require("mxPolyline"), mxRectangle = require("mxRectangle"), mxRectangleShape = require("mxRectangleShape"), mxShape = require("mxShape"), mxStencil = require("mxStencil"), mxStencilRegistry = require("mxStencilRegistry"), mxText = require("mxText"), mxUtils = require("mxUtils"), skCellHighlighter = require("skNavigator/skCellHighlighter"), skColors = require("skGraph/skColors"), skConstants = require("skConstants"), skEdgeSegmentHandler = require("skGraph/skEdgeSegmentHandler"), skTemplateManager = require("skGraph/skTemplateManager"), skGraphUtils = require("skGraph/skGraphUtils"), skRoleManager = require("skGraph/skNewRoleManager"), skShell = require("skShell"), skStylesheet = require("skStylesheet/skStylesheet"), skUtils = require("skUtils");
    mxConstants.DEFAULT_FONTFAMILY = "'Open Sans', 'sans-serif'", mxConstants.DEFAULT_FONTSIZE = 15, 
    mxConstants.DEFAULT_VALID_COLOR = skColors.BOX_VALID, mxConstants.VERTEX_SELECTION_COLOR = skColors.BOX_SELECTED, 
    mxConstants.VERTEX_SELECTION_STROKEWIDTH = skConstants.STROKE_BOX_SELECTED, mxConstants.EDGE_SELECTION_STROKEWIDTH = skConstants.STROKE_LINE_selected, 
    mxConstants.VERTEX_SELECTION_DASHED = !1, mxConstants.EDGE_SELECTION_COLOR = skColors.BOX_SELECTED, 
    mxConstants.EDGE_SELECTION_DASHED = !1, mxConstants.HANDLE_SIZE = 14, mxConstants.GUIDE_COLOR = skColors.GUIDE, 
    mxConstants.GUIDE_STROKEWIDTH = 2, mxConstants.OUTLINE_COLOR = skColors.OUTLINE_FRAME, 
    mxUtils.extend(wBoxDetailed, mxShape), wBoxDetailed.prototype.paintBackground = function(c, x, y, w, h) {
        var stickSize = 20 * this.scale, stickOffset = -5 * this.scale;
        c.translate(x, y), c.begin(), c.moveTo(-stickOffset, -stickOffset + stickSize), 
        c.lineTo(-stickOffset, -stickOffset), c.lineTo(-stickOffset + stickSize, -stickOffset), 
        c.moveTo(w + stickOffset - stickSize, -stickOffset), c.lineTo(w + stickOffset, -stickOffset), 
        c.lineTo(w + stickOffset, -stickOffset + stickSize), c.moveTo(w + stickOffset - stickSize, h + stickOffset), 
        c.lineTo(w + stickOffset, h + stickOffset), c.lineTo(w + stickOffset, h + stickOffset - stickSize), 
        c.moveTo(-stickOffset, h + stickOffset - stickSize), c.lineTo(-stickOffset, h + stickOffset), 
        c.lineTo(-stickOffset + stickSize, h + stickOffset), c.moveTo(0, 0), c.lineTo(w, 0), 
        c.lineTo(w, h), c.lineTo(0, h), c.lineTo(0, 0), c.close(), c.end(), c.stroke();
    }, mxCellRenderer.registerShape(skConstants.whatboxdetailed, wBoxDetailed), mxCellRenderer.prototype.getControlBounds = function() {
        return new mxRectangle();
    }, mxCellRenderer.prototype.getShapesForState = function(state) {
        return null != state.shape && null != state.shape.node ? [ state.shape, state.text ] : [ null, state.text ];
    }, function() {
        for (var xmlShape = mxUtils.parseXml('<shapes><shape name="' + skConstants.whybox + '" h="50" w="100" aspect="variable" strokewidth="inherit"><foreground><path><move x="5" y="5"/><arc rx="50" ry="50" x-axis-rotation="0" large-arc-flag="0" sweep-flag="0" x="5" y="45"/></path><stroke/><path><move x="95" y="5"/><arc rx="50" ry="50" x-axis-rotation="0" large-arc-flag="0" sweep-flag="1" x="95" y="45"/></path><stroke/></foreground></shape></shapes>'), shape = xmlShape.documentElement.firstChild; null != shape; ) shape.nodeType == mxConstants.NODETYPE_ELEMENT && mxStencilRegistry.addStencil(shape.getAttribute("name"), new mxStencil(shape)), 
        shape = shape.nextSibling;
    }(), mxUtils.extend(skGraph, mxGraph), skGraph.prototype.visibleAreas = null, skGraph.prototype.createStylesheet = function() {
        return new skStylesheet(this);
    }, skGraph.prototype.cellHTMLCache = null, skGraph.prototype.minFitScale = null, 
    skGraph.prototype.maxFitScale = null, skGraph.prototype.transparentBackground = !1, 
    skGraph.prototype.scrollTileSize = new mxRectangle(0, 0, 400, 400), skGraph.prototype.directLinkModeEnabled = !1, 
    skGraph.prototype.isDirectLinkModeEnabled = function() {
        return this.directLinkModeEnabled;
    }, skGraph.prototype.createEdgeSegmentHandler = function(state) {
        return new skEdgeSegmentHandler(state);
    }, skGraph.prototype.getAllConnectionConstraints = function(terminal) {
        return terminal.cell.isEdge() ? null : [ new mxConnectionConstraint(new mxPoint(.5, 0), !0), new mxConnectionConstraint(new mxPoint(.25, 0), !0), new mxConnectionConstraint(new mxPoint(.75, 0), !0), new mxConnectionConstraint(new mxPoint(1, .5), !0), new mxConnectionConstraint(new mxPoint(.5, 1), !0), new mxConnectionConstraint(new mxPoint(.25, 1), !0), new mxConnectionConstraint(new mxPoint(.75, 1), !0), new mxConnectionConstraint(new mxPoint(0, .5), !0) ];
    }, skGraph.prototype.getConnectionPoint = function(vertex, constraint) {
        var point = null;
        if (null != vertex && null != constraint.point) {
            var bounds = this.view.getPerimeterBounds(vertex), dx = constraint.point.x * bounds.width, dy = 0 === constraint.point.y || 1 === constraint.point.y ? constraint.point.y * bounds.height : 40 * this.view.scale;
            point = new mxPoint(bounds.x + dx, bounds.y + dy), constraint.perimeter && (point = this.view.getPerimeterPoint(vertex, point, !1)), 
            point.x = Math.round(point.x), point.y = Math.round(point.y);
            var selectedLine = this.getSelectionCell();
            selectedLine && selectedLine.isEdge() && (selectedLine.source === vertex.cell && this.getCellStyle(selectedLine)[mxConstants.STYLE_EXIT_Y] === constraint.point.y && this.getCellStyle(selectedLine)[mxConstants.STYLE_EXIT_X] === constraint.point.x || selectedLine.target === vertex.cell && this.getCellStyle(selectedLine)[mxConstants.STYLE_ENTRY_Y] === constraint.point.y && this.getCellStyle(selectedLine)[mxConstants.STYLE_ENTRY_X] === constraint.point.x) && (point.DONOTDISPLAY = !0);
        }
        return point;
    }, skGraph.prototype.getTitle = function() {
        return this.model.cells[1].value ? skUtils.readValue(this.model.cells[1].value.getElementsByTagName(skConstants.TEXT)) : "untitled";
    }, skGraph.prototype.isValidRoot = function(cell) {
        return "1" === cell.id || cell.isWhatbox();
    };
    var oSetSelectionCell = skGraph.prototype.setSelectionCell;
    skGraph.prototype.setSelectionCell = function() {
        this.isEnabled() && oSetSelectionCell.apply(this, arguments);
    };
    var oSetSelectionCells = skGraph.prototype.setSelectionCells;
    skGraph.prototype.setSelectionCells = function() {
        this.isEnabled() && oSetSelectionCells.apply(this, arguments);
    };
    var mxUtilsReplaceTrailingNewlines = mxUtils.replaceTrailingNewlines;
    mxUtils.replaceTrailingNewlines = function(str, pattern) {
        return str ? mxUtilsReplaceTrailingNewlines.call(null, str, pattern) : str;
    }, skGraph.prototype.initTemplateManager = function(existingTemplateManager) {
        this.templateManager = new skTemplateManager(this, existingTemplateManager);
    }, skGraph.prototype.initRoleManager = function(existingRoleManager) {
        this.roleManager = new skRoleManager(this, existingRoleManager);
    }, skGraph.prototype.updateCellsSize = function(cells) {
        var graph = this, update = function(cell) {
            cell.isVertex() && cell.value && graph.updateCellSize(cell);
        };
        graph.model.beginUpdate();
        try {
            cells ? cells.forEach(function(cell) {
                update(cell);
            }) : graph.model.filterDescendants(function(cell) {
                update(cell);
            });
        } finally {
            graph.model.endUpdate();
        }
        graph.refresh();
    }, skGraph.prototype.updatePageBreaks = function(visible, width, height) {
        var pts, pageBreak, i, scale = this.view.scale, tr = this.view.translate, fmt = this.pageFormat, ps = scale * this.pageScale, bounds2 = this.view.getBackgroundPageBounds();
        width = bounds2.width, height = bounds2.height;
        var bounds = new mxRectangle(scale * tr.x, scale * tr.y, fmt.width * ps, fmt.height * ps);
        visible = visible && Math.min(bounds.width, bounds.height) > this.minPageBreakDist;
        var horizontalCount = visible ? Math.ceil(width / bounds.width) - 1 : 0, verticalCount = visible ? Math.ceil(height / bounds.height) - 1 : 0, right = bounds2.x + width, bottom = bounds2.y + height;
        if (null == this.horizontalPageBreaks && horizontalCount > 0 && (this.horizontalPageBreaks = []), 
        null != this.horizontalPageBreaks) {
            for (i = 0; horizontalCount >= i; i++) pts = [ new mxPoint(bounds2.x + (i + 1) * bounds.width, bounds2.y), new mxPoint(bounds2.x + (i + 1) * bounds.width, bottom) ], 
            null != this.horizontalPageBreaks[i] ? (this.horizontalPageBreaks[i].points = pts, 
            this.horizontalPageBreaks[i].redraw()) : (pageBreak = new mxPolyline(pts, this.pageBreakColor), 
            pageBreak.dialect = this.dialect, pageBreak.isDashed = this.pageBreakDashed, pageBreak.pointerEvents = !1, 
            pageBreak.init(this.view.backgroundPane), pageBreak.redraw(), this.horizontalPageBreaks[i] = pageBreak);
            for (i = horizontalCount; i < this.horizontalPageBreaks.length; i++) this.horizontalPageBreaks[i].destroy();
            this.horizontalPageBreaks.splice(horizontalCount, this.horizontalPageBreaks.length - horizontalCount);
        }
        if (null == this.verticalPageBreaks && verticalCount > 0 && (this.verticalPageBreaks = []), 
        null != this.verticalPageBreaks) {
            for (i = 0; verticalCount >= i; i++) pts = [ new mxPoint(bounds2.x, bounds2.y + (i + 1) * bounds.height), new mxPoint(right, bounds2.y + (i + 1) * bounds.height) ], 
            null != this.verticalPageBreaks[i] ? (this.verticalPageBreaks[i].points = pts, this.verticalPageBreaks[i].redraw()) : (pageBreak = new mxPolyline(pts, this.pageBreakColor), 
            pageBreak.dialect = this.dialect, pageBreak.isDashed = this.pageBreakDashed, pageBreak.pointerEvents = !1, 
            pageBreak.init(this.view.backgroundPane), pageBreak.redraw(), this.verticalPageBreaks[i] = pageBreak);
            for (i = verticalCount; i < this.verticalPageBreaks.length; i++) this.verticalPageBreaks[i].destroy();
            this.verticalPageBreaks.splice(verticalCount, this.verticalPageBreaks.length - verticalCount);
        }
    }, skGraph.prototype.navigateTo_top = function() {
        this.navigateTo(this.model.getRoot().children[0]);
    }, skGraph.prototype.navigateTo_up = function() {
        this.navigateTo(this.model.getParent(this.getCurrentRoot()));
    }, skGraph.prototype.resetToOrigin = function(force, pageSize) {
        var tx = 0, ty = 0;
        if (force || this.isEnabled()) {
            var cells = this.getChildCells(this.getDefaultParent(), !0, !0), bounds = this.getBoundingBoxFromGeometry(cells, !0);
            if (bounds && bounds.x < 0 && bounds.y < 0) {
                var pw = pageSize ? pageSize.width : this.getPageSize().width, ph = pageSize ? pageSize.height : this.getPageSize().height;
                if (bounds.x > 0 && bounds.x > pw) for (;bounds.x > pw; ) bounds.x -= pw, tx -= pw; else if (bounds.x < 0) for (;bounds.x < 0; ) bounds.x += pw, 
                tx += pw;
                if (bounds.y > 0 && bounds.y > ph) for (;bounds.y > ph; ) bounds.y -= ph, ty -= ph; else if (bounds.y < 0) for (;bounds.y < 0; ) bounds.y += ph, 
                ty += ph;
                this.moveCells(cells, tx, ty);
            }
        }
        return new mxPoint(tx, ty);
    }, skGraph.prototype.translateViewer = function(point) {
        var viewerRectangle = new mxRectangle(-this.view.translate.x + this.container.scrollLeft, -this.view.translate.y + this.container.scrollTop, this.container.clientWidth, this.container.clientHeight);
        viewerRectangle.x += point.x, viewerRectangle.y += point.y, this.scrollRectToVisible(viewerRectangle);
    }, mxGraphView.prototype.getVisibleArea = function() {
        return {
            scale: this.scale,
            x: this.translate.x,
            y: this.translate.y,
            left: this.graph.container.scrollLeft,
            top: this.graph.container.scrollTop
        };
    }, mxGraphView.prototype.setVisibleArea = function(object) {
        this.graph.container.scrollLeft = object.left, this.graph.container.scrollTop = object.top, 
        this.scaleAndTranslate(object.scale, object.x, object.y);
    }, skGraph.prototype.navigateToCellHandler = function(dest) {
        $("#skAttachmentsModal").modal("hide"), dest = "" + dest;
        var cellID = dest.match(/\d+/), realCell = this.model.cells[cellID];
        realCell ? "d" === dest[dest.length - 1] ? this.navigateTo(realCell) : this.navigateTo(realCell, !0) : ($(event.target).css({
            "border-bottom": "1px dashed gray",
            display: "inline"
        }), skShell.skStatus.set("Destination cell does not exist"));
    };
    var smartView = !1;
    skGraph.prototype.navigateTo = function(destinationCell, stayAbove, navSource, centerOnDestination) {
        "string" == typeof destinationCell && (destinationCell = this.model.cells[parseInt(destinationCell, 10)]), 
        stayAbove = stayAbove || !1;
        var departingCell = this.getCurrentRoot() ? this.getCurrentRoot() : this.getDefaultParent();
        destinationCell = destinationCell || this.getSelectionCell();
        var root = this.model.getRoot(), originalDestinationCell = destinationCell;
        if (null != destinationCell) {
            for (stayAbove && "1" !== destinationCell.id && (destinationCell = destinationCell.parent), 
            this.cellEditor.stopEditing(!0); destinationCell != root && !this.isValidRoot(destinationCell) && this.model.getParent(destinationCell) != root; ) destinationCell = this.model.getParent(destinationCell);
            this.translateViewer(this.resetToOrigin()), departingCell != destinationCell && (this.fireEvent(new mxEventObject("navigationStarted", "destinationCell", originalDestinationCell, "stayAbove", stayAbove, "graph", this)), 
            smartView && (this.visibleAreas[departingCell.id] = this.view.getVisibleArea()), 
            this.isEnabled() && departingCell.geometry && (departingCell.children && departingCell.children.length > 0 ? (skUtils.setCellsStyleName(this.model, [ departingCell ], skConstants.whatboxdetailed), 
            departingCell.geometry.width = departingCell.geometry.width, departingCell.geometry.height = departingCell.geometry.height, 
            this.model.setCollapsed(departingCell, !0)) : (skUtils.setCellsStyleName(this.model, [ departingCell ], skConstants.whatbox), 
            this.model.setCollapsed(departingCell, !0))), this.clearSelection(), destinationCell == root || this.model.getParent(destinationCell) == root ? this.view.setCurrentRoot(null) : this.view.setCurrentRoot(destinationCell), 
            this.translateViewer(this.resetToOrigin()), (!window.localStorage.getItem("skDetailedViewHintHasBeenSeen") || !window.localStorage.getItem("skDetailedViewHintHasBeenSeen") && skShell.tutorial) && (skShell.hint && skShell.hint.hey($(".skTitle li:last-child"), {
                title: "You entered a Detailed View",
                content: [ "To navigate back to where you were, click on the links" ]
            }, "bottom", !0, null), localStorage.setItem("skDetailedViewHintHasBeenSeen", !0), 
            skShell.tutorial && skShell.tutorial.stepDone(skShell.tutorial.NAVIGATE)), smartView && navSource == skConstants.NAV_BREADCRUMB && this.visibleAreas[destinationCell.id] ? this.view.setVisibleArea(this.visibleAreas[destinationCell.id]) : this.view.setVisibleArea({
                scale: this.view.scale,
                x: this.container.offsetWidth - skConstants.OFFSET_EDITOR,
                y: this.container.offsetHeight - skConstants.OFFSET_EDITOR,
                left: this.container.offsetWidth - skConstants.OFFSET_EDITOR,
                top: this.container.offsetHeight - skConstants.OFFSET_EDITOR
            }));
            var rel = skUtils.findRelationship(this.getModel(), destinationCell, departingCell);
            if (rel == skConstants.REL_VISIBLE) this.cellHighlighter.toggleHighlightOn(this.view.getState(departingCell)), 
            this.setSelectionCell(departingCell); else if (rel == skConstants.REL_CHILD) {
                for (var tmp = departingCell; tmp.parent != destinationCell; ) tmp = tmp.parent;
                this.cellHighlighter.toggleHighlightOn(this.view.getState(tmp)), this.setSelectionCell(tmp);
            } else this.clearSelection(), stayAbove && (this.scrollCellToVisible(originalDestinationCell, centerOnDestination), 
            this.cellHighlighter.toggleHighlightOn(this.view.getState(originalDestinationCell)), 
            this.setSelectionCell(originalDestinationCell));
            departingCell !== destinationCell && this.fireEvent(new mxEventObject("navigationDone", "destinationCell", originalDestinationCell, "stayAbove", stayAbove));
        }
    }, skGraph.prototype.getParents = function(cellAtBottom, cellAtTop) {
        cellAtTop = cellAtTop || this.getDefaultParent();
        for (var parents = [], tmp = cellAtBottom.parent; tmp != cellAtTop; ) parents.unshift(tmp), 
        tmp = tmp.parent;
        return parents;
    };
    var graphViewValidate = mxGraphView.prototype.validate;
    mxGraphView.prototype.validate = function() {
        if (null != this.graph.container && mxUtils.hasScrollbars(this.graph.container)) {
            var pad = this.graph.getPagePadding(), size = this.graph.getPageSize();
            this.translate.x = pad.x - (this.x0 || 0) * size.width, this.translate.y = pad.y - (this.y0 || 0) * size.height;
        }
        graphViewValidate.apply(this, arguments);
    };
    var graphSizeDidChange = mxGraph.prototype.sizeDidChange;
    mxGraph.prototype.sizeDidChange = function() {
        if (null != this.container && mxUtils.hasScrollbars(this.container)) {
            var pages = this.getPageLayout(), pad = this.getPagePadding(), size = this.getPageSize(), minw = Math.ceil(2 * pad.x + pages.width * size.width), minh = Math.ceil(2 * pad.y + pages.height * size.height), min = this.minimumGraphSize;
            (null == min || min.width != minw || min.height != minh) && (this.minimumGraphSize = new mxRectangle(0, 0, minw, minh));
            var dx = pad.x - pages.x * size.width, dy = pad.y - pages.y * size.height;
            if (!this.autoTranslate && (this.view.translate.x != dx || this.view.translate.y != dy)) {
                this.autoTranslate = !0, this.view.x0 = pages.x, this.view.y0 = pages.y;
                var tx = this.view.translate.x, ty = this.view.translate.y;
                return this.view.setTranslate(dx, dy), this.container.scrollLeft += (dx - tx) * this.view.scale, 
                this.container.scrollTop += (dy - ty) * this.view.scale, void (this.autoTranslate = !1);
            }
            graphSizeDidChange.apply(this, arguments), this.cellEditor && this.cellEditor.wrapperDiv && this.cellEditor.positionEditor(this.cellEditor.editingCell);
        }
    }, mxGraphView.prototype.getBackgroundPageBounds = function() {
        var layout = this.graph.getPageLayout(), page = this.graph.getPageSize();
        return new mxRectangle(this.scale * (this.translate.x + layout.x * page.width), this.scale * (this.translate.y + layout.y * page.height), this.scale * layout.width * page.width, this.scale * layout.height * page.height);
    }, mxGraph.prototype.getPreferredPageSize = function() {
        var pages = this.getPageLayout(), size = this.getPageSize();
        return new mxRectangle(0, 0, pages.width * size.width, pages.height * size.height);
    }, mxGraph.prototype.getPageLayout = function() {
        var size = this.pageVisible ? this.getPageSize() : this.scrollTileSize, bounds = this.getGraphBounds();
        if (0 === bounds.width || 0 === bounds.height) return new mxRectangle(0, 0, 1, 1);
        var x = Math.ceil(bounds.x / this.view.scale - this.view.translate.x), y = Math.ceil(bounds.y / this.view.scale - this.view.translate.y), w = Math.floor(bounds.width / this.view.scale), h = Math.floor(bounds.height / this.view.scale), x0 = Math.floor(x / size.width), y0 = Math.floor(y / size.height), w0 = Math.ceil((x + w) / size.width) - x0, h0 = Math.ceil((y + h) / size.height) - y0;
        return new mxRectangle(x0, y0, w0, h0);
    }, mxGraph.prototype.getPagePadding = function() {
        return new mxPoint(Math.max(0, Math.round((this.container.offsetWidth - skConstants.OFFSET_EDITOR) / this.view.scale)), Math.max(0, Math.round((this.container.offsetHeight - skConstants.OFFSET_EDITOR) / this.view.scale)));
    }, mxGraph.prototype.getPageSize = function() {
        return this.pageVisible ? new mxRectangle(0, 0, this.pageFormat.width * this.pageScale, this.pageFormat.height * this.pageScale) : this.scrollTileSize;
    }, mxGraph.prototype.scrollTileSize = new mxRectangle(0, 0, 400, 400), mxPanningHandler.prototype.isPanningTrigger = function(me) {
        var evt = me.getEvent();
        if (this.graph.isEnabled() && (skShell.wKeyIsPressed || skShell.yKeyIsPressed || skShell.nKeyIsPressed)) {
            var box = null;
            this.graph.getModel().beginUpdate();
            try {
                var style;
                skShell.wKeyIsPressed ? (style = this.graph.getStylesheet().styles[skConstants.whatbox], 
                box = this.graph.insertVertex(this.graph.getDefaultParent(), null, skUtils.createBoxElement(skConstants.whatbox), me.graphX / this.graph.getView().scale - this.graph.getView().translate.x, me.graphY / this.graph.getView().scale - this.graph.getView().translate.y, parseInt(style.width, 10), parseInt(style.height, 10), skConstants.whatbox), 
                box.collapsed = !0) : skShell.yKeyIsPressed ? (style = this.graph.getStylesheet().styles[skConstants.whybox], 
                box = this.graph.insertVertex(this.graph.getDefaultParent(), null, skUtils.createBoxElement(skConstants.whybox), me.graphX / this.graph.getView().scale - this.graph.getView().translate.x, me.graphY / this.graph.getView().scale - this.graph.getView().translate.y, parseInt(style.width, 10), parseInt(style.height, 10), skConstants.whybox)) : skShell.nKeyIsPressed && (box = this.graph.insertVertex(this.graph.getDefaultParent(), null, skUtils.createBoxElement(skConstants.stickynote), me.graphX / this.graph.getView().scale - this.graph.getView().translate.x, me.graphY / this.graph.getView().scale - this.graph.getView().translate.y, skConstants.WB_WIDTH, skConstants.WB_HEIGHT, skConstants.stickynote));
            } finally {
                this.graph.getModel().endUpdate();
            }
        }
        return mxEvent.isShiftDown(evt) ? !1 : this.useLeftButtonForPanning && (this.ignoreCell || null == me.getState()) ? !0 : void (this.graph.panningHandler.autoExpand = !0);
    };
    var mxGraphPanGraph = mxGraph.prototype.panGraph;
    mxGraph.prototype.panGraph = function(dx, dy) {
        if (mxGraphPanGraph.apply(this, arguments), null != this.shiftPreview1) {
            var canvas = this.view.canvas;
            null != canvas.ownerSVGElement && (canvas = canvas.ownerSVGElement);
            var phase = this.gridSize * this.view.scale * this.view.gridSteps, position = -Math.round(phase - mxUtils.mod(this.view.translate.x * this.view.scale + dx, phase)) + "px " + -Math.round(phase - mxUtils.mod(this.view.translate.y * this.view.scale + dy, phase)) + "px";
            canvas.style.backgroundPosition = position;
        }
    }, mxGraphView.prototype.createBackgroundPageShape = function(bounds) {
        return new mxRectangleShape(bounds, "#ffffff", "#cccccc");
    }, mxGraphView.prototype.createSvgGrid = function(color) {
        for (var tmp = this.graph.gridSize * this.scale; tmp < this.minGridSize; ) tmp *= 2;
        for (var tmp2 = this.gridSteps * tmp, d = [], i = 1; i < this.gridSteps; i++) {
            var tmp3 = i * tmp;
            d.push("M 0 " + tmp3 + " L " + tmp2 + " " + tmp3 + " M " + tmp3 + " 0 L " + tmp3 + " " + tmp2);
        }
        var size = tmp2, svg = '<svg width="' + size + '" height="' + size + '" xmlns="' + mxConstants.NS_SVG + '"><defs><pattern id="grid" width="' + tmp2 + '" height="' + tmp2 + '" patternUnits="userSpaceOnUse"><path d="' + d.join(" ") + '" fill="none" stroke="' + color + '" opacity="0.2" stroke-width="1"/><path d="M ' + tmp2 + " 0 L 0 0 0 " + tmp2 + '" fill="none" stroke="' + color + '" stroke-width="1"/></pattern></defs><rect width="100%" height="100%" fill="url(#grid)"/></svg>';
        return svg;
    }, mxGraphView.prototype.validateBackgroundPage = function() {
        var graph = this.graph;
        if (null != graph.container && !graph.transparentBackground) {
            if (graph.pageVisible) {
                var bounds = this.getBackgroundPageBounds();
                if (null == this.backgroundPageShape) {
                    for (var firstChild = graph.container.firstChild; null != firstChild && firstChild.nodeType != mxConstants.NODETYPE_ELEMENT; ) firstChild = firstChild.nextSibling;
                    null != firstChild && (this.backgroundPageShape = this.createBackgroundPageShape(bounds), 
                    this.backgroundPageShape.scale = 1, this.backgroundPageShape.isShadow = !mxClient.IS_QUIRKS, 
                    this.backgroundPageShape.dialect = mxConstants.DIALECT_STRICTHTML, this.backgroundPageShape.init(graph.container), 
                    firstChild.style.position = "absolute", graph.container.insertBefore(this.backgroundPageShape.node, firstChild), 
                    this.backgroundPageShape.redraw(), this.backgroundPageShape.node.className = "geBackgroundPage", 
                    mxEvent.addListener(this.backgroundPageShape.node, "dblclick", mxUtils.bind(this, function(evt) {
                        graph.dblClick(evt);
                    })), mxEvent.addGestureListeners(this.backgroundPageShape.node, mxUtils.bind(this, function(evt) {
                        graph.fireMouseEvent(mxEvent.MOUSE_DOWN, new mxMouseEvent(evt));
                    }), mxUtils.bind(this, function(evt) {
                        null != graph.tooltipHandler && graph.tooltipHandler.isHideOnHover() && graph.tooltipHandler.hide(), 
                        graph.isMouseDown && !mxEvent.isConsumed(evt) && graph.fireMouseEvent(mxEvent.MOUSE_MOVE, new mxMouseEvent(evt));
                    }), mxUtils.bind(this, function(evt) {
                        graph.fireMouseEvent(mxEvent.MOUSE_UP, new mxMouseEvent(evt));
                    })));
                } else this.backgroundPageShape.scale = 1, this.backgroundPageShape.bounds = bounds, 
                this.backgroundPageShape.redraw();
            } else null != this.backgroundPageShape && (this.backgroundPageShape.destroy(), 
            this.backgroundPageShape = null);
            this.validateBackgroundStyles();
        }
    }, mxGraphView.prototype.gridImage = mxClient.IS_SVG ? "data:image/gif;base64,R0lGODlhCgAKAJEAAAAAAP///8zMzP///yH5BAEAAAMALAAAAAAKAAoAAAIJ1I6py+0Po2wFADs=" : require("IMAGE_PATH") + "/grid.gif", 
    mxGraphView.prototype.gridSteps = 4, mxGraphView.prototype.gridColor = "#e0e0e0", 
    mxGraphView.prototype.minGridSize = 4, mxGraphView.prototype.validateBackgroundStyles = function() {
        var graph = this.graph, color = null == graph.background || graph.background == mxConstants.NONE ? "#ffffff" : graph.background, gridColor = this.gridColor != color.toLowerCase() ? this.gridColor : "#ffffff", image = "none", position = "";
        if (graph.isGridEnabled()) {
            var phase = 10;
            mxClient.IS_SVG ? (image = window.unescape(encodeURIComponent(this.createSvgGrid(gridColor))), 
            image = btoa(image), image = "url(data:image/svg+xml;base64," + image + ")", phase = graph.gridSize * this.scale * this.gridSteps) : image = "url(" + this.gridImage + ")";
            var x0 = 0, y0 = 0;
            if (null != graph.view.backgroundPageShape) {
                var bds = this.getBackgroundPageBounds();
                x0 = 1 + bds.x, y0 = 1 + bds.y;
            }
            position = -Math.round(phase - mxUtils.mod(this.translate.x * this.scale - x0, phase)) + "px " + -Math.round(phase - mxUtils.mod(this.translate.y * this.scale - y0, phase)) + "px";
        }
        var canvas = graph.view.canvas;
        null != canvas.ownerSVGElement && (canvas = canvas.ownerSVGElement), null != graph.view.backgroundPageShape ? (graph.view.backgroundPageShape.node.style.backgroundPosition = position, 
        graph.view.backgroundPageShape.node.style.backgroundImage = image, graph.view.backgroundPageShape.node.style.backgroundColor = color, 
        canvas.style.backgroundImage = "none", canvas.style.backgroundColor = "") : (canvas.style.backgroundPosition = position, 
        canvas.style.backgroundColor = color, canvas.style.backgroundImage = image);
    }, skGraph.prototype.defaultGraphBackground = "#ffffff", skGraph.prototype.updateGraphComponents = function() {
        null != this.container && (this.view.validateBackground(), this.container.style.overflow = this.scrollbars ? "auto" : "hidden");
    };
    var oInit = skGraph.prototype.init;
    return skGraph.prototype.init = function(container) {
        oInit.call(this, container), this.container.setAttribute("tabindex", "0"), this.container.style.overflow = mxClient.IS_TOUCH ? "hidden" : "auto", 
        this.container.style.cursor = "default", this.panningHandler.useLeftButtonForPanning = !0, 
        this.panningHandler.addListener(mxEvent.PAN_START, mxUtils.bind(this, function() {
            this.container.style.cursor = "pointer";
        })), this.panningHandler.addListener(mxEvent.PAN_END, mxUtils.bind(this, function() {
            skShell.hint && skShell.hint.hide(), this.isEnabled() && skShell.skStatus.setAndDontShowAgain("For <b>selection square</b>,<br>press <kbd>Shift <span>⇧</span></kbd> at the same time", "DONTSHOWAGAINpanOrSelect");
        })), this.addListener(mxEvent.DOUBLE_CLICK, mxUtils.bind(this, function(sender, evt) {
            var cell = evt.getProperty("cell");
            this.isEnabled() && cell && cell.isEdge() && this.clearWaypoints([ cell ]), evt.consume();
        }));
    }, skGraph.prototype.initialTopSpacing = 0, skGraph.prototype.resetScrollbars = function() {
        var bounds;
        if (mxUtils.hasScrollbars(this.container)) if (this.pageVisible) {
            var pad = this.getPagePadding();
            this.container.scrollTop = Math.floor(pad.y - this.initialTopSpacing), this.container.scrollLeft = Math.floor(Math.min(pad.x, (this.container.scrollWidth - this.container.clientWidth) / 2));
        } else {
            bounds = this.getGraphBounds();
            var width = Math.max(bounds.width, this.scrollTileSize.width * this.view.scale), height = Math.max(bounds.height, this.scrollTileSize.height * this.view.scale);
            this.container.scrollTop = Math.floor(Math.max(0, bounds.y - Math.max(20, (this.container.clientHeight - height) / 4))), 
            this.container.scrollLeft = Math.floor(Math.max(0, bounds.x - Math.max(0, (this.container.clientWidth - width) / 2)));
        } else if (this.pageVisible) {
            var b = this.view.getBackgroundPageBounds();
            this.view.setTranslate(Math.floor(Math.max(0, (this.container.clientWidth - b.width) / 2) - b.x), Math.floor(Math.max(0, (this.container.clientHeight - b.height) / 2) - b.y));
        } else bounds = this.getGraphBounds(), this.view.setTranslate(Math.floor(Math.max(0, Math.max(0, (this.container.clientWidth - bounds.width) / 2) - bounds.x)), Math.floor(Math.max(0, Math.max(20, (this.container.clientHeight - bounds.height) / 4)) - bounds.y));
    }, skGraph.prototype.setBackgroundColor = function(value) {
        this.background = value, this.fireEvent(new mxEventObject("graphBackgroundChanged", "graph", this));
    }, skGraph;
});
define('skModal/skModal_Print',['require','text!skModal/skModal_Print.html','./skModal_Settings_Pagesetup','jquery','mxPrintPreview','mxRectangle','mxUtils','skGraph/skGraph','skGraph/skGraphUtils','skUtils'],function(require) {
    "use strict";
    function skModal_Print(navigatorUI) {
        this.navigatorUI = navigatorUI;
        var that = this, sendDataToPrint = function(event) {
            if ("ready" === event.data) {
                var html = $(".mxPages").html(), e = encodeURI(html), dataToTransfer = {
                    html: e,
                    paperSize: pageSettings.paperSize,
                    printLandscape: pageSettings.landscape
                };
                event.source.postMessage(JSON.stringify(dataToTransfer), "*"), that.clearStuffs();
            }
        };
        window.removeEventListener("message", sendDataToPrint, !1), window.addEventListener("message", sendDataToPrint, !1);
        var modalPrint = navigatorUI.loadModal("skModal_Print", require("text!skModal/skModal_Print.html"), mxUtils.bind(this, function() {
            this.graph = this.navigatorUI.getCurrentGraph();
            var pageSetup = require("./skModal_Settings_Pagesetup");
            pageSettings = $.extend({}, skGraphUtils.getPageSettings(this.graph), {
                grid: !1,
                pageView: !0
            });
            var pSetup = pageSetup({
                pageView: !1,
                grid: !1,
                backgroundColor: !1
            }, pageSettings, null, function(settings) {
                pageSettings = $.extend({}, pageSettings, settings);
            });
            $(".col-sm-4", pSetup).removeClass("col-sm-4").addClass("col-sm-2"), $(".col-sm-8", pSetup).removeClass("col-sm-8").addClass("col-sm-10"), 
            $("#skPageSettings").empty().append($(".form-horizontal", pSetup)[0]), this.clearStuffs();
        }), mxUtils.bind(this, function() {
            window.removeEventListener("message", sendDataToPrint, !1), this.clearStuffs(), 
            this.graph.refresh();
        }), mxUtils.bind(this, function() {
            $(".skModalPrintPreview").on("click", mxUtils.bind(this, function() {
                if ($("#skPrintWhatProcess").is(":checked")) {
                    var preview;
                    if ("skCurrentLevel" === $("input[name=skPrintScope]:checked").val()) preview = this.createGraphForPrint(this.graph, this.graph.getDefaultParent(), !0), 
                    preview.open(null, window); else if ("skAllLevels" === $("input[name=skPrintScope]:checked").val()) {
                        var cells = this.graph.model.filterDescendants(function(cell) {
                            return 1 == cell.id || cell.isWhatboxDetailed() ? !0 : void 0;
                        }, this.graph.getDefaultParent()), that = this;
                        cells.forEach(function(e) {
                            preview = that.createGraphForPrint(that.graph, e, !0), preview.open(null, window);
                        });
                    }
                    0 === $(".mxPages").length && $(document.body).append($("<div class='mxPages'>")), 
                    $("[id^=mxPage-]").addClass("mxPage").appendTo($(".mxPages"));
                }
                $("#skPrintWhatAtt").is(":checked") && this.createAttachmentList(this.graph, "skCurrentLevel" === $("input[name=skPrintScope]:checked").val()), 
                window.open("template_printPreview.html");
            })), $(".skPagePoster").on("change", function() {
                $(this).is(":checked") ? $("#skPaperPosterNbPages").attr("disabled", !1) : $("#skPaperPosterNbPages").attr("disabled", !0);
            });
        }));
        return modalPrint;
    }
    var newGraph, $ = require("jquery"), mxPrintPreview = require("mxPrintPreview"), mxRectangle = require("mxRectangle"), mxUtils = require("mxUtils"), skGraph = require("skGraph/skGraph"), skGraphUtils = require("skGraph/skGraphUtils"), skUtils = require("skUtils"), pageSettings = {}, createDuplicateGraph = !0;
    return skModal_Print.prototype.navigatorUI = null, skModal_Print.prototype.clearStuffs = function() {
        $(".newGraph").empty().remove(), $(".mxPages").empty().remove(), createDuplicateGraph && (newGraph = null);
    }, skModal_Print.prototype.createAttachmentList = function(graph, currentLevelOnly) {
        0 === $(".mxPages").length && $(document.body).append($("<div class='mxPages'>"));
        var cells;
        cells = currentLevelOnly ? [ graph.getDefaultParent() ] : graph.model.filterDescendants(function(cell) {
            return 1 == cell.id || cell.isWhatboxDetailed() ? !0 : void 0;
        }, graph.getDefaultParent());
        var pageAdded;
        cells.forEach(function(cell) {
            if (pageAdded = !1, cell.children && cell.children.length) {
                var att_page = $("<div class='att_page'><h1 class='att_title'>" + cell.getBoxText(!0, !1) + "</h1><div class='att_page_content'></div><div>");
                cell.children.forEach(function(currentCell) {
                    if (currentCell.numberOfAttachments() > 0) {
                        pageAdded || ($(".mxPages").append(att_page), pageAdded = !0);
                        var panel = $('<div class="panel panel-default"><div class="panel-heading">' + currentCell.getBoxText(!0, !1) + '</div><ul class="list-group"></ul></div>');
                        $(".att_page_content", att_page).append(panel);
                        var ul = $("ul", panel);
                        $("attachment", currentCell.value).each(function(i, att) {
                            ul.append($('<li class="list-group-item"><p class="list-group-item-text">' + skUtils.renderMarkdown(skUtils.readValue(att)) + "</p></li>"));
                        });
                    }
                });
            }
        });
    }, skModal_Print.prototype.createGraphForPrint = function(graph, parentCell, createDuplicateGraph) {
        if (createDuplicateGraph = !0) {
            $(".newGraph").remove();
            var container = document.createElement("div");
            container.className = "newGraph", container.style.position = "absolute", container.style.top = "100%", 
            document.body.appendChild(container), newGraph = new skGraph(null, null, null, graph.getStylesheet(), graph.model.root.templates.cloneNode(!0), graph.model.root.roles.cloneNode(!0)), 
            newGraph.init(container), newGraph.labelMode = "htmlstring";
        } else newGraph = graph;
        var autoOrigin = $(".skPageFitOne").is(":checked") || $(".skPagePoster").is(":checked"), printScale = .9, scale = 1 / graph.pageScale, pf = skGraphUtils.getPageFormat(pageSettings.paperSize, pageSettings.landscape);
        if (createDuplicateGraph) {
            var cells = graph.model.getChildren(parentCell);
            cells = newGraph.cloneCells(cells), newGraph.addCells(cells), newGraph.resetToOrigin(!0, pf);
        }
        if (autoOrigin) {
            var pageCount = $(".skPageFitOne").is(":checked") ? 1 : parseInt($("#skPaperPosterNbPages").val(), 10);
            isNaN(pageCount) || (scale = mxUtils.getScaleForPageCount(pageCount, newGraph, pf));
        }
        var border = 0, x0 = 0, y0 = 0;
        if (pf = mxRectangle.fromRectangle(pf), pf.width = Math.ceil(pf.width * printScale), 
        pf.height = Math.ceil(pf.height * printScale), scale *= printScale, !autoOrigin && newGraph.pageVisible) {
            var layout = newGraph.getPageLayout();
            x0 -= layout.x * pf.width, y0 -= layout.y * pf.height;
        }
        var preview = new mxPrintPreview(newGraph, scale, pf, border, x0, y0);
        return preview.autoOrigin = autoOrigin, preview.graph.isPrintPreview = !0, preview;
    }, skModal_Print;
});
define('text!skModal/skModal_JustSkoreIt.html',[],function () { return '<div class="modal fade" id="skModal_JustSkoreIt" tabindex="-1" role="dialog" aria-labelledby="skModalJustSkoreItLabel" aria-hidden="true">\n\t<div class="modal-dialog modal-lg">\n\t\t<div class="modal-content">\n\t\t\t<div class="modal-header">\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\t\t\t\t<h4 class="modal-title" id="skModalJustSkoreItLabel">Publish online</h4>\n\t\t\t</div>\n\t\t\t<div class="modal-body">\n\t\t\t\t<div class="container-fluid">\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-md-6">\n\n\t\t\t\t\t\t\t<p>Share this Skore anonymously and secretely with friends and colleagues.</p>\n\n\t\t\t\t\t\t\t<p>Disclaimer</p>\n\n\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\tThis will create an online version of your process that will be available to anyone with the link.\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\tYou will not be able to update this once published.\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t \tIf you wish to update simply republish and receive a new link\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\tYou agree with the terms &amp; conditions of the service <a class="infoBoxStart_eula">https://www.getskore.com/end-user-license-agreement-eula/</a>.\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class="col-md-6">\n\t\t\t\t\t\t\t<div class="text-center">\n\t\t\t\t\t\t\t\t<p>&nbsp;</p>\n\t\t\t\t\t\t\t\t<p>&nbsp;</p>\n\t\t\t\t\t\t\t\t<button class="jsit_go btn btn-primary">Go!</button>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<ul class="list-group skJSITList">\n\n\t\t\t\t\t</ul>\n\n\n\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="modal-footer">\n\t\t\t\t<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n';});
define('skModal/skModal_JustSkoreIt',['require','text!skModal/skModal_JustSkoreIt.html','jquery','mxUtils','skConstants','skShell','skUtils'],function(require) {
    "use strict";
    function skModal_JustSkoreIt(navigatorUI) {
        this.navigatorUI = navigatorUI;
        var that = this;
        return navigatorUI.loadModal("skModal_JustSkoreIt", require("text!skModal/skModal_JustSkoreIt.html"), null, null, function() {
            $(".jsit_go").on("click", function(event) {
                skShell.wellDoneMessage.hey(this), that.justSkoreIt_post(mxUtils.bind(that, that.sucessJSITCallback)), 
                ga && ga("send", "event", "SkoreAction", "sideBar", "JustSkoreIt_Go"), event.stopPropagation();
            });
        });
    }
    var myData, $ = require("jquery"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skShell = require("skShell"), skUtils = require("skUtils");
    return skModal_JustSkoreIt.prototype.sucessJSITCallback = function(data) {
        myData = data;
        var li = $("<li class='list-group-item'><a class='jsit_urlLink'>Open...</a> &nbsp;<a style='display:none;' class='jsit_copyClipbard'>Copy link to clipboard</a> &nbsp;<a class='jsit_email'>Email link</a> &nbsp;<input class='jsit_urlInput selectionAllowed' type='text' /><br/><span class='jsit_timeStamp'></span></li>");
        $(".skJSITList").prepend(li), $(".jsit_timeStamp", li).html("Published: " + new Date(myData.created_at).toString()), 
        $(".jsit_urlLink", li).on("click", function() {
            skUtils.openURL(skConstants.justSkoreItBaseUrl + myData.id);
        });
        var $url = $(".jsit_urlInput", li).val(skConstants.justSkoreItBaseUrl + myData.id), $input = $url;
        $input.select(), $input.mouseup(function() {
            return $input.unbind("mouseup"), !1;
        }), $(".jsit_email", li).on("click", function() {
            var s = "mailto:?subject=Skore&body=%0D%0AHello, %0D%0AHere is a Skore I just made : " + skConstants.justSkoreItBaseUrl + myData.id + "%0D%0A%0D%0A Skore app is a simple way to visualize how things work. Take a look for yourself.%0D%0A%0D%0ACheers, ";
            skUtils.openURL(s);
        }), $(".jsit_go").html("publish again (new link!)");
    }, skModal_JustSkoreIt.prototype.navigatorUI = null, skModal_JustSkoreIt.prototype.justSkoreIt_post = function(callback) {
        var data = mxUtils.getPrettyXml(this.navigatorUI.getCurrentGraph().getGraphXml()), visibility = !1, key = {
            description: "Created with Skore, see https://www.getskore.com",
            "public": visibility
        }, deneme = {};
        deneme["gist.skore"] = {
            content: "" + encodeURIComponent(data)
        }, key.files = deneme;
        var type, url;
        return type = "POST", url = "https://api.github.com/gists", $.ajax({
            url: url,
            type: type,
            dataType: "json",
            data: JSON.stringify(key),
            success: function(data) {
                callback(data);
            },
            error: function() {
                skShell.skStatus.set("error :-(");
            }
        });
    }, skModal_JustSkoreIt;
});
define('text!skModal/skModal_Settings.html',[],function () { return '<div class="modal fade" id="skModal_Settings" tabindex="-1" role="dialog" aria-labelledby="skModalSettingsLabel" aria-hidden="true">\n\t<div class="modal-dialog modal-lg">\n\t\t<div class="modal-content">\n\t\t\t<div class="modal-header">\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\t\t\t\t<h4 class="modal-title" id="skModalSettingsLabel">Skore Settings</h4>\n\t\t\t</div>\n\t\t\t<div class="modal-body">\n\t\t\t\t<div class="container-fluid">\n\t\t\t\t\t<div class="row">\n\t\t\t\t\t\t<div class="col-md-4 skSettings_pagesetup"></div>\n\t\t\t\t\t\t<div class="col-md-4 skSettings_mrt"></div>\n\t\t\t\t\t\t<div class="col-md-4 skSettings_handoverhighlight"></div>\n\t\t\t\t\t\t<div class="col-md-4 skSettings_htmlExport"></div>\n\t\t\t\t\t\t<div class="col-md-4 skSettings_background"></div>\n\t\t\t\t\t\t<div class="col-md-4 skSettings_mappingMode"></div>\n\t\t\t\t\t\t<div class="col-md-4 skSettings_spellcheck"></div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="modal-footer">\n\t\t\t\t<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n';});
define('skGraphEditorUI/skHandoverHighlight',['require','jquery','mxArrowConnector','mxCell','mxCellRenderer','mxConstants','mxEvent','mxUtils','skConstants','skShell','skGraph/skColors','skUtils'],function(require) {
    "use strict";
    var $ = require("jquery"), mxArrowConnector = require("mxArrowConnector"), mxCell = require("mxCell"), mxCellRenderer = require("mxCellRenderer"), mxConstants = require("mxConstants"), mxEvent = require("mxEvent"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skShell = require("skShell"), skColors = require("skGraph/skColors"), skUtils = require("skUtils");
    return function(graph) {
        function FlexArrowShape() {
            mxArrowConnector.call(this), this.spacing = 0;
        }
        mxUtils.extend(FlexArrowShape, mxArrowConnector), FlexArrowShape.prototype.defaultWidth = 2, 
        FlexArrowShape.prototype.defaultArrowWidth = 10, FlexArrowShape.prototype.getStartArrowWidth = function() {
            return this.getEdgeWidth() + mxUtils.getNumber(this.style, skConstants.START_WIDTH, this.defaultArrowWidth);
        }, FlexArrowShape.prototype.getEndArrowWidth = function() {
            return this.getEdgeWidth() + mxUtils.getNumber(this.style, skConstants.END_WIDTH, this.defaultArrowWidth);
        }, FlexArrowShape.prototype.getEdgeWidth = function() {
            return mxUtils.getNumber(this.style, "width", this.defaultWidth) + Math.max(0, this.strokewidth - 1);
        }, mxCellRenderer.prototype.defaultShapes[skConstants.FLEX_ARROW] = FlexArrowShape, 
        skShell.ext_lineHighlightLoaded = !0;
        var highlightColor = skColors.HANDOVER;
        if (graph.model.root.handoverhighlight) {
            var highlightColorSetup = skUtils.readValue(graph.model.root.handoverhighlight, "color");
            "" === highlightColorSetup ? graph.model.root.handoverhighlight.setAttribute("color", skColors.HANDOVER) : highlightColor = highlightColorSetup;
        }
        var highlightedLineStyle = {};
        highlightedLineStyle[mxConstants.STYLE_ENDSIZE] = 5, highlightedLineStyle[mxConstants.STYLE_STROKECOLOR] = highlightColor, 
        highlightedLineStyle[skConstants.END_WIDTH] = 8, highlightedLineStyle[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_TOP, 
        highlightedLineStyle[mxConstants.STYLE_SHAPE] = skConstants.FLEX_ARROW, graph.getStylesheet().putCellStyle(skConstants.LINE_HIGHLIGHT, highlightedLineStyle), 
        mxCell.prototype.highlightLines = function(actuallyUpdateStyle) {
            if (!this || this.isWhybox()) {
                actuallyUpdateStyle = actuallyUpdateStyle && skShell.ext_lineHighlightLoaded;
                var lines2 = [], lines4 = [], currentWhybox = this, connections = [];
                if ($(currentWhybox.edges).each(function(i, edge) {
                    edge.source == currentWhybox ? lines4.push(edge) : edge.target == currentWhybox && lines2.push(edge);
                }), actuallyUpdateStyle) {
                    var tmp = lines4.concat(lines2);
                    $(tmp).each(function(i, edge) {
                        graph.setCellStyle(mxUtils.removeAllStylenames(edge.style), [ edge ]);
                    }), graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, null, [ this ]), graph.setCellStyles("handover", null, [ this ]);
                }
                var box1resource, box5resource;
                return $(lines2).each(function(i, edge2) {
                    box1resource = edge2.source.getBoxRoles(0)[0], $(lines4).each(function(i, edge4) {
                        box5resource = edge4.target.getBoxRoles(0)[0], box1resource != box5resource && (actuallyUpdateStyle && (skUtils.setCellsStyleName(graph.model, [ edge2, edge4 ], skConstants.LINE_HIGHLIGHT), 
                        graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, highlightColor, [ currentWhybox ]), 
                        graph.setCellStyles("handover", "1", [ currentWhybox ])), connections.push({
                            code: "" + edge2.source.id + " " + currentWhybox.id + " " + edge4.target.id,
                            sourceCell: edge2.source,
                            sourceResource: box1resource,
                            whybox: currentWhybox,
                            destinationCell: edge4.target,
                            destinationResource: box5resource
                        }));
                    });
                }), connections;
            }
        };
        var filter = function(cell) {
            return cell.isWhybox();
        };
        graph.model.beginUpdate();
        try {
            for (var yb = graph.model.filterDescendants(filter), i = 0; i < yb.length; i++) yb[i].highlightLines(!0);
        } finally {
            graph.model.endUpdate();
        }
        graph.addListener(mxEvent.CELL_CONNECTED, function(sender, event) {
            event.getProperty("source") || (event.getProperty("edge").source.highlightLines(!0), 
            event.getProperty("edge").target.highlightLines(!0));
        }), graph.addListener(mxEvent.LABEL_CHANGED, function(event, sender) {
            var cell = sender.getProperty("cell");
            $(cell.edges).each(function(i, edge) {
                edge.source.isWhybox() ? edge.source.highlightLines(!0) : edge.target.isWhybox() && edge.target.highlightLines(!0);
            });
        }), graph.addListener(mxEvent.CELLS_REMOVED, function(event, sender) {
            var cells = sender.getProperty("cells");
            $(cells).each(function(i, cell) {
                cell.isWhatbox && $(cell.edges).each(function(i, edge) {
                    edge.source.isWhybox() ? edge.source.highlightLines(!0) : edge.target.isWhybox() && edge.target.highlightLines(!0);
                }), cell.isEdge && (cell.source && cell.source.isWhybox() ? cell.source.highlightLines(!0) : cell.target && cell.target.isWhybox() && cell.target.highlightLines(!0));
            });
        });
    };
});
define('skExtensions/skExtensions',['require','skConstants','skUtils','skGraphEditorUI/skHandoverHighlight'],function(require) {
    "use strict";
    function skExtensions(graph) {
        this.graph = graph, this.loadedExtensions = [];
    }
    var skConstants = require("skConstants"), skUtils = require("skUtils");
    return skExtensions.prototype.loadedExtensions = null, skExtensions.prototype.graph = null, 
    skExtensions.prototype.loadExistingExtensions = function() {
        var node = this.graph.getGraphXml(), exts = node.getElementsByTagName("extension");
        if (exts.length > 0) for (var i = 0; i < exts.length; i++) {
            var ext = exts[i];
            this.load(ext);
        }
    }, skExtensions.prototype.load = function(node) {
        var extensionName = skUtils.readValue(node);
        this.loadedExtensions.hasOwnProperty(extensionName) || this.loadedExtensions[extensionName] === !1 || this["load_" + extensionName]();
    }, skExtensions.prototype.load_mrt = function() {}, skExtensions.prototype.load_handoverhighlight = function() {
        this.loadedExtensions[skConstants.HANDHOVER_HIGHLIGHT] || (require("skGraphEditorUI/skHandoverHighlight")(this.graph), 
        this.loadedExtensions[skConstants.HANDHOVER_HIGHLIGHT] = !0);
    }, skExtensions;
});
define('skModal/skModal_Settings_Handoverhighlight',['require','jquery','mxConstants','mxUtils','skConstants','skShell','skExtensions/skExtensions'],function(require) {
    "use strict";
    var $ = require("jquery"), mxConstants = require("mxConstants"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skShell = require("skShell");
    return function(navigatorUI) {
        var graph = navigatorUI.getCurrentGraph(), alreadyOn = " ";
        graph.model.root.handoverhighlight && (alreadyOn = " checked ");
        var div = $('<div class="panel panel-default"><div class="panel-heading"><h3 class="panel-title">Handover highlight</h3><span class="experimental">Experimental</span></div><div class="panel-body"><div class="checkbox"><label><input aria-describedby="skLHDesc" class="skLineHighlight" type="checkbox" ' + alreadyOn + '>Enable handover highlight</label></div><span id="skLHDesc" class="help-block"><ul><li>Lines & whyboxes connecting activities performed by different roles will be highlighted.</li><li>If you have enabled multi resources, only the <strong>first role</strong> of an activity is taken into account for handover.</li></ul></span></div></div>');
        $(".skLineHighlight", div).on("change", function() {
            if ($(this).is(":checked")) {
                if (graph.model.root.handoverhighlight = mxUtils.createXmlDocument().createElement("extension"), 
                graph.model.root.handoverhighlight.setAttribute(skConstants.VALUE, "handoverhighlight"), 
                !graph.extensions) {
                    var skExtensions = require("skExtensions/skExtensions");
                    graph.extensions = new skExtensions(graph);
                }
                graph.extensions.load_handoverhighlight(), skShell.skStatus.set("Handover highlight extension enabled"), 
                ga && ga("send", "event", "SkoreAction", "settingsLineHighlight", "loadLineHighlight");
            } else {
                graph.model.beginUpdate();
                try {
                    graph.model.filterDescendants(function(cell) {
                        cell.isEdge() && cell.style && mxUtils.getStylename(cell.style) === skConstants.LINE_HIGHLIGHT ? graph.setCellStyle(mxUtils.removeAllStylenames(cell.style), [ cell ]) : cell.isWhybox() && graph.setCellStyles(mxConstants.STYLE_STROKECOLOR, null, [ cell ]);
                    });
                } finally {
                    graph.model.endUpdate();
                }
                graph.model.root.handoverhighlight = null, delete graph.extensions.loadedExtensions[skConstants.HANDHOVER_HIGHLIGHT], 
                skShell.ext_lineHighlightLoaded = !1;
            }
        }), $(".skSettings_handoverhighlight").append($(div));
    };
});
define('skModal/skModal_Settings_MRT',['require','jquery','skConstants','skUtils'],function(require) {
    "use strict";
    var $ = require("jquery"), skConstants = require("skConstants"), skUtils = require("skUtils");
    return function(graph) {
        var div = $('<div class="panel panel-default"><div class="panel-heading"><h3 class="panel-title">Responsibility model</h3></div><div class="panel-body"><div class="radio"><label><input name="formType" data-formtype="' + skConstants.TEMPLATE_BASE + '" type="radio" >Default (multi-roles)</label></div><div class="radio"><label><input name="formType" data-formtype="' + skConstants.RESP_RACI + '" type="radio"  aria-describedby="skHelpBlockRACI" >Enable RACI</label><span id="skHelpBlockRACI" class="help-block">RACI stands for Responsible Accountable Consulted and Informed</span></div><div class="radio"><label><input name="formType" data-formtype="' + skConstants.RESP_RATSI + '" type="radio"  aria-describedby="skHelpBlockRATSI" >Enable RATSI</label><span id="skHelpBlockRATSI" class="help-block">RATSI stands for Responsible, Authority, Task, Support & Informed. Read our <a class="skExtLink" href="https://www.getskore.com/roles-responsibilities-in-process-new-use-case/">blog post about RATSI</a> to learn more.</span></div><div class="radio"><label><input name="formType" data-formtype="' + skConstants.TEMPLATE_CUSTOM + '" type="radio"  aria-describedby="skHelpBlockCustom" >Custom </label><span id="skHelpBlockCustom" class="help-block"><a class="skBlogCustom">Learn how to setup custom tags here</a>.</span></div><div class="customizer" ><textarea class="selectionAllowed form-control" rows="5" aria-describedby="helpTACust"></textarea><span id="helpTACust" class="help-block">Paste your code above.</span><a class="btn btn-default okCustom" role="button">Save custom template</a><br/><span class="jsonstatus"></span></div></div></div>');
        $(".skBlogCustom", div).on("click", function() {
            skUtils.openURL("https://www.getskore.com/manual/how-to-add-custom-tags-to-roles-in-skore-app/");
        }), $("[data-formtype=" + graph.templateManager.currentTemplateForForm.responsibility + "]", div).prop("checked", !0), 
        $(".customizer", div).toggle(graph.templateManager.currentTemplateForForm.responsibility === skConstants.TEMPLATE_CUSTOM), 
        $(".customizer textarea", div).html(JSON.stringify(graph.templateManager.getTemplate(skConstants.RESPONSIBILITY), null, 2)), 
        $("input", div).on("change", function() {
            var t = $(this).data("formtype");
            if ($(".customizer", div).toggle(t === skConstants.TEMPLATE_CUSTOM), t !== skConstants.TEMPLATE_CUSTOM) {
                graph.templateManager.setTemplateForForm(skConstants.RESPONSIBILITY, t);
                var obj = graph.templateManager.getTemplate(skConstants.RESPONSIBILITY);
                delete obj.isBuiltIn, $(".customizer textarea", div).html(JSON.stringify(obj, null, 2));
            }
        });
        var validateJSON = function(text) {
            try {
                var c = $.parseJSON(text);
                return $(".jsonstatus", div).html("JSON ok!"), c;
            } catch (err) {
                return $(".jsonstatus", div).html("JSON invalid"), !1;
            }
        };
        $("textarea", div).on("keyup", function() {
            $(".jsonstatus", div).empty();
        }), $(".okCustom", div).on("click", function() {
            var text = $("textarea", div).val();
            validateJSON(text) && (graph.templateManager.setTemplateForForm(skConstants.RESPONSIBILITY, skConstants.TEMPLATE_CUSTOM, text), 
            $(".jsonstatus", div).html($(".jsonstatus", div).html() + " Saved!"));
        }), $(".skSettings_mrt").append($(div));
    };
});
define('text!skModal/skModal_Settings_Background.html',[],function () { return '<div class="panel panel-default">\n\t<div class="panel-heading">\n\t\t<h3 class="panel-title">Skore background</h3>\n\t</div>\n\t<div class="panel-body">\n\t\t<div class="form-group">\n\n\t\t\t<div class="radio">\n\t\t\t\t<label>\n\t\t\t\t\t<input type="radio" name="skSettingBg" id="skSettingBg1" value="happy" checked>\n\t\t\t\t\tHappy background\n\t\t\t\t</label>\n\t\t\t</div>\n\t\t\t<div class="radio">\n\t\t\t\t<label>\n\t\t\t\t\t<input type="radio" name="skSettingBg" id="skSettingBg2" value="neutral">\n\t\t\t\t\tNeutral background\n\t\t\t\t</label>\n\t\t\t</div>\n\n\t\t</div>\n\t</div>\n</div>\n';});
define('skModal/skModal_Settings_Background',['require','jquery','text!./skModal_Settings_Background.html'],function(require) {
    "use strict";
    var $ = require("jquery");
    return function() {
        var div = require("text!./skModal_Settings_Background.html");
        $(".skSettings_background").empty().append($(div)), $(document.body).hasClass("mainContainerBackground") ? $("#skSettingBg1").prop("checked", !0) : $("#skSettingBg2").prop("checked", !0), 
        $("input[name=skSettingBg]").on("change", function() {
            var bgType = $("input[name=skSettingBg]:checked").val();
            switch (bgType) {
              case "neutral":
                $(document.body).removeClass("mainContainerBackground");
                break;

              case "happy":
                $(document.body).addClass("mainContainerBackground");
            }
            window.localStorage.setItem("skBg", bgType);
        });
    };
});
define('skModal/skModal_Settings',['require','text!./skModal_Settings.html','./skModal_Settings_Pagesetup','./skModal_Settings_Handoverhighlight','./skModal_Settings_MRT','./skModal_Settings_Background','jquery','mxEventObject'],function(require) {
    "use strict";
    function skModal_Settings(navigatorUI) {
        return navigatorUI.loadModal("skModal_Settings", require("text!./skModal_Settings.html"), function() {
            $(".skSettings_pagesetup, .skSettings_mrt, .skSettings_handoverhighlight").empty();
            var graph = navigatorUI.getCurrentGraph(), pageSetup = require("./skModal_Settings_Pagesetup");
            $(".skSettings_pagesetup").append(pageSetup(null, null, navigatorUI.getCurrentGraph(), function(settings) {
                navigatorUI.graphManager.fireEvent(new mxEventObject("pageSettingsChanged", "graph", navigatorUI.getCurrentGraph(), "settings", settings));
            })), require("./skModal_Settings_Handoverhighlight")(navigatorUI), require("./skModal_Settings_MRT")(graph), 
            require("./skModal_Settings_Background")(), ga && ga("send", "event", "SkoreAction", "sideBar", "Settings");
        }, null, null);
    }
    var $ = require("jquery"), mxEventObject = require("mxEventObject");
    return skModal_Settings;
});
define('skNavigatorUI/skThumbnail',['require','jquery','canvg','mxOutline'],function(require) {
    "use strict";
    function skThumbnail() {}
    var $ = require("jquery"), canvg = require("canvg"), mxOutline = require("mxOutline");
    return skThumbnail.prototype.createDataURL = function(graph, cell) {
        var tmp = {};
        tmp.t = {}, tmp.t.outlineTMP = new mxOutline(graph), tmp.t.outlineTMPContainer = document.createElement("div");
        var imgSrc;
        document.body.appendChild(tmp.t.outlineTMPContainer), tmp.t.outlineTMPContainer.style.width = "300px", 
        tmp.t.outlineTMPContainer.style.height = "300px", tmp.t.outlineTMP.init(tmp.t.outlineTMPContainer), 
        null != cell && tmp.t.outlineTMP.outline.view.setCurrentRoot(cell), tmp.t.g = tmp.t.outlineTMP.outline.getView().drawPane, 
        tmp.t.svg = tmp.t.g.viewportElement, tmp.t.svg.setAttribute("style", "");
        var viewBoxHeight = "200", viewBoxWidth = "200";
        tmp.t.svg.setAttribute("width", viewBoxWidth), tmp.t.svg.setAttribute("height", viewBoxHeight);
        var bbox = tmp.t.g.getBBox();
        tmp.t.svg.setAttribute("viewBox", "" + bbox.x + " " + bbox.y + " " + bbox.width + " " + bbox.height), 
        tmp.t.canvas = document.createElement("canvas"), document.body.appendChild(tmp.t.canvas), 
        tmp.t.canvas.setAttribute("width", viewBoxWidth), tmp.t.canvas.setAttribute("height", viewBoxHeight), 
        tmp.t.o = tmp.t.outlineTMP.outline.getView().getOverlayPane(), tmp.t.o.parentNode.removeChild(tmp.t.o), 
        tmp.t.x = new XMLSerializer(), $("[stroke-opacity], [fill-opacity]", tmp.t.svg).each(function(index, value) {
            value.setAttribute("stroke-opacity", ""), value.setAttribute("fill-opacity", "");
        }), canvg(tmp.t.canvas, tmp.t.x.serializeToString(tmp.t.svg), {
            ignoreClear: !0
        }), tmp.t.destinationCanvas = document.createElement("canvas"), tmp.t.destinationCanvas.width = tmp.t.canvas.width + 20, 
        tmp.t.destinationCanvas.height = tmp.t.canvas.height + 20, tmp.t.destCtx = tmp.t.destinationCanvas.getContext("2d"), 
        tmp.t.destCtx.fillStyle = "#FFFFFF", tmp.t.destCtx.fillRect(0, 0, tmp.t.canvas.width + 20, tmp.t.canvas.height + 20), 
        tmp.t.destCtx.drawImage(tmp.t.canvas, 10, 10), imgSrc = tmp.t.destinationCanvas.toDataURL(), 
        tmp.t.destCtx.clearRect(0, 0, tmp.t.destinationCanvas.width, tmp.t.destinationCanvas.height);
        var ctx = tmp.t.canvas.getContext("2d");
        return ctx.clearRect(0, 0, tmp.t.canvas.width, tmp.t.canvas.height), tmp.t.canvas.parentElement.removeChild(tmp.t.canvas), 
        tmp.t.outlineTMPContainer.parentElement.removeChild(tmp.t.outlineTMPContainer), 
        delete tmp.t, imgSrc;
    }, skThumbnail.prototype.createThumbnail = function(cell) {
        var imgSrc = this.createDataURL(cell);
        return imgSrc = imgSrc.split(",")[1], imgSrc = imgSrc.replace(/\+/g, "-").replace(/\//g, "_"), 
        {
            image: imgSrc,
            mimeType: "image/png"
        };
    }, skThumbnail;
});
define('skModal/skModal_SocialExport',['require','jquery','mxUtils','skShell','skGraph/skGraphUtils','skNavigatorUI/skThumbnail','skUtils'],function(require) {
    "use strict";
    function skModal_SocialExport(navigatorUI) {
    }
    require("jquery"), require("mxUtils"), require("skShell"), require("skGraph/skGraphUtils"), 
    require("skNavigatorUI/skThumbnail"), require("skUtils");
    return skModal_SocialExport;
});
define('text!skModal/skModal_kbdShortcuts.html',[],function () { return '<div id="skFooter_kbdShortcuts" class="modal sideModal sideRight" tabindex="-1" role="dialog" aria-labelledby="skKBDModalLabel" data-backdrop="" aria-hidden="true">\n    <div class="modal-dialog modal-sm">\n        <div class="modal-content">\n            <div class="modal-header">\n                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n                <h4 class="modal-title" id="skKBDModalLabel">Keyboard shortcuts</h4>\n            </div>\n\n            <div class="modal-body ">\n\n                <h4>Navigate in Skore</h4>\n                <table class="table table-bordered table-hover">\n\n                    <tr>\n                        <td>\n                            Click &amp; drag\n                        </td>\n                        <td>\n                            Move canvas\n                        </td>\n                    </tr>\n                    <tr>\n                        <td>\n                            <kbd>Shift&nbsp;<span>⇧</span></kbd> + Drag\n                        </td>\n                        <td>\n                            Selection square\n                        </td>\n                    </tr>\n\n                    <tbody>\n                        <tr>\n                            <td>\n                                <kbd>Shift&nbsp;<span>⇧</span></kbd> + Scroll\n                            </td>\n                            <td>Zooms</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>+</kbd> / <kbd>-</kbd> / <kbd>=</kbd>\n                            </td>\n                            <td>Zooms… bigger / smaller / standard size</td>\n                        </tr>\n                    </tbody>\n                </table>\n\n\n\n                <h4>Create box rapidly</h4>\n                <table class="table table-bordered table-hover">\n                    <tbody>\n                        <tr>\n                            <td>\n                                <kbd>Space</kbd>\n                            </td>\n                            <td>Creates the next box</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>W</kbd> + Click\n                            </td>\n                            <td>Create a whatbox</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Y</kbd> + Click\n                            </td>\n                            <td>Create a whybox</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>N</kbd> + Click\n                            </td>\n                            <td>Create a note</td>\n                        </tr>\n                    </tbody>\n                </table>\n\n                <h4>Edit box content</h4>\n                <table class="table table-bordered table-hover">\n                    <tbody>\n                        <tr>\n                            <td>\n                                <kbd>Enter&nbsp;<span>↩</span></kbd> or <kbd>F2</kbd>\n                            </td>\n                            <td>Enters in edit mode<br>-or- validates changes and exit edit mode</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Esc</kbd>\n                            </td>\n                            <td>Exits edit mode without saving the changes</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Tab&nbsp;<span>⇥</span></kbd>\n                            </td>\n                            <td>Goes to next field<br>-or- validates changes and exit edit mode</td>\n                        </tr>\n                    </tbody>\n                </table>\n                <p data-skaction="modal_formatting" class="alert alert-info text-center skTextFormat">Learn more about <a>text formatting</a>.</p>\n\n                <h4>Box order &amp; alignment</h4>\n\n                <table class="table table-bordered table-hover">\n                    <tbody>\n                        <tr>\n                            <td style="width:30%">\n                                <kbd>B</kbd>\n                            </td>\n                            <td>\n                            <svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n                                <rect style="stroke:gray" class="block blockFull" shape-rendering="crispEdges" x="10" y="8" width="9" height="9" stroke-width="1"></rect>\n                                <rect  class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="9" height="9" fill="black" stroke-width="1"></rect>\n                            </svg>\n                                Send selected box(es) to <strong>b</strong>ack\n                            </td>\n                        </tr>\n                        <tr>\n                            <td style="width:30%">\n                                <kbd>F</kbd>\n                            </td>\n                            <td>\n                                <svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\n                                <rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="9" height="9"  stroke-width="1"></rect>\n                                <rect style="stroke:gray" class="block blockFull" shape-rendering="crispEdges" x="10" y="8" width="9" height="9" stroke-width="1"></rect>\n                            </svg>\n                            Bring selected box(s) to <strong>f</strong>ront\n                            </td>\n                        </tr>\n                        <tr>\n                            <td style="width:30%">\n                                <kbd>T</kbd>\n                            </td>\n                            <td>\n                                <svg class="formatBtn" width="20" height="18" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n                                <line class="block" shape-rendering="crispEdges" x1="2" x2="18" y1="2" y2="2" fill="" stroke-width="1"></line>\n                                <rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="4" height="9" fill="" stroke-width="1"></rect>\n                                <rect class="block blockEmpty" shape-rendering="crispEdges" x="10" y="4" width="4" height="12" fill="" stroke-width="1"></rect>\n                            </svg>\n                            Align <strong>t</strong>ops\n                            </td>\n                        </tr>\n                        <tr>\n                            <td style="width:30%">\n                                <kbd>C</kbd>\n                            </td>\n                            <td>\n                                <svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n                                    <line class="block" shape-rendering="crispEdges" x1="10" x2="10" y1="2" y2="18" fill="" stroke-width="1"></line>\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="12" height="4" fill="" stroke-width="1"></rect>\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="6" y="10" width="8" height="4" fill="" stroke-width="1"></rect>\n                                </svg>\n                            Align <strong>c</strong>enters\n                            </td>\n                        </tr>\n                        <tr>\n                            <td style="width:30%">\n                                <kbd>L</kbd>\n                            </td>\n                            <td>\n                                <svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n                                    <line class="block" shape-rendering="crispEdges" x1="10" x2="10" y1="2" y2="18" fill="" stroke-width="1"></line>\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="12" height="4" fill="" stroke-width="1"></rect>\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="10" width="8" height="4" fill="" stroke-width="1"></rect>\n                                </svg>\n                            Align <strong>l</strong>eft\n                            </td>\n                        </tr>\n                        <tr>\n                            <td style="width:30%">\n                                <kbd>R</kbd>\n                            </td>\n                            <td>\n                                <svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n                                    <line class="block" shape-rendering="crispEdges" x1="10" x2="10" y1="2" y2="18" fill="" stroke-width="1"></line>\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="8" y="4" width="12" height="4" fill="" stroke-width="1"></rect>\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="12" y="10" width="8" height="4" fill="" stroke-width="1"></rect>\n                                </svg>\n                            Align <strong>r</strong>ight\n                            </td>\n                        </tr>\n                        <tr>\n                            <td style="width:30%">\n                                <kbd>V</kbd>\n                            </td>\n                            <td>\n                                <svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="0" y="0" width="12" height="4" fill="" stroke-width="1"></rect>\n\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="0" y="6" width="12" height="4" fill="" stroke-width="1"></rect>\n\n                                    <rect class="block blockEmpty" shape-rendering="crispEdges" x="0" y="12" width="12" height="4" fill="" stroke-width="1"></rect>\n\n                                </svg>\n                            Space out <strong>v</strong>ertically\n                            </td>\n                        </tr>\n                        <tr>\n                            <td style="width:30%">\n                                <kbd>H</kbd>\n                            </td>\n                            <td>\n                            <svg class="formatBtn" width="20" height="18" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\n                                <rect class="block blockEmpty" shape-rendering="crispEdges" x="0" y="0" width="4" height="12" fill="" stroke-width="1"></rect>\n\n                                <rect class="block blockEmpty" shape-rendering="crispEdges" x="6" y="0" width="4" height="12" fill="" stroke-width="1"></rect>\n\n                                <rect class="block blockEmpty" shape-rendering="crispEdges" x="12" y="0" width="4" height="12" fill="" stroke-width="1"></rect>\n\n                            </svg>\n                            Space out <strong>h</strong>orizontally\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n\n                <h4>Full keyboard navigation</h4>\n                <table class="table table-bordered table-hover">\n                    <tbody>\n                        <tr>\n                            <td>\n                                <kbd>Tab&nbsp;<span>⇥</span></kbd>\n                            </td>\n                            <td>Selects the next box</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Shift&nbsp;<span>⇧</span></kbd> + <kbd>Tab&nbsp;<span>⇥</span></kbd>\n                            </td>\n                            <td>Selects the previous box</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Enter&nbsp;<span>↩</span></kbd>\n                            </td>\n                            <td>Enter edit mode</td>\n                        </tr>\n                    </tbody>\n                </table>\n\n\n\n                <h4>Advanced keyboard navigation</h4>\n\n                <table class="table table-bordered table-hover">\n                    <tbody>\n                        <tr>\n                            <td>\n                                <kbd>Shift&nbsp;<span>⇧</span></kbd> + <kbd>Space</kbd>\n                            </td>\n                            <td>Creates a box before (on the same line)</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Ctrl</kbd> + <kbd>Space</kbd>\n                            </td>\n                            <td>Creates a box on the next line</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Tab&nbsp;<span>⇥</span></kbd>\n                            </td>\n                            <td>Selects the next box (like clicking)</td>\n                        </tr>\n\n                        <tr>\n                            <td>\n                                <kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>G</kbd>\n                            </td>\n                            <td>Groups</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>U</kbd>\n                            </td>\n                            <td>Ungroups</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>D</kbd>\n                            </td>\n                            <td>Duplicates</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Page down&nbsp;<span>⇟</span> </kbd>\n                            </td>\n                            <td>Enters detail of selected What box</td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <kbd>Page up&nbsp;<span>⇞</span> </kbd>\n                            </td>\n                            <td>Exits detail of what box</td>\n                        </tr>\n                    </tbody>\n                </table>\n\n\n                <h4>Stylesheet</h4>\n                <table class="table table-bordered table-hover">\n                    <tbody class="rowSpan">\n                        <tr>\n                            <td><kbd>S</kbd><br>(No box selected)</td>\n                            <td>Opens stylesheet editor</td>\n                        </tr>\n                    </tbody>\n                    <tbody class="rowSpan">\n                        <tr>\n                            <td><kbd>S</kbd><br>(Boxes selected)</td>\n                            <td>Opens style editor for the box(es)</td>\n                        </tr>\n                    </tbody>\n                </table>\n\n                <h4>On the outline (in the bottom left corner)</h4>\n                <table class="table table-bordered table-hover">\n                    <tbody class="rowSpan">\n                        <tr>\n                            <td>Scroll on outline</td>\n                            <td>Zooms</td>\n                        </tr>\n                        <tr>\n                            <td>Resize red box</td>\n                            <td>Zooms</td>\n                        </tr>\n                    </tbody>\n                    <tbody>\n                        <tr>\n                            <td>Drag and drop of highlight</td>\n                            <td>Pans</td>\n                        </tr>\n                    </tbody>\n                    <tbody>\n                        <tr>\n                            <td><kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>K</kbd></td>\n                            <td>Show / Hide Stickynotes</td>\n                        </tr>\n                    </tbody>\n                </table>\n\n                <h4>Other</h4>\n                <table class="table table-bordered table-hover">\n                    <tbody>\n                        <tr>\n                            <td><kbd>X</kbd></td>\n                            <td>Open XML modal</td>\n                        </tr>\n                    </tbody>\n                </table>\n\n            </div>\n            <div class="modal-footer">\n                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n            </div>\n        </div>\n    </div>\n</div>\n';});
define('skModal/skModal_kbdShortcuts',['require','text!skModal/skModal_kbdShortcuts.html'],function(require) {
    "use strict";
    function skModal_kdbShortcuts(navigatorUI) {
        return navigatorUI.loadModal("skModal_kdbShortcuts", require("text!skModal/skModal_kbdShortcuts.html"));
    }
    return skModal_kdbShortcuts;
});
define('text!skModal/skModal_Formatting.html',[],function () { return '<div id="skModal_Formatting" class="modal sideModal sideRight" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false" aria-labelledby="skModalFormatLabel" aria-hidden="true">\n    <div class="modal-dialog">\n        <div class="modal-content">\n            <div class="modal-header">\n                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n                <h4 class="modal-title" id="skModalFormatLabel">Text formatting</h4>\n            </div>\n\n            <div class="modal-body">\n\n                <p>\n                    These can be used in any "large" text fields.\n                </p>\n\n                <h4>New lines...</h4>\n\n\n                <table class="table table-bordered">\n                    <thead>\n                        <tr>\n                            <td colspan="2">Note on line returns</td>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr>\n                            <td>\n                                <p><strong>For whatbox / whybox</strong></p>\n                                <p>\n                                    Use <kbd>Shift&nbsp;⇧</kbd> + <kbd>Enter&nbsp;↩ </kbd>\n                                </p>\n                                <p>to make a new line</p>\n                            </td>\n                            <td>\n                                <p><strong>Everywhere</strong></p>\n                                <p>Leave a blank line to create a new paragraph</p>\n                                <p>Example:</p>\n                                <p>\n                                    <pre>My first line of text<kbd>↩</kbd><br><kbd>↩</kbd>(empty line)<br>My second line of text</pre>\n                                </p>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n\n                <h4>Basic formatting</h4>\n\n                <table class="table table-bordered">\n                    <thead>\n                        <tr>\n                            <th>Write...</th>\n                            <th>And this renders as...</th>\n                        </tr>\n                    </thead>\n\n                    <tbody>\n\n                        <tr>\n                            <td><code>#</code>Header 1<br/><code>##</code>Header 2</td>\n                            <td><h1>Header 1<h1><h2>Header 2</h2></td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <code>*<span class="explicitSpacer">˽</span></code>this<br/>\n                                <code>*<span class="explicitSpacer">˽</span></code>is a<br/>\n                                <code>*<span class="explicitSpacer">˽</span></code>bullet points<br/>\n                                <code>*<span class="explicitSpacer">˽</span></code>list\n                            </td>\n                            <td>\n                                <ul><li>this</li><li>is a</li><li>bullet points</li><li>list</li></ul>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                <code>1.<span class="explicitSpacer">˽</span></code>and this<br/>\n                                <code>1.<span class="explicitSpacer">˽</span></code>renders<br/>\n                                <code>1.<span class="explicitSpacer">˽</span></code>as<br/>\n                                <code>1.<span class="explicitSpacer">˽</span></code>a numbered<br/>\n                                <code>1.<span class="explicitSpacer">˽</span></code>list\n                            </td>\n                            <td>\n                                <ol><li>and this</li><li>renders</li><li>as</li><li>a numbered</li><li>list</li></ol>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>Use <code>*</code>italic<code>*</code> text\n                            </td>\n                            <td>Use <i>italic</i> text\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>Use <code>**</code>bold<code>**</code> text\n                            </td>\n                            <td>Use <b>bold</b> text\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>Use <code>`</code>code notation<code>`</code> with backtick quotes\n                            </td>\n                            <td>\n                                Use <code>code notation</code> with backtick quotes\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>\n                                horizontal rule / separator<br>\n                                <kbd>↩</kbd><br/>\n                                <code>---</code> (3 dashes)<br/>\n                                <kbd>↩</kbd><br/>\n                            </td>\n                            <td>\n                                horizontal rule<br>\n                                <hr>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n\n                <h4>Links within a Skore <span class="label label-warning">New!</span></h4>\n\n                <table class="table table-bordered">\n                    <thead>\n                        <tr>\n                            <th>Write...</th>\n                            <th>This will do</th>\n                        </tr>\n                    </thead>\n                    <tr>\n                        <td>\n                            <p>\n                                Link to an activity box\n                            </p>\n                            <p>\n                                <code>[link text](ID)</code>\n\n                            </p>\n                            <p>\n                                Example : <code>[visit the new packaging design process](54)</code>\n                            </p>\n                        </td>\n                        <td>\n                            <p>Creates a link to the box <em>ID</em>.</p>\n                            <p>To know the <em>ID</em> of a box, select 1 box and check the number in the footer.</p>\n                            <p><small>The target box will be displayed and centered on screen.</small></p>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td>\n                            <p>\n                                Link to an activity box and enter detailed view\n                            </p>\n                            <p>\n                                <code>[link text](ID<strong>d</strong>)</code>\n\n                            </p>\n                            <p>\n                                Example : <code>[visit the new packaging design process](54d)</code>\n                            </p>\n                        </td>\n                        <td>\n                            <p>Creates a link to the box <em>ID</em> and enter in the <strong>d</strong>etailed view.</p>\n                            <p>To know the <em>ID</em> of a box, select 1 box and check ID number in the footer.</p>\n                            <p><small>Works only for whatbox.</small></p>\n                        </td>\n                    </tr>\n                </table>\n\n                <h4>Links</h4>\n\n\n                <table class="table table-bordered">\n                    <thead>\n                        <tr>\n                            <th>Write...</th>\n                            <th>And this renders as...</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr>\n                            <td>\n                                An explicit link to <code>https://www.getskore.com</code> (autoconverts text with http:// to a link)\n                            </td>\n                            <td>\n                                An explicit link to <a href="https://www.getskore.com/">https://www.getskore.com</a> (autoconverts text with http:// to a link)<br/>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td>A discreet link to <code class="selectionAllowed"><span style="white-space: nowrap;">[Skore](https://www.getskore.com/)</span></code>\n                            </td>\n                            <td>\n                                A discreet link to <a href="https://www.getskore.com/">Skore</a>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n\n                <h4>Images</h4>\n\n                <table class="table table-bordered">\n                    <thead>\n                        <tr>\n                            <th>Write...</th>\n                            <th>And this renders as...</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr>\n                            <td> <code class="selectionAllowed">![image](http://path.to/your/image.png)<code>\n                            </td>\n                            <td>\n                                Displays the image you are pointing to.<br/>Image <strong>must</strong> be self-hosted online (accessible via http).\n                            </td>\n                        </tr>\n                        <tr>\n                            <td> <code class="selectionAllowed">![image <span class="emphasize">noresize</span>](http://path.to/your/image.png)<code>\n                            </td>\n                            <td>\n                                Will not resize the image to fit in the container\n                            </td>\n                        </tr>\n\n                    </tbody>\n                </table>\n\n                <h4>Icons</h4>\n\n                <p class="alert text-center skTextIcon">\n                    <a data-skaction="modal_icons">\n                        Dozens of icons are ready to be used. Learn more.\n                    </a>\n                </p>\n\n                <h4>More...</h4>\n\n                <table class="table table-bordered">\n                    <thead>\n                        <tr>\n                            <th>Write...</th>\n                            <th>And this renders as...</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                    \t<tr>\n                            <td> Leave a field blank\n                            </td>\n                            <td>\n                                Type one single space in a field.\n                            </td>\n                        </tr>\n                        <tr>\n                            <td> Tables\n                            </td>\n                            <td>\n                                <a href="https://help.github.com/articles/organizing-information-with-tables/" class="skMkdownTableDoc">Click here for documentation</a>\n                            </td>\n                        </tr>\n                    </tbody>\n                </table>\n            </div>\n            <div class="modal-footer">\n                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n            </div>\n        </div>\n    </div>\n</div>\n';});
define('skModal/skModal_formatting',['require','text!skModal/skModal_Formatting.html'],function(require) {
    "use strict";
    function skModal_Formatting(navigatorUI) {
        return navigatorUI.loadModal("skModal_Formatting", require("text!skModal/skModal_Formatting.html"), null, null, null);
    }
    return skModal_Formatting;
});
define('text!skModal/skModal_Icons.html',[],function () { return '<div id="skModal_Icons" class="modal sideModal sideRight" tabindex="-1" role="dialog" data-backdrop="" data-keyboard="false" aria-labelledby="skModalIconLabel" aria-hidden="true">\n\t<div class="modal-dialog">\n\t\t<div class="modal-content">\n\t\t\t<div class="modal-header">\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\t\t\t\t<h4 class="modal-title" id="skModalIconLabel">Icons</h4>\n\t\t\t\t<p>\n\t\t\t\t\tDrag icon to canvas &nbsp;&nbsp;&nbsp;\n\t\t\t\t\t<span class="small">For color &amp; size, see bottom of the list</span>\n\t\t\t\t</p>\n\t\t\t</div>\n\n\t\t\t<div class="modal-body">\n\n\t\t\t\t<input type="text" class="form-control selectionAllowed" id="skIconFilter" placeholder="Type here to filter icon">\n\n\t\t\t\t<p class="skIconResult"></p>\n\n\t\t\t\t<div class="row skIconList">\n\t\t\t\t</div>\n\n\t\t\t\t<hr>\n\n\t\t\t\t<div class="row">\n\t\t\t\t\t<div class="col-md-12">\n\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t<div class="panel-heading">Add icon manually</div>\n\t\t\t\t\t\t\t<div class="panel-body">\n\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\tIn a box, write <code class="selectionAllowed">![icon](<em>icon-code</em>)</code> to add an icon\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\tExample : <code class="selectionAllowed">![icon](<span class="emphasize">camera-retro</span>)</code> will render as <i class="fa fa-camera-retro"></i><br/>\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-md-12">\n\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t<div class="panel-heading">Colors</div>\n\t\t\t\t\t\t\t<div class="panel-body">\n\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\tAdd any HTML-valid color.\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\tExample : <code class="selectionAllowed">![icon <span class="emphasize">red</span>](heart3)</code> will render as <i class="fa fa-heart" style="color:red"></i><br/>\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-md-12">\n\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t<div class="panel-heading">Sizing</div>\n\t\t\t\t\t\t\t<div class="panel-body">\n\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\tAdd the following to make your icon bigger depending on the need.\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\tExample : <code class="selectionAllowed">![icon <span class="emphasize">2x</span>](camera-retro)</code>\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t<ul class="list-inline">\n\t\t\t\t\t\t\t\t\t<li><code>lg</code></li>\n\t\t\t\t\t\t\t\t\t<li><code>2x</code></li>\n\t\t\t\t\t\t\t\t\t<li><code>3x</code></li>\n\t\t\t\t\t\t\t\t\t<li><code>4x</code></li>\n\t\t\t\t\t\t\t\t\t<li><code>5x</code></li>\n\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t\t<p>Doing vertical lists of icons? Add <code>fw</code> so they\'ll all have the same width (fixed width)</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-md-12">\n\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t<div class="panel-heading">Link + icon</div>\n\t\t\t\t\t\t\t<div class="panel-body">\n\t\t\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\t\t\tYou can make an icon a link\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t<p>Remember, code for a link : <code class="selectionAllowed">[Open this document](http://link..com)</code></p>\n\t\t\t\t\t\t\t\t<p>Code for an icon : <i class="fa fa-file-pdf-o"></i> <code class="selectionAllowed">![icon](file-pdf-o)</code></p>\n\t\t\t\t\t\t\t\t<p>Together : <code class="selectionAllowed">[<span class="emphasize">![icon](file-pdf-o)</span>](http://link..com)</code></p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="col-md-12">\n\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\tIcons by Font Awesome - <a href="http://fontawesome.io" class="skExtLink">http://fontawesome.io</a>. Thanks Dave Gandy.\n\t\t\t\t\t\t</p>\n\t\t\t\t\t\t<p>\n\t\t\t\t\t\t\tAnd Linear Icons - <a class="skExtLink" href="https://linearicons.com">https://linearicons.com</a>. Thanks <a href="https://perxis.com" class="skExtLink">Perxis</a>.\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\n\t\t\t\t</div> <!-- end row -->\n\t\t\t</div> <!-- end body -->\n\n\t\t\t<div class="modal-footer">\n\t\t\t\t<div class="pull-left">\n\t\t\t\t\t<p>\n\t\t\t\t\t\tDrag icon to canvas &nbsp;&nbsp;&nbsp;\n\t\t\t\t\t\t<span class="small">For color & size, see bottom of the list</span>\n\t\t\t\t\t</p>\n\n\t\t\t\t</div>\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n';});
define('skModal/skModal_Icons',['require','text!skModal/skModal_Icons.html','jquery','mxUtils','skConstants','skShell'],function(require) {
    "use strict";
    function skModal_Icons(navigatorUI) {
        var dragHandles = [];
        return navigatorUI.loadModal("skModal_Icons", require("text!skModal/skModal_Icons.html"), null, null, function() {
            dragHandles = [];
            var icons = [ "500px", "adjust", "adn", "align-center", "align-justify", "align-left", "align-right", "amazon", "ambulance", "american-sign-language-interpreting", "anchor", "android", "angellist", "angle-double-down", "angle-double-left", "angle-double-right", "angle-double-up", "angle-down", "angle-left", "angle-right", "angle-up", "apple", "archive", "area-chart", "arrow-circle-down", "arrow-circle-left", "arrow-circle-o-down", "arrow-circle-o-left", "arrow-circle-o-right", "arrow-circle-o-up", "arrow-circle-right", "arrow-circle-up", "arrow-down", "arrow-left", "arrow-right", "arrow-up", "arrows", "arrows-alt", "arrows-h", "arrows-v", "asl-interpreting", "assistive-listening-systems", "asterisk", "at", "audio-description", "automobile", "backward", "balance-scale", "ban", "bank", "bar-chart", "bar-chart-o", "barcode", "bars", "battery-0", "battery-1", "battery-2", "battery-3", "battery-4", "battery-empty", "battery-full", "battery-half", "battery-quarter", "battery-three-quarters", "bed", "beer", "behance", "behance-square", "bell", "bell-o", "bell-slash", "bell-slash-o", "bicycle", "binoculars", "birthday-cake", "bitbucket", "bitbucket-square", "bitcoin", "black-tie", "blind", "bluetooth", "bluetooth-b", "bold", "bolt", "bomb", "book", "bookmark", "bookmark-o", "braille", "briefcase", "btc", "bug", "building", "building-o", "bullhorn", "bullseye", "bus", "buysellads", "cab", "calculator", "calendar", "calendar-check-o", "calendar-minus-o", "calendar-o", "calendar-plus-o", "calendar-times-o", "camera", "camera-retro", "car", "caret-down", "caret-left", "caret-right", "caret-square-o-down", "caret-square-o-left", "caret-square-o-right", "caret-square-o-up", "caret-up", "cart-arrow-down", "cart-plus", "cc", "cc-amex", "cc-diners-club", "cc-discover", "cc-jcb", "cc-mastercard", "cc-paypal", "cc-stripe", "cc-visa", "certificate", "chain", "chain-broken", "check", "check-circle", "check-circle-o", "check-square", "check-square-o", "chevron-circle-down", "chevron-circle-left", "chevron-circle-right", "chevron-circle-up", "chevron-down", "chevron-left", "chevron-right", "chevron-up", "child", "chrome", "circle", "circle-o", "circle-o-notch", "circle-thin", "clipboard", "clock-o", "clone", "close", "cloud", "cloud-download", "cloud-upload", "cny", "code", "code-fork", "codepen", "codiepie", "coffee", "cog", "cogs", "columns", "comment", "comment-o", "commenting", "commenting-o", "comments", "comments-o", "compass", "compress", "connectdevelop", "contao", "copy", "copyright", "creative-commons", "credit-card", "credit-card-alt", "crop", "crosshairs", "css3", "cube", "cubes", "cut", "cutlery", "dashboard", "dashcube", "database", "deaf", "deafness", "dedent", "delicious", "desktop", "deviantart", "diamond", "digg", "dollar", "dot-circle-o", "download", "dribbble", "dropbox", "drupal", "edge", "edit", "eject", "ellipsis-h", "ellipsis-v", "empire", "envelope", "envelope-o", "envelope-square", "envira", "eraser", "eur", "euro", "exchange", "exclamation", "exclamation-circle", "exclamation-triangle", "expand", "expeditedssl", "external-link", "external-link-square", "eye", "eye-slash", "eyedropper", "facebook", "facebook-f", "facebook-official", "facebook-square", "fast-backward", "fast-forward", "fax", "feed", "female", "fighter-jet", "file", "file-archive-o", "file-audio-o", "file-code-o", "file-excel-o", "file-image-o", "file-movie-o", "file-o", "file-pdf-o", "file-photo-o", "file-picture-o", "file-powerpoint-o", "file-sound-o", "file-text", "file-text-o", "file-video-o", "file-word-o", "file-zip-o", "files-o", "film", "filter", "fire", "fire-extinguisher", "firefox", "flag", "flag-checkered", "flag-o", "flash", "flask", "flickr", "floppy-o", "folder", "folder-o", "folder-open", "folder-open-o", "font", "fonticons", "fort-awesome", "forumbee", "forward", "foursquare", "frown-o", "futbol-o", "gamepad", "gavel", "gbp", "ge", "gear", "gears", "genderless", "get-pocket", "gg", "gg-circle", "gift", "git", "git-square", "github", "github-alt", "github-square", "gitlab", "gittip", "glass", "glide", "glide-g", "globe", "google", "google-plus", "google-plus-square", "google-wallet", "graduation-cap", "gratipay", "group", "h-square", "hacker-news", "hand-grab-o", "hand-lizard-o", "hand-o-down", "hand-o-left", "hand-o-right", "hand-o-up", "hand-paper-o", "hand-peace-o", "hand-pointer-o", "hand-rock-o", "hand-scissors-o", "hand-spock-o", "hand-stop-o", "hard-of-hearing", "hashtag", "hdd-o", "header", "headphones", "heart", "heart-o", "heartbeat", "history", "home", "hospital-o", "hotel", "hourglass", "hourglass-1", "hourglass-2", "hourglass-3", "hourglass-end", "hourglass-half", "hourglass-o", "hourglass-start", "houzz", "html5", "i-cursor", "ils", "image", "inbox", "indent", "industry", "info", "info-circle", "inr", "instagram", "institution", "internet-explorer", "intersex", "ioxhost", "italic", "joomla", "jpy", "jsfiddle", "key", "keyboard-o", "krw", "language", "laptop", "lastfm", "lastfm-square", "leaf", "leanpub", "legal", "lemon-o", "level-down", "level-up", "life-bouy", "life-buoy", "life-ring", "life-saver", "lightbulb-o", "line-chart", "link", "linkedin", "linkedin-square", "linux", "list", "list-alt", "list-ol", "list-ul", "location-arrow", "lock", "long-arrow-down", "long-arrow-left", "long-arrow-right", "long-arrow-up", "low-vision", "magic", "magnet", "mail-forward", "mail-reply", "mail-reply-all", "male", "map", "map-marker", "map-o", "map-pin", "map-signs", "mars", "mars-double", "mars-stroke", "mars-stroke-h", "mars-stroke-v", "maxcdn", "meanpath", "medium", "medkit", "meh-o", "mercury", "microphone", "microphone-slash", "minus", "minus-circle", "minus-square", "minus-square-o", "mixcloud", "mobile", "mobile-phone", "modx", "money", "moon-o", "mortar-board", "motorcycle", "mouse-pointer", "music", "navicon", "neuter", "newspaper-o", "object-group", "object-ungroup", "odnoklassniki", "odnoklassniki-square", "opencart", "openid", "opera", "optin-monster", "outdent", "pagelines", "paint-brush", "paper-plane", "paper-plane-o", "paperclip", "paragraph", "paste", "pause", "pause-circle", "pause-circle-o", "paw", "paypal", "pencil", "pencil-square", "pencil-square-o", "percent", "phone", "phone-square", "photo", "picture-o", "pie-chart", "pied-piper", "pied-piper-alt", "pinterest", "pinterest-p", "pinterest-square", "plane", "play", "play-circle", "play-circle-o", "plug", "plus", "plus-circle", "plus-square", "plus-square-o", "power-off", "print", "product-hunt", "puzzle-piece", "qq", "qrcode", "question", "question-circle", "question-circle-o", "quote-left", "quote-right", "ra", "random", "rebel", "recycle", "reddit", "reddit-alien", "reddit-square", "refresh", "registered", "remove", "renren", "reorder", "repeat", "reply", "reply-all", "retweet", "rmb", "road", "rocket", "rotate-left", "rotate-right", "rouble", "rss", "rss-square", "rub", "ruble", "rupee", "safari", "save", "scissors", "scribd", "search", "search-minus", "search-plus", "sellsy", "send", "send-o", "server", "share", "share-alt", "share-alt-square", "share-square", "share-square-o", "shekel", "sheqel", "shield", "ship", "shirtsinbulk", "shopping-bag", "shopping-basket", "shopping-cart", "sign-in", "sign-language", "sign-out", "signal", "signing", "simplybuilt", "sitemap", "skyatlas", "skype", "slack", "sliders", "slideshare", "smile-o", "snapchat", "snapchat-ghost", "snapchat-square", "soccer-ball-o", "sort", "sort-alpha-asc", "sort-alpha-desc", "sort-amount-asc", "sort-amount-desc", "sort-asc", "sort-desc", "sort-down", "sort-numeric-asc", "sort-numeric-desc", "sort-up", "soundcloud", "space-shuttle", "spinner", "spoon", "spotify", "square", "square-o", "stack-exchange", "stack-overflow", "star", "star-half", "star-half-empty", "star-half-full", "star-half-o", "star-o", "steam", "steam-square", "step-backward", "step-forward", "stethoscope", "sticky-note", "sticky-note-o", "stop", "stop-circle", "stop-circle-o", "street-view", "strikethrough", "stumbleupon", "stumbleupon-circle", "subscript", "subway", "suitcase", "sun-o", "superscript", "support", "table", "tablet", "tachometer", "tag", "tags", "tasks", "taxi", "television", "tencent-weibo", "terminal", "text-height", "text-width", "th", "th-large", "th-list", "thumb-tack", "thumbs-down", "thumbs-o-down", "thumbs-o-up", "thumbs-up", "ticket", "times", "times-circle", "times-circle-o", "tint", "toggle-down", "toggle-left", "toggle-off", "toggle-on", "toggle-right", "toggle-up", "trademark", "train", "transgender", "transgender-alt", "trash", "trash-o", "tree", "trello", "tripadvisor", "trophy", "truck", "try", "tty", "tumblr", "tumblr-square", "turkish-lira", "tv", "twitch", "twitter", "twitter-square", "umbrella", "underline", "undo", "universal-access", "university", "unlink", "unlock", "unlock-alt", "unsorted", "upload", "usb", "usd", "user", "user-md", "user-plus", "user-secret", "user-times", "users", "venus", "venus-double", "venus-mars", "viacoin", "viadeo", "viadeo-square", "video-camera", "vimeo", "vimeo-square", "vine", "vk", "volume-control-phone", "volume-down", "volume-off", "volume-up", "warning", "wechat", "weibo", "weixin", "whatsapp", "wheelchair", "wheelchair-alt", "wifi", "wikipedia-w", "windows", "won", "wordpress", "wpbeginner", "wpforms", "wrench", "xing", "xing-square", "y-combinator", "y-combinator-square", "yahoo", "yc", "yc-square", "yelp", "yen", "youtube", "youtube-play", "youtube-square" ], linearicons = [ "alarm", "apartment", "arrow-down", "arrow-down-circle", "arrow-left", "arrow-left-circle", "arrow-right", "arrow-right-circle", "arrow-up", "arrow-up-circle", "bicycle", "bold", "book", "bookmark", "briefcase", "bubble", "bug", "bullhorn", "bus", "calendar-full", "camera", "camera-video", "car", "cart", "chart-bars", "checkmark-circle", "chevron-down", "chevron-down-circle", "chevron-left", "chevron-left-circle", "chevron-right", "chevron-right-circle", "chevron-up", "chevron-up-circle", "circle-minus", "clock", "cloud", "cloud-check", "cloud-download", "cloud-sync", "cloud-upload", "code", "coffee-cup", "cog", "construction", "crop", "cross", "cross-circle", "database", "diamond", "dice", "dinner", "direction-ltr", "direction-rtl", "download", "drop", "earth", "enter", "enter-down", "envelope", "exit", "exit-up", "eye", "file-add", "file-empty", "film-play", "flag", "frame-contract", "frame-expand", "funnel", "gift", "graduation-hat", "hand", "heart", "heart-pulse", "highlight", "history", "home", "hourglass", "inbox", "indent-decrease", "indent-increase", "italic", "keyboard", "laptop", "laptop-phone", "layers", "leaf", "license", "lighter", "line-spacing", "linearicons", "link", "list", "location", "lock", "magic-wand", "magnifier", "map", "map-marker", "menu", "menu-circle", "mic", "moon", "move", "music-note", "mustache", "neutral", "page-break", "paperclip", "paw", "pencil", "phone", "phone-handset", "picture", "pie-chart", "pilcrow", "plus-circle", "pointer-down", "pointer-left", "pointer-right", "pointer-up", "poop", "power-switch", "printer", "pushpin", "question-circle", "redo", "rocket", "sad", "screen", "select", "shirt", "smartphone", "smile", "sort-alpha-asc", "sort-amount-asc", "spell-check", "star", "star-empty", "star-half", "store", "strikethrough", "sun", "sync", "tablet", "tag", "text-align-center", "text-align-justify", "text-align-left", "text-align-right", "text-format", "text-format-remove", "text-size", "thumbs-down", "thumbs-up", "train", "trash", "underline", "undo", "unlink", "upload", "user", "users", "volume", "volume-high", "volume-low", "volume-medium", "warning", "wheelchair" ];
            linearicons = linearicons.map(function(e) {
                return "l-" + e;
            });
            var containerForIcons = $(".skIconList");
            containerForIcons.empty();
            var createIconButtons = function(icons, prefix) {
                icons.forEach(function(e) {
                    var iconToDrag = $('<div class="col-md-2"><div class="skIconItem iconItem"data-toggle="tooltip" data-placement="top" title="' + e + '" data-icon="' + e + '"><i class="fa ' + prefix + "f fa-fw fa-2x " + prefix + "-" + (e.startsWith("l-") ? e.substring(2) : e) + '"></i><span style="display:none">fa-2x ' + prefix + "-" + e + '"</span></div></div>');
                    containerForIcons.append(iconToDrag), dragHandles.push(navigatorUI.graphManager.makeCardshoeBoxDraggable(iconToDrag[0], skConstants.stickynote, "![icon 2x](" + e + ")"));
                });
            };
            createIconButtons(icons, "fa"), createIconButtons(linearicons, "lnr"), $('#skModal_Icons [data-toggle="tooltip"]').tooltip({
                container: "body"
            }), $("#skIconFilter").on("keyup", function() {
                var search = $(this).val();
                if ("" !== search.trim()) {
                    var elem = $(".skIconList").findElementsContainingText(search);
                    elem.length ? ($(".skIconList > .col-md-2").hide(), elem.parents(".col-md-2").show(), 
                    $(".skIconResult").show().html("" + elem.length + " icon(s) found.")) : ($(".skIconList > .col-md-2").show(), 
                    $(".skIconResult").show().html("No icon found. Displaying all."));
                } else $(".skIconList > .col-md-2").show(), $(".skIconResult").hide();
            });
            var setEnabled = function(enabled) {
                $(dragHandles).each(function(i, e) {
                    e.setEnabled(enabled);
                }), $(".iconItem").toggleClass("disabled", !enabled);
            };
            setEnabled(navigatorUI.getCurrentGraph().isEnabled()), navigatorUI.getGraphManager().addListener("graphEnabled", mxUtils.bind(this, function(sender, event) {
                var graph = event.getProperty("graph");
                setEnabled(graph.isEnabled());
            }));
        });
    }
    var $ = require("jquery"), mxUtils = require("mxUtils"), skConstants = require("skConstants");
    require("skShell");
    return skModal_Icons;
});
define('text!skModal/skModal_XML.html',[],function () { return '<div class="modal fade" id="skModal_XML" tabindex="-1" role="dialog" aria-labelledby="skModalXMLLabel" aria-hidden="true">\n\t<div class="modal-dialog modal-lg">\n\t\t<div class="modal-content">\n\t\t\t<div class="modal-header">\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\t\t\t\t<h4 class="modal-title" id="skModalXMLLabel">XML Playground</h4>\n\t\t\t</div>\n\t\t\t<div class="modal-body">\n\t\t\t\t<div class="container-fluid">\n\t\t\t\t\t<textarea class="form-control selectionAllowed skXMLTextarea">Loading...</textarea>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="modal-footer">\n\t\t\t\t<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\t\t\t\t<button type="button" data-loading-text="Loading..." class="btn btn-primary skXMLApplyChange">Apply changes</button>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>\n';});
define('skModal/skModal_XML',['require','text!./skModal_XML.html','skShell','jquery','skConstants'],function(require) {
    "use strict";
    function skModal_XML(navigatorUI) {
        var modalXML = navigatorUI.loadModal("skModal_XML", require("text!./skModal_XML.html"), function() {
            $(".skXMLTextarea").val("Loading..."), $(".skXMLTextarea").val(navigatorUI.getXmlForGraph(null, !0)), 
            $(".skXMLTextarea").select(), navigatorUI.modal.fit_modal_body($("#skModal_XML"));
        }, function() {}, function() {
            $(".skXMLApplyChange").on("click", function() {
                var $btn = $(this).button("loading");
                skShell.wellDoneMessage.hey(this), $("#skModal_XML").modal("hide"), navigatorUI.graphManager.openGraph({
                    xml: $(".skXMLTextarea").val(),
                    source: skConstants.SOURCE_XMLEDIT
                }), $btn.button("reset");
            });
        });
        return modalXML;
    }
    var skShell = require("skShell"), $ = require("jquery"), skConstants = require("skConstants");
    return skModal_XML;
});
define('text!skModal/skModal_Stylesheet.html',[],function () { return '<div class="modal fade" id="skModal_Stylesheet" tabindex="-1" role="dialog" aria-labelledby="skModalStylesheetLabel" aria-hidden="true">\n\t<div class="modal-dialog modal-lg" >\n\t\t<div class="modal-content">\n\t\t\t<div class="modal-header">\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\t\t\t\t<h4 class="modal-title" id="skModalStylesheetLabel">Skore Stylesheet</h4>\n\t\t\t</div>\n\t\t\t<div class="modal-body">\n\t\t\t\t<div class="container-fluid">\n\n\t\t\t\t\t<ul class="nav nav-tabs skCellTypeTab">\n\t\t\t\t\t\t<li role="presentation" class="active">\n\t\t\t\t\t\t\t<a data-skcelltype="whatbox">\n\t\t\t\t\t\t\t\tWhatbox\n\t\t\t\t\t\t\t\t<span class="badge"></span>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li role="presentation">\n\t\t\t\t\t\t\t<a data-skcelltype="whybox">\n\t\t\t\t\t\t\t\tWhybox\n\t\t\t\t\t\t\t\t<span class="badge"></span>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li role="presentation">\n\t\t\t\t\t\t\t<a data-skcelltype="line">\n\t\t\t\t\t\t\t\tLine (box to box)\n\t\t\t\t\t\t\t\t<span class="badge"></span>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li role="presentation">\n\t\t\t\t\t\t\t<a data-skcelltype="group">\n\t\t\t\t\t\t\t\tGroup\n\t\t\t\t\t\t\t\t<span class="badge"></span>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li role="presentation">\n\t\t\t\t\t\t\t<a data-skcelltype="note">\n\t\t\t\t\t\t\t\tNote\n\t\t\t\t\t\t\t\t<span class="badge"></span>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t\t<li role="presentation">\n\t\t\t\t\t\t\t<a data-skcelltype="wy2ss">\n\t\t\t\t\t\t\t\tLine (box to note)\n\t\t\t\t\t\t\t\t<span class="badge"></span>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t</li>\n\t\t\t\t\t</ul>\n\t\t\t\t\t<p>&nbsp;</p>\n\t\t\t\t\t<div class="skdevss"></div>\n\n\t\t\t\t\t<div class="alert alert-info skStylesheetAlert" style="display:none;" role="alert"></div>\n\n\t\t\t\t\t<div class="row">\n\n\t\t\t\t\t\t<!-- text -->\n\t\t\t\t\t\t<div class="col-md-6 skStyleTheme">\n\t\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t\t<div class="panel-heading">\n\t\t\t\t\t\t\t\t\t<h3 class="panel-title">\n\t\t\t\t\t\t\t\t\t\tText\n\t\t\t\t\t\t\t\t\t</h3>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class="panel-body">\n\t\t\t\t\t\t\t\t\t<div class="form-group">\n\n\t\t\t\t\t\t\t\t\t\t<!--  fontColor -->\n\t\t\t\t\t\t\t\t\t\t<label for="skTextColor" class="col-sm-6 control-label">Text Color</label>\n\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t\t\t\t\t\t<input type="text" class="colorTypeahead form-control selectionAllowed" id="skTextColor" data-styleattr="fontColor">\n\t\t\t\t\t\t\t\t\t\t\t\t<span class="input-group-addon">&nbsp;</span>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">HTML valid color</span>\n\t\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t\t<!-- fontSize -->\n\t\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t\t<label for="skFontSize" class="col-sm-6 control-label">Text size</label>\n\t\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t\t<input type="number" class="form-control selectionAllowed" id="skFontSize" data-styleattr="fontSize">\n\t\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">Pixel (default 13)</span>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t\t<!-- linkColor -->\n\t\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t\t<label for="skLinkColor" class="col-sm-6 control-label">Link Color</label>\n\t\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type="text" class="colorTypeahead form-control selectionAllowed" id="skLinkColor" data-styleattr="linkColor">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<span class="input-group-addon">&nbsp;</span>\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">HTML valid color</span>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t<!-- Border -->\n\t\t\t\t\t\t<div class="col-md-6 skStyleTheme">\n\t\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t\t<div class="panel-heading">\n\t\t\t\t\t\t\t\t\t<h3 class="panel-title">\n\t\t\t\t\t\t\t\t\t\tBorder\n\t\t\t\t\t\t\t\t\t</h3>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class="panel-body">\n\n\t\t\t\t\t\t\t\t\t<!-- dashed & rounded -->\n\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t<label class="col-sm-6 control-label">Border</label>\n\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\n\t\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input id="skWBLineDashed" data-styleattr="dashed" type="checkbox" >\n\t\t\t\t\t\t\t\t\t\t\t\t\tDashed\n\t\t\t\t\t\t\t\t\t\t\t\t</label>\n\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t<div class="col-sm-offset-6 col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\n\t\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input id="skWBLineRounded" data-styleattr="rounded" type="checkbox" >\n\t\t\t\t\t\t\t\t\t\t\t\t\tRounded\n\t\t\t\t\t\t\t\t\t\t\t\t</label>\n\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t<!-- strokeWidth -->\n\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t<label for="skStrokeWidth" class="col-sm-6 control-label">Border thickness</label>\n\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t<input type="number" class="selectionAllowed form-control" id="skStrokeWidth" data-styleattr="strokeWidth">\n\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">Pixel (default 1)</span>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t<!-- color -->\n\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t<label for="skStrokeColor" class="col-sm-6 control-label">Border Color</label>\n\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t\t\t\t\t\t<input type="text" class="colorTypeahead selectionAllowed form-control" id="skStrokeColor" data-styleattr="strokeColor">\n\t\t\t\t\t\t\t\t\t\t\t\t<span class="input-group-addon">&nbsp;</span>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">HTML valid color or <em>transparent</em></span>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="row">\n\n\t\t\t\t\t\t<!-- Fill -->\n\t\t\t\t\t\t<div class="col-md-6 skStyleTheme">\n\t\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t\t<div class="panel-heading">\n\t\t\t\t\t\t\t\t\t<h3 class="panel-title">\n\t\t\t\t\t\t\t\t\t\tFill\n\t\t\t\t\t\t\t\t\t</h3>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class="panel-body">\n\t\t\t\t\t\t\t\t\t<div class="form-group">\n\n\t\t\t\t\t\t\t\t\t\t<!-- fillColor -->\n\t\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t\t<label for="skFillColor" class="col-sm-6 control-label">\n\t\t\t\t\t\t\t\t\t\t\t\tFill Color\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type="text" class="colorTypeahead form-control selectionAllowed" id="skFillColor" data-styleattr="fillColor">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<span class="input-group-addon">&nbsp;</span>\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">HTML Color or <em>transparent</em></span>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t\t<!-- opacity -->\n\t\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t\t<label for="skWBFillOpacity" class="col-sm-6 control-label">Box opacity</label>\n\t\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t\t<input type="number" class="form-control selectionAllowed" id="skWBFillOpacity" data-styleattr="opacity">\n\t\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">0 is transparent, 100 opaque.</span>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\n\n\t\t\t\t\t\t<!-- Size -->\n\t\t\t\t\t\t<div class="col-md-6 skStyleTheme">\n\t\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t\t<div class="panel-heading">\n\t\t\t\t\t\t\t\t\t<h3 class="panel-title">\n\t\t\t\t\t\t\t\t\t\tSize\n\t\t\t\t\t\t\t\t\t</h3>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class="panel-body">\n\n\t\t\t\t\t\t\t\t\t<!-- sizing, width, height -->\n\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t<label class="col-sm-6 control-label">Size<br/><br/>\n\t\t\t\t\t\t\t\t\t\t\t<span class="experimental block">Experimental. Please provide <a class="infoatgetskoredotcom" href="mailto:info@getskore.com">feedback</a></span>\n\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input id="skWBAllowResize" data-celltype="whatbox" data-styleattr="resizable" type="checkbox" >\n\t\t\t\t\t\t\t\t\t\t\t\t\tAllow resize\n\t\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t<label for="skWBWidth">Width</label>\n\t\t\t\t\t\t\t\t\t\t\t<input type="number" class="form-control selectionAllowed styleFreeText" id="skWBWidth" data-celltype="whatbox"\n\t\t\t\t\t\t\t\t\t\t\tdata-styleattr="width">\n\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">Pixel (default 190)</span>\n\n\t\t\t\t\t\t\t\t\t\t\t<label for="skWBheight">Height</label>\n\t\t\t\t\t\t\t\t\t\t\t<input type="number" class="form-control selectionAllowed styleFreeText" id="skWBheight" data-celltype="whatbox"\n\t\t\t\t\t\t\t\t\t\t\tdata-styleattr="height">\n\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">pixel (default 80)</span>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class="row">\n\n\t\t\t\t\t\t<!-- Other -->\n\t\t\t\t\t\t<div class="col-md-6 skStyleTheme">\n\t\t\t\t\t\t\t<div class="panel panel-default">\n\t\t\t\t\t\t\t\t<div class="panel-heading">\n\t\t\t\t\t\t\t\t\t<h3 class="panel-title">\n\t\t\t\t\t\t\t\t\t\tOthers\n\t\t\t\t\t\t\t\t\t</h3>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t<div class="panel-body">\n\t\t\t\t\t\t\t\t\t<div class="form-group">\n\n\t\t\t\t\t\t\t\t\t\t<!-- detailColor -->\n\t\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t\t<label for="skWBIconDetailColor" class="col-sm-6 control-label">Enter Detail Icon Color</label>\n\t\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type="text" class="colorTypeahead form-control selectionAllowed" id="skWBIconDetailColor" data-styleattr="detailColor">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<span class="input-group-addon">&nbsp;</span>\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">HTML valid color</span>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t\t<!-- attachmentColor -->\n\t\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t\t<label for="skWBIconAttachmentColor" class="col-sm-6 control-label">Attachment Icon Color</label>\n\t\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class="input-group">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type="text" class="colorTypeahead form-control selectionAllowed" id="skWBIconAttachmentColor" data-styleattr="attachmentColor">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<span class="input-group-addon">&nbsp;</span>\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">HTML valid color</span>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t\t\t<!-- line separator -->\n\t\t\t\t\t\t\t\t\t\t<div class="form-group">\n\t\t\t\t\t\t\t\t\t\t\t<label class="col-sm-6 control-label">Line between what and who</label>\n\t\t\t\t\t\t\t\t\t\t\t<div class="col-sm-6">\n\t\t\t\t\t\t\t\t\t\t\t\t<div class="radio">\n\n\t\t\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type="radio" id="skWhoWhatLineOff" name="skWhoWhatLine" data-styleattr="whoWhatLine" value="0" >\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tOff\n\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t<div class="radio">\n\n\t\t\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type="radio" id="skWhoWhatLineSimple"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tname="skWhoWhatLine"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tdata-styleattr="whoWhatLine" value="1" >\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tSimple\n\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">Same color and width of the border</span>\n\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t\t<div class="radio">\n\n\t\t\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type="radio" id="skWhoWhatLineCustom"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tname="skWhoWhatLine"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tdata-styleattr="whoWhatLine" value="css" >\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tCustom\n\t\t\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input type="text" class="form-control selectionAllowed styleFreeText" id="skWBSplitBeforeRoles"\n\t\t\t\t\t\t\t\t\t\t\t\t\tdata-styleattr="whoWhatLineCSS">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<span class="help-block">Inline css</span>\n\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\n\t\t\t\t</div>\n\t\t\t\t<div class="modal-footer">\n\n\t\t\t\t\t<div class="pull-left">\n\t\t\t\t\t\t<button class="btn btn-default skResetToSaved">\n\t\t\t\t\t\t\tReset stylesheet\n\t\t\t\t\t\t</button>\n\t\t\t\t\t</div>\n\n\t\t\t\t\t<form class="form-inline">\n\n\t\t\t\t\t\t<div class="checkbox skApplyAsDefaultContainer">\n\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t<input class="skApplyAsDefault" type="checkbox" value="">\n\t\t\t\t\t\t\t\tSave as system default stylesheet\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<button type="button" class="btn btn-default" data-dismiss="modal">Cancel & Close</button>\n\n\t\t\t\t\t\t<button type="button" class="skApplyStylesheet btn btn-primary">Apply</button>\n\t\t\t\t\t</form>\n\n\n\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n';});
define('skModal/skModal_Stylesheet',[ "require", "jquery", "mxCell", "mxConstants", "mxUtils", "skConstants", "skShell", "colors", "skUtils", "text!skModal/skModal_Stylesheet.html", "lib/bootstrap3-typeahead" ], function(require, $, mxCell, mxConstants, mxUtils, skConstants, skShell, colors, skUtils, html) {
    "use strict";
    function skModal_Stylesheet(navigatorUI) {
        var that = this;
        this.graphManager = navigatorUI.graphManager;
        var modalStylesheet = navigatorUI.loadModal("skModal_Stylesheet", html, mxUtils.bind(this, function() {
            this.styleObjectForCells = {}, this.loadtype = $("#skModal_Stylesheet").data("loadtype"), 
            $(".skCellTypeTab li").show(), that.loadUIForCell();
        }), null, mxUtils.bind(this, function(myModal) {
            theModal = myModal;
            var that = this;
            $(".skCellTypeTab a", theModal).click(function(e) {
                that.saveStyleForCellType(), $(".skCellTypeTab li", theModal).removeClass("active"), 
                $(this).parents("li").addClass("active"), e.preventDefault();
                var cellType = $(this).data("skcelltype");
                that.loadUIForCell(cellType);
            }), $(".colorTypeahead", theModal).typeahead({
                autoSelect: !1,
                items: "all",
                minLength: 0,
                source: colors
            }), $(".colorTypeahead", theModal).on("change", function() {
                $(this).siblings(".input-group-addon").css({
                    "background-color": $(this).val()
                });
            });
            var d, cp = $(".skColorPanel", theModal);
            colors.forEach(function(e) {
                d = $("<div class='col-md-3'><p class='selectionAllowed' style='background-color:" + e.htmlcode + "; color:black; padding:10px'>" + e.name + "</p></div>"), 
                cp.append(d);
            }), $(".skColorPanelHeading", theModal).on("click", function() {
                $(".skColorPanelContainer", theModal).toggle();
            }), $(".skApplyStylesheet", theModal).on("click", function() {
                that.writeStyle(that.sortedCells);
                $("#skModal_Stylesheet").modal("hide");
            }), $(".skResetToSaved", theModal).on("click", function() {
                that.sortedCells ? (that.graphManager.getCurrentGraph().getStylesheet().applyStyleToCells(that.graphManager.getCurrentGraph().getSelectionCells(), ""), 
                $("#skModal_Stylesheet").modal("hide")) : skShell.setStatus("not availabed");
            }), $(".skApplyAsDefaultContainer", theModal).hide();
        }));
        return modalStylesheet;
    }
    var theModal, loadMatrix = {
        fontColor: [ "whatbox", "group", "whybox", "note" ],
        fontSize: [ "whatbox", "group", "whybox", "note" ],
        linkColor: [ "whatbox", "group", "whybox", "note" ],
        detailColor: [ "whatbox" ],
        attachmentColor: [ "whatbox" ],
        strokeColor: [ "whatbox", "group", "whybox", "note", "line", "wy2ss" ],
        strokeWidth: [ "whatbox", "group", "whybox", "note", "line", "wy2ss" ],
        dashed: [ "whatbox", "group", "whybox", "note", "line", "wy2ss" ],
        rounded: [ "whatbox", "group", "note" ],
        fillColor: [ "whatbox", "group", "note" ],
        opacity: [ "whatbox", "whybox", "group", "note", "line", "wy2ss" ],
        whoWhatLine: [ "whatbox" ],
        whoWhatLineCSS: [ "whatbox" ],
        resizable: [ "whatbox", "whybox", "note" ],
        width: [ "whatbox", "whybox", "note" ],
        height: [ "whatbox", "whybox", "note" ],
        align: [ "whatbox", "whybox", "note" ]
    };
    skModal_Stylesheet.prototype.loadUIForCell = function(cellType) {
        this.sortedCells = {}, "stylecell" === this.loadtype ? (this.sortedCells = getSortedCells(this.graphManager.getCurrentGraph()), 
        cellType = Object.keys(this.sortedCells)[0], $(".skApplyStylesheet", theModal).show().text("Apply to selected boxes & lines only"), 
        $(".skApplyAsDefaultContainer", theModal).hide(), $(".skStylesheetAlert", theModal).show().html("<p>Customize only selected elements</p>")) : (cellType = cellType || skConstants.whatbox, 
        $(".skStylesheetAlert", theModal).show().html("<p>You are editing the default stylesheet. Change will apply to all elements</p><p>To customize <strong>selected elements only</strong>, select them first and click on the counter in the footer.</p><p>Elements that have been individually customized will not be affected by changes made here."), 
        $(".skApplyStylesheet", theModal).show().text("Apply"));
        var styleSheetForCellType = this.graphManager.getStyleForCell(null, cellType, null, this.sortedCells[cellType]);
        this.currentCellType = cellType;
        for (var items = Object.keys(loadMatrix), i = 0; i < items.length; i++) {
            var styleAttr = items[i], okForCellType = loadMatrix[styleAttr].includes(cellType), currentInput = $("[data-styleattr='" + styleAttr + "']", theModal);
            if (currentInput.attr("data-enabledforcelltype", okForCellType).parents(".form-group").toggle(okForCellType), 
            okForCellType) if (currentInput.is(":checkbox")) currentInput.prop("checked", "" + styleSheetForCellType[styleAttr] == "1" ? !0 : !1); else if (currentInput.is(":radio")) currentInput.prop("checked", !1), 
            currentInput.filter("input[value='" + styleSheetForCellType[styleAttr] + "']").prop("checked", !0); else {
                var tmp = styleSheetForCellType[styleAttr];
                currentInput.hasClass("styleFreeText") ? tmp && currentInput.val(decodeURIComponent(tmp)) : currentInput.val(tmp), 
                currentInput.hasClass("colorTypeahead") && currentInput.siblings(".input-group-addon").css({
                    "background-color": styleSheetForCellType[styleAttr]
                });
            }
        }
        $(".skStyleTheme", theModal).each(function(i, theme) {
            $(theme).toggle($("[data-enabledforcelltype='true']", theme).length > 0);
        });
    };
    var getSortedCells = function(graph) {
        var sortedCells = {};
        return $(".skCellTypeTab li").hide(), graph.getSelectionCells().forEach(function(cell) {
            var cellType = cell.getCellType();
            void 0 === sortedCells[cellType] && (sortedCells[cellType] = []);
            var i = sortedCells[cellType].push(cell);
            $("a[data-skcelltype='" + cellType + "']", theModal).parents("li").toggle(i > 0), 
            $("a[data-skcelltype='" + cellType + "'] .badge", theModal).text(i);
        }), sortedCells;
    };
    return skModal_Stylesheet.prototype.saveStyleForCellType = function() {
        var styleString = {};
        $("[data-enabledforcelltype='true']").each(function(i, e) {
            var val, key = $(e).data("styleattr");
            $(e).is(":checkbox") ? val = $(e).is(":checked") ? "1" : null : $(e).is(":radio") ? val = $(e).is(":checked") ? $(e).val() : null : (val = $(e).val().trim(), 
            $(e).hasClass("styleFreeText") && (val = encodeURIComponent(val)), "number" == $(e).attr("type") && "" !== val && (val = parseInt(val, 10))), 
            null !== val && "" !== val && (styleString[key] = val);
        }), Object.keys(styleString).length && (this.styleObjectForCells[this.currentCellType] = styleString);
    }, skModal_Stylesheet.prototype.saveStylesheet = function() {
        this.graphManager.setStyleSheet(null, this.styleObjectForCells, null, this.sortedCells);
    }, skModal_Stylesheet.prototype.writeStyle = function() {
        this.saveStyleForCellType(), this.saveStylesheet();
    }, skModal_Stylesheet.prototype.sortedCells = null, skModal_Stylesheet.prototype.styleObjectForCells = null, 
    skModal_Stylesheet.prototype.graphManager = null, skModal_Stylesheet.prototype.currentCellType = null, 
    skModal_Stylesheet;
});
define('skModal/skModal_ImageExport',['require','jquery'],function(require) {
    "use strict";
    function skModal_ImageExport(navigatorUI) {
    }
    require("jquery");
    return skModal_ImageExport.prototype.navigatorUI = null, skModal_ImageExport;
});
define('text!skUI/skSideMenu.html',[],function () { return '\n<!-- tabs -->\n<div class="skTabMenu forGraphEditor">\n\t<ul class="nav nav-tabs skNavTabs" role="tablist">\n\t\t<li class="canBeMini active" role="presentation" >\n\t\t\t<a href="#create" aria-controls="create" role="tab" data-toggle="tab">\n\t\t\t\tCreate\n\t\t\t</a>\n\t\t</li>\n\t\t<li class="canBeMini" role="presentation">\n\t\t\t<a href="#learn" aria-controls="learn" role="tab" data-toggle="tab">\n\t\t\t\tLearn\n\t\t\t</a>\n\t\t</li>\n\t\t<li class="canBeMini" role="presentation">\n\t\t\t<a href="#export" aria-controls="export" role="tab" data-toggle="tab">\n\t\t\t\tExport\n\t\t\t</a>\n\t\t</li>\n\t\t<li class="canBeMini" role="presentation">\n\t\t\t<a href="#customize" aria-controls="customize" role="tab" data-toggle="tab">\n\t\t\t\tCustomize\n\t\t\t</a>\n\t\t</li>\n\t\t<li class="canBeMini" role="presentation">\n\t\t\t<a href="#further" aria-controls="further" role="tab" data-toggle="tab">\n\t\t\t\tMore\n\t\t\t</a>\n\t\t</li>\n\t\t<li>\n\t\t\t<a class="skMiniMaxiMenu">\n\t\t\t\t<i class="fa fa-angle-double-up"></i>\n\t\t\t</a>\n\t\t</li>\n\t</ul>\n</div>\n<!-- end tabs -->\n\n<!-- tab content -->\n<div class="skSideMenu forGraphEditor canBeMini">\n\t<div class="tab-content">\n\n\t\t<!-- create  -->\n\t\t<div role="tabpanel" class="tab-pane active" id="create">\n\n\n\t\t\t<!-- enable... -->\n\t\t\t<!-- <p class="graphenablebtn" data-skaction="enableGraph">\n\t\t\t\t<i class="fa fa-unlock"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tEdit mode\n\t\t\t\t</span>\n\t\t\t</p> -->\n\n\n\t\t\t<!-- cardshoe -->\n\t\t\t<div class="cardshoe">\n\t\t\t\t<!-- <i class="fa fa-plus-circle"></i> -->\n\t\t\t\t<span class="cardshoe-box cardshoe-box-wb" data-toggle="tooltip" data-placement="bottom" title="Drag &amp; drop on the canvas">\n\t\t\t\t\t<span class="cardshoe-box-text">\n\t\t\t\t\t\t<span class="boxKeyword">Add</span> whatbox\n\t\t\t\t\t</span>\n\t\t\t\t</span>\n\t\t\t</div>\n\n\t\t\t<div class="cardshoe">\n\t\t\t\t<!-- <i class="fa fa-plus-circle"></i> -->\n\t\t\t\t<span class="cardshoe-box cardshoe-box-yb" data-toggle="tooltip" data-placement="bottom" title="Drag &amp; drop on the canvas">\n\t\t\t\t\t<span class="cardshoe-box-text">\n\t\t\t\t\t\t<span class="boxKeyword">Add</span> whybox\n\t\t\t\t\t</span>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<div class="cardshoe">\n\t\t\t\t<!-- <i class="fa fa-plus-circle"></i> -->\n\t\t\t\t<span class="cardshoe-box cardshoe-box-sn" data-toggle="tooltip" data-placement="bottom" title="Drag &amp; drop on the canvas">\n\t\t\t\t\t<span class="cardshoe-box-text">\n\t\t\t\t\t\t<span class="boxKeyword">Add</span> note\n\t\t\t\t\t</span>\n\t\t\t\t</span>\n\t\t\t</div>\n\n\t\t\t<div class="skWhyBoxInputsList" style="display:none;">\n\n\t\t\t\t<div class="skWingsIO_Inputs" >\n\t\t\t\t\t<h3>Inputs</h3>\n\t\t\t\t\t<p class="silent explanation">\n\t\t\t\t\t\tInputs connected at the upper level. Used to ensure consistency between level of details.\n\t\t\t\t\t</p>\n\t\t\t\t\t<ul class="skWingIOList "></ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="skWhyBoxOutputsList" style="display:none;">\n\t\t\t\t<div class="skWingsIO_Outputs">\n\t\t\t\t\t<h3>Outputs</h3>\n\t\t\t\t\t<p class="silent explanation">\n\t\t\t\t\t\tOutputs connected at the upper level. Used to ensure consistency between level of details.\n\t\t\t\t\t</p>\n\t\t\t\t\t<ul class="skWingIOList "></ul>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<!-- icons... -->\n\t\t\t<p data-skaction="modal_icons">\n\t\t\t\t<i class="fa fa-tree"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tAdd icons...\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">Illustrate your processes</span>\n\t\t\t</p>\n\n\t\t\t<!-- formatting -->\n\t\t\t<p data-skaction="modal_formatting">\n\t\t\t\t<i class="fa fa-paragraph"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tFormatting...\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tLearn how to format text in boxes\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- keyboard shorcut -->\n\t\t\t<p data-skaction="modal_kbd">\n\t\t\t\t<i class="fa fa-keyboard-o"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tKeyboard shortcuts...\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tBe faster\n\t\t\t\t</span>\n\t\t\t</p>\n\t\t</div>\n\n\t\t<!-- learn -->\n\t\t<div role="tabpanel" class="tab-pane" id="learn">\n\n\t\t\t<span class="subIcon">\n\t\t\t\t<i class="fa fa-graduation-cap"></i>\n\t\t\t</span>\n\n\t\t\t<h3>\n\t\t\t\tLearn\n\t\t\t</h3>\n\n\t\t\t<!-- tutorial -->\n\t\t\t<p class="skSideMenu_tutorial">\n\t\t\t\t<i class="fa fa-life-saver"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tStart tutorial\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tLet you be guided\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<p class="skExtLink" href="https://www.getskore.com/learn">\n\t\t\t\t<i class="fa fa-graduation-cap"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tTraining materials\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tLearn on getskore.com\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<p  class="skExtLink" href="https://www.youtube.com/user/skorelabs">\n\t\t\t\t<i class="fa fa-youtube-play"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tVideos\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tDozens of short modules to master Skore app\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- demo content -->\n\t\t\t<h3 class="skSideMenu_loadExample">\n\t\t\t\tExample content\n\t\t\t</h3>\n\t\t</div>\n\n\t\t<!-- export -->\n\t\t<div role="tabpanel" class="tab-pane" id="export">\n\t\t\t<span class="subIcon">\n\t\t\t\t<i class="fa fa-upload"></i>\n\t\t\t</span>\n\n\t\t\t<h3>Export</h3>\n\n\t\t\t<!-- just skore it -->\n\t\t\t<p data-skaction="modal_justskoreit" >\n\t\t\t\t<i class="fa fa-globe"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tJust Skore it\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tShare this Skore anonymously and secretely with friends and colleagues.\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- image -->\n\t\t\t<p\n\t\t\t\tdata-skaction="modal_imageexport"\n\t\t\t\tdata-target="clipboard">\n\t\t\t\t<i class="fa fa-image"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tCopy as image\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tEntire graph or selection to ready to insert in email or presentations\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<p\n\t\t\t\tdata-skaction="modal_imageexport"\n\t\t\t\tdata-target="preview">\n\t\t\t\t<i class="fa fa-file-image-o"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tPreview image &amp; save...\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- print -->\n\t\t\t<p data-skaction="modal_print">\n\t\t\t\t<i class="fa fa-print"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tPrint...\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- xml -->\n\t\t\t<p data-skaction="modal_xml">\n\t\t\t\t<i class="fa fa-file-code-o"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tXML...\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">View &amp; manipulate the XML source</span>\n\t\t\t</p>\n\t\t</div>\n\n\t\t<!-- customize -->\n\t\t<div role="tabpanel" class="tab-pane" id="customize">\n\n\t\t\t<span class="subIcon">\n\t\t\t\t<i class="fa fa-cog"></i>\n\t\t\t</span>\n\n\t\t\t<h3>Customize</h3>\n\n\t\t\t<!-- stylesheet -->\n\t\t\t<p data-skaction="modal_stylesheet" class="skSideMenu_StyleSheet" data-loadtype="stylesheet">\n\t\t\t\t<i class="fa fa-paint-brush fa-rotate-270"></i>\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tStylesheet...\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tChange look of boxes &amp; lines for the entire map\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- stylesheet -->\n\t\t\t<p data-skaction="modal_stylesheet" class="disabled skSideMenu_StyleSheet" data-loadtype="stylecell" >\n\t\t\t\t<i class="fa fa-paint-brush"></i>\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tBox Style...\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tEdit stylesheet for <strong>selected</strong> box<span class="box-pluralise"></span> only\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- settings -->\n\t\t\t<p data-skaction="modal_settings">\n\t\t\t\t<i class="fa fa-cogs"></i>\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tSettings...\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">Page format, extensions, ...</span>\n\t\t\t</p>\n\t\t</div>\n\n\t\t<!-- more... -->\n\t\t<div role="tabpanel" class="tab-pane" id="further">\n\n\t\t\t<span class="subIcon">\n\t\t\t\t<i class="fa fa-info"></i>\n\t\t\t</span>\n\n\t\t\t<h3>More</h3>\n\n\t\t\t<!-- web -->\n\t\t\t<p class="skSideMenu_visitgetskorecom skExtLink" href="https://www.getskore.com">\n\t\t\t\t<i class="fa fa-globe"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tVisit getskore.com\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- feedback / email -->\n\t\t\t<p class="skSideMenu_feedback" href="mailto:info@getskore.com">\n\t\t\t\t<i class="fa fa-envelope"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tSend us feedback\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tinfo@getskore.com\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- youtube (repeat from learn) -->\n\t\t\t<p  class="skExtLink" href="https://www.youtube.com/user/skorelabs">\n\t\t\t\t<i class="fa fa-youtube-play"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tVideos\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\tDozens of short modules to master Skore app\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- twitter -->\n\t\t\t<p class="skExtLink" href="https://twitter.com/skoreapp">\n\t\t\t\t<i class="fa fa-twitter"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tTwitter\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation">\n\t\t\t\t\t@skoreApp\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- facebook -->\n\t\t\t<p class="skExtLink" href="https://www.facebook.com/SkoreApp">\n\t\t\t\t<i class="fa fa-facebook-official"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tFacebook\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<!-- license agreement -->\n\t\t\t<p class="skExtLink" href="https://www.getskore.com/end-user-license-agreement-eula/">\n\t\t\t\t<i class="fa fa-balance-scale"></i> &nbsp;\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tView license\n\t\t\t\t</span>\n\t\t\t</p>\n\n\t\t\t<p>\n\t\t\t\t<span class="side-menu-text">\n\t\t\t\t\tCurrent version\n\t\t\t\t</span>\n\t\t\t\t<span class="explanation skVersionNumber">\n\n\t\t\t\t</span>\n\t\t\t</p>\n\t\t</div>\n\t</div>\n</div>\n';});

define('text!demoContent/EmployeeOnboarding.skore',[],function () { return '<mxGraphModel dx="1117" dy="610" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" background="#ffffff" skoreFileSpec="2"><root><mxCell id="0"><stylesheets as="stylesheets"><stylesheet value="default"/></stylesheets><roles as="roles"><role value="HR Partner"/><role value="IT coordinator"/><role value="Line Manager"/><role value="Future employee"/><role value="Whom it may concern"/><role value="Employee"/></roles></mxCell><title id="1">&#xa;&#xa;      <text value="onboarding a new employee"/><mxCell style="title" parent="0"/></title><stickynote id="222">&#xa;&#xa;      <text value="## Employee onboarding&#10;---"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="11" y="13" width="610" height="107" as="geometry"/></mxCell></stickynote><whybox id="230">&#xa;&#xa;      <text value="job offer accepted by candidate"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="19" y="145" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="231">&#xa;      <text value="Prepare new employee arrival"/>&#xa;      <resource value="HR Partner"/><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="159" y="145" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="235">&#xa;&#xa;      <text value="job offer accepted by candidate"/><mxCell style="whybox" parent="231" vertex="1"><mxGeometry x="41" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="236">&#xa;<text value="Get IT / electronic equipments for the employee"/>&#xa;&#xa;      <resource value="HR Partner"/><mxCell style="whatboxdetailed" parent="231" vertex="1" collapsed="1"><mxGeometry x="954" y="198" width="190" height="101" as="geometry"/></mxCell></whatbox><whatbox id="237">&#xa;<text value="Create all IT permissions"/>&#xa;<resource value="IT coordinator"/>&#xa;&#xa;      <attachment type="text" value="* Active Directory&#10;* ERP (SAP, ...)&#10;* Cloud (Salesforce, Google, ...)"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="773" y="89" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="238">&#xa;&#xa;      <text value="all accounts ready and summary sheet ready"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="983" y="89" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="239" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="237" target="238" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="240">&#xa;<text value="Complete initial IT accounts request"/>&#xa;&#xa;      <resource value="HR Partner"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="72" y="89" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="241">&#xa;&#xa;      <text value="standard IT needs of the employee established"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="282" y="89" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="242" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="240" target="241" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="243">&#xa;<text value="Finalise specific IT needs for the employee"/>&#xa;&#xa;      <resource value="Line Manager"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="422" y="89" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="244" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="241" target="243" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="245">&#xa;&#xa;      <text value="extra IT needs reviewed and passed on to IT"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="632" y="89" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="246" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="243" target="245" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="247" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="245" target="237" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="248" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="245" target="254" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="249" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="254" target="255" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="250" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="245" target="252" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="251" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="252" target="253" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="252">&#xa;<text value="Setup email"/>&#xa;&#xa;      <resource value="IT coordinator"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="772" y="347" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="253">&#xa;&#xa;      <text value="email added to relevant groups / mailing lists"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="982" y="347" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="254">&#xa;<text value="Order IT equipment (laptop)"/>&#xa;&#xa;      <resource value="IT coordinator"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="772" y="218" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="255">&#xa;&#xa;      <text value="equipment recevied"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="982" y="218" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="256" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="253" target="262" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="257" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="262" target="263" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="258" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="245" target="260" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="259" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="260" target="261" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="260">&#xa;<text value="Get phone number(s)"/>&#xa;<resource value="IT coordinator"/>&#xa;&#xa;      <attachment type="text" value="* landline&#10;* mobile&#10;* VoIP"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="772" y="497" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="261">&#xa;&#xa;      <text value="phone(s) setup, contract OK"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="982" y="497" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="262">&#xa;<text value="Provide email / phone number to HR for the business cards"/>&#xa;&#xa;      <resource value="IT coordinator"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="1122" y="417" width="190" height="101" as="geometry"/></mxCell></whatbox><whybox id="263">&#xa;&#xa;      <text value="HR aware of progress"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="1332" y="417" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="264" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="261" target="262" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="265">&#xa;<text value="Prepare equipment for employee"/>&#xa;&#xa;      <resource value="IT coordinator"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="1122" y="218" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="266" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="255" target="265" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="267">&#xa;&#xa;      <text value="equipment tested and ready. Initial log in performed"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="1332" y="218" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="268" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="265" target="267" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="269" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="238" target="271" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1485" y="129"/><mxPoint x="1485" y="198"/></Array></mxGeometry></mxCell><mxCell id="270" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="271" target="272" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="271">&#xa;<text value="Prepare &quot;summary sheet&quot; for employee"/>&#xa;&#xa;      <resource value="IT coordinator"/><mxCell style="whatbox" parent="236" vertex="1" collapsed="1"><mxGeometry x="1543" y="158" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="272">&#xa;&#xa;      <text value="Employee has all she / he needs to get started with systems"/><mxCell style="whybox" parent="236" vertex="1"><mxGeometry x="1753" y="158" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="273" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="267" target="271" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="274" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="236" source="263" target="271" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="275">&#xa;&#xa;      <text value="all IT accounts and equipments ready for the first day"/><mxCell style="whybox" parent="231" vertex="1"><mxGeometry x="1164" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="276" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="236" target="275" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="277">&#xa;<text value="Prepare contract "/>&#xa;<resource value="HR Partner"/>&#xa;&#xa;      <attachment type="text" value="&#10;* Review by legal if necesary&#10;* If stocks, review by Finance&#10;"/><mxCell style="whatbox" parent="231" vertex="1" collapsed="1"><mxGeometry x="192" y="198" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="278" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="235" target="277" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="279">&#xa;&#xa;      <text value="contract sent to future employee"/><mxCell style="whybox" parent="231" vertex="1"><mxGeometry x="402" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="280" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="277" target="279" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="282">&#xa;<text value="Sign &amp; post contract back"/>&#xa;&#xa;      <resource value="Future employee"/><mxCell style="whatbox" parent="231" vertex="1" collapsed="1"><mxGeometry x="563" y="198" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="283" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="279" target="282" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="284">&#xa;&#xa;      <text value="contract received by company"/><mxCell style="whybox" parent="231" vertex="1"><mxGeometry x="773" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="285" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="282" target="284" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="286" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="284" target="236" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><mxCell id="287" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="284" target="288" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><whatbox id="288">&#xa;<text value="Define initial employee objective"/>&#xa;&#xa;      <resource value="Line Manager"/><mxCell style="whatbox" parent="231" vertex="1" collapsed="1"><mxGeometry x="954" y="435" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="289">&#xa;&#xa;      <text value="early objectives for the employee to be used for onboarding plan"/><mxCell style="whybox" parent="231" vertex="1"><mxGeometry x="1164" y="435" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="290" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="288" target="289" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="291">&#xa;<text value="Customize onboarding plan"/>&#xa;&#xa;      <resource value="Line Manager"/><mxCell style="whatbox" parent="231" vertex="1" collapsed="1"><mxGeometry x="1314" y="435" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="292">&#xa;&#xa;      <text value="plan approved by Line Manager &amp; HR"/><mxCell style="whybox" parent="231" vertex="1"><mxGeometry x="1524" y="435" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="293" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="291" target="292" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="294">&#xa;<text value="Get other equipments for the employee"/>&#xa;<resource value="HR Partner"/>&#xa;&#xa;      <attachment type="text" value="* Assign desk + chair + furniture &#10;* Badge&#10;* Office keys&#10;* Locker + keys&#10;* Car park spot&#10;&#10;* Company credit card&#10;&#10;* business cards &#10;&#10;* Company &quot;Welcome pack&quot; with bag, pen, notepad, etc&#10;&#10;* Review special equipment needs with line managers (safety shoes, glasses, etc.)"/><mxCell style="whatbox" parent="231" vertex="1" collapsed="1"><mxGeometry x="1308" y="322" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="295" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="284" target="294" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="924" y="238"/><mxPoint x="924" y="362"/></Array></mxGeometry></mxCell><whybox id="296">&#xa;&#xa;      <text value="all equipments ready for employee"/><mxCell style="whybox" parent="231" vertex="1"><mxGeometry x="1518" y="322" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="297" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="294" target="296" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="298">&#xa;<text value="Announce arrival of new employee"/>&#xa;&#xa;      <resource value="HR Partner"/><mxCell style="whatboxdetailed" parent="231" vertex="1" collapsed="1"><mxGeometry x="954" y="571" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="299">&#xa;<text value="Announce the arrival within the team"/>&#xa;<resource value="Line Manager"/>&#xa;      &#xa;&#xa;      <attachment type="text" value="if internal candidates have been rejected, good to take time with them to explain why"/><mxCell style="whatbox" parent="298" vertex="1" collapsed="1"><mxGeometry x="181" y="220" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="300">&#xa;&#xa;      <text value="name, mission and arrival date confirmed to the team"/><mxCell style="whybox" parent="298" vertex="1"><mxGeometry x="391" y="220" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="301" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="298" source="299" target="300" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="302" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="298" source="303" target="304" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="303">&#xa;<text value="Finalise communication to be sent per email"/>&#xa;&#xa;      <resource value="Line Manager"/><mxCell style="whatbox" parent="298" vertex="1" collapsed="1"><mxGeometry x="529" y="220" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="304">&#xa;&#xa;      <text value="draft sent to HR"/><mxCell style="whybox" parent="298" vertex="1"><mxGeometry x="739" y="220" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="305" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="298" source="300" target="303" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="306">&#xa;<text value="Review &amp; send communication 2 weeks before arrival"/>&#xa;&#xa;      <resource value="HR Partner"/><mxCell style="whatbox" parent="298" vertex="1" collapsed="1"><mxGeometry x="879" y="220" width="190" height="101" as="geometry"/></mxCell></whatbox><mxCell id="307" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="298" source="304" target="306" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="308">&#xa;&#xa;      <text value="arrival officially announced to company"/><mxCell style="whybox" parent="298" vertex="1"><mxGeometry x="1089" y="220" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="309" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="298" source="306" target="308" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="310" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="298" source="312" target="299" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><stickynote id="311">&#xa;&#xa;      <text value="## Principle: &#10;* The line manager annouces to the team&#10;&#10;* HR sends official communication to the rest of the business"/><mxCell style="stickynote;fillColor=#FCF393;" parent="298" vertex="1"><mxGeometry x="36" y="26" width="543" height="139" as="geometry"/></mxCell></stickynote><whybox id="312">&#xa;&#xa;      <text value="contract received by company"/><mxCell style="whybox" parent="298" vertex="1"><mxGeometry x="30" y="220" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="313" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="289" target="291" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="314">&#xa;&#xa;      <text value="Employee official annouced to the company staff"/><mxCell style="whybox" parent="231" vertex="1"><mxGeometry x="1164" y="571" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="317" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="275" target="294" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="408" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="231" source="298" target="314" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="232" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="230" target="231" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="233">&#xa;      <text value="administrative, IT and onboarding plan ready"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="369" y="145" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="234" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="231" target="233" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="321">&#xa;      <text value="Get ready for arrival (1 week before arrival)"/>&#xa;      <resource value="HR Partner"/><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="509" y="145" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="325">&#xa;<text value="Review arrival checklist with Line Manager"/>&#xa;&#xa;      <resource value="HR Partner"/><mxCell style="whatbox" parent="321" vertex="1" collapsed="1"><mxGeometry x="195" y="96" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="326">&#xa;&#xa;      <text value="missing elements identified"/><mxCell style="whybox" parent="321" vertex="1"><mxGeometry x="405" y="96" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="327" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="321" source="325" target="326" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="328">&#xa;<text value="Finalise all tasks as appropriate"/>&#xa;&#xa;      <resource value="Whom it may concern"/><mxCell style="whatbox" parent="321" vertex="1" collapsed="1"><mxGeometry x="545" y="96" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="329" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="321" source="326" target="328" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="330">&#xa;&#xa;      <text value="Line Manager notified that everything is ready"/><mxCell style="whybox" parent="321" vertex="1"><mxGeometry x="777" y="96" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="331" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="321" source="328" target="330" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="332">&#xa;&#xa;      <text value="Close to employee arrival (1 week)"/><mxCell style="whybox" parent="321" vertex="1"><mxGeometry x="55" y="96" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="333" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="321" source="332" target="325" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="322" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="233" target="321" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="323">&#xa;      <text value="Line Manager notified everything is ready"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="729" y="145" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="324" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="321" target="323" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="334">&#xa;      <text value="Welcome employee (first days)"/>&#xa;      <resource value="HR Partner"/><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="96" y="331" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="338">&#xa;<text value="Follow Security, Safety &amp; Health eLearning"/>&#xa;&#xa;      <resource value="Employee"/><mxCell style="whatbox" parent="334" vertex="1" collapsed="1"><mxGeometry x="879" y="229" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="339">&#xa;&#xa;      <text value="basics understood"/><mxCell style="whybox" parent="334" vertex="1"><mxGeometry x="1089" y="229" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="340" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="338" target="339" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="341">&#xa;<text value="Introduce future employee to facilities"/>&#xa;<resource value="HR Partner"/>&#xa;      &#xa;&#xa;      <attachment type="text" value="* building layout&#10;* opening hours&#10;* badge, access, security...&#10;* kitchen(s)&#10;* toilets, etc.&#10;* canteen, etc.&#10;&#10;* office supplies"/><mxCell style="whatbox" parent="334" vertex="1" collapsed="1"><mxGeometry x="522" y="229" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="342">&#xa;&#xa;      <text value="employee knows how things work"/><mxCell style="whybox" parent="334" vertex="1"><mxGeometry x="732" y="229" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="344" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="341" target="342" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="345">&#xa;<text value="Introduce employee to her / his team"/>&#xa;<resource value="Line Manager"/>&#xa;      &#xa;&#xa;      <attachment type="text" value="* introduce the roles of each team member&#10;&#10;* clarify role of the new employee in  the team&#10;&#10;* organise tel- / videoconference with remote members&#10;"/><mxCell style="whatbox" parent="334" vertex="1" collapsed="1"><mxGeometry x="173" y="229" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="346">&#xa;&#xa;      <text value="new colleagues know each other"/><mxCell style="whybox" parent="334" vertex="1"><mxGeometry x="383" y="229" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="347" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="345" target="346" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="348" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="346" target="341" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="349">&#xa;<text value="Introduce employee to IT system"/>&#xa;&#xa;      <resource value="IT coordinator"/><mxCell style="whatbox" parent="334" vertex="1" collapsed="1"><mxGeometry x="522" y="437" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="350" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="346" target="349" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="351"><mxCell style="whybox" parent="334" vertex="1"><mxGeometry x="732" y="437" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="352" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="349" target="351" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="353" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="342" target="338" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="354">&#xa;<text value="Start onboarding programme / training"/>&#xa;&#xa;      <resource value="HR Partner"/><mxCell style="whatbox" parent="334" vertex="1" collapsed="1"><mxGeometry x="173" y="537" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="355">&#xa;&#xa;      <text value="employee following the initial training required for the job"/><mxCell style="whybox" parent="334" vertex="1"><mxGeometry x="383" y="537" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="356" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="354" target="355" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="357">&#xa;&#xa;      <text value="Employee arrived at agreed time"/><mxCell style="whybox" parent="334" vertex="1"><mxGeometry x="33" y="229" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="358" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="357" target="345" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="359">&#xa;&#xa;      <text value="onboarding programme defined earlier"/><mxCell style="whybox" parent="334" vertex="1"><mxGeometry x="33" y="537" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="360" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="359" target="354" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="361">&#xa;      <text value="Introduce employee to various policies in place"/>&#xa;      <resource value="HR Partner"/>&#xa;      <attachment type="text" value="* Holidays&#10;* working hours&#10;* overtime&#10;* ..."/><mxCell style="whatbox" parent="334" vertex="1" collapsed="1"><mxGeometry x="872" y="349" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="362" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="342" target="361" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="363"><mxCell style="whybox" parent="334" vertex="1"><mxGeometry x="1082" y="349" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="364" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="334" source="361" target="363" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><stickynote id="343">&#xa;&#xa;      <text value="## Employee arrival (first day)&#10;&#10;&lt;!-- A comment visible only in edit mode --&gt;"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="334" vertex="1"><mxGeometry x="9" y="36" width="610" height="80" as="geometry"/></mxCell></stickynote><mxCell id="335" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="323" target="334" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="336">&#xa;      <text value="employee understood the working in the company"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="306" y="331" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="337" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="334" target="336" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="365">&#xa;      <text value="Follow up employee onboarding during the next 100 days"/>&#xa;      <resource value="Line Manager"/><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="446" y="331" width="190" height="101" as="geometry"/></mxCell></whatbox><whatbox id="393">&#xa;<text value="Perform 100 days review"/>&#xa;&#xa;      <resource value="Line Manager"/><mxCell style="whatbox" parent="365" vertex="1" collapsed="1"><mxGeometry x="158" y="179" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="394">&#xa;&#xa;      <text value="confirmation employee is comfortable in new role"/><mxCell style="whybox" parent="365" vertex="1"><mxGeometry x="376" y="179" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="395" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="365" source="393" target="394" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="403">&#xa;&#xa;      <text value="100 days after arrival"/><mxCell style="whybox" parent="365" vertex="1"><mxGeometry x="25" y="179" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="404" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="365" source="403" target="393" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><group id="397">&#xa;&#xa;      <text value="Repeat every 6 months"/><mxCell style="group" parent="365" vertex="1" connectable="0"><mxGeometry x="25" y="327" width="548" height="110" as="geometry"/></mxCell></group><whatbox id="398">&#xa;<text value="Discuss &amp; agree objectives with employee"/>&#xa;&#xa;      <resource value="Line Manager"/><mxCell style="whatbox" parent="397" vertex="1" collapsed="1"><mxGeometry x="162" y="15" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="399">&#xa;&#xa;      <text value="objective recorded in system"/><mxCell style="whybox" parent="397" vertex="1"><mxGeometry x="380" y="15" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="400" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="397" source="398" target="399" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="401">&#xa;&#xa;      <text value="Employee has better understanding of her / his role"/><mxCell style="whybox" parent="397" vertex="1"><mxGeometry x="22" y="15" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="402" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="397" source="401" target="398" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><stickynote id="396">&#xa;&#xa;      <text value="## Employee onboarding period finished"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="365" vertex="1"><mxGeometry x="43" y="46" width="610" height="80" as="geometry"/></mxCell></stickynote><mxCell id="366" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="336" target="365" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="367">&#xa;      <text value="employee comfortable in role"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="656" y="331" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="368" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="365" target="367" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell></root></mxGraphModel>';});


define('text!demoContent/training_company_process.skore',[],function () { return '<mxGraphModel dx="1102" dy="721" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" background="#ffffff"><root><mxCell id="0"><stylesheets as="stylesheets">&#xa;  <stylesheet value="default">&#xa;    <style value="whatbox" styleTokens="fontColor=darkslategray;strokeColor=steelblue;strokeWidth=2;rounded=1;"/>&#xa;    <style value="whybox" styleTokens="fontColor=darkslategray;strokeColor=transparent;"/>&#xa;    <style value="line" styleTokens="strokeColor=steelblue;"/>&#xa;    <style value="stickynote" styleTokens="fontColor=darkslategray;"/>&#xa;    <style value="group" styleTokens=""/>&#xa;    <style value="wy2s" styleTokens=""/>&#xa;  </stylesheet>&#xa;</stylesheets><roles as="roles"><role value="Learning Designer"/><role value="Marketing Manager"/><role value="Training Manager"/><role value="Finance Administrator"/></roles></mxCell><title id="1"><text value="Training Company Process"/><mxCell style="title" parent="0"/></title><mxCell id="2" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="22" target="31" edge="1"><mxGeometry x="768.75" y="529.5" as="geometry"/></mxCell><mxCell id="3" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="23" target="30" edge="1"><mxGeometry x="768.75" y="672" as="geometry"/></mxCell><mxCell id="4" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="29" target="23" edge="1"><mxGeometry x="547.5" y="529.5" as="geometry"/></mxCell><mxCell id="5" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="29" target="22" edge="1"><mxGeometry x="547.5" y="529.5" as="geometry"/></mxCell><mxCell id="6" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="21" target="29" edge="1"><mxGeometry x="402.5" y="529.5" as="geometry"/></mxCell><mxCell id="7" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="28" target="21" edge="1"><mxGeometry x="162.5" y="529.5" as="geometry"/></mxCell><mxCell id="8" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="17" target="28" edge="1"><mxGeometry x="32.5" y="289.5" as="geometry"/></mxCell><mxCell id="9" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="27" target="17" edge="1"><mxGeometry x="913.75" y="290.75" as="geometry"/></mxCell><mxCell id="10" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="20" target="27" edge="1"><mxGeometry x="768.75" y="290.75" as="geometry"/></mxCell><mxCell id="11" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="26" target="20" edge="1"><mxGeometry x="555" y="290.75" as="geometry"/></mxCell><mxCell id="12" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="16" target="26" edge="1"><mxGeometry x="410" y="290.75" as="geometry"/></mxCell><mxCell id="13" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="25" target="16" edge="1"><mxGeometry x="163.75" y="290.75" as="geometry"/></mxCell><mxCell id="14" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="24" target="16" edge="1"><mxGeometry x="163.75" y="244.5" as="geometry"/></mxCell><whatbox id="16"><text value="Design Courses"/><resource value="Learning Designer"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="219.88888888888889" y="251.12345679012344" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="17"><text value="Market and sell training"/><resource value="Marketing Manager"/><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="938.4320987654321" y="250.82716049382714" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="20"><text value="Schedule public training"/><resource value="Training Manager"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="578.753086419753" y="250.8395061728395" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="21"><text value="Deliver training"/><resource value="Training Manager"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="213.09876543209873" y="489.753086419753" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="22"><text value="Review feedback"/><resource value="Training Manager"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="579.1851851851852" y="489.5185185185185" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="23"><text value="Collect payment"/><resource value="Finance Administrator"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="578.2839506172838" y="632.5185185185185" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="24"><text value="Company Strategy"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="43.82716049382715" y="203.91358024691357" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="25"><text value="Market Research"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="44" y="316.3086419753086" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="26"><text value="Courses ready for delivery"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="434.8888888888889" y="251.12345679012344" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="27"><text value="Course calendar available"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="793.753086419753" y="250.8395061728395" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="28"><text value="Demand for training"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="42.53086419753086" y="490.49382716049377" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="29"><text value="Course completion info and feedback"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="428.09876543209873" y="489.753086419753" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="30"><text value="Bill settled"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="793.2839506172838" y="632.5185185185185" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="31"><text value="Improved courses"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="794.1851851851852" y="489.5185185185185" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="32"><text value="##Training Company Process"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="11" y="14" width="1010" height="89" as="geometry"/></mxCell></stickynote><stickynote id="33"><text value="![icon 2x](calendar)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="655" y="199" width="67" height="47" as="geometry"/></mxCell></stickynote><stickynote id="34"><text value="![icon 2x](recycle)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="655" y="450" width="80" height="43" as="geometry"/></mxCell></stickynote><stickynote id="35"><text value="![icon 2x](shopping-cart)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="1009" y="199" width="57" height="47" as="geometry"/></mxCell></stickynote><stickynote id="36"><text value="![icon 2x](dollar)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="655" y="592" width="64" height="40" as="geometry"/></mxCell></stickynote><stickynote id="37"><text value="![icon 2x](pencil)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="298" y="199" width="95" height="52" as="geometry"/></mxCell></stickynote><stickynote id="38"><text value="![icon 2x](truck)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="281" y="450" width="60" height="45" as="geometry"/></mxCell></stickynote></root></mxGraphModel>';});


define('text!demoContent/DesignProcess.skore',[],function () { return '<mxGraphModel dx="100" dy="100" grid="0" guides="1" tooltips="0" connect="1" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826"><root><mxCell id="0"><stylesheets as="stylesheets"><stylesheet value="default"><style value="whatbox" styleTokens="fontColor=darkslategrey;strokeColor=yellowgreen;rounded=1;fillColor=yellowgreen;"/><style value="whybox" styleTokens="fontColor=darkslategrey;strokeColor=transparent;"/><style value="line" styleTokens="strokeColor=yellowgreen;"/><style value="stickynote" styleTokens="fontColor=darkslategrey;"/><style value="group" styleTokens=""/><style value="wy2s" styleTokens=""/></stylesheet></stylesheets><roles as="roles"><role value="."/></roles></mxCell><title id="1"><text value="Design Process"/><mxCell style="title" parent="0"><mxRectangle x="97" y="39" width="1274" height="802" as="visibleRect"/></mxCell></title><mxCell id="29" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="6" target="28" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="27" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="25" target="6" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="26" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="5" target="25" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="24" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="22" target="5" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1058" y="186"/><mxPoint x="1058" y="298"/><mxPoint x="238" y="298"/><mxPoint x="238" y="410"/></Array></mxGeometry></mxCell><mxCell id="23" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="4" target="22" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="21" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="19" target="4" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="20" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="3" target="19" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="17" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="2" target="3" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="2"><text value="Requirement for new design"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="32" y="146" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="3"><text value="Discover&#xa;&#xa;![icon 2x white](binoculars)"/><resource value="."/><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="206" y="146" width="190" height="90" as="geometry"/><mxRectangle x="-47" y="-142" width="1274" height="802" as="visibleRect"/></mxCell></whatbox><whatbox id="4"><text value="Design interactions&#xa;&#xa;![icon 2x white](hand-pointer-o)"/><resource value="."/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="592" y="146" width="190" height="90" as="geometry"/></mxCell></whatbox><whatbox id="5"><text value="Design visuals&#xa;&#xa;![icon 2x white](picture-o)"/><resource value="."/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="206" y="370" width="190" height="90" as="geometry"/></mxCell></whatbox><whatbox id="6"><text value="Build&#xa;&#xa;![icon 2x white](industry)"/><resource value="."/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="592" y="370" width="190" height="90" as="geometry"/></mxCell></whatbox><whybox id="19"><text value="Business requirements"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="432" y="146" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="22"><text value="Wireframes and plan"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="832" y="146" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="25"><text value="Comps and styleguide"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="432" y="370" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="28"><text value="Launched product"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="832" y="370" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="36"><text value="#Design process&#xa;---"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="16" y="16" width="899" height="106" as="geometry"/></mxCell></stickynote></root></mxGraphModel>';});


define('text!demoContent/TransformationApproach.skore',[],function () { return '<mxGraphModel dx="1117" dy="610" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="1" fold="1" page="1" pageScale="1.1" pageWidth="1169" pageHeight="826" background="#ffffff" skoreFileSpec="2"><root><mxCell id="0"><stylesheets as="stylesheets">&#xa;&#xa;&#xa;\t\t\t\t<stylesheet value="default">&#xa;&#xa;&#xa;\t\t\t\t\t<style styleTokens="fontColor=royalblue;strokeColor=darkblue;strokeWidth=2;rounded=1;" value="whatbox"/>&#xa;&#xa;&#xa;\t\t\t\t\t<style styleTokens="fontColor=royalblue;strokeColor=transparent;" value="whybox"/>&#xa;&#xa;&#xa;\t\t\t\t\t<style styleTokens="" value="line"/>&#xa;&#xa;&#xa;\t\t\t\t\t<style styleTokens="fontColor=black;" value="stickynote"/>&#xa;&#xa;&#xa;\t\t\t\t\t<style styleTokens="" value="group"/>&#xa;&#xa;&#xa;\t\t\t\t\t<style styleTokens="" value="wy2s"/>&#xa;&#xa;&#xa;\t\t\t\t</stylesheet>&#xa;&#xa;&#xa;\t\t\t</stylesheets><roles as="roles">&#xa;&#xa;&#xa;\t\t\t\t<role value="Analyst"/>&#xa;&#xa;&#xa;\t\t\t\t<role value="CEO"/>&#xa;&#xa;&#xa;\t\t\t\t<role value="Initiative Owner"/>&#xa;&#xa;&#xa;\t\t\t</roles></mxCell><title id="1">&#xa;&#xa;&#xa;\t\t\t<text value="Transformation approach"/><mxCell style="title" parent="0"/></title><mxCell id="19" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="16" target="18" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="17" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="11" target="16" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="15" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="4" target="5" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="14" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="2" target="5" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="13" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="3" target="5" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="12" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="9" target="11" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="10" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="7" target="9" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="8" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="5" target="7" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="2">&#xa;&#xa;&#xa;\t\t\t<text value="![icon 2x darkblue](area-chart)&#10;&#10;Market trends"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="17" y="290" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="3">&#xa;&#xa;&#xa;\t\t\t<text value="![icon 2x darkblue](bullhorn)&#10;&#10;Voice of the customer"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="17" y="392" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="4">&#xa;&#xa;&#xa;\t\t\t<text value="![icon 2x darkblue](pie-chart)&#10;&#10;Market share"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="17" y="493" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="5">&#xa;&#xa;&#xa;\t\t\t<text value="![icon 3x darkblue](cubes)&#10;&#10;Analyse current situation"/>&#xa;&#xa;&#xa;\t\t\t<resource value="Analyst"/>&#xa;&#xa;&#xa;\t\t\t<attachment type="text" value="Market research, customer interviews, employee interviews, surveys, competitor analysis."/><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="177" y="392" width="190" height="118" as="geometry"/></mxCell></whatbox><whybox id="7">&#xa;&#xa;&#xa;\t\t\t<text value="Current state"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="392" y="392" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="9">&#xa;&#xa;&#xa;\t\t\t<text value="![icon 3x darkblue](binoculars)&#10;&#10;Define vision and strategy"/>&#xa;&#xa;&#xa;\t\t\t<resource value="CEO">&#xa;\t\t\t\t&#xa;\t\t\t\t&#xa;\t\t\t</resource><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="552" y="392" width="190" height="118" as="geometry"/></mxCell></whatbox><whybox id="11">&#xa;&#xa;&#xa;\t\t\t<text value="Strategic initiatives, goals and measures"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="767" y="392" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="16">&#xa;&#xa;&#xa;\t\t\t<text value="![icon 3x darkblue](cogs)&#10;&#10;Implement strategy"/>&#xa;&#xa;&#xa;\t\t\t<resource value="CEO"/><mxCell style="whatboxdetailed" parent="1" collapsed="1" vertex="1"><mxGeometry x="927" y="392" width="190" height="118" as="geometry"/></mxCell></whatbox><stickynote id="54">&#xa;&#xa;&#xa;\t\t\t<text value="#Implement Strategy&#10;---&#10;**Purpose:** To understand, define, plan and approve each individual strategi initiative."/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="16" vertex="1"><mxGeometry x="27" y="36" width="1219" height="143" as="geometry"/></mxCell></stickynote><mxCell id="39" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="16" source="34" target="38" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="35" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="16" source="32" target="34" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="31" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="16" source="28" target="30" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1033" y="276"/><mxPoint x="1033" y="382"/><mxPoint x="257" y="382"/><mxPoint x="257" y="487"/></Array></mxGeometry></mxCell><mxCell id="33" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="16" source="30" target="32" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="32">&#xa;&#xa;&#xa;\t\t\t<text value="Project budget and targets"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="514" y="447" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="29" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="16" source="26" target="28" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="27" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="16" source="24" target="26" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="25" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="16" source="22" target="24" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="23" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="16" source="20" target="22" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="20">&#xa;&#xa;&#xa;\t\t\t<text value="Strategic initiatives, goals and measures"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="139" y="236" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="22">&#xa;&#xa;&#xa;\t\t\t<text value="Analyse current state"/>&#xa;&#xa;&#xa;\t\t\t<resource value="Analyst"/><mxCell style="whatbox" parent="16" vertex="1" collapsed="1"><mxGeometry x="299" y="236" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="24">&#xa;&#xa;&#xa;\t\t\t<text value="Highlevel business requirements"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="514" y="236" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="26">&#xa;&#xa;&#xa;\t\t\t<text value="Design future state"/>&#xa;&#xa;&#xa;\t\t\t<resource value="Initiative Owner"/><mxCell style="whatbox" parent="16" vertex="1" collapsed="1"><mxGeometry x="674" y="236" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="28">&#xa;&#xa;&#xa;\t\t\t<text value="Highlevel project plan"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="889" y="236" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="34">&#xa;&#xa;&#xa;\t\t\t<text value="Execute strategic initiative"/>&#xa;&#xa;&#xa;\t\t\t<resource value="Initiative Owner"/><mxCell style="whatbox" parent="16" vertex="1" collapsed="1"><mxGeometry x="674" y="447" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="30">&#xa;&#xa;&#xa;\t\t\t<text value="![icon 3x crimson](flag-checkered)&#10;&#10;Approve strategic initiative"/>&#xa;&#xa;&#xa;\t\t\t<resource value="CEO"/><mxCell style="whatbox" parent="16" vertex="1" collapsed="1"><mxGeometry x="299" y="447" width="190" height="118" as="geometry"/></mxCell></whatbox><whybox id="38">&#xa;&#xa;&#xa;\t\t\t<text value="Improved results"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="917" y="447" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="18">&#xa;&#xa;&#xa;\t\t\t<text value="![icon 2x darkblue](line-chart)&#10;&#10;Improved results"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1142" y="392" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="37">&#xa;&#xa;&#xa;\t\t\t<text value="#Transformation Approach&#10;&#10;  ---  &#10;**Purpose:** To understand the current situation, define and implement a strategy that will lead to better results for the company, its employees, customers and shareholders."/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="17" y="26" width="609" height="179" as="geometry"/></mxCell></stickynote><stickynote id="55"><text value="![icon 2x ](graduation-cap) **Noteworthy in this demo content**&#10;&#10;* very high level view to give a bigger picture before going into details&#10;* use of icons to make it easier to read&#10;* use of custom styles&#10;&#10;How to create custom styles ? &#10;1. Go to menu : **Customize** &gt; **Stylesheet**&#10;2. Edit the stylesheet for the map&#10;3. You can make it your default stylesheet for future map too&#10;&#10;Note : you can style box individually from the **Box Style** menu"/><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="647" y="19" width="364" height="314" as="geometry"/></mxCell></stickynote></root></mxGraphModel>';});


define('text!demoContent/npd.skore',[],function () { return '<mxGraphModel dx="1328" dy="429" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="1" fold="1" page="0" pageScale="1.1" pageWidth="1169" pageHeight="826" background="#ffffff"><root><mxCell id="0"><roles as="roles"><role value="Chief Innovation Officer"/><role value="Chier Operations Officer"/><role value="Chief Commercial Officer"/></roles><stylesheets as="stylesheets"><stylesheet value="default"/></stylesheets></mxCell><title id="1"><text value="New Product Development"/><mxCell style="title" parent="0"/></title><mxCell id="49" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="48" target="32" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="34" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="32" target="18" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="33" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="3" target="32" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="27" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="24" target="26" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="25" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="22" target="24" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="23" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="20" target="22" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="21" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="18" target="20" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><stickynote id="2"><text value="#The New Product Development (NPD) &#10;&#10;process from [wikipedia](https://en.wikipedia.org/wiki/New_product_development)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="25" y="18" width="732" height="133" as="geometry"/></mxCell></stickynote><whybox id="3"><text value="Company Strategy"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="25" y="305" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="18"><text value="![icon 3x](lightbulb-o)&#10;&#10;Ideas ready for prototyping"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="395" y="376" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="20"><text value="Build"/><resource value="Chier Operations Officer"/><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="538" y="376" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="69" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="66" target="68" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="82" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="74" target="81" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="75" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="72" target="74" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="73" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="70" target="72" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="71" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="68" target="70" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="67" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="64" target="66" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="65" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="62" target="64" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="63" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="60" target="62" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="61" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="58" target="60" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="59" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="20" source="57" target="58" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="57"><text value="Ideas ready for prototyping"/><mxCell style="whybox" parent="20" vertex="1"><mxGeometry x="57.14285714285717" y="201.2857142857143" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="58"><text value="Build prototypes"/><resource value=""/><mxCell style="whatbox" parent="20" vertex="1"><mxGeometry x="217.14285714285717" y="201.2857142857143" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="60"><text value="Prototypes ready for validation"/><mxCell style="whybox" parent="20" vertex="1"><mxGeometry x="432.14285714285717" y="201.2857142857143" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="62"><text value="Test with consumers"/><resource value=""/><mxCell style="whatbox" parent="20" vertex="1"><mxGeometry x="592.1428571428571" y="201.2857142857143" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="64"><text value="Customer feedback"/><mxCell style="whybox" parent="20" vertex="1"><mxGeometry x="807.1428571428571" y="201.2857142857143" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="66"><text value="Design final product"/><resource value=""/><mxCell style="whatbox" parent="20" vertex="1"><mxGeometry x="967.1428571428571" y="201.2857142857143" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="68"><text value="Designs ready for production"/><mxCell style="whybox" parent="20" vertex="1"><mxGeometry x="57.14285714285711" y="361.28571428571433" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="70"><text value="Produce product"/><resource value=""/><mxCell style="whatbox" parent="20" vertex="1"><mxGeometry x="217.1428571428571" y="361.28571428571433" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="72"><text value="Product ready for quality checking"/><mxCell style="whybox" parent="20" vertex="1"><mxGeometry x="432.1428571428571" y="361.28571428571433" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="74"><text value="Test product"/><resource value=""/><mxCell style="whatbox" parent="20" vertex="1"><mxGeometry x="592.1428571428571" y="361.28571428571433" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="81"><text value="Product ready for release"/><mxCell style="whybox" parent="20" vertex="1"><mxGeometry x="865.7142857142858" y="361.5714285714286" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="22"><text value="![icon 3x](cubes)&#10;&#10;Product ready for release"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="751" y="376" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="24"><text value="Commercialize"/><resource value="Chief Commercial Officer"/><attachment type="text" value="* Launch the product&#10;* Market and sell product&#10;* Distribute product"/><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="910" y="376" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="26"><text value="![icon 3x](usd)&#10;&#10;Product launched to marketplace"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1136" y="376" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="32"><text value="Ideate"/><resource value="Chief Innovation Officer"/><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="178" y="376" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="53" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="51" target="41" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="52" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="50" target="41" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="36" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="45" target="46" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="37" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="44" target="45" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="38" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="43" target="44" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="39" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="42" target="43" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="40" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="41" target="42" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="50"><text value="Company Strategy"/><mxCell style="whybox" parent="32" vertex="1"><mxGeometry x="34.285714285714306" y="82.85714285714286" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="51"><text value="Market and consumer trends"/><mxCell style="whybox" parent="32" vertex="1"><mxGeometry x="34.285714285714306" y="184.28571428571433" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="41"><text value="Generate Ideas"/><resource value=""/><attachment type="text" value="* Ideas for new products can be obtained from basic research using a SWOT analysis (Strengths, Weaknesses, Opportunities &amp; Threats). Market and consumer trends, company\'s R&amp;D department, competitors, focus groups, employees, salespeople, corporate spies, trade shows, or ethnographic discovery methods (searching for user patterns and habits) may also be used to get an insight into new product lines or product features.&#10;* Lots of ideas are generated about the new product. Out of these ideas many are implemented. The ideas are generated in many forms. Many reasons are responsible for generation of an idea.&#10;* Idea for new product can come from many sources, such as customer, scientists, competitors, employees, channel member, and top management.&#10;* customer need and wants are the logical place to start the search.&#10;* Ideas for new products can be obtained from basic research using a SWOT analysis (Strengths, Weaknesses, Opportunities &amp; Threats). Market and consumer trends, company\'s R&amp;D department, competitors, focus groups, employees, salespeople, corporate spies, trade shows, or ethnographic discovery methods (searching for user patterns and habits) may also be used to get an insight into new product lines or product features."/><mxCell style="whatbox" parent="32" vertex="1"><mxGeometry x="206" y="83" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="42"><text value="Initial ideas ready for screening"/><mxCell style="whybox" parent="32" vertex="1"><mxGeometry x="421" y="83" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="43"><text value="Screen Ideas"/><resource value=""/><attachment type="text" value="* Eliminate unsound concepts prior to devoting resources to them."/><attachment type="text" value="* Will the customer in the target market benefit from the product?&#10;What is the size and growth forecasts of the market segment / target market?&#10;* What is the current or expected competitive pressure for the product idea?&#10;* What are the industry sales and market trends the product idea is based on?&#10;* Is it technically feasible to manufacture the product?&#10;* Will the product be profitable when manufactured and delivered to the customer at the target price?"/><mxCell style="whatbox" parent="32" vertex="1"><mxGeometry x="558" y="83" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="44"><text value="Potential ideas ready for validation"/><mxCell style="whybox" parent="32" vertex="1"><mxGeometry x="773" y="83" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="56" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="47" target="55" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="35" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="32" source="46" target="47" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="45"><text value="Develop and test ideas"/><resource value=""/><attachment type="text" value="* Investigate intellectual property issues and search patent databases&#10;* Who is the target market and who is the decision maker in the purchasing process?&#10;* What product features must the product incorporate?&#10;* What benefits will the product provide?&#10;* How will consumers react to the product?&#10;* How will the product be produced most cost effectively?&#10;* Prove feasibility through virtual computer aided rendering and rapid prototyping&#10;* What will it cost to produce it?"/><mxCell style="whatbox" parent="32" vertex="1"><mxGeometry x="206" y="319" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="46"><text value="Shortlist of ideas"/><mxCell style="whybox" parent="32" vertex="1"><mxGeometry x="426" y="319" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="47"><text value="Analyze new ideas"/><resource value=""/><attachment type="text" value="* Estimate likely selling price based upon competition and customer feedback&#10;* Estimate sales volume based upon size of market and such tools as the Fourt-Woodlock equation&#10;* Estimate profitability and break-even point"/><mxCell style="whatbox" parent="32" vertex="1"><mxGeometry x="558" y="319" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="55"><text value="Ideas ready for prototyping"/><mxCell style="whybox" parent="32" vertex="1"><mxGeometry x="772.5714285714287" y="318.5714285714286" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="48"><text value="Market and consumer trends"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="24.85714285714286" y="461.7142857142857" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="83"><text value="![icon 2x ](graduation-cap) **Noteworthy in this demo content**&#10;&#10;* very high level view to give a bigger picture before going into details&#10;* use of icons in the whybox&#10;* use of attachments in the lower levels"/><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="31" y="158" width="364" height="135" as="geometry"/></mxCell></stickynote></root></mxGraphModel>';});


define('text!demoContent/projet-rouge.skore',[],function () { return '<mxGraphModel dx="1117" dy="610" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" background="#ffffff" skoreFileSpec="2"><root><mxCell id="0"><extension value="mrt" type="RATSI" as="tagsInUse">&#xa;        <tag value="R" style="background-color:rgba(255, 165, 0, 0.5)" name="Responsible"/>&#xa;        <tag value="A" style="background-color:rgba(255, 0, 0, 0.5)" name="Authority"/>&#xa;        <tag value="T" style="background-color:rgba(245, 245, 220, 0.5)" name="Task"/>&#xa;        <tag value="S" name="Support"/>&#xa;        <tag value="I" name="Informed"/>&#xa;      </extension><stylesheets as="stylesheets"><stylesheet value="default"/></stylesheets><roles as="roles"><role value="Marketing Director"/><role value="Marketing Manager"/><role value="Product Marketing Manager"/><role value="CEO"/><role value="VP Sales"/><role value="COO"/><role value="CTO"/><role value="Training Manager"/><role value="Events Manager"/><role value="Development Manager"/><role value="Marketing Assistant"/><role value="Copywriter"/><role value="Creative Designer"/><role value="Legal"/><role value="Translator"/><role value="Trainer"/><role value="External Relations Director"/><role value="Finance"/><role value="Requestor"/><role value="Line Manager"/><role value="Social Media Specialist"/><role value="Speaker"/><role value="Employee"/><role value="Web Designer"/></roles><extension value="handoverhighlight" color="purple" as="handoverhighlight"/></mxCell><title id="1">&#xa;      <text value="Project Rouge (Second pass at roles)"/><mxCell style="title" parent="0"/></title><mxCell id="742" style="linehighlight;exitX=0.5;exitY=1;entryX=0.5;entryY=0" parent="1" source="24" target="8" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="25" style="linehighlight;exitX=0.5;exitY=1;entryX=0.5;entryY=0" parent="1" source="12" target="24" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="23" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="4" target="5" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="22" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="3" target="5" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="21" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="2" target="5" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="20" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="14" target="16" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="19" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="16" target="18" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="17" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="10" target="16" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="15" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="12" target="14" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="13" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="6" target="12" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="11" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="8" target="10" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="9" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="6" target="8" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="7" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="5" target="6" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="2">&#xa;      <text value="Market Needs"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="5" y="430" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="3">&#xa;      <text value="Business strategy"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="5" y="532" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="4">&#xa;      <text value="Innovation pipeline"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="5" y="625" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="5">&#xa;      <text value="Prepare strategic and operational plans"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="150" y="532" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="44" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="39" target="28" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="43" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="40" target="28" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="42" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="41" target="28" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="40">&#xa;      <text value="Business strategy"/><mxCell style="whybox" parent="5" vertex="1"><mxGeometry x="5" y="224" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="39">&#xa;      <text value="Market Needs"/><mxCell style="whybox" parent="5" vertex="1"><mxGeometry x="5" y="110" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="41">&#xa;      <text value="Innovation pipeline"/><mxCell style="whybox" parent="5" vertex="1"><mxGeometry x="5" y="342" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="67" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="29" target="31" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="30" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="28" target="29" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="29">&#xa;      <text value="Themes agreed"/><mxCell style="whybox" parent="5" vertex="1"><mxGeometry x="372" y="224" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="36" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="33" target="35" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="34" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="31" target="33" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><whatbox id="31">&#xa;      <text value="Develop the 3 year plan"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="5" vertex="1" collapsed="1"><mxGeometry x="501" y="224" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="91" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="73" target="75" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="90" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="71" target="75" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="89" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="74" target="75" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="88" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="83" target="87" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="84" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="81" target="83" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="82" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="79" target="81" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="80" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="77" target="79" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="78" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="75" target="77" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="76" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="31" source="72" target="75" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="71">&#xa;      <text value="Business strategy"/><mxCell style="whybox" parent="31" vertex="1"><mxGeometry x="15" y="305" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="72">&#xa;      <text value="Market Needs"/><mxCell style="whybox" parent="31" vertex="1"><mxGeometry x="15" y="191" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="73">&#xa;      <text value="Innovation pipeline"/><mxCell style="whybox" parent="31" vertex="1"><mxGeometry x="15" y="423" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="74">&#xa;      <text value="Themes agreed"/><mxCell style="whybox" parent="31" vertex="1"><mxGeometry x="15" y="78" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="75">&#xa;      <text value="Analyze current situation"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Training Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="31" vertex="1" collapsed="1"><mxGeometry x="155" y="191" width="190" height="165" as="geometry"/></mxCell></whatbox><whybox id="77">&#xa;      <text value="Identified issues and opportunities"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="31" vertex="1"><mxGeometry x="365" y="191" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="79">&#xa;      <text value="Review and prioritize issues and opportunties"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="31" vertex="1" collapsed="1"><mxGeometry x="505" y="191" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="81">&#xa;      <text value="Draft plan"/><mxCell style="whybox" parent="31" vertex="1"><mxGeometry x="715" y="191" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="83">&#xa;      <text value="Approve plan and resources"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="A"/>&#xa;      </resource><mxCell style="whatbox;fontColor=#774400;detailColor=#067C7E;attachmentColor=#067C7E;strokeColor=red;strokeWidth=2;dashed=;rounded=;fillColor=transparent;opacity=100;" parent="31" vertex="1" collapsed="1"><mxGeometry x="855" y="191" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="87">&#xa;      <text value="3 yr plan approved"/><mxCell style="whybox" parent="31" vertex="1"><mxGeometry x="1065" y="191" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="744"><text value="![icon 2x ](graduation-cap) **Pro Tip**&#10;&#10;This activity is a decision. We highlight it in red so it stands out!&#10;&#10;How to?&#10;&#10;1. Select one or more box(es)&#10;2. Go to menu **Customize** &gt; **Style** to change the style of the selection"/><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="31" vertex="1"><mxGeometry x="826" y="332" width="297" height="198" as="geometry"/></mxCell></stickynote><mxCell id="38" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="35" target="37" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><whatbox id="35">&#xa;      <text value="Develop tactical operating plans"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="5" vertex="1" collapsed="1"><mxGeometry x="834" y="224" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="105" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="35" source="102" target="104" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><mxCell id="109" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="35" source="106" target="108" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><mxCell id="107" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="35" source="104" target="106" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="103" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="35" source="100" target="102" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="101" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="35" source="98" target="100" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="99" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="35" source="95" target="98" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="97" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="35" source="93" target="94" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="93">&#xa;      <text value="3 yr plan approved"/><mxCell style="whybox" parent="35" vertex="1"><mxGeometry x="22" y="81" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="96" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="35" source="94" target="95" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="94">&#xa;      <text value="Define tactical plans and resources"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Training Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="35" vertex="1" collapsed="1"><mxGeometry x="156" y="81" width="190" height="205" as="geometry"/></mxCell></whatbox><whybox id="95">&#xa;      <text value="Approved operating plans"/><mxCell style="whybox" parent="35" vertex="1"><mxGeometry x="366" y="81" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="98">&#xa;      <text value="Communicate plan to stakeholders"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="35" vertex="1" collapsed="1"><mxGeometry x="506" y="81" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="100">&#xa;      <text value="All stakeholders aligned with plans"/><mxCell style="whybox" parent="35" vertex="1"><mxGeometry x="716" y="81" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="102">&#xa;      <text value="Monitor implementation of the plans"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Training Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="35" vertex="1" collapsed="1"><mxGeometry x="856" y="81" width="190" height="205" as="geometry"/></mxCell></whatbox><whybox id="104">&#xa;      <text value="Performance metrics"/><mxCell style="whybox" parent="35" vertex="1"><mxGeometry x="161" y="419" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="106">&#xa;      <text value="Review performance against KPIs"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Training Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="35" vertex="1" collapsed="1"><mxGeometry x="301" y="419" width="190" height="205" as="geometry"/></mxCell></whatbox><whybox id="108">&#xa;      <text value="Improvements for next planning cycle"/><mxCell style="whybox" parent="35" vertex="1"><mxGeometry x="511" y="419" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="37">&#xa;      <text value="Approved operating plans"/><mxCell style="whybox" parent="5" vertex="1"><mxGeometry x="1049" y="224" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="28">&#xa;      <text value="Define the main communication themes"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="5" vertex="1" collapsed="1"><mxGeometry x="163" y="224" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="63" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="28" source="58" target="62" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="59" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="28" source="56" target="58" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="57" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="28" source="54" target="56" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="55" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="28" source="50" target="54" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="53" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="28" source="47" target="48" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="52" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="28" source="45" target="48" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="51" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="28" source="48" target="50" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="49" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="28" source="46" target="48" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="45">&#xa;      <text value="Innovation pipeline"/><mxCell style="whybox" parent="28" vertex="1"><mxGeometry x="9" y="93" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="46">&#xa;      <text value="Business strategy"/><mxCell style="whybox" parent="28" vertex="1"><mxGeometry x="9" y="173" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="47">&#xa;      <text value="Market Needs"/><mxCell style="whybox" parent="28" vertex="1"><mxGeometry x="9" y="253" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="48">&#xa;      <text value="Define themes"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="CEO">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="28" vertex="1" collapsed="1"><mxGeometry x="149" y="173" width="190" height="165" as="geometry"/></mxCell></whatbox><whybox id="50">&#xa;      <text value="Key themes on which external communications will be based"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="28" vertex="1"><mxGeometry x="359" y="173" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="54">&#xa;      <text value="Approve themes"/>&#xa;      <resource value="CEO">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="VP Sales">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="COO">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="CTO">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="28" vertex="1" collapsed="1"><mxGeometry x="499" y="173" width="190" height="195" as="geometry"/></mxCell></whatbox><whybox id="56">&#xa;      <text value="Themes agreed"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="28" vertex="1"><mxGeometry x="709" y="173" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="58">&#xa;      <text value="Review and update themes on an annual basis"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="28" vertex="1" collapsed="1"><mxGeometry x="849" y="173" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="62">&#xa;      <text value="Themes agreed"/><mxCell style="whybox" parent="28" vertex="1"><mxGeometry x="1059" y="173" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="749"><text value="![icon 2x ](graduation-cap) **Pro Tip**&#10;&#10;This map makes extensive use of RATSI (which is a variation of RACI)&#10;&#10;**RATSI** stands for :&#10;* **R**esponsible : who makes sure the activity is done (not necessarily doing the activity)&#10;* **A**uthority : who can make decisions&#10;* **T**ask : who does the work&#10;* **S**support : who provides support&#10;* **I**nformed : who is informed when done&#10;&#10;[See this post on our blog to learn more](https://www.getskore.com/roles-responsibilities-in-process-new-use-case/)&#10;&#10;&#10;How to Enable RATSI / RACI plugin ?&#10;&#10;1. Go to **Customize** &gt; **Settings** &#10;2. Enable the &quot;mutli roles and tag&quot;&#10;3. Edit a box and keep working&#10;4. Go to the analytics to extract reports"/><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="28" vertex="1"><mxGeometry x="190" y="396" width="297" height="482" as="geometry"/></mxCell></stickynote><whybox id="33">&#xa;      <text value="3 yr plan approved"/><mxCell style="whybox" parent="5" vertex="1"><mxGeometry x="702" y="224" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="6">&#xa;      <text value="Agreed plans"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="1" vertex="1"><mxGeometry x="360" y="532" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="8">&#xa;      <text value="Create &amp; update content"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="510" y="633" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="454" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="8" source="440" target="451" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="453" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="8" source="441" target="451" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="452" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="8" source="439" target="451" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="447" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="8" source="443" target="440" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="446" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="8" source="444" target="441" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="445" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="8" source="444" target="439" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="439">&#xa;      <text value="Create content based on latest innovations"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="8" vertex="1" collapsed="1"><mxGeometry x="303" y="123" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="470" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="439" source="457" target="458" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="469" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="439" source="456" target="458" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="468" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="439" source="466" target="460" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="467" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="439" source="459" target="466" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="465" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="439" source="460" target="464" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="463" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="439" source="461" target="459" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="462" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="439" source="458" target="461" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="456">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="439" vertex="1"><mxGeometry x="31" y="151" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="458">&#xa;      <text value="Define topics and target audience"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Development Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="439" vertex="1" collapsed="1"><mxGeometry x="176" y="231" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="457">&#xa;      <text value="Latest products and services in development"/><mxCell style="whybox" parent="439" vertex="1"><mxGeometry x="31" y="291" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="461">&#xa;      <text value="Brief"/><mxCell style="whybox" parent="439" vertex="1"><mxGeometry x="376" y="231" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="460">&#xa;      <text value="Create guidelines for use"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="439" vertex="1" collapsed="1"><mxGeometry x="846" y="231" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="464">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="439" vertex="1"><mxGeometry x="1062" y="231" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="459">&#xa;      <text value="Create articles and press releases"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="439" vertex="1" collapsed="1"><mxGeometry x="510" y="231" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="485" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="482" target="484" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="483" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="480" target="482" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><mxCell id="481" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="478" target="480" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="479" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="476" target="478" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="477" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="474" target="476" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="475" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="472" target="474" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="473" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="471" target="472" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="471">&#xa;      <text value="Brief"/><mxCell style="whybox" parent="459" vertex="1"><mxGeometry x="14" y="163" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="472">&#xa;      <text value="Gather all existing information"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="459" vertex="1" collapsed="1"><mxGeometry x="154" y="163" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="474">&#xa;      <text value="Ready to create content"/><mxCell style="whybox" parent="459" vertex="1"><mxGeometry x="364" y="163" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="476">&#xa;      <text value="Develop the copy"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="459" vertex="1" collapsed="1"><mxGeometry x="504" y="163" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="478">&#xa;      <text value="Draft copy"/><mxCell style="whybox" parent="459" vertex="1"><mxGeometry x="714" y="163" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="480">&#xa;      <text value="Prepare any visuals and imagery required"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="459" vertex="1" collapsed="1"><mxGeometry x="834" y="163" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="482">&#xa;      <text value="Copy and visuals"/><mxCell style="whybox" parent="459" vertex="1"><mxGeometry x="1044" y="163" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="493" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="488" target="492" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="489" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="486" target="488" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="487" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="459" source="484" target="486" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="484">&#xa;      <text value="Gain approvals"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Legal">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="459" vertex="1" collapsed="1"><mxGeometry x="40" y="339" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="486">&#xa;      <text value="Approved content"/><mxCell style="whybox" parent="459" vertex="1"><mxGeometry x="250" y="339" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="488">&#xa;      <text value="Translate into target languages where relevant"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Translator">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="459" vertex="1" collapsed="1"><mxGeometry x="390" y="339" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="492">&#xa;      <text value="Content prepared for use"/><mxCell style="whybox" parent="459" vertex="1"><mxGeometry x="604" y="339" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="466">&#xa;      <text value="Content prepared for use"/><mxCell style="whybox" parent="439" vertex="1"><mxGeometry x="720" y="231" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="440">&#xa;      <text value="Create general content on the company, its products and services"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="8" vertex="1" collapsed="1"><mxGeometry x="305" y="358" width="190" height="101" as="geometry"/></mxCell></whatbox><mxCell id="532" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="440" source="565" target="562" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="533" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="440" source="540" target="565" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="534" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="440" source="562" target="564" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="535" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="440" source="563" target="540" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="536" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="440" source="538" target="563" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="540">&#xa;      <text value="Create articles and press releases"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="440" vertex="1" collapsed="1"><mxGeometry x="510" y="251" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="541" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="547" target="561" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="542" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="560" target="545" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="543" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="546" target="547" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="544" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="545" target="546" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="545">&#xa;      <text value="Gain approvals"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="540" vertex="1" collapsed="1"><mxGeometry x="35" y="283" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="546">&#xa;      <text value="Approved content"/><mxCell style="whybox" parent="540" vertex="1"><mxGeometry x="245" y="283" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="547">&#xa;      <text value="Translate into target languages where relevant"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Translator">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="540" vertex="1" collapsed="1"><mxGeometry x="385" y="283" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="561">&#xa;      <text value="Content prepared for use"/><mxCell style="whybox" parent="540" vertex="1"><mxGeometry x="599" y="283" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="548" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="559" target="560" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><mxCell id="549" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="558" target="559" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="550" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="557" target="558" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="551" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="556" target="557" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="552" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="555" target="556" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="553" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="540" source="554" target="555" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="554">&#xa;      <text value="Brief"/><mxCell style="whybox" parent="540" vertex="1"><mxGeometry x="14" y="103" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="555">&#xa;      <text value="Gather all existing information"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="540" vertex="1" collapsed="1"><mxGeometry x="154" y="103" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="556">&#xa;      <text value="Ready to create content"/><mxCell style="whybox" parent="540" vertex="1"><mxGeometry x="364" y="103" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="557">&#xa;      <text value="Develop the copy"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="540" vertex="1" collapsed="1"><mxGeometry x="504" y="103" width="190" height="105" as="geometry"/></mxCell></whatbox><whatbox id="559">&#xa;      <text value="Prepare any visuals and imagery required"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="540" vertex="1" collapsed="1"><mxGeometry x="834" y="103" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="560">&#xa;      <text value="Copy and visuals"/><mxCell style="whybox" parent="540" vertex="1"><mxGeometry x="1044" y="103" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="558">&#xa;      <text value="Draft copy"/><mxCell style="whybox" parent="540" vertex="1"><mxGeometry x="704" y="103" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="565">&#xa;      <text value="Content prepared for use"/><mxCell style="whybox" parent="440" vertex="1"><mxGeometry x="720" y="251" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="531" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="440" source="537" target="538" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="538">&#xa;      <text value="Define topics and target audience"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="440" vertex="1" collapsed="1"><mxGeometry x="162" y="251" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="537">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="440" vertex="1"><mxGeometry x="17" y="251" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="563">&#xa;      <text value="Brief"/><mxCell style="whybox" parent="440" vertex="1"><mxGeometry x="375" y="251" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="562">&#xa;      <text value="Create guidelines for use"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="440" vertex="1" collapsed="1"><mxGeometry x="864" y="251" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="564">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="440" vertex="1"><mxGeometry x="1080" y="251" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="441">&#xa;      <text value="Create educational content to support products and services"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="8" vertex="1" collapsed="1"><mxGeometry x="303" y="240" width="190" height="101" as="geometry"/></mxCell></whatbox><mxCell id="495" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="441" source="501" target="502" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="499" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="441" source="527" target="504" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="500" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="441" source="502" target="527" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="502">&#xa;      <text value="Define topics and target audience"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="441" vertex="1" collapsed="1"><mxGeometry x="186" y="241" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="527">&#xa;      <text value="Brief"/><mxCell style="whybox" parent="441" vertex="1"><mxGeometry x="386" y="241" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="501">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="441" vertex="1"><mxGeometry x="41" y="241" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="496" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="441" source="529" target="526" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="497" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="441" source="504" target="529" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="504">&#xa;      <text value="Create modules and assessments"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="441" vertex="1" collapsed="1"><mxGeometry x="521" y="241" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="505" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="511" target="525" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="506" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="524" target="509" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1154" y="80"/><mxPoint x="1154" y="249"/><mxPoint x="25" y="249"/><mxPoint x="25" y="323"/></Array></mxGeometry></mxCell><mxCell id="507" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="510" target="511" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="508" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="509" target="510" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="509">&#xa;      <text value="Gain approvals"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="504" vertex="1" collapsed="1"><mxGeometry x="35" y="283" width="190" height="135" as="geometry"/></mxCell></whatbox><whybox id="510">&#xa;      <text value="Approved content"/><mxCell style="whybox" parent="504" vertex="1"><mxGeometry x="245" y="283" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="511">&#xa;      <text value="Translate into target languages where relevant"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Translator">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="504" vertex="1" collapsed="1"><mxGeometry x="385" y="283" width="190" height="115" as="geometry"/></mxCell></whatbox><mxCell id="512" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="523" target="524" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><mxCell id="513" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="522" target="523" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="514" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="521" target="522" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="515" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="520" target="521" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="516" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="519" target="520" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="517" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="504" source="518" target="519" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="525">&#xa;      <text value="Content prepared for use"/><mxCell style="whybox" parent="504" vertex="1"><mxGeometry x="599" y="283" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="518">&#xa;      <text value="Brief"/><mxCell style="whybox" parent="504" vertex="1"><mxGeometry x="14" y="40" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="519">&#xa;      <text value="Gather all existing information"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="504" vertex="1" collapsed="1"><mxGeometry x="154" y="40" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="520">&#xa;      <text value="Ready to create content"/><mxCell style="whybox" parent="504" vertex="1"><mxGeometry x="364" y="40" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="521">&#xa;      <text value="Develop the copy"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Trainer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="504" vertex="1" collapsed="1"><mxGeometry x="504" y="40" width="190" height="135" as="geometry"/></mxCell></whatbox><whybox id="522">&#xa;      <text value="Draft copy"/><mxCell style="whybox" parent="504" vertex="1"><mxGeometry x="714" y="40" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="523">&#xa;      <text value="Prepare any visuals and imagery required"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Trainer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="504" vertex="1" collapsed="1"><mxGeometry x="834" y="40" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="524">&#xa;      <text value="Copy and visuals"/><mxCell style="whybox" parent="504" vertex="1"><mxGeometry x="1044" y="40" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="529">&#xa;      <text value="Content prepared for use"/><mxCell style="whybox" parent="441" vertex="1"><mxGeometry x="731" y="241" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="498" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="441" source="526" target="528" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="526">&#xa;      <text value="Create guidelines for use"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="441" vertex="1" collapsed="1"><mxGeometry x="883" y="241" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="528">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="441" vertex="1"><mxGeometry x="1099" y="241" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="444">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="8" vertex="1"><mxGeometry x="111" y="177" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="451">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="8" vertex="1"><mxGeometry x="630" y="304" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="443">&#xa;      <text value="Experts available to provide input"/><mxCell style="whybox" parent="8" vertex="1"><mxGeometry x="111" y="358" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="10">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="741" y="633" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="12">&#xa;      <text value="Build network"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="510" y="286" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="148" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="113" target="143" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="147" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="119" target="143" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="146" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="122" target="142" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="145" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="116" target="142" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="144" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="110" target="142" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="139" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="125" target="113" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="138" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="125" target="119" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="137" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="125" target="122" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="136" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="125" target="116" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="135" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="12" source="125" target="110" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="110">&#xa;      <text value="Create and manage a list of external speakers"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="12" vertex="1" collapsed="1"><mxGeometry x="240" y="90" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="175" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="172" target="174" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="173" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="170" target="172" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="171" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="168" target="170" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="169" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="166" target="168" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="167" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="162" target="166" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="163" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="160" target="162" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="162">&#xa;      <text value="Onboard speakers"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="110" vertex="1" collapsed="1"><mxGeometry x="38" y="228" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="249" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="162" source="242" target="246" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="248" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="162" source="238" target="246" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="247" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="162" source="234" target="246" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="243" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="162" source="233" target="242" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="239" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="162" source="233" target="238" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="235" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="162" source="233" target="234" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="233">&#xa;      <text value="Speakers ready to be used on engagements"/><mxCell style="whybox" parent="162" vertex="1"><mxGeometry x="313" y="72" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="234">&#xa;      <text value="Develop and implement onboarding program"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Trainer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="162" vertex="1" collapsed="1"><mxGeometry x="453" y="72" width="190" height="175" as="geometry"/></mxCell></whatbox><whybox id="246">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="162" vertex="1"><mxGeometry x="680" y="72" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="238">&#xa;      <text value="Develop and implement a program of updates on changes to the company and services"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Trainer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="162" vertex="1" collapsed="1"><mxGeometry x="453" y="299" width="190" height="207" as="geometry"/></mxCell></whatbox><whatbox id="242">&#xa;      <text value="Review compensation packages &amp; expectations annually"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Finance">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="162" vertex="1" collapsed="1"><mxGeometry x="453" y="528.5" width="190" height="161" as="geometry"/></mxCell></whatbox><whybox id="166">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="110" vertex="1"><mxGeometry x="261" y="228" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="168">&#xa;      <text value="Add and update speaker list"/>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="110" vertex="1" collapsed="1"><mxGeometry x="401" y="228" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="170">&#xa;      <text value="List available to all internal stakeholders"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="110" vertex="1"><mxGeometry x="611" y="228" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="172">&#xa;      <text value="Manage and schedule speakers"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="110" vertex="1" collapsed="1"><mxGeometry x="751" y="228" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="264" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="261" target="263" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1152" y="139"/><mxPoint x="1152" y="362"/><mxPoint x="20" y="362"/><mxPoint x="20" y="423"/></Array></mxGeometry></mxCell><mxCell id="260" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="257" target="259" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="258" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="255" target="257" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="256" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="253" target="255" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="254" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="251" target="253" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="252" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="250" target="251" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="250">&#xa;      <text value="List available to all internal stakeholders"/><mxCell style="whybox" parent="172" vertex="1"><mxGeometry x="10" y="99" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="251">&#xa;      <text value="Request a speaker"/>&#xa;      <resource value="Requestor">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <attachment type="text" value="Complete attached request form (to be completed)"/><mxCell style="whatbox" parent="172" vertex="1" collapsed="1"><mxGeometry x="150" y="99" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="253">&#xa;      <text value="Completed request form"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="172" vertex="1"><mxGeometry x="360" y="99" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="255">&#xa;      <text value="Consolidate and prioritize requests"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="172" vertex="1" collapsed="1"><mxGeometry x="500" y="99" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="257">&#xa;      <text value="Updated calendar"/><mxCell style="whybox" parent="172" vertex="1"><mxGeometry x="710" y="99" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="262" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="259" target="261" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="259">&#xa;      <text value="Confirm speaker availability"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="172" vertex="1" collapsed="1"><mxGeometry x="832" y="99" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="261">&#xa;      <text value="Speaker returned booking agreement"/><mxCell style="whybox" parent="172" vertex="1"><mxGeometry x="1042" y="99" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="276" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="267" target="275" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="274" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="271" target="273" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="272" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="265" target="271" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="268" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="265" target="267" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="266" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="172" source="263" target="265" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="263">&#xa;      <text value="Provide speaker details to the requestor"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="172" vertex="1" collapsed="1"><mxGeometry x="30" y="383" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="265">&#xa;      <text value="Event completed"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="172" vertex="1"><mxGeometry x="240" y="383" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="267">&#xa;      <text value="Conduct post evaluation of engagement"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="172" vertex="1" collapsed="1"><mxGeometry x="380" y="383" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="275">&#xa;      <text value="Feedback from speaking engagements"/><mxCell style="whybox" parent="172" vertex="1"><mxGeometry x="590" y="383" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="271">&#xa;      <text value="Pay speaker"/>&#xa;      <resource value="Finance">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="172" vertex="1" collapsed="1"><mxGeometry x="380" y="567" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="273">&#xa;      <text value="Transaction complete"/><mxCell style="whybox" parent="172" vertex="1"><mxGeometry x="590" y="567" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="174">&#xa;      <text value="Feedback from speaking engagements"/><mxCell style="whybox" parent="110" vertex="1"><mxGeometry x="961" y="228" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="161" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="158" target="160" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="159" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="156" target="158" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="157" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="154" target="156" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="155" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="152" target="154" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="153" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="150" target="152" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="151" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="110" source="149" target="150" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="150">&#xa;      <text value="Identify influential speakers"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="110" vertex="1" collapsed="1"><mxGeometry x="135" y="102" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="204" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="176" target="186" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="203" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="176" target="183" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="202" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="176" target="180" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="201" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="176" target="177" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="198" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="177" target="181" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="182" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="180" target="181" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="177">&#xa;      <text value="Gather recommendations internally"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Line Manager">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="150" vertex="1" collapsed="1"><mxGeometry x="347" y="110" width="190" height="175" as="geometry"/></mxCell></whatbox><whybox id="176">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="150" vertex="1"><mxGeometry x="96" y="318" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="200" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="186" target="181" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="199" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="183" target="181" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="180">&#xa;      <text value="Monitor conferences, and publications"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="150" vertex="1" collapsed="1"><mxGeometry x="347" y="305" width="190" height="175" as="geometry"/></mxCell></whatbox><mxCell id="197" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="181" target="192" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="196" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="150" source="192" target="195" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="195">&#xa;      <text value="List of potential speakers based on expertise"/><mxCell style="whybox" parent="150" vertex="1"><mxGeometry x="971" y="378" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="181">&#xa;      <text value="List of potential candidates"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="150" vertex="1"><mxGeometry x="626" y="378" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="192">&#xa;      <text value="Compile list"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="150" vertex="1" collapsed="1"><mxGeometry x="756" y="378" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="183">&#xa;      <text value="Monitor social media"/>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatbox" parent="150" vertex="1" collapsed="1"><mxGeometry x="347" y="501" width="190" height="105" as="geometry"/></mxCell></whatbox><whatbox id="186">&#xa;      <text value="Gather input from customers"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="VP Sales">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="150" vertex="1" collapsed="1"><mxGeometry x="347" y="644" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="152">&#xa;      <text value="List of potential speakers based on expertise"/><mxCell style="whybox" parent="110" vertex="1"><mxGeometry x="345" y="102" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="154">&#xa;      <text value="Map influence of speakers"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="110" vertex="1" collapsed="1"><mxGeometry x="485" y="102" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="215" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="154" source="211" target="206" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="212" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="154" source="209" target="211" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="210" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="154" source="207" target="209" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="208" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="154" source="205" target="207" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="205">&#xa;      <text value="List of potential speakers based on expertise"/><mxCell style="whybox" parent="154" vertex="1"><mxGeometry x="76" y="191" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="207">&#xa;      <text value="Populate influencer mapping tool"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="154" vertex="1" collapsed="1"><mxGeometry x="216" y="191" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="209">&#xa;      <text value="Picture of relationships"/><mxCell style="whybox" parent="154" vertex="1"><mxGeometry x="426" y="191" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="211">&#xa;      <text value="Create list of potential speakers"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="154" vertex="1" collapsed="1"><mxGeometry x="566" y="191" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="206">&#xa;      <text value="Shortlist of speakers"/><mxCell style="whybox" parent="154" vertex="1"><mxGeometry x="794" y="191" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="156">&#xa;      <text value="Shortlist of speakers"/><mxCell style="whybox" parent="110" vertex="1"><mxGeometry x="695" y="102" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="158">&#xa;      <text value="Recruit speakers"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="110" vertex="1" collapsed="1"><mxGeometry x="835" y="102" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="226" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="158" source="221" target="225" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="222" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="158" source="219" target="221" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="220" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="158" source="217" target="219" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="218" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="158" source="216" target="217" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="216">&#xa;      <text value="Shortlist of speakers"/><mxCell style="whybox" parent="158" vertex="1"><mxGeometry x="61" y="212" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="217">&#xa;      <text value="Initiate contact with potential candidate"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="VP Sales">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="158" vertex="1" collapsed="1"><mxGeometry x="201" y="212" width="190" height="235" as="geometry"/></mxCell></whatbox><whybox id="219">&#xa;      <text value="Agreement to move forward"/><mxCell style="whybox" parent="158" vertex="1"><mxGeometry x="411" y="212" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="221">&#xa;      <text value="Secure agreement on expectations and compensation"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Finance">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Legal">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="158" vertex="1" collapsed="1"><mxGeometry x="551" y="212" width="190" height="161" as="geometry"/></mxCell></whatbox><whybox id="225">&#xa;      <text value="Speakers ready to be used on engagements"/><mxCell style="whybox" parent="158" vertex="1"><mxGeometry x="765" y="212" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="160">&#xa;      <text value="Speakers ready to be used on engagements"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="110" vertex="1"><mxGeometry x="1045" y="102" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="149">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="110" vertex="1"><mxGeometry x="15" y="102" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="116">&#xa;      <text value="Create and manage a list of internal speakers"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="12" vertex="1" collapsed="1"><mxGeometry x="240" y="200" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="297" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="116" source="305" target="303" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="839" y="105"/><mxPoint x="839" y="232"/><mxPoint x="138" y="232"/><mxPoint x="138" y="289"/></Array></mxGeometry></mxCell><mxCell id="298" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="116" source="302" target="305" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="300" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="116" source="301" target="302" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="301">&#xa;      <text value="List of potential internal speakers"/><mxCell style="whybox" parent="116" vertex="1"><mxGeometry x="368" y="65" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="302">&#xa;      <text value="Screen and assess potential candidates"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="116" vertex="1" collapsed="1"><mxGeometry x="508" y="65" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="340" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="331" target="339" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="328" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="325" target="327" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1145" y="112"/><mxPoint x="1145" y="241"/><mxPoint x="20" y="241"/><mxPoint x="20" y="301"/></Array></mxGeometry></mxCell><mxCell id="324" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="321" target="323" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="320" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="317" target="319" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="318" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="315" target="317" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="316" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="314" target="315" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="314">&#xa;      <text value="List of potential internal speakers"/><mxCell style="whybox" parent="302" vertex="1"><mxGeometry x="11" y="72" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="315">&#xa;      <text value="Confirm interest and expectations"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Speaker">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Line Manager">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="302" vertex="1" collapsed="1"><mxGeometry x="151" y="72" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="317">&#xa;      <text value="Desire to continue"/><mxCell style="whybox" parent="302" vertex="1"><mxGeometry x="361" y="72" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="332" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="329" target="331" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="330" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="327" target="329" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="326" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="323" target="325" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="323">&#xa;      <text value="Create agreement on expectations and align with Line Manager"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Line Manager">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Speaker">&#xa;        <tag value="S"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="302" vertex="1" collapsed="1"><mxGeometry x="825" y="72" width="190" height="161" as="geometry"/></mxCell></whatbox><whybox id="325">&#xa;      <text value="Ready to proceed with training"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="302" vertex="1"><mxGeometry x="1035" y="72" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="322" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="302" source="319" target="321" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="319">&#xa;      <text value="Assess qualifications and perform skills gap analysis"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Training Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="302" vertex="1" collapsed="1"><mxGeometry x="491" y="72" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="321">&#xa;      <text value="Required development understood"/><mxCell style="whybox" parent="302" vertex="1"><mxGeometry x="701" y="72" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="327">&#xa;      <text value="Perform skills training"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Speaker">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="302" vertex="1" collapsed="1"><mxGeometry x="30" y="261" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="329">&#xa;      <text value="Gaps closed"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="302" vertex="1"><mxGeometry x="240" y="261" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="331">&#xa;      <text value="Create speaker profile"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="302" vertex="1" collapsed="1"><mxGeometry x="380" y="261" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="339">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="302" vertex="1"><mxGeometry x="596" y="261" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="305">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="116" vertex="1"><mxGeometry x="729" y="65" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="306" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="116" source="308" target="301" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="308">&#xa;      <text value="Request internal nominees"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Line Manager">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="116" vertex="1" collapsed="1"><mxGeometry x="148" y="65" width="190" height="105" as="geometry"/></mxCell></whatbox><mxCell id="309" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="116" source="310" target="308" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="310">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="116" vertex="1"><mxGeometry x="10" y="65" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="307" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="116" source="304" target="312" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="311" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="116" source="312" target="313" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><whatbox id="312">&#xa;      <text value="Perform ongoing skills development"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatbox" parent="116" vertex="1" collapsed="1"><mxGeometry x="508" y="249" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="313">&#xa;      <text value="Updated speaker profile"/><mxCell style="whybox" parent="116" vertex="1"><mxGeometry x="729" y="249" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="299" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="116" source="303" target="304" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="303">&#xa;      <text value="Manage speaker calendar"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="116" vertex="1" collapsed="1"><mxGeometry x="148" y="249" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="369" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="366" target="364" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="368" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="367" target="353" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="342" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="363" target="366" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="345" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="359" target="363" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="346" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="362" target="358" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="347" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="356" target="361" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="348" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="355" target="356" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="349" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="354" target="355" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="350" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="353" target="354" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="353">&#xa;      <text value="Request a speaker"/>&#xa;      <resource value="Requestor">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <attachment type="text" value="Requestor completes request form and sends to Events Manager"/>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="303" vertex="1" collapsed="1"><mxGeometry x="151" y="109" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="354">&#xa;      <text value="Completed request form"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="303" vertex="1"><mxGeometry x="361" y="109" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="355">&#xa;      <text value="Consolidate and prioritize requests"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="303" vertex="1" collapsed="1"><mxGeometry x="501" y="109" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="356">&#xa;      <text value="Updated calendar"/><mxCell style="whybox" parent="303" vertex="1"><mxGeometry x="711" y="109" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="357" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="358" target="359" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="360" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="361" target="362" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="361">&#xa;      <text value="Confirm speaker availability"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Speaker">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="303" vertex="1" collapsed="1"><mxGeometry x="833" y="109" width="190" height="135" as="geometry"/></mxCell></whatbox><whybox id="362">&#xa;      <text value="Speaker returned booking agreement"/><mxCell style="whybox" parent="303" vertex="1"><mxGeometry x="1043" y="109" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="367">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="303" vertex="1"><mxGeometry x="18" y="109" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="343" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="303" source="364" target="365" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="358">&#xa;      <text value="Provide speaker details to the requestor"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="303" vertex="1" collapsed="1"><mxGeometry x="31" y="321" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="359">&#xa;      <text value="Event completed"/><mxCell style="whybox" parent="303" vertex="1"><mxGeometry x="241" y="321" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="363">&#xa;      <text value="Conduct post evaluation of engagement"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="303" vertex="1" collapsed="1"><mxGeometry x="381" y="321" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="366">&#xa;      <text value="Feedback from speaking engagements"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="303" vertex="1"><mxGeometry x="591" y="321" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="364">&#xa;      <text value="Provide feedback to Line Manager"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Line Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="303" vertex="1" collapsed="1"><mxGeometry x="724" y="321" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="365">&#xa;      <text value="Transaction complete"/><mxCell style="whybox" parent="303" vertex="1"><mxGeometry x="934" y="321" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="304">&#xa;      <text value="Updated calendar"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="116" vertex="1"><mxGeometry x="358" y="249" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="122">&#xa;      <text value="Develop an independent network of social media commentators"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="12" vertex="1" collapsed="1"><mxGeometry x="240" y="309" width="190" height="101" as="geometry"/></mxCell></whatbox><mxCell id="392" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="387" target="391" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="388" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="385" target="387" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="384" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="381" target="383" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1139" y="145"/><mxPoint x="1139" y="362"/><mxPoint x="28" y="362"/><mxPoint x="28" y="447"/></Array></mxGeometry></mxCell><mxCell id="380" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="377" target="379" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="378" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="375" target="377" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="376" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="373" target="375" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="374" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="371" target="373" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="372" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="370" target="371" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="371">&#xa;      <text value="Define selection criteria for individuals we want to target"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="122" vertex="1" collapsed="1"><mxGeometry x="138" y="105" width="190" height="205" as="geometry"/></mxCell></whatbox><whybox id="373">&#xa;      <text value="Profile available"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="122" vertex="1"><mxGeometry x="348" y="105" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="375">&#xa;      <text value="Identify and prioritize individuals that fit criteria"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="122" vertex="1" collapsed="1"><mxGeometry x="488" y="105" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="377">&#xa;      <text value="List of targets"/><mxCell style="whybox" parent="122" vertex="1"><mxGeometry x="698" y="105" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="370">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="122" vertex="1"><mxGeometry x="15" y="105" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="382" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="379" target="381" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><whatbox id="379">&#xa;      <text value="Initiate relationship"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="122" vertex="1" collapsed="1"><mxGeometry x="819" y="105" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="381">&#xa;      <text value="Agreement to continue"/><mxCell style="whybox" parent="122" vertex="1"><mxGeometry x="1029" y="105" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="386" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="122" source="383" target="385" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="383">&#xa;      <text value="Onboard the commentator"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="122" vertex="1" collapsed="1"><mxGeometry x="38" y="407" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="400" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="383" source="396" target="394" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="399" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="383" source="395" target="394" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="398" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="383" source="393" target="396" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="397" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="383" source="393" target="395" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="393">&#xa;      <text value="Agreement to continue"/><mxCell style="whybox" parent="383" vertex="1"><mxGeometry x="110" y="164" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="395">&#xa;      <text value="Develop and implement onboarding program"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="383" vertex="1" collapsed="1"><mxGeometry x="394" y="113" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="394">&#xa;      <text value="Commentator ready to disseminate our messages"/><mxCell style="whybox" parent="383" vertex="1"><mxGeometry x="869" y="164" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="396">&#xa;      <text value="Develop and implement an ongoing program of company updates"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="S"/>&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatbox" parent="383" vertex="1" collapsed="1"><mxGeometry x="394" y="306" width="190" height="101" as="geometry"/></mxCell></whatbox><whybox id="385">&#xa;      <text value="Commentator ready to disseminate our messages"/><mxCell style="whybox" parent="122" vertex="1"><mxGeometry x="248" y="407" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="387">&#xa;      <text value="Evaluate the relationship on an ongoing basis"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="122" vertex="1" collapsed="1"><mxGeometry x="388" y="407" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="391">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="122" vertex="1"><mxGeometry x="605" y="407" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="119">&#xa;      <text value="Develop strategic alliances with non-commercial organizations"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="12" vertex="1" collapsed="1"><mxGeometry x="240" y="434" width="190" height="101" as="geometry"/></mxCell></whatbox><mxCell id="415" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="412" target="414" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1149" y="178"/><mxPoint x="1149" y="347"/><mxPoint x="17" y="347"/><mxPoint x="17" y="418"/></Array></mxGeometry></mxCell><mxCell id="411" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="408" target="410" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="409" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="406" target="408" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="407" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="404" target="406" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="405" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="402" target="404" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="403" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="401" target="402" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="401">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="119" vertex="1"><mxGeometry x="4" y="138" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="402">&#xa;      <text value="Define selection criteria for orgs to partner with"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="119" vertex="1" collapsed="1"><mxGeometry x="144" y="138" width="190" height="175" as="geometry"/></mxCell></whatbox><whybox id="404">&#xa;      <text value="Selection criteria agreed"/><mxCell style="whybox" parent="119" vertex="1"><mxGeometry x="354" y="138" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="406">&#xa;      <text value="Identify and prioritize orgs according to criteria"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="119" vertex="1" collapsed="1"><mxGeometry x="494" y="138" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="408">&#xa;      <text value="List of target orgs"/><mxCell style="whybox" parent="119" vertex="1"><mxGeometry x="704" y="138" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="413" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="410" target="412" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="410">&#xa;      <text value="Initiate relationships"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="119" vertex="1" collapsed="1"><mxGeometry x="829" y="138" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="412">&#xa;      <text value="Agreement to enter alliance"/><mxCell style="whybox" parent="119" vertex="1"><mxGeometry x="1039" y="138" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="427" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="424" target="426" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="425" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="416" target="424" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="423" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="418" target="422" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="419" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="416" target="418" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="417" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="119" source="414" target="416" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="414">&#xa;      <text value="Negotiate and agree contracts"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Legal">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="119" vertex="1" collapsed="1"><mxGeometry x="27" y="378" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="416">&#xa;      <text value="Alliance established"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="119" vertex="1"><mxGeometry x="237" y="378" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="418">&#xa;      <text value="Assess and maintain relationships"/>&#xa;      <resource value="External Relations Director">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="119" vertex="1" collapsed="1"><mxGeometry x="377" y="378" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="422">&#xa;      <text value="Experts available to provide input"/><mxCell style="whybox" parent="119" vertex="1"><mxGeometry x="604" y="378" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="424">&#xa;      <text value="Prepare market communications on new alliance"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="119" vertex="1" collapsed="1"><mxGeometry x="377" y="498" width="190" height="161" as="geometry"/></mxCell></whatbox><whybox id="426">&#xa;      <text value="Market awareness"/><mxCell style="whybox" parent="119" vertex="1"><mxGeometry x="587" y="498" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="113">&#xa;      <text value="Create a network of internal experts"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="12" vertex="1" collapsed="1"><mxGeometry x="240" y="550" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="438" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="113" source="433" target="437" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="434" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="113" source="431" target="433" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="432" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="113" source="429" target="431" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="430" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="113" source="428" target="429" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="428">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="113" vertex="1"><mxGeometry x="56" y="158" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="429">&#xa;      <text value="Define the needs for the business"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <attachment type="text" value="Where do we have gaps? What areas do we have expertise in? What are the most important areas for us to focus on right now?"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="113" vertex="1" collapsed="1"><mxGeometry x="196" y="158" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="431">&#xa;      <text value="Requirements for experts"/><mxCell style="whybox" parent="113" vertex="1"><mxGeometry x="406" y="158" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="433">&#xa;      <text value="Identify and engage internal experts"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Employee">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Line Manager">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="113" vertex="1" collapsed="1"><mxGeometry x="546" y="158" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="437">&#xa;      <text value="Experts available to provide input"/><mxCell style="whybox" parent="113" vertex="1"><mxGeometry x="776" y="158" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="125">&#xa;      <text value="Agreed plans"/><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="49" y="309" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="143">&#xa;      <text value="Experts available to provide input"/><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="549" y="497" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="142">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="549" y="200" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="14">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="1" vertex="1"><mxGeometry x="741" y="289" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="16">&#xa;      <text value="Communicate content"/>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="901" y="532" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="722" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="609" target="604" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="721" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="603" target="609" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="720" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="602" target="609" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="700" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="608" target="604" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="699" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="603" target="608" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="698" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="602" target="608" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="677" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="607" target="604" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="673" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="603" target="607" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="672" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="602" target="607" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="644" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="606" target="604" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="643" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="603" target="606" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="642" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="602" target="606" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="612" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="605" target="604" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="611" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="603" target="605" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="610" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="602" target="605" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="605">&#xa;      <text value="Attend industry conferences as exhibitor"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="16" vertex="1" collapsed="1"><mxGeometry x="273" y="87" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="637" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="634" target="627" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1150" y="148"/><mxPoint x="1150" y="324"/><mxPoint x="142" y="324"/><mxPoint x="142" y="423"/></Array></mxGeometry></mxCell><mxCell id="636" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="632" target="619" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="635" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="619" target="634" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="630" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="614" target="615" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="629" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="613" target="615" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="613">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="605" vertex="1"><mxGeometry x="9" y="108" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="614">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="605" vertex="1"><mxGeometry x="9" y="243" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="619">&#xa;      <text value="Prepare exhibit and materials"/>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="605" vertex="1" collapsed="1"><mxGeometry x="830" y="108" width="190" height="175" as="geometry"/></mxCell></whatbox><whybox id="634">&#xa;      <text value="All materials and staff at location"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="605" vertex="1"><mxGeometry x="1040" y="108" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="633" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="623" target="632" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="631" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="617" target="623" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="618" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="615" target="617" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="615">&#xa;      <text value="Identify conferences and themes"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="605" vertex="1" collapsed="1"><mxGeometry x="152" y="108" width="190" height="175" as="geometry"/></mxCell></whatbox><whybox id="617">&#xa;      <text value="Short list of conferences we want to attend"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="605" vertex="1"><mxGeometry x="362" y="108" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="623">&#xa;      <text value="Manage event logistics"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatbox" parent="605" vertex="1" collapsed="1"><mxGeometry x="490" y="108" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="632">&#xa;      <text value="Where, when and who known"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="605" vertex="1"><mxGeometry x="700" y="108" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="641" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="628" target="616" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="640" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="638" target="628" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="639" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="605" source="627" target="638" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="627">&#xa;      <text value="Attend event"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatbox" parent="605" vertex="1" collapsed="1"><mxGeometry x="152" y="383" width="190" height="105" as="geometry"/></mxCell></whatbox><whatbox id="628">&#xa;      <text value="Evaluate the ROI of attendance"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="605" vertex="1" collapsed="1"><mxGeometry x="490" y="383" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="616">&#xa;      <text value="Better awareness, recognition and reputation"/><mxCell style="whybox" parent="605" vertex="1"><mxGeometry x="710" y="383" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="638">&#xa;      <text value="New leads and/or customers"/><mxCell style="whybox" parent="605" vertex="1"><mxGeometry x="362" y="383" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="606">&#xa;      <text value="Manage web presence"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="16" vertex="1" collapsed="1"><mxGeometry x="273" y="193" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="667" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="645" target="649" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="666" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="646" target="649" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="665" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="663" target="651" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1176" y="88"/><mxPoint x="1176" y="268"/><mxPoint x="146" y="268"/><mxPoint x="146" y="340"/></Array></mxGeometry></mxCell><mxCell id="664" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="650" target="663" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="662" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="656" target="650" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="661" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="659" target="648" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="645">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="17" y="48" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="660" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="649" target="659" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="649">&#xa;      <text value="Define scope and objectives for web presence"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <attachment type="text" value="What are we trying to achieve, who are our target audiences, how will we measure success?"/>&#xa;      <resource value="Web Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="606" vertex="1" collapsed="1"><mxGeometry x="156" y="48" width="190" height="175" as="geometry"/></mxCell></whatbox><whybox id="659">&#xa;      <text value="Web strategy"/><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="366" y="48" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="657" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="648" target="656" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"/></mxGeometry></mxCell><whybox id="646">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="17" y="199" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="648">&#xa;      <text value="Assess online platforms for suitability"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <attachment type="text" value="What\\\'s best for us? Facebook, own website, Twitter, Pintrest etc"/>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="606" vertex="1" collapsed="1"><mxGeometry x="497" y="48" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="656">&#xa;      <text value="List of platforms we want to leverage"/><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="707" y="48" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="650">&#xa;      <text value="Assess capabilities (internal vs external)"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <attachment type="text" value="What can we do internally? Do we need an agency to support us? What\\\'s our budget for doing so?"/>&#xa;      <resource value="Web Designer">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="S"/>&#xa;      </resource><mxCell style="whatbox" parent="606" vertex="1" collapsed="1"><mxGeometry x="837" y="48" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="663">&#xa;      <text value="Resources identified"/><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="1046" y="48" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="671" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="653" target="647" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="670" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="668" target="653" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="669" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="606" source="651" target="668" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="651">&#xa;      <text value="Prepare and publish content for use on specific platforms"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Web Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Web Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="606" vertex="1" collapsed="1"><mxGeometry x="156" y="300" width="190" height="235" as="geometry"/></mxCell></whatbox><whybox id="668">&#xa;      <text value="Content publicly avaiable"/><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="366" y="300" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="653">&#xa;      <text value="Measure success of each platform"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Web Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Social Media Specialist">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="606" vertex="1" collapsed="1"><mxGeometry x="506" y="300" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="647">&#xa;      <text value="Better awareness, recognition and reputation"/><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="735" y="300" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="607">&#xa;      <text value="Manage presence in print media"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="16" vertex="1" collapsed="1"><mxGeometry x="273" y="297" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="696" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="694" target="685" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1190" y="80"/><mxPoint x="1190" y="236"/><mxPoint x="206" y="236"/><mxPoint x="206" y="307"/></Array></mxGeometry></mxCell><mxCell id="690" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="688" target="682" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="689" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="681" target="688" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="687" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="678" target="681" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="686" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="679" target="681" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="678">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="607" vertex="1"><mxGeometry x="9" y="40" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="679">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="607" vertex="1"><mxGeometry x="11" y="162" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="681">&#xa;      <text value="Define scope and objectives for print media campaigns"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Director">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="607" vertex="1" collapsed="1"><mxGeometry x="165" y="40" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="688">&#xa;      <text value="Criteria for print media"/><mxCell style="whybox" parent="607" vertex="1"><mxGeometry x="375" y="40" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="697" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="685" target="680" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="685">&#xa;      <text value="Measure success of print media campaigns"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="607" vertex="1" collapsed="1"><mxGeometry x="216" y="267" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="680">&#xa;      <text value="Better awareness, recognition and reputation"/><mxCell style="whybox" parent="607" vertex="1"><mxGeometry x="438" y="267" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="693" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="691" target="683" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="692" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="682" target="691" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="682">&#xa;      <text value="Identify suitable print media"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="607" vertex="1" collapsed="1"><mxGeometry x="509" y="40" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="691">&#xa;      <text value="List of publications to target"/><mxCell style="whybox" parent="607" vertex="1"><mxGeometry x="719" y="40" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="695" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="607" source="683" target="694" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="683">&#xa;      <text value="Prepare and diseminate content for print media"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Copywriter">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Creative Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="607" vertex="1" collapsed="1"><mxGeometry x="850" y="40" width="190" height="175" as="geometry"/></mxCell></whatbox><whybox id="694">&#xa;      <text value="Articles published in print media"/><mxCell style="whybox" parent="607" vertex="1"><mxGeometry x="1060" y="40" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="608">&#xa;      <text value="Create and manage user community"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="16" vertex="1" collapsed="1"><mxGeometry x="273" y="409" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="718" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="716" target="707" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="715" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="713" target="706" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="714" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="705" target="713" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="712" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="710" target="705" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="711" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="704" target="710" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="709" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="702" target="704" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="708" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="701" target="704" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="701">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="608" vertex="1"><mxGeometry x="9" y="30" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="704">&#xa;      <text value="Define scope and objectives for user community"/>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Web Designer">&#xa;        <tag value="S"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatbox" parent="608" vertex="1" collapsed="1"><mxGeometry x="160" y="30" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="702">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="608" vertex="1"><mxGeometry x="9" y="189" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="710">&#xa;      <text value="Direction and audience agreed"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="608" vertex="1"><mxGeometry x="370" y="30" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="719" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="707" target="703" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="707">&#xa;      <text value="Measure success of community against objectives"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Web Designer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="608" vertex="1" collapsed="1"><mxGeometry x="221" y="305" width="190" height="131" as="geometry"/></mxCell></whatbox><whybox id="703">&#xa;      <text value="Better awareness, recognition and reputation"/><mxCell style="whybox" parent="608" vertex="1"><mxGeometry x="441" y="305" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="705">&#xa;      <text value="Identify and manage platform for online community"/>&#xa;      <resource value="Web Designer">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <attachment type="text" value="Identify technology, Invite users, Oversee discussions, Set ground rules"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="608" vertex="1" collapsed="1"><mxGeometry x="510" y="30" width="190" height="161" as="geometry"/></mxCell></whatbox><whybox id="713">&#xa;      <text value="Online community available to users"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="608" vertex="1"><mxGeometry x="720" y="30" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="717" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="608" source="706" target="716" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="706">&#xa;      <text value="Schedule and manage community events"/>&#xa;      <resource value="Events Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <attachment type="text" value="Physical events"/>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="R"/>&#xa;      </resource>&#xa;      <resource value="Marketing Assistant">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="608" vertex="1" collapsed="1"><mxGeometry x="864" y="30" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="716">&#xa;      <text value="Better understanding of community\\\'s issues"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="608" vertex="1"><mxGeometry x="1074" y="30" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="609">&#xa;      <text value="Deliver external product and services training"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="16" vertex="1" collapsed="1"><mxGeometry x="273" y="524" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="740" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="738" target="729" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="739" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="728" target="738" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="737" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="735" target="728" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="736" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="727" target="735" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="734" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="732" target="727" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="733" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="726" target="732" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="731" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="724" target="726" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="730" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="723" target="726" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="726">&#xa;      <text value="Define objectives for training services"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Product Marketing Manager">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="609" vertex="1" collapsed="1"><mxGeometry x="161" y="45" width="190" height="145" as="geometry"/></mxCell></whatbox><whybox id="732">&#xa;      <text value="Program requirements understood"/><mxCell style="whybox" parent="609" vertex="1"><mxGeometry x="371" y="45" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="727">&#xa;      <text value="Develop educational programs"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="609" vertex="1" collapsed="1"><mxGeometry x="493" y="45" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="735">&#xa;      <text value="Programs available for use"/><mxCell style="whybox" parent="609" vertex="1"><mxGeometry x="703" y="45" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="728">&#xa;      <text value="Deliver educational programs"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="Trainer">&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="609" vertex="1" collapsed="1"><mxGeometry x="826" y="45" width="190" height="115" as="geometry"/></mxCell></whatbox><whybox id="738">&#xa;      <text value="Improved technical competence in market place"/><mxCell style="whybox" parent="609" vertex="1"><mxGeometry x="1036" y="45" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="723">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="609" vertex="1"><mxGeometry x="12" y="45" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="724">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="609" vertex="1"><mxGeometry x="12" y="155" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="741" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="609" source="729" target="725" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="729">&#xa;      <text value="Measure performance of educational programs against objectives"/>&#xa;      <resource value="Training Manager">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource><mxCell style="whatbox" parent="609" vertex="1" collapsed="1"><mxGeometry x="210" y="298" width="190" height="101" as="geometry"/></mxCell></whatbox><whybox id="725">&#xa;      <text value="Better awareness, recognition and reputation"/><mxCell style="whybox" parent="609" vertex="1"><mxGeometry x="433" y="298" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="603">&#xa;      <text value="Approved content available for distribution"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="37" y="193" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="602">&#xa;      <text value="Network available to disseminate content"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="37" y="87" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="604">&#xa;      <text value="Better awareness, recognition and reputation"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="618" y="87" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="18">&#xa;      <text value="Better awareness, recognition and reputation"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1100" y="532" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="24">&#xa;      <text value="Experts available to provide input"/><mxCell style="whybox;strokeColor=purple;handover=1;" parent="1" vertex="1"><mxGeometry x="545" y="482" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="27">&#xa;      <text value="##Ambition&#10;To create a global organization, and associated network, that enables the business to disseminate key information about its products and services.&#10;&#10;##Objectives&#10;* To achieve clear recognition of the company\\\'s contribution to advancing the industry&#10;* Build and protect the company\\\'s reputation&#10;* Increase the effectivenes of the company\\\'s communication network&#10;* Influence policies and standards within the industry"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="31" y="15" width="1108" height="252" as="geometry"/></mxCell></stickynote></root></mxGraphModel>';});


define('text!demoContent/fullFeaturesTests.skore',[],function () { return '<mxGraphModel dx="50" dy="50" grid="1" gridSize="10" guides="1" tooltips="0" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="826" pageHeight="1169" background="lightyellow"><root><mxCell id="0"><extension value="mrt" type="RATSI" as="tagsInUse">&#xa;        <tag value="R" style="background-color:rgba(255, 165, 0, 0.5)" name="Responsible"/>&#xa;        <tag value="A" style="background-color:rgba(255, 0, 0, 0.5)" name="Authority"/>&#xa;        <tag value="T" style="background-color:rgba(245, 245, 220, 0.5)" name="Task"/>&#xa;        <tag value="S" name="Support"/>&#xa;        <tag value="I" name="Informed"/>&#xa;      </extension><stylesheets as="stylesheets"><stylesheet value="default"/></stylesheets><roles as="roles"><role value="user 1"/><role value="user 2 should have RT"/><role value="I have a A"/><role value="Who does it?"/><role value="user 3"/><role value="user 4"/><role value="r1"/><role value="r2"/><role value="text"/><role value="Colin"/><role value="Craig"/><role value="Cathy"/><role value="DD"/><role value="This is &lt; a **res** with ![icon red](heart) and ## header"/><role value="another **one**"/><role value="aaaa I have a comment !!"><comment value="Here is a comment for **me**&#xa;&#xa;## hello&#xa;&#xa;---&#xa;&#xa;* I agree&#xa;* about &#xa;* this !"/></role><role value="special caracters \' &quot; / \\ &lt; &gt; &amp;"><comment value="sdfsdf sf special caracters \' &quot; / \\ &lt; &gt; &amp; "/></role></roles><extension value="handoverhighlight" color="purple" as="handoverhighlight"/></mxCell><title id="1">&#xa;      <text value="This is the title (id=1)"/><mxCell style="title" parent="0"><mxRectangle x="435" y="1036" width="1150" height="526" as="visibleRect"/></mxCell></title><mxCell id="99" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="3" target="97" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="98" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="97" target="40" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="96" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="60" target="95" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="2">&#xa;      <text value="first box (id=2)"/>&#xa;      <resource value="user 1"/>&#xa;      <attachment type="text" value="att text"/>&#xa;      <attachment type="text" value="dsf&#xa;&#xa;* df&#xa;* sdf &#xa;&#xa;this is a link : http://www.the-skore.com&#xa;"/>&#xa;      <attachment type="url" value="skore hompage" addr="http://www.the-skore.com"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="55" y="85" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="3">&#xa;      <text value="first whybox (id=3)"/><mxCell style="whybox;strokeColor=purple;" parent="1" vertex="1"><mxGeometry x="265" y="85" width="100" height="80" as="geometry"/></mxCell></whybox><mxCell id="4" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="2" target="3" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="5">&#xa;      <text value="box with details (5)"/>&#xa;      <resource value="user 2 should have RT">&#xa;        <tag value="R"/>&#xa;        <tag value="T"/>&#xa;      </resource>&#xa;      <resource value="I have a A">&#xa;        <tag value="A"/>&#xa;      </resource><mxCell style="whatboxdetailed" parent="1" collapsed="1" vertex="1"><mxGeometry x="405" y="85" width="190" height="121" as="geometry"/><mxRectangle width="1237" height="781" as="visibleRect"/></mxCell></whatbox><mxCell id="11" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="5" source="9" target="10" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="9">&#xa;      <text value="drill"/>&#xa;      <resource value="Who does it?"/>&#xa;      <attachment type="text" value="bulle"/><mxCell style="whatbox" parent="5" vertex="1"><mxGeometry x="54" y="128" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="10">&#xa;      <text value="So that?"/><mxCell style="whybox" parent="5" vertex="1"><mxGeometry x="264" y="128" width="100" height="80" as="geometry"/></mxCell></whybox><mxCell id="6" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="3" target="5" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="7">&#xa;      <text value="second whybox"/><mxCell style="whybox;strokeColor=purple;" parent="1" vertex="1"><mxGeometry x="615" y="85" width="100" height="80" as="geometry"/></mxCell></whybox><mxCell id="8" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="5" target="7" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="13" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="7" target="12" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><group id="16">&#xa;      <text value="Group (16)"/>&#xa;      <resource value="Who does it?"/><mxCell style="group" parent="1" connectable="0" vertex="1"><mxGeometry x="740" y="70" width="340" height="110" as="geometry"/></mxCell></group><whatbox id="12">&#xa;      <text value="box inside group (12)"/>&#xa;      <resource value="user 3"/><mxCell style="whatbox" parent="16" vertex="1"><mxGeometry x="15" y="15" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="14">&#xa;      <text value="third whybox"/><mxCell style="whybox" parent="16" vertex="1"><mxGeometry x="225" y="15" width="100" height="80" as="geometry"/></mxCell></whybox><mxCell id="15" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="16" source="12" target="14" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><stickynote id="17">&#xa;      <text value="#h1&#xa;&#xa;text&#xa;*italic*&#xa;&#xa;** bold **"/><mxCell style="stickynote" parent="1" vertex="1"><mxGeometry x="80" y="198" width="216" height="151" as="geometry"/></mxCell></stickynote><whatbox id="18">&#xa;      <text value="box under group"/>&#xa;      <resource value="user 4"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="755" y="205" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="19" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="7" target="18" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="20">&#xa;      <text value="fourth whybox"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="965" y="205" width="100" height="80" as="geometry"/></mxCell></whybox><mxCell id="21" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="18" target="20" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="22"><text value="Resources formatting"/><resource order="0" value="This is &lt; a **res** with ![icon red](heart) and ## header"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="405" y="234" width="190" height="87" as="geometry"/></mxCell></whatbox><mxCell id="23" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="3" target="22" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="24" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="53" target="60" edge="1"><mxGeometry x="1650" y="798" as="geometry"/></mxCell><mxCell id="25" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="52" target="53" edge="1"><mxGeometry x="1420" y="722" as="geometry"/></mxCell><mxCell id="26" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="43" target="52" edge="1"><mxGeometry x="1275" y="633" as="geometry"/></mxCell><mxCell id="27" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="47" target="49" edge="1"><mxGeometry x="451" y="923" as="geometry"/></mxCell><mxCell id="28" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="38" target="47" edge="1"><mxGeometry x="221" y="923" as="geometry"/></mxCell><mxCell id="29" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="42" target="43" edge="1"><mxGeometry x="1045" y="633" as="geometry"/></mxCell><mxCell id="30" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="41" target="42" edge="1"><mxGeometry x="900" y="633" as="geometry"/></mxCell><mxCell id="31" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="40" target="41" edge="1"><mxGeometry x="670" y="633" as="geometry"/></mxCell><mxCell id="32" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="39" target="40" edge="1"><mxGeometry x="525" y="633" as="geometry"/></mxCell><mxCell id="33" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="37" target="39" edge="1"><mxGeometry x="221" y="633" as="geometry"/></mxCell><mxCell id="34" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="36" target="39" edge="1"><mxGeometry x="221" y="567" as="geometry"/></mxCell><stickynote id="35"><text value="## This is a title&#xa;---&#xa;an a subtitle"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="95" y="354" width="595" height="135" as="geometry"/></mxCell></stickynote><whybox id="36"><text value="![icon 2x](arrow-right)&#xa;&#xa;input 1"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="101" y="527" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="37"><text value="![icon 2x](arrow-right)&#xa;&#xa;input 2"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="101" y="654" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="38"><text value="![icon 2x](arrow-right)&#xa;&#xa;input 3 custom branch"/><mxCell style="whybox;fontColor=orange;strokeWidth=1;dashed=;" parent="1" vertex="1"><mxGeometry x="101" y="883" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="39"><text value="text normal"/><resource value="r1"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="335" y="593" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="40"><text value="blabla"/><mxCell style="whybox;strokeColor=purple;" parent="1" vertex="1"><mxGeometry x="550" y="593" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="41"><text value="![icon 2x](arrow-right) icon"/><resource value="r2"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="710" y="593" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="42"><text value="*formated*&#xa;&#xa;**text on new line**&#xa;&#xa;---&#xa;* bullet &#xa;* bullet"/><mxCell style="whybox;strokeColor=purple;" parent="1" vertex="1"><mxGeometry x="925" y="593" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="43"><text value="This box has icon in attachment"/><resource value="aaaa I have a comment !!"/><attachment type="text" value="![icon 5x](arrow-up)&#xa;&#xa;and a dynamic link https://www.getskore.com &#xa;&#xa;and a [prepared link](https://www.getskore.com)&#xa;"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1085" y="593" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="44"><text value="Blue"/><mxCell style="stickynote;fillColor=#00FFFF;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="751" y="330" width="190" height="80" as="geometry"/></mxCell></stickynote><stickynote id="45"><text value="Red"/><mxCell style="stickynote;fillColor=#FF0000;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="846" y="365" width="190" height="80" as="geometry"/></mxCell></stickynote><stickynote id="46"><text value="# Important message"/><mxCell style="stickynote;fontColor=red;strokeColor=Red;strokeWidth=3;dashed=;rounded=;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="700" y="747" width="400" height="69" as="geometry"/></mxCell></stickynote><whatbox id="47"><text value="text"/><resource value="text"/><mxCell style="whatbox;fontColor=orange;strokeColor=orange;strokeWidth=1;dashed=1;rounded=;fillColor=orange;opacity=30;" parent="1" vertex="1"><mxGeometry x="261" y="883" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="48"><text value="### the orange branch"/><mxCell style="stickynote;fontColor=orange;strokeColor=orange;strokeWidth=5;dashed=1;rounded=1;" parent="1" vertex="1"><mxGeometry x="112" y="792" width="400" height="69" as="geometry"/></mxCell></stickynote><whybox id="49"><text value="dsf"/><mxCell style="whybox;fontColor=orange;strokeWidth=1;dashed=;" parent="1" vertex="1"><mxGeometry x="476" y="883" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="50"><text value="![icon 2x](arrow-down)&#xa;&#xa;![icon 2x](arrow-down)&#xa;&#xa;![icon 2x](arrow-down)&#xa;&#xa;![icon 2x](arrow-down)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="1241" y="423" width="46" height="164" as="geometry"/></mxCell></stickynote><stickynote id="51"><text value="**Try** the attachements &#xa;&#xa;Click on the ![icon 2x](paperclip)"/><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="1287" y="433" width="190" height="80" as="geometry"/></mxCell></stickynote><whybox id="52"><mxCell style="whybox;strokeColor=purple;" parent="1" vertex="1"><mxGeometry x="1300" y="682" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="53"><text value="This activity has a detail&#xa;&#xa;but as well **plenty** of *formatting* with `code`for geeks and even a [link](https://www.getskore.com) for curious&#xa;&#xa;and an ![icon 2x](bullhorn)"/><resource value=""/><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="1541" y="602" width="190" height="166" as="geometry"/><mxRectangle x="-100" y="-63" width="1150" height="526" as="visibleRect"/></mxCell></whatbox><mxCell id="54" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="53" source="55" target="57" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="55"><text value="Blabla"/><resource value=""/><mxCell style="whatboxdetailed" parent="53" vertex="1" collapsed="1"><mxGeometry x="80" y="313" width="190" height="80" as="geometry"/><mxRectangle x="-100" y="-100" width="1150" height="526" as="visibleRect"/></mxCell></whatbox><whatbox id="56"><text value=""/><resource value=""/><mxCell style="whatbox" parent="55" vertex="1"><mxGeometry x="372" y="44" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="57"><text value="box 57"/><mxCell style="whybox" parent="53" vertex="1"><mxGeometry x="396" y="330" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="58"><text value="![icon 2x](arrow-up)&#xa;&#xa;![icon 2x](arrow-up)&#xa;&#xa;![icon 2x](arrow-up)&#xa;&#xa;How does that render ?"/><mxCell style="stickynote;fillColor=#FF00FF;gradientColor=none;" parent="53" vertex="1"><mxGeometry x="80" y="16" width="190" height="154" as="geometry"/></mxCell></stickynote><stickynote id="59"><text value="Link destination"/><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="53" vertex="1"><mxGeometry x="623" y="159" width="190" height="80" as="geometry"/></mxCell></stickynote><whybox id="60"><text value="qsd"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1811" y="541" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="61"><text value=""/><mxCell style="stickynote;fillColor=#00FF00;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="941" y="410" width="190" height="80" as="geometry"/></mxCell></stickynote><whatbox id="62"><text value="[link to 95](95) on same diagram"/><resource value=""/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="346" y="1022" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="63"><text value="[link to 95](95) on same diagram"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="369" y="1168" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="64"><text value="[link to 43](43)"/><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="318" y="1299" width="190" height="80" as="geometry"/></mxCell></stickynote><whatbox id="65"><text value="[link inside 57](57d) but this is a whybox so should stay above"/><resource value=""/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="569" y="1030" width="190" height="86" as="geometry"/></mxCell></whatbox><whybox id="66"><text value="[link 57](57)"/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="596" y="1168" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="68" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="88" target="91" edge="1"><mxGeometry x="1771" y="1299" as="geometry"/></mxCell><mxCell id="69" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="87" target="90" edge="1"><mxGeometry x="1771" y="1184" as="geometry"/></mxCell><mxCell id="70" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="86" target="89" edge="1"><mxGeometry x="1771" y="1050" as="geometry"/></mxCell><mxCell id="71" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="84" target="85" edge="1"><mxGeometry x="2121" y="922" as="geometry"/></mxCell><mxCell id="72" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="82" target="83" edge="1"><mxGeometry x="1771" y="922" as="geometry"/></mxCell><mxCell id="73" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="81" target="82" edge="1"><mxGeometry x="1541" y="922" as="geometry"/></mxCell><mxCell id="74" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="78" target="80" edge="1"><mxGeometry x="1137" y="922" as="geometry"/></mxCell><mxCell id="75" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="78" target="79" edge="1"><mxGeometry x="1137" y="922" as="geometry"/></mxCell><mxCell id="76" style="linehighlight;exitX=1;exitY=0.5;entryX=0;entryY=0.5" parent="1" source="77" target="78" edge="1"><mxGeometry x="1017" y="922" as="geometry"/></mxCell><whatbox id="77">&#xa;      <text value=" "/>&#xa;      <resource value="Colin"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="855" y="1070" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="78">&#xa;      <text value=" "/><mxCell style="whybox;strokeColor=purple;" parent="1" vertex="1"><mxGeometry x="1065" y="1070" width="100" height="80" as="geometry"/></mxCell></whybox><whatbox id="79">&#xa;      <text value=" "/>&#xa;      <resource value="Colin"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1205" y="930" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="80">&#xa;      <text value=" "/>&#xa;      <resource value="Craig"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1205" y="1138" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="81">&#xa;      <text value=" "/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1441" y="882" width="100" height="80" as="geometry"/></mxCell></whybox><whatbox id="82">&#xa;      <text value=" "/>&#xa;      <resource value="Colin"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1581" y="882" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="83">&#xa;      <text value=" "/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1791" y="882" width="100" height="80" as="geometry"/></mxCell></whybox><whatbox id="84">&#xa;      <text value=" "/>&#xa;      <resource value="Colin"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1931" y="882" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="85">&#xa;      <text value=" "/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="2155" y="882" width="100" height="80" as="geometry"/></mxCell></whybox><whatbox id="86">&#xa;      <text value=" "/>&#xa;      <resource value="Craig"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1581" y="1010" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="87">&#xa;      <text value=" "/>&#xa;      <resource value="Cathy"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1581" y="1144" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="88">&#xa;      <text value=" "/>&#xa;      <resource value="Colin"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1581" y="1259" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="89">&#xa;      <text value=" "/><mxCell style="whybox;" parent="1" vertex="1"><mxGeometry x="1791" y="1010" width="100" height="80" as="geometry"/></mxCell></whybox><whybox id="90">&#xa;      <text value=" "/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1791" y="1144" width="100" height="80" as="geometry"/></mxCell></whybox><whybox id="91">&#xa;      <text value=" "/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1791" y="1259" width="100" height="80" as="geometry"/></mxCell></whybox><stickynote id="92"><text value="Add connections here"/><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="1358" y="944" width="98" height="66" as="geometry"/></mxCell></stickynote><stickynote id="93"><text value="Add connections here"/><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="1857" y="780" width="98" height="66" as="geometry"/></mxCell></stickynote><whatbox id="94"><text value="sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd  sqd qsd"/><resource order="0" value="DD"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1275" y="45" width="190" height="166" as="geometry"/></mxCell></whatbox><whatbox id="95"><text value="box 95 !"/><resource order="0" value=""/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="1971" y="541" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="97"><text value="Resources formatting 2"/><resource order="0" value="This is &lt; a **res** with ![icon red](heart) and ## header"/><resource value="another **one**"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="415" y="341" width="190" height="106" as="geometry"/></mxCell></whatbox><whatbox id="100"><text value="special caracters \' &quot; / \\ &lt; &gt; &amp;"/><resource order="0" value="special caracters \' &quot; / \\ &lt; &gt; &amp;"/><attachment type="text" value="special caracters \' &quot; / \\ &lt; &gt; &amp;"/><mxCell style="whatbox" parent="1" vertex="1"><mxGeometry x="106" y="1403" width="190" height="80" as="geometry"/></mxCell></whatbox></root></mxGraphModel>\n';});


define('text!demoContent/SkoreOfSkoreApp.skore',[],function () { return '<mxGraphModel dx="1117" dy="468" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" background="#ffffff" skoreFileSpec="2"><root><mxCell id="0"><roles as="roles">&#xa;        <role value="GraphEditorUI" localid="0"/>&#xa;        <role value="skNavigatorUI" localid="1"/>&#xa;        <role value="user" localid="2"/>&#xa;        <role value="skSearch" localid="3"/>&#xa;        <role value="skMenu_search" localid="4"/>&#xa;        <role value="skImageExport" localid="5"/>&#xa;        <role value="skModal_ImageExport" localid="6"/>&#xa;        <role value="desktop installer" localid="7"/>&#xa;        <role value="registered user" localid="8"/>&#xa;        <role value="unregistered user" localid="9"/>&#xa;        <role value="sko.re" localid="10"/>&#xa;        <role value="loc" localid="11"/>&#xa;        <role value="central" localid="12">&#xa;          <comment value="aka Server side"/>&#xa;        </role>&#xa;        <role value="roles manager" localid="13"/>&#xa;        <role value="graph editor" localid="14"/>&#xa;        <role value="ui" localid="15"/>&#xa;        <role value="User" localid="16"/>&#xa;        <role value="Skore" localid="17"/>&#xa;      <role value="editor" localid="18"/><role value="commenter" localid="19"/><role value="approver" localid="20"/><role value="skore" localid="21"/><role value="skore (graph editor)" localid="22"/></roles><stylesheets as="stylesheets">&#xa;        <stylesheet value="default">&#xa;          <style value="whatbox" styleTokens=""/>&#xa;          <style value="whybox" styleTokens=""/>&#xa;          <style value="line" styleTokens=""/>&#xa;          <style value="stickynote" styleTokens=""/>&#xa;          <style value="group" styleTokens=""/>&#xa;          <style value="wy2s" styleTokens=""/>&#xa;        </stylesheet>&#xa;      </stylesheets><forms as="forms"><form xmlns="http://www.w3.org/1999/xhtml" value="whatbox" name="multirolesratsi"></form></forms></mxCell><title id="1">&#xa;      <box><text value="Skore of Skore app"/></box><mxCell style="title" parent="0"/></title><mxCell id="821" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="818" target="820" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="819" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="816" target="818" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="817" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="578" target="816" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="640" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="637" target="639" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="638" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="514" target="637" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="609" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="606" target="608" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="607" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="514" target="606" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="579" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="171" target="578" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="560" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="514" target="559" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="515" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="512" target="514" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="513" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="505" target="512" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="417" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="184" target="416" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="404" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="171" target="179" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="403" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="401" target="397" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="402" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="37" target="401" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="400" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="37" target="399" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="394" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="173" target="393" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="185" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="25" target="184" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="183" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="179" target="182" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="176" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="171" target="175" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="174" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="171" target="173" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="172" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="19" target="171" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="38" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="31" target="37" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="34" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="11" target="33" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="32" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="29" target="31" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="30" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="21" target="29" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="26" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="23" target="25" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="24" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="12" target="23" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="22" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="19" target="21" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="20" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="16" target="19" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="18" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="12" target="16" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="17" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="14" target="16" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="15" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="11" target="14" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="13" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="1" source="11" target="12" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="11">&#xa;      <box><text value="a process to be captured"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="111" y="244" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="12"><box><text value="Open existing Skore map"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="271" y="244" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="504" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="431" target="435" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="503" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="445" target="435" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="502" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="489" target="492" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="501" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="493" target="482" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="498" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="489" target="463" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="506" y="908"/><mxPoint x="506" y="544"/></Array></mxGeometry></mxCell><mxCell id="497" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="495" target="463" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="496" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="203" target="495" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="491" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="489" target="482" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="490" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="209" target="489" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="485" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="480" target="442" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="484" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="421" target="429" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="483" style="entryX=0;entryY=0.5;exitX=1;exitY=0.5;" parent="12" source="482" target="480" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="481" style="entryX=0;entryY=0.5;exitX=1;exitY=0.5;" parent="12" source="480" target="420" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="474" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="231" target="429" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1337" y="424"/><mxPoint x="1337" y="909"/></Array></mxGeometry></mxCell><mxCell id="462" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="459" target="461" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="460" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="448" target="459" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="458" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="453" target="457" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="456" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="435" target="453" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="455" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="453" target="448" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="454" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="437" target="453" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="438" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="431" target="437" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="432" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="429" target="431" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="429"><box><text value="open graph from xml content"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="1354.0891089108911" y="868.2772277227723" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="431">&#xa;      <box><text value="content loaded"/></box><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="1569.0891089108911" y="868.2772277227723" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="437"><box><text value="Init graph anaylisis&#10;&#10;* extension&#10;* stylesheet [see here](295)&#10;* roles manager&#10;* html cache"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="1729.0891089108911" y="868.2772277227723" width="190" height="143" as="geometry"/></mxCell></whatbox><whatbox id="435"><box><text value="(if necessary) &#10;&#10;build editor UI"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="1729.0891089108911" y="1034.2772277227723" width="190" height="95" as="geometry"/></mxCell></whatbox><whybox id="453">&#xa;      <box><text value="more UI to be init"/></box><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="1944.0891089108911" y="868.2772277227723" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="448"><box><text value="inti editor UI&#10;* title&#10;* navigator"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="2104.089108910891" y="868.2772277227723" width="190" height="111" as="geometry"/></mxCell></whatbox><whatbox id="457"><box><text value="init ELECTRON"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="2104.089108910891" y="996.2772277227723" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="459"><box/><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="2319.089108910891" y="868.2772277227723" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="461"><box><text value="show welcome splash scren"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="2479.089108910891" y="868.2772277227723" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="422" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="420" target="421" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="420"><box><text value="Load skGraphNavigator"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="979.0891089108911" y="868.2772277227723" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="421">&#xa;      <box><text value="base to open a diagram"/></box><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="1194.0891089108911" y="868.2772277227723" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="480"><box/><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="819.0891089108911" y="868.2772277227723" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="482"><box><text value="Start Skore"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="604.0891089108911" y="868.2772277227723" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="479" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="471" target="231" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="478" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="470" target="231" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="477" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="463" target="231" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="476" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="464" target="231" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="464"><box><text value="open skore HTML file"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="845.078431372549" y="254.70588235294116" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="463"><box><text value="open from file"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="845.078431372549" y="504.3921568627451" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="470"><box><text value="drop file on skore"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="845.078431372549" y="614.3921568627451" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="471"><box><text value="web demo"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="845.078431372549" y="720.3921568627451" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="231">&#xa;      <box><text value="xml content"/></box><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="1117.078431372549" y="374.70588235294116" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="203"><box><text value="Recently opened file"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="451.07843137254895" y="374.70588235294116" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="495"><box/><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="680.078431372549" y="374.70588235294116" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="209"><box><text value="double click on .skore file"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="13.078431372549005" y="868.3921568627451" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="489">&#xa;      <box><text value="skore to be opened (with file content)"/></box><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="245.078431372549" y="868.3921568627451" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="494" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="492" target="493" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="492"><box><text value="open Skore app"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="203.078431372549" y="996.3921568627451" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="493">&#xa;      <box><text value="skore opened"/></box><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="432.078431372549" y="996.3921568627451" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="446" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="12" source="442" target="445" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="442"><box><text value="load base UI"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="987.029702970297" y="1033.930693069307" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="445"><box/><mxCell style="whybox" parent="12" vertex="1"><mxGeometry x="1202.029702970297" y="1033.930693069307" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="14"><box><text value="Create a new Skore"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="271" y="364" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="16">&#xa;      <box><text value="Skore map ready to be edited"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="486" y="364" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="19"><box><text value="Edit / Make change to a Skore map"/><responsibility><role localid="0"/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="646" y="364" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="120"><box><text value="Edit &amp; format box content"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="19" vertex="1" collapsed="1"><mxGeometry x="317" y="539" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="121" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="140" target="164" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="122" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="139" target="140" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="123" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="138" target="139" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="124" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="137" target="138" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="125" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="140" target="136" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="126" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="135" target="140" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="127" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="134" target="135" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="128" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="133" target="134" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="129" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="132" target="133" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="130" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="120" source="131" target="132" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="131">&#xa;      <box><text value="need to change the style for selected boxed"/></box><mxCell style="whybox" parent="120" vertex="1"><mxGeometry x="46" y="336" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="132"><box><text value="Select the boxes"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="120" vertex="1" collapsed="1"><mxGeometry x="206" y="336" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="133">&#xa;      <box><text value="ID tabs is updated with number of items selected in between dots"/></box><mxCell style="whybox" parent="120" vertex="1"><mxGeometry x="421" y="336" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="134"><box><text value="Click on ID tabs to open stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="120" vertex="1" collapsed="1"><mxGeometry x="581" y="336" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="135">&#xa;      <box><text value="modal opened with only relevant panels displayed on correct item count"/></box><mxCell style="whybox" parent="120" vertex="1"><mxGeometry x="796" y="336" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="136">&#xa;      <box><text value="item-s"/></box><mxCell style="whybox" parent="120" vertex="1"><mxGeometry x="1171" y="336" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="137">&#xa;      <box><text value="need to change the default stylesheet for the map"/></box><mxCell style="whybox" parent="120" vertex="1"><mxGeometry x="46" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="138"><box><text value="Open modal from the sandwich menu"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="120" vertex="1" collapsed="1"><mxGeometry x="206" y="198" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="139">&#xa;      <box><text value="modal opened with all panels displayed"/></box><mxCell style="whybox" parent="120" vertex="1"><mxGeometry x="796" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="140"><box><text value="Change style in modal"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatboxdetailed" parent="120" vertex="1" collapsed="1"><mxGeometry x="943" y="261" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="141" style="wy2s;exitX=0.5;exitY=0;entryX=0;entryY=0.5;" parent="140" source="157" target="163" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="142" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="160" target="161" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="143" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="153" target="160" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="144" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="158" target="159" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="145" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="157" target="158" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="146" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="156" target="157" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="147" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="155" target="156" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="148" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="154" target="155" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="149" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="153" target="154" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="150" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="140" source="152" target="153" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="151">&#xa;      <box><text value="modal opened with only relevant panels displayed on correct item count"/></box><mxCell style="whybox" parent="140" vertex="1"><mxGeometry x="71" y="596" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="152">&#xa;      <box><text value="modal opened with all panels displayed"/></box><mxCell style="whybox" parent="140" vertex="1"><mxGeometry x="71" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="153"><box><text value="Load default &quot;hard coded&quot; stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="140" vertex="1" collapsed="1"><mxGeometry x="231" y="438" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="154">&#xa;      <box><text value="stylesheet **does** exist"/></box><mxCell style="whybox" parent="140" vertex="1"><mxGeometry x="446" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="155"><box><text value="Load custom stylesheet of the skore map"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="140" vertex="1" collapsed="1"><mxGeometry x="606" y="438" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="156">&#xa;      <box><text value=""/></box><mxCell style="whybox" parent="140" vertex="1"><mxGeometry x="821" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="157"><box><text value="Compile &quot;current&quot; stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="140" vertex="1" collapsed="1"><mxGeometry x="981" y="438" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="158">&#xa;      <box><text value=""/></box><mxCell style="whybox" parent="140" vertex="1"><mxGeometry x="1196" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="159"><box><text value="Change the values"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="140" vertex="1" collapsed="1"><mxGeometry x="1356" y="438" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="160">&#xa;      <box><text value="stylesheet **does not** exist"/></box><mxCell style="whybox" parent="140" vertex="1"><mxGeometry x="446" y="558" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="161"><box><text value="Create (empty) custom stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="140" vertex="1" collapsed="1"><mxGeometry x="606" y="558" width="190" height="85" as="geometry"/></mxCell></whatbox><stickynote id="162">&#xa;      <box><text value="Type | Whatbox, Stickynte &amp; Group | Whybox | Line&#10;--- | --- | ---&#10;**Font color**          | **yes** | **yes** | n/a &#10;**Border color**        | **yes** | **yes** | **yes** &#10;**Border thickness**    | **yes** | **yes** | **yes** &#10;**Rounded corner**      | **yes** | n/a   | n/a &#10;**Dotted border**       | **yes** | **yes** | **yes**&#10;**Background color**    | **yes** | n/a   | n/a   &#10;**Background opacity**  | **yes** | n/a   | n/a"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="140" vertex="1"><mxGeometry x="231" y="19" width="752" height="293" as="geometry"/></mxCell></stickynote><stickynote id="163">&#xa;      <box><text value=""/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="140" vertex="1"><mxGeometry x="1150" y="303" width="279" height="73" as="geometry"/></mxCell></stickynote><whybox id="164">&#xa;      <box><text value="default style to be saved at skore map level"/></box><mxCell style="whybox" parent="120" vertex="1"><mxGeometry x="1171" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="165">&#xa;      <box><text value="## Look and feel"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="19" vertex="1"><mxGeometry x="37" y="445" width="336" height="75" as="geometry"/></mxCell></stickynote><stickynote id="167">&#xa;      <box><text value="## Others"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="19" vertex="1"><mxGeometry x="37" y="714" width="336" height="75" as="geometry"/></mxCell></stickynote><whatbox id="168"><box><text value="Create &amp; arrange boxes, fill in with text &amp; icons"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="19" vertex="1" collapsed="1"><mxGeometry x="68" y="172" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="415" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="413" target="317" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="414" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="410" target="413" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="411" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="409" target="410" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="408" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="317" target="407" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="323" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="319" target="322" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="321" style="exitX=0.5;exitY=1;entryX=0.5;entryY=0;" parent="168" source="307" target="317" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="320" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="314" target="319" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="318" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="314" target="317" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="316" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="309" target="314" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="315" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="305" target="314" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="310" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="306" target="309" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="308" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="168" source="306" target="305" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="305"><box><text value="add box from the cardshoe"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="168" vertex="1" collapsed="1"><mxGeometry x="266" y="175" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="309"><box><text value="add box from keyboard shortcut"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="168" vertex="1" collapsed="1"><mxGeometry x="266" y="295" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="306">&#xa;      <box><text value="boxes to be added"/></box><mxCell style="whybox" parent="168" vertex="1"><mxGeometry x="99" y="175" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="314">&#xa;      <box><text value="empty box on canvas"/></box><mxCell style="whybox" parent="168" vertex="1"><mxGeometry x="517" y="175" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="317"><box><text value="Move box"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="168" vertex="1" collapsed="1"><mxGeometry x="677" y="175" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="319"><box><text value="Edit box"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="168" vertex="1" collapsed="1"><mxGeometry x="677" y="295" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="346" style="wy2s;exitX=0.5;exitY=0;entryX=0;entryY=0.5;" parent="319" source="335" target="343" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="345" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="319" source="329" target="335" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="340" style="wy2s;exitX=0.5;exitY=0;entryX=0;entryY=0.5;" parent="319" source="325" target="328" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="337" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="319" source="335" target="333" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="330" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="319" source="325" target="329" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="327" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="319" source="326" target="325" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="325"><box><text value="edit text"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="319" vertex="1" collapsed="1"><mxGeometry x="209" y="224" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="326">&#xa;      <box><text value="box to be edited"/></box><mxCell style="whybox" parent="319" vertex="1"><mxGeometry x="48" y="224" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="329">&#xa;      <box><text value="text entered"/></box><mxCell style="whybox" parent="319" vertex="1"><mxGeometry x="438" y="224" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="335"><box><text value="Compute height of the **whatbox** or **stickynote**"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="319" vertex="1" collapsed="1"><mxGeometry x="618" y="224" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="333">&#xa;      <box><text value="box resized"/></box><mxCell style="whybox" parent="319" vertex="1"><mxGeometry x="831" y="224" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="328">&#xa;      <box><text value="**whatbox**&#10;* keyup : message if text too long"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="319" vertex="1"><mxGeometry x="343" y="67" width="275" height="103" as="geometry"/></mxCell></stickynote><stickynote id="343">&#xa;      <box><text value="**whatbox**&#10;* box gets taller with min height&#10;&#10;**stickynote**&#10;* box keeps same width as user defined&#10;* gets taller if needed&#10;* doesn\'t get less tall"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="319" vertex="1"><mxGeometry x="738" y="32" width="292" height="176" as="geometry"/></mxCell></stickynote><whybox id="307">&#xa;      <box><text value="boxes to be moved"/></box><mxCell style="whybox" parent="168" vertex="1"><mxGeometry x="712" y="30" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="322">&#xa;      <box><text value="box text on canvas and with right size"/></box><mxCell style="whybox" parent="168" vertex="1"><mxGeometry x="892" y="295" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="407"><box/><mxCell style="whybox" parent="168" vertex="1"><mxGeometry x="892" y="175" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="413">&#xa;      <box><text value="icon on canvas"/></box><mxCell style="whybox" parent="168" vertex="1"><mxGeometry x="517" y="442.8571428571429" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="410"><box><text value="add pre-filled stickynote with icon to canvas"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="168" vertex="1" collapsed="1"><mxGeometry x="266" y="442.8571428571429" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="409">&#xa;      <box><text value="icon to be added"/></box><mxCell style="whybox" parent="168" vertex="1"><mxGeometry x="99" y="442.8571428571429" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="166">&#xa;      <box><text value="## Boxes and lines"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="19" vertex="1"><mxGeometry x="37" y="76" width="336" height="75" as="geometry"/></mxCell></stickynote><whatbox id="71"><box><text value="Search in a Skore&#10;&#10;[See &quot;view Skore&quot;](94d)"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="19" vertex="1" collapsed="1"><mxGeometry x="42" y="798" width="190" height="95" as="geometry"/></mxCell></whatbox><whatbox id="43"><box><text value="Change default stylesheet or style of selected boxes"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="19" vertex="1" collapsed="1"><mxGeometry x="68" y="539" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="68" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="56" target="67" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="66" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="62" target="56" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="63" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="60" target="62" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="61" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="44" target="60" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="59" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="56" target="58" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="57" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="54" target="56" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="55" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="50" target="54" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="51" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="48" target="50" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="49" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="46" target="48" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="47" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="43" source="45" target="46" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="45">&#xa;      <box><text value="need to change the style for selected boxed"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="46" y="336" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="46"><box><text value="Select the boxes"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="43" vertex="1" collapsed="1"><mxGeometry x="206" y="336" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="48">&#xa;      <box><text value="ID tabs is updated with number of items selected in between dots"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="421" y="336" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="50"><box><text value="Click on ID tabs to open stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="43" vertex="1" collapsed="1"><mxGeometry x="581" y="336" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="54">&#xa;      <box><text value="* modal opened with only relevant panels displayed&#10;* correct item count"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="796" y="336" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="58">&#xa;      <box><text value="item-s have correct *inline* style"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="1171" y="336" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="44">&#xa;      <box><text value="need to change the default stylesheet for the map"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="46" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="62">&#xa;      <box><text value="modal opened with all panels displayed"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="796" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="56"><box><text value="Change style in modal"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatboxdetailed" parent="43" vertex="1" collapsed="1"><mxGeometry x="943" y="261" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="241" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="238" target="240" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="239" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="226" target="238" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="227" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="110" target="226" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="223" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="97" target="222" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="221" style="wy2s;exitX=0.5;exitY=0;entryX=0;entryY=0.5;" parent="56" source="195" target="220" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="219" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="110" target="218" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="202" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="197" target="201" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="198" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="195" target="197" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="196" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="190" target="195" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="192" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="188" target="110" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1335" y="598"/><mxPoint x="1335" y="478"/></Array></mxGeometry></mxCell><mxCell id="191" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="110" target="190" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="189" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="114" target="188" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="119" style="wy2s;exitX=0.5;exitY=0;entryX=0;entryY=0.5;" parent="56" source="106" target="118" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="115" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="112" target="114" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="113" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="98" target="112" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="111" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="108" target="110" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="109" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="106" target="108" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="107" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="104" target="106" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="105" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="102" target="104" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="103" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="100" target="102" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="101" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="98" target="100" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="99" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="56" source="96" target="98" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="96">&#xa;      <box><text value="modal opened with all panels displayed"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="71" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="98"><box><text value="Load SYSTEM Stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="231" y="438" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="100">&#xa;      <box><text value="stylesheet **does** exist"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="446" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="102"><box><text value="Load CUSTOM stylesheet of the skore map"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="606" y="438" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="104">&#xa;      <box><text value=""/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="821" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="106"><box><text value="Compile &quot;current&quot; stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="981" y="438" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="108">&#xa;      <box><text value=""/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="1196" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="110"><box><text value="Change the values in the modal"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="1356" y="438" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="112">&#xa;      <box><text value="stylesheet **does not** exist"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="446" y="558" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="114"><box><text value="Create (empty) custom stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="606" y="558" width="190" height="85" as="geometry"/></mxCell></whatbox><stickynote id="69">&#xa;      <box><text value="Type | Whatbox, Stickynte &amp; Group | Whybox | Line&#10;--- | --- | ---&#10;**Font color**          | **yes** | **yes** | n/a &#10;**Border color**        | **yes** | **yes** | **yes** &#10;**Border thickness**    | **yes** | **yes** | **yes** &#10;**Rounded corner**      | **yes** | n/a   | n/a &#10;**Dotted border**       | **yes** | **yes** | **yes**&#10;**Background color**    | **yes** | n/a   | n/a   &#10;**Background opacity**  | **yes** | n/a   | n/a"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="56" vertex="1"><mxGeometry x="231" y="19" width="625" height="318" as="geometry"/></mxCell></stickynote><stickynote id="118">&#xa;      <box><text value="Current Stylesheet = system + custom"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="56" vertex="1"><mxGeometry x="1150" y="303" width="279" height="73" as="geometry"/></mxCell></stickynote><whybox id="188">&#xa;      <box><text value="stylesheet added to the model"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="824.8" y="558" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="190">&#xa;      <box><text value="clicked &quot;Apply"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="1571" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="197"><box/><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="1946" y="438" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="201"><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="2106" y="438" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="97">&#xa;      <box><text value="modal opened with only relevant panels displayed on correct item count"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="64" y="732" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="222"><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="981" y="732" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="218">&#xa;      <box><text value="clicked &quot;cancel&quot;... modal closed&#10;&#10;![icon](close)"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="1571" y="859" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="226">&#xa;      <box><text value="clicked &quot;save for new skore&quot;"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="1571" y="658" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="238"><box><text value="Write stylesheet in user pref&#10;&#10;&#10;[![icon](arrow-right)](296)"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="1731" y="658" width="190" height="95" as="geometry"/></mxCell></whatbox><whybox id="240">&#xa;      <box><text value="stylesheet saved for new skore&#10;&#10;[![icon](arrow-right)](296)"/></box><mxCell style="whybox" parent="56" vertex="1"><mxGeometry x="1946" y="658" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="195"><box><text value="Save in stylesheet of the map"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="56" vertex="1" collapsed="1"><mxGeometry x="1731" y="438" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="220">&#xa;      <box><text value="Saves only variation to system stylesheet"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="56" vertex="1"><mxGeometry x="1876.1372549019607" y="336.3137254901961" width="190" height="80" as="geometry"/></mxCell></stickynote><whybox id="67">&#xa;      <box><text value="default style to be saved at skore map level"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="1171" y="198" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="60"><box><text value="Open modal from the sandwich menu"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="43" vertex="1" collapsed="1"><mxGeometry x="581" y="198" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="295"><box><text value="load styleSheet"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="43" vertex="1" collapsed="1"><mxGeometry x="212" y="44" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="815" style="wy2s;exitX=0.5;exitY=1;entryX=0.5;entryY=0;" parent="295" source="813" target="804" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="795" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="803" target="811" edge="1"><mxGeometry x="397" y="264" as="geometry"/></mxCell><mxCell id="796" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="809" target="810" edge="1"><mxGeometry x="564" y="314" as="geometry"/></mxCell><mxCell id="797" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="803" target="809" edge="1"><mxGeometry x="397" y="264" as="geometry"/></mxCell><mxCell id="798" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="807" target="808" edge="1"><mxGeometry x="794" y="191" as="geometry"/></mxCell><mxCell id="799" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="804" target="807" edge="1"><mxGeometry x="564" y="191" as="geometry"/></mxCell><mxCell id="800" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="806" target="803" edge="1"><mxGeometry x="173" y="264" as="geometry"/></mxCell><mxCell id="801" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="805" target="803" edge="1"><mxGeometry x="173" y="172" as="geometry"/></mxCell><mxCell id="802" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="803" target="804" edge="1"><mxGeometry x="397" y="191" as="geometry"/></mxCell><whatbox id="803"><box><text value="open skore file&#10;&#10;* in current canvas if empty&#10;* in new window"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="295" collapsed="1" vertex="1"><mxGeometry x="207" y="224" width="190" height="127" as="geometry"/></mxCell></whatbox><whybox id="805">&#xa;      <box><text value="blank skore canvas"/></box><mxCell style="whybox" parent="295" vertex="1"><mxGeometry x="53" y="132" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="806">&#xa;      <box><text value="blank skore canvas"/></box><mxCell style="whybox" parent="295" vertex="1"><mxGeometry x="53" y="309" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="809">&#xa;      <box><text value="no stylehseet (old file)"/></box><mxCell style="whybox" parent="295" vertex="1"><mxGeometry x="444" y="274" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="810"><box><text value="load file with default stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="295" vertex="1" collapsed="1"><mxGeometry x="604" y="274" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="804">&#xa;      <box><text value="stylesheet existing but empty&#10;&#10;`&lt;stylesheets as=&quot;stylesheets&quot;&gt;&#10;    &lt;stylesheet value=&quot;default&quot;/&gt;&#10;   &lt;/stylesheets&gt;`&#10;&#10;hello"/></box><mxCell style="whybox" parent="295" vertex="1"><mxGeometry x="444" y="139" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="794" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="295" source="811" target="812" edge="1"><mxGeometry x="564" y="504" as="geometry"/></mxCell><whybox id="811">&#xa;      <box><text value="stylesheet"/></box><mxCell style="whybox" parent="295" vertex="1"><mxGeometry x="444" y="419" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="812"><box><text value="load file stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="295" vertex="1" collapsed="1"><mxGeometry x="604" y="419" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="807"><box><text value="load file with system stylesheet"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="295" vertex="1" collapsed="1"><mxGeometry x="604" y="139" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="808"><box/><mxCell style="whybox" parent="295" vertex="1"><mxGeometry x="819" y="139" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="813">&#xa;      <box><text value="example : NPD"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="295" vertex="1"><mxGeometry x="411" y="29" width="140" height="61" as="geometry"/></mxCell></stickynote><whybox id="349">&#xa;      <box><text value="I want to customize stylesheet for this map"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="70" y="535" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="348">&#xa;      <box><text value="I want to reset a stylesheet to the &quot;hardcoded&quot; system stylesheet"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="70" y="666" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="350">&#xa;      <box><text value="I want my new skore to use  my customized stylesheet"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="276" y="535" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="351">&#xa;      <box><text value="I want to reset to my"/></box><mxCell style="whybox" parent="43" vertex="1"><mxGeometry x="475" y="545" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="347">&#xa;      <box><text value="## Attachments"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="19" vertex="1"><mxGeometry x="595" y="76" width="336" height="75" as="geometry"/></mxCell></stickynote><whatbox id="406"><box><text value="Copy from one skore to another"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="19" vertex="1" collapsed="1"><mxGeometry x="68" y="296" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="612"><box><text value="Attach a link or text"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="19" vertex="1" collapsed="1"><mxGeometry x="602.3809523809524" y="183.33333333333337" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="613"><box><text value="Attach a file (file registry)"/><responsibility><role localid="10"/></responsibility></box><mxCell style="whatboxdetailed" parent="19" vertex="1" collapsed="1"><mxGeometry x="602.3809523809524" y="303.33333333333337" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="628" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="613" source="614" target="627" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="626" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="613" source="623" target="625" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="624" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="613" source="619" target="623" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="620" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="613" source="617" target="619" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="618" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="613" source="615" target="617" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="616" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="613" source="614" target="615" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="614"><box><text value="Add a file"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="613" vertex="1" collapsed="1"><mxGeometry x="119.14285714285714" y="277.0952380952381" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="615">&#xa;      <box><text value="file can be linked internally"/></box><mxCell style="whybox" parent="613" vertex="1"><mxGeometry x="334.1428571428571" y="277.0952380952381" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="617"><box><text value="Manage meta-data"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="613" vertex="1" collapsed="1"><mxGeometry x="494.1428571428571" y="277.0952380952381" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="619">&#xa;      <box><text value="meta data up to date"/></box><mxCell style="whybox" parent="613" vertex="1"><mxGeometry x="709.1428571428571" y="277.0952380952381" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="623"><box><text value="Remove file"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="613" vertex="1" collapsed="1"><mxGeometry x="869.1428571428571" y="277.0952380952381" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="625"><box/><mxCell style="whybox" parent="613" vertex="1"><mxGeometry x="1084.142857142857" y="277.0952380952381" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="627">&#xa;      <box><text value="permalink available"/></box><mxCell style="whybox" parent="613" vertex="1"><mxGeometry x="334.1428571428571" y="397.0952380952381" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="629">&#xa;      <box><text value="# File registry&#10;&#10;File can be stored in the workspace, to be used in links on boxes or anywhere really as it will just create a permalink"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="613" vertex="1"><mxGeometry x="60" y="42" width="550" height="158" as="geometry"/></mxCell></stickynote><whybox id="21">&#xa;      <box><text value="Changes done"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="861" y="364" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="23">&#xa;      <box><text value="Skore map loaded"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="486" y="244" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="25"><box><text value="View a Skore"/><responsibility><role localid="1"/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="1783" y="244" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="94"><box><text value="Search in a Skore"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="25" vertex="1" collapsed="1"><mxGeometry x="419" y="127" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="272" style="wy2s;exitX=0.5;exitY=1;entryX=0;entryY=0.5;" parent="94" source="257" target="271" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="270" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="267" target="269" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="268" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="265" target="267" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="266" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="249" target="265" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="264" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="253" target="263" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="262" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="255" target="251" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="261" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="257" target="255" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="260" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="95" target="259" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="258" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="95" target="257" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="254" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="251" target="253" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="252" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="249" target="251" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="250" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="247" target="249" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="248" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="94" source="95" target="247" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="95">&#xa;      <box><text value="need to find something in a SKore"/></box><mxCell style="whybox" parent="94" vertex="1"><mxGeometry x="51" y="243" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="247"><box><text value="Type keyword search box"/><responsibility><role localid="2"/></responsibility><responsibility><role localid="4"/></responsibility></box><mxCell style="whatbox" parent="94" vertex="1" collapsed="1"><mxGeometry x="211" y="243" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="249">&#xa;      <box><text value="keyword entered"/></box><mxCell style="whybox" parent="94" vertex="1"><mxGeometry x="426" y="243" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="253">&#xa;      <box><text value="list of boxes to highlight"/></box><mxCell style="whybox" parent="94" vertex="1"><mxGeometry x="801" y="242" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="251"><box><text value="Search relevant boxes according to keyword and filters"/><responsibility><role localid="3"/></responsibility></box><mxCell style="whatbox" parent="94" vertex="1" collapsed="1"><mxGeometry x="586" y="243" width="190" height="101" as="geometry"/></mxCell></whatbox><whatbox id="263"><box><text value="highlight boxes"/><responsibility><role localid="3"/></responsibility></box><mxCell style="whatbox" parent="94" vertex="1" collapsed="1"><mxGeometry x="961" y="242" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="257"><box><text value="Select &quot;filters&quot;"/><responsibility><role localid="4"/></responsibility><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="94" vertex="1" collapsed="1"><mxGeometry x="211" y="384" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="255">&#xa;      <box><text value="filters selected"/></box><mxCell style="whybox" parent="94" vertex="1"><mxGeometry x="426" y="384" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="265"><box><text value="Clear search"/><responsibility><role localid="4"/></responsibility><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="94" vertex="1" collapsed="1"><mxGeometry x="586" y="112.5" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="267">&#xa;      <box><text value="menu reset"/></box><mxCell style="whybox" parent="94" vertex="1"><mxGeometry x="801" y="112.5" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="269"><box><text value="unhighlight boxes"/><responsibility><role localid="3"/></responsibility></box><mxCell style="whatbox" parent="94" vertex="1" collapsed="1"><mxGeometry x="961" y="112.5" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="259"><box><text value="Select scope"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox;fontColor=lightgrey;strokeColor=lightgrey;strokeWidth=1;dashed=;rounded=;fillColor=transparent;opacity=100;" parent="94" vertex="1" collapsed="1"><mxGeometry x="211" y="628" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="271">&#xa;      <box><text value="* text whybox&#10;* text whatbox&#10;* text note&#10;&#10;* role whatbox&#10;&#10;* attachment&#10;&#10;&#10;* exact match&#10;&#10;* id"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="94" vertex="1"><mxGeometry x="432" y="508" width="249" height="187" as="geometry"/></mxCell></stickynote><whatbox id="832"><box><text value="Navigate in Skore"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="25" vertex="1" collapsed="1"><mxGeometry x="87" y="129" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="882" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="880" target="865" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="879" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="876" target="878" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="868" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="838" target="867" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="857" style="exitX=1;exitY=0.5;entryY=0.5;entryX=0" parent="832" source="838" target="845" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="856" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="838" target="844" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="834" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="839" target="842" edge="1"><mxGeometry x="353" y="133" as="geometry"/></mxCell><mxCell id="836" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="838" target="840" edge="1"><mxGeometry x="123" y="213" as="geometry"/></mxCell><mxCell id="837" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="838" target="839" edge="1"><mxGeometry x="123" y="133" as="geometry"/></mxCell><whybox id="838">&#xa;      <box><text value="need to navigate"/></box><mxCell style="whybox" parent="832" vertex="1"><mxGeometry x="3" y="173" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="871" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="867" target="842" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="866" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="842" target="865" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="873" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="863" target="872" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="864" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="832" source="840" target="863" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="863">&#xa;      <box><text value="destination is precise"/></box><mxCell style="whybox" parent="832" vertex="1"><mxGeometry x="380" y="128" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="872"><box><text value="Open with specific box centered and selected"/><responsibility><role localid="17"/></responsibility></box><mxCell style="whatbox" parent="832" vertex="1" collapsed="1"><mxGeometry x="540" y="128" width="190" height="85" as="geometry"/></mxCell></whatbox><group id="889"><box><text value="link nav"/></box><mxCell style="group" parent="832" vertex="1" connectable="0"><mxGeometry x="150" y="113" width="220" height="220" as="geometry"/></mxCell></group><whatbox id="840"><box><text value="navigate with a link to a **specifc** box i.e.  \\[link](xx)"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="889" vertex="1" collapsed="1"><mxGeometry x="15" y="15" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="867"><box><text value="navigate with a link to a **level** box i.e.  \\[link](xx**d**)"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="889" vertex="1" collapsed="1"><mxGeometry x="15" y="120" width="190" height="85" as="geometry"/></mxCell></whatbox><group id="885"><box><text value="Breadcrumb nav"/></box><mxCell style="group" parent="832" vertex="1" connectable="0"><mxGeometry x="150" y="589" width="365" height="305" as="geometry"/></mxCell></group><whatbox id="845"><box><text value="naviigate up several levels"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="885" vertex="1" collapsed="1"><mxGeometry x="15" y="210" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="876"><box><text value="last positioning known"/></box><mxCell style="whybox" parent="885" vertex="1"><mxGeometry x="230" y="183" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="883" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="885" source="845" target="876" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="880"><box><text value="last position not known"/></box><mxCell style="whybox" parent="885" vertex="1"><mxGeometry x="230" y="82" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="884" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="885" source="845" target="880" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="844"><box><text value="naviigate up a level"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="885" vertex="1" collapsed="1"><mxGeometry x="15" y="15" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="877" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="885" source="844" target="876" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="881" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="885" source="844" target="880" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="878"><box><text value="Open with last positioning"/><responsibility><role localid="17"/></responsibility></box><mxCell style="whatbox" parent="832" vertex="1" collapsed="1"><mxGeometry x="542" y="772" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="839"><box><text value="navigate to a lower level with &quot;enter detail&quot; button"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="832" vertex="1" collapsed="1"><mxGeometry x="165" y="418" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="842">&#xa;      <box><text value="user to enter details first time or has already visited details"/></box><mxCell style="whybox" parent="832" vertex="1"><mxGeometry x="382" y="351" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="865"><box><text value="Open with position reset"/><responsibility><role localid="17"/></responsibility></box><mxCell style="whatbox" parent="832" vertex="1" collapsed="1"><mxGeometry x="540" y="431" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="888"><box><text value="**new**"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="832" vertex="1"><mxGeometry x="710" y="411" width="83" height="40" as="geometry"/></mxCell></stickynote><whatbox id="29"><box><text value="Save Skore"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="1021" y="364" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="31"><box/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1236" y="364" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="33"><box><text value="Install Skore"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="271" y="484" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="405"><box><text value="Register .skore extension to OS"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="33" vertex="1" collapsed="1"><mxGeometry x="294.28571428571433" y="205.71428571428572" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="37"><box><text value="Share Skore"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="1396" y="364" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="42">&#xa;      <box><text value="# Skore of Skore App&#10;Describes the user journey through the &#10;&#10;used for product testing"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="32" y="20" width="589" height="139" as="geometry"/></mxCell></stickynote><whybox id="171">&#xa;      <box><text value="skore ready"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="861" y="484" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="173"><box><text value="Export as image"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="1021" y="484" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="392" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="376" target="383" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="378" style="exitX=0.5;exitY=1;entryX=0;entryY=0.5" parent="173" source="376" target="364" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="365" style="exitX=0.5;exitY=1;entryX=0;entryY=0.5" parent="173" source="362" target="364" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="390" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="383" target="389" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="388" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="383" target="387" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="386" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="383" target="385" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="384" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="381" target="383" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="382" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="364" target="381" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="377" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="366" target="376" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="373" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="362" target="366" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="370" style="exitX=1;exitY=0.5;entryX=0.5;entryY=0;" parent="173" source="352" target="358" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="367" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="360" target="366" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="363" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="358" target="362" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="361" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="358" target="360" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="359" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="356" target="358" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="357" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="354" target="356" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="355" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="173" source="353" target="354" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="352">&#xa;      <box><text value="all skore to be exported as image"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="38" y="56" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="353">&#xa;      <box><text value="**selected** cells to be exported"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="38" y="184" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="354"><box><text value="Select the cells"/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="173" vertex="1" collapsed="1"><mxGeometry x="198" y="184" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="356">&#xa;      <box><text value="cells ready"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="413" y="184" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="358"><box><text value="Select image destination"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="173" vertex="1" collapsed="1"><mxGeometry x="573" y="184" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="360">&#xa;      <box><text value="direct to clipboard"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="788" y="184" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="362">&#xa;      <box><text value="preview first, then save"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="788" y="304" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="366"><box><text value="Create image file"/><responsibility><role localid="5"/></responsibility></box><mxCell style="whatbox" parent="173" vertex="1" collapsed="1"><mxGeometry x="999" y="184" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="376">&#xa;      <box><text value="image file created &amp; cropped"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="1214" y="184" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="383"><box><text value="Save image"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="173" vertex="1" collapsed="1"><mxGeometry x="1758" y="184" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="385">&#xa;      <box><text value="saved to clipboard"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="1973" y="184" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="387">&#xa;      <box><text value="saved to desktop"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="1973" y="304" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="389">&#xa;      <box><text value="saved to specified file"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="1973" y="424" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="364"><box><text value="Show preview window"/><responsibility><role localid="6"/></responsibility></box><mxCell style="whatbox" parent="173" vertex="1" collapsed="1"><mxGeometry x="1383" y="356" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="381">&#xa;      <box><text value="image file displayed"/></box><mxCell style="whybox" parent="173" vertex="1"><mxGeometry x="1598" y="356" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="175"><box><text value="Print Skore"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="1021" y="604" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="179"><box><text value="Analytics"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="1021" y="782" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="561"><box><text value="Reports data"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="179" vertex="1" collapsed="1"><mxGeometry x="189.28571428571428" y="83.33333333333334" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="562"><box><text value="Manage roles"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="179" vertex="1" collapsed="1"><mxGeometry x="188.90476190476193" y="263.42857142857144" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="563"><box><text value=""/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="179" vertex="1" collapsed="1"><mxGeometry x="188.8095238095238" y="457.8571428571429" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="182"><box/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1223" y="776" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="184"><box/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1998" y="244" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="393">&#xa;      <box><text value="image can be used in other tool"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1236" y="484" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="397"><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="1771" y="470" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="399">&#xa;      <box><text value="justSkore.it URL received"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1611" y="308" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="401">&#xa;      <box><text value="HTML file created"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1611" y="470" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="416"><box><text value="Navigate in a Skore"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="2158" y="244" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="505">&#xa;      <box><text value="someone on app.sko.re"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="129.04761904761907" y="1157.6666666666667" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="512"><box><text value="login to workspace"/><responsibility><role localid="8"/></responsibility><responsibility><role localid="9"/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="281.04761904761904" y="1157.6666666666667" width="190" height="105" as="geometry"/></mxCell></whatbox><mxCell id="558" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="555" target="557" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="556" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="553" target="555" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="554" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="539" target="553" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="552" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="549" target="539" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="550" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="547" target="549" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="548" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="545" target="547" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="546" style="entryX=0;entryY=0.5;exitX=1;exitY=0.5;" parent="512" source="545" target="531" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="542" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="539" target="541" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="540" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="527" target="539" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="538" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="537" target="526" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="528" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="512" source="526" target="527" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="527">&#xa;      <box><text value="email / password setup"/></box><mxCell style="whybox" parent="512" vertex="1"><mxGeometry x="498.33333333333337" y="122.61904761904762" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="537">&#xa;      <box><text value="someone new on app.sko.re"/></box><mxCell style="whybox" parent="512" vertex="1"><mxGeometry x="104.14285714285714" y="122.28571428571428" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="526"><box><text value="create user account"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="512" vertex="1" collapsed="1"><mxGeometry x="272.33333333333337" y="122.61904761904762" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="539"><box><text value="create a workspace"/><responsibility><role/></responsibility></box><attachments><text value="* check workspace does not exist yet&#10;"/></attachments><mxCell style="whatbox" parent="512" vertex="1" collapsed="1"><mxGeometry x="658.3333333333334" y="122.61904761904762" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="541">&#xa;      <box><text value="workspace created, user on the right workspace"/></box><mxCell style="whybox" parent="512" vertex="1"><mxGeometry x="873.3333333333334" y="122.61904761904762" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="545">&#xa;      <box><text value="registered user on app.sko.re"/></box><mxCell style="whybox" parent="512" vertex="1"><mxGeometry x="112.38095238095241" y="280.95238095238096" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="531"><box><text value="retrieve forgotten password"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="512" vertex="1" collapsed="1"><mxGeometry x="272.3809523809524" y="416.95238095238096" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="547"><box><text value="login to workspace"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="512" vertex="1" collapsed="1"><mxGeometry x="272.3809523809524" y="280.95238095238096" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="549">&#xa;      <box><text value="user logged in"/></box><mxCell style="whybox" parent="512" vertex="1"><mxGeometry x="487.3809523809524" y="280.95238095238096" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="553"><box/><mxCell style="whybox" parent="512" vertex="1"><mxGeometry x="873.3333333333334" y="242.61904761904762" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="555"><box><text value="invite additional users"/><responsibility><role/></responsibility></box><attachments><text value="can invite people with email. system must recognise if person already has account or not"/></attachments><mxCell style="whatbox" parent="512" vertex="1" collapsed="1"><mxGeometry x="1033.3333333333335" y="242.61904761904762" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="557"><box/><mxCell style="whybox" parent="512" vertex="1"><mxGeometry x="1248.3333333333335" y="242.61904761904762" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="514">&#xa;      <box><text value="user on right workspace"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="511.04761904761904" y="1157.6666666666667" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="559"><box><text value="manage workspace"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="671.047619047619" y="1157.6666666666667" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="575" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="559" source="572" target="574" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="564"><box><text value="manage users of a workspace"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="559" vertex="1" collapsed="1"><mxGeometry x="155.85714285714286" y="355" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="565"><box><text value="manage content"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="559" vertex="1" collapsed="1"><mxGeometry x="155.85714285714286" y="511" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="570"><box><text value="manage billing / invoicing"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="559" vertex="1" collapsed="1"><mxGeometry x="622.5238095238095" y="355" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="573">&#xa;      <box><text value="As a user"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="559" vertex="1"><mxGeometry x="113" y="42" width="982" height="38" as="geometry"/></mxCell></stickynote><whatbox id="572"><box><text value="manage workspace i\'m part of"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="559" vertex="1" collapsed="1"><mxGeometry x="139.52380952380952" y="128" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="574">&#xa;      <box><text value="can view &amp; access all workspaces"/></box><mxCell style="whybox" parent="559" vertex="1"><mxGeometry x="354.5238095238095" y="128" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="576">&#xa;      <box><text value="As a workspace admin"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="559" vertex="1"><mxGeometry x="123" y="283" width="982" height="38" as="geometry"/></mxCell></stickynote><whatbox id="605"><box><text value="See workspace activity"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="559" vertex="1" collapsed="1"><mxGeometry x="155.85714285714286" y="633" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="577">&#xa;      <box><text value="Sko.re"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1"><mxGeometry x="111" y="892" width="2123" height="37" as="geometry"/></mxCell></stickynote><whatbox id="578"><box><text value="manage versions of a process"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="1021" y="1042" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="604" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="592" target="603" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="602" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="592" target="601" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="600" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="592" target="599" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="598" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="596" target="585" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="597" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="594" target="596" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="595" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="583" target="594" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="586" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="583" target="585" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="584" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="581" target="583" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="582" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="580" target="581" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="580">&#xa;      <box><text value="skore ready"/></box><mxCell style="whybox" parent="578" vertex="1"><mxGeometry x="47.71428571428572" y="298.7142857142857" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="581"><box><text value="make changes"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="578" vertex="1" collapsed="1"><mxGeometry x="207.71428571428572" y="298.7142857142857" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="1066" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="999" target="1065" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1058" style="exitX=1;exitY=0.5;entryY=0.5;entryX=0" parent="581" source="1003" target="1050" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1055" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1053" target="999" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1054" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1050" target="1053" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1052" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="988" target="1013" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1051" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="997" target="1050" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1049" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1038" target="1003" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1048" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1038" target="997" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1045" style="exitX=1;exitY=0.5;entryY=0.5;entryX=0" parent="581" source="1043" target="993" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1044" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1041" target="1043" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1042" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="991" target="1041" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1040" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1038" target="991" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1039" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1036" target="1038" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1037" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1024" target="1036" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1025" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1022" target="1024" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1023" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="1013" target="1022" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="988"><box><text value="user opened process version `N`"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="52" y="401" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="1013"><box><text value="map process....."/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="581" vertex="1" collapsed="1"><mxGeometry x="196" y="401" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="1022"><box><text value="first save, `version=null`"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="411" y="401" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="1024"><box><text value="save on workspace, create version `N+1` communicate value in callback data"/><responsibility><role localid="21"/></responsibility></box><mxCell style="whatbox" parent="581" vertex="1" collapsed="1"><mxGeometry x="571" y="401" width="190" height="101" as="geometry"/></mxCell></whatbox><whybox id="1036"><box><text value="process version `N+1` and `version=N+1`"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="786" y="401" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="1038"><box><text value="make more changes....."/><responsibility><role localid="2"/></responsibility></box><mxCell style="whatbox" parent="581" vertex="1" collapsed="1"><mxGeometry x="946" y="401" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="991"><box><text value="SAVE button clicked"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="1184" y="401" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="996" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="993" target="995" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="993"><box><text value="Save process as `N+2`"/><responsibility><role localid="21"/></responsibility></box><mxCell style="whatbox" parent="581" vertex="1" collapsed="1"><mxGeometry x="1714" y="401" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="995"><box><text value="version `N+2` is saved, `version=N+2`"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="1929" y="401" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="1041"><box><text value="Trigger save with parameter `version=null`"/><responsibility><role localid="22"/></responsibility></box><mxCell style="whatbox" parent="581" vertex="1" collapsed="1"><mxGeometry x="1344" y="401" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="1043"><box><text value="version is null, so the server creates a new version and send version number in callback"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="1559" y="401" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="1002" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="581" source="999" target="1001" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="1003"><box><text value="autosave triggered"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="1184" y="709" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="1057" style="wy2s;exitX=0.5;exitY=1;entryY=0.5;entryX=0" parent="581" source="1053" target="1056" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="999"><box><text value="Save process as `N+1` because parameter `version=N+1`"/><responsibility><role localid="21"/></responsibility></box><mxCell style="whatbox" parent="581" vertex="1" collapsed="1"><mxGeometry x="1714" y="658" width="190" height="101" as="geometry"/></mxCell></whatbox><whatbox id="1050"><box><text value="Trigger save with parameter `version=N+1`"/><responsibility><role localid="22"/></responsibility></box><mxCell style="whatbox" parent="581" vertex="1" collapsed="1"><mxGeometry x="1344" y="658" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="1053"><box><text value="version is known so we will (over)write on this version"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="1559" y="658" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="1056"><box><text value="note : we can only overwrite the latest version"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="581" vertex="1"><mxGeometry x="1652" y="772" width="190" height="80" as="geometry"/></mxCell></stickynote><group id="1062"><box><text value="back to workspace"/></box><mxCell style="group" parent="581" vertex="1" connectable="0"><mxGeometry x="1169" y="517" width="895" height="127" as="geometry"/></mxCell></group><whybox id="997"><box><text value="BACK TO WORKSPACE button clicked"/></box><mxCell style="whybox" parent="1062" vertex="1"><mxGeometry x="15" y="15" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="1001"><box><text value="user in workspace"/></box><mxCell style="whybox" parent="1062" vertex="1"><mxGeometry x="760" y="32" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="1065"><box><text value="user keep working in the file, version is still N+1"/></box><mxCell style="whybox" parent="581" vertex="1"><mxGeometry x="1929" y="719" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="1029"><box><text value="## Save logic online &#10;&#10;The challenge is to know when to create a new version of the process. We don\'t want each and every small autosave to be a new version. &#10;&#10;Implementation suggestioin: &#10;There is a variable called &quot;version&quot; in the graph editor that is `null` when starting. This variable is communicated to the server when saving (along with the XML). &#10;* If it\'s null, a new version is created, new version number communicated in callback;&#10;* if non-null; it keeps updating the latest version&#10;&#10;Note : there is only WHOLE / INTEGER number called revision&#10;&#10;benefit of this approach&#10;* latest version always up to date because autosave just overwrites the latest version&#10;* if I close browser and comeback in graph editor, my `version` variable will be null so it will automatically start a new version&#10;* with this implementation, a revision should be equal to a &quot;session&quot; of editing a process"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="581" vertex="1"><mxGeometry x="32" y="26" width="814" height="303" as="geometry"/></mxCell></stickynote><stickynote id="1067"><box><text value="etc."/></box><mxCell style="stickynote;fillColor=#00AE9A;gradientColor=none;" parent="581" vertex="1"><mxGeometry x="2140" y="586" width="153" height="53" as="geometry"/></mxCell></stickynote><stickynote id="1068"><box><text value="Process name | version | who | when"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="581" vertex="1"><mxGeometry x="596" y="578" width="190" height="80" as="geometry"/></mxCell></stickynote><whybox id="583">&#xa;      <box><text value="new revision created"/></box><mxCell style="whybox" parent="578" vertex="1"><mxGeometry x="422.7142857142857" y="298.7142857142857" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="587">&#xa;      <box><text value="**revision** : any simple change in a process&#10;&#10;**version** : a revision where a significant *milestone* reached and want to mark it"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="578" vertex="1"><mxGeometry x="50" y="75" width="678" height="69" as="geometry"/></mxCell></stickynote><whatbox id="594"><box><text value="get authorization to publish"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="578" vertex="1" collapsed="1"><mxGeometry x="582.7142857142858" y="418.7142857142857" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="593" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="590" target="592" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="591" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="588" target="590" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="589" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="578" source="585" target="588" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="585"><box><text value="mark as a new &quot;published&quot; version"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="578" vertex="1" collapsed="1"><mxGeometry x="957.7142857142858" y="298.7142857142857" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="588"><box/><mxCell style="whybox" parent="578" vertex="1"><mxGeometry x="1172.7142857142858" y="298.7142857142857" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="590"><box><text value="visit archives"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="578" vertex="1" collapsed="1"><mxGeometry x="1332.7142857142858" y="298.7142857142857" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="592">&#xa;      <box><text value=""/></box><mxCell style="whybox" parent="578" vertex="1"><mxGeometry x="1547.7142857142858" y="298.7142857142857" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="596">&#xa;      <box><text value="authorization granted"/></box><mxCell style="whybox" parent="578" vertex="1"><mxGeometry x="797.7142857142858" y="418.7142857142857" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="599"><box><text value="Compare 2 versions"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="578" vertex="1" collapsed="1"><mxGeometry x="1707.7142857142858" y="298.7142857142857" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="601"><box><text value="Restore previous version"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="578" vertex="1" collapsed="1"><mxGeometry x="1707.7142857142858" y="418.7142857142857" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="603"><mxCell style="whatbox" parent="578" vertex="1" collapsed="1"><mxGeometry x="1707.7142857142858" y="538.7142857142858" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="606"><box><text value="Create &amp; manage landing pages"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="671.047619047619" y="1301.6666666666667" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="636" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="606" source="611" target="635" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="634" style="entryX=0;entryY=0.5;exitX=1;exitY=0.5;" parent="606" source="633" target="611" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="632" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="606" source="630" target="611" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="631" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="606" source="610" target="630" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="610"><box><text value="Register new HTML landing page in the [file registry](613d)"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="606" vertex="1" collapsed="1"><mxGeometry x="108.1904761904762" y="235.80952380952382" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="630">&#xa;      <box><text value="HTML file to be used as a landing pag"/></box><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="342.79047619047617" y="235.80952380952382" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="611"><box><text value="manage landing page"/><responsibility><role/></responsibility></box><attachments><text value="This is the page that user will see when they connect to [workspace-name.justskore.it]&#10; &#10;this page can be&#10;- a html file (stored in the [files registry](613) )&#10;- a page of the workspace ( /workspace or /admin/user-management...)&#10;- a process (/process/abc&#10;- anyother page of the web: will be shown in iFrame"/></attachments><mxCell style="whatbox" parent="606" vertex="1" collapsed="1"><mxGeometry x="510.19047619047626" y="89" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="633">&#xa;      <box><text value="workspace setup with default landing page (&quot;home&quot;)"/></box><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="350.19047619047626" y="89" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="635">&#xa;      <box><text value="right landing page for the users"/></box><mxCell style="whybox" parent="606" vertex="1"><mxGeometry x="725.1904761904763" y="89" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="608"><box/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="886.047619047619" y="1301.6666666666667" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="637"><box><text value="Manage assets (roles, links, files, ...)"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="671.047619047619" y="1453.6666666666667" width="190" height="85" as="geometry"/></mxCell></whatbox><mxCell id="793" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="766" target="782" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="792" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="783" target="791" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="790" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="788" target="786" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="789" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="782" target="788" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="787" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="784" target="786" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="785" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="782" target="784" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="781" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="772" target="780" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="779" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="774" target="778" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="775" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="766" target="774" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="773" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="766" target="772" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="767" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="760" target="766" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="761" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="677" target="760" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="759" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="667" target="689" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="1909" y="491"/><mxPoint x="1909" y="739"/></Array></mxGeometry></mxCell><mxCell id="758" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="743" target="757" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="756" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="743" target="755" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="754" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="689" target="753" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="752" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="749" target="751" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="750" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="738" target="749" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="748" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="745" target="747" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="746" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="738" target="745" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="744" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="734" target="743" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="742" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="740" target="699" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="741" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="738" target="740" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="739" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="734" target="738" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="735" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="691" target="734" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="692" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="689" target="691" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="690" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="679" target="689" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><stickynote id="669">&#xa;      <box><text value="* what happens when a role is CREATED in the central role management ? &#10;&#10;it must be synced to the local rep&#10;&#10;* what happens when a role is RENAMED  in the central role management&#10;&#10;change must propagate to the LOC rep&#10;&#10;* what happens when a role is DELETED centrally&#10;&#10;1. destructive : role is marked as deleted centrally, info is sent to LOC repositories who will delete it from their map&#10;2. non-destructive: role is marked as deleted centrally, can still be used locally but will not be synced (UI hint in edit mode ?) &#10;&#10;* MERGE two roles&#10;&#10;1. centrally : &#10;1. locally :&#10;&#10;&#10;think about central to workspae and workspace to map"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="637" vertex="1"><mxGeometry x="1320" y="16" width="580" height="389" as="geometry"/></mxCell></stickynote><stickynote id="674">&#xa;      <box><text value="`Role`&#10;&#10;- `value`the role&#10;- `notes`notes&#10;&#10;- `id-local` the id to use within the map&#10;- `id` the id synced with the central repo&#10;- `status-local`: `pendingSync`, `localOnly`, &#10;`{lastSyncDate}`&#10;- `status` : `deleted`, `merged`, `renamed`"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="637" vertex="1"><mxGeometry x="862" y="70" width="422" height="210" as="geometry"/></mxCell></stickynote><mxCell id="681" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="677" target="679" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="680" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="675" target="679" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="678" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="647" target="677" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="673" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="671" target="650" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="672" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="643" target="671" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="668" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="665" target="667" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="666" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="662" target="665" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="664" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="662" target="642" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="663" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="661" target="662" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="651" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="645" target="650" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="648" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="645" target="647" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="646" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="643" target="645" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="644" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="642" target="643" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="642">&#xa;      <box><text value="**new** role added to the process map"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="52" y="699" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="643"><box><text value="Save role to LOC role manager"/><responsibility><role localid="13"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="212" y="699" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="645">&#xa;      <box><text value="`id-local` for the role"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="427" y="699" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="647"><box><text value="Whatbox displayed with `id-local` for role"/><responsibility><role localid="13"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="587" y="699" width="190" height="85" as="geometry"/></mxCell></whatbox><stickynote id="649">&#xa;      <box><text value="LOCAL role manager"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="637" vertex="1"><mxGeometry x="60" y="632" width="863" height="40" as="geometry"/></mxCell></stickynote><whybox id="661">&#xa;      <box><text value="open map, load role manager"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="60" y="452" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="662"><box><text value="Display graph editor to work on the mpa"/><responsibility><role localid="14"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="220" y="452" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="665">&#xa;      <box><text value="role manager to be updated"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="435" y="452" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="667"><box><text value="Sync roles with central rep"/><responsibility><role localid="13"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="595" y="452" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="671">&#xa;      <box><text value="`pendingSync` status"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="427" y="806" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="677">&#xa;      <box><text value="role can be used in the map"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="802" y="699" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="679"><box><text value="update role with `id` and status to `lastSyncDate`"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="1712" y="699" width="190" height="85" as="geometry"/></mxCell></whatbox><stickynote id="641">&#xa;      <box><text value="## assets manager&#10;&#10;### case study roles"/></box><mxCell style="stickynote;fillColor=none;gradientColor=none;" parent="637" vertex="1"><mxGeometry x="46" y="70" width="394" height="124" as="geometry"/></mxCell></stickynote><whybox id="689">&#xa;      <box><text value="role has been changed on server"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1927" y="699" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="691"><box><text value="add `renamed`, `deleted` or `merged` to roles manager and mark as `localOnly`"/><responsibility><role localid="13"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="2087" y="699" width="190" height="101" as="geometry"/></mxCell></whatbox><whybox id="734">&#xa;      <box><text value="user to take action"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="2302" y="699" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="738"><box><text value="Confirm change"/><responsibility><role localid="16"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="2462" y="699" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="740">&#xa;      <box><text value="confirmed `renamed`"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="2677" y="699" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="745">&#xa;      <box><text value="confirm `deleted`"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="2677" y="819" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="747"><box><text value="Remove the role from the map"/><responsibility><role localid="13"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="2837" y="819" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="699"><box><text value="Apply role name change to map"/><responsibility><role localid="14"/></responsibility><responsibility><role localid="13"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="2837" y="699" width="190" height="115" as="geometry"/></mxCell></whatbox><whatbox id="743"><box><text value="Deny change"/><responsibility><role localid="16"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="2468" y="1079" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="749">&#xa;      <box><text value="confirm merged"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="2677" y="939" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="751"><box><text value="Apply merge to local role manager and apply to graph"/><responsibility><role localid="13"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="2837" y="939" width="190" height="85" as="geometry"/></mxCell></whatbox><whatbox id="753"><box><text value="Color the resources differently"/><responsibility><role localid="14"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="2087" y="823.5" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="755">&#xa;      <box><text value="changes to be kept `localOnly`"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="2683" y="1079" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="757">&#xa;      <box><text value="changes to be restored on `central` roles manager"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="2683" y="1199" width="120" height="80" as="geometry"/></mxCell></whybox><mxCell id="654" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="650" target="653" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="760"><box><text value="Rename, delete or merge role locally"/><responsibility><role localid="16"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="962" y="819" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="766">&#xa;      <box><text value="role changed locally"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1177" y="819" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="772"><box><text value="Update graph"/><responsibility><role localid="14"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="1338" y="819" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="676" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="659" target="675" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="660" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="657" target="659" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="658" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="655" target="657" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="656" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="637" source="653" target="655" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whybox id="653">&#xa;      <box><text value="role description sent"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="810" y="1432" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="655"><box><text value="(if not existing) Add role to central role manager"/><responsibility><role localid="12"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="970" y="1432" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="657">&#xa;      <box><text value="role added with unique `id`"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1185" y="1432" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="659"><box><text value="send `id` back to map"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="1345" y="1432" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="675">&#xa;      <box><text value="`id` received"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1560" y="1432" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="650"><box><text value="Sync new role to central role manager"/><responsibility><role localid="11"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="595" y="1432" width="190" height="85" as="geometry"/></mxCell></whatbox><stickynote id="652">&#xa;      <box><text value="BACKEND loop"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="637" vertex="1"><mxGeometry x="60" y="1179" width="863" height="40" as="geometry"/></mxCell></stickynote><whatbox id="774"><box><text value="delete role from map and remove from loc role manager"/><responsibility><role localid="13"/></responsibility><responsibility><role localid="14"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="1337" y="939" width="190" height="131" as="geometry"/></mxCell></whatbox><whybox id="778">&#xa;      <box><text value="role removed from existing whatbox (but can be re-added later by the user!)"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1552" y="939" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="780"><box/><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1553" y="819" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="782"><box><text value="Rename role in map"/><responsibility><role localid="13"/></responsibility><responsibility><role localid="14"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="1338" y="1069" width="190" height="105" as="geometry"/></mxCell></whatbox><whybox id="784">&#xa;      <box><text value="change to be synced"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1553" y="1069" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="783"><box><text value="Merge role (locally)"/><responsibility><role localid="13"/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="1338" y="1273" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="786"><box><text value="Create new role and mark original as deleted"/><responsibility><role/></responsibility></box><mxCell style="whatbox" parent="637" vertex="1" collapsed="1"><mxGeometry x="1713" y="1069" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="791">&#xa;      <box><text value="if local change, do nothing centrally"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1553" y="1273" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="788">&#xa;      <box><text value="new role name is given new `id-local` and will be synced"/></box><mxCell style="whybox" parent="637" vertex="1"><mxGeometry x="1553" y="1172" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="639"><box/><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="886.047619047619" y="1453.6666666666667" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="816">&#xa;      <box><text value="process up to date"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1236" y="1042" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="818"><box><text value="Publish a process"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="1396" y="1042" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="1084" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" edge="1" parent="818" source="1083" target="1077"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1078" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" edge="1" parent="818" source="1071" target="1077"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1076" style="exitX=1;exitY=0.5;entryY=0.5;entryX=0" edge="1" parent="818" source="1073" target="1071"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1075" style="exitX=1;exitY=0.5;entryY=0.5;entryX=0" edge="1" parent="818" source="1070" target="1071"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1074" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" edge="1" parent="818" source="1069" target="1073"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="1072" style="exitX=1;exitY=0.5;entryY=0.5;entryX=0" edge="1" parent="818" source="1069" target="1070"><mxGeometry relative="1" as="geometry"/></mxCell><stickynote id="829">&#xa;      <box><text value="Jobs to be done&#10;&#10;*"/></box><mxCell style="stickynote;fillColor=transparent;gradientColor=none;" parent="818" vertex="1"><mxGeometry x="105" y="97" width="190" height="80" as="geometry"/></mxCell></stickynote><whatbox id="1070"><box><text value="Click publish"/><responsibility/></box><mxCell style="whatbox" vertex="1" parent="818"><mxGeometry x="226" y="351" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="1069"><box><text value="ready to gain approval"/></box><mxCell style="whybox" vertex="1" parent="818"><mxGeometry x="66" y="402" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="1073"><box/><mxCell style="whatbox" vertex="1" parent="818"><mxGeometry x="226" y="492" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="1077"><box/><mxCell style="whatbox" vertex="1" parent="818"><mxGeometry x="654" y="414" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="1079"><box/><mxCell style="whybox" vertex="1" parent="818"><mxGeometry x="869" y="414" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="1080"><box/><mxCell style="whatbox" vertex="1" parent="818"><mxGeometry x="1029" y="414" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="1083"><box/><mxCell style="whybox" vertex="1" parent="818"><mxGeometry x="494" y="604" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="1071"><box/><mxCell style="whybox" vertex="1" parent="818"><mxGeometry x="494" y="431" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="820">&#xa;      <box><text value="trusted content visible by the right audience"/></box><mxCell style="whybox" parent="1" vertex="1"><mxGeometry x="1611" y="1042" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="890"><box><text value="Comment on a process"/><responsibility><role/></responsibility></box><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="1135" y="1240" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="891" style="wy2s;exitX=0.5;exitY=1;entryY=0.5;entryX=0" parent="890" source="925" target="965" edge="1"><mxGeometry x="488" y="337" as="geometry"/></mxCell><mxCell id="892" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="963" target="927" edge="1"><mxGeometry x="273" y="771" as="geometry"/></mxCell><mxCell id="893" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="962" target="963" edge="1"><mxGeometry x="982" y="771" as="geometry"/></mxCell><mxCell id="894" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="921" target="962" edge="1"><mxGeometry x="748" y="767" as="geometry"/></mxCell><mxCell id="895" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="960" target="961" edge="1"><mxGeometry x="618" y="1103" as="geometry"/></mxCell><mxCell id="896" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="927" target="960" edge="1"><mxGeometry x="473" y="1103" as="geometry"/></mxCell><mxCell id="897" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="926" target="915" edge="1"><mxGeometry x="728" y="294" as="geometry"/></mxCell><mxCell id="898" style="exitX=1;exitY=0.5;entryY=0.5;entryX=0" parent="890" source="914" target="925" edge="1"><mxGeometry x="359" y="294" as="geometry"/></mxCell><mxCell id="899" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="925" target="926" edge="1"><mxGeometry x="583" y="297" as="geometry"/></mxCell><mxCell id="900" style="entryX=0;entryY=0.5;exitX=1;exitY=0.5;" parent="890" source="924" target="914" edge="1"><mxGeometry x="214" y="294" as="geometry"/></mxCell><mxCell id="901" style="wy2s;exitX=0.5;exitY=1;entryY=0.5;entryX=0" parent="890" source="915" target="917" edge="1"><mxGeometry x="853" y="339" as="geometry"/></mxCell><mxCell id="902" style="wy2s;exitX=0.5;exitY=1;entryX=0;entryY=0.5;" parent="890" source="920" target="923" edge="1"><mxGeometry x="508" y="807" as="geometry"/></mxCell><mxCell id="903" style="wy2s;exitX=0.5;exitY=1;entryY=0.5;entryX=0" parent="890" source="918" target="922" edge="1"><mxGeometry x="133" y="807" as="geometry"/></mxCell><mxCell id="904" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="920" target="921" edge="1"><mxGeometry x="603" y="767" as="geometry"/></mxCell><mxCell id="905" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="919" target="920" edge="1"><mxGeometry x="373" y="767" as="geometry"/></mxCell><mxCell id="906" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="918" target="919" edge="1"><mxGeometry x="228" y="767" as="geometry"/></mxCell><mxCell id="907" style="exitX=0.5;exitY=1;entryX=0.5;entryY=0;" parent="890" source="910" target="918" edge="1"><mxGeometry x="133" y="702" as="geometry"/></mxCell><mxCell id="908" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="916" target="918" edge="1"><mxGeometry x="28" y="294" as="geometry"/></mxCell><mxCell id="909" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="890" source="915" target="916" edge="1"><mxGeometry x="948" y="294" as="geometry"/></mxCell><whybox id="910"><box><text value="I have been asked to comment"/></box><mxCell style="whybox" parent="890" vertex="1"><mxGeometry x="73" y="622" width="120" height="80" as="geometry"/></mxCell></whybox><group id="911"><box><text value="not relevant now"/></box><mxCell style="group" parent="890" vertex="1" connectable="0"><mxGeometry x="601" y="56" width="150" height="110" as="geometry"/></mxCell></group><whybox id="912"><box><text value="I find something to comment on and want to tell the author"/></box><mxCell style="whybox" parent="911" vertex="1"><mxGeometry x="15" y="15" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="913"><box><text value="#### scenario: &#10;process is being developed and we need input from one or more person"/></box><mxCell style="stickynote;fillColor=transparent;gradientColor=none;" parent="890" vertex="1"><mxGeometry x="29" y="49" width="728" height="110" as="geometry"/></mxCell></stickynote><whybox id="914"><box><text value="I need comments on my process"/></box><mxCell style="whybox" parent="890" vertex="1"><mxGeometry x="239" y="254" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="915"><box><text value="Send email with URL to commenters"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatbox" parent="890" vertex="1" collapsed="1"><mxGeometry x="758" y="254" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="916"><box><text value="commenters receive a link to the process version to comment on"/></box><mxCell style="whybox" parent="890" vertex="1"><mxGeometry x="973" y="254" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="917"><box><text value="the feature is &quot;send notification to people who can access th process&quot;&#10;&#10;with warning for &quot;everyone&quot;"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="890" vertex="1"><mxGeometry x="894" y="380" width="190" height="89" as="geometry"/></mxCell></stickynote><whatbox id="918"><box><text value="add comment to the process"/><responsibility><role localid="19"/></responsibility></box><mxCell style="whatbox" parent="890" vertex="1" collapsed="1"><mxGeometry x="38" y="727" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="919"><box><text value="all comments/ requests for change added"/></box><mxCell style="whybox" parent="890" vertex="1"><mxGeometry x="253" y="727" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="920"><box><text value="review and act on comments"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatbox" parent="890" vertex="1" collapsed="1"><mxGeometry x="413" y="727" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="921"><box><text value="action taken, ready for approval / publication"/></box><mxCell style="whybox" parent="890" vertex="1"><mxGeometry x="628" y="727" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="922"><box><text value="commenter can delete its own comments"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="890" vertex="1"><mxGeometry x="158" y="846" width="190" height="80" as="geometry"/></mxCell></stickynote><stickynote id="923"><box><text value="editor &#10;* can delete a comments&#10;* mark it as &quot;done&quot; in the DB --&gt; disappears from the graph (will be retrievable in a UI... tdb)"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="890" vertex="1"><mxGeometry x="533" y="846" width="240" height="128" as="geometry"/></mxCell></stickynote><whatbox id="924"><box><text value="Develop a process"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatbox" parent="890" vertex="1" collapsed="1"><mxGeometry x="24" y="254" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="925"><box><text value="Turn on COMMENT MODE"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatbox" parent="890" vertex="1" collapsed="1"><mxGeometry x="393" y="257" width="190" height="80" as="geometry"/></mxCell></whatbox><whybox id="926"><box><text value="process will accept comments"/></box><mxCell style="whybox" parent="890" vertex="1"><mxGeometry x="608" y="257" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="927"><box><text value="Gain approval of a process"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatboxdetailed" parent="890" vertex="1" collapsed="1"><mxGeometry x="283" y="1063" width="190" height="80" as="geometry"/></mxCell></whatbox><mxCell id="928" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="958" target="959" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="929" style="entryX=0;entryY=0.5;exitX=1;exitY=0.5;" parent="927" source="952" target="953" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="930" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="953" target="949" edge="1"><mxGeometry relative="1" as="geometry"><Array as="points"><mxPoint x="794" y="580" DONOTDISPLAY="1"/><mxPoint x="794" y="676" DONOTDISPLAY="1"/><mxPoint x="424" y="676" DONOTDISPLAY="1"/><mxPoint x="424" y="385" DONOTDISPLAY="1"/></Array></mxGeometry></mxCell><mxCell id="931" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="949" target="953" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="932" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="946" target="950" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="933" style="exitX=1;exitY=0.5;entryY=0.5;entryX=0" parent="927" source="955" target="957" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="934" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="954" target="955" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="935" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="956" target="954" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="936" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="949" target="956" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="937" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="957" target="958" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="938" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="948" target="957" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="939" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="949" target="948" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="940" style="wy2s;exitX=0.5;exitY=1;entryY=0.5;entryX=0" parent="927" source="946" target="951" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="941" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="946" target="949" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="942" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="947" target="946" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="943" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="945" target="959" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><mxCell id="944" style="exitX=1;exitY=0.5;entryX=0;entryY=0.5;" parent="927" source="947" target="945" edge="1"><mxGeometry relative="1" as="geometry"/></mxCell><whatbox id="945"><box><text value="click publish"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatbox" parent="927" vertex="1" collapsed="1"><mxGeometry x="219" y="94" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="946"><box><text value="open approval modal and add an approver + a comment"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatbox" parent="927" vertex="1" collapsed="1"><mxGeometry x="219" y="294" width="190" height="101" as="geometry"/></mxCell></whatbox><whybox id="947"><box><text value="ready to gain approval"/></box><mxCell style="whybox" parent="927" vertex="1"><mxGeometry x="59" y="207" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="948"><box><text value="click on link in the email to open the process"/><responsibility><role localid="20"/></responsibility></box><mxCell style="whatbox" parent="927" vertex="1" collapsed="1"><mxGeometry x="594" y="247" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="949"><box><text value="email sent to approver"/></box><mxCell style="whybox" parent="927" vertex="1"><mxGeometry x="434" y="345" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="950"><box><text value="flag process &quot;awaiting approval&quot;, only the EDITORS can see"/></box><mxCell style="whybox" parent="927" vertex="1"><mxGeometry x="434" y="214" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="951"><box><text value="default email to be designed in the admin interface"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="927" vertex="1"><mxGeometry x="181" y="500" width="190" height="80" as="geometry"/></mxCell></stickynote><whybox id="952"><box><text value="approver has &quot;lost&quot; the email or forgotten to act"/></box><mxCell style="whybox" parent="927" vertex="1"><mxGeometry x="434" y="540" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="953"><box><text value="re-send approval email from the same modal to remind the approver"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatbox" parent="927" vertex="1" collapsed="1"><mxGeometry x="594" y="540" width="190" height="101" as="geometry"/></mxCell></whatbox><whybox id="954"><box><text value="in the right workspace"/></box><mxCell style="whybox" parent="927" vertex="1"><mxGeometry x="839" y="395" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="955"><box><text value="Access process to be approved from the &quot;pinned&quot; area above the complete process list"/><responsibility><role localid="20"/></responsibility></box><mxCell style="whatbox" parent="927" vertex="1" collapsed="1"><mxGeometry x="1014" y="395" width="190" height="117" as="geometry"/></mxCell></whatbox><whatbox id="956"><box><text value="Log in to the workspace (not a specific process)"/><responsibility><role localid="20"/></responsibility></box><mxCell style="whatbox" parent="927" vertex="1" collapsed="1"><mxGeometry x="594" y="395" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="957"><box><text value="logged in and see the right process"/></box><mxCell style="whybox" parent="927" vertex="1"><mxGeometry x="1239" y="247" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="958"><box><text value="Click on &quot;approve&quot; button in the processUI"/><responsibility><role localid="20"/></responsibility></box><mxCell style="whatbox" parent="927" vertex="1" collapsed="1"><mxGeometry x="1391" y="247" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="959"><box><text value="published"/></box><mxCell style="whybox" parent="927" vertex="1"><mxGeometry x="1608" y="94" width="120" height="80" as="geometry"/></mxCell></whybox><whybox id="960"><box><text value="new version is published"/></box><mxCell style="whybox" parent="890" vertex="1"><mxGeometry x="498" y="1063" width="120" height="80" as="geometry"/></mxCell></whybox><whatbox id="961"><box><text value="Approve"/><responsibility><role localid="20"/></responsibility></box><mxCell style="whatbox" parent="890" vertex="1" collapsed="1"><mxGeometry x="658" y="1063" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="962"><box><text value="STOP the COMMENTING mode"/><responsibility><role localid="18"/></responsibility></box><mxCell style="whatbox" parent="890" vertex="1" collapsed="1"><mxGeometry x="792" y="731" width="190" height="85" as="geometry"/></mxCell></whatbox><whybox id="963"><box/><mxCell style="whybox" parent="890" vertex="1"><mxGeometry x="1007" y="731" width="120" height="80" as="geometry"/></mxCell></whybox><stickynote id="964"><box/><mxCell style="stickynote" parent="890" vertex="1"><mxGeometry x="458" y="1192" width="190" height="80" as="geometry"/></mxCell></stickynote><stickynote id="965"><box><text value="comment mode is meant to be enabled for a short period of time&#10;&#10;when enabled&#10;&#10;* &quot;add comment&quot; button and UI to add/remove comments&#10;&#10;when disabled&#10;&#10;* no UI for commenting&#10;* non-deleted comments are saved and will still be available (regardless which version of the process)"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="890" vertex="1"><mxGeometry x="535" y="366" width="325" height="211" as="geometry"/></mxCell></stickynote></root></mxGraphModel>';});


define('text!demoContent/skNavigationTest.skore',[],function () { return '<mxGraphModel dx="977" dy="516" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="0" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" background="#ffffff" skoreFileSpec="2"><root><mxCell id="0"><roles as="roles"><role xmlns="http://www.w3.org/1999/xhtml" value="box 23" localid="1"></role><role xmlns="http://www.w3.org/1999/xhtml" value="and more playground material" localid="2"></role><role xmlns="http://www.w3.org/1999/xhtml" value="10" localid="3"></role></roles><stylesheets as="stylesheets">&#xa;  <stylesheet value="default">&#xa;    <style value="whatbox" styleTokens="rounded=1;"/>&#xa;    <style value="whybox" styleTokens="undefined"/>&#xa;    <style value="line" styleTokens="undefined"/>&#xa;    <style value="stickynote" styleTokens="undefined"/>&#xa;    <style value="group" styleTokens="undefined"/>&#xa;    <style value="wy2s" styleTokens="undefined"/>&#xa;  </stylesheet>&#xa;</stylesheets><templates as="templates"/></mxCell><title id="1"><box><text value="navigation test"/></box><mxCell style="title" parent="0"/></title><whatbox id="7"><box><text value="1. go in the detail view"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="120" y="114" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="8"><box><text value="click back again&#10;&#10;should go to top"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatbox" parent="7" vertex="1" collapsed="1"><mxGeometry x="165" y="121" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="9"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role localid="2"/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="437" y="137" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="10"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role localid="3"/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="9" vertex="1" collapsed="1"><mxGeometry x="447" y="147" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="11"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="10" vertex="1" collapsed="1"><mxGeometry x="457" y="157" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="12"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="11" vertex="1" collapsed="1"><mxGeometry x="467" y="167" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="13"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="12" vertex="1" collapsed="1"><mxGeometry x="447" y="147" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="14"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="13" vertex="1" collapsed="1"><mxGeometry x="447" y="147" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="15"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="14" vertex="1" collapsed="1"><mxGeometry x="457" y="157" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="16"><box><text value="End !"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatbox" parent="15" vertex="1" collapsed="1"><mxGeometry x="467" y="167" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="20"><box><text value="Go to [box 19](19) somewhere in the middle"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatbox" parent="15" vertex="1" collapsed="1"><mxGeometry x="467" y="285" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="19"><box><text value="Here"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatbox" parent="12" vertex="1" collapsed="1"><mxGeometry x="343" y="300" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="22"><box><text value="click back now ? should go back to bottom"/></box><mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="12" vertex="1"><mxGeometry x="580" y="300" width="190" height="80" as="geometry"/></mxCell></stickynote><whatbox id="37"><box><text value="[go to 7 on top level](7)"/></box><responsibilities><responsibility displayed="1"/></responsibilities><mxCell style="whatbox" vertex="1" parent="10" collapsed="1"><mxGeometry x="158" y="184" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="39"><box><text value="[link box 10 same level](10)"/></box><responsibilities><responsibility displayed="1"/></responsibilities><mxCell style="whatbox" vertex="1" parent="9" collapsed="1"><mxGeometry x="710" y="323" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="17"><box><text value="Go to [box 16](16) at the bottom of the hierarchy"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="120" y="240" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="23"><box><text value="Deep 2"/></box><responsibilities><responsibility displayed="1"><role localid="1"/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="1" vertex="1" collapsed="1"><mxGeometry x="437" y="233" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="24"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="23" vertex="1" collapsed="1"><mxGeometry x="447" y="147" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="25"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="24" vertex="1" collapsed="1"><mxGeometry x="457" y="157" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="26"><box><text value="Deep"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatboxdetailed" parent="25" vertex="1" collapsed="1"><mxGeometry x="467" y="167" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="34"><box><text value="Found me !"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatbox" parent="26" vertex="1" collapsed="1"><mxGeometry x="238" y="229" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="36"><box><text value="Click back and forth and navigate"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatbox" parent="26" vertex="1" collapsed="1"><mxGeometry x="238" y="334" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="35"><box><text value="Search for box `34`"/></box><responsibilities><responsibility displayed="1"><role/></responsibility></responsibilities><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="120" y="340" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="38"><box><text value="[link on same level to box 23](23)"/></box><responsibilities><responsibility displayed="1"/></responsibilities><mxCell style="whatbox" vertex="1" parent="1" collapsed="1"><mxGeometry x="682" y="396" width="190" height="80" as="geometry"/></mxCell></whatbox></root></mxGraphModel>';});


define('text!demoContent/RACIFileConversion.skore',[],function () { return '<mxGraphModel dx="888" dy="615" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="0" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" skoreFileSpec="2"><root><mxCell id="0"><roles as="roles"><role value="Colin"/><role value="Cathy"/></roles><stylesheets as="stylesheets">&#xa;  <stylesheet value="default">&#xa;    <style value="whatbox" styleTokens="rounded=1;"/>&#xa;  </stylesheet>&#xa;</stylesheets><extension value="mrt" type="RACI" as="tagsInUse"><tag value="R" name="Responsible"/><tag value="A" name="Accountable"/><tag value="C" name="Consulted"/><tag value="I" name="Informed"/></extension></mxCell><title id="1"><text value="Name your Skore"/><mxCell parent="0"/></title><whatbox id="2"><text value="1 role, no RACI"/><resource value="Colin"/><mxCell style="whatbox" vertex="1" parent="1" collapsed="1"><mxGeometry x="96" y="137" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="3"><text value="2 roles, no RACI"/><resource order="0" value="Colin"/><resource value="Cathy"/><mxCell style="whatbox" vertex="1" parent="1" collapsed="1"><mxGeometry x="330" y="137" width="190" height="105" as="geometry"/></mxCell></whatbox><whatbox id="4"><text value="1 role with full RACI"/><resource order="0" value="Colin"><tag value="R"/><tag value="A"/><tag value="C"/><tag value="I"/></resource><mxCell style="whatbox" vertex="1" parent="1" collapsed="1"><mxGeometry x="96" y="306" width="190" height="80" as="geometry"/></mxCell></whatbox><stickynote id="5"><text value="## RACI"/><mxCell style="stickynote;fillColor=transparent;gradientColor=none;" vertex="1" parent="1"><mxGeometry x="68" y="23" width="190" height="80" as="geometry"/></mxCell></stickynote><whatbox id="6"><text value="2 roles with full RACI"/><resource order="0" value="Colin"><tag value="R"/><tag value="A"/><tag value="C"/><tag value="I"/></resource><resource value="Cathy"><tag value="R"/><tag value="A"/><tag value="C"/><tag value="I"/></resource><mxCell style="whatbox" vertex="1" collapsed="1" parent="1"><mxGeometry x="330" y="306" width="190" height="105" as="geometry"/></mxCell></whatbox></root></mxGraphModel>';});


define('text!demoContent/RATSIFileConversion.skore',[],function () { return '<mxGraphModel dx="888" dy="615" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="0" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" skoreFileSpec="2"><root><mxCell id="0"><roles as="roles"><role value="Colin"/><role value="Cathy"/></roles><stylesheets as="stylesheets">&#xa;  <stylesheet value="default">&#xa;    <style value="whatbox" styleTokens="rounded=1;"/>&#xa;  </stylesheet>&#xa;</stylesheets><extension value="mrt" type="RATSI" as="tagsInUse"><tag value="R" style="background-color:rgba(255, 165, 0, 0.5)" name="Responsible"/><tag value="A" style="background-color:rgba(255, 0, 0, 0.5)" name="Authority"/><tag value="T" style="background-color:rgba(245, 245, 220, 0.5)" name="Task"/><tag value="S" name="Support"/><tag value="I" name="Informed"/></extension></mxCell><title id="1"><text value="Name your Skore"/><mxCell parent="0"/></title><stickynote id="2"><text value="## RATSI"/><mxCell style="stickynote;fillColor=transparent;gradientColor=none;" vertex="1" parent="1"><mxGeometry x="68" y="23" width="190" height="80" as="geometry"/></mxCell></stickynote><whatbox id="3"><text value="1 role no RATSI"/><resource order="0" value="Colin"/><mxCell style="whatbox" vertex="1" parent="1" collapsed="1"><mxGeometry x="103" y="144" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="4"><text value="2 roles no RATSI"/><resource order="0" value="Colin"/><resource value="Cathy"/><mxCell style="whatbox" vertex="1" collapsed="1" parent="1"><mxGeometry x="363" y="144" width="190" height="105" as="geometry"/></mxCell></whatbox><whatbox id="5"><text value="1 role full RATSI"/><resource order="0" value="Colin"><tag value="R"/><tag value="A"/><tag value="T"/><tag value="S"/><tag value="I"/></resource><mxCell style="whatbox" vertex="1" collapsed="1" parent="1"><mxGeometry x="103" y="323" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="6"><text value="2 roles full RATSI"/><resource order="0" value="Colin"><tag value="R"/><tag value="A"/><tag value="T"/><tag value="S"/><tag value="I"/></resource><resource value="Cathy"><tag value="R"/><tag value="A"/><tag value="T"/><tag value="S"/><tag value="I"/></resource><mxCell style="whatbox" vertex="1" collapsed="1" parent="1"><mxGeometry x="363" y="323" width="190" height="105" as="geometry"/></mxCell></whatbox></root></mxGraphModel>';});


define('text!demoContent/CustomFileConversion.skore',[],function () { return '<mxGraphModel dx="888" dy="615" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="0" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" skoreFileSpec="2"><root><mxCell id="0"><roles as="roles"><role value="Colin"/><role value="Cathy"/><role value="Hello"/></roles><stylesheets as="stylesheets">&#xa;  <stylesheet value="default">&#xa;    <style value="whatbox" styleTokens="rounded=1;"/>&#xa;  </stylesheet>&#xa;</stylesheets><extension value="mrt" type="custom" as="tagsInUse"><tag value="Sys" name="System" style="background-color:rgba(245, 245, 220, 0.5)"/><tag value="Doc" name="Document" style="background-color:rgba(255, 165, 0, 0.5)"/></extension></mxCell><title id="1"><text value="Name your Skore"/><mxCell parent="0"/></title><stickynote id="2"><text value="## custom MRT conversion test"/><mxCell style="stickynote;fillColor=transparent;gradientColor=none;" vertex="1" parent="1"><mxGeometry x="107" y="46" width="342" height="78" as="geometry"/></mxCell></stickynote><whatbox id="3"><text value="1 role nothing"/><resource order="0" value="Colin"/><mxCell style="whatbox" vertex="1" parent="1" collapsed="1"><mxGeometry x="93" y="224" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="4"><text value="2 role no selection"/><resource order="0" value="Colin"/><resource value="Cathy"/><mxCell style="whatbox" vertex="1" collapsed="1" parent="1"><mxGeometry x="354" y="218" width="190" height="105" as="geometry"/></mxCell></whatbox><whatbox id="5"><text value="1 role SYS + DOC"/><resource order="0" value="Colin"><tag value="Sys"/><tag value="Doc"/></resource><mxCell style="whatbox" vertex="1" collapsed="1" parent="1"><mxGeometry x="93" y="391" width="190" height="80" as="geometry"/></mxCell></whatbox><whatbox id="6"><text value="2 role  SYS + DOC"/><resource order="0" value="Colin"><tag value="Sys"/><tag value="Doc"/></resource><resource value="Cathy"><tag value="Sys"/><tag value="Doc"/></resource><mxCell style="whatbox" vertex="1" collapsed="1" parent="1"><mxGeometry x="364" y="401" width="190" height="105" as="geometry"/></mxCell></whatbox><whatbox id="7"><text value="1role nothing&#10;&#10;2nd role SYS+DOC&#10;&#10;3rd nothig"/><resource order="0" value="Colin"/><resource value="Cathy"><tag value="Sys"/><tag value="Doc"/></resource><resource value="Hello"/><mxCell style="whatbox" vertex="1" collapsed="1" parent="1"><mxGeometry x="601" y="401" width="190" height="182" as="geometry"/></mxCell></whatbox></root></mxGraphModel>';});


define('text!demoContent/test-search.skore',[],function () { return '<mxGraphModel dx="1056" dy="487" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="0" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" background="#ffffff">\n  <root>\n    <mxCell id="0">\n      <forms as="forms">\n        <form value="whatbox" name="multirolesratsi"/>\n      </forms>\n      <roles as="roles">\n        <role value="test" localid="1"/>\n        <role value="cat" localid="2"/>\n        <role value="test cat" localid="3"/>\n        <role value="e" localid="4"/>\n        <role value="tiger" localid="5"/>\n        <role value="t" localid="6"/>\n        <role value="te" localid="7"/>\n      </roles>\n      <stylesheets as="stylesheets">\n        <stylesheet value="default"/>\n      </stylesheets>\n    </mxCell>\n    <title id="1">\n      <box>\n        <text/>\n      </box>\n      <mxCell style="title" parent="0"/>\n    </title>\n    <whatbox id="2">\n      <box>\n        <text value="test"/>\n        <responsibility>\n          <role localid="1"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" parent="1" vertex="1" collapsed="1">\n        <mxGeometry x="182" y="44" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whybox id="4">\n      <box>\n        <text value="test"/>\n      </box>\n      <mxCell style="whybox" parent="1" vertex="1">\n        <mxGeometry x="412" y="44" width="120" height="80" as="geometry"/>\n      </mxCell>\n    </whybox>\n    <stickynote id="5">\n      <box>\n        <text value="test"/>\n      </box>\n      <mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1">\n        <mxGeometry x="571" y="44" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </stickynote>\n    <whatbox id="6">\n      <box>\n        <text value="zebra"/>\n        <responsibility>\n          <role localid="2"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" parent="1" vertex="1" collapsed="1">\n        <mxGeometry x="182" y="141" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whybox id="7">\n      <box>\n        <text value="lion"/>\n      </box>\n      <mxCell style="whybox" parent="1" vertex="1">\n        <mxGeometry x="412" y="141" width="120" height="80" as="geometry"/>\n      </mxCell>\n    </whybox>\n    <stickynote id="8">\n      <box>\n        <text value="tiger"/>\n      </box>\n      <mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1">\n        <mxGeometry x="571" y="141" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </stickynote>\n    <whatbox id="9">\n      <box>\n        <text value="test zebra"/>\n        <responsibility>\n          <role localid="3"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" parent="1" vertex="1" collapsed="1">\n        <mxGeometry x="182" y="238" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whybox id="10">\n      <box>\n        <text value="test lion"/>\n      </box>\n      <mxCell style="whybox" parent="1" vertex="1">\n        <mxGeometry x="412" y="238" width="120" height="80" as="geometry"/>\n      </mxCell>\n    </whybox>\n    <stickynote id="11">\n      <box>\n        <text value="test tiger"/>\n      </box>\n      <mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" parent="1" vertex="1">\n        <mxGeometry x="571" y="238" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </stickynote>\n    <whatbox id="12">\n      <box>\n        <text value="test"/>\n        <responsibility>\n          <role localid="1"/>\n        </responsibility>\n        <responsibility>\n          <role localid="1"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" parent="1" vertex="1" collapsed="1">\n        <mxGeometry x="182" y="355" width="190" height="98" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whatbox id="13">\n      <box>\n        <responsibility>\n          <role localid="2"/>\n        </responsibility>\n        <responsibility>\n          <role localid="5"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" parent="1" vertex="1" collapsed="1">\n        <mxGeometry x="187" y="474" width="190" height="98" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whybox id="14">\n      <box>\n        <text value="tiger"/>\n      </box>\n      <mxCell style="whybox" parent="1" vertex="1">\n        <mxGeometry x="412" y="355" width="120" height="80" as="geometry"/>\n      </mxCell>\n    </whybox>\n    <whatbox id="15">\n      <box>\n        <text value="same in drill down&#xa;"/>\n      </box>\n      <mxCell style="whatboxdetailed" vertex="1" parent="1" collapsed="1">\n        <mxGeometry x="819" y="58" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whatbox id="16">\n      <box>\n        <text value="test"/>\n        <responsibility>\n          <role localid="1"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" vertex="1" collapsed="1" parent="15">\n        <mxGeometry x="192" y="54" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whybox id="17">\n      <box>\n        <text value="test"/>\n      </box>\n      <mxCell style="whybox" vertex="1" parent="15">\n        <mxGeometry x="422" y="54" width="120" height="80" as="geometry"/>\n      </mxCell>\n    </whybox>\n    <stickynote id="18">\n      <box>\n        <text value="test"/>\n      </box>\n      <mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" vertex="1" parent="15">\n        <mxGeometry x="581" y="54" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </stickynote>\n    <whatbox id="19">\n      <box>\n        <text value="zebra"/>\n        <responsibility>\n          <role localid="2"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" vertex="1" collapsed="1" parent="15">\n        <mxGeometry x="192" y="151" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whybox id="20">\n      <box>\n        <text value="lion"/>\n      </box>\n      <mxCell style="whybox" vertex="1" parent="15">\n        <mxGeometry x="422" y="151" width="120" height="80" as="geometry"/>\n      </mxCell>\n    </whybox>\n    <stickynote id="21">\n      <box>\n        <text value="tiger"/>\n      </box>\n      <mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" vertex="1" parent="15">\n        <mxGeometry x="581" y="151" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </stickynote>\n    <whatbox id="22">\n      <box>\n        <text value="test zebra"/>\n        <responsibility>\n          <role localid="3"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" vertex="1" collapsed="1" parent="15">\n        <mxGeometry x="192" y="248" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whybox id="23">\n      <box>\n        <text value="test lion"/>\n      </box>\n      <mxCell style="whybox" vertex="1" parent="15">\n        <mxGeometry x="422" y="248" width="120" height="80" as="geometry"/>\n      </mxCell>\n    </whybox>\n    <stickynote id="24">\n      <box>\n        <text value="test tiger"/>\n      </box>\n      <mxCell style="stickynote;fillColor=#FCF393;gradientColor=none;" vertex="1" parent="15">\n        <mxGeometry x="581" y="248" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </stickynote>\n    <whatbox id="25">\n      <box>\n        <text value="test"/>\n        <responsibility>\n          <role localid="1"/>\n        </responsibility>\n        <responsibility>\n          <role localid="1"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" vertex="1" collapsed="1" parent="15">\n        <mxGeometry x="192" y="365" width="190" height="98" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whatbox id="26">\n      <box>\n        <responsibility>\n          <role localid="2"/>\n        </responsibility>\n        <responsibility>\n          <role localid="5"/>\n        </responsibility>\n      </box>\n      <mxCell style="whatbox" vertex="1" collapsed="1" parent="15">\n        <mxGeometry x="197" y="484" width="190" height="98" as="geometry"/>\n      </mxCell>\n    </whatbox>\n    <whybox id="27">\n      <box>\n        <text value="tiger"/>\n      </box>\n      <mxCell style="whybox" vertex="1" parent="15">\n        <mxGeometry x="422" y="365" width="120" height="80" as="geometry"/>\n      </mxCell>\n    </whybox>\n    <whatbox id="28">\n      <box>\n        <text value="attachments"/>\n      </box>\n      <attachments>\n        <text value="cat&#xa;&#xa;zebra"/>\n        <text value="tiger"/>\n        <text value="test"/>\n      </attachments>\n      <mxCell style="whatbox" vertex="1" parent="1" collapsed="1">\n        <mxGeometry x="820" y="183" width="190" height="80" as="geometry"/>\n      </mxCell>\n    </whatbox>\n  </root>\n</mxGraphModel>\n';});


define('text!demoContent/attachmentstests.skore',[],function () { return '<mxGraphModel dx="639" dy="538" grid="0" gridSize="10" guides="1" tooltips="0" connect="1" arrows="0" fold="1" page="1" pageScale="1" pageWidth="1169" pageHeight="826" background="#ffffff" skoreFileSpec="2"><root><mxCell id="0"><forms as="forms"/><roles as="roles"><role xmlns="http://www.w3.org/1999/xhtml" value="role of the box" localid="1"></role><role xmlns="http://www.w3.org/1999/xhtml" value="responsability &quot;naked&quot;" localid="2"></role><role xmlns="http://www.w3.org/1999/xhtml" value="responsibility *bold*" localid="3"></role><role xmlns="http://www.w3.org/1999/xhtml" value="responsibility **bold**" localid="4"></role><role xmlns="http://www.w3.org/1999/xhtml" value="responsibility ART" localid="5"></role></roles><stylesheets as="stylesheets">&#xa;  <stylesheet value="default">&#xa;    <style value="whatbox" styleTokens="rounded=1;whoWhatLine=1;"/>&#xa;    <style value="whybox" styleTokens=""/>&#xa;    <style value="line" styleTokens=""/>&#xa;    <style value="stickynote" styleTokens=""/>&#xa;    <style value="group" styleTokens=""/>&#xa;    <style value="wy2s" styleTokens=""/>&#xa;  </stylesheet>&#xa;</stylesheets></mxCell><title id="1"><box><text value="attachments tests"/></box><mxCell style="title" parent="0"/></title><whatbox id="2"><box><text value="this box has all attachments"/><responsibility><role localid="1"/></responsibility></box><attachments><text value="a simple boring text"/><url value="Get skore homepage" addr="https://www.getskore.com"/><responsibility><role localid="2"/></responsibility><responsibility><role localid="4"/></responsibility><responsibility><role localid="5"/><tag value="A"/><tag value="R"/><tag value="T"/></responsibility><checklist value="not checked item" status="0"/><checklist value="not checked item **bold**" status="0"/><checklist value="checked item" status="1"/><text value="a text with **bold** and *italic*&#10;&#10;## a header&#10;&#10;* this&#10;* is &#10;* a &#10;* list&#10;&#10;and a link https://www.getskore.com and a silent link [skore home](https://www.getskore.com)"/><externalcontent value="https://youtu.be/1-2sZAMrWB4" contenttype="youtube"/><externalcontent value="https://vimeo.com/190785902" contenttype="vimeo"/><externalcontent value="http://www.dailymotion.com/video/x51n81s_gopro-view-freeskiing-pristine-alaskan-spines-shades-of-winter-between_sport" contenttype="dailymotion"/><externalcontent value="https://www.getskore.com/wp-content/uploads/2015/08/Skore-app1.png" contenttype="image"/><externalcontent value="&lt;iframe src=&quot;https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21830306.233576037!2d4.1185600427070925!3d48.09417807995644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46ed8886cfadda85%3A0x72ef99e6b3fcf079!2sEurope!5e0!3m2!1sen!2sus!4v1479152926286&quot; width=&quot;600&quot; height=&quot;450&quot; frameborder=&quot;0&quot; style=&quot;border:0&quot; allowfullscreen&gt;&lt;/iframe&gt;" contenttype="googlemaps"/><externalcontent value="&lt;iframe src=&quot;https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21830306.233576037!2d4.1185600427070925!3d48.09417807995644!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46ed8886cfadda85%3A0x72ef99e6b3fcf079!2sEurope!5e0!3m2!1sen!2sus!4v1479152926286&quot; width=&quot;600&quot; height=&quot;450&quot; frameborder=&quot;0&quot; style=&quot;border:0&quot; allowfullscreen&gt;&lt;/iframe&gt;" contenttype="iframe"/></attachments><mxCell style="whatbox" parent="1" vertex="1" collapsed="1"><mxGeometry x="160" y="165" width="190" height="80" as="geometry"/></mxCell></whatbox></root></mxGraphModel>';});

define('skGraphEditorUI/skDemoContentLoader',['require','text!demoContent/EmployeeOnboarding.skore','text!demoContent/training_company_process.skore','text!demoContent/DesignProcess.skore','text!demoContent/TransformationApproach.skore','text!demoContent/npd.skore','text!demoContent/projet-rouge.skore','text!demoContent/fullFeaturesTests.skore','text!demoContent/SkoreOfSkoreApp.skore','text!demoContent/skNavigationTest.skore','text!demoContent/RACIFileConversion.skore','text!demoContent/RATSIFileConversion.skore','text!demoContent/CustomFileConversion.skore','text!demoContent/test-search.skore','text!demoContent/attachmentstests.skore','jquery','skShell'],function(require) {
    "use strict";
    function skDemoContent(navigatorUI) {
        for (var demoContent = [ {
            title: "Employee Onboarding",
            fileName: "EmployeeOnboarding.skore",
            skore: require("text!demoContent/EmployeeOnboarding.skore"),
            description: "HR - getting ready for a new employee arrival",
            isPublic: !0
        }, {
            title: "Training company process",
            fileName: "training_company_process.skore",
            skore: require("text!demoContent/training_company_process.skore"),
            description: "Process for a company delivering training courses",
            isPublic: !0
        }, {
            title: "Design Process",
            fileName: "DesignProcess.skore",
            skore: require("text!demoContent/DesignProcess.skore"),
            description: "Design & build a new feature",
            isPublic: !0
        }, {
            title: "Transformation Approach",
            fileName: "TransformationApproach.skore",
            skore: require("text!demoContent/TransformationApproach.skore"),
            description: "Understand the current situation, define and implement a strategy",
            isPublic: !0
        }, {
            title: "New Product Development",
            fileName: "npd.skore",
            skore: require("text!demoContent/npd.skore"),
            description: "A process from a wikipedia article",
            isPublic: !0
        }, {
            title: "Projet Rouge",
            fileName: "projet-rouge.skore",
            skore: require("text!demoContent/projet-rouge.skore"),
            description: "",
            isPublic: !1
        }, {
            title: "full feature test ",
            fileName: "fullFeaturesTests.skore",
            skore: require("text!demoContent/fullFeaturesTests.skore"),
            description: "",
            isPublic: !1
        }, {
            title: "Skore of Skore app",
            fileName: "SkoreOfSkoreApp.skore",
            skore: require("text!demoContent/SkoreOfSkoreApp.skore"),
            description: "Skore of all user journeys in Skore",
            isPublic: !1
        }, {
            title: "Test navigation",
            fileName: "skNavigationTest.skore",
            skore: require("text!demoContent/skNavigationTest.skore"),
            description: "Test the navigation in skore",
            isPublic: !1
        }, {
            title: "RACI Conversion",
            fileName: "RACIFileConversion.skore",
            skore: require("text!demoContent/RACIFileConversion.skore"),
            description: "Test opening old RACI file",
            isPublic: !1
        }, {
            title: "RATSI Conversion",
            fileName: "RATSIFileConversion.skore",
            skore: require("text!demoContent/RATSIFileConversion.skore"),
            description: "Test opening old RATSI file",
            isPublic: !1
        }, {
            title: "Custom MRT Conversion",
            fileName: "CustomFileConversion.skore",
            skore: require("text!demoContent/CustomFileConversion.skore"),
            description: "Test opening old Custom MRT file",
            isPublic: !1
        }, {
            title: "Test SEARCH",
            fileName: "test-search.skore",
            skore: require("text!demoContent/test-search.skore"),
            description: "Test search feature",
            isPublic: !1
        }, {
            title: "Attachments test",
            fileName: "attachmentstests.skore",
            skore: require("text!demoContent/attachmentstests.skore"),
            description: "Test ATTACHMENTS feature",
            isPublic: !1
        } ], firstExample = $(".skSideMenu_loadExample"), makeLink = function(e) {
            e.isPublic && $("<p><i class='fa fa-map-o'></i> &nbsp;" + e.title + '<span class="explanation">' + e.description + "</span></p>").on("click", function() {
                navigatorUI.graphManager.openGraph({
                    xml: e.skore,
                    source: "demo",
                    filePath: skShell.currentFilePath
                });
            }).insertAfter(firstExample);
        }, i = 0; i < demoContent.length; i++) makeLink(demoContent[i]);
    }
    var $ = require("jquery"), skShell = require("skShell");
    return skDemoContent;
});
define('skUI/skSideMenu',['require','text!./skSideMenu.html','skGraphEditorUI/skDemoContentLoader','jquery','mxUtils','skGraphEditorUI/skGraphEditorTutorial','skShell'],function(require) {
    "use strict";
    function skSideMenu(navigatorUI) {
        this.navigatorUI = navigatorUI;
        var that = this;
        $(document.body).append($(require("text!./skSideMenu.html")));
        var skDemoContent = require("skGraphEditorUI/skDemoContentLoader");
        this.demoContent = new skDemoContent(this.navigatorUI), $("li.parent").on("mouseover", function() {
            var $menuItem = $(this), $submenuWrapper = $("> .wrapper", $menuItem), menuItemPos = $menuItem.position();
            $submenuWrapper.css({
                top: menuItemPos.top,
                left: menuItemPos.left + Math.round(.85 * $menuItem.outerWidth())
            });
        }), $(".skSideMenu_tutorial").on("click", function() {
            skShell.tutorial = new skGraphEditorTutorial(that.navigatorUI);
        }), this.navigatorUI.graphManager.addListener("graphEnabled", mxUtils.bind(this, function(sender, event) {
            var enabled = event.getProperty("enabled");
            $(".graphenablebtn i").toggleClass("fa-unlock", !enabled), $(".graphenablebtn i").toggleClass("fa-lock", enabled);
        }));
        var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" ], date = "", version = "";
        $.ajax("package.json", {
            success: function(data) {
                date = new Date(data.versionDate), version = data.version, $(".skVersionNumber").html("v" + version + " (" + date.getDate() + " " + monthNames[date.getMonth()] + " " + date.getFullYear() + ")");
            }
        }), $(".skMiniMaxiMenu").on("click", function() {
            $("i", this).toggleClass("fa-angle-double-up"), $("i", this).toggleClass("fa-angle-double-down"), 
            $(".canBeMini").toggleClass("mini").toggle(), that.navigatorUI.refreshNow();
        }), $(".skSideMenu_license .sideMenuText").html("Read license (Skore app Web demo)");
    }
    var $ = require("jquery"), mxUtils = require("mxUtils"), skGraphEditorTutorial = require("skGraphEditorUI/skGraphEditorTutorial"), skShell = require("skShell");
    return skSideMenu;
});
define('skNavigatorUI/skFooter_Kbd',['require','jquery','mxEvent','mxUtils'],function(require) {
    "use strict";
    function skFooter_Kbd(graphManager) {
        var li = $("<li class='skFooter_Kbd'>More keyboard shortcuts</li>").attr("data-skaction", "modal_kbd");
        $(".skFooter").prepend(li), this.kbdPlaceholder = $("<span>"), $(li).prepend(this.kbdPlaceholder), 
        this.updateKbdShortcut("canvas");
        var footer = this;
        graphManager.addListener("graphEnabled", mxUtils.bind(this, function() {
            var enabled = event.getProperty("enabled");
            enabled || this.updateKbdShortcut("canvasViewer");
        })), graphManager.addListener("newGraphCreated", function(sender, event) {
            var graph = event.getProperty("graph");
            graph.getSelectionModel().addListener(mxEvent.CHANGE, function(sender, evt) {
                var cells = evt.getProperty("removed");
                cells && 1 == cells.length ? footer.updateKbdShortcut("boxSelected") : cells && cells.length > 1 ? footer.updateKbdShortcut("boxesSelected") : footer.updateKbdShortcut("canvas");
            }), graph.addListener(mxEvent.START_EDITING, function() {
                footer.updateKbdShortcut("boxEdit");
            });
        });
    }
    var $ = require("jquery"), mxEvent = require("mxEvent"), mxUtils = require("mxUtils");
    return skFooter_Kbd.prototype.kbdPlaceholder = null, skFooter_Kbd.prototype.kbdList = {
        canvasViewer: "kbdshortcut for viewer",
        canvas: "<span class='kbdshortcut'><kbd>W</kbd> + <i class='fa fa-mouse-pointer'></i> Create Whatbox</span><span class='kbdshortcut'><kbd>Y</kbd> + <i class='fa fa-mouse-pointer'></i> Create Whybox</span><span class='kbdshortcut'><kbd>N</kbd> + <i class='fa fa-mouse-pointer'></i> Create Note</span><span class='kbdshortcut'><kbd>Shift&nbsp;<span>⇧</span></kbd> + Drag <i class='fa fa-mouse-pointer'></i> Selection mode</span>",
        boxSelected: "<span class='kbdshortcut'><kbd>Space</kbd> Create next box</span><span class='kbdshortcut'><kbd>Tab&nbsp;<span>⇥</span></kbd> Select next box</span><span class='kbdshortcut'><kbd>Enter&nbsp;<span>↩</span></kbd> Edit text</span><span class='kbdshortcut'><kbd>F</kbd> Bring to <strong>f</strong>ront</span><span class='kbdshortcut'><kbd>B</kbd> Send to <strong>b</strong>ack</span>",
        boxesSelected: "<span class='kbdshortcut'><kbd class='skCtrlOrCmd'></kbd> + <kbd>G</kbd> Groups</span><span class='kbdshortcut'><kbd>T</kbd> Align tops (horizontal)</span><span class='kbdshortcut'><kbd>C</kbd> Align centers (vertical)</span>",
        boxEdit: "<span class='kbdshortcut'>Use Markdown : <code>**</code>bold<code>**</code></span><span class='kbdshortcut'><code>*</code>italic<code>*</code></span><span class='kbdshortcut'><kbd>Shift&nbsp;<span>⇧</span></kbd> + <kbd>↩</kbd> for line return</span>"
    }, skFooter_Kbd.prototype.updateKbdShortcut = function(listCase) {
        this.kbdPlaceholder.empty(), this.kbdList[listCase] && this.kbdPlaceholder.append($(this.kbdList[listCase]));
    }, skFooter_Kbd;
});
define('skNavigatorUI/skFooter_IDs',['require','jquery','mxUtils'],function(require) {
    "use strict";
    function skFooter_IDs(graphManager) {
        var li = $("<li class='skFooter_IDs'></li>").attr("data-skaction", "showID");
        $(".skFooter").prepend(li), graphManager.addListener("graphEnabled", mxUtils.bind(this, function(sender, event) {
            li.toggleClass("on", event.getProperty("graph").showBadgeIcon);
        })), graphManager.addListener("boxSelectionChanged", mxUtils.bind(this, function(sender, event) {
            var cellsAdded = event.getProperty("cellsAdded");
            cellsAdded && 0 === cellsAdded.length ? li.hide() : cellsAdded && 1 === cellsAdded.length ? li.show().text(cellsAdded[0].id) : cellsAdded && cellsAdded.length > 1 && li.show().text("• " + cellsAdded.length + " •");
        }));
    }
    var $ = require("jquery"), mxUtils = require("mxUtils");
    return skFooter_IDs;
});
define('skNavigatorUI/skFooter_Icons',['require','jquery','mxUtils'],function(require) {
    "use strict";
    function skFooter_Icons(graphManager) {
        this.iconBtn = $("<li class='skFooter_Icons'>Icons</li>").attr("data-skaction", "modal_icons"), 
        $(".skFooter").prepend(this.iconBtn), graphManager.addListener("graphEnabled", mxUtils.bind(this, function(sender, event) {
            var enabled = event.getProperty("enabled");
            this.setEnabled(enabled);
        }));
    }
    var $ = require("jquery"), mxUtils = require("mxUtils");
    return skFooter_Icons.prototype.setEnabled = function(enabled) {
        this.iconBtn.toggle(enabled);
    }, skFooter_Icons.prototype.iconBtn = null, skFooter_Icons;
});
define('text!skUI/skEditMenu.html',[],function () { return '\n\t<li data-skaction="editMenu" class="dropdown forGraphEditor">\n\t\t<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Edit <span class="caret"></span></a>\n\t\t<ul class="editMenu dropdown-menu sk-large-menu">\n\t\t\t<li>\n\t\t\t\t<a data-skaction="undo">\n\t\t\t\t\t<span class="pull-right"><kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>Z</kbd></span>\n\t\t\t\t\t<i class="fa fa-undo"></i>\n\t\t\t\t\tUndo\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="redo">\n\t\t\t\t\t<span class="pull-right"><kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>Y</kbd></span>\n\t\t\t\t\t<i class="fa fa-repeat"></i>\n\t\t\t\t\tRedo\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class="divider"></li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="copy">\n\t\t\t\t\t<span class="pull-right"><kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>C</kbd></span>\n\t\t\t\t\t<i class="fa fa-files-o"></i>\n\t\t\t\t\tCopy\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="paste">\n\t\t\t\t\t<span class="pull-right"><kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>V</kbd></span>\n\t\t\t\t\t<i class="fa fa-clipboard"></i>\n\t\t\t\t\tPaste\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="cut">\n\t\t\t\t\t<span class="pull-right"><kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>X</kbd></span>\n\t\t\t\t\t<i class="fa fa-scissors"></i>\n\t\t\t\t\tCut\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="delete">\n\t\t\t\t\t<span class="pull-right"><kbd>Del</kbd> or <kbd>←</kbd></span>\n\t\t\t\t\t<i class="fa fa-trash-o"></i>\n\t\t\t\t\tDelete\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t</ul>\n\t</li>\n';});
define('text!skUI/skFormatMenu.html',[],function () { return '\t<li data-skaction="formatMenu" class="dropdown forGraphEditor">\n\t\t<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Format <span class="caret"></span></a>\n\t\t<ul class="formatMenu dropdown-menu sk-large-menu">\n\n\t\t\t<!-- group -->\n\t\t\t<li>\n\t\t\t\t<a data-skaction="group">\n\t\t\t\t\t<span class="pull-right"><kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>G</kbd></span>\n\t\t\t\t\t<i class="fa fa-object-group"></i>\n\t\t\t\t\tGroup\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="unGroup">\n\t\t\t\t\t<span class="pull-right"><kbd><span class="skCtrlOrCmd"></span></kbd> + <kbd>U</kbd></span>\n\t\t\t\t\t<i class="fa fa-object-ungroup"></i>\n\t\t\t\t\tUngroup\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class="divider"></li>\n\t\t\t<!-- front / back -->\n\t\t\t<li>\n\t\t\t\t<a data-skaction="bringFront">\n\t\t\t\t\t<span class="pull-right"><kbd>F</kbd></span>\n\t\t\t\t\t<svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="9" height="9"  stroke-width="1"></rect>\n\t\t\t\t\t\t<rect class="block blockFull" shape-rendering="crispEdges" x="10" y="8" width="9" height="9" stroke-width="1"></rect>\n\t\t\t\t\t</svg>\n\t\t\t\t\tBring to front\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="sendBack">\n\t\t\t\t\t<span class="pull-right"><kbd>B</kbd></span>\n\t\t\t\t\t<svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\n\n\t\t\t\t\t\t<rect class="block blockFull" shape-rendering="crispEdges" x="10" y="8" width="9" height="9" stroke-width="1"></rect>\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="9" height="9" fill="black" stroke-width="1"></rect>\n\t\t\t\t\t</svg>\n\t\t\t\t\tSend to back\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class="divider"></li>\n\t\t\t<!-- align -->\n\t\t\t<li>\n\t\t\t\t<a data-skaction="alignTop">\n\t\t\t\t\t<span class="pull-right"><kbd>T</kbd></span>\n\t\t\t\t\t<svg class="formatBtn" width="20" height="18" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\t\t\t\t\t\t<line class="block" shape-rendering="crispEdges" x1="2" x2="18" y1="2" y2="2" fill="" stroke-width="1"></line>\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="4" height="9" fill="" stroke-width="1"></rect>\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="10" y="4" width="4" height="12" fill="" stroke-width="1"></rect>\n\t\t\t\t\t</svg>\n\t\t\t\t\tAlign tops\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="alignCenter">\n\t\t\t\t\t<span class="pull-right"><kbd>C</kbd></span>\n\t\t\t\t\t<svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\t\t\t\t\t\t<line class="block" shape-rendering="crispEdges" x1="10" x2="10" y1="2" y2="18" fill="" stroke-width="1"></line>\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="4" y="4" width="12" height="4" fill="" stroke-width="1"></rect>\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="6" y="10" width="8" height="4" fill="" stroke-width="1"></rect>\n\t\t\t\t\t</svg>\n\t\t\t\t\tAlign centers\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li class="divider"></li>\n\t\t\t<!-- align -->\n\t\t\t<li>\n\t\t\t\t<a data-skaction="distributeHorizontal">\n\t\t\t\t\t<span class="pull-right"><kbd>H</kbd></span>\n\t\t\t\t\t<svg class="formatBtn" width="20" height="18" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="0" y="0" width="4" height="12" fill="" stroke-width="1"></rect>\n\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="6" y="0" width="4" height="12" fill="" stroke-width="1"></rect>\n\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="12" y="0" width="4" height="12" fill="" stroke-width="1"></rect>\n\n\t\t\t\t\t</svg>\n\t\t\t\t\tSpace out horizontally\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t\t<li>\n\t\t\t\t<a data-skaction="distributeVertical">\n\t\t\t\t\t<span class="pull-right"><kbd>V</kbd></span>\n\t\t\t\t\t<svg class="formatBtn" width="20" height="20" version="1.1" xmlns="http://www.w3.org/2000/svg" style="display: inline-block;">\n\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="0" y="0" width="12" height="4" fill="" stroke-width="1"></rect>\n\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="0" y="6" width="12" height="4" fill="" stroke-width="1"></rect>\n\n\t\t\t\t\t\t<rect class="block blockEmpty" shape-rendering="crispEdges" x="0" y="12" width="12" height="4" fill="" stroke-width="1"></rect>\n\n\t\t\t\t\t</svg>\n\t\t\t\t\tSpace out vertically\n\t\t\t\t</a>\n\t\t\t</li>\n\t\t</ul>\n\t</li>\n';});
define('skGraphEditorUI/skClipboard',['require','jquery','mxClient','mxClipboard','mxCodec','mxEvent','mxGraph','mxGraphModel','mxUtils','skConstants','skShell','skUtils'],function(require) {
    "use strict";
    var $ = require("jquery"), mxClient = require("mxClient"), mxClipboard = require("mxClipboard"), mxCodec = require("mxCodec"), mxEvent = require("mxEvent"), mxGraph = require("mxGraph"), mxGraphModel = require("mxGraphModel"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skShell = require("skShell"), skUtils = require("skUtils");
    return function(graphManager) {
        var codec, model;
        mxClipboard.cellsToString = function(cells) {
            codec = new mxCodec(), model = new mxGraphModel();
            for (var parent = model.getChildAt(model.getRoot(), 0), i = 0; i < cells.length; i++) model.add(parent, cells[i]);
            return model.filterDescendants(function(cell) {
                var roleCandidate = $("role", cell.value);
                roleCandidate.length > 0 && $(roleCandidate).each(function(j, role) {
                    var lid = $(role).attr(skConstants.LOCAL_ID);
                    $(role).attr(skConstants.VALUE, skUtils.readValue(graphManager.getCurrentGraph().roleManager.getRoleByLocalID(lid))), 
                    $(role).removeAttr(skConstants.LOCAL_ID);
                });
            }), mxUtils.getXml(codec.encode(model));
        };
        var textInput = document.createElement("textarea");
        mxUtils.setOpacity(textInput, 0), textInput.setAttribute("class", "selectionAllowed skClipboard"), 
        textInput.style.width = "1px", textInput.style.height = "1px";
        var restoreFocus = !1, gs = mxGraph.prototype.gridSize, lastPaste = null, dx = 0, dy = 0;
        graphManager.addListener("navigationDone", function() {
            dx = 0, dy = 0;
        }), textInput.value = " ", mxEvent.addListener(document, "keydown", function(evt) {
            var source = mxEvent.getSource(evt);
            if (!$(document.body).hasClass("modal-open")) {
                var graph = graphManager.getCurrentGraph();
                !graph || !graph.isEnabled() || graph.isMouseDown || graph.isEditing() || window.textEditing(evt) || "INPUT" == source.nodeName || (224 == evt.keyCode || !mxClient.IS_MAC && 17 == evt.keyCode || mxClient.IS_MAC && 91 == evt.keyCode) && (textInput.style.position = "absolute", 
                textInput.style.left = graph.container.scrollLeft + 10 + "px", textInput.style.top = graph.container.scrollTop + 10 + "px", 
                graphManager.getCurrentGraph().container.appendChild(textInput), restoreFocus = !0, 
                textInput.focus(), textInput.select());
            }
        }), $(document).on("keyup", function(event) {
            var evt = $(event)[0];
            $(evt.target).hasClass("skClipboard") && restoreFocus && (224 == evt.keyCode || 17 == evt.keyCode || 91 == evt.keyCode) && (restoreFocus = !1, 
            graphManager.getCurrentGraph().isEditing() || graphManager.getCurrentGraph().container.focus(), 
            textInput.parentNode.removeChild(textInput));
        }), skShell.copyCells = function(graph, cells) {
            if (cells.length > 0) {
                for (var clones = graph.cloneCells(cells), i = 0; i < clones.length; i++) {
                    var state = graph.view.getState(cells[i]);
                    if (null != state) {
                        var geo = graph.getCellGeometry(clones[i]);
                        null != geo && geo.relative && (geo.relative = !1, geo.x = state.x / state.view.scale - state.view.translate.x, 
                        geo.y = state.y / state.view.scale - state.view.translate.y);
                    }
                }
                textInput.value = mxClipboard.cellsToString(clones);
            }
            return textInput.select(), lastPaste = textInput.value, textInput.value;
        }, mxEvent.addListener(textInput, "copy", mxUtils.bind(this, function() {
            var graph = graphManager.getCurrentGraph();
            graph.isEnabled() && !graph.isSelectionEmpty() && (skShell.copyCells(graph, mxUtils.sortCells(graph.model.getTopmostCells(graph.getSelectionCells()))), 
            dx = 0, dy = 0);
        })), mxEvent.addListener(textInput, "cut", mxUtils.bind(this, function() {
            var graph = graphManager.getCurrentGraph();
            graph.isEnabled() && !graph.isSelectionEmpty() && (skShell.copyCells(graph, graph.removeCells()), 
            dx = -gs, dy = -gs);
        }));
        var importXml = function(graph, xml, dx, dy) {
            dx = null != dx ? dx : 0, dy = null != dy ? dy : 0;
            var cells = [];
            try {
                var doc = mxUtils.parseXml(xml), node = doc.documentElement;
                if (null != node) {
                    model = new mxGraphModel(), codec = new mxCodec(node.ownerDocument), codec.decode(node, model);
                    var childCount = model.getChildCount(model.getRoot()), targetChildCount = graph.model.getChildCount(graph.model.getRoot());
                    graph.model.beginUpdate();
                    try {
                        for (var children, i = 0; childCount > i; i++) {
                            var parent = model.getChildAt(model.getRoot(), i);
                            if (targetChildCount > i) {
                                var target = 1 == childCount ? graph.getDefaultParent() : graph.model.getChildAt(graph.model.getRoot(), i);
                                graph.isCellLocked(target) || (children = model.getChildren(parent), cells = cells.concat(graph.importCells(children, dx, dy, target)));
                            } else parent = graph.importCells([ parent ], 0, 0, graph.model.getRoot())[0], children = graph.model.getChildren(parent), 
                            graph.moveCells(children, dx, dy), cells = cells.concat(children);
                        }
                        graph.roleManager.initModel();
                    } finally {
                        graph.model.endUpdate(), codec = null, model = null;
                    }
                }
            } catch (e) {
                throw e;
            }
            return cells;
        };
        skShell.pasteText = function(graph, text) {
            skShell.setStatus("Pasting... this can take a while...");
            var xml = mxUtils.trim(text);
            if (xml.length > 0 && (lastPaste != xml ? (lastPaste = xml, dx = 0, dy = 0) : (dx += gs, 
            dy += gs), "<mxGraphModel>" == xml.substring(0, 14))) {
                graph.setSelectionCells(importXml(graph, xml, dx, dy)), setTimeout(function() {
                    graph.scrollCellToVisible(graph.getSelectionCell());
                }, 100), skShell.setStatus("Paste done!");
            }
        };
        var extractGraphModelFromEvent = function(evt) {
            var data = null;
            if (null != evt) {
                var provider = null != evt.dataTransfer ? evt.dataTransfer : evt.clipboardData;
                null != provider && (10 == document.documentMode || 11 == document.documentMode ? data = provider.getData("Text") : (data = mxUtils.indexOf(provider.types, "text/html") >= 0 ? provider.getData("text/html") : null, 
                mxUtils.indexOf(provider.types, null == data || 0 === data.length) && (data = provider.getData("text/plain"))));
            }
            return data;
        };
        mxEvent.addListener(textInput, "paste", function(evt) {
            var graph = graphManager.getCurrentGraph();
            if ($(evt.target).hasClass("skClipboard")) {
                if (textInput.value = "", graph.isEnabled() && !graph.isEditing()) {
                    var xml = extractGraphModelFromEvent(evt);
                    null !== xml && xml.length > 0 ? skShell.pasteText(graph, xml) : window.setTimeout(mxUtils.bind(this, function() {
                        skShell.pasteText(graph, textInput.value);
                    }), 0);
                }
                textInput.select();
            }
        });
    };
});
define('skNavigator/skAction',['require','jquery','mxEventSource','mxUtils'],function(require) {
    "use strict";
    function skAction(label, funct, requireOneCell, requireMultiCells) {
        mxEventSource.call(this), this.label = label, this.funct = funct, this.enabled = !0, 
        this.requireMultiCells = requireMultiCells || !1, this.requireOneCell = requireOneCell || !1;
    }
    var $ = require("jquery"), mxEventSource = require("mxEventSource"), mxUtils = require("mxUtils");
    return mxUtils.extend(skAction, mxEventSource), skAction.prototype.setEnabled = function(value) {
        this.enabled != value && (this.enabled = value, $("[data-skaction=" + this.label + "]").toggleClass("disabled", !this.enabled));
    }, skAction;
});
define('skNavigator/skActions',['require','jquery','mxClipboard','mxConstants','mxEvent','mxEventObject','mxGeometry','mxUtils','./skAction','skConstants','skShell'],function(require) {
    "use strict";
    function skActions(graphManager) {
        this.actions = {}, this.graphManager = graphManager;
        var that = this;
        $(document.body, "[data-skaction]").on("click", function(event) {
            var act = that.get($(event.target).closest("[data-skaction]").data("skaction"));
            if (null != act) {
                event.stopPropagation();
                var funct = act.funct;
                null !== funct && funct(event), mxEvent.consume(event);
            }
        }), this.init(graphManager), this.loadActions();
    }
    var $ = require("jquery"), mxClipboard = require("mxClipboard"), mxConstants = require("mxConstants"), mxEvent = require("mxEvent"), mxEventObject = require("mxEventObject"), mxGeometry = require("mxGeometry"), mxUtils = require("mxUtils"), skAction = require("./skAction"), skConstants = require("skConstants"), skShell = require("skShell");
    return skActions.prototype.actions = null, skActions.prototype.graphManager = null, 
    skActions.prototype.init = function(graphManager) {
        graphManager.addListener("newGraphCreated", mxUtils.bind(this, function(sender, event) {
            var graph = event.getProperty("graph");
            graph.getSelectionModel().addListener(mxEvent.CHANGE, mxUtils.bind(this, function() {
                var cells = graph.getSelectionCells(), multiCells = cells.length > 1, oneCell = 1 == cells.length, that = this;
                Object.keys(this.actions).forEach(function(action) {
                    if (that.actions.hasOwnProperty(action)) {
                        var currentAction = that.get(action);
                        currentAction.requireOneCell && currentAction.requireMultiCells ? currentAction.setEnabled(oneCell || multiCells) : (currentAction.requireMultiCells && currentAction.setEnabled(multiCells), 
                        currentAction.requireOneCell && currentAction.setEnabled(oneCell));
                    }
                }), 1 === cells.length && cells[0].isGroup() ? this.get("unGroup").setEnabled(!0) : this.get("unGroup").setEnabled(!1), 
                null != mxClipboard.cells && this.get("paste").setEnabled(!0);
            }));
        }));
    }, skActions.prototype.addAction = function(key, requireOneCell, requireMultiCells, funct) {
        return this.put(key, new skAction(key, funct, requireOneCell, requireMultiCells));
    }, skActions.prototype.put = function(name, action) {
        return this.actions[name] = action, action;
    }, skActions.prototype.get = function(name) {
        return this.actions[name];
    }, skActions.prototype.loadActions = function() {
        var curGrph = mxUtils.bind(this, function() {
            return this.graphManager.getCurrentGraph();
        }), actions = this;
        this.addAction("home", !1, !1, function() {
            curGrph().home();
        }), this.addAction("exitGroup", !0, !1, function() {
            curGrph().navigateTo();
        }), this.addAction("enterGroup", !0, !1, function(cell) {
            curGrph().navigateTo(cell);
        }), this.addAction("undo", !1, !1, function() {
            curGrph().undoManager.undo();
        }), this.addAction("redo", !1, !1, function() {
            curGrph().undoManager.redo();
        }), this.addAction("enableGraph", !1, !1, function() {
            curGrph().setEnabled(!0);
        }), this.addAction("createNextBox", !0, !1, function() {
            arguments[0] && "[object KeyboardEvent]" === arguments[0][0].toString() && mxEvent.consume(arguments[0]), 
            window.setTimeout(function() {
                var currentCells = curGrph().getSelectionCells(), currentCell = currentCells[0];
                if (1 == currentCells.length && currentCell.isVertex() && !currentCell.isStickynote()) {
                    var currentState = curGrph().getView().getState(currentCell), newBox = currentState.add_box_after_box_state(null, "right", null, !0);
                    curGrph().setSelectionCell(newBox), skShell.tutorial && skShell.tutorial.stepDone(skShell.tutorial.BOX_CREATED_WITH_SPACE_BAR, {
                        cell: newBox
                    });
                }
            }, 10);
        }), this.addAction("createPreviousBox", !0, !1, function() {
            arguments[0] && "[object KeyboardEvent]" === arguments[0][0].toString() && mxEvent.consume(arguments[0]), 
            window.setTimeout(function() {
                var currentCells = curGrph().getSelectionCells(), currentCell = currentCells[0];
                if (1 == currentCells.length && currentCell.isVertex) {
                    var currentState = curGrph().getView().getState(currentCell), newBox = currentState.add_box_after_box_state(null, "left", null, !0);
                    curGrph().setSelectionCell(newBox);
                }
            }, 10);
        }), this.addAction("createNextBoxNewLine", !0, !1, function() {
            arguments[0] && "[object KeyboardEvent]" === arguments[0][0].toString() && mxEvent.consume(arguments[0]), 
            window.setTimeout(function() {
                var currentCells = curGrph().getSelectionCells(), currentCell = currentCells[0];
                if (1 == currentCells.length && currentCell.isVertex) {
                    var currentState = curGrph().getView().getState(currentCell), newGeo = new mxGeometry();
                    newGeo.x = 15, newGeo.y = currentCell.geometry.y + currentCell.geometry.height + skConstants.DIST_LINES;
                    var newBox = currentState.add_box_after_box_state(newGeo, "right", null, !0);
                    curGrph().setSelectionCell(newBox);
                }
            }, 10);
        });
        var siblingsToVisit = {
            cells: [],
            nextItem: 0,
            isRolling: !1,
            timer: null
        }, selectPreviousBoxWithTab = function() {
            selectBoxWithTab(!0);
        }, selectBoxWithTab = function(backwards) {
            function startTimer() {
                siblingsToVisit.timer = window.setTimeout(function() {
                    siblingsToVisit.isRolling && (siblingsToVisit.isRolling = !1), window.clearTimeout(siblingsToVisit.timer), 
                    siblingsToVisit.timer = null;
                }, 500);
            }
            if (backwards = "boolean" == typeof backwards ? backwards : !1, curGrph().getSelectionCells().length) if (siblingsToVisit.isRolling) curGrph().setSelectionCell(siblingsToVisit.cells[siblingsToVisit.nextItem]), 
            siblingsToVisit.nextItem = (siblingsToVisit.nextItem + 1) % siblingsToVisit.cells.length, 
            window.clearTimeout(siblingsToVisit.timer), startTimer(); else {
                siblingsToVisit.cells.length = 0;
                var currentCell = curGrph().getSelectionCells()[0];
                if ($(currentCell.edges).each(function(i, edge) {
                    backwards ? edge.source.isVertex && edge.target == currentCell && siblingsToVisit.cells.push(edge.source) : edge.target.isVertex && edge.source == currentCell && siblingsToVisit.cells.push(edge.target);
                }), 0 === siblingsToVisit.cells.length) return actions.get("createNextBox").funct(), 
                !0;
                if (curGrph().setSelectionCell(siblingsToVisit.cells[0]), 1 == siblingsToVisit.cells.length) return !0;
                siblingsToVisit.nextItem = 0, siblingsToVisit.isRolling = !0, siblingsToVisit.nextItem = (siblingsToVisit.nextItem + 1) % siblingsToVisit.cells.length, 
                startTimer();
            }
        };
        this.addAction("selectNextBox", !0, !1, selectBoxWithTab), this.addAction("selectPreviousBox", !0, !1, selectPreviousBoxWithTab), 
        this.addAction("cut", !0, !0, function() {
            mxClipboard.cut(curGrph());
        }), this.addAction("copy", !0, !0, function() {
            mxClipboard.copy(curGrph()), actions.get("paste").setEnabled(!0);
        }), this.addAction("paste", !1, !1, function() {
            curGrph().isEditing() || mxClipboard.paste(curGrph());
        }), this.addAction("delete", !0, !0, function() {
            curGrph().removeCells();
        }), this.addAction("duplicate", !0, !0, function() {
            var s = curGrph().gridSize;
            curGrph().setSelectionCells(curGrph().moveCells(curGrph().getSelectionCells(), s, s, !0));
        }), this.addAction("selectAll", !1, !1, function() {
            curGrph().selectAll();
        }), this.addAction("group", !0, !0, function() {
            curGrph().setSelectionCell(curGrph().groupCells(null, 15));
        }), this.addAction("unGroup", !0, !1, function() {
            curGrph().setSelectionCells(curGrph().ungroupCells());
        }), this.addAction("alignTop", !1, !0, function() {
            curGrph().alignCells(mxConstants.ALIGN_TOP);
        }), this.addAction("alignLeft", !1, !0, function() {
            curGrph().alignCells(mxConstants.ALIGN_LEFT);
        }), this.addAction("alignRight", !1, !0, function() {
            curGrph().alignCells(mxConstants.ALIGN_RIGHT);
        }), this.addAction("alignCenter", !1, !0, function() {
            curGrph().alignCells(mxConstants.ALIGN_CENTER);
        }), this.addAction("bringFront", !0, !0, function() {
            curGrph().alignCells(mxConstants.ALIGN_TOP);
        }), this.addAction("sendBack", !0, !0, function() {
            curGrph().orderCells(!0);
        }), this.addAction("bringFront", !0, !0, function() {
            curGrph().orderCells(!1);
        }), this.addAction("distributeHorizontal", !1, !0, function() {
            curGrph().distributeCells(!0);
        }), this.addAction("distributeVertical", !1, !0, function() {
            curGrph().distributeCells(!1);
        }), this.addAction("modal_print", !1, !1, function() {
            actions.openModal("print");
        }), this.addAction("modal_justskoreit", !1, !1, function() {
            actions.openModal("justskoreit");
        }), this.addAction("modal_settings", !1, !1, function() {
            actions.openModal("settings");
        }), this.addAction("modal_registration", !1, !1, function() {
            actions.openModal("registration");
        }), this.addAction("modal_socialexport", !1, !1, function() {
            actions.openModal("socialexport");
        }), this.addAction("modal_imageexport", !1, !1, function(event) {
            var target = "preview";
            event && (target = $(event.toElement).closest("[data-target]").data("target")), 
            actions.openModal("imageexport", "target", target);
        }), this.addAction("zoomIn", !1, !1, function() {
            curGrph().zoomIn();
        }), this.addAction("zoomFit", !1, !1, function() {
            curGrph().fit(10);
        }), this.addAction("zoomOut", !1, !1, function() {
            curGrph().zoomOut();
        }), this.addAction("zoomReset", !1, !1, function() {
            curGrph().zoomTo(1);
        }), this.addAction("zoomFitPageWidth", !1, !1, function() {
            var fmt = curGrph().pageFormat, ps = curGrph().pageScale, cw = curGrph().container.clientWidth - 0, scale = Math.floor(20 * cw / fmt.width / ps) / 20;
            if (curGrph().zoomTo(scale), mxUtils.hasScrollbars(curGrph().container)) {
                var pad = curGrph().getPagePadding();
                curGrph().container.scrollLeft = Math.min(pad.x * curGrph().view.scale, (curGrph().container.scrollWidth - curGrph().container.clientWidth) / 2);
            }
        });
        var that = this;
        this.toggledStickyNotes = [], this.addAction("toggleStickynotes", !1, !1, function() {
            if (that.toggledStickyNotes.length > 0) curGrph().toggleCells(!0, that.toggledStickyNotes, !0), 
            that.toggledStickyNotes = []; else {
                var cells = curGrph().model.filterDescendants(function(cell) {
                    return cell.isStickynote();
                });
                that.toggledStickyNotes = curGrph().toggleCells(!1, cells, !0);
            }
        }), this.addAction("showID", !1, !1, function() {
            $(".skFooter_IDs").toggleClass("on"), curGrph().showBadgeIcon = !curGrph().showBadgeIcon, 
            curGrph().refresh();
        }), this.addAction("modal_kbd", !1, !1, function() {
            actions.openModal("kbd");
        }), this.addAction("modal_formatting", !1, !1, function() {
            actions.openModal("formatting");
        }), this.addAction("modal_icons", !1, !1, function() {
            actions.openModal("icons");
        }), this.addAction("modal_xml", !1, !1, function() {
            actions.openModal("xml");
        }), this.addAction("modal_stylesheet", !1, !1, function(evt) {
            var loadtype = "stylesheet";
            if (evt.target) {
                var closest = $(evt.target.closest("[data-loadtype]"));
                closest && (loadtype = closest.data("loadtype"));
            } else curGrph().getSelectionCells().length > 0 && (loadtype = "stylecell");
            actions.openModal("stylesheet", "loadtype", loadtype);
        });
    }, skActions.prototype.openModal = function(name, arg1Def, arg1Val, arg2Def, arg2Val, arg3Def, arg3Val) {
        this.graphManager.fireEvent(new mxEventObject("openModal", "graph", this.graphManager.getCurrentGraph(), "name", name, arg1Def, arg1Val, arg2Def, arg2Val, arg3Def, arg3Val));
    }, skActions;
});
define('skGraphIO/skFileSaver',['require','jquery','mxEvent','mxUtils','skUtils'],function(require) {
    "use strict";
    function skFileSaver(graphManager) {
        this.graphManager = graphManager, this.graphManager.addListener("newGraphCreated", mxUtils.bind(this, function(sender, event) {
            this.enableForGraph(event.getProperty("graph"));
        }));
        var that = this;
        this.save = skUtils.debounce(function() {
            that.saveNow();
        }, 4e3);
    }
    var $ = require("jquery"), mxEvent = require("mxEvent"), mxUtils = require("mxUtils"), skUtils = require("skUtils");
    return skFileSaver.prototype.graphManager = null, skFileSaver.prototype.save = function() {
        return null;
    }, skFileSaver.prototype.saveNow = function() {
        this.graphManager.getCurrentGraph().isEnabled() && ($(".skSaveNow").css({
            color: ""
        }).html("Saving..."), window.parent.postMessage(JSON.stringify({
            name: "saveContent",
            data: {
                image: "",
                xml: mxUtils.getPrettyXml(this.graphManager.getCurrentGraph().getGraphXml())
            }
        }), "*"));
    }, skFileSaver.prototype.sendContentToSave = function(force) {
        force ? this.saveNow() : this.save();
    }, skFileSaver.prototype.setUpEnterprise = function() {
        function messageListener(event) {
            "saveContent" === event.data && that.sendContentToSave(!0), "contentSaved" === event.data && ($(".skSaveNow").html("Saved"), 
            setTimeout(function() {
                $(".skSaveNow").html("Save");
            }, 500));
        }
        var that = this;
        $(".skSaveNow").show(), $(".skSaveNow").on("click", function() {
            that.sendContentToSave(!0);
        }), window.addEventListener("message", messageListener), window.setTimeout(function() {
            that.sendContentToSave();
        }, 6e4);
    }, skFileSaver.prototype.enableForGraph = function(graph) {
        var that = this;
        graph.getModel().addListener(mxEvent.CHANGE, function(sender, evt) {
            var changes = evt.getProperty("edit").changes;
            changes.length && ($(".skSaveNow").html("Save"), that.sendContentToSave(!1));
        });
    }, skFileSaver;
});
define('skGraphEditorUI/skKeyboardHandler',['require','mxClient','mxEvent','mxKeyHandler','mxUtils','skShell'],function(require) {
    "use strict";
    function skGraphKeyboardHandler(graph, actions) {
        function nudge(keyCode) {
            if (!graph.isSelectionEmpty()) {
                var dx = 0, dy = 0;
                37 == keyCode ? dx = -1 : 38 == keyCode ? dy = -1 : 39 == keyCode ? dx = 1 : 40 == keyCode && (dy = 1), 
                graph.moveCells(graph.getSelectionCells(), dx, dy);
            }
        }
        var keyHandler = new mxKeyHandler(graph, graph.container);
        keyHandler.isControlDown = function(evt) {
            return mxEvent.isControlDown(evt) || mxClient.IS_MAC && evt.metaKey;
        };
        var keyHandleEscape = keyHandler.escape;
        keyHandler.escape = function() {
            graph.cellEditor.stopEditing(!0), keyHandleEscape.apply(this, arguments);
        };
        var bindAction = mxUtils.bind(this, function(code, control, key, shift) {
            var action = actions.get(key);
            if (null != action) {
                var f = function() {
                    action.enabled && action.funct(arguments);
                };
                control ? shift ? keyHandler.bindControlShiftKey(code, f) : keyHandler.bindControlKey(code, f) : shift ? keyHandler.bindShiftKey(code, f) : keyHandler.bindKey(code, f);
            }
        });
        return skShell.keyBindAction = bindAction, keyHandler.bindKey(33, function() {
            actions.get("exitGroup").funct.apply();
        }), keyHandler.bindKey(34, function() {
            actions.get("enterGroup").funct.apply();
        }), keyHandler.bindKey(36, function() {
            graph.home();
        }), keyHandler.bindKey(35, function() {
            graph.refresh();
        }), keyHandler.bindKey(37, function() {
            nudge(37);
        }), keyHandler.bindKey(38, function() {
            nudge(38);
        }), keyHandler.bindKey(39, function() {
            nudge(39);
        }), keyHandler.bindKey(40, function() {
            nudge(40);
        }), keyHandler.bindKey(13, function() {
            graph.startEditingAtCell();
        }), keyHandler.bindKey(113, function() {
            graph.startEditingAtCell();
        }), bindAction(70, !1, "bringFront"), bindAction(66, !1, "sendBack"), bindAction(84, !1, "alignTop"), 
        bindAction(76, !1, "alignLeft"), bindAction(82, !1, "alignRight"), bindAction(67, !1, "alignCenter"), 
        bindAction(83, !1, "modal_stylesheet"), bindAction(86, !1, "distributeVertical"), 
        bindAction(72, !1, "distributeHorizontal"), bindAction(88, !1, "modal_xml"), bindAction(46, !1, "delete"), 
        bindAction(8, !1, "delete"), bindAction(107, !1, "zoomIn"), bindAction(109, !1, "zoomOut"), 
        bindAction(187, !1, "zoomReset"), bindAction(90, !0, "undo"), bindAction(89, !0, "redo"), 
        bindAction(68, !0, "duplicate"), bindAction(71, !0, "group"), bindAction(65, !0, "selectAll"), 
        bindAction(80, !0, "modal_print"), bindAction(85, !0, "unGroup"), bindAction(75, !0, "toggleStickynotes"), 
        bindAction(32, !1, "createNextBox"), bindAction(32, !1, "createPreviousBox", !0), 
        bindAction(32, !0, "createNextBoxNewLine"), bindAction(9, !1, "selectNextBox"), 
        bindAction(9, !1, "selectPreviousBox", !0), keyHandler.bindKey(87, function() {
            skShell.wKeyIsPressed && (window.clearTimeout(skShell.wKeyIsPressed), skShell.wKeyIsPressed = null), 
            skShell.wKeyIsPressed = window.setTimeout(function() {
                skShell.wKeyIsPressed = null;
            }, 600);
        }), keyHandler.bindKey(89, function() {
            skShell.yKeyIsPressed && (window.clearTimeout(skShell.yKeyIsPressed), skShell.yKeyIsPressed = null), 
            skShell.yKeyIsPressed = window.setTimeout(function() {
                skShell.yKeyIsPressed = null;
            }, 600);
        }), keyHandler.bindKey(78, function() {
            skShell.nKeyIsPressed && (window.clearTimeout(skShell.nKeyIsPressed), skShell.nKeyIsPressed = null), 
            skShell.nKeyIsPressed = window.setTimeout(function() {
                skShell.nKeyIsPressed = null;
            }, 600);
        }), keyHandler;
    }
    var mxClient = require("mxClient"), mxEvent = require("mxEvent"), mxKeyHandler = require("mxKeyHandler"), mxUtils = require("mxUtils"), skShell = require("skShell");
    return skGraphKeyboardHandler;
});
define('skGraphEditorUI/skOutline',['require','jquery','mxConstants','mxEvent','mxOutline','mxUtils'],function(require) {
    "use strict";
    function skOutline(graph) {
        this.source = graph, this.border = 0;
        var container = $("#skOutline").empty();
        if (container.empty(), this.init(container[0]), this.isOutline = !0, mxEvent.addListener(window, "resize", mxUtils.bind(this, function() {
            this.update();
        })), graph.addListener("refreshOutline", mxUtils.bind(this, function() {
            this.update();
        })), this.outline.dialect == mxConstants.DIALECT_SVG) {
            var that = this;
            mxEvent.addMouseWheelListener(function(evt, up) {
                for (var outlineWheel = !1, source = mxEvent.getSource(evt); null != source; ) {
                    if (source == that.outline.view.canvas.ownerSVGElement) {
                        outlineWheel = !0;
                        break;
                    }
                    source = source.parentNode;
                }
                outlineWheel && (up ? graph.zoomIn() : graph.zoomOut(), mxEvent.consume(evt));
            });
        }
        return this.update(), this;
    }
    var $ = require("jquery"), mxConstants = require("mxConstants"), mxEvent = require("mxEvent"), mxOutline = require("mxOutline"), mxUtils = require("mxUtils"), outlineCreateGraph = mxOutline.prototype.createGraph;
    return mxOutline.prototype.createGraph = function(container) {
        this.container = container;
        var g = outlineCreateGraph.apply(this, arguments);
        g.foldingEnabled = !1, g.autoScroll = !1, g.gridEnabled = !1, g.pageScale = this.source.pageScale, 
        g.pageFormat = this.source.pageFormat, g.background = this.source.background, g.pageVisible = this.source.pageVisible;
        var current = mxUtils.getCurrentStyle(this.source.container);
        return container.style.backgroundColor = current.backgroundColor, g;
    }, mxUtils.extend(skOutline, mxOutline), skOutline.prototype.outline = null, skOutline;
});
define('skGraphManager/skProcessLinkHandler',['require','mxUtils'],function(require) {
    "use strict";
    function skProcessLinkHandler(graphManager) {
        this.graphManager = graphManager, document.body.addEventListener("click", mxUtils.bind(this, this.supportLinks), !1);
    }
    var mxUtils = require("mxUtils");
    return skProcessLinkHandler.prototype.graphManager = null, skProcessLinkHandler.prototype.checkDomElement = function(event, element) {
        if (element.classList.contains("skGoTo")) {
            if (!this.graphManager.getCurrentGraph().isEditing()) {
                var goTo = element.getAttribute("data-goto");
                goTo && this.graphManager.navigateTo(goTo);
            }
            event.preventDefault();
        } else element.parentElement && this.checkDomElement(event, element.parentElement);
    }, skProcessLinkHandler.prototype.supportLinks = function(event) {
        this.checkDomElement(event, event.target);
    }, skProcessLinkHandler;
});
define('skGraph/skExternalContent',['require','jquery'],function(require) {
    "use strict";
    var $ = require("jquery"), skExternalContent = {}, embedContainerCSS = "<style>.embed-container { position: relative; padding-bottom: 56.25%;height: 0;overflow: hidden;max-width: 100%;height: auto;}.embed-container iframe, .embed-container object, .embed-container embed { position: absolute;top: 0;left: 0;width: 100%;height: 100%;}</style>", embedContainerDivOpen = "<div class='embed-container'>", embedContainerDivClose = "</div>";
    return skExternalContent.youtube = function(youtubeURL) {
        var youtubeID;
        if (youtubeURL.length > 28) {
            var uri = youtubeURL, queryString = {};
            uri.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function($0, $1, $2, $3) {
                queryString[$1] = $3;
            }), youtubeID = queryString.v;
        } else youtubeID = youtubeURL.substring(16);
        var youtubeEmbed = "<iframe src='http://www.youtube.com/embed/" + youtubeID + "' frameborder='0' allowfullscreen></iframe>";
        return $("<div>" + embedContainerCSS + embedContainerDivOpen + youtubeEmbed + embedContainerDivClose + "</div>");
    }, skExternalContent.vimeo = function(vimeoURL) {
        var vimeoID, protocol = vimeoURL.slice(0, 5);
        "https" == protocol ? vimeoID = vimeoURL.substring(18) : (protocol = "http", vimeoID = vimeoURL.substring(17));
        var vimeoEmbed = "<iframe src='" + protocol + "://player.vimeo.com/video/" + vimeoID + "' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
        return $("<div>" + embedContainerCSS + embedContainerDivOpen + vimeoEmbed + embedContainerDivClose + "</div>");
    }, skExternalContent.dailymotion = function(dailymotionURL) {
        var m, dailymotionID = (m = dailymotionURL.match(new RegExp("/video/([^_?#]+).*?"))) ? m[1] : void 0, dailymotionEmbed = "<iframe src='http://www.dailymotion.com/embed/video/" + dailymotionID + "' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
        return $("<div>" + embedContainerCSS + embedContainerDivOpen + dailymotionEmbed + embedContainerDivClose + "</div>");
    }, skExternalContent.googlemaps = function(googlemapsURL) {
        var escapediFrameURL = googlemapsURL.replace(/\"/g, "'"), escapediFrameURLCode = escapediFrameURL.replace(/</g, "<"), escapediFrameURLCodeFinal = escapediFrameURLCode.replace(/>/g, ">");
        return $("<div>" + embedContainerCSS + embedContainerDivOpen + escapediFrameURLCodeFinal + embedContainerDivClose + "</div>");
    }, skExternalContent.image = function(imageURL) {
        return $("<img src='" + imageURL + "' class='img-responsive' />");
    }, skExternalContent.iframe = function(genericURL) {
        var escapediFrameURL = genericURL.replace(/\"/g, "'"), escapediFrameURLCode = escapediFrameURL.replace(/</g, "<"), escapediFrameURLCodeFinal = escapediFrameURLCode.replace(/>/g, ">");
        return $("<div>" + embedContainerCSS + embedContainerDivOpen + escapediFrameURLCodeFinal + embedContainerDivClose + "</div>");
    }, skExternalContent;
});
define('skGraph/skFormRenderer',[ "require", "jquery", "mxUtils", "mxCell", "skUtils", "colors", "mxConstants", "skConstants", "skGraph/skExternalContent", "lib/bootstrap3-typeahead" ], function(require, $, mxUtils, mxCell, skUtils, allColors, mxConstants, skConstants, externalcontent) {
    "use strict";
    function skFormRenderer(graph, cell, fieldDescription, parentValue, value, viewerMode, editableProp, saveCallBack) {
        counter = 0, this.graph = graph, this.cell = cell, this.editableProp = editableProp, 
        this.fieldDescription = fieldDescription, this.originalParentValue = parentValue, 
        this.originalValue = value, this.parentValue = parentValue, this.container = $("<div />"), 
        this.container.addClass("skForm"), this.container.append(this.renderForm(fieldDescription, parentValue, value, viewerMode)), 
        this.saveCallBack = saveCallBack;
    }
    var counter = 0;
    skFormRenderer.prototype.editableProp = null, skFormRenderer.prototype.saveCallBack = null, 
    skFormRenderer.prototype.fieldDescription = null, skFormRenderer.prototype.parentValue = null, 
    skFormRenderer.prototype.graph = null, skFormRenderer.prototype.cell = null, skFormRenderer.prototype.container = null, 
    skFormRenderer.prototype.renderForm = function(fieldDescription, parentValue, value, viewerMode) {
        var that = this, editorMode = !viewerMode;
        $(".popover").popover("hide"), editorMode && (value = value.cloneNode(!0), this.editedValue = value);
        var container = $("<div />");
        if (container.toggleClass("viewer", viewerMode), container.toggleClass("skSearchable", viewerMode), 
        container.toggleClass("editor", !viewerMode), fieldDescription.name ? container.append(that.renderField(fieldDescription, parentValue, value, viewerMode)) : fieldDescription.fields && container.append(that.renderFields(fieldDescription.fields, value, viewerMode)), 
        (this.editableProp.editable !== !1 || fieldDescription.editable) && this.editableProp.showBtn !== !1) {
            var buttonsSuperContainer = $('<div class="editButtons" />'), buttonsContainer = $('<div class="btn-group btn-group-xs" role="group" ></div>');
            container.append(buttonsSuperContainer), buttonsSuperContainer.append(buttonsContainer);
            var editButton, cancelButton, saveButton, removeButton, moveUpBtn, moveDownBtn;
            editButton = $("<button class='btn btn-default'>Edit</button>").on("click", function() {
                container.empty(), container.append(that.renderForm(fieldDescription, parentValue, value, !1));
            }), saveButton = $("<button class='btn btn-default'>Save</button>").on("click", function() {
                that.save();
            }), this.editableProp.showBtnCancel !== !1 && (cancelButton = $("<button class='btn btn-link'>Cancel</button>").on("click", function() {
                container.empty(), container.append(that.renderForm(fieldDescription, parentValue, value, !0));
            })), this.editableProp.showBtnRemove !== !1 && (removeButton = $("<button class='btn btn-link'>Remove</button>").on("click", function() {
                $(that.originalValue).remove(), container.remove();
            })), fieldDescription.sortable && (moveUpBtn = $("<button class='btn btn-default'><i class='fa fa-angle-up'></i></button>").on("click", function() {
                var currentValue = $($(this).parents(".skForm").find(".skField")[0]).data("value");
                currentValue && currentValue.previousSibling && currentValue.parentElement.insertBefore(currentValue, currentValue.previousSibling), 
                $(this).parents(".skForm").prev(".skForm").length > 0 && $(this).parents(".skForm").insertBefore($(this).parents(".skForm").prev(".skForm"));
            }), moveDownBtn = $("<button class='btn btn-default'><i class='fa fa-angle-down'></i></button>").on("click", function() {
                var currentValue = $($(this).parents(".skForm").find(".skField")[0]).data("value");
                currentValue && currentValue.nextSibling && currentValue.parentElement.insertBefore(currentValue, currentValue.nextSibling.nextSibling), 
                $(this).parents(".skForm").next(".skForm").length > 0 && $(this).parents(".skForm").insertAfter($(this).parents(".skForm").next(".skForm"));
            })), viewerMode ? (buttonsContainer.append(moveUpBtn), buttonsContainer.append(moveDownBtn), 
            buttonsContainer.append(editButton)) : (cancelButton && buttonsContainer.append(cancelButton), 
            removeButton && buttonsContainer.append(removeButton), buttonsContainer.append(saveButton));
        }
        return editorMode && skUtils.makeExpandingArea($(".expandingArea", container)), 
        viewerMode && fieldDescription.showOnlySelected && $(".skFormFieldInput", container).each(function(i, field) {
            "1" !== $($(field).data("value")).attr("displayed") && $(field).remove();
        }), container.append($("<div class='attSeparator'></div>")), container;
    }, skFormRenderer.prototype.renderFields = function(fields, parentValue, viewerMode, container) {
        var that = this, editorMode = !viewerMode;
        return container = container || $("<div />"), container.addClass("skFields"), fields.forEach(function(field) {
            if (field.multi) {
                var existingValues = $(field.name, parentValue);
                if ($(existingValues).each(function(i, val) {
                    container.append(that.renderField(field, parentValue, val, viewerMode));
                }), 0 === existingValues.length && field.multi.addempty === !0 && container.append(that.renderField(field, parentValue, mxUtils.createXmlDocument().createElement(field.name), viewerMode)), 
                editorMode && field.multi.addbutton) {
                    var addButton = $('<a class="btn btn-default btn-xs" style="width:100%;" role="button">' + (field.multi.addtext || "add 1 more") + "</a>").on("click", function() {
                        $(this).before(that.renderField(field, parentValue, mxUtils.createXmlDocument().createElement(field.name), viewerMode));
                    });
                    container.append(addButton);
                }
            } else {
                var val = null;
                parentValue && field.value ? (val = parentValue.querySelector("" + field.name + "[value='" + field.value + "']"), 
                editorMode && !val && (val = mxUtils.createXmlDocument().createElement(field.name))) : parentValue && (val = parentValue.querySelector(field.name), 
                editorMode && !val && (val = mxUtils.createXmlDocument().createElement(field.name))), 
                container.append(that.renderField(field, parentValue, val, viewerMode));
            }
        }), container;
    };
    var valueSecretPlaceholder = "____I_hope_Nobody_writes__that_as_a_value___";
    return skFormRenderer.prototype.renderField = function(fieldDescription, parentValue, value, viewerMode) {
        var editorMode = !viewerMode, that = this, container = $("<div />");
        container.addClass(fieldDescription.type), container.addClass(fieldDescription.name);
        var localid, myVal = skUtils.readValue(value);
        fieldDescription.localid && (localid = skUtils.readValue(value, skConstants.LOCAL_ID), 
        myVal = skUtils.readValue($("" + fieldDescription.name + "[" + skConstants.LOCAL_ID + "='" + localid + "']", this.graph.roleManager.roles)));
        var myInput, text;
        switch (fieldDescription.type) {
          case "group":
            fieldDescription.removable && !fieldDescription.editable && (myInput = container, 
            myInput.val(valueSecretPlaceholder), $(container).popover({
                container: "body",
                placement: "auto right",
                html: "true",
                trigger: "focus",
                content: function() {
                    var popoverTrigger = $(this), buttons = $('<div><button style="padding-bottom:5px;" class="popBtn popDelete btn btn-primary btn-sm"><i class=\'fa fa-trash-o\'></i></button>&nbsp;&nbsp;</div>');
                    return fieldDescription.sortable === !0 && (container.prev("." + fieldDescription.name).length > 0 && buttons.append($('<button style="padding-bottom:5px;" class="popBtn popUp btn btn-primary btn-sm"><i class=\'fa fa-angle-up\'></i></button>&nbsp;&nbsp;').on("click", function() {
                        container.insertBefore(container.prev(".responsibility"));
                    })), container.next("." + fieldDescription.name).length > 0 && buttons.append($('<button style="padding-bottom:5px;" class="popBtn popDown btn btn-primary btn-sm"><i class=\'fa fa-angle-down\'></i></button>&nbsp;&nbsp;').on("click", function() {
                        container.insertAfter(container.next(".responsibility"));
                    }))), $(".popBtn", buttons).on("click", function() {}), $(".popDelete", buttons).on("click", function() {
                        container.remove(), value.remove(), value = null, popoverTrigger.popover("destroy"), 
                        $(".popover").popover("hide");
                    }), buttons;
                }
            }));
            break;

          case "longtext":
            viewerMode ? (text = skUtils.renderMarkdown(myVal), "" === text && fieldDescription.defaulttext && (text = fieldDescription.defaulttext, 
            container.addClass("defaulttext")), container.html(text)) : (container.append($("<div class=\"expandingArea\"><pre><span></span><br></pre><textarea class='form-control selectionAllowed'></textarea></div>")), 
            "formatting" === fieldDescription.helptext && container.append('<div data-skaction="modal_formatting" class="formatButton" ><a>Format text</a></div>'), 
            myInput = $("textarea", container), myInput.attr("placeholder", fieldDescription.defaulttext || ""), 
            myInput.val(myVal));
            break;

          case "singleline":
            if (viewerMode) text = skUtils.renderMarkdown(myVal, !0), "" === text && fieldDescription.defaulttext && (text = fieldDescription.defaulttext, 
            container.addClass("defaulttext")), container.html(text), fieldDescription.addon && (container.addClass("input-group"), 
            container.addClass("input-group-sm"), fieldDescription.addon.after && container.append('<span class="input-group-addon">' + fieldDescription.addon.after + "</span>"), 
            fieldDescription.addon.before && container.prepend('<span class="input-group-addon">' + fieldDescription.addon.before + "</span>")); else if (myInput = $('<input type="text" class="form-control input-sm selectionAllowed" >'), 
            myInput.attr("placeholder", fieldDescription.defaulttext || ""), container.append(myInput), 
            myInput.val(myVal), fieldDescription.autocomplete) {
                var source;
                "roles" === fieldDescription.autocomplete.source ? source = that.graph.roleManager.getRoles() : "allColors" === fieldDescription.autocomplete.source && (source = allColors), 
                myInput.attr("autocomplete", "off"), myInput.typeahead({
                    autoSelect: !1,
                    items: "all",
                    minLength: 0,
                    source: source,
                    afterSelect: function(item) {
                        fieldDescription.localid && $(myInput).data("localid", item.localid);
                    }
                }), myInput.on("keydown", function(evt) {
                    (13 == evt.keyCode || 9 == evt.keyCode) && $(".typeahead .active").length && $(".typeahead").is(":visible") && ($(this).val($(".typeahead .active").data("value").name), 
                    fieldDescription.localid && $(this).data(skConstants.LOCAL_ID, $(".typeahead .active").data("value").localid), 
                    evt.preventDefault());
                });
            }
            break;

          case "button":
            viewerMode ? Boolean($(value).attr("value") && $(value).attr("value").length) ? (container = $("<span>" + fieldDescription.value + "</span>"), 
            container.css(fieldDescription.style || "")) : container = null : editorMode && (container = $('<a class="btn btn-default btn-xs">' + fieldDescription.value + "</a>"), 
            container.toggleClass("active", Boolean($(value).attr("value") && $(value).attr("value").length)), 
            myInput = container, myInput.val(myVal), myInput.on("click", function() {
                $(this).toggleClass("active"), $(this).hasClass("active") ? $(this).val(fieldDescription.value) : $(this).val("");
            }));
            break;

          case "colorpicker":
            if (editorMode) {
                container.append($('<div class="colorPicker"><span class="editorSubTitle">Background color</span><div class="cpList" style="text-align:center;"></div></div>'));
                var currentColor, state = that.graph.view.getState(that.cell);
                currentColor = state ? state.style.fillColor || "transparent" : "transparent";
                var recentlyUsed = window.localStorage.getItem("skore.colors.recentlyUsed");
                recentlyUsed = recentlyUsed ? recentlyUsed.split(",") : [ "Red", "Blue", "Aqua", "Lime", "Fuchsia", "Silver" ];
                var placeholder = $(".cpList", container);
                recentlyUsed.forEach(function(e, i) {
                    placeholder.append($('<input id="skCP' + i + '" type="radio" name="color" value="' + e + '" ' + (currentColor === e ? "checked" : "") + " />")), 
                    placeholder.append($('<label for="skCP' + i + '" style="background-color:' + e + ' "></label>'));
                }), placeholder.append($('<input id="skYellow" type="radio" name="color" value="#FCF393" ' + ("#FCF393" === currentColor ? "checked" : "") + '><label for="skYellow" style="background-color: #FCF393"></label><input id="skCPNone" type="radio" name="color" value="transparent" ' + ("transparent" === currentColor ? "checked" : "") + '/><label for="skCPNone" style="background-image: url(images/grid.gif); background-position-x: 4px; background-position-y: 4px;"></label>'));
                var freeText = that.renderField({
                    type: "singleline",
                    name: "allColors",
                    autocomplete: {
                        source: "allColors"
                    }
                }, null, currentColor, viewerMode);
                container.append(freeText);
                var freeTextInput = freeText.is(":input") ? freeText : $("input", freeText);
                $("label", container).on("click", function() {
                    freeTextInput.val($("input#" + $(this).attr("for")).val());
                }), freeTextInput.data("specialtype", "colorpicker");
            }
            break;

          case "url":
            if (viewerMode) {
                var lnk = $("<a/>");
                lnk.addClass("skExtLink"), lnk.attr("href", skUtils.readValue(value, "addr")), lnk.html(skUtils.renderMarkdown(skUtils.readValue(value), !0)), 
                container.append(lnk);
            } else {
                container.append($("<label>Displayed text</label>")), myInput = $('<input type="text" placeholder="Enter plain text" class="form-control input-sm selectionAllowed" >'), 
                container.append(myInput), myInput.val(skUtils.readValue(value)), container.append($("<label>URL</label>"));
                var urlInput = $('<input placeholder="http://" type="text" class="form-control input-sm selectionAllowed" >');
                container.append(urlInput), myInput.data("attributes", [ {
                    key: "addr",
                    input: urlInput
                } ]), urlInput.val(skUtils.readValue(value, "addr"));
            }
            break;

          case skConstants.EXT_CONTENT:
            if (viewerMode) container.append(externalcontent[skUtils.readValue(value, "contenttype")](skUtils.readValue(value))); else {
                container.append($('<div class="input-group input-group-sm"><div class="input-group-btn"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="contenttype">Youtube</span> <span class="caret"></span></button><ul class="dropdown-menu"><li class="dropdown-header">Video</li><li><a data-xcontenttype="youtube" data-xcontentdescription="YouTube Page URL">Youtube</a></li><li><a data-xcontenttype="vimeo" data-xcontentdescription="Vimeo Page URL">Vimeo</a></li><li><a data-xcontenttype="dailymotion" data-xcontentdescription="Dailymotion Page URL">DailyMotion</a></li><li class="dropdown-header">Images</li><li><a data-xcontenttype="image" data-xcontentdescription="Image URL">Image URL</a></li><li class="dropdown-header">Other</li><li><a data-xcontenttype="googlemaps" data-xcontentdescription="Google Maps iFrame Embed">Google Maps</a></li><li><a data-xcontenttype="iframe" data-xcontentdescription="iFrame embed">Generic iFrame</a></li></ul></div><input type="text" class="selectionAllowed form-control"></div>')), 
                myInput = $("input", container), myInput.val(skUtils.readValue(value));
                var label = $("<label></label>");
                container.prepend(label);
                var ctttype = skUtils.readValue(value, "contenttype");
                ctttype || (ctttype = "youtube", $(value).attr("contenttype", "youtube")), $(".contenttype", container).text($("[data-xcontenttype='" + ctttype + "']", container).text()), 
                $(".contenttype", container).val(ctttype), label.text($("[data-xcontenttype='" + ctttype + "']", container).data("xcontentdescription")), 
                $("[data-xcontenttype]", container).on("click", function() {
                    $(".contenttype", container).text($(this).text()), $(".contenttype", container).val($(this).data("xcontenttype")), 
                    label.text($(this).data("xcontentdescription"));
                }), myInput.data("attributes", [ {
                    key: "contenttype",
                    input: $(".contenttype", container)
                } ]);
            }
            break;

          case "checklist":
            viewerMode ? container.append($('<div class="checkbox"><label><input type="checkbox" disabled ' + ("1" === skUtils.readValue(value, "status") ? "checked " : "") + ">" + skUtils.renderMarkdown(skUtils.readValue(value), !0) + "</label></div>")) : (container.append('<div class="input-group"><span class="input-group-addon"><input class="selectionAllowed" type="checkbox" ' + ("1" === skUtils.readValue(value, "status") ? "checked " : "") + '></span><input type="text" class="selectionAllowed form-control"></div>'), 
            myInput = $("input[type=text]", container), myInput.val(skUtils.readValue(value)), 
            myInput.data("attributes", [ {
                key: "status",
                input: $("input[type=checkbox]", container)
            } ]));
            break;

          case "separator":
            if (viewerMode || editorMode && fieldDescription.editable === !1) {
                var style = that.graph.getCellStyle(that.cell), line = "" + style.whoWhatLine;
                line && "1" === line ? container.append($("<div style='height:1px; border-bottom:" + style[mxConstants.STYLE_STROKEWIDTH] + "px solid " + style[mxConstants.STYLE_STROKECOLOR] + "; '></div>")) : line && "css" === line && container.append($("<div style='height:1px; " + decodeURIComponent(style.whoWhatLineCSS) + "'></div>"));
            }
        }
        if (container && (container.addClass("skField " + fieldDescription.type + " "), 
        container.toggleClass("viewerMode", viewerMode), container.toggleClass("editorMode", editorMode), 
        container.attr("data-skcounter", ++counter), viewerMode && container.data("value", value)), 
        myInput && (myInput.addClass("skFormFieldInput"), editorMode && fieldDescription.sortable && value.parentElement && (value = value.parentElement.removeChild(value)), 
        myInput.data("value", value), myInput.data("parentvalue", parentValue), myInput.data("fielddescription", fieldDescription)), 
        fieldDescription.fields && container && container.append(that.renderFields(fieldDescription.fields, value, viewerMode, container)), 
        editorMode && "responsibility" === fieldDescription.name && container) {
            var shownCheckBox = $('<div class="checkbox"><label><input type="checkbox" ' + ("0" === $(value).attr("displayed") ? "" : "checked") + ">Show on box</label></div>");
            container.append(shownCheckBox), myInput.data("attributes", [ {
                key: skConstants.ATTR_SHOWN,
                input: $("input[type=checkbox]", shownCheckBox)
            } ]);
        }
        return container;
    }, skFormRenderer.prototype.save = function(reload) {
        return $(".skFormFieldInput", this.container).each(mxUtils.bind(this, function(i, field) {
            var myVal = $(field).val(), value = $(field).data("value"), parentValue = $(field).data("parentvalue"), fieldDescription = $(field).data("fielddescription"), specialtype = $(field).data("specialtype");
            if (myVal && 0 !== myVal.length) if ("colorpicker" === specialtype) {
                var color = $(field).val();
                if (this.graph.setCellStyles(mxConstants.STYLE_FILLCOLOR, color, [ this.cell ]), 
                this.graph.setCellStyles(mxConstants.STYLE_GRADIENTCOLOR, "none", [ this.cell ]), 
                "transparent" !== color && "none" !== color && "#FCF393" !== color && skUtils.isValidHtmlColor(color)) {
                    var recentlyUsed = window.localStorage.getItem("skore.colors.recentlyUsed");
                    recentlyUsed && (recentlyUsed = recentlyUsed.split(","), recentlyUsed.indexOf(color) > -1 ? (recentlyUsed.splice(recentlyUsed.indexOf(color), 1), 
                    recentlyUsed.unshift(color)) : (recentlyUsed.pop(), recentlyUsed.unshift(color)), 
                    recentlyUsed.splice(6), window.localStorage.setItem("skore.colors.recentlyUsed", recentlyUsed.toString()));
                }
            } else {
                if (myVal !== valueSecretPlaceholder) if (fieldDescription.localid) if ($(field).data(skConstants.LOCAL_ID)) $(value).attr(skConstants.LOCAL_ID, $(field).data(skConstants.LOCAL_ID)); else {
                    var r = this.graph.roleManager.addRole($(field).val());
                    fieldDescription.localid && $(value).attr(skConstants.LOCAL_ID, $(r).attr(skConstants.LOCAL_ID));
                } else $(value).attr("value", myVal);
                var attributes = $(field).data("attributes");
                attributes && $(attributes).each(function(i, att) {
                    var v;
                    v = $(att.input).is(":checkbox") ? $(att.input).is(":checked") ? "1" : "0" : $(att.input).val(), 
                    $(value).attr(att.key, v);
                });
            } else $(value).remove(), value = null;
            value && !value.parentElement && parentValue && parentValue.appendChild(value);
        })), this.originalValue.parentElement.replaceChild(this.editedValue, this.originalValue), 
        this.originalValue = this.editedValue, this.container.empty(), reload !== !1 && this.container.append(this.renderForm(this.fieldDescription, this.parentValue, this.editedValue, !0)), 
        this.saveCallBack && "function" == typeof this.saveCallBack && this.saveCallBack.call(this, this.parentValue, this.editedValue), 
        this.parentValue;
    }, skFormRenderer;
});
define('skGraph/skFormLoader',['require','jquery','skConstants','skGraph/skFormRenderer','./skTemplateDescriptions'],function(require) {
    "use strict";
    function skFormLoader(graph, cell, type, viewerMode, editableProp, saveCallBack) {
        this.container = $("<div />"), this.container.addClass("skFormContainer"), this.container.addClass(type), 
        this.forms = [];
        var template, form, that = this;
        switch (type = type || cell.value.tagName.toLowerCase(), type.toLowerCase()) {
          case skConstants.ROLE:
            var roleManager = graph.roleManager;
            template = graph.templateManager.getTemplate(type), form = new skFormRenderer(graph, null, template, roleManager.roles, cell, viewerMode, {
                editable: graph.isEnabled()
            }, saveCallBack), that.container.append(form.container), that.forms.push(form);
            break;

          case skConstants.ATTACHMENTS:
            var parentValue = cell.value.querySelector(skConstants.ATTACHMENTS);
            if (parentValue) {
                var attList = parentValue.children;
                $(attList).each(function(i, att) {
                    var form = new skFormRenderer(graph, cell, skTemplateDescriptions.attachment[att.tagName.toLowerCase()], parentValue, att, viewerMode, {
                        editable: graph.isEnabled() && editableProp.editable
                    }, saveCallBack);
                    that.forms.push(form), that.container.append(form.container);
                });
            }
            break;

          case "whatbox":
            template = graph.templateManager.getTemplate(skConstants.whatbox), form = new skFormRenderer(graph, cell, template, cell.value, cell.value.querySelector(skConstants.BOX), viewerMode, {
                editable: !0,
                showBtn: !1
            }, saveCallBack), that.forms.push(form), that.container.append(form.container), 
            template = graph.templateManager.getTemplate(skConstants.RESPONSIBILITY), template.showOnlySelected = !0;
            var respsForm = new skFormRenderer(graph, cell, template, cell.value, cell.value.querySelector(skConstants.RESPONSIBILITIES), viewerMode, {
                editable: !0,
                showBtn: !1
            }, saveCallBack);
            that.forms.push(respsForm), that.container.append(respsForm.container);
            break;

          case skConstants.RESPONSIBILITIES:
            template = graph.templateManager.getTemplate(skConstants.RESPONSIBILITY), template.showOnlySelected = !1, 
            form = new skFormRenderer(graph, cell, template, cell.value, cell.value.querySelector(skConstants.RESPONSIBILITIES), viewerMode, {
                editable: !0,
                showBtn: !0,
                showBtnRemove: !1,
                showBtnCancel: !1
            }, saveCallBack), that.forms.push(form), that.container.append(form.container);
            break;

          case "whatbox-light":
            type = "whatbox";

          default:
            template = graph.templateManager.getTemplate(type), form = new skFormRenderer(graph, cell, template, cell.value, cell.value.querySelector(skConstants.BOX), viewerMode, $.extend({}, editableProp), saveCallBack), 
            that.container.append(form.container), that.forms.push(form);
        }
    }
    var $ = require("jquery"), skConstants = require("skConstants"), skFormRenderer = require("skGraph/skFormRenderer"), skTemplateDescriptions = require("./skTemplateDescriptions");
    return skFormLoader.prototype.forms = null, skFormLoader.prototype.container = null, 
    skFormLoader.prototype.save = function(reload) {
        this.forms.forEach(function(form) {
            form.save(reload);
        });
    }, skFormLoader.prototype.getFullForm = function() {
        return this.container[0];
    }, skFormLoader;
});
define('skGraph/skAttachments',['require','skGraph/skTemplateDescriptions','skGraph/skFormLoader','skGraph/skFormRenderer','skConstants','jquery','mxUtils'],function(require) {
    "use strict";
    var skTemplateDescriptions = require("skGraph/skTemplateDescriptions"), skFormLoader = require("skGraph/skFormLoader"), skFormRenderer = require("skGraph/skFormRenderer"), skConstants = require("skConstants"), $ = require("jquery"), mxUtils = require("mxUtils"), renderAttachments = function(graph, cell, type) {
        var form;
        switch (type) {
          case skConstants.RESPONSIBILITIES:
            form = new skFormLoader(graph, cell, type, !0, {
                editable: !0,
                showBtn: !0
            }, function() {
                delete graph.cellHTMLCache[cell.id], graph.updateCellsSize([ cell ]);
            });
            break;

          default:
            form = new skFormLoader(graph, cell, type, !0, {
                editable: !0,
                showBtn: !0
            }), $(".newAttaButtonGroup").show();
        }
        return form;
    }, getAttachmentTypes = function() {
        return Object.keys(skTemplateDescriptions.attachment);
    }, createNewAttachment = function(graph, cell, fieldName) {
        var field = skTemplateDescriptions.attachments[fieldName];
        if (field) {
            var allAttachments = cell.value.querySelector("attachments");
            allAttachments || (allAttachments = mxUtils.createXmlDocument().createElement("attachments"), 
            cell.value.appendChild(allAttachments));
            var tmpValue = mxUtils.createXmlDocument().createElement(field.name);
            allAttachments.appendChild(tmpValue);
            var form = new skFormRenderer(graph, cell, field, allAttachments, tmpValue, !1, {
                editable: !0,
                showBtn: !0
            }, null);
            return form;
        }
    };
    return {
        renderAttachments: renderAttachments,
        getAttachmentTypes: getAttachmentTypes,
        createNewAttachment: createNewAttachment
    };
});
define('skGraph/skSearch',['require','jquery','mxCellState','mxConstants','mxUtils','skConstants','skShell','skUtils'],function(require) {
    "use strict";
    function skSearch(graphManager) {
        this.graphManager = graphManager;
    }
    var $ = require("jquery"), mxCellState = require("mxCellState"), mxConstants = require("mxConstants"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skShell = require("skShell"), skUtils = require("skUtils");
    return skSearch.prototype.graphManager = null, skSearch.prototype.prevKeyword = "", 
    skSearch.prototype.clearSearch = function() {
        this.prevKeyword = "", this.performSearch("");
    }, skSearch.prototype.highlightedItems = [], skSearch.prototype.performSearch = function(keyword, searchParams, callback) {
        var tmp, cells;
        if (keyword && keyword.length) {
            if (cells = [], isNaN(parseInt(keyword, 10)) || (this.prevKeyword = ""), "" !== this.prevKeyword && keyword.startsWith(this.prevKeyword)) cells = [], 
            tmp = this.results.map(function(obj) {
                return obj.cell;
            }), cells = tmp.filter(function(item, i, tmp) {
                return i == tmp.indexOf(item);
            }); else {
                if ("currentonly" === searchParams.levels) tmp = this.graphManager.getCurrentGraph().getDefaultParent().children, 
                tmp && (cells = tmp.filter(function(cell) {
                    return cell.isVertex();
                })); else {
                    var parent = null;
                    "withchildren" === searchParams.levels && (parent = this.graphManager.getCurrentGraph().getDefaultParent()), 
                    cells = this.graphManager.getCurrentGraph().model.filterDescendants(function(cell) {
                        return cell.isVertex();
                    }, parent), parent && delete cells[cells.indexOf(this.graphManager.getCurrentGraph().getDefaultParent())];
                }
                cells = $.map(cells, function(cell) {
                    return cell && (cell.isWhatbox() && searchParams.whatbox || cell.isWhybox() && searchParams.whybox || cell.isStickynote() && searchParams.stickynote) ? cell : void 0;
                });
            }
            this.prevKeyword = keyword, this.unhighlight(), this.results = [];
            var that = this;
            if (!cells) return void skUtils.reflowStyleCss();
            keyword = keyword.toLowerCase(), cells.forEach(mxUtils.bind(this, function(cell) {
                searchParams.id && cell.id.toLowerCase() === keyword && this.results.push({
                    cell: cell,
                    nodeType: "id",
                    nodeValue: keyword
                }), (searchParams.whatbox || searchParams.whybox || searchParams.stickynote) && that.searchInNodes(cell.value.querySelectorAll(skConstants.BOX), keyword, cell, skConstants.BOX, searchParams), 
                searchParams.responsibilities && that.searchInNodes(cell.value.querySelectorAll(skConstants.RESPONSIBILITIES), keyword, cell, skConstants.RESPONSIBILITIES, searchParams), 
                searchParams.attachments && that.searchInNodes(cell.value.querySelectorAll(skConstants.ATTACHMENTS), keyword, cell, skConstants.ATTACHMENTS, searchParams);
            })), this.results.forEach(mxUtils.bind(this, function(result) {
                result.cellIsVisible = null !== this.graphManager.getCurrentGraph().view.getState(result.cell);
            })), this.highlightAllSearchResult(keyword, this.results, searchParams), callback && "function" == typeof callback && callback(keyword, this.results, searchParams);
        } else this.unhighlight(), this.keyword = "", this.prevKeyword = "";
        skUtils.reflowStyleCss();
    }, skSearch.prototype.unhighlight = function() {
        this.highlightedItems.forEach(function(e) {
            e.state instanceof mxCellState && e.state.shape && (e.state.style[mxConstants.STYLE_STROKECOLOR] = skShell.colors.box, 
            e.state.shape.apply(e.state), $(".actionIcon", e.state.icon.enterDetail.node).removeClass("highlight"), 
            e.state.shape.reconfigure()), e.state instanceof $ && e.state.removeClass("highlight");
        }), this.highlightedItems = [], $(".skSearchable").unhighlight();
    }, skSearch.prototype.searchInAttributes = function(element, keyword, cell, specialtype, searchParams) {
        for (var i = 0; i < element.attributes.length; i++) {
            var valToSearch;
            valToSearch = "role" === element.nodeName && "localid" === element.attributes[i].nodeName ? skUtils.readValue(this.graphManager.getCurrentGraph().roleManager.roles.querySelector("role[" + skConstants.LOCAL_ID + "='" + skUtils.readValue(element, skConstants.LOCAL_ID) + "']")) : element.attributes[i].value.toLowerCase(), 
            searchParams.exactmatch ? valToSearch === keyword && this.results.push({
                cell: cell,
                nodeType: specialtype,
                nodeValue: element.attributes[i]
            }) : valToSearch.indexOf(keyword) >= 0 && this.results.push({
                cell: cell,
                nodeType: specialtype,
                nodeValue: element.attributes[i]
            });
        }
    }, skSearch.prototype.searchInNodes = function(nodes, keyword, newCell, specialtype, searchParams) {
        var that = this;
        $(nodes).each(function(i, node) {
            1 == node.nodeType && (that.searchInAttributes(node, keyword, newCell, specialtype, searchParams), 
            that.searchInNodes(node.childNodes, keyword, newCell, specialtype, searchParams));
        });
    }, skSearch.prototype.highlightAllSearchResult = function(keyword, results, searchParams) {
        searchParams.responsibilities && $(".responsibility").highlight(keyword), searchParams.whatbox && $(".whatbox .skSearchable .text").highlight(keyword), 
        searchParams.whybox && $(".whybox .skSearchable .text").highlight(keyword), searchParams.stickynote && $(".stickynote .skSearchable").highlight(keyword), 
        searchParams.attachments && $("#skModal_Attachments .skSearchable").highlight(keyword), 
        $(".skTitle.skSearchable").highlight(keyword), this.highlightedItems = [];
        var that = this;
        null != results && results.length > 0 && $(results).each(function(i, result) {
            var state;
            if (result.cellIsVisible) {
                if (result.nodeType == skConstants.ATTACHMENTS && (state = that.graphManager.getCurrentGraph().getView().getState(result.cell), 
                null != state)) {
                    that.addResultToList(result.cell, !1);
                    var domNode = $(".actionIcon", state.icon.attachment.node).addClass("highlight");
                    that.highlightedItems.push({
                        cell: result.cell,
                        state: domNode
                    });
                }
            } else {
                for (var tmp = result.cell; null == that.graphManager.getCurrentGraph().view.getState(tmp) && tmp.parent; ) tmp = tmp.parent;
                state = that.graphManager.getCurrentGraph().getView().getState(tmp), state && null != state.shape && (state.style[mxConstants.STYLE_STROKECOLOR] = skShell.colors.BOX_HIGHLIGHT, 
                state.shape.apply(state), $(".actionIcon", state.icon.enterDetail.node).addClass("highlight"), 
                state.shape.redraw(), state.shape.reconfigure(), that.highlightedItems.push({
                    cell: tmp,
                    state: state
                }));
            }
        });
    }, skSearch;
});
define('skGraph/skGraphCellUtil',['require','jquery','mxCell','mxEventObject','mxUtils','skConstants','skUtils','skGraph/skFormLoader'],function(require) {
    "use strict";
    var $ = require("jquery"), mxCell = require("mxCell"), mxEventObject = require("mxEventObject"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skUtils = require("skUtils"), skFormLoader = require("skGraph/skFormLoader");
    mxCell.prototype.getCellType = function() {
        return this.isWhatbox() ? skConstants.whatbox : this.isWhybox() ? skConstants.whybox : this.isStickynote() ? skConstants.stickynote : this.isGroup() ? skConstants.group : this.isEdge() ? this.style.split(";")[0] === skConstants.LINE_WY2S ? skConstants.LINE_WY2S : "line" : "unknown cell type";
    }, mxCell.prototype.isWhybox = function() {
        return this.value && this.value.tagName == skConstants.whybox;
    }, mxCell.prototype.isWhatbox = function() {
        return this.value && this.value.tagName == skConstants.whatbox;
    }, mxCell.prototype.isWhatboxDetailed = function() {
        return this.value && this.isWhatbox() && this.children && this.children.length > 0;
    }, mxCell.prototype.isImage = function() {
        return this.value && this.value.tagName == skConstants.image;
    }, mxCell.prototype.numberOfAttachments = function() {
        return $("attachments", this.value).children().length;
    }, mxCell.prototype.numberOfSurveys = function() {
        var count = 0;
        return this.value && (count = this.value.getElementsByTagName(skConstants.SURVEY).length), 
        count;
    }, mxCell.prototype.isGroup = function() {
        return this.value && this.value.tagName == skConstants.group;
    }, mxCell.prototype.isStickynote = function() {
        return this.value && this.value.tagName == skConstants.stickynote;
    }, mxCell.prototype.createHtmlContent = function(graph) {
        var id = parseInt(this.id, 10);
        if (!graph.cellHTMLCache[id]) {
            var formLoader = new skFormLoader(graph, this, this.value.tagName, !0, {
                editable: graph.isEnabled(),
                showBtn: !1
            }), t = "";
            return this.value && (t = formLoader.getFullForm()), graph.labelMode && "htmlstring" == graph.labelMode ? t = t.outerHTML : graph.fireEvent(new mxEventObject("checkCellLink", "cellHTML", t)), 
            graph.cellHTMLCache[this.id] = t, t;
        }
        return graph.cellHTMLCache[this.id];
    }, mxCell.prototype.getAttachmentString = function(graph, markdown) {
        var attachments = [];
        return this.isWhatbox() && this.numberOfAttachments() > 0 ? ($("attachments > *", this.value).each(function(i, att) {
            if (att.nodeName === skConstants.RESPONSIBILITY) {
                var v = skUtils.readValue(graph.roleManager.getRoleByLocalID(skUtils.readValue($("role", att)[0], skConstants.LOCAL_ID)));
                if (markdown && (v = skUtils.renderMarkdown(v, !0)), $("tag", att).length) {
                    var tags = [];
                    $("tag", att).each(function(j, tag) {
                        tags.push(skUtils.readValue(tag));
                    }), v += " (", v += tags.join(", "), v += ")";
                }
                attachments.push(v);
            } else if (att.nodeName === skConstants.EXT_CONTENT) attachments.push(mxUtils.htmlEntities(skUtils.readValue(att))); else {
                var t = skUtils.readValue(att);
                markdown && (t = skUtils.renderMarkdown(t, !0)), attachments.push(t);
            }
        }), attachments.join(skConstants.SEPARATOR)) : "";
    }, mxCell.prototype.getBoxText = function(markdown, cutTheFat) {
        markdown = void 0 !== markdown ? markdown : !1, cutTheFat = void 0 !== cutTheFat ? cutTheFat : !1;
        var text = "";
        return text = skUtils.readValue($("box text", this.value)), markdown ? skUtils.renderMarkdown(text, cutTheFat) : text;
    }, mxCell.prototype.getBoxRoles = function(itemNumber, parentValue) {
        var res = [];
        return itemNumber = itemNumber || 0, parentValue = void 0 !== parentValue ? parentValue : this.value, 
        this.value && (itemNumber ? res.push(skUtils.readValue($(skConstants.ROLE, this.value)[itemNumber], skConstants.LOCAL_ID)) : $(skConstants.ROLE, this.value).each(function(i, role) {
            res.push(skUtils.readValue(role, skConstants.LOCAL_ID));
        })), res;
    }, mxCell.prototype.getInboundsText = function() {
        var text = "";
        if (this.edges && this.edges.length > 0) {
            var me = this;
            $(this.edges).each(function(i, e) {
                e.target == me && (text += "" === text ? e.source.getBoxText() : " | " + e.source.getBoxText());
            });
        }
        return text;
    }, mxCell.prototype.getOutboundsText = function() {
        var text = "";
        if (this.edges && this.edges.length > 0) {
            var me = this;
            $(this.edges).each(function(i, e) {
                e.source == me && (text += "" === text ? e.target.getBoxText() : " | " + e.target.getBoxText());
            });
        }
        return text;
    }, mxCell.prototype.getInboundsCells = function(outbounds) {
        var cells = [], currentCell = this;
        return $(this.edges).each(function(i, edge) {
            outbounds ? edge.source == currentCell && cells.push(edge.target) : edge.target == currentCell && cells.push(edge.source);
        }), cells;
    }, mxCell.prototype.getInboundsCellsSameType = function(outbounds) {
        var cells;
        cells = this.getInboundsCells(outbounds);
        var cellsSameType = [];
        return $(cells).each(function(i, cell) {
            cellsSameType = cellsSameType.concat(cell.getInboundsCells(outbounds));
        }), cellsSameType;
    }, mxCell.prototype.getOutboundsCells = function() {
        return this.getInboundsCells(!0);
    }, mxCell.prototype.getOutboundsCellsSameType = function() {
        return this.getInboundsCellsSameType(!0);
    }, mxCell.prototype.getSuppliers = function(graph, customers) {
        var supplierList = [], cells = this.getInboundsCellsSameType(customers);
        return $(cells).each(function(i, cell) {
            supplierList = supplierList.concat(cell.getBoxRoles());
        }), supplierList;
    }, mxCell.prototype.getCustomers = function() {
        return this.getSuppliers(!0);
    }, mxCell.prototype.getSuppliersUniqueList = function(customers) {
        return $.distinct(this.getSuppliers(customers));
    }, mxCell.prototype.getCustomersUniqueList = function() {
        return this.getSuppliersUniqueList(!0);
    }, mxCell.prototype.getSuppliersUniqueListText = function(customers) {
        return this.getSuppliersUniqueList(customers).join(skConstants.SEPARATOR);
    }, mxCell.prototype.getCustomersUniqueListText = function() {
        return this.getSuppliersUniqueListText(!0);
    }, mxCell.prototype.getParents = function() {
        for (var parents = [], p = this.getParent(); p && 0 !== p.id; ) parents.unshift(p), 
        p = p.getParent();
        var parentTexts = parents.map(function(cell) {
            return cell.getBoxText();
        });
        return {
            toString: parentTexts.join(" | "),
            cells: parents,
            count: parents.length
        };
    }, mxCell.prototype.getSuppliersAndCustomers = function() {
        return {
            suppliersUniqueResourcesList: "resource1 | resource2 | resource3",
            suppliersUniqueResourcesListCount: 3,
            suppliersUniqueResourcesListWithCount: [ {
                resource: "resource1",
                count: 3,
                cells: [ "mxCell", "mxCell", "mxCell" ]
            }, {
                resource: "resource2",
                count: 1,
                cell: [ "mxCell" ]
            } ],
            supplierUniqueResources: []
        };
    };
});
define('skGraphEditor/skGraphEditor',['require','jquery','mxCellMarker','mxCellState','mxClient','mxConnectionConstraint','mxConnectionHandler','mxConstants','mxConstraintHandler','mxEdgeHandler','mxEvent','mxEventObject','mxGraph','mxMultiplicity','mxPoint','mxRubberband','mxShape','mxStencilRegistry','mxUndoManager','mxUtils','mxVertexHandler','skConstants','skGraph/skGraph','skShell','skUtils'],function(require) {
    "use strict";
    var $ = require("jquery"), mxCellMarker = require("mxCellMarker"), mxCellState = require("mxCellState"), mxClient = require("mxClient"), mxConnectionConstraint = require("mxConnectionConstraint"), mxConnectionHandler = require("mxConnectionHandler"), mxConstants = require("mxConstants"), mxConstraintHandler = require("mxConstraintHandler"), mxEdgeHandler = require("mxEdgeHandler"), mxEvent = require("mxEvent"), mxEventObject = require("mxEventObject"), mxGraph = require("mxGraph"), mxMultiplicity = require("mxMultiplicity"), mxPoint = require("mxPoint"), mxRubberband = require("mxRubberband"), mxShape = require("mxShape"), mxStencilRegistry = require("mxStencilRegistry"), mxUndoManager = require("mxUndoManager"), mxUtils = require("mxUtils"), mxVertexHandler = require("mxVertexHandler"), skConstants = require("skConstants"), skGraph = require("skGraph/skGraph"), skShell = require("skShell"), skUtils = require("skUtils");
    skGraph.prototype.isCellEditable = function(cell) {
        return this.isEnabled() && !cell.isEdge();
    }, skGraph.prototype.isCellMovable = function() {
        return this.isEnabled();
    };
    var mxGraphMoveCells = mxGraph.prototype.moveCells;
    skGraph.prototype.moveCells = function(cells) {
        if (1 == cells.length && cells[0].isWhatbox()) {
            var myCell = cells[0];
            if (myCell.edges) for (var i = 0; i < myCell.edges.length; i++) {
                var edge = myCell.edges[i];
                edge.target == myCell && 1 == edge.source.edges.length ? cells.push(edge.source) : edge.source == myCell && 1 == edge.target.edges.length && cells.push(edge.target);
            }
        }
        return mxGraphMoveCells.apply(this, arguments);
    }, skGraph.prototype.was = {}, skGraph.prototype.setEnabled = function(enabled) {
        if (this.enabled = enabled, this.isPrintPreview !== !0) {
            if (this.enabled) {
                if (this.pageVisible = this.was.pageVisible || !0, this.pageBreaksVisible = this.was.pageBreaksVisible || !0, 
                !this.getDefaultParent().value) {
                    var value = skUtils.createBoxElement(skConstants.title), text = mxUtils.createXmlDocument().createElement(skConstants.TEXT);
                    value.querySelector(skConstants.BOX).appendChild(text), this.getDefaultParent().value = value;
                }
                this.undoManager ? this.undoManager.clear() : this.createUndoManager();
            } else this.was.pageVisible = this.pageVisible, this.pageVisible = !1, this.was.pageBreaksVisible = this.pageBreaksVisible, 
            this.pageBreaksVisible = !1;
            if (enabled) {
                var rubberband = new mxRubberband(this);
                this.getRubberband = function() {
                    return rubberband;
                };
            } else this.getRubberband = null;
            this.updateGraphComponents(), this.refresh(), this.resetScrollbars(), this.isOutline !== !0 && this.fireEvent(new mxEventObject("graphEnabled", "enabled", enabled, "graph", this));
        }
    }, skGraph.prototype.initGraphEditor = function() {
        this.getSelectionModel().addListener(mxEvent.CHANGE, mxUtils.bind(this, function(sender, evt) {
            var graph = this;
            $(".box").removeClass("editable"), graph.cellHighlighter.toggleHighlightOff();
            var cellsRemovedFromSelection = evt.getProperty("added");
            cellsRemovedFromSelection && cellsRemovedFromSelection.length && cellsRemovedFromSelection.forEach(function(cell) {
                graph.cellRenderer.destroyIconsIfNecessary(graph.view.getState(cell));
            });
            var cellsAdded = evt.getProperty("removed");
            if (cellsAdded && cellsAdded.length) for (var i = 0; i < cellsAdded.length; i++) graph.cellRenderer.destroyIconsIfNecessary(graph.view.getState(cellsAdded[i])); else cellsAdded = [];
            cellsAdded && (0 === cellsAdded.length && ($(".skSideMenu [data-loadtype=stylecell]").addClass("disabled"), 
            $(".box-pluralise").html("")), 1 == cellsAdded.length && ($(".skSideMenu [data-loadtype=stylecell]").removeClass("disabled"), 
            $(".box-pluralise").html(""), graph.view.createState(cellsAdded[0], !0), graph.cellRenderer.redraw(graph.view.getState(cellsAdded[0]), !0), 
            $("#box" + cellsAdded[0].id).addClass("editable"), $("#skModal_Attachments").length && graph.fireEvent(new mxEventObject("openAttachment", "cell", cellsAdded[0]))), 
            graph.isEnabled() && cellsAdded.length > 1 && ($(".skSideMenu [data-loadtype=stylecell]").removeClass("disabled"), 
            $(".box-pluralise").html("es")));
        })), this.getModel().addListener(mxEvent.CHANGE, mxUtils.bind(this, function() {
            this.setDocumentEdited(this.isEnabled() && !0);
        })), this.addListener("graphBackgroundChanged", mxUtils.bind(this, function() {
            this.getView().validateBackground();
        })), this.connectionHandler.addListener(mxEvent.CONNECT, mxUtils.bind(this, function(sender, evt) {
            var target2, source2, graph = this, edge = evt.getProperty("cell"), source = graph.getModel().getTerminal(edge, !0), target = graph.getModel().getTerminal(edge, !1);
            graph.view.getState(target) || (0 === sender.sourceConstraint.point.y ? (target2 = edge.target, 
            source2 = edge.source, edge.target = source2, edge.source = target2, graph.setConnectionConstraint(edge, source, !1, new mxConnectionConstraint(new mxPoint(sender.sourceConstraint.point.x, 0))), 
            graph.setConnectionConstraint(edge, target, !0, new mxConnectionConstraint(new mxPoint(.5, 1)))) : 1 === sender.sourceConstraint.point.x && .5 === sender.sourceConstraint.point.y ? (graph.setConnectionConstraint(edge, source, !0, sender.sourceConstraint), 
            graph.setConnectionConstraint(edge, target, !1, new mxConnectionConstraint(new mxPoint(0, .5)))) : 1 === sender.sourceConstraint.point.y ? (graph.setConnectionConstraint(edge, source, !0, new mxConnectionConstraint(new mxPoint(sender.sourceConstraint.point.x, sender.sourceConstraint.point.y))), 
            graph.setConnectionConstraint(edge, target, !1, new mxConnectionConstraint(new mxPoint(.5, 0)))) : 0 === sender.sourceConstraint.point.x && .5 === sender.sourceConstraint.point.y && (target2 = edge.target, 
            source2 = edge.source, edge.target = source2, edge.source = target2, graph.setConnectionConstraint(edge, source, !0, new mxConnectionConstraint(new mxPoint(1, .5))), 
            graph.setConnectionConstraint(edge, target, !1, new mxConnectionConstraint(new mxPoint(0, .5))))), 
            -1 === edge.style.indexOf(mxConstants.STYLE_ENTRY_Y) && (edge.style = mxUtils.addStylename(edge.style, mxConstants.STYLE_ENTRY_Y + "=0.5"), 
            edge.style = mxUtils.addStylename(edge.style, mxConstants.STYLE_ENTRY_X + "=0"));
        })), this.addListener(mxEvent.CELL_CONNECTED, mxUtils.bind(this, function(sender, event) {
            var graph = this, edge = event.getProperty("edge"), source = event.getProperty("source"), terminal = event.getProperty("terminal"), otherEnd = edge.getTerminal(!source), stylename = otherEnd && otherEnd.isStickynote() || terminal.isStickynote() ? skConstants.LINE_WY2S : "";
            skUtils.setCellsStyleName(graph.model, [ edge ], stylename), graph.orderCells(!0, [ event.getProperty("edge") ]);
        })), this.addBoxConnectionConstraints = mxUtils.bind(this, function() {
            var graph = this;
            graph.removeBoxConnectionConstraints(), graph.multiplicities.push(new mxMultiplicity(!0, skConstants.whatbox, null, null, 0, "n", [ skConstants.whybox, skConstants.stickynote, skConstants.group ], "", "You need to insert a whybox first")), 
            graph.multiplicities.push(new mxMultiplicity(!0, skConstants.whybox, null, null, 0, "n", [ skConstants.whatbox, skConstants.stickynote, skConstants.group ], "", "You need to insert a whatbox first"));
        }), this.removeBoxConnectionConstraints = mxUtils.bind(this, function() {
            this.multiplicities = [];
        }), this.addBoxConnectionConstraints(), this.connectionHandler.tapAndHoldTolerance = 16, 
        this.connectionHandler.createEdgeState = mxUtils.bind(this, function() {
            var graph = this, edge = graph.createEdge(null, null, null, null, null);
            return new mxCellState(graph.view, edge, graph.getCellStyle(edge));
        }), this.addListener(mxEvent.DOUBLE_CLICK, mxUtils.bind(this, function(sender, evt) {
            var cell = evt.getProperty("cell");
            cell && cell.isEdge() && this.clearWaypoints([ cell ]), evt.consume();
        })), this.addListener("styleSheetUpdated", mxUtils.bind(this, function() {
            this.cellHTMLCache = [], this.updateCellsSize(), this.refresh();
        }));
    }, mxConnectionHandler.prototype.movePreviewAway = !1, mxConstraintHandler.prototype.isEventIgnored = function(me) {
        return mxEvent.isControlDown(me.evt) || mxClient.IS_MAC && me.evt.metaKey ? !0 : !1;
    };
    var mxConstraintHandlerUpdate = mxConstraintHandler.prototype.update;
    mxConstraintHandler.prototype.update = function() {
        mxConstraintHandlerUpdate.apply(this, arguments);
        var focusIcons = this.focusIcons;
        if (null != focusIcons && focusIcons.length) for (var i = 0, l = focusIcons.length; l > i; i += 1) focusIcons[i].node.style.cursor = "pointer", 
        this.focusPoints[i].DONOTDISPLAY && (focusIcons[i].node.style.display = "none");
    }, mxCellState.prototype.add_box_after_box_state = function(dropPoint, positionForNewBox, sourceConstraint, createLine) {
        var distance, width, height, value, box, style_box, x = dropPoint ? dropPoint.x : null, y = dropPoint ? dropPoint.y : null, graph = this.view.graph, styleWB = graph.getStylesheet().styles[skConstants.whatbox], styleYB = graph.getStylesheet().styles[skConstants.whybox];
        if (this.cell.isWhybox()) distance = "left" === positionForNewBox ? skConstants.DIST_W2Y : skConstants.DIST_Y2W, 
        width = parseInt(styleWB.width, 10), height = parseInt(styleWB.height, 10), value = skUtils.createBoxElement(skConstants.whatbox), 
        style_box = skConstants.whatbox; else {
            if (!this.cell.isWhatbox() && !this.cell.isGroup()) return null;
            distance = "left" === positionForNewBox ? skConstants.DIST_Y2W : skConstants.DIST_W2Y, 
            width = parseInt(styleYB.width, 10), height = parseInt(styleYB.height, 10), value = skUtils.createBoxElement(skConstants.whybox), 
            style_box = skConstants.whybox;
        }
        if (sourceConstraint) {
            var scp = sourceConstraint.point;
            0 === scp.y ? positionForNewBox = "top" : 1 === scp.x ? positionForNewBox = "right" : 1 === scp.y ? positionForNewBox = "bottom" : 0 === scp.x && (positionForNewBox = "left");
        }
        var FACTOR = 1.5, east = this.origin.x + this.cell.geometry.width, farEast = east + FACTOR * this.cell.geometry.width, west = this.origin.x, farWest = west - FACTOR * this.cell.geometry.width, north = this.origin.y, farNorth = north - FACTOR * this.cell.geometry.height, south = this.origin.y + this.cell.geometry.height, farSouth = south + FACTOR * this.cell.geometry.height;
        switch (positionForNewBox) {
          case "top":
            (!dropPoint || dropPoint.x > west && dropPoint.x < east) && (x = west + this.cell.geometry.width / 2 - width / 2, 
            (!dropPoint || dropPoint.y < north && dropPoint.y > farNorth) && (y = north - distance - height));
            break;

          case "right":
            (!dropPoint || dropPoint.y > north && dropPoint.y < south) && (y = this.origin.y, 
            (!dropPoint || dropPoint.x > east && dropPoint.x < farEast) && (x = east + distance));
            break;

          case "bottom":
            (!dropPoint || dropPoint.x > west && dropPoint.x < east) && (x = west + this.cell.geometry.width / 2 - width / 2, 
            (!dropPoint || dropPoint.y > south && dropPoint.y < farSouth) && (y = south + distance));
            break;

          case "left":
            (!dropPoint || dropPoint.y > north && dropPoint.y < south) && (x = west - distance - width, 
            (!dropPoint || dropPoint.x < west && dropPoint.x > farWest) && (y = this.origin.y));
        }
        if (this.cell.parent != graph.getDefaultParent()) {
            var p = this.cell.parent;
            x -= p.geometry.x, y -= p.geometry.y;
        }
        graph.getModel().beginUpdate();
        try {
            box = graph.insertVertex(this.cell.parent, null, value, x, y, width, height, style_box), 
            box.style_box === skConstants.whatbox && (box.collapsed = !0);
            var myEdge;
            value = null;
            var style_line = "line";
            if (createLine === !0) switch (positionForNewBox) {
              case "left":
                myEdge = graph.insertEdge(this.cell.parent, null, value, box, this.cell, style_line), 
                graph.setConnectionConstraint(myEdge, box, !0, new mxConnectionConstraint(new mxPoint(1, .5), !0)), 
                graph.setConnectionConstraint(myEdge, this.cell, !1, new mxConnectionConstraint(new mxPoint(0, .5), !0));
                break;

              case "top":
                myEdge = graph.insertEdge(this.cell.parent, null, value, box, this.cell, style_line), 
                graph.setConnectionConstraint(myEdge, this.cell, !0, new mxConnectionConstraint(new mxPoint(.5, 1), !0)), 
                graph.setConnectionConstraint(myEdge, box, !1, new mxConnectionConstraint(new mxPoint(.5, 0), !0));
                break;

              case "right":
                myEdge = graph.insertEdge(this.cell.parent, null, value, this.cell, box, style_line), 
                graph.setConnectionConstraint(myEdge, this.cell, !0, new mxConnectionConstraint(new mxPoint(1, .5), !0)), 
                graph.setConnectionConstraint(myEdge, box, !1, new mxConnectionConstraint(new mxPoint(0, .5), !0));
                break;

              case "bottom":
                myEdge = graph.insertEdge(this.cell.parent, null, value, this.cell, box, style_line), 
                graph.setConnectionConstraint(myEdge, this.cell, !0, new mxConnectionConstraint(new mxPoint(.5, 1), !0)), 
                graph.setConnectionConstraint(myEdge, box, !1, new mxConnectionConstraint(new mxPoint(.5, 0), !0));
            }
        } finally {
            graph.getModel().endUpdate();
        }
        return skShell.tutorial && (skShell.tutorial.stepDone(skShell.tutorial.BOX_CREATED_WITH_DND_FROM_CONNECT_ARROW, {
            cell: box
        }), box.isWhybox() ? skShell.tutorial.stepDone(skShell.tutorial.NEXT_Y_BOX_IS_CREATED, {
            cell: box
        }) : box.isWhatbox() && skShell.tutorial.stepDone(skShell.tutorial.W_BOX_IS_CREATED, {
            cell: box
        })), graph.scrollCellToVisible(box), box;
    }, mxCellState.prototype.avoid_superimpose = function(before) {
        var state = this, view = this.view, y = state.origin.y;
        if (state.cell.edges && state.cell.edges.length > 0) {
            var cells_to_avoid = [];
            $(state.cell.edges).each(function(i, e) {
                before ? e.target == state.cell && cells_to_avoid.push(e.source) : e.source == state.cell && cells_to_avoid.push(e.target);
            }), cells_to_avoid.length > 0 && $(cells_to_avoid).each(function(i, e) {
                y == view.getState(e).origin.y && (y += e.geometry.height / 2 + skConstants.DIST_LINES);
            });
        }
        return y;
    };
    var mxGraphSelectCellForEvent = mxGraph.prototype.selectCellForEvent;
    mxGraph.prototype.selectCellForEvent = function(cell, evt) {
        !this.isToggleEvent(evt) && this.isCellSelected(cell) && 1 === this.getSelectionCount() ? this.startEditingAtCell(cell, evt) : mxGraphSelectCellForEvent.apply(this, arguments);
    }, mxConnectionHandler.prototype.createTarget = !0;
    var mxConnectionHandlerupdateEdgeState = mxConnectionHandler.prototype.updateEdgeState;
    mxConnectionHandler.prototype.updateEdgeState = function(current, constraint) {
        constraint || (constraint = new mxConnectionConstraint(new mxPoint(0, .5), !0)), 
        mxConnectionHandlerupdateEdgeState.call(this, current, constraint);
    }, mxConnectionHandler.prototype.select = !1, mxConnectionHandler.prototype.createTargetVertex = function(evt, sourceCell) {
        var graph = this.graph, new_box = null;
        return new_box = graph.view.getState(sourceCell).add_box_after_box_state(this.graph.getPointForEvent(evt), null, this.sourceConstraint), 
        mxEvent.consume(evt), new_box;
    }, mxRubberband.prototype.defaultOpacity = 30, mxVertexHandler.prototype.createSelectionShape = function(bounds) {
        var key = this.state.style[mxConstants.STYLE_SHAPE], stencil = mxStencilRegistry.getStencil(key), shape = null;
        return null != stencil ? (shape = new mxShape(stencil), shape.apply(this.state)) : shape = new this.state.shape.constructor(), 
        shape.outline = !0, shape.bounds = bounds, shape.stroke = this.getSelectionColor(), 
        shape.strokewidth = this.getSelectionStrokeWidth(), shape.isDashed = this.isSelectionDashed(), 
        shape.isShadow = !1, shape;
    };
    var graphCreateGroupCell = mxGraph.prototype.createGroupCell;
    mxGraph.prototype.createGroupCell = function() {
        var group = graphCreateGroupCell.apply(this, arguments);
        return group.setStyle(skConstants.group), group.value = skUtils.createBoxElement(skConstants.group), 
        group.setConnectable(!0), group;
    }, mxGraph.prototype.getCells = function(x, y, width, height, parent, result) {
        if (result = null != result ? result : [], width > 0 || height > 0) {
            var right = x + width, bottom = y + height, left = x, top = y;
            if (parent = parent || this.getDefaultParent(), null != parent) for (var childCount = this.model.getChildCount(parent), i = 0; childCount > i; i++) {
                var cell = this.model.getChildAt(parent, i), state = this.view.getState(cell);
                if (this.isCellVisible(cell) && null != state) {
                    var stateXPlusW = state.x + state.width, stateYplusH = state.y + state.height;
                    state.x >= x && state.y >= y && right >= stateXPlusW && bottom >= stateYplusH || cell.isVertex() && (stateXPlusW >= left && left >= state.x && top <= state.y && bottom >= stateYplusH || stateXPlusW >= left && left >= state.x && top >= state.y && stateYplusH >= top || left <= state.x && right >= stateXPlusW && top >= state.y && stateYplusH >= top || left >= state.x && stateXPlusW >= left && bottom >= state.y && stateYplusH >= bottom || right >= state.x && stateXPlusW >= right && top <= state.y && bottom >= stateYplusH || right >= state.x && stateXPlusW >= right && bottom >= state.y && stateYplusH >= bottom || left <= state.x && right >= stateXPlusW && bottom >= state.y && stateYplusH >= bottom || right >= state.x && stateXPlusW >= right && top >= state.y && stateYplusH >= top) ? result.push(cell) : this.getCells(x, y, width, height, cell, result);
                }
            }
        }
        return result;
    }, mxCellMarker.prototype.enabled = !0, mxCellMarker.prototype.intersects = function() {
        return !1;
    }, mxConnectionHandler.prototype.factoryMethod = function() {}, skGraph.prototype.clearWaypoints = function(myCells) {
        var cells = myCells || this.getSelectionCells();
        if (null != cells) {
            this.getModel().beginUpdate();
            try {
                for (var i = 0; i < cells.length; i++) {
                    var cell = cells[i];
                    if (this.getModel().isEdge(cell)) {
                        var geo = this.getCellGeometry(cell);
                        null != geo && (geo = geo.clone(), geo.points = null, this.getModel().setGeometry(cell, geo));
                    }
                }
            } finally {
                this.getModel().endUpdate();
            }
        }
    }, mxConnectionHandler.prototype.select = !1, mxGraph.prototype.distributeCells = function(horizontal, cells) {
        var i;
        if (null == cells && (cells = this.getSelectionCells()), null != cells && cells.length > 1) {
            var vertices = [], max = null, min = null;
            for (i = 0; i < cells.length; i++) if (this.getModel().isVertex(cells[i])) {
                var state = this.view.getState(cells[i]);
                if (null != state) {
                    var tmp = horizontal ? state.getCenterX() : state.getCenterY();
                    max = null != max ? Math.max(max, tmp) : tmp, min = null != min ? Math.min(min, tmp) : tmp, 
                    vertices.push(state);
                }
            }
            if (vertices.length > 2) {
                vertices.sort(function(a, b) {
                    return horizontal ? a.x - b.x : a.y - b.y;
                });
                var t = this.view.translate, s = this.view.scale;
                min = min / s - (horizontal ? t.x : t.y), max = max / s - (horizontal ? t.x : t.y), 
                this.getModel().beginUpdate();
                try {
                    var dt = (max - min) / (vertices.length - 1), t0 = min;
                    for (i = 1; i < vertices.length - 1; i++) {
                        var geo = this.getCellGeometry(vertices[i].cell);
                        t0 += dt, null != geo && (geo = geo.clone(), horizontal ? geo.x = Math.round(t0 - geo.width / 2) : geo.y = Math.round(t0 - geo.height / 2), 
                        this.getModel().setGeometry(vertices[i].cell, geo));
                    }
                } finally {
                    this.getModel().endUpdate();
                }
            }
        }
        return cells;
    }, skGraph.prototype.createUndoManager = function() {
        var graph = this, undoManager = new mxUndoManager();
        this.undoListener = function(sender, evt) {
            undoManager.undoableEditHappened(evt.getProperty("edit"));
        };
        var listener = mxUtils.bind(this, function() {
            this.undoListener.apply(this, arguments);
        });
        graph.getModel().addListener(mxEvent.UNDO, listener), graph.getView().addListener(mxEvent.UNDO, listener);
        var undoHandler = function(sender, evt) {
            for (var cand = graph.getSelectionCellsForChanges(evt.getProperty("edit").changes), model = graph.getModel(), cells = [], i = 0; i < cand.length; i++) (model.isVertex(cand[i]) || model.isEdge(cand[i])) && null != graph.view.getState(cand[i]) && (cells.push(cand[i]), 
            graph.cellHTMLCache[parseInt(cand[i].id, 10)] = null);
            graph.setSelectionCells(cells);
        };
        undoManager.addListener(mxEvent.UNDO, undoHandler), undoManager.addListener(mxEvent.REDO, undoHandler), 
        graph.undoManager = undoManager;
    }, skGraph.prototype.documentEdited = !1, skGraph.prototype.setDocumentEdited = function(value) {
        this.documentEdited = value;
    }, mxEdgeHandler.prototype.removeEnabled = !0, mxEdgeHandler.prototype.mergeRemoveEnabled = !0, 
    mxEdgeHandler.prototype.dblClickRemoveEnabled = !0, mxEdgeHandler.prototype.straightRemoveEnabled = !0;
    var cp = mxEdgeHandler.prototype.changePoints;
    mxEdgeHandler.prototype.changePoints = function() {
        cp.apply(this, arguments), skShell.skStatus.setAndDontShowAgain("Line path changed!<br/><hr>To reset to original path,<br/><span style='border-bottom:black double'>double click</span> on one of the handles.", "DONTSHOWAGAINlineHasChanged");
    };
});
define('skGraphEditor/skGraphEditorCellEditor',['require','jquery','mxCellEditor','mxEventObject','mxUtils','skConstants','skShell','skGraph/skFormLoader'],function(require) {
    "use strict";
    var $ = require("jquery"), mxCellEditor = require("mxCellEditor"), mxEventObject = require("mxEventObject"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skShell = require("skShell"), skFormLoader = require("skGraph/skFormLoader");
    mxCellEditor.prototype.autoSize = !1, mxCellEditor.prototype.positionEditor = function(cell) {
        if (this.wrapperDiv && (this.wrapperDiv.style.left = this.graph.view.getState(cell).x - 1 + "px", 
        this.wrapperDiv.style.position = "absolute", this.wrapperDiv.style.top = this.graph.view.getState(cell).y + "px", 
        cell.value && cell.value.tagName && "title" !== cell.value.tagName)) {
            var wsys = this.graph.getStylesheet().styles["sys-" + cell.value.tagName].width || skConstants.WB_WIDTH + 20, w = Math.max(cell.geometry.width + 2, wsys);
            this.wrapperDiv.style.minWidth = w + "px";
        }
    };
    var mxCellEditorStartEditing = mxCellEditor.prototype.startEditing;
    mxCellEditor.prototype.startEditing = function(cell, trigger) {
        if (cell.isImage()) return void skShell.setStatus("image cannot be changed. Please import a new one.");
        mxCellEditorStartEditing.apply(this, arguments), $(".skBoxActionIcon").hide();
        var that = this;
        if (this.graph.isHtmlLabel(cell)) {
            this.myForm = new skFormLoader(this.graph, cell, null, !1, !1), this.textarea.style.display = "none", 
            this.wrapperDiv = document.createElement("div"), this.wrapperDiv.className = "cellEditor", 
            this.graph.view.scale > 1 && (this.wrapperDiv.style["-webkit-transform-origin"] = "top left", 
            this.wrapperDiv.style["-webkit-transform"] = "scaleX(" + this.graph.view.scale + ") scaleY(" + this.graph.view.scale + ")"), 
            this.graph.getDefaultParent() !== cell ? (this.positionEditor(cell), $(this.graph.container).append(this.wrapperDiv)) : ($(".skTitle").children().hide(), 
            $(".skTitle").append(this.wrapperDiv)), $(this.wrapperDiv).append(this.myForm.getFullForm());
            var selectMe = function(area) {
                area && window.setTimeout(function() {
                    area.focus(), area.select();
                }, 10);
            };
            if (trigger) {
                var targetCounter = $(trigger.toElement).data("skcounter");
                targetCounter || (targetCounter = $($(trigger.toElement).parents("[data-skcounter]")).data("skcounter")), 
                selectMe(targetCounter ? $("[data-skcounter=" + targetCounter + "] textarea, [data-skcounter=" + targetCounter + "] input", this.wrapperDiv)[0] : $("textarea, input[type=text]", this.wrapperDiv).first());
            } else selectMe($("textarea, input[type=text]", this.wrapperDiv).first());
            $("textarea, input[type=text]", this.wrapperDiv).last().on("keydown", function(event) {
                var keyCode = event.keyCode || event.which;
                (13 == keyCode || 9 == keyCode) && (event.preventDefault(), that.graph.cellEditor.stopEditing(!that.graph.isInvokesStopCellEditing())), 
                skShell.tutorial && 9 == keyCode && skShell.tutorial.stepDone(skShell.tutorial.BOX_TAB_PRESSED, {
                    cell: cell,
                    field: $(this)[0]
                });
            }), cell.isStickynote() || $("textarea", this.wrapperDiv).on("keydown", function(event) {
                var keyCode = event.keyCode || event.which;
                13 != keyCode || event.shiftKey || (event.preventDefault(), that.graph.cellEditor.stopEditing(!that.graph.isInvokesStopCellEditing()));
            }), skShell.tutorial && skShell.tutorial.stepDone(skShell.tutorial.BOX_ENTERED_EDIT_MODE, {
                cell: cell,
                firstInput: $("textarea, input[type=text]", this.wrapperDiv).first()[0]
            });
        }
    }, mxCellEditor.prototype.stopEditing = function(cancel) {
        if (cancel = cancel || !1, null !== this.editingCell) {
            if (null !== this.textNode && (this.textNode.style.visibility = "visible", this.textNode = null), 
            !cancel) {
                this.graph.model.beginUpdate();
                try {
                    delete this.graph.cellHTMLCache[this.editingCell.id];
                    var v = this.editingCell.value;
                    if (this.myForm.save(!1), this.graph.labelChanged(this.editingCell, v, this.trigger), 
                    this.graph.cellRenderer.redraw(this.graph.view.getState(this.editingCell), !0, !0), 
                    this.editingCell.value && this.editingCell.value.tagName == skConstants.title) {
                        this.graph.fireEvent(new mxEventObject("updateTitle", "graph", this.graph));
                    }
                } finally {
                    this.graph.model.endUpdate();
                }
            }
            $(".skTitle").children().show(), this.trigger = null, this.textarea.blur(), $(this.textarea).remove();
        }
        null != this.wrapperDiv && (this.wrapperDiv.parentNode && $(this.wrapperDiv).remove(), 
        this.wrapperDiv = null), $(".skBoxActionIcon").show(), skShell.tutorial && skShell.tutorial.stepDone(skShell.tutorial.STOP_EDITING, {
            cell: this.editingCell
        }), this.editingCell = null, window.setTimeout(mxUtils.bind(this, function() {
            this.graph.container.focus();
        }), 10);
    };
    var mxCellEditorFocusLost = mxCellEditor.prototype.focusLost;
    mxCellEditor.prototype.focusLost = function() {
        this.graph.isHtmlLabel(this.getEditingCell()) || mxCellEditorFocusLost.apply(this, arguments);
    };
});
define('skGraphIO/skGraphIO_xml',['require','mxCodec','mxGraph','mxGraphView','mxPoint','mxRectangle','mxUtils','skConstants','skGraph/skGraph'],function(require) {
    "use strict";
    var mxCodec = require("mxCodec"), mxGraph = require("mxGraph"), mxGraphView = require("mxGraphView"), mxRectangle = (require("mxPoint"), 
    require("mxRectangle")), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skGraph = require("skGraph/skGraph");
    skGraph.prototype.readGraphState = function(node) {
        this.gridEnabled = "0" != node.getAttribute("grid"), this.gridSize = parseFloat(node.getAttribute("gridSize")) || mxGraph.prototype.gridSize, 
        this.graphHandler.guidesEnabled = "0" != node.getAttribute("guides"), this.setTooltips("0" != node.getAttribute("tooltips")), 
        this.setConnectable("0" != node.getAttribute("connect")), this.connectionArrowsEnabled = "0" != node.getAttribute("arrows"), 
        this.foldingEnabled = "0" != node.getAttribute("fold"), this.directLinkModeEnabled = "1" == node.getAttribute("directlinkmode") || !1;
        var ps = node.getAttribute("pageScale");
        null != ps ? this.pageScale = ps : this.pageScale = mxGraph.prototype.pageScale;
        var gc = node.getAttribute("gridColor");
        null != gc ? this.view.gridColor = gc.toLowerCase() : this.view.gridColor = mxGraphView.prototype.gridColor;
        var pv = node.getAttribute("page");
        null != pv ? this.pageVisible = "1" == pv : this.pageVisible = this.defaultPageVisible, 
        this.pageBreaksVisible = this.pageVisible, this.preferPageSize = this.pageBreaksVisible;
        var pw = node.getAttribute("pageWidth"), ph = node.getAttribute("pageHeight");
        null != pw && null != ph && (this.pageFormat = new mxRectangle(0, 0, parseFloat(pw), parseFloat(ph)));
        var bg = node.getAttribute("background");
        null != bg && bg.length > 0 ? this.background = bg : this.background = this.defaultGraphBackground;
    }, skGraph.prototype.setGraphXml = function(node) {
        if (null != node) {
            var dec = new mxCodec(node.ownerDocument);
            if ("mxGraphModel" != node.nodeName) throw {
                message: "Cannot open file",
                node: node,
                toString: function() {
                    return this.message;
                }
            };
            this.model.beginUpdate();
            try {
                this.model.clear(), this.readGraphState(node), this.updateGraphComponents(), this.isDirectLinkModeEnabled() ? this.removeBoxConnectionConstraints() : this.isEnabled() && this.addBoxConnectionConstraints(), 
                dec.decode(node, this.getModel()), this.getStylesheet().loadCustomStylesheet(!0, !1);
            } catch (e) {} finally {
                this.model.endUpdate();
            }
            this.resetScrollbars();
        } else this.resetGraph(), this.model.clear(), this.resetScrollbars();
    }, skGraph.prototype.getGraphXml = function(ignoreSelection) {
        ignoreSelection = null != ignoreSelection ? ignoreSelection : !0;
        var node = null;
        if (ignoreSelection) {
            var enc = new mxCodec(mxUtils.createXmlDocument());
            node = enc.encode(this.getModel());
        } else node = this.encodeCells(this.getSelectionCells());
        (0 !== this.view.translate.x || 0 !== this.view.translate.y) && (node.setAttribute("dx", Math.round(100 * this.view.translate.x) / 100), 
        node.setAttribute("dy", Math.round(100 * this.view.translate.y) / 100)), node.setAttribute("grid", this.isGridEnabled() ? "1" : "0"), 
        node.setAttribute("gridSize", this.gridSize), node.setAttribute("guides", this.graphHandler.guidesEnabled ? "1" : "0"), 
        node.setAttribute("tooltips", this.tooltipHandler.isEnabled() ? "1" : "0"), node.setAttribute("connect", this.connectionHandler.isEnabled() ? "1" : "0"), 
        node.setAttribute("arrows", this.connectionArrowsEnabled ? "1" : "0"), node.setAttribute("fold", this.foldingEnabled ? "1" : "0"), 
        node.setAttribute("page", this.pageVisible ? "1" : "0"), node.setAttribute("pageScale", this.pageScale), 
        node.setAttribute("pageWidth", this.pageFormat.width), node.setAttribute("pageHeight", this.pageFormat.height), 
        this.isDirectLinkModeEnabled() ? node.setAttribute("directlinkmode", "1") : node.removeAttribute("directlinkmode"), 
        this.view.gridColor != mxGraphView.prototype.gridColor && node.setAttribute("gridColor", this.view.gridColor), 
        null != this.background && node.setAttribute("background", this.background);
        return node;
    }, mxCodec.prototype.skConvertValue = function(cell) {
        try {
            var i, j, box, text, oldValues = {
                stickyNote_text: "stickyText",
                whatbox_attachments: "wAtts",
                whatbox_attachment: "wAtt",
                resource: "wRes",
                WB_DFLT_TEXT_activity: "wText",
                whybox: "yText",
                whatbox_value: "Whatbox",
                whybox_value: "Whybox",
                stickyNote_value: "stickyNote",
                att_type: "a_t"
            }, newDoc = mxUtils.createXmlDocument(), newValue = null;
            if (cell.value.nodeName == oldValues.whatbox_value) {
                if (newValue = cell.style == skConstants.group ? newDoc.createElement(skConstants.group) : cell.style == skConstants.title ? newDoc.createElement(skConstants.title) : newDoc.createElement(skConstants.whatbox), 
                text = mxUtils.createXmlDocument().createElement(skConstants.TEXT), text.setAttribute(skConstants.VALUE, cell.value.getAttribute(oldValues.WB_DFLT_TEXT_activity)), 
                cell.value.removeAttribute(oldValues.WB_DFLT_TEXT_activity), newValue.appendChild(text), 
                cell.value.getAttribute(oldValues.resource)) {
                    var res = mxUtils.createXmlDocument().createElement(skConstants.RESOURCE);
                    res.setAttribute(skConstants.VALUE, cell.value.getAttribute(oldValues.resource)), 
                    newValue.appendChild(res);
                }
                cell.value.removeAttribute(oldValues.resource);
                var allAtts = cell.value.getElementsByTagName(oldValues.whatbox_attachment);
                for (i = 0; i < allAtts.length; i++) {
                    var att = mxUtils.createXmlDocument().createElement(skConstants.ATTACHMENT);
                    switch (allAtts[i].getAttribute(oldValues.att_type)) {
                      case "text":
                        att.setAttribute(skConstants.ATTR_TYPE, skConstants.text_type), att.setAttribute(skConstants.VALUE, allAtts[i].getAttribute("value"));
                        break;

                      case "url":
                        att.setAttribute(skConstants.ATTR_TYPE, skConstants.url_type), att.setAttribute(skConstants.VALUE, allAtts[i].getAttribute("value")), 
                        att.setAttribute(skConstants.url_address, allAtts[i].getAttribute(skConstants.url_address));
                    }
                    newValue.appendChild(att);
                }
                var toDelete = cell.value.getElementsByTagName(oldValues.whatbox_attachments)[0];
                toDelete && cell.value.removeChild(toDelete), cell.value = null, cell.value = newValue;
            } else cell.value.nodeName == oldValues.whybox_value ? (newValue = newDoc.createElement(skConstants.whybox), 
            text = mxUtils.createXmlDocument().createElement(skConstants.TEXT), text.setAttribute(skConstants.VALUE, cell.value.getAttribute(oldValues.whybox)), 
            newValue.appendChild(text), cell.value = null, cell.value = newValue) : cell.value.nodeName == oldValues.stickyNote_value && (newValue = newDoc.createElement(skConstants.stickynote), 
            text = mxUtils.createXmlDocument().createElement(skConstants.TEXT), text.setAttribute(skConstants.VALUE, cell.value.getAttribute(oldValues.stickyNote_text)), 
            newValue.appendChild(text), cell.value = null, cell.value = newValue);
            if (cell.value.nodeName == skConstants.whatbox && null == cell.value.querySelector(skConstants.BOX)) {
                newValue = newDoc.createElement(skConstants.whatbox), box = newDoc.createElement(skConstants.BOX), 
                newValue.appendChild(box);
                var textTag = cell.value.getElementsByTagName(skConstants.TEXT)[0];
                textTag && box.appendChild(textTag);
                var resps = newDoc.createElement(skConstants.RESPONSIBILITIES);
                newValue.appendChild(resps);
                var resources = cell.value.getElementsByTagName(skConstants.RESOURCE);
                for (i = 0; i < resources.length; i++) {
                    var resp = newDoc.createElement(skConstants.RESPONSIBILITY);
                    resp.setAttribute(skConstants.ATTR_SHOWN, "1"), resps.appendChild(resp);
                    var role = newDoc.createElement(skConstants.ROLE), roleText = resources[i].getAttribute(skConstants.VALUE);
                    roleText.length && role.setAttribute(skConstants.VALUE, roleText), resp.appendChild(role);
                    var tags = resources[i].getElementsByTagName(skConstants.ELMT_TAG);
                    for (j = 0; j < tags.length; j++) resp.appendChild(tags[j]), j--;
                }
                var attachments = cell.value.getElementsByTagName(skConstants.ATTACHMENT);
                if (attachments && attachments.length) {
                    var atts = newDoc.createElement(skConstants.ATTACHMENTS);
                    for (newValue.appendChild(atts), i = 0; i < attachments.length; i++) {
                        var att2 = attachments[i], newAtt = newDoc.createElement(att2.getAttribute(skConstants.ATTR_TYPE));
                        att2.removeAttribute(skConstants.ATTR_TYPE);
                        var attributes = Array.prototype.slice.call(att2.attributes);
                        for (j = 0; j < attributes.length; j++) newAtt.setAttribute(attributes[j].nodeName, attributes[j].nodeValue);
                        atts.appendChild(newAtt);
                    }
                }
                cell.value = newValue;
            } else null == cell.value.querySelector(skConstants.BOX) && (box = newDoc.createElement(skConstants.BOX), 
            cell.value.appendChild(box), box.appendChild(cell.value.getElementsByTagName(skConstants.TEXT)[0]));
        } catch (err) {}
    }, mxCodec.prototype.skConvertStyle = function(cell) {
        if (1 == cell.id) return void (cell.style = skConstants.title);
        var oldName = {
            box_style_w: "wBox",
            box_style_y: "yBox",
            box_style_wDetailed: "wBoxDetailed",
            box_style_wGroup: "wBoxGroup",
            box_style_stickyNote: "stickyNote"
        };
        cell.style && cell.style.indexOf(oldName.box_style_stickyNote) >= 0 ? cell.style = cell.style.replace(oldName.box_style_stickyNote, skConstants.stickynote) : cell.style && cell.style.indexOf(oldName.box_style_y) >= 0 ? cell.style = skConstants.whybox : cell.style && cell.style.indexOf(oldName.box_style_wGroup) >= 0 ? cell.style = skConstants.group : cell.style && cell.style.indexOf(oldName.box_style_wDetailed) >= 0 ? cell.style = skConstants.whatboxdetailed : cell.style && cell.style.indexOf(oldName.box_style_w) >= 0 ? cell.style = skConstants.whatbox : cell.isEdge() && (cell.style == skConstants.LINE_WY2S ? cell.style = "" + skConstants.LINE_WY2S + ";exitX=1;exitY=0.5;entryX=0;entryY=0.5;" : ("w2y" === cell.style || "y2w" == cell.style || void 0 == cell.style) && (cell.style = "exitX=1;exitY=0.5;entryX=0;entryY=0.5;"));
    };
    var mxCodecprototypedecodeCell = mxCodec.prototype.decodeCell;
    mxCodec.prototype.decodeCell = function() {
        var cell = mxCodecprototypedecodeCell.apply(this, arguments);
        return cell && (this.skConvertStyle(cell), cell.value && this.skConvertValue(cell), 
        cell.visibleRect && (cell.visibleRect = null), cell.isWhatbox() && cell.setCollapsed(!0)), 
        cell;
    };
});
define('skGraphManager/skGraphManager',['require','skGraphEditorUI/skClipboard','jquery','mxCell','mxEvent','mxEventObject','mxEventSource','mxGeometry','mxUtils','skNavigator/skActions','skConstants','skExtensions/skExtensions','skGraphIO/skFileSaver','skGraph/skGraph','skGraph/skGraphUtils','skGraphEditorUI/skKeyboardHandler','skGraphEditorUI/skOutline','skGraphManager/skProcessLinkHandler','skGraph/skAttachments','skGraph/skSearch','skShell','skGraph/skGraphCellUtil','skGraphEditor/skGraphEditor','skGraphEditor/skGraphEditorCellEditor','skUtils','skGraphIO/skGraphIO_xml'],function(require) {
    "use strict";
    function skGraphManager(container) {
        this.container = $(container ? container : document.body), this.actions = new skActions(this), 
        this.linkHandler = new skProcessLinkHandler(this), this.graphs = {}, this.defaultGraphEnabled = !0, 
        this.tabsEnabled = !1, this.clipboard = require("skGraphEditorUI/skClipboard")(this), 
        this.tabsEnabled && this.initTabs(), this.fileSaver = new skFileSaver(this), this.searchEngine = new skSearch(this);
        var undo = this.actions.get("undo"), redo = this.actions.get("redo");
        this.undoListener = function(undoMgr) {
            undo.setEnabled(undoMgr.canUndo()), redo.setEnabled(undoMgr.canRedo());
        }, this.addListener("refresh", mxUtils.bind(this, function() {
            this.getCurrentGraph().sizeDidChange();
        })), this.addListener("pageSettingsChanged", mxUtils.bind(this, function(sender, event) {
            var graph = event.getProperty("graph");
            this.setPageSettings(graph, event.getProperty("settings")), graph.fireEvent(new mxEventObject("refreshOutline", "graph", graph));
        }));
    }
    var $ = require("jquery"), mxCell = require("mxCell"), mxEvent = require("mxEvent"), mxEventObject = require("mxEventObject"), mxEventSource = require("mxEventSource"), mxGeometry = require("mxGeometry"), mxUtils = require("mxUtils"), skActions = require("skNavigator/skActions"), skConstants = require("skConstants"), skExtensions = require("skExtensions/skExtensions"), skFileSaver = require("skGraphIO/skFileSaver"), skGraph = require("skGraph/skGraph"), skGraphUtils = require("skGraph/skGraphUtils"), skKeyboardHandler = require("skGraphEditorUI/skKeyboardHandler"), skOutline = require("skGraphEditorUI/skOutline"), skProcessLinkHandler = require("skGraphManager/skProcessLinkHandler"), skAttachments = require("skGraph/skAttachments"), skSearch = require("skGraph/skSearch"), skShell = require("skShell");
    require("skGraph/skGraphCellUtil");
    var counter = 0;
    require("skGraphEditor/skGraphEditor"), require("skGraphEditor/skGraphEditorCellEditor");
    var skUtils = require("skUtils");
    require("skGraphIO/skGraphIO_xml"), mxUtils.extend(skGraphManager, mxEventSource), 
    skGraphManager.prototype.init = function() {
        if (this.getGraphXmlDataFromSource(), this.tabsEnabled && 0 === $(".skGraphManager").length) {
            var mgr = $('<ol class="skGraphManager navigators breadcrumb forGraphEditor"><li><a class="newBlankGraph"><i class="fa fa-plus"></i></a></li></ol>'), that = this;
            $(document.body).prepend(mgr), $(".newBlankGraph", mgr).on("click", function() {
                that.openGraph({
                    source: "new "
                });
            }), this.addListener("graphEnabled", function(sender, event) {
                var graphID = event.getProperty("graph").id;
                $(".skGraphManager li").removeClass("active"), $(".skGraphManager li[data-graphid='" + graphID + "']").addClass("active");
            });
        }
    }, skGraphManager.prototype.tabsEnabled = null, skGraphManager.prototype.defaultGraphEnabled = null, 
    skGraphManager.prototype.graphs = null, skGraphManager.prototype.currentGraph = null, 
    skGraphManager.prototype.initTabs = function(graphID) {
        if (void 0 !== this.graphs[graphID]) {
            var graph = this.graphs[graphID];
            $(".skGraphManager .active").removeClass("active");
            var tab = $("<li class='active'><a>untitled</a></li>");
            $(".skGraphManager").prepend(tab), tab.attr("data-graphid", graphID);
            var that = this;
            tab.on("click", function() {
                that.setCurrentGraph(graph);
            }), graph.addListener("contentLoaded", function() {
                $("a", tab).text(graph.getTitle());
            }), graph.addListener("updateTitle", function() {
                $("a", tab).text(graph.getTitle());
            });
        }
    }, skGraphManager.prototype.openGraph = function(graphObject, cellID, enabled, makeCurrent, graphID) {
        enabled = enabled || this.defaultGraphEnabled, makeCurrent = makeCurrent === !1 ? !1 : !0;
        var graph;
        if (graphID && (graph = this.getGraphFromID(graphID)), graph = this.getCurrentGraph()) {
            if ("demo" === graphObject.source && Object.keys(graph.model.cells).length > 2) return void skShell.setStatus("Process must be empty to load a demo content");
        } else graph = this.createGraph(), graph.id = graphObject.id || counter++, this.graphs[graph.id] = graph;
        var content = !1;
        if (graphObject.xml && "" !== graphObject.xml) {
            skShell.setStatus("Loading process...");
            var data = skUtils.zapGremlins(mxUtils.trim(graphObject.xml)), doc = mxUtils.parseXml(data);
            content = !0;
            var graphXml = "";
            "html" === doc.documentElement.tagName ? doc.documentElement.getElementsByTagName("script").length > 0 && "application/skore" == doc.documentElement.getElementsByTagName("script")[0].getAttribute("type") ? graphXml = doc.documentElement.getElementsByTagName("script")[0].firstChild : (graphObject.source = "invalidHTML", 
            skShell.setStatus("Not a valid Skore HTML file.<br/>Only file created in Skore app (desktop) can be opened here."), 
            content = !1) : graphXml = doc.documentElement, graph.model.beginUpdate();
            try {
                graph.setGraphXml(graphXml, graphObject.source), graph.initTemplateManager(), graph.initRoleManager();
            } catch (e) {} finally {
                graph.model.endUpdate();
            }
            graph.extensions || (graph.extensions = new skExtensions(graph)), graph.extensions.loadExistingExtensions(), 
            graph.view && (graph.cellHTMLCache = []);
        } else graph.getStylesheet().loadCustomStylesheet(!1);
        return makeCurrent && this.setCurrentGraph(graph), graph.view.validate(), this.fireEvent(new mxEventObject("refresh")), 
        graph.fireEvent(new mxEventObject("contentLoaded", "graph", graph, "source", graphObject.source)), 
        graph.fireEvent(new mxEventObject("refreshOutline")), graph.setEnabled(enabled), 
        graph.resetScrollbars(), graph;
    }, skGraphManager.prototype.isGraphOpened = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        return null !== graph;
    }, skGraphManager.prototype.closeGraph = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        graph && ($(graph.container).remove(), this.graphs[graph.id] = null, this.fireEvent(new mxEventObject("graphClosed", "graph", graph)));
    }, skGraphManager.prototype.setGraphVisible = function(graphID, visible) {
        var graph = this.getGraphFromID(graphID);
        return $(".skDiagramContainer").hide(), visible = visible === !1 ? !1 : !0, $(graph.container).toggle(visible), 
        this.fireEvent(new mxEventObject("graphVisible", "graph", graph, "visible", visible)), 
        graph;
    }, skGraphManager.prototype.isGraphVisible = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        return $(graph.container).is(":visible"), graph;
    }, skGraphManager.prototype.isCurrentGraph = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        return graph === this.currentGraph ? !0 : !1;
    }, skGraphManager.prototype.setCurrentGraph = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        return graph && (this.setGraphVisible(graph), this.currentGraph = graph, this.fireEvent(new mxEventObject("graphEnabled", "graph", graph, "enabled", graph.isEnabled()))), 
        graph;
    }, skGraphManager.prototype.getCurrentGraph = function() {
        return this.currentGraph;
    }, skGraphManager.prototype.isGraphEnabled = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        return graph && graph.isEnabled();
    }, skGraphManager.prototype.setGraphEnabled = function(graphID, enabled) {
        var graph = this.getGraphFromID(graphID);
        enabled = enabled !== !1 ? !0 : !1, graph && graph.setEnabled(enabled);
    }, skGraphManager.prototype.createGraph = function() {
        var graph = new skGraph(), graphContainer = document.createElement("div");
        return graphContainer.classList.add("skDiagramContainer", "forGraphEditor"), this.container.append(graphContainer), 
        graph.init(graphContainer), this.fireEvent(new mxEventObject("newGraphCreated", "graph", graph)), 
        this.keyHandler = new skKeyboardHandler(graph, this.actions), graph.addListener("graphEnabled", mxUtils.bind(this, function(sender, event) {
            var graph = event.getProperty("graph");
            this.fireEvent(new mxEventObject("graphEnabled", "graph", graph, "enabled", event.getProperty("enabled"), "box", "" + graph.id + "." + graph.getDefaultParent().id + "d"));
        })), graph.addListener("navigationStarted", mxUtils.bind(this, function(sender, event) {
            this.fireEvent(new mxEventObject("navigationStarted", "graph", event.getProperty("graph"), "destinationCell", event.getProperty("destinationCell"), "stayAbove", event.getProperty("stayAbove")));
        })), graph.addListener("navigationDone", mxUtils.bind(this, function(sender, event) {
            this.fireEvent(new mxEventObject("navigationDone", "graph", graph, "destinationCell", event.getProperty("destinationCell"), "stayAbove", event.getProperty("stayAbove")));
        })), graph.addListener("contentLoaded", mxUtils.bind(this, function() {
            this.fireEvent(new mxEventObject("contentLoaded", "graph", graph));
        })), graph.getSelectionModel().addListener(mxEvent.CHANGE, mxUtils.bind(this, function(sender, evt) {
            this.fireEvent(new mxEventObject("boxSelectionChanged", "graph", graph, "cellsRemoved", evt.getProperty("added"), "cellsAdded", evt.getProperty("removed")));
        })), graph.addListener("graphEnabled", mxUtils.bind(this, function(sender, event) {
            var enabled = event.getProperty("enabled"), graph = event.getProperty("graph");
            enabled ? graph.undoManager && (graph.undoManager.addListener(mxEvent.ADD, this.undoListener), 
            graph.undoManager.addListener(mxEvent.UNDO, this.undoListener), graph.undoManager.addListener(mxEvent.REDO, this.undoListener), 
            graph.undoManager.addListener(mxEvent.CLEAR, this.undoListener), this.undoListener(graph.undoManager)) : graph.undoManager && graph.undoManager.removeListener(this.undoListener), 
            this.outline = new skOutline(graph);
        })), graph.addListener("openAttachment", mxUtils.bind(this, function(sender, event) {
            this.actions.openModal("attachments", "cell", event.getProperty("cell"), "forceOpen", event.getProperty("forceOpen"), "atttype", event.getProperty("atttype"));
        })), graph.initGraphEditor(), this.outline = new skOutline(graph), graph;
    }, skGraphManager.prototype.getDataFromJustSkoreIt = function(id) {
        var that = this;
        return $.ajax({
            url: "https://api.github.com/gists/" + id + "?callback",
            type: "GET",
            dataType: "jsonp",
            success: function(data) {
                that.openGraph({
                    xml: decodeURIComponent(data.data.files["gist.skore"].content),
                    source: skConstants.SOURCE_JUSTSKOREIT
                });
            }
        });
    }, skGraphManager.prototype.getDataFromDesktop = function() {
    }, skGraphManager.prototype.getGraphXmlDataFromSource = function() {
        var that = this;
        if (skShell.readUrlParams(window.location.href), "?embed" === window.location.search && (window.addEventListener("message", function(event) {
            that.openGraph({
                xml: event.data.trim(),
                source: skConstants.SOURCE_EMBED
            });
        }), window.parent.postMessage("ready", "*")), null != skShell.urlParams[skConstants.JUSTSKOREIT]) that.getDataFromJustSkoreIt(skShell.urlParams[skConstants.JUSTSKOREIT]); else if ("skEnterprise" === skShell.urlParams.source || "skEnterpriseViewer" === skShell.urlParams.source) {
            var url = skShell.urlParams.url;
            url ? $.ajax({
                url: url,
                type: "GET"
            }).done(function(data) {
                that.openGraph({
                    xml: data.graphXml,
                    source: skConstants.SRC_ENT
                });
            }).error(function(err) {}) : skShell.setStatus("Need process ID, mate.");
        } else that.openGraph({
            xml: "",
            source: skConstants.SOURCE_NEW
        });
    }, skGraphManager.prototype.openFromFilePath = function(filePath) {
        var graph = this.getCurrentGraph();
        if (null !== graph.getDefaultParent().children && graph.getDefaultParent().children.length) window.requireNode("electron").ipcRenderer.send("newskorewindow", filePath); else {
            var fsj = window.requireNode("fs-jetpack"), data = fsj.read(filePath);
            this.openGraph({
                xml: data,
                source: skConstants.SOURCE_FILE,
                filePath: filePath
            });
        }
    }, skGraphManager.prototype.container = null, skGraphManager.prototype.getGraphFromID = function(graphID) {
        if (graphID instanceof skGraph) return graphID;
        if ("string" == typeof graphID) {
            if (void 0 !== this.graphs[graphID]) return this.graphs[graphID];
        } else if (null == graphID) return this.getCurrentGraph();
        return null;
    }, skGraphManager.prototype.getTitle = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        return graph.getTitle();
    }, skGraphManager.prototype.getSelectionCells = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        return graph ? graph.getSelectionCells() : void 0;
    }, skGraphManager.prototype.navigateTo = function(cellCode) {
        if (-1 == cellCode.indexOf(".")) this.getCurrentGraph().navigateToCellHandler(cellCode); else {
            cellCode = cellCode.split(".");
            var graph = this.getGraphFromID(cellCode[0]);
            this.isGraphOpened(graph) ? (this.setGraphVisible(graph), graph.navigateToCellHandler(cellCode[1])) : this.fireEvent(new mxEventObject("requestGraphOpen", "graphID", cellCode[0], "cellCode", cellCode[1]));
        }
    };
    var createDragElement = function(type) {
        var dragElement = $("<div class='icon_dragged icon_" + type + "_dragged'><div class='icon_dragged_inner'>Drop me on the canvas</div></div>");
        return dragElement[0];
    };
    return skGraphManager.prototype.makeDropFunction = function(type, valueTextToReuse) {
        return mxUtils.bind(this, function(graph, evt, target, x, y) {
            var value, style = graph.getStylesheet().styles[type];
            "object" == typeof valueTextToReuse ? value = valueTextToReuse : "string" == typeof valueTextToReuse ? (value = skUtils.createBoxElement(type), 
            $("box", value).append($("<text value='" + valueTextToReuse + "'></text>")), $("text", value).attr("value", valueTextToReuse)) : value = skUtils.createBoxElement(type);
            var w = isNaN(parseInt(style.width, 10)) ? skConstants.WB_WIDTH : parseInt(style.width, 10), h = isNaN(parseInt(style.height, 10)) ? skConstants.WB_HEIGHT : parseInt(style.height, 10), box = new mxCell(value, new mxGeometry(0, 0, w, h), type);
            box.vertex = !0;
            var boxes = graph.importCells([ box ], x, y, target);
            null !== boxes && boxes.length > 0 && (graph.scrollCellToVisible(boxes[0]), graph.setSelectionCells(boxes), 
            this.fireEvent(new mxEventObject("boxDropped", "graph", graph, "cells", boxes)), 
            valueTextToReuse || (graph.cellEditor.startEditing(boxes[0]), setTimeout(function() {
                $($("textarea, input[type=text]", graph.cellEditor.wrapperDiv)[0]).select();
            }, 100)));
        });
    }, skGraphManager.prototype.makeCardshoeBoxDraggable = function(element, type, valueTextToReuse) {
        return mxUtils.makeDraggable(element, mxUtils.bind(this, this.getCurrentGraph), this.makeDropFunction(type, valueTextToReuse), createDragElement(type), null, null, !0, !0);
    }, skGraphManager.prototype.getIOWhyboxes = function(cell) {
        var result = {
            inputs: [],
            outputs: []
        };
        return cell.edges && cell.edges.forEach(function(edge) {
            edge.target.isWhybox() && edge.source === cell ? result.outputs.push(edge.target) : edge.source.isWhybox() && edge.target === cell && result.inputs.push(edge.source);
        }), result;
    }, skGraphManager.prototype.getPageSettings = function(graphID) {
        var graph = this.getGraphFromID(graphID);
        return skGraphUtils.getPageSettings(graph);
    }, skGraphManager.prototype.setPageSettings = function(graphID, pageSettings) {
        var hasScrollbars, tx, ty, graph = this.getGraphFromID(graphID), currentPageSettings = skGraphUtils.getPageSettings(graph), newPageSettings = $.extend({}, currentPageSettings, pageSettings);
        if (currentPageSettings.pageView !== pageSettings.pageView && (hasScrollbars = mxUtils.hasScrollbars(graph.container), 
        tx = 0, ty = 0, hasScrollbars && (tx = graph.view.translate.x * graph.view.scale - graph.container.scrollLeft, 
        ty = graph.view.translate.y * graph.view.scale - graph.container.scrollTop)), graph.pageVisible = newPageSettings.pageView, 
        graph.pageBreaksVisible = newPageSettings.pageView, graph.preferPageSize = newPageSettings.pageView, 
        graph.setGridEnabled(newPageSettings.grid), graph.pageScale = newPageSettings.pageScale, 
        graph.background = newPageSettings.backgroundColor, graph.pageFormat = skGraphUtils.getPageFormat(newPageSettings.paperSize, newPageSettings.landscape), 
        graph.refresh(), graph.updateGraphComponents(), graph.view.validateBackground(), 
        graph.sizeDidChange(), currentPageSettings.pageView !== pageSettings.pageView) {
            if (hasScrollbars) {
                var cells = graph.getSelectionCells();
                graph.clearSelection(), graph.setSelectionCells(cells);
            }
            hasScrollbars && graph.container && (graph.container.scrollLeft = graph.view.translate.x * graph.view.scale - tx, 
            graph.container.scrollTop = graph.view.translate.y * graph.view.scale - ty);
        }
        return this.fireEvent(new mxEventObject("graphBackgroundChanged", "graph", this)), 
        graph;
    }, skGraphManager.prototype.getStyleForCell = function(graphID, cellType, styleSheetName, cells) {
        var graph = this.getGraphFromID(graphID);
        return cellType = cellType || "whatbox", styleSheetName = styleSheetName || "default", 
        graph ? graph.getStylesheet().buildCustomStyle(cellType, styleSheetName, cells) : null;
    }, skGraphManager.prototype.setStyleSheet = function(graphID, styleObject, styleSheetName, cells) {
        var graph = this.getGraphFromID(graphID);
        graph && graph.getStylesheet().saveStyleSheet(styleObject, null, cells);
    }, skGraphManager.prototype.getXmlForGraph = function(graphID, pretty) {
        var graph = this.getGraphFromID(graphID), xml = graph.getGraphXml();
        return pretty ? mxUtils.getPrettyXml(xml) : xml;
    }, skGraphManager.prototype.triggerGraphAction = function(graphID, actionName) {
        var graph = this.getGraphFromID();
        if (graph) {
            var act = this.actions.get(actionName);
            null !== act && null !== act.funct && act.funct();
        }
    }, skGraphManager.prototype.renderAttachments = function(graph, cell, type) {
        return skAttachments.renderAttachments(cell, type);
    }, skGraphManager.prototype.getAttachmentTypes = function() {
        return skAttachments.getAttachmentTypes();
    }, skGraphManager.prototype.createNewAttachment = function(graphID, cell, attachmentType) {
        var graph = this.getGraphFromID(graphID);
        return skAttachments.createNewAttachment(graph, cell, attachmentType);
    }, skGraphManager.prototype.searchEngine = null, skGraphManager;
});
define('skNavigatorUI/skExternalLinkHandler',['require','mxUtils','skUtils'],function(require) {
    "use strict";
    function skExternalLinkHandler(navigatorUI) {
        this.navigatorUI = navigatorUI, document.addEventListener("click", mxUtils.bind(this, this.supportExternalLinks), !1);
    }
    var mxUtils = require("mxUtils"), skUtils = require("skUtils");
    return skExternalLinkHandler.prototype.navigatorUI = null, skExternalLinkHandler.prototype.checkDomElement = function(event, element) {
        var href;
        element.classList.contains("skExtLink") && element.getAttribute("href") && (href = element.getAttribute("href"), 
        skUtils.openURL(href), event.preventDefault()), element.parentElement && this.checkDomElement(event, element.parentElement);
    }, skExternalLinkHandler.prototype.supportExternalLinks = function(event) {
        this.checkDomElement(event, event.target);
    }, skExternalLinkHandler;
});
define('skModal/skModal_Registration',['require','jquery','mxEventObject','skShell','skUtils'],function(require) {
    "use strict";
    function skModal_Registration(navigatorUI) {
        return;
    }
    var $ = require("jquery");
    require("mxEventObject"), require("skShell"), require("skUtils");
    return skModal_Registration.prototype.setMessage = function(severity, message) {
        $(".skSubmitMessage div").empty().removeClass().addClass("alert alert-" + severity).html(message);
    }, skModal_Registration;
});
define('skUI/skNavHistory',['require','jquery','mxEventObject','mxUtils'],function(require) {
    "use strict";
    function skNavHistory(graphManager) {
        this.graphManager = graphManager, graphManager.addListener("newGraphCreated", mxUtils.bind(this, function(event, sender) {
            var graph = sender.getProperty("graph");
            this.initForGraph(graph), this.buildUI();
        }));
    }
    var $ = require("jquery"), mxEventObject = require("mxEventObject"), mxUtils = require("mxUtils");
    return skNavHistory.prototype.initForGraph = function(graph) {
        graph.addListener("navigationDone", mxUtils.bind(this, function(sender, evt) {
            this.addHistory(graph, evt.getProperty("destinationCell"), evt.getProperty("stayAbove"));
        })), this.init(graph);
    }, skNavHistory.prototype.init = function(graph) {
        this.history = [ graph.getDefaultParent() ], this.currentPosition = 0, $(".skNavBack").addClass("disabled"), 
        $(".skNavForward").addClass("disabled");
    }, skNavHistory.prototype.buildUI = function() {
        var that = this;
        $(".skNavBack").off("click"), $(".skNavBack").on("click", function() {
            that.graphManager.fireEvent(new mxEventObject("navigateTo", "cell", that.history[that.currentPosition - 1]));
        }), $(".skNavForward").off("click"), $(".skNavForward").on("click", function() {
            that.graphManager.fireEvent(new mxEventObject("navigateTo", "cell", that.history[that.currentPosition + 1]));
        }), $('.skNavbar [data-toggle="tooltip"]').tooltip();
    }, skNavHistory.prototype.history = null, skNavHistory.prototype.graph = null, skNavHistory.prototype.nextButton = null, 
    skNavHistory.prototype.graphManager = null, skNavHistory.prototype.refreshButtons = function() {
        this.currentPosition > 0 ? $(".skNavBack").removeClass("disabled") : $(".skNavBack").addClass("disabled"), 
        this.currentPosition + 1 < this.history.length ? $(".skNavForward").removeClass("disabled") : $(".skNavForward").addClass("disabled");
    }, skNavHistory.prototype.addHistory = function(graph, destinationCell, stayAbove) {
        destinationCell || (destinationCell = graph.getDefaultParent()), stayAbove && (destinationCell = destinationCell.parent), 
        this.history[this.currentPosition - 1] === destinationCell ? --this.currentPosition : this.history[this.currentPosition + 1] === destinationCell ? ++this.currentPosition : this.history[this.currentPosition] !== destinationCell && (this.history.splice(this.currentPosition + 1, this.history.length - this.currentPosition - 1, destinationCell), 
        this.currentPosition = this.history.length - 1), this.refreshButtons();
    }, skNavHistory;
});
define('skNavigatorUI/skStatus',['require','jquery','mxUtils','skShell','skUtils'],function(require) {
    "use strict";
    function skStatus(shell) {
        this.shell = shell || skShell, this.container = $(".skStatus"), this.shell.setStatus = mxUtils.bind(this, function(text) {
            this.set(text);
        });
    }
    var $ = require("jquery"), mxUtils = require("mxUtils"), skShell = require("skShell"), skUtils = require("skUtils");
    return skStatus.prototype.shell = null, skStatus.prototype.container = null, skStatus.prototype.getHTML = function(text, className) {
        return $('<div class="alert alert-warning alert-dismissible ' + className + '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + text + "</div>");
    }, skStatus.prototype.set = function(text) {
        var a = $(this.getHTML(text)).appendTo(this.container);
        this.container.show(), a.fadeOut(4e3, function() {
            $(this).remove();
        });
    }, skStatus.prototype.setAndDontShowAgain = function(text, key) {
        this.setWithButtons(text, key);
    }, skStatus.prototype.setWithButtons = function(text, key, customAction) {
        if (null == key || (localStorage.hasOwnProperty(key) === !1 || parseInt(localStorage.getItem(key), 10) === !1) && 0 === $("." + key, this.container).length && (null == localStorage.getItem(key + "TIMING") || new Date(new Date() - new Date(parseInt(localStorage.getItem(key + "TIMING"), 10))).getSeconds() < 120)) {
            var messageAndButtonContainer = this.getHTML(text, key), stdFunc = function() {
                localStorage.setItem(key + "TIMING", new Date().getTime());
            }, myAction = skUtils.extend({}, {
                message: "Got it!",
                icon: null,
                func: stdFunc
            }, customAction);
            messageAndButtonContainer.append($("<div>" + (key ? '<div class="btn-group" role="group">' : "") + (key ? '<button type="button" class="skGotItForever btn btn-default"><input type="checkbox"> Got it, forever!</button>' : "") + '<button type="button" class="skGotIt btn btn-default">' + myAction.message + "</button>" + (key ? "</div>" : "") + "</div>")), 
            $(".skGotIt", messageAndButtonContainer).on("touchstart click", function() {
                myAction.func(), skShell.wellDoneMessage.hey(this), $(messageAndButtonContainer).fadeOut(400, function() {
                    $(this).remove();
                });
            }), $(".skGotItForever", messageAndButtonContainer).on("click", function() {
                $("input", this).prop("checked", !0), localStorage.setItem(key, 1), skShell.wellDoneMessage.hey(this), 
                $(messageAndButtonContainer).fadeOut(1e3, function() {
                    $(this).remove();
                });
            }), this.container.show(), messageAndButtonContainer.appendTo(this.container), messageAndButtonContainer.delay(200).fadeIn(200);
        }
    }, skStatus.prototype.setAndAcknowledge = function(text) {
        this.setWithButtons(text, null);
    }, skStatus.prototype.setAndAction = function(text, actionMessage, actionFunction, icon) {
        this.setWithButtons(text, null, {
            message: actionMessage,
            func: actionFunction,
            icon: icon
        });
    }, skStatus;
});
define('text!skModal/skModal_Attachments.html',[],function () { return '<div id="skModal_Attachments" class="modal sideModal sideRight" tabindex="-1" role="dialog" aria-labelledby="skModal_AttachmentsLabel" data-backdrop="" aria-hidden="true">\n\t<div class="modal-dialog modal-md">\n\t\t<div class="modal-content">\n\t\t\t<div class="modal-header skAttHeaderTitle">\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close">\n\t\t\t\t\t<span aria-hidden="true">&times;</span>\n\t\t\t\t</button>\n\t\t\t\t<h4 class="modal-title" id="skModal_AttachmentsLabel">\n\t\t\t\t\t<span class="skAttCurrentIcon">\n\t\t\t\t\t\t<i class="fa fa-paperclip"></i>\n\t\t\t\t\t</span>\n\t\t\t\t\t<span data-toggle="tooltip" data-placement="bottom" title="Text of the current box" class="skAttachmentBoxName"></span>\n\t\t\t\t</h4>\n\t\t\t</div>\n\t\t\t<div class="modal-header skAttHeaderSwitcher">\n\t\t\t\t<div class="btn-group btn-group-justified skAttSwitcher" role="group">\n\t\t\t\t  <div\n\t\t\t\t  \tclass="btn-group btn-group-xs"\n\t\t\t    \tdata-toggle="tooltip"\n\t\t\t    \tdata-placement="bottom"\n\t\t\t    \ttitle="Attachments"\n\t\t\t\t  \trole="group">\n\t\t\t\t    <button type="button" data-attbtn="attachments" class="btn btn-default active" >\n\n\t\t\t\t\t\t<i class="fa fa-paperclip"></i></button>\n\t\t\t\t  </div>\n\t\t\t\t  <div class="btn-group btn-group-xs" data-toggle="tooltip" data-placement="bottom" role="group"  title="Responsibilities">\n\t\t\t\t    <button type="button" data-attbtn="responsibilities" class="btn btn-default">\n\t\t\t\t    \t<i class="fa fa-user"></i>\n\t\t\t\t    </button>\n\t\t\t\t  </div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class="modal-body">\n\t\t\t\t<div class="alert alert-info in skAttPanelTutorial" role="alert">\n\t\t\t\t\t<button type="button" class="close" data-dismiss="alert" aria-label="Close">\n\t\t\t\t\t\t<span class="skAttPanelTutorialClose" aria-hidden="true">&times;</span>\n\t\t\t\t\t</button>\n\t\t\t\t\t<p>\n\t\t\t\t\t\tAttachments are great for linking to documents or collecting more information such as descriptions, systems or durations.\n\t\t\t\t\t</p>\n\t\t\t\t</div>\n\n\n\n\t\t\t\t<!-- actual content -->\n\t\t\t\t<ul class="list-group skSearchable skAttList">\n\t\t\t\t</ul>\n\n\t\t\t</div>\n\n\t\t\t<div class="modal-footer">\n\t\t\t\t<!-- buttons -->\n\t\t\t\t<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>\n\n\t\t\t\t<div class="newAttaButtonGroup pull-left"></div>\n\n\t\t\t</div>\n\n\t\t</div> <!-- /.modal-content -->\n\t</div> <!-- /.modal-dialog -->\n\n</div>\n';});
define('skModal/skModal_Attachments',['require','text!skModal/skModal_Attachments.html','jquery','skUtils','skConstants'],function(require) {
    "use strict";
    function skModal_Attachments(navigatorUI) {
        var that = this;
        return this.navigatorUI = navigatorUI, navigatorUI.loadModal("skModal_Attachments", require("text!skModal/skModal_Attachments.html"), function() {
            var graph = navigatorUI.getCurrentGraph(), cell = $("#skModal_Attachments").data("cell"), attType = $("#skModal_Attachments").data("atttype") || wasAttTypePreviously || "attachments";
            if ($(".skAttachmentBoxName").empty().html(skUtils.renderMarkdown(cell.getBoxText(), !0)), 
            that.loadForType(attType, graph, cell), navigatorUI.ui.search && navigatorUI.ui.search.prevKeyword && $("#skModal_Attachments .skSearchable").highlight(navigatorUI.ui.search.prevKeyword), 
            graph.isEnabled()) {
                var possibleFields = that.navigatorUI.graphManager.getAttachmentsType(), btnGrp = $('<div class="btn-group dropup btn-group-sm"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span>New attachment</span> <span class="caret"></span></button><ul class="dropdown-menu"></ul></div>');
                $(".newAttaButtonGroup").empty().append(btnGrp), possibleFields.forEach(function(fieldName) {
                    var li = $("<li></li>");
                    li.append($("<a>" + fieldName + "</a>").on("click", function() {
                        var form = navigatorUI.graphManager.createNewAttachment(null, cell, fieldName);
                        $(".skAttList .skFormContainer").append(form.container), setTimeout(function() {
                            $($(".skAttList .skFormContainer textarea, .skAttList .skFormContainer input[type=text]")[0]).select();
                        }, 100);
                    })), $(".dropdown-menu", btnGrp).append(li);
                });
            }
        }, null, function() {
            $('.skAttSwitcher [data-toggle="tooltip"]').tooltip({
                container: "body"
            }), $("[data-attbtn]").on("click", function() {
                that.loadForType($(this).data("attbtn")), $(".skAttSwitcher button").removeClass("active"), 
                $(this).toggleClass("active"), $(".skAttCurrentIcon i").removeClass(), $(".skAttCurrentIcon i").addClass($("i", this).attr("class"));
            }), navigatorUI.getCurrentGraph().isEnabled() || localStorage.hasOwnProperty("DONTSHOWAGAINAttachmentMessage") !== !0 || 1 !== parseInt(localStorage.getItem("DONTSHOWAGAINAttachmentMessage"), 10) ? $(".skAttPanelTutorial").hide() : ($(".skAttPanelTutorial").show(), 
            $(".skAttPanelTutorial p").text("Follow these links for guidance notes and templates"), 
            $(".skAttPanelTutorialClose").on("click", function() {
                localStorage.setItem("DONTSHOWAGAINAttachmentMessage", 1);
            }));
        });
    }
    var $ = require("jquery"), skUtils = require("skUtils"), skConstants = require("skConstants"), wasAttTypePreviously = null;
    return skModal_Attachments.prototype.navigatorUI = null, skModal_Attachments.prototype.loadForType = function(type, graph, cell) {
        graph = graph || this.navigatorUI.getCurrentGraph(), cell = cell || $("#skModal_Attachments").data("cell");
        var form = this.navigatorUI.graphManager.renderAttachments(graph, cell, type);
        wasAttTypePreviously = type, type == skConstants.ATTACHMENTS && $(".newAttaButtonGroup").hide(), 
        $(".skAttList").empty().append(form.container);
    }, skModal_Attachments;
});
define('skNavigatorUI/skSearchUI',['require','jquery','skUtils','mxUtils'],function(require) {
    "use strict";
    function skSearchUI(graphManager) {
        this.graphManager = graphManager;
        var createSearchParam = function() {
            return {
                whatbox: $("[data-searchboxtype=whatbox]").is(":checked"),
                whybox: $("[data-searchboxtype=whybox]").is(":checked"),
                stickynote: $("[data-searchboxtype=stickynote]").is(":checked"),
                id: $("[data-searchboxtype=id]").is(":checked"),
                attachments: $("[data-searchboxtype=attachments]").is(":checked"),
                responsibilities: $("[data-searchboxtype=responsibilities]").is(":checked"),
                levels: $("input[name=searchScope]:checked").val(),
                exactmatch: $("[data-searchboxtype=exactmatch]").is(":checked")
            };
        };
        $(".skMenuFind_match").hide(), $(".skMenuFind_clear").hide(), $(".skSearchFilter").hide(), 
        $(".skMenuFind_input").focusin(function() {
            $(this).select();
        });
        var searchEngine = graphManager.searchEngine;
        $(".skMenuFind_clear").on("click touchstart", function() {
            $(".skMenuFind_input").val(""), searchEngine.clearSearch(), hideStuffs();
        });
        var hideStuffs = function() {
            $(".skMenuFind_match").hide(), $(".skMenuFind_clear").hide(), $(".skSearchFilter").hide();
        }, searchDebounce = skUtils.debounce(mxUtils.bind(this, function() {
            var keyword = $(".skMenuFind_input").val();
            "" === keyword ? hideStuffs() : searchEngine.performSearch(keyword, createSearchParam(), mxUtils.bind(this, this.resultsCallback));
        }), 300);
        $(".skMenuFind_input").on("keyup", searchDebounce), $(".skSearchFilter").on("change", mxUtils.bind(this, function() {
            searchEngine.clearSearch(), searchEngine.performSearch($(".skMenuFind_input").val(), createSearchParam(), mxUtils.bind(this, this.resultsCallback));
        })), graphManager.addListener("graphEnabled", mxUtils.bind(this, function() {
            searchEngine.performSearch($(".skMenuFind_input").val(), createSearchParam(), mxUtils.bind(this, this.resultsCallback));
        })), graphManager.addListener("navigationStarted", function() {
            searchEngine.unhighlight();
        }), graphManager.addListener("navigationDone", mxUtils.bind(this, function() {
            searchEngine.clearSearch(), searchEngine.performSearch($(".skMenuFind_input").val(), createSearchParam(), mxUtils.bind(this, this.resultsCallback));
        })), $(".skFilterList li").on("click", function(event) {
            event.stopPropagation();
        });
    }
    var thislevel, otherlevel, $ = require("jquery"), skUtils = require("skUtils"), mxUtils = require("mxUtils");
    return skSearchUI.prototype.resultsCallback = function(keyword, results) {
        $(".skMenuFind_clear").show(), $(".skSearchFilter").show(), $(".skMenuFind_match").show(), 
        $(".skMenuFind_matchResults").html(results.length + " found"), $(".skResultList").empty();
        var thislevel = null, otherlevel = null;
        results.length || $(".skResultList").append($("<li><a>No results</a></li>")), thislevel = null, 
        otherlevel = null;
        var that = this;
        results.forEach(function(result) {
            that.addResultToList(result.cell, !result.cellIsVisible);
        });
    }, skSearchUI.prototype.addResultToList = function(cell, other) {
        var that = this;
        if (0 === $(".res-" + cell.id).length) {
            var entry = $("<li class='res-" + cell.id + "'><a>" + cell.getBoxText(!0, !0) + "</a></li>");
            entry.on("click", function() {
                that.graphManager.getCurrentGraph().navigateTo(cell, !0);
            }), other ? (otherlevel || (otherlevel = $('<li class="dropdown-header" class="otherlevel" >Another level</li>'), 
            $(".skResultList").append(otherlevel)), otherlevel.after(entry)) : (thislevel || (thislevel = $('<li class="dropdown-header" class="thislevel" >On this level</li>'), 
            $(".skResultList").prepend(thislevel)), thislevel.after(entry));
        }
    }, skSearchUI.prototype.graphManager = null, skSearchUI;
});
define('text!skUI/skTopMenu.html',[],function () { return '\n<!-- topmenu -->\n<nav class="skNavbar navbar navbar-default navbar-fixed-top navbar-sk">\n\t<div class="container-fluid">\n\n\t\t<!-- toggle for small screens and brand -->\n\t\t<div class="navbar-header">\n\t\t\t<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">\n\t\t\t\t<span class="sr-only">Toggle navigation</span>\n\t\t\t\t<span class="icon-bar"></span>\n\t\t\t\t<span class="icon-bar"></span>\n\t\t\t\t<span class="icon-bar"></span>\n\t\t\t</button>\n\t\t\t<a class="navbar-brand">\n\t\t\t\t<img src="images/SkoreLogo20.png">\n\t\t\t</a>\n\t\t</div>\n\n\t\t<!-- Collect the nav links, forms, and other content for toggling -->\n\t\t<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">\n\n\t\t\t<div class="nav navbar-nav"></div>\n\n\t\t\t<!-- edit / view (editor) here -->\n\n\n\t\t\t<ul class="nav navbar-nav forGraphEditor">\n\n\t\t\t\t<!-- navigator : back -->\n\n\t\t\t\t<li data-skaction="navback" class="skNavBack disabled" data-toggle="tooltip" data-placement="bottom" title="Back to where you were in the process">\n\t\t\t\t\t<a>\n\t\t\t\t\t\t<i class="fa fa-arrow-circle-left"></i>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\n\t\t\t\t<!-- navigator : forward -->\n\n\t\t\t\t<li data-skaction="navforward" class="skNavForward disabled" data-toggle="tooltip" data-placement="bottom" title="Go to where you have been in the process">\n\t\t\t\t\t<a>\n\t\t\t\t\t\t<i class="fa fa-arrow-circle-right"></i>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\n\t\t\t\t<!-- zoom -->\n\n\t\t\t\t<li data-skaction="zoomOut" class="skMenuView" data-toggle="tooltip" data-placement="bottom" title="Smaller (zoom out)">\n\t\t\t\t\t<a>\n\t\t\t\t\t\t<i class="zoomOff fa fa-fw fa-search-minus"></i>\n\t\t\t\t\t\t<i class="zoomOn fa fa-fw fa-minus" style="display:none"></i>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li data-skaction="zoomIn" class="skMenuView"  data-toggle="tooltip" data-placement="bottom" title="BIGGER (zoom in)">\n\t\t\t\t\t<a>\n\t\t\t\t\t\t<i class="zoomOff fa fa-fw fa-search-plus"></i>\n\t\t\t\t\t\t<i class="zoomOn fa fa-fw fa-plus" style="display:none"></i>\n\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li data-skaction="zoomReset" class="skMenuView" data-toggle="tooltip" data-placement="bottom" title="Reset zoom">\n\t\t\t\t\t<a>\n\t\t\t\t\t\t<i class="zoomOff fa fa-fw fa-search"></i>\n\t\t\t\t\t\t<i class="zoomOn fa fa-fw skMenuViewEgal" style="display:none;"></i>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li data-skaction="zoomFit" class="skMenuView" data-toggle="tooltip" data-placement="bottom" title="Zoom to fit all content">\n\t\t\t\t\t<a>\n\t\t\t\t\t\t<i class="zoomOff fa fa-fw fa-search"></i>\n\t\t\t\t\t\t<i class="zoomOn fa fa-fw fa-arrows" style="display:none"></i>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li data-skaction="zoomFitPageWidth" class="skMenuView" data-toggle="tooltip" data-placement="bottom" title="Zoom to fit page width">\n\t\t\t\t\t<a>\n\t\t\t\t\t\t<i class="zoomOff fa fa-fw fa-search"></i>\n\t\t\t\t\t\t<i class="zoomOn fa fa-fw fa-arrows-h" style="display:none"></i>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t\t<li>\n\t\t\t\t\t<a data-skaction="modal_print" class="skSideMenu_print" style="display: none">\n\t\t\t\t\t\t<i class="fa fa-print"></i>\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\n\t\t\t</ul>\n\n\t\t\t<!-- search -->\n\n\t\t\t<form class="navbar-form navbar-left nav menuSearchForm forGraphEditor" onsubmit="return false" role="search">\n\t\t\t\t<div class="form-group">\n\t\t\t\t\t<div class="input-group input-group-sm">\n\n\t\t\t\t\t\t<!-- input field -->\n\t\t\t\t\t\t<input type="text" class="form-control skMenuFind_input selectionAllowed" placeholder="Search">\n\n\t\t\t\t\t\t<!-- <span class="input-group-addon " id="basic-addon2">results</span> -->\n\n\t\t\t\t\t\t<span class="input-group-btn">\n\t\t\t\t\t\t<!-- result -->\n\t\t\t\t\t\t<button class="btn btn-default dropdown-toggle skMenuFind_match"\tdata-toggle="dropdown" aria-haspopup="true"\n\t\t\t\t\t\t\taria-expanded="false" type="button"><span class="skMenuFind_matchResults">Results</span><span class="caret"></span></button>\n\t\t\t\t\t\t<ul class="skResultList dropdown-menu dropdown-menu-right">\n\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t</span>\n\n\t\t\t\t\t\t<span class="input-group-btn">\n\n\t\t\t\t\t\t\t<!-- filter button + dropdown -->\n\t\t\t\t\t\t\t<button class="btn btn-default dropdown-toggle skSearchFilter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" type="button">Filter <span class="caret"></span></button>\n\t\t\t\t\t\t\t<ul class="skFilterList dropdown-menu dropdown-menu-right">\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a>\n\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input\n\t\t\t\t\t\t\t\t\t\t\t\t\tdata-searchboxtype="whatbox"\n\t\t\t\t\t\t\t\t\t\t\t\t\tclass="skSearchFilter"\n\t\t\t\t\t\t\t\t\t\t\t\t\ttype="checkbox"\n\t\t\t\t\t\t\t\t\t\t\t\t\tchecked> Whatboxes\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a>\n\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input\n\t\t\t\t\t\t\t\t\t\t\t\tclass="skSearchFilter"\n\t\t\t\t\t\t\t\t\t\t\t\tdata-searchboxtype="responsibilities"\n\t\t\t\t\t\t\t\t\t\t\t\ttype="checkbox" checked> Responsibilities\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a>\n\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input\n\t\t\t\t\t\t\t\t\t\t\t\t\tclass="skSearchFilter skSearchYBText"\n\t\t\t\t\t\t\t\t\t\t\t\t\ttype="checkbox"\n\t\t\t\t\t\t\t\t\t\t\t\t\tdata-searchboxtype="whybox"\n\t\t\t\t\t\t\t\t\t\t\t\t\tchecked> Whyboxes\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a>\n\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input\n\t\t\t\t\t\t\t\t\t\t\t\t\tclass="skSearchFilter skSearchSNText"\n\t\t\t\t\t\t\t\t\t\t\t\t\ttype="checkbox"\n\t\t\t\t\t\t\t\t\t\t\t\t\tdata-searchboxtype="stickynote"\n\t\t\t\t\t\t\t\t\t\t\t\t\tchecked> Stickynotes\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li role="separator" class="divider"></li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a>\n\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input\n\t\t\t\t\t\t\t\t\t\t\t\t\tclass="skSearchFilter"\n\t\t\t\t\t\t\t\t\t\t\t\t\ttype="checkbox"\n\t\t\t\t\t\t\t\t\t\t\t\t\tdata-searchboxtype="attachments"\n\t\t\t\t\t\t\t\t\t\t\t\t\tchecked> Attachments\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a>\n\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input class="skSearchFilter skSearchID" type="checkbox" data-searchboxtype="id" checked> ID\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li role="separator" class="divider"></li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a >\n\t\t\t\t\t\t\t\t\t\t<div class="radio">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input class="skSearchFilter" type="radio" name="searchScope" id="withchildren" value="withchildren" checked>\n\t\t\t\t\t\t\t\t\t\t\t\tThis level and detailed views\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a >\n\t\t\t\t\t\t\t\t\t\t<div class="radio">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input class="skSearchFilter" type="radio" name="searchScope" id="currentonly" value="currentonly">\n\t\t\t\t\t\t\t\t\t\t\t\tThis level only\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a >\n\t\t\t\t\t\t\t\t\t\t<div class="radio">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input class="skSearchFilter" type="radio" name="searchScope" id="entireprocess" value="entireprocess">\n\t\t\t\t\t\t\t\t\t\t\t\tEntire process\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t<li role="separator" class="divider"></li>\n\t\t\t\t\t\t\t\t<li>\n\t\t\t\t\t\t\t\t\t<a>\n\t\t\t\t\t\t\t\t\t\t<div class="checkbox">\n\t\t\t\t\t\t\t\t\t\t\t<label>\n\t\t\t\t\t\t\t\t\t\t\t\t<input class="skSearchFilter skSearchExctMtch" data-searchboxtype="exactmatch" type="checkbox"> Exact match\n\t\t\t\t\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t</ul>\n\n\t\t\t\t\t\t\t<!-- clear button -->\n\t\t\t\t\t\t\t<button style="display: inline-block;" type="button" class="btn btn-default skMenuFind_clear">&times;\n\n\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t</span>\n\n\n\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\n\t\t\t</form>\n\n\t\t\t<!-- save button (web) -->\n\t\t\t<ul class="nav navbar-nav navbar-right skSave" style="display:none"><li><a class="skSaveNow">Save now</a></li></ul>\n\n\n\t\t\t</div> <!-- /.collapse navbar-collapse -->\n\t\t</div> <!-- /.nav container-fluid -->\n\t</nav> <!-- /.nav -->\n';});
define('skUI/skTopMenu',['require','text!./skTopMenu.html','jquery'],function(require) {
    "use strict";
    function skTopMenu() {
        var myMenu = $(require("text!./skTopMenu.html"));
        $(document.body).prepend(myMenu), $(".skMenuView").hover(function() {
            $(".zoomOff").toggle(), $(".zoomOn").toggle();
        });
    }
    var $ = require("jquery");
    return skTopMenu;
});
define('skModal/skModal',['require','jquery','skShell'],function(require) {
    "use strict";
    function skModal(navigatorUI) {
        this.modals = [], this.navigatorUI = navigatorUI;
    }
    var $ = require("jquery"), skShell = require("skShell");
    return skModal.prototype.navigatorUI = null, skModal.prototype.modals = null, skModal.prototype.load = function(name, html, shown, hidden, firstLoadFunction) {
        if (!$("#" + name).length) {
            var myModal = $(html);
            $(document.body).append(myModal), shown && myModal.on("shown.bs.modal", shown), 
            hidden && myModal.on("hidden.bs.modal", hidden), myModal.on("hidden.bs.modal", function() {
                myModal.remove();
            }), firstLoadFunction && firstLoadFunction(myModal), this.modals[name] = myModal;
        }
        return this.modals[name];
    }, skModal.prototype.fit_modal_body = function(modal) {
        var body, bodypaddings, header, headerheight, footer, footerheight, height, modalheight;
        header = $(".modal-header", modal), body = $(".modal-body", modal), footer = $(".modal-footer", modal), 
        modalheight = parseInt(modal.css("height"), 10), headerheight = parseInt(header.css("height"), 10) + parseInt(header.css("padding-top"), 10) + parseInt(header.css("padding-bottom"), 10), 
        footerheight = parseInt(footer.css("height"), 10) + parseInt(footer.css("padding-top"), 10) + parseInt(footer.css("padding-bottom"), 10), 
        bodypaddings = parseInt(body.css("padding-top"), 10) + parseInt(body.css("padding-bottom"), 10), 
        height = modalheight - footerheight - headerheight - bodypaddings - 5, $(".skXMLTextarea").css("height", "" + height + "px");
    }, skModal.prototype.showSideModal = function(modal$, onCloseFunc, position) {
        var that = this;
        position = position || "right";
        var positionClass = "right" === position ? "sideRight" : "sideLeft";
        $(".sideModal." + positionClass).hide(), $(document.body).removeClass("modal-open"), 
        modal$.show(), $(document.body).addClass("modal-open"), modal$.trigger("shown.bs.modal");
        var w = window.localStorage.getItem("skSideModalWidth_" + modal$.id);
        if (w || (w = "430px"), $(".modal-dialog", modal$).css({
            width: w
        }), $(".modal-content", modal$).css({
            width: w
        }), !$(".modal-header", modal$).hasClass("ui-resizable")) {
            var handles = "right" === position ? "w" : "e";
            $(".modal-content", modal$).resizable({
                minWidth: 300,
                handles: handles,
                ghost: !0,
                helper: "ui-resizable-helper",
                resize: function(event, ui) {
                    $(ui.helper).css({
                        "z-index": "99999"
                    });
                },
                stop: function(event, ui) {
                    $(".modal-dialog, .modal-content", modal$).css({
                        width: ui.size.width + "px"
                    }), "right" === position && $(".modal-dialog, .modal-content", modal$).css({
                        left: ""
                    }), window.localStorage.setItem("skSideModalWidth_" + modal$.id, "" + ui.size.width + "px"), 
                    that.navigatorUI.refreshNow(), that.navigatorUI.getCurrentGraph().sizeDidChange();
                }
            });
        }
        $(".modal-header .close, .modal-footer .close", modal$).on("click", function() {
            onCloseFunc && "function" == typeof onCloseFunc && onCloseFunc().allowClose === !1 ? skShell.setStatus(onCloseFunc().message) : (modal$.hide(), 
            $(document.body).removeClass("modal-open"), modal$.trigger("hidden.bs.modal"), $(".modal-header .close, .modal-footer .close", modal$).off("click"), 
            that.navigatorUI.refreshNow(), that.navigatorUI.getCurrentGraph().sizeDidChange());
        }), that.navigatorUI.refreshNow(), that.navigatorUI.getCurrentGraph().sizeDidChange();
    }, skModal;
});
define('text!skUI/skTitle.html',[],function () { return '<ol class="skTitle breadcrumb navigators skSearchable forGraphEditor">\n\t<li><a>Loading...</a></li>\n</ol>\n';});
define('skUI/skTitle',['require','text!./skTitle.html','jquery','mxEvent','mxUtils','skConstants','skUtils','skGraph/skGraphUtils'],function(require) {
    "use strict";
    function skTitle(graphManager) {
        if (0 === $(".skTitle").length) {
            var tit = $(require("text!./skTitle.html"));
            $(document.body).prepend(tit);
        }
        this.container = $(".skTitle"), this.graphManager = graphManager;
        var that = this;
        graphManager.addListener("newGraphCreated", mxUtils.bind(that, function(sender, event) {
            this.initForGraph(event.getProperty("graph"));
        })), graphManager.addListener("navigationDone", mxUtils.bind(that, function(sender, event) {
            this.update(event.getProperty("graph"));
        })), graphManager.addListener("graphEnabled", mxUtils.bind(that, function(sender, event) {
            this.update(event.getProperty("graph"));
        })), graphManager.addListener("contentLoaded", mxUtils.bind(that, function(sender, event) {
            this.update(event.getProperty("graph"));
        })), graphManager.addListener("upadteTitle", mxUtils.bind(that, function(sender, event) {
            this.update(event.getProperty("graph"));
        }));
    }
    var $ = require("jquery"), mxEvent = require("mxEvent"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skUtils = require("skUtils"), skGraphUtils = require("skGraph/skGraphUtils");
    return skTitle.prototype.updateTitleOnClick = function() {
        this.graphManager.getCurrentGraph().model.root == this.graphManager.getCurrentGraph().getDefaultParent().parent && (this.graphManager.getCurrentGraph().cellEditor.textarea || (this.graphManager.getCurrentGraph().cellEditor.init(), 
        this.graphManager.getCurrentGraph().cellEditor.textarea.style.display = "none"), 
        this.graphManager.getCurrentGraph().cellEditor.startEditing(this.graphManager.getCurrentGraph().getDefaultParent()));
    }, skTitle.prototype.graphManager = null, skTitle.prototype.initForGraph = function(graph) {
        graph.getView().addListener(mxEvent.DOWN, mxUtils.bind(this, function() {
            this.update();
        })), graph.getView().addListener(mxEvent.UP, mxUtils.bind(this, function() {
            this.update();
        })), graph.addListener("updateTitle", mxUtils.bind(this, function() {
            this.update();
        }));
    }, skTitle.prototype.update = function(myGraph) {
        myGraph = myGraph || this.graphManager.getCurrentGraph();
        var beforeUpdateCount = $("li", this.container).length;
        this.container.empty();
        var i, allCells = skGraphUtils.getBreadcrumbs(myGraph), activeCell = null;
        for (i = 0; i < allCells.length; i++) {
            activeCell = allCells[i];
            var text = skUtils.renderMarkdown(activeCell.text, !0);
            "" === text ? text = 0 === i ? "Click here to name your process" : "(unnamed)" : " " === text && (text = "&nbsp;");
            var tmp = $("<li><a>" + text + "</a></li>");
            this.container.append(tmp);
            var link = $("a", tmp);
            1 == allCells.length && myGraph.isEnabled() ? (link.css({
                cursor: "text"
            }), $(link).on("click", mxUtils.bind(this, this.updateTitleOnClick))) : i + 1 === allCells.length || activeCell.isGroup ? link.addClass("isGroup") : this.attachNavigationAction(myGraph, activeCell.cell, link[0]);
        }
        allCells.length >= beforeUpdateCount + 1 && $("li:last", this.container).animateCss("bounceInRight");
    }, skTitle.prototype.attachNavigationAction = function(myGraph, cell, span) {
        mxEvent.addGestureListeners(span, null, null, mxUtils.bind(this, function() {
            myGraph.navigateTo(cell, !1, skConstants.NAV_BREADCRUMB);
        }));
    }, skTitle.prototype.hide = function() {
        this.container.css("display", "none");
    }, skTitle.prototype.show = function() {
        this.container.css("display", "");
    }, skTitle;
});
define('text!skUI/skDiagramNavigator.html',[],function () { return '<div class="forGraphEditor skDiagramNavigator navigators breadcrumb">\n\t<a ><i class="fa fa-list"></i></a>\n</div>\n';});
define('skUI/skDiagramNavigator',['require','text!./skDiagramNavigator.html','jquery','skUtils','skConstants'],function(require) {
    "use strict";
    function skDiagramNavigator(graphManager) {
        $(document.body).prepend($(require("text!./skDiagramNavigator.html"))), this.graphManager = graphManager;
        var that = this;
        graphManager.addListener("graphEnabled", function() {
            that.tree = null;
        }), graphManager.addListener("navigationDone", function() {
            that.tree = null;
        }), this.initPopover($(".skDiagramNavigator"));
    }
    var $ = require("jquery"), skUtils = require("skUtils"), skConstants = require("skConstants");
    return skDiagramNavigator.prototype.graphManager = null, skDiagramNavigator.prototype.treeview = null, 
    skDiagramNavigator.prototype.tree = null, skDiagramNavigator.prototype.dismiss = function(event) {
        0 === $(event.target).closest(".popover").length && $(".popover").popover("hide");
    }, skDiagramNavigator.prototype.initPopover = function(btn) {
        var that = this;
        $(btn).popover({
            title: "Diagram Navigator",
            placement: "bottom",
            container: "body",
            html: !0,
            trigger: "focus click",
            content: function() {
                return $("<div id='treeview'>Loading...</div>");
            },
            template: '<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        }).on("shown.bs.popover", function() {
            $(".tooltip").tooltip("hide");
            var treeviewDom = $("#treeview").treeview({
                data: that.getTree(),
                enableLinks: !0,
                showBorder: !1,
                onhoverColor: "black",
                showIcon: !1,
                icon: " ",
                selectedIcon: " ",
                backColor: "inherit",
                selectedBackColor: "#50DC93",
                showCheckbox: !0,
                uncheckedIcon: " ",
                checkedIcon: " ",
                collapseIcon: "fa fa-chevron-down",
                expandIcon: "fa fa-chevron-right",
                onNodeSelected: function(event, node) {
                    that.graphManager.getCurrentGraph().navigateTo(node.cell), that.refreshTooltip();
                },
                onNodeExpanded: function() {
                    that.refreshTooltip();
                },
                onNodeCollapsed: function() {
                    that.refreshTooltip();
                }
            });
            that.treeview = treeviewDom.treeview(!0);
            var node = that.treeview.findNodes(that.graphManager.getCurrentGraph().getDefaultParent().id, "g", "cell.id")[0];
            that.treeview.revealNode(node), that.treeview.selectNode(node, {
                silent: !0
            }), that.treeview.expandNode(node, {
                silent: !0
            });
            for (var parent = that.treeview.getParent(node); parent; ) that.treeview.checkNode(parent), 
            parent = that.treeview.getParent(parent);
            $(".skDiagramContainer").one("click", that.dismiss), that.refreshTooltip(), $(".popover-content").css({
                "max-height": "" + (parseInt(window.innerHeight, 10) - 130) + "px"
            });
        }).on("hidden.bs.popover", function() {
            $(".skDiagramContainer").off("click", that.dismiss), $(".tooltip").tooltip("hide");
        });
    }, skDiagramNavigator.prototype.refreshTooltip = function() {
        $(".tooltip").tooltip("hide");
        var that = this;
        setTimeout(function() {
            $(".treeview .node-treeview").tooltip({
                container: "body",
                placement: "right",
                html: !0,
                title: function() {
                    var cell = that.treeview.getNode($(this).data("nodeid")).cell, type = skConstants.BOX;
                    return cell.isGroup() && (type = "group"), "" + skUtils.renderMarkdown(cell.getBoxText(), !0) + "<br/>(" + type + " id : " + cell.id + ")";
                }
            });
        }, 100);
    }, skDiagramNavigator.prototype.getTree = function() {
        return this.tree || (this.tree = this.getCellsTree()), this.tree;
    }, skDiagramNavigator.prototype.getCellsTree = function(root) {
        var graph = this.graphManager.getCurrentGraph();
        root = root || graph.model.root.children[0];
        var obj, tree = [];
        if (root.children || root == graph.getDefaultParent()) {
            var text = skUtils.renderMarkdown(root.getBoxText(), !0);
            "" === text && (text = "(unnamed)");
            var selectable = !0;
            root.isGroup() && (selectable = !1), obj = {
                text: text,
                cell: root,
                selectable: selectable
            }, tree.push(obj);
            for (var childCount = graph.model.getChildCount(root), childTree = [], i = 0; childCount > i; i++) {
                var child = graph.model.getChildAt(root, i);
                childTree = childTree.concat(this.getCellsTree(child));
            }
            childTree.length && (obj.nodes = childTree);
        }
        return tree;
    }, skDiagramNavigator;
});
define('skUI/skProcessAnalyticsSwitch',['require','jquery','mxEventObject','skShell'],function(require) {
    "use strict";
    function skProcessAnalyticsSwitch(navigatorUI) {
        var isSideMenuMini = !1, switchButtons = $('<li class="active"><a class="skMapButton">Work processes</a></li><li><a class="skProcessAnalytics">Analytics</a></li>');
        $(".skNavbar .navbar-nav.nav").first().append(switchButtons), $(".skMapButton", switchButtons).on("click", function() {
            if (!$(this).parent().hasClass("active")) {
                $(this).parent().addClass("active"), $(".skProcessAnalytics").parent().removeClass("active"), 
                $(".skContainerForAnalytics").empty().hide(), $(".forGraphEditor").show(), $(".skTitle").show(), 
                isSideMenuMini ? $(".canBeMini").addClass("mini").hide() : $(".canBeMini").removeClass("mini").show();
                var graph = navigatorUI.getCurrentGraph();
                graph.cellHTMLCache = [], graph.view.invalidate(), graph.view.refresh(), graph.fireEvent(new mxEventObject("updatetitle")), 
                navigatorUI.refresh(), graph.sizeDidChange(), graph.fireEvent(new mxEventObject("refreshoutline"));
            }
        }), $(".skProcessAnalytics", switchButtons).on("click", function() {
            if (!$(this).parent().hasClass("active")) {
                $(this).parent().addClass("active"), $(".skMapButton").parent().removeClass("active"), 
                $(".skTitle").hide(), $(".forGraphEditor").hide(), isSideMenuMini = $(".canBeMini").hasClass("mini") ? !0 : !1;
                var iframe;
                iframe = $("<iframe name='analytics' src='analytics.html' frameborder='0' style='overflow:hidden;height:100%;width:100%'height='100%' width='100%' />"), 
                window.setupGraphFromParent = function() {
                    var frame = window.frames.analytics;
                    frame.setupGraphInFrame(navigatorUI.getCurrentGraph(), skShell.allowSave, !1, window.requireNode);
                };
                var window_h = parseInt(window.innerHeight, 10), window_w = parseInt(window.innerWidth, 10), nav_h = $(".skNavbar").outerHeight() || 0;
                $(".skContainerForAnalytics").empty().show().css({
                    position: "fixed",
                    width: "" + window_w + "px",
                    height: "" + (window_h - nav_h) + "px",
                    top: "" + nav_h + "px",
                    bottom: "0px"
                }).append(iframe), $(iframe).load(function() {});
            }
        });
    }
    var $ = require("jquery"), mxEventObject = require("mxEventObject"), skShell = require("skShell");
    return skProcessAnalyticsSwitch;
});
define('skMenu/skCardshoe',['require','mxUtils','skConstants','jquery','skUtils'],function(require) {
    "use strict";
    function skCardshoe(graphManager) {
        this.graphManager = graphManager, $('.cardshoe [data-toggle="tooltip"]').tooltip(), 
        this.dragsource_wb = this.graphManager.makeCardshoeBoxDraggable($(".skSideMenu .cardshoe-box-wb")[0], skConstants.whatbox), 
        this.dragsource_yb = this.graphManager.makeCardshoeBoxDraggable($(".skSideMenu .cardshoe-box-yb")[0], skConstants.whybox), 
        this.dragsource_sn = this.graphManager.makeCardshoeBoxDraggable($(".skSideMenu .cardshoe-box-sn")[0], skConstants.stickynote), 
        this.setEnabled(!1), this.graphManager.addListener("graphEnabled", mxUtils.bind(this, function(sender, event) {
            this.setEnabled(event.getProperty("enabled"));
        })), this.graphManager.addListener("navigationDone", mxUtils.bind(this, function(sender, event) {
            this.buildIOList(event.getProperty("destinationCell"));
        }));
    }
    var mxUtils = require("mxUtils"), skConstants = require("skConstants"), $ = require("jquery"), skUtils = require("skUtils");
    return skCardshoe.prototype.dragsource_yb = null, skCardshoe.prototype.dragsource_wb = null, 
    skCardshoe.prototype.dragsource_sn = null, skCardshoe.prototype.buildIOList = function(destinationCell) {
        var that = this;
        $(".skWingIOList").empty(), $(".skWhyBoxIOList").hide();
        var y, ioList = this.graphManager.getIOWhyboxes(destinationCell), yb = '<div class="cardshoe"><span class="cardshoe-box cardshoe-box-yb" data-toggle="tooltip" data-placement="bottom" title="Drag &amp; drop on the canvas"><span class="cardshoe-box-text"></span></span></div>', createList = function(list, placeholder) {
            var listIO = $("ul", placeholder);
            listIO.empty(), list.length ? ($(placeholder).show(), listIO.empty(), list.forEach(function(cell) {
                y = $(yb), $(".cardshoe-box-text", y).html(skUtils.renderMarkdown(cell.getBoxText(), !0)), 
                listIO.append(y), that.additional.push(that.graphManager.makeDraggable(y[0], skConstants.whybox, cell.value.cloneNode(!0)));
            })) : ($(placeholder).hide(), listIO.empty());
        };
        this.additional = [], createList(ioList.inputs, $(".skWhyBoxInputsList")), createList(ioList.outputs, $(".skWhyBoxOutputsList"));
    }, skCardshoe.prototype.getCurrentGraph = function() {
        return this.graphManager.getCurrentGraph();
    }, skCardshoe.prototype.additional = null, skCardshoe.prototype.setEnabled = function(enabled) {
        this.dragsource_wb.setEnabled(enabled), this.dragsource_yb.setEnabled(enabled), 
        this.dragsource_sn.setEnabled(enabled), this.additional && this.additional.forEach(function(source) {
            source.setEnabled(enabled);
        }), $(".cardshoe").toggleClass("disabled", !enabled);
    }, skCardshoe;
});
define('skGraph/skGraphIcons',['require','jquery','mxCellRenderer','mxConstants','mxEventObject','mxGraph','mxGraphView','mxRectangle','mxText','skConstants'],function(require) {
    "use strict";
    var $ = require("jquery"), mxCellRenderer = require("mxCellRenderer"), mxConstants = require("mxConstants"), mxEventObject = require("mxEventObject"), mxGraph = require("mxGraph"), mxGraphView = require("mxGraphView"), mxRectangle = require("mxRectangle"), mxText = require("mxText"), skConstants = require("skConstants");
    mxGraph.prototype.getIcon = {}, mxGraph.prototype.getIcon.enterDetail = function(cell, force) {
        if (cell && cell.isWhatbox() && (force && this.isEnabled() || cell.children && cell.children.length)) {
            var style = " style='";
            cell.children && cell.children.length ? this.getCellStyle(cell)[skConstants.DETAIL_COLOR] && (style += " color:" + this.getCellStyle(cell)[skConstants.DETAIL_COLOR] + "; ") : style += " color:#808080; ";
            var fillColor = this.getCellStyle(cell)[mxConstants.STYLE_FILLCOLOR];
            fillColor && "transparent" !== fillColor && (style += "background: #ffffff;background: -moz-linear-gradient   (top,       rgba(255,255,255,0) 0%, rgba(255,255,255,0) 9%, #ffffff 10%, #ffffff 80%, " + fillColor + " 81%, " + fillColor + " 100%);background: -webkit-linear-gradient(top,       rgba(255,255,255,0) 0%, rgba(255,255,255,0) 9%, #ffffff 10%, #ffffff 80%, " + fillColor + " 81%, " + fillColor + " 100%);background: linear-gradient        (to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,0) 9%, #ffffff 10%, #ffffff 80%, " + fillColor + " 81%, " + fillColor + " 100%);"), 
            style += "'";
            var toi = "<div class='actionIconWrapper'><i " + style + ' class="fa fa-toggle-down actionIcon skBoxActionIcon"  ></i></div>';
            return toi;
        }
        return null;
    }, mxGraph.prototype.getIcon.attachment = function(cell, force) {
        if (cell && cell.isWhatbox() && (force && this.isEnabled() || cell.numberOfAttachments())) {
            var style = "";
            this.getCellStyle(cell)[skConstants.ATT_COLOR] && (style = ' style="color:' + this.getCellStyle(cell)[skConstants.ATT_COLOR] + '" '), 
            cell.numberOfAttachments() || (style = ' style="color:#808080" ');
            var toi = "<div " + style + '><i class="fa fa-paperclip actionIconWrapper actionIcon skBoxActionIcon" ></i></div>';
            return toi;
        }
    }, mxGraph.prototype.getIcon.remove = function(cell) {
        if (this.isEnabled() && cell && this.getSelectionCell() === cell && this.currentCell === cell && !cell.isGroup()) {
            var toi = '<div><i class="fa fa-trash actionIconWrapper actionIcon actionIconRemove skBoxActionIcon"></i></div>';
            return toi;
        }
    }, mxGraph.prototype.getIcon.boxnumber = function(cell) {
        return this.isEnabled() && this.showBadgeIcon && cell && !cell.isEdge() ? "<div class='badge actionIconBadge'>" + cell.id + "</div>" : void 0;
    }, mxGraphView.prototype.createStateIcon = function(icon, state, force) {
        var currentIcon = this.graph.getIcon[icon].call(this.graph, state.cell, force);
        state.icon = state.icon || {}, null == state.shape || state.icon[icon] || null == currentIcon || (state.icon[icon] = new mxText(currentIcon, new mxRectangle(), mxConstants.ALIGN_LEFT, mxConstants.ALIGN_TOP), 
        state.icon[icon].dialect = state.shape.dialect, state.icon[icon].dialect = mxConstants.DIALECT_STRICTHTML, 
        this.graph.cellRenderer.initializeLabel(state, state.icon[icon]));
    };
    var oCreateState = mxGraphView.prototype.createState;
    mxGraphView.prototype.createState = function(cell, force) {
        var state = this.getState(cell) || oCreateState.apply(this, arguments);
        return null == state.cell.geometry || state.cell.geometry.relative || (state.icon = state.icon || {}, 
        this.createStateIcon("enterDetail", state, force), this.createStateIcon("attachment", state, force), 
        this.createStateIcon("remove", state, force), this.createStateIcon("boxnumber", state, force)), 
        state;
    }, mxCellRenderer.prototype.redrawIcon = function(icon, state, offset, force) {
        if (null != state.shape && state.icon[icon]) {
            var graph = state.view.graph, scale = state.view.getScale(), bounds = new mxRectangle(state.x + offset.x, state.y + offset.y, 35, 0);
            state.icon[icon].state = state, state.icon[icon].value = graph.getIcon[icon].call(graph, state.cell, force), 
            state.icon[icon].scale = scale, state.icon[icon].bounds = bounds, state.icon[icon].redraw();
            var dismiss = function() {
                0 === $(event.target).closest(".popover").length && $(".popover").popover("hide");
            };
            $(".skBoxActionIcon", state.icon[icon].node).parent().off("click touchend"), $(".skBoxActionIcon", state.icon[icon].node).parent().on("click touchend", function() {
                "enterDetail" == icon && (state.cell.children && state.cell.children.length > 0 || $(this).data("bs.popover") || localStorage.hasOwnProperty("detailedViewPopover") && 1 === parseInt(localStorage.getItem("detailedViewPopover"), 10) ? state.view.graph.navigateTo(state.cell) : ($(this).popover({
                    content: function() {
                        var popoverTrigger = $(this), buttons = $('<div><div><button style="width:100%; padding-bottom:5px;" class="popBtn popOK btn btn-primary btn-sm">Create detailed view</button></div><div><button style="width:100%" class="popBtn popCancel btn btn-default btn-sm">Ignore</button></div><div class="checkbox"><label><input class="popBtn" type="checkbox" id="detailedViewPopover" >Don\'t ask me again</label></div></div>');
                        return $(".popBtn", buttons).on("click", function() {
                            popoverTrigger.popover("destroy"), dismiss();
                        }), $(".popOK", buttons).on("click", function() {
                            state.view.graph.navigateTo(state.cell);
                        }), $("#detailedViewPopover", buttons).on("change", function() {
                            localStorage.setItem("detailedViewPopover", 1);
                        }), buttons;
                    },
                    container: "body",
                    placement: "auto top",
                    html: "true"
                }).on("hidden.bs.popover", function() {
                    $(".skDiagramContainer").off("click", dismiss);
                }).on("shown.bs.popover", function() {
                    $(".skDiagramContainer").one("click", dismiss);
                }), $(this).popover("show"))), "remove" == icon && ($(this).data("bs.popover") || localStorage.hasOwnProperty("removeViewPopover") && 1 === parseInt(localStorage.getItem("removeViewPopover"), 10) ? graph.removeCells([ state.cell ]) : ($(this).popover({
                    content: function() {
                        var popoverTrigger = $(this), buttons = $('<div><div><button style="width:100%; padding-bottom:5px;" class="popBtn popOK btn btn-primary btn-sm">Remove box</button></div><div><button style="width:100%" class="popBtn popCancel btn btn-default btn-sm">Ignore</button></div><div class="checkbox"><label><input class="popBtn" type="checkbox" id="removeViewPopover" >Don\'t ask me again</label></div></div>');
                        return $(".popBtn", buttons).on("click", function() {
                            popoverTrigger.popover("destroy"), dismiss();
                        }), $(".popOK", buttons).on("click", function() {
                            graph.removeCells([ state.cell ]);
                        }), $("#removeViewPopover", buttons).on("change", function() {
                            localStorage.setItem("removeViewPopover", 1);
                        }), buttons;
                    },
                    container: "body",
                    placement: "auto bottom",
                    html: "true"
                }).on("hidden.bs.popover", function() {
                    $(".skDiagramContainer").off("click", dismiss);
                }).on("shown.bs.popover", function() {
                    $(".skDiagramContainer").one("click", dismiss);
                }), $(this).popover("show"))), "attachment" == icon && graph.fireEvent(new mxEventObject("openAttachment", "cell", state.cell, "forceOpen", !0, "atttype", "attachments"));
            });
        }
    };
    var oRedraw = mxCellRenderer.prototype.redraw;
    mxCellRenderer.prototype.redraw = function(state, force) {
        if (state) {
            oRedraw.apply(this, arguments), state.icon = state.icon || {};
            var scale = state.view.getScale();
            this.redrawIcon("enterDetail", state, {
                x: 0,
                y: -18 * scale
            }, force), this.redrawIcon("attachment", state, {
                x: state.width - 40 * scale,
                y: -18 * scale
            }, force), this.redrawIcon("remove", state, {
                x: 0,
                y: state.height - 18 * scale
            }, force), this.redrawIcon("boxnumber", state, {
                x: 0,
                y: state.height - 19 * scale
            }, force);
        }
    };
    var mxCellRendererprototypedestroy = mxCellRenderer.prototype.destroy;
    mxCellRenderer.prototype.destroy = function(state) {
        mxCellRendererprototypedestroy.apply(this, arguments), this.destroyIcons(state);
    }, mxCellRenderer.prototype.destroyIcons = function(state) {
        this.destroyIcon("enterDetail", state), this.destroyIcon("attachment", state), this.destroyIcon("remove", state), 
        this.destroyIcon("boxnumber", state);
    }, mxCellRenderer.prototype.destroyIconsIfNecessary = function(state) {
        state && (state.icon.remove && this.destroyIcon("remove", state), !state.view.graph.showBadgeIcon && state.icon.boxnumber && this.destroyIcon("boxnumber", state), 
        0 === state.cell.numberOfAttachments() && this.destroyIcon("attachment", state), 
        state.cell.isWhatboxDetailed() || this.destroyIcon("enterDetail", state));
    }, mxCellRenderer.prototype.destroyIcon = function(icon, state) {
        state && state.icon && null != state.icon[icon] && (state.icon[icon].destroy(), 
        state.icon[icon] = null);
    }, mxCellRenderer.prototype.getShapesForState = function(state) {
        var array = [ state.shape, state.text, state.control ];
        return state.icon && array.push(state.icon.attachment, state.icon.boxnumber, state.icon.remove, state.icon.enterDetail), 
        array;
    };
});
define('skNavigatorUI/skNavigatorUI',['require','skModal/skModal_Splash','skModal/skModal_Print','skModal/skModal_JustSkoreIt','skModal/skModal_Settings','skModal/skModal_SocialExport','skModal/skModal_kbdShortcuts','skModal/skModal_formatting','skModal/skModal_Icons','skModal/skModal_XML','skModal/skModal_Stylesheet','skModal/skModal_ImageExport','skUI/skSideMenu','./skFooter_Kbd','./skFooter_IDs','./skFooter_Icons','text!skUI/skEditMenu.html','text!skUI/skFormatMenu.html','jquery','mxEvent','mxEventSource','mxEventObject','mxClient','mxUtils','skConstants','skGraphManager/skGraphManager','skNavigatorUI/skHint','skNavigatorUI/skExternalLinkHandler','skModal/skModal_Registration','skUI/skNavHistory','skShell','skNavigatorUI/skStatus','skModal/skModal_Attachments','skUtils','skGraphEditorUI/skWellDoneMessage','skNavigatorUI/skSearchUI','skUI/skTopMenu','skModal/skModal','skUI/skTitle','skUI/skDiagramNavigator','skUI/skProcessAnalyticsSwitch','skMenu/skCardshoe','skGraph/skGraphIcons','IMAGE_PATH'],function(require) {
    "use strict";
    function skNavigatorUI() {
        mxEventSource.call(this), this.extLinkHandler = new skExternalLinkHandler(this), 
        skShell.skStatus = new skStatus(skShell), this.addListener("enableSave", function(sender, e) {
            var enable = e.properties.enable;
            skShell.allowSave = enable, enable ? $(".warningForNotAllowSave").hide() : $(".warningForNotAllowSave").show();
        }), window.onbeforeunload = mxUtils.bind(this, function() {
            return this.getCurrentGraph().documentEdited ? "All changes will be lost!" : void 0;
        }), this.ui = {}, this.ui.topMenu = new skTopMenu(), this.graphManager = new skGraphManager(), 
        this.ui.searchUI = new skSearchUI(this.graphManager), this.graphManager.addListener("graphEnabled", mxUtils.bind(this, function(sender, event) {
            var enabled = event.getProperty("enabled");
            $(".formatMenu").toggleClass("enabled", enabled), $(".editMenu").toggleClass("enabled", enabled), 
            $("[data-skaction=editMenu]").toggle(enabled), $("[data-skaction=formatMenu]").toggle(enabled);
        })), this.graphManager.addListener("contentLoaded", mxUtils.bind(this, function(sender, event) {
            event.getProperty("source") !== skConstants.SOURCE_EMBED && (skShell.splash = new (require("skModal/skModal_Splash"))());
        })), this.graphManager.addListener("openModal", mxUtils.bind(this, function(sender, event) {
            var modalName = event.getProperty("name");
            switch (modalName) {
              case "print":
                var skModal_Print = require("skModal/skModal_Print"), modalPrint = new skModal_Print(this);
                $(modalPrint).modal("toggle");
                break;

              case "justskoreit":
                var skModal_JustSkoreIt = require("skModal/skModal_JustSkoreIt"), modalJSIT = new skModal_JustSkoreIt(this);
                $(modalJSIT).modal("toggle");
                break;

              case "settings":
                var skModal_Settings = require("skModal/skModal_Settings"), modalSettings = new skModal_Settings(this);
                $(modalSettings).modal("toggle");
                break;

              case "registration":
                !this.ui.registration, $(this.ui.registration).modal("toggle");
                break;

              case "socialexport":
                var skModal_SocialExport = require("skModal/skModal_SocialExport"), modalSocExp = new skModal_SocialExport(this);
                $(modalSocExp).modal("toggle");
                break;

              case "kbd":
                var skModal_kbd = require("skModal/skModal_kbdShortcuts"), modalkbd = new skModal_kbd(this);
                this.modal.showSideModal(modalkbd);
                break;

              case "formatting":
                var skModal_formatting = require("skModal/skModal_formatting"), modal_formatting = new skModal_formatting(this);
                this.modal.showSideModal(modal_formatting);
                break;

              case "icons":
                var skModal_Icons = require("skModal/skModal_Icons"), modal_icons = new skModal_Icons(this);
                this.modal.showSideModal(modal_icons, function() {
                    return $("#skModal_Icons").remove(), {
                        allowClose: !0
                    };
                });
                break;

              case "xml":
                var skModal_XML = require("skModal/skModal_XML"), modalXML = new skModal_XML(this);
                modalXML.modal("toggle");
                break;

              case "stylesheet":
                var skModal_Stylesheet = require("skModal/skModal_Stylesheet"), modalStylesheet = new skModal_Stylesheet(this);
                modalStylesheet.data("loadtype", event.getProperty("loadtype")), modalStylesheet.modal("show");
                break;

              case "imageexport":
                var skModal_ImageExport = require("skModal/skModal_ImageExport");
                this.modalImageExport = new skModal_ImageExport(this), this.imageExportNow(event.getProperty("target"), this.modalImageExport);
                break;

              case "attachments":
                var cell = event.getProperty("cell"), forceOpen = event.getProperty("forceOpen"), atttype = event.getProperty("atttype"), modal_attachments = new skModal_Attachments(this);
                modal_attachments.removeData([ "cell", "atttype" ]), modal_attachments.data("cell", cell), 
                modal_attachments.data("atttype", atttype), (forceOpen || $(modal_attachments).is(":visible")) && this.modal.showSideModal(modal_attachments, function() {
                    return {
                        allowClose: !0
                    };
                });
            }
        }));
        var skSideMenu = require("skUI/skSideMenu");
        this.ui.sideMenu = new skSideMenu(this), this.ui.title = new skTitle(this.graphManager), 
        this.ui.navHistory = new skNavHistory(this.graphManager), this.ui.cardshoe = new skCardshoe(this.graphManager), 
        this.ui.diagramNavigator = new skDiagramNavigator(this), this.graphManager.init(), 
        this.ui.footer = {};
        var skFooter_Kbd = require("./skFooter_Kbd"), skFooter_IDs = require("./skFooter_IDs"), skFooter_Icons = require("./skFooter_Icons");
        this.ui.footer.kbd = new skFooter_Kbd(this), this.ui.footer.ids = new skFooter_IDs(this.graphManager), 
        this.ui.footer.icons = new skFooter_Icons(this.graphManager), this.modal = new skModal(this);
        var source;
        source = skConstants.SOURCE_NEW, $("[data-skaction=modal_imageexport]").hide();
        skShell.wellDoneMessage = new skWellDoneMessage(), skShell.hint = new skHint(this), 
        mxEvent.addListener(window, "resize", mxUtils.bind(this, function() {
            this.refresh();
        })), this.refresh = skUtils.debounce(mxUtils.bind(this, function() {
            this.refreshNow();
        }), 300), mxEvent.addMouseWheelListener(mxUtils.bind(this, function(evt, up) {
            var outlineWheel = !1;
            (mxEvent.isShiftDown(evt) || outlineWheel) && (up ? this.getCurrentGraph().zoomIn() : this.getCurrentGraph().zoomOut(), 
            mxEvent.consume(evt));
        })), this.refreshNow(), this.ui.processAnalyticsSwitcher = new skProcessAnalyticsSwitch(this), 
        $(".skNavbar .navbar-nav.nav").first().append(require("text!skUI/skEditMenu.html")), 
        $(".skNavbar .navbar-nav.nav").first().append(require("text!skUI/skFormatMenu.html")), 
        this.enableFileDrop();
        $(document.head).append("<style type='text/css'>.skCtrlOrCmd:before {content:" + (mxClient.IS_MAC ? "'⌘'" : "'Ctrl'") + "}</style>"), 
        (null === window.localStorage.getItem("skBg") || "happy" === window.localStorage.getItem("skBg")) && $(".mainContainer").addClass("mainContainerBackground");
    }
    var $ = require("jquery"), mxEvent = require("mxEvent"), mxEventSource = require("mxEventSource"), mxEventObject = require("mxEventObject"), mxClient = require("mxClient"), mxUtils = require("mxUtils"), skConstants = require("skConstants"), skGraphManager = require("skGraphManager/skGraphManager"), skHint = require("skNavigatorUI/skHint"), skExternalLinkHandler = require("skNavigatorUI/skExternalLinkHandler"), skNavHistory = (require("skModal/skModal_Registration"), 
    require("skUI/skNavHistory")), skShell = require("skShell"), skStatus = require("skNavigatorUI/skStatus"), skModal_Attachments = require("skModal/skModal_Attachments"), skUtils = require("skUtils"), skWellDoneMessage = require("skGraphEditorUI/skWellDoneMessage"), skSearchUI = require("skNavigatorUI/skSearchUI"), skTopMenu = require("skUI/skTopMenu"), skModal = require("skModal/skModal"), skTitle = require("skUI/skTitle"), skDiagramNavigator = require("skUI/skDiagramNavigator"), skProcessAnalyticsSwitch = require("skUI/skProcessAnalyticsSwitch"), skCardshoe = require("skMenu/skCardshoe");
    return require("skGraph/skGraphIcons"), mxUtils.extend(skNavigatorUI, mxEventSource), 
    skNavigatorUI.prototype.loadELECTRON = function() {
    }, skNavigatorUI.prototype.graphContainerImage = require("IMAGE_PATH") + "/Skore_BrainStorm_BackgroundImage_lofi.jpg", 
    skNavigatorUI.prototype.currentGraph = null, skNavigatorUI.prototype.getGraphManager = function() {
        return this.graphManager;
    }, skNavigatorUI.prototype.getCurrentGraph = function() {
        return this.getGraphManager().getCurrentGraph();
    }, skNavigatorUI.prototype.initialTopSpacing = 0, skNavigatorUI.prototype.refreshNow = function() {
        var window_h = parseInt(window.innerHeight, 10), window_w = parseInt(window.innerWidth, 10), nav_h = $(".skNavbar").outerHeight() || 0, title_h = $(".skTitle").outerHeight() || 0, diagramNavigator_w = $(".skDiagramNavigator").outerWidth() || 0;
        if ($(".forGraphEditor:visible").length) {
            var modal_header_h, modal_footer_h, h, tab_w = $(".skTabMenu").outerWidth() || 0, side_w = $(".skSideMenu").outerWidth() || 0, sideLeft = $(".sideModal.sideLeft:visible"), left_w = $(".modal-content", sideLeft).outerWidth() || 0, outline_h = $(".outlineBox").outerHeight() || 0, footer_h = $(".skFooter").outerHeight() || 0, sideRight = $(".sideModal.sideRight:visible"), right_w = $(".modal-dialog", sideRight).outerWidth() || 0, skGraphManager_h = $(".skGraphManager").outerHeight() || 0;
            if (sideRight.length) {
                var modal_header_h1 = $(".skAttHeaderTitle", sideRight).outerHeight() || 0, modal_header_h2 = $(".skAttHeaderSwitcher", sideRight).outerHeight() || 0;
                modal_footer_h = $(".modal-footer", sideRight).outerHeight() || 0, modal_header_h = modal_header_h1 + modal_header_h2, 
                sideRight.css({
                    height: "" + window_h + "px",
                    right: "0"
                }), $(".modal-content", sideRight).css({
                    height: "" + (window_h + 2) + "px"
                }), $(".modal-body", sideRight).css({
                    height: window_h - modal_header_h - modal_footer_h + 2 + "px",
                    "overflow-y": "scroll",
                    "overflow-x": "hidden"
                });
            }
            sideLeft.length && (h = window_h - nav_h - title_h - skGraphManager_h - footer_h + 2, 
            $(".modal-dialog", sideLeft).css({
                top: "" + (nav_h + title_h + skGraphManager_h - 1) + "px",
                bottom: "" + (footer_h - 1) + "px",
                height: "" + h + "px",
                left: "" + (tab_w + side_w) + "px"
            }), modal_header_h = $(".modal-header", sideLeft).outerHeight() || 0, modal_footer_h = $(".modal-footer", sideLeft).outerHeight() || 0, 
            $(".modal-content", sideLeft).css({
                height: "" + h + "px"
            }), $(".modal-body", sideLeft).css({
                height: "" + (h - modal_header_h - modal_footer_h + 2) + "px",
                "overflow-y": "scroll",
                "overflow-x": "hidden"
            })), $(".skTabMenu:visible") && $(".skTabMenu").css({
                top: "" + nav_h + "px"
            }), $(".skSideMenu:visible") && $(".skSideMenu").css({
                top: "" + nav_h + "px",
                bottom: "" + (footer_h + outline_h) + "px"
            }), $(".outlineBox").css({
                left: "" + tab_w + "px",
                width: "" + side_w + "px",
                bottom: "" + footer_h + "px"
            }), $(".skTitle:visible") && ($(".skGraphManager").css({
                top: "" + nav_h + "px",
                left: "" + (tab_w + side_w) + "px"
            }), $(".skDiagramNavigator").css({
                top: "" + (nav_h + skGraphManager_h) + "px",
                left: "" + (tab_w + side_w) + "px"
            }), $(".skTitle").css({
                left: "" + (tab_w + side_w + diagramNavigator_w) + "px",
                top: "" + (nav_h + skGraphManager_h) + "px"
            })), $(".skDiagramContainer").css({
                height: "" + (window_h - nav_h - title_h - skGraphManager_h - footer_h + 2) + "px",
                top: "" + (nav_h + title_h + skGraphManager_h - 2) + "px",
                width: "" + (window_w - tab_w - side_w - left_w - right_w + 2) + "px",
                left: "" + (tab_w + side_w + left_w) + "px",
                right: "" + right_w + "px",
                bottom: "" + footer_h + "px"
            });
        } else $(".skContainerForAnalytics").css({
            width: "" + window_w + "px",
            height: "" + (window_h - nav_h) + "px",
            top: "" + nav_h + "px",
            bottom: "0px"
        });
        this.graphManager.fireEvent(new mxEventObject("refresh"));
    }, skNavigatorUI.prototype.loadModal = function(name, html, shown, hidden, callback) {
        return this.modal.load(name, html, shown, hidden, callback);
    }, skNavigatorUI.prototype.enableFileDrop = function() {
        function handleDrop(graph, file, x, y) {
            if ("image" == file.type.substring(0, 5)) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    var data = e.target.result;
                    if ("image/svg" == file.type.substring(0, 9)) {
                        var comma = data.indexOf(","), svgText = atob(data.substring(comma + 1)), root = mxUtils.parseXml(svgText);
                        if (null != root) {
                            var svgs = root.getElementsByTagName("svg");
                            if (svgs.length > 0) {
                                var svgRoot = svgs[0], w = parseFloat(svgRoot.getAttribute("width")), h = parseFloat(svgRoot.getAttribute("height")), vb = svgRoot.getAttribute("viewBox");
                                if (null == vb || 0 === vb.length) svgRoot.setAttribute("viewBox", "0 0 " + w + " " + h); else if (isNaN(w) || isNaN(h)) {
                                    var tokens = vb.split(" ");
                                    tokens.length > 3 && (w = parseFloat(tokens[2]), h = parseFloat(tokens[3]));
                                }
                                w = Math.max(1, Math.round(w)), h = Math.max(1, Math.round(h));
                                var value = skUtils.createBoxElement(skConstants.image);
                                data = "data:image/svg+xml," + btoa(mxUtils.getXml(svgs[0], "\n"));
                                graph.insertVertex(null, null, value, x, y, w, h, "image;shape=image;image=" + data + ";");
                            }
                        }
                    } else {
                        var img = new Image();
                        img.onload = function() {
                            var w = Math.max(1, img.width), h = Math.max(1, img.height), semi = data.indexOf(";");
                            semi > 0 && (data = data.substring(0, semi) + data.substring(data.indexOf(",", semi + 1)));
                            var value = skUtils.createBoxElement(skConstants.image);
                            graph.insertVertex(null, null, value, x, y, w, h, "image;shape=image;image=" + data + ";");
                        }, img.src = data;
                    }
                }, reader.readAsDataURL(file);
            }
        }
        var that = this;
        mxEvent.addListener(document, "dragover", function(evt) {
            evt.stopPropagation(), evt.preventDefault();
        }), mxEvent.addListener(document, "drop", function(evt) {
            evt.stopPropagation(), evt.preventDefault();
            if (that.getCurrentGraph().isEnabled()) {
                for (var graph = that.getCurrentGraph(), pt = mxUtils.convertPoint(graph.container, mxEvent.getClientX(evt), mxEvent.getClientY(evt)), tr = graph.view.translate, scale = graph.view.scale, x = pt.x / scale - tr.x, y = pt.y / scale - tr.y, filesArray = event.dataTransfer.files, i = 0; i < filesArray.length; i++) handleDrop(graph, filesArray[i], x + 10 * i, y + 10 * i);
                skShell.skStatus.setAndDontShowAgain("Image handling is an experimental feature. <br/>Technical note: the image is stored directly in the xml file (<i>serialised</i> is the technical term) so the file can become very large. <br/>We appreciate feedback on this!", "DONTSHOWAGAINimageDrop");
            }
        });
    }, skNavigatorUI;
});
define('skStart',['require','jquery','skNavigatorUI/skNavigatorUI','skShell'],function(require) {
    "use strict";
    var $ = require("jquery"), skNavigatorUI = require("skNavigatorUI/skNavigatorUI"), skShell = require("skShell");
    skShell.nav = new skNavigatorUI();
    window.jQuery.fn.extend({
        animateCss: function(animationName) {
            var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
            $(this).addClass("animated " + animationName).one(animationEnd, function() {
                $(this).removeClass("animated " + animationName);
            });
        }
    }), window.textEditing = function(evt) {
        var $ = window.jQuery;
        evt = evt || window.event;
        var allowed = !1;
        return evt && (evt = $.event.fix(evt), allowed = $(evt.target).hasClass("selectionAllowed") || $(evt.target).parent(".selectionAllowed").length > 0), 
        allowed;
    }, document.onselectstart = window.textEditing, document.onmousedown = window.textEditing, 
    document.oncontextmenu = window.textEditing;
});
!function(mx) {
    "use strict";
    function makeMx(mod) {
        return function() {
            return window[mod];
        };
    }
    for (var i = 0, l = mx.length; l > i; i += 1) define(mx[i], makeMx(mx[i]));
}([ "mxArrowConnector", "mxCell", "mxCellEditor", "mxCellMarker", "mxCellOverlay", "mxCellPath", "mxCellRenderer", "mxCellState", "mxClient", "mxClipboard", "mxCodec", "mxCompactTreeLayout", "mxConnectionConstraint", "mxConnectionHandler", "mxConstants", "mxConstraintHandler", "mxDivResizer", "mxEdgeHandler", "mxEdgeSegmentHandler", "mxEdgeStyle", "mxEvent", "mxEventObject", "mxEventSource", "mxGeometry", "mxGraph", "mxGraphModel", "mxGraphView", "mxImage", "mxImageShape", "mxKeyHandler", "mxLayoutManager", "mxMorphing", "mxMouseEvent", "mxMultiplicity", "mxOutline", "mxPanningHandler", "mxPoint", "mxPolyline", "mxPrintPreview", "mxRectangle", "mxRectangleShape", "mxRubberband", "mxShape", "mxStencil", "mxStencilRegistry", "mxStylesheet", "mxText", "mxUndoManager", "mxUtils", "mxVertexHandler" ]), 
define("jquery", function() {
    "use strict";
    return window.jQuery;
}), define("marked", function() {
    "use strict";
    return window.marked;
}), define("canvg", function() {
    "use strict";
    return window.canvg;
}), define("colors", function() {
    "use strict";
    return [ {
        name: "IndianRed",
        htmlcode: "#CD5C5C"
    }, {
        name: "LightCoral",
        htmlcode: "#F08080"
    }, {
        name: "Salmon",
        htmlcode: "#FA8072"
    }, {
        name: "DarkSalmon",
        htmlcode: "#E9967A"
    }, {
        name: "LightSalmon",
        htmlcode: "#FFA07A"
    }, {
        name: "Crimson",
        htmlcode: "#DC143C"
    }, {
        name: "Red",
        htmlcode: "#FF0000"
    }, {
        name: "FireBrick",
        htmlcode: "#B22222"
    }, {
        name: "DarkRed",
        htmlcode: "#8B0000"
    }, {
        name: "Pink",
        htmlcode: "#FFC0CB"
    }, {
        name: "LightPink",
        htmlcode: "#FFB6C1"
    }, {
        name: "HotPink",
        htmlcode: "#FF69B4"
    }, {
        name: "DeepPink",
        htmlcode: "#FF1493"
    }, {
        name: "MediumVioletRed",
        htmlcode: "#C71585"
    }, {
        name: "PaleVioletRed",
        htmlcode: "#DB7093"
    }, {
        name: "LightSalmon",
        htmlcode: "#FFA07A"
    }, {
        name: "Coral",
        htmlcode: "#FF7F50"
    }, {
        name: "Tomato",
        htmlcode: "#FF6347"
    }, {
        name: "OrangeRed",
        htmlcode: "#FF4500"
    }, {
        name: "DarkOrange",
        htmlcode: "#FF8C00"
    }, {
        name: "Orange",
        htmlcode: "#FFA500"
    }, {
        name: "Gold",
        htmlcode: "#FFD700"
    }, {
        name: "Yellow",
        htmlcode: "#FFFF00"
    }, {
        name: "LightYellow",
        htmlcode: "#FFFFE0"
    }, {
        name: "LemonChiffon",
        htmlcode: "#FFFACD"
    }, {
        name: "LightGoldenrodYellow",
        htmlcode: "#FAFAD2"
    }, {
        name: "PapayaWhip",
        htmlcode: "#FFEFD5"
    }, {
        name: "Moccasin",
        htmlcode: "#FFE4B5"
    }, {
        name: "PeachPuff",
        htmlcode: "#FFDAB9"
    }, {
        name: "PaleGoldenrod",
        htmlcode: "#EEE8AA"
    }, {
        name: "Khaki",
        htmlcode: "#F0E68C"
    }, {
        name: "DarkKhaki",
        htmlcode: "#BDB76B"
    }, {
        name: "Lavender",
        htmlcode: "#E6E6FA"
    }, {
        name: "Thistle",
        htmlcode: "#D8BFD8"
    }, {
        name: "Plum",
        htmlcode: "#DDA0DD"
    }, {
        name: "Violet",
        htmlcode: "#EE82EE"
    }, {
        name: "Orchid",
        htmlcode: "#DA70D6"
    }, {
        name: "Fuchsia",
        htmlcode: "#FF00FF"
    }, {
        name: "Magenta",
        htmlcode: "#FF00FF"
    }, {
        name: "MediumOrchid",
        htmlcode: "#BA55D3"
    }, {
        name: "MediumPurple",
        htmlcode: "#9370DB"
    }, {
        name: "BlueViolet",
        htmlcode: "#8A2BE2"
    }, {
        name: "DarkViolet",
        htmlcode: "#9400D3"
    }, {
        name: "DarkOrchid",
        htmlcode: "#9932CC"
    }, {
        name: "DarkMagenta",
        htmlcode: "#8B008B"
    }, {
        name: "Purple",
        htmlcode: "#800080"
    }, {
        name: "Indigo",
        htmlcode: "#4B0082"
    }, {
        name: "SlateBlue",
        htmlcode: "#6A5ACD"
    }, {
        name: "DarkSlateBlue",
        htmlcode: "#483D8B"
    }, {
        name: "MediumSlateBlue",
        htmlcode: "#7B68EE"
    }, {
        name: "GreenYellow",
        htmlcode: "#ADFF2F"
    }, {
        name: "Chartreuse",
        htmlcode: "#7FFF00"
    }, {
        name: "LawnGreen",
        htmlcode: "#7CFC00"
    }, {
        name: "Lime",
        htmlcode: "#00FF00"
    }, {
        name: "LimeGreen",
        htmlcode: "#32CD32"
    }, {
        name: "PaleGreen",
        htmlcode: "#98FB98"
    }, {
        name: "LightGreen",
        htmlcode: "#90EE90"
    }, {
        name: "MediumSpringGreen",
        htmlcode: "#00FA9A"
    }, {
        name: "SpringGreen",
        htmlcode: "#00FF7F"
    }, {
        name: "MediumSeaGreen",
        htmlcode: "#3CB371"
    }, {
        name: "SeaGreen",
        htmlcode: "#2E8B57"
    }, {
        name: "ForestGreen",
        htmlcode: "#228B22"
    }, {
        name: "Green",
        htmlcode: "#008000"
    }, {
        name: "DarkGreen",
        htmlcode: "#006400"
    }, {
        name: "YellowGreen",
        htmlcode: "#9ACD32"
    }, {
        name: "OliveDrab",
        htmlcode: "#6B8E23"
    }, {
        name: "Olive",
        htmlcode: "#808000"
    }, {
        name: "DarkOliveGreen",
        htmlcode: "#556B2F"
    }, {
        name: "MediumAquamarine",
        htmlcode: "#66CDAA"
    }, {
        name: "DarkSeaGreen",
        htmlcode: "#8FBC8F"
    }, {
        name: "LightSeaGreen",
        htmlcode: "#20B2AA"
    }, {
        name: "DarkCyan",
        htmlcode: "#008B8B"
    }, {
        name: "Teal",
        htmlcode: "#008080"
    }, {
        name: "Aqua",
        htmlcode: "#00FFFF"
    }, {
        name: "Cyan",
        htmlcode: "#00FFFF"
    }, {
        name: "LightCyan",
        htmlcode: "#E0FFFF"
    }, {
        name: "PaleTurquoise",
        htmlcode: "#AFEEEE"
    }, {
        name: "Aquamarine",
        htmlcode: "#7FFFD4"
    }, {
        name: "Turquoise",
        htmlcode: "#40E0D0"
    }, {
        name: "MediumTurquoise",
        htmlcode: "#48D1CC"
    }, {
        name: "DarkTurquoise",
        htmlcode: "#00CED1"
    }, {
        name: "CadetBlue",
        htmlcode: "#5F9EA0"
    }, {
        name: "SteelBlue",
        htmlcode: "#4682B4"
    }, {
        name: "LightSteelBlue",
        htmlcode: "#B0C4DE"
    }, {
        name: "PowderBlue",
        htmlcode: "#B0E0E6"
    }, {
        name: "LightBlue",
        htmlcode: "#ADD8E6"
    }, {
        name: "SkyBlue",
        htmlcode: "#87CEEB"
    }, {
        name: "LightSkyBlue",
        htmlcode: "#87CEFA"
    }, {
        name: "DeepSkyBlue",
        htmlcode: "#00BFFF"
    }, {
        name: "DodgerBlue",
        htmlcode: "#1E90FF"
    }, {
        name: "CornflowerBlue",
        htmlcode: "#6495ED"
    }, {
        name: "MediumSlateBlue",
        htmlcode: "#7B68EE"
    }, {
        name: "RoyalBlue",
        htmlcode: "#4169E1"
    }, {
        name: "Blue",
        htmlcode: "#0000FF"
    }, {
        name: "MediumBlue",
        htmlcode: "#0000CD"
    }, {
        name: "DarkBlue",
        htmlcode: "#00008B"
    }, {
        name: "Navy",
        htmlcode: "#000080"
    }, {
        name: "MidnightBlue",
        htmlcode: "#191970"
    }, {
        name: "Cornsilk",
        htmlcode: "#FFF8DC"
    }, {
        name: "BlanchedAlmond",
        htmlcode: "#FFEBCD"
    }, {
        name: "Bisque",
        htmlcode: "#FFE4C4"
    }, {
        name: "NavajoWhite",
        htmlcode: "#FFDEAD"
    }, {
        name: "Wheat",
        htmlcode: "#F5DEB3"
    }, {
        name: "BurlyWood",
        htmlcode: "#DEB887"
    }, {
        name: "Tan",
        htmlcode: "#D2B48C"
    }, {
        name: "RosyBrown",
        htmlcode: "#BC8F8F"
    }, {
        name: "SandyBrown",
        htmlcode: "#F4A460"
    }, {
        name: "Goldenrod",
        htmlcode: "#DAA520"
    }, {
        name: "DarkGoldenrod",
        htmlcode: "#B8860B"
    }, {
        name: "Peru",
        htmlcode: "#CD853F"
    }, {
        name: "Chocolate",
        htmlcode: "#D2691E"
    }, {
        name: "SaddleBrown",
        htmlcode: "#8B4513"
    }, {
        name: "Sienna",
        htmlcode: "#A0522D"
    }, {
        name: "Brown",
        htmlcode: "#A52A2A"
    }, {
        name: "Maroon",
        htmlcode: "#800000"
    }, {
        name: "White",
        htmlcode: "#FFFFFF"
    }, {
        name: "Snow",
        htmlcode: "#FFFAFA"
    }, {
        name: "Honeydew",
        htmlcode: "#F0FFF0"
    }, {
        name: "MintCream",
        htmlcode: "#F5FFFA"
    }, {
        name: "Azure",
        htmlcode: "#F0FFFF"
    }, {
        name: "AliceBlue",
        htmlcode: "#F0F8FF"
    }, {
        name: "GhostWhite",
        htmlcode: "#F8F8FF"
    }, {
        name: "WhiteSmoke",
        htmlcode: "#F5F5F5"
    }, {
        name: "Seashell",
        htmlcode: "#FFF5EE"
    }, {
        name: "Beige",
        htmlcode: "#F5F5DC"
    }, {
        name: "OldLace",
        htmlcode: "#FDF5E6"
    }, {
        name: "FloralWhite",
        htmlcode: "#FFFAF0"
    }, {
        name: "Ivory",
        htmlcode: "#FFFFF0"
    }, {
        name: "AntiqueWhite",
        htmlcode: "#FAEBD7"
    }, {
        name: "Linen",
        htmlcode: "#FAF0E6"
    }, {
        name: "LavenderBlush",
        htmlcode: "#FFF0F5"
    }, {
        name: "MistyRose",
        htmlcode: "#FFE4E1"
    }, {
        name: "Gainsboro",
        htmlcode: "#DCDCDC"
    }, {
        name: "LightGrey",
        htmlcode: "#D3D3D3"
    }, {
        name: "Silver",
        htmlcode: "#C0C0C0"
    }, {
        name: "DarkGray",
        htmlcode: "#A9A9A9"
    }, {
        name: "Gray",
        htmlcode: "#808080"
    }, {
        name: "DimGray",
        htmlcode: "#696969"
    }, {
        name: "LightSlateGray",
        htmlcode: "#778899"
    }, {
        name: "SlateGray",
        htmlcode: "#708090"
    }, {
        name: "DarkSlateGray",
        htmlcode: "#2F4F4F"
    }, {
        name: "Black",
        htmlcode: "#000000"
    } ];
}), define("IMAGE_PATH", "images"), requirejs.config({
    baseUrl: "js",
    paths: {
        root: "../",
        text: "lib/text"
    }
}), requirejs([ "skStart" ]);
