
/*
 * Recently opened and "currently opened" files
 *
 */

"use strict";

var electron = require( "electron" );
var app = electron.app;
var jetpack = require( "fs-jetpack" );
var ipc = electron.ipcMain;

function skElStyleSheetsManager () {

    this.userDataDir = jetpack.cwd( app.getPath( "userData" ) );
    this.fileName = "stylesheet.json";

    var file = this.userDataDir.read( this.fileName, "json" );

    ipc.on( "updateStyleSheet", function( styleSheetXML ) {

        if ( styleSheetXML && styleSheetXML !== "" ) {

        }

    } );

}

skElStyleSheetsManager.prototype.save = function() {

    var that = this;
    this.userDataDir.write( this.fileName, {
        recentlyOpened: that.recentlyOpened,
        currentlyOpened: that.currentlyOpened
    }, { atomic:true } );

};

module.exports = skElStyleSheetsManager;
