

"use strict";

var windowStateKeeper = require( "./window_state" );

var electron = require( "electron" );
var app = require( "electron" ).app;

var fs = require( "fs" );
var fsj = require( "fs-jetpack" );
var BrowserWindow = electron.BrowserWindow;
var ipc = electron.ipcMain;
var dialog = electron.dialog;

var MenuBuilder = require( "./skElMenuOS" );
var FilesManager = require( "./skElFilesManager" );

var defaultSizes = {
    width: 1280,
    height: 720
};

function SkoreWindow( windowsOpened, skoreSaver ) {

    this.windowsOpened = windowsOpened;

    this.skoreSaver = skoreSaver;

    this.menuBuilder = new MenuBuilder( this, this.skoreSaver );
    this.filesManager = new FilesManager();

    // Register event listener
    var that = this;

}

SkoreWindow.prototype.windowWithinBounds = function( windowState, bounds ) {

    return windowState.x >= bounds.x &&
        windowState.y >= bounds.y &&
        windowState.x + windowState.width <= bounds.x + bounds.width &&
        windowState.y + windowState.height <= bounds.y + bounds.height;

};

SkoreWindow.prototype.resetToDefaults = function( windowState ) {

    var screen = electron.screen;
    var bounds = screen.getPrimaryDisplay().bounds;
    return Object.assign( {}, defaultSizes, {
        x: ( bounds.width - defaultSizes.width ) / 2,
        y: ( bounds.height - defaultSizes.height ) / 2
    } );

};
SkoreWindow.prototype.ensureVisibleOnSomeDisplay = function( /* windowState */) {

    var that = this;

    var getState = function( win ) {

        var position = win.getPosition();
        var size = win.getSize();
        return {
            x: position[ 0 ],
            y: position[ 1 ],
            width: size[ 0 ],
            height: size[ 1 ]
        };

    };

    // get all the windows
    this.windowsOpened.forEach( function( window ) {

        var state = getState( window );
        var screen = electron.screen;
        var visible = screen.getAllDisplays().some( function( display ) {

            return that.windowWithinBounds( state, display.bounds );

        } );
        if ( !visible ) {

            // Window is partially or fully not visible now.
            // Reset it to safe defaults.
            return that.resetToDefaults( state );

        }

    } );

    // return windowState;

};

SkoreWindow.prototype.windowsOpened = null;

// SkoreWindow.prototype.filePathToBeLoaded = null;
SkoreWindow.prototype.lastWindowState = null;
SkoreWindow.prototype.skoreSaver = null;

// SkoreWindow.prototype.builtMenu = null;

SkoreWindow.prototype.newWindow = function() {

    console.log( "SkoreWindow", "newWindow" );

    // Preserver of the window size and position between app launches.
    var mainWindowState = windowStateKeeper( "main", defaultSizes );

    // Opens a new window "like" the one we are one
    if ( BrowserWindow.getFocusedWindow() ) {

        mainWindowState.saveState( BrowserWindow.getFocusedWindow() );

    }

    var that = this;

    // Create the browser window.
    var currentWindow = new BrowserWindow( {

        x:      mainWindowState.x + 20,
        y:      mainWindowState.y + 20,
        width:  mainWindowState.width,
        height: mainWindowState.height
    } );

    // If ( mainWindowState.isMaximized ) {

    //     CurrentWindow.maximize();

    // }

    // And load the index.html of the app.
    currentWindow.loadURL( "file://" + app.getAppPath() + "/index.html" );

    var id = this.windowsOpened.push( currentWindow ) - 1;

    console.log( "SkoreWindow", "id", id );

    currentWindow.on( "close", function( event ) {

        // Event.preventDefault(); // prevents the window to be closed completely... a bit hardcore

        mainWindowState.saveState( currentWindow );

    } );

    var that = this;

    // Emitted when the window is closed.
    currentWindow.on( "closed", function() {

        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        delete that.windowsOpened[ id ];

    } );

    this.menuBuilder.makeDefault();

    this.menuBuilder.enableMenusForWindow( true );

    currentWindow.on( "focus", function() {

        that.menuBuilder.makeDefault();
        currentWindow.webContents.send( "send-console", "focus", id, that.windowsOpened.length );
        currentWindow.webContents.send( "focused" );

    } );

};

module.exports = SkoreWindow;
