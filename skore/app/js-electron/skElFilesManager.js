
/*
 * Recently opened and "currently opened" files
 *
 */

"use strict";

var app = require( "electron" ).app;
var jetpack = require( "fs-jetpack" );

function filesManager () {

    this.userDataDir = jetpack.cwd( app.getPath( "userData" ) );
    this.fileName = "files.json";

    var files = this.userDataDir.read( this.fileName, "json" ) || {
        recentlyOpened: [],
        currentlyOpened: []
    };

    this.recentlyOpened = files.recentlyOpened;
    this.currentlyOpened = files.currentlyOpened;

}

filesManager.prototype.fileName = null;
filesManager.prototype.userDataDir = null;
filesManager.prototype.recentlyOpened = null;
filesManager.prototype.currentlyOpened = null;

filesManager.prototype.fileHasBeenOpened = function( path ) {

    var f = this.recentlyOpened.indexOf( path );
    if ( f  >= 0 ) {

        this.recentlyOpened.splice( f, 1 );

    };
    this.recentlyOpened.unshift( path );

    f = this.currentlyOpened.indexOf( path );
    if ( f < 0 ) {

        this.currentlyOpened.unshift( path );

    };

    this.save();

};

filesManager.prototype.fileClosed = function( path ) {

    var f = this.currentlyOpened.indexOf( path );
    if ( f >= 0 ) {

        this.currentlyOpened.splice( f, 1 );

    };

    this.save();

};

filesManager.prototype.save = function() {

    var that = this;
    this.userDataDir.write( this.fileName, {
        recentlyOpened: that.recentlyOpened,
        currentlyOpened: that.currentlyOpened
    }, { atomic:true } );

};

module.exports = filesManager;
