
"use strict";

var electron = require( "electron" );
var app = electron.app;
var ipc = electron.ipcMain;
var dialog = electron.dialog;
var BrowserWindow = electron.browser;
var process = require( "process" );
var nodeShell = electron.shell;
var Menu = electron.Menu;
var MenuItem = electron.MenuItem;

var fs = require( "fs" );
var fsj = require( "fs-jetpack" );

var SkoreSaver = require( "./skElSkoreSaver" );

function SkoreMenuBuilder( windowManager, skoreSaver ) {

  this.windowManager = windowManager;
  this.skoreSaver = skoreSaver;

  this.menuToDisable = [];
  this.hideWhenNoWindow = [];
  this.graphOnlyMenu = [];
  this.skoreActionName = [];
  this.toDisableWhileTypingText = [];

  var that = this;

  // Builds the menu

  this.menu = this.buildMe();

};

SkoreMenuBuilder.prototype.windowManager = null;
SkoreMenuBuilder.prototype.menuToDisable = null;
SkoreMenuBuilder.prototype.menu = null;

SkoreMenuBuilder.prototype.enableMenusForWindow = function( enable ) {

  this.hideWhenNoWindow.forEach( function( e, i ) {

    e.enabled = enable;

  } );

};

SkoreMenuBuilder.prototype.makeDefault = function() {

  Menu.setApplicationMenu( this.menu );

};

SkoreMenuBuilder.prototype.buildMe = function() {

  var that = this;

  var mainMenu = new Menu();

  var macOSSubMenu = null;
  var fileSubMenu = new Menu();
  var editSubMenu = new Menu();
  var formatSubMenu = new Menu();
  var viewSubMenu = new Menu();
  var developerSubMenu = new Menu();
  var windowSubMenu = new Menu();
  var helpSubMenu = new Menu();

  var license = new MenuItem( {
    label: "Enter license",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "modal_registration" );

      }

    }
  } );

  this.hideWhenNoWindow.push( license );

  // Get app version and write in sublabel (only windows, mac has a "about menu")

  var pack = fsj.read( app.getAppPath() + "/package.json", "json" );
  var d = new Date( pack.versionDate );
  var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" ];
  var s = "v"  + pack.version + " (" + d.getDate() + " " + monthNames[ d.getMonth() ] + " " + d.getFullYear() + ")";

  var checkUpdate = new MenuItem( {
    label: "Check for update",
    sublabel: s,
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "update" );

      }

    }
  } );

  this.hideWhenNoWindow.push( checkUpdate );

  /*
   * Mac os menu
   *
   */

   if ( process.platform == "darwin" ) {

    macOSSubMenu = new Menu();

    macOSSubMenu.append( new MenuItem( {
      label: "About Skore app",
      role: "about"
    } ) );

    macOSSubMenu.append( license );

    macOSSubMenu.append( checkUpdate );

    macOSSubMenu.append( new MenuItem( {
      type: "separator"
    } ) );

    macOSSubMenu.append( new MenuItem( {
      label: "Services",
      role: "services"

      // Submenu: []
    } ) );

    macOSSubMenu.append( new MenuItem( {
      type: "separator"
    } ) );
    macOSSubMenu.append( new MenuItem( {
      label: "Hide " + "Skore",
      accelerator: "Command+H",
      role: "hide"
    } ) );
    macOSSubMenu.append( new MenuItem( {
      label: "Hide Others",
      accelerator: "Command+Shift+H",
      role: "hideothers:"
    } ) );
    macOSSubMenu.append( new MenuItem( {
      label: "Show All",
      role: "unhide:"
    } ) );
    macOSSubMenu.append( new MenuItem( {
      type: "separator"
    } ) );
    macOSSubMenu.append( new MenuItem( {
      label: "Quit",
      accelerator: "Command+Q",
      click: function() {

        app.quit();

      }
    } ) );

    mainMenu.append( new MenuItem( {
      label: "Skore",
      submenu: macOSSubMenu
    } ) );

  }

  /*
   * File menu
   *
   */

  // New

  var file_new = new MenuItem( {
    label: "New",
    accelerator: "CmdOrCtrl+N",
    click: function( menuItem, focusedWindow ) {

      that.windowManager.newWindow();

    }
  } );
  fileSubMenu.append( file_new );

  var file_open = new MenuItem( {
    label: "Open",
    accelerator: "CmdOrCtrl+O",
    click: function( item, focusedWindow ) {

      focusedWindow = focusedWindow || null;
      dialog.showOpenDialog(
        focusedWindow,  // Browser window
        {
          title: "Open Skore file",
          filters: [
          { name: "Skore file", extensions:[ "skore" ] },
          { name: "Skore HTML file", extensions: [ "html" ] }
          ],
          properties: [ "openFile" ]
        },
        function( fileNames ) {

          if ( fileNames === undefined ) {

            return;

          }

          var filePath = fileNames[ 0 ];

          // Var data = fsj.read(fileName, 'utf-8');

          if ( focusedWindow ) {

              // Deciding to open a new window or not is done in the client
              focusedWindow.webContents.send( "executeCommandFromOSMenu", "open", filePath );

            } else {

              global.filePathToBeLoaded = filePath;
              that.windowManager.newWindow();

            }

          }
      ); // End showOpenDialog

    } // Ekd click
  } ); // End open)

  fileSubMenu.append( file_open );

  fileSubMenu.append( new MenuItem( {
    type:"separator"
  }
  )
  );

  /*
   * Save
   *
   */

   var file_save = new MenuItem( {
    label: "Save",
    accelerator: "CmdOrCtrl+S",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        that.skoreSaver.save( focusedWindow );

      };

    } // End click;
  } );
   fileSubMenu.append( file_save );
   this.menuToDisable.push( file_save );
   this.hideWhenNoWindow.push( file_save );

   var file_saveAs = new MenuItem( {

    label: "Save as...",
    accelerator: "CmdOrCtrl+Shift+S",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        that.skoreSaver.saveAs( focusedWindow ); // No path so it prompts for dialog

      }

    }
  } );
   fileSubMenu.append( file_saveAs );
   this.menuToDisable.push( file_saveAs );
   this.hideWhenNoWindow.push( file_saveAs );

   var file_saveHTML = new MenuItem( {

    label: "Save as HTML File",
    sublabel: "Standalone, interactive, Skore file to share with others",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        that.skoreSaver.saveAs( focusedWindow, "html" );

      }

    }

  } );
   fileSubMenu.append( file_saveHTML );
   this.menuToDisable.push( file_saveHTML );
   this.hideWhenNoWindow.push( file_saveHTML );

   var file_saveImage = new MenuItem( {
    label: "Save as image",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "modal_imageexport" );

      }

    }
  } );
   fileSubMenu.append( file_saveImage );
   this.hideWhenNoWindow.push( file_saveImage );
   this.graphOnlyMenu.push( file_saveImage );

   fileSubMenu.append( new MenuItem( {
    type:"separator"
  }
  )
   );

   var file_printPreview = new MenuItem( {
    label: "Print...",
    accelerator: "CmdOrCtrl+P",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "modal_print" );

      }

    }
  } );
   fileSubMenu.append( file_printPreview );
   this.hideWhenNoWindow.push( file_printPreview );
   this.graphOnlyMenu.push( file_printPreview );

   fileSubMenu.append( new MenuItem( {
    type:"separator"
  }
  )
   );

   var file_close = new MenuItem( {
    label: "Close",
    accelerator: "CmdOrCtrl+W",
    role: "close"

  } );
   fileSubMenu.append( file_close );
   this.hideWhenNoWindow.push( file_close );

   var file = new MenuItem( {
    label: "File",
    submenu: fileSubMenu
  } );

   mainMenu.append( file );

  /*
   * EDIT
   *
   */

   var edit_undo = new MenuItem( {
    label: "Undo",
    accelerator: "CmdOrCtrl+Z",
    role: "undo",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "undo" );

      }

    }
  } );
   editSubMenu.append( edit_undo );
   this.hideWhenNoWindow.push( edit_undo );
   this.graphOnlyMenu.push( edit_undo );

   var edit_redo = new MenuItem( {
    label: "Redo",
    accelerator: "CmdOrCtrl+Shift+Z",
    role: "redo",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "redo" );

      }

    }
  } );
   editSubMenu.append( edit_redo );
   this.hideWhenNoWindow.push( edit_redo );
   this.graphOnlyMenu.push( edit_redo );

   editSubMenu.append( new MenuItem( {
    type: "separator"
  }
  )
   );

   var copy = new MenuItem( {
    label: "Copy",
    accelerator: "CmdOrCtrl+C",
    role: "copy"
  } );
   editSubMenu.append( copy );
   this.hideWhenNoWindow.push( copy );
   this.graphOnlyMenu.push( copy );
   this.skoreActionName.copy = copy;

   var cut = new MenuItem( {
    label: "Cut",
    accelerator: "CmdOrCtrl+X",
    role: "cut"
  } );
   editSubMenu.append( cut );
   this.hideWhenNoWindow.push( cut );
   this.graphOnlyMenu.push( cut );
   this.skoreActionName.cut = cut;

   var paste = new MenuItem( {
    label: "Paste",
    accelerator: "CmdOrCtrl+V",
    role: "paste"
  } );
   editSubMenu.append( paste );
   this.hideWhenNoWindow.push( paste );
   this.graphOnlyMenu.push( paste );
   this.skoreActionName.paste = paste;

   var del = new MenuItem( {
    label: "Delete",
    accelerator: "Backspace",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "delete" );

      }

    }
  } );
   editSubMenu.append( del );
   this.hideWhenNoWindow.push( del );
   this.graphOnlyMenu.push( del );
   this.skoreActionName.delete = del;

   var selectall = new MenuItem( {
    label: "Select all",
    accelerator: "CmdOrCtrl+A",
    role: "selectall",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "selectAll" );

      }

    }
  } );
   editSubMenu.append( selectall );
   this.hideWhenNoWindow.push( selectall );
   this.graphOnlyMenu.push( selectall );
   this.skoreActionName.selectall = selectall;

   var edit = new MenuItem( {
    label: "Edit",
    submenu: editSubMenu
  } );

   mainMenu.append( edit );
   this.hideWhenNoWindow.push( edit );

  /*
   * Format
   *
   */

   var group = new MenuItem( {
    label: "Group",
    accelerator: "CmdOrCtrl+G",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "group" );

      }

    }
  } );
   formatSubMenu.append( group );
   this.hideWhenNoWindow.push( group );
   this.graphOnlyMenu.push( group );
   this.skoreActionName.group = group;

   var ungroup = new MenuItem( {
    label: "Ungroup",
    accelerator: "CmdOrCtrl+U",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "ungroup" );

      }

    }
  } );
   formatSubMenu.append( ungroup );
   this.hideWhenNoWindow.push( ungroup );
   this.graphOnlyMenu.push( ungroup );
   this.skoreActionName.ungroup = ungroup;

  formatSubMenu.append( new MenuItem( {
      type: "separator"
    } ) );

   var aligntop = new MenuItem( {
    label: "Align tops (horizontal)",
    accelerator: "T",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "alignTop" );

      }

    }
  } );
   formatSubMenu.append( aligntop );
   this.hideWhenNoWindow.push( aligntop );
   this.graphOnlyMenu.push( aligntop );
   this.skoreActionName.aligntop = aligntop;
   this.toDisableWhileTypingText.push( aligntop );

   var aligncenter = new MenuItem( {
    label: "Align centers (vertical)",
    accelerator: "C",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "alignCenter" );

      }

    }
  } );
   formatSubMenu.append( aligncenter );
   this.hideWhenNoWindow.push( aligncenter );
   this.graphOnlyMenu.push( aligncenter );
   this.toDisableWhileTypingText.push( aligncenter );
   this.skoreActionName.aligncenter = aligncenter ;

  formatSubMenu.append( new MenuItem( {
      type: "separator"
    } ) );

   var bringfront = new MenuItem( {
    label: "Bring to front",
    accelerator: "F",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "bringFront" );

      }

    }
  } );
   formatSubMenu.append( bringfront );
   this.hideWhenNoWindow.push( bringfront );
   this.graphOnlyMenu.push( bringfront );
   this.toDisableWhileTypingText.push( bringfront );
   this.skoreActionName.bringfront = bringfront ;

   var sendback = new MenuItem( {
    label: "Send to back",
    accelerator: "B",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "sendBack" );

      }

    }
  } );
   formatSubMenu.append( sendback );
   this.hideWhenNoWindow.push( sendback );
   this.graphOnlyMenu.push( sendback );
   this.toDisableWhileTypingText.push( sendback );
   this.skoreActionName.sendback = sendback;

   formatSubMenu.append( new MenuItem( {
      type: "separator"
    } ) );

   var distributehorizontal = new MenuItem( {
    label: "Space out horizontally",
    accelerator: "H",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "distributeHorizontal" );

      }

    }
  } );

   formatSubMenu.append( distributehorizontal );
   this.hideWhenNoWindow.push( distributehorizontal );
   this.graphOnlyMenu.push( distributehorizontal );
   this.toDisableWhileTypingText.push( distributehorizontal );
   this.skoreActionName.distributehorizontal = distributehorizontal;

   var distributevertical = new MenuItem( {
    label: "Space out vertically",
    accelerator: "H",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "distributeVertical" );

      }

    }
  } );
   formatSubMenu.append( distributevertical );
   this.hideWhenNoWindow.push( distributevertical );
   this.graphOnlyMenu.push( distributevertical );
   this.toDisableWhileTypingText.push( distributevertical );
   this.skoreActionName.distributevertical = distributevertical;

   var format = new MenuItem( {
    label: "Format",
    submenu: formatSubMenu
  } );

   mainMenu.append( format );
   this.hideWhenNoWindow.push( format );

  /*
   * View
   *
   */

   var zoomin = new MenuItem( {
    label: "Bigger",
    accelerator: "CmdOrCtrl+Plus",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "zoomIn" );

      }

    }
  } );
   viewSubMenu.append( zoomin );
   this.hideWhenNoWindow.push( zoomin );
   this.graphOnlyMenu.push( zoomin );
   this.skoreActionName.zoomin = zoomin;

   var zoomout = new MenuItem( {
    label: "Smaller",
    accelerator: "CmdOrCtrl+-",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "zoomOut" );

      }

    }
  } );

  viewSubMenu.append( zoomout );
  this.hideWhenNoWindow.push( zoomout );
  this.graphOnlyMenu.push( zoomout );
  this.skoreActionName.zoomout = zoomout;

   var zoomreset = new MenuItem( {
    label: "Standard size",
    accelerator: "=",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "zoomReset" );

      }

    }
  } );
   viewSubMenu.append( zoomreset );
   this.hideWhenNoWindow.push( zoomreset );
   this.graphOnlyMenu.push( zoomreset );
   this.skoreActionName.zoomreset = zoomreset ;

   var zoomfit = new MenuItem( {
    label: "Fit content to window",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "zoomFit" );

      }

    }
  } );
   viewSubMenu.append( zoomfit );
   this.hideWhenNoWindow.push( zoomfit );
   this.graphOnlyMenu.push( zoomfit );
   this.skoreActionName.zoomfit = zoomfit;

  viewSubMenu.append( new MenuItem( {
    type: "separator"
  }
  )
  );

  var toggleStickynotes = new MenuItem( {
    label: "Show / Hide sticky notes",
    accelerator: "CmdOrCtrl+K",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "toggleStickynotes" );

      }

    }
  } );
  viewSubMenu.append( toggleStickynotes );
  this.hideWhenNoWindow.push( toggleStickynotes );
  this.graphOnlyMenu.push( toggleStickynotes );

  viewSubMenu.append( new MenuItem( {
    type: "separator"
  }
  )
  );

  var fullScreen = new MenuItem( {
    label: "Toggle full screen",
    accelerator: ( function() {

      if ( process.platform == "darwin" ) {

        return "Ctrl+Command+F";

      } else {

        return "F11";

      }

    }
    )(),
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.setFullScreen( !focusedWindow.isFullScreen()
          );

      }

    }
  } );
  viewSubMenu.append( fullScreen );
  this.hideWhenNoWindow.push( fullScreen );

  var view = new MenuItem( {
    label: "View",
    submenu: viewSubMenu
  } );
  mainMenu.append( view );
  this.hideWhenNoWindow.push( view );

  /*
   * Window
   *
   */

   if ( process.platform === "darwin" ) {

    windowSubMenu.append( new MenuItem( {
      type:"separator"
    } ) );

    windowSubMenu.append( new MenuItem( {
      label: "Bring All to Front",
      role: "front"
    } ) );

    var win = new MenuItem( {
      label: "Window",
      submenu: windowSubMenu
    }
    );

    mainMenu.append( win );

  }

  /*
   * Developer
   *
   */

   var DEV = fsj.read( app.getAppPath() + "/config/env_dev.json", "json" );

   console.log( "DEV : ", DEV, DEV && DEV.developerMenu );

   if ( DEV && DEV.developerMenu ) {

    var reload = new MenuItem( {
      label: "Reload",
      accelerator: "CmdOrCtrl+R",
      click: function( item, focusedWindow ) {

        if ( focusedWindow ) {

          // focusedWindow.reload();
          focusedWindow.webContents.reloadIgnoringCache();

        }

      }
    }
    );
    developerSubMenu.append( reload );
    this.hideWhenNoWindow.push( reload );

    var toggleDevTools = new MenuItem( {
      label: "Toggle Developer Tools",
      accelerator: ( function() {

        if ( process.platform == "darwin" ) {

          return "Alt+Command+I";

        } else {

          return "Ctrl+Shift+I";

        }

      }
      )(),
      click: function( item, focusedWindow ) {

        if ( focusedWindow ) {

          focusedWindow.toggleDevTools();

        }

      }
    }
    );
    developerSubMenu.append( toggleDevTools );
    this.hideWhenNoWindow.push( toggleDevTools );

    var dev = new MenuItem( {
      label: "Developer",
      submenu: developerSubMenu
    }
    );

    mainMenu.append( dev );
    this.hideWhenNoWindow.push( dev );

  }

  /*
   * Help
   *
   */

   var getSkoreDotCom = new MenuItem( {
    label: "Visit getSkore.com",
    click: function() {

      nodeShell.openExternal( "https://www.getskore.com" );

    }
  } );
   helpSubMenu.append( getSkoreDotCom );

   var tutorial = new MenuItem( {
    label: "Start / Stop tutorial",
    click: function( item, focusedWindow ) {

      if ( focusedWindow ) {

        focusedWindow.webContents.send( "executeCommandFromOSMenu", "tutorial" );

      }

    }
  } );
   helpSubMenu.append( tutorial );
   this.hideWhenNoWindow.push( tutorial );
   this.graphOnlyMenu.push( tutorial );

   helpSubMenu.append( license );

   helpSubMenu.append( checkUpdate );

   var help = new MenuItem( {
    label: "Help",
    role: "help",
    submenu: helpSubMenu
  } );
   mainMenu.append( help );

   return mainMenu;

 };

 module.exports = SkoreMenuBuilder;
