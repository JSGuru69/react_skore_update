
"use strict";

var windowStateKeeper = require( "./window_state" );

var electron = require( "electron" );
var app = electron.app;

var fs = require( "fs" );
var fsj = require( "fs-jetpack" );
var BrowserWindow = electron.BrowserWindow;
var dialog = electron.dialog;
var ipcMain = electron.ipcMain;

var skoreSaver = function() {

    var that = this;

    ipcMain.on( "hereAreTheThingsToSave", function( event, params ) {

        var param = JSON.parse( params );

        console.log( "hereAreTheThingsToSave", params );

        if ( !param.silent && ( that.isSaveAs || !param.filePath ) ) {

            var myFilters = [];

            if ( param.thumbnail ) {

                myFilters.push( { name: "Skore HTML", extensions: [ "html" ] } );

            } else {

                myFilters.push( { name: "Skore", extensions: [ "skore" ] } );

            }

            // Asks where to Save
            var suggestedFileName = "Name your Skore";

            if ( param.filePath ) {

                var tmp = param.filePath.split( /[\\/]/ ).pop();

                console.log( "file : ", tmp, "thumbnail", param.thumbnail );

                if ( param.thumbnail && tmp.split( "." ).pop() === "skore" ) {

                    suggestedFileName = tmp.split( "." ).pop() + ".html";

                } else {

                    suggestedFileName = tmp;

                }

            } else if ( !param.filePath ) {

                suggestedFileName = app.getPath( "documents" ) + "/" + param.currentTitle;

            }

            dialog.showSaveDialog(
                null,
                {
                    title:"Save Skore file",
                    defaultPath: suggestedFileName,
                    filters: myFilters
                },
                function( fileName ) {

                    if ( fileName === undefined ) {

                        if ( param.actionToDoAfter === "closeMeNow" ) {

                            event.returnValue = "dontClose";
                            return;

                        }
                        return;

                    }

                    param.filePath = fileName;

                    that.saveNow( event, param );

                    if ( param.actionToDoAfter === "closeMeNow" ) {

                        event.returnValue = "closeMeNow";

                    }

                }
            ); // End showSaveDialog

        } else if ( param.silent ) {

            if ( param.filePath ) {

                that.saveNow( event, param );

            }

        } else {

            that.saveNow( event, param );

            if ( param.actionToDoAfter === "closeMeNow" ) {

                event.returnValue = "closeMeNow";

            }

        }

    } );

};

skoreSaver.prototype.saveAs = function( focusedWindow, type ) {

  focusedWindow.webContents.send( "giveMeStuffToSave", type );
  this.isSaveAs = true;

};

skoreSaver.prototype.save = function( focusedWindow, type, silent ) {

    silent = silent || false;

    focusedWindow.webContents.send( "giveMeStuffToSave", type, silent );
    this.isSaveAs = false;

};

skoreSaver.prototype.isEmptyString = function( str ) {

    if ( typeof str == "undefined" || !str || str.length === 0 || str === "" ||
        !/[^\s]/.test( str ) || /^\s*$/.test( str ) || str.replace( /\s/g, "" ) === "" ) {

        return true;

    } else {

        return false;

    }

};

skoreSaver.prototype.isSaveAs = false;
skoreSaver.prototype.isHTML = false;

skoreSaver.prototype.saveNow = function( event, params ) {

    if ( params.thumbnail ) {

        // This is html
        var html = fs.readFileSync( app.getAppPath() + "/template_exportHtml.html", "utf-8" );

        if ( !params.htmlServer ) {

            params.htmlServer = "https://justskore.it/?embed";

        }

        params.data = html
            .replace( "{%xml%}", params.data )
            .replace( "{%fulltext%}", params.fulltext )
            .replace( "{%thumbnail%}", params.thumbnail )
            .replace( "{%htmlserver%}", params.htmlServer );

    }

    // HERE !!! if params.data is empty, do not save !!! warn user that a problem occured
    if ( !this.isEmptyString( params.data ) ) {

        fsj.writeAsync( params.filePath, params.data ).then(

            function( err ) {

                if ( err ) {

                    event.sender.send( "fileSaved", err, params.filePath );

                } else {

                    event.sender.send( "fileSaved", "ok", params.filePath, params.mode );

                    if ( params.mode !== "social" ) {

                        app.addRecentDocument( params.filePath );

                    }

                }

            }

        );

    } else {

        event.sender.send( "fileSaved", "error", params.filePath, params.mode );

    }

};

module.exports = skoreSaver;
