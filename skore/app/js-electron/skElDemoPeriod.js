
/*jshint strict:false */

"use strict";

/*
 * The most insecure license check of the world of software
 *
 */

var electron = require( "electron" );
var app = electron.app;
var jetpack = require( "fs-jetpack" );
var BrowserWindow = electron.BrowserWindow;

module.exports = function() {

    var saveFirstUseDate = function() {

        var userDataDir = jetpack.cwd( app.getPath( "userData" ) );
        var stateStoreFile = "skore.json";

        var a = new Date().getTime();

        userDataDir.write( stateStoreFile, { code:a }, { atomic: true } );

        return a;

    };

    var getFirstUseDate = function() {

        var userDataDir = jetpack.cwd( app.getPath( "userData" ) );
        var stateStoreFile = "skore.json";

        var obj = userDataDir.read( stateStoreFile, "json" );

        if ( obj === null ) {

            return saveFirstUseDate();

        }

        return obj.code;

    };

    return {
        getFirstUseDate: getFirstUseDate
    };

};
