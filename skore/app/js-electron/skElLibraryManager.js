
"use strict";

// var _ = require( "lodash" );
var fsj = require( "fs-jetpack" );
var app = require( "electron" ).app;
/*
 * will manage the items on the server
 *
 */

var skLibrary = function( libraryName ) {

	/*
	Normal array

	[
		{
			remoteid: id,
			name: "", // main" name or value of the item,
			... // any property....
	 	}
	]

	*/

	// load the items

	this.libraryName = libraryName;

	this.loadLibrary();
	this.saveLibrary();

};

skLibrary.prototype.items = null;
skLibrary.prototype.libraryName = null;
skLibrary.prototype.libraryPrefix = "skore-library-";

skLibrary.prototype.loadLibrary = function() {

	var userDataDir = fsj.cwd( app.getPath( "userData" ) );
    var file = "" + this.libraryPrefix + this.libraryName + ".json";
    var obj = userDataDir.read( file, "json" ) || [];

    this.items = obj;

    return this.items;

};

skLibrary.prototype.saveLibrary = function() {

	var userDataDir = fsj.cwd( app.getPath( "userData" ) );
    var file = "" + this.libraryPrefix + this.libraryName + ".json";
    userDataDir.write( file, JSON.stringify( this.items ), { atomic: true } );

};

/*
 * adds the item or return the existing item
 *
 */

skLibrary.prototype.addItemByName = function( name ) {

	var item = {
		name: name
	};

	var _id = this.items.push( item );
	item.remoteid = _id	- 1;

	return item;

};

skLibrary.prototype.getItemById = function( remoteid ) {

	remoteid = parseInt( remoteid, 10 );
	if ( !isNaN( remoteid ) ) {

		var i = 0, len = this.items.length;
		for ( i; i < len; i++ ) {

			if ( this.items[ i ].remoteid === remoteid ) {

				return this.items[ i ];

			}

		}

	} else {

		return {
			remoteid: remoteid
			err: {
				message: "remoteid is NaN : " + remoteid
			}
		};

	}

};

module.exports = skLibrary;
