// Var electron = require("electron");
var app = require( "electron" ).app; // Module to control application life.
var fs = require( "fs" );
var BrowserWindow = require( "electron" ).BrowserWindow; // Module to create native browser window.
var ipcMain = require( "electron" ).ipcMain;

console.log( "enter background.js" );

var SkoreSaver = require( "./js-electron/skElSkoreSaver" );
var SkoreWindow = require( "./js-electron/skElSkoreWindow" );
var licenseCodeCheck = require( "./js-electron/skElCode" );
global.filePathToBeLoaded = "";

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
// var mainWindow = null;
var windowsOpened = [];

var skoreSaver = new SkoreSaver();
var skoreWindow = new SkoreWindow( windowsOpened, skoreSaver );

/*
 * Catch the file launched...
 *
 *
 *********************/

/*
 * For mac use the app open-file event
 *
 */

app.on( "open-file", function( event, path ) {

    console.log( "open-file", path );

    event.preventDefault();

    global.filePathToBeLoaded = path;

    if ( app.isReady() ) {

        skoreWindow.newWindow();

    }

} );

/*
 * For WIN use argv
 *
 */

if ( process.platform !== "darwin" ) {

    var file = process.argv[ process.argv.length - 1 ];
    var ext = file.split( "." ).pop();

    if ( ext === "skore" || ext === "html" ) {

        global.filePathToBeLoaded = file;

    }

}

app.on( "ready", function() {

    skoreWindow.newWindow();

    var screen = require( "electron" ).screen;

    screen.on( "display-removed", function() {

        skoreWindow.ensureVisibleOnSomeDisplay();

    } );

    // When goes to sleep
    // require( "power-monitor" ).on( "suspend", function() {

    //     console.log( "power-monitor", windowsOpened.length, "windows opened" );

    //     // For each window, save if filename known
    //     windowsOpened.forEach( function( e ) {

    //         skoreSaver.save( e, null, true );

    //         // SkoreWindow.skoreSaver.save(e, null, true);

    //     } );

    // } );

} );

ipcMain.on( "readyToLoadFileContent", function( event ) {

    console.log( "event", "readyToLoadFileContent", global.filePathToBeLoaded );

    event.returnValue = global.filePathToBeLoaded || "";

    // Remove reference for further file open...
    global.filePathToBeLoaded = "";

} );

ipcMain.on( "newskorewindow", function( event, filePath ) {

    console.log( "event", "newskorewindow", filePath );

    // That.dataToBeLoaded = arg1;
    global.filePathToBeLoaded = filePath;
    skoreWindow.newWindow();

} );

/*
 * Listener to enable / disable save
 *
 */

ipcMain.on( "enableSave", function( event, enable ) {

    // Todo
    skoreWindow.menuBuilder.menuToDisable.forEach( function( e, i ) {

        e.enabled = enable;

    } );

} );

ipcMain.on( "enableGraphMenu", function( event, enable ) {

    skoreWindow.menuBuilder.graphOnlyMenu.forEach( function( e, i ) {

        e.enabled = enable;

    } );

} );

ipcMain.on( "disableWhileTypingText", function( event, allowed ) {

    console.send( "disableWhileTypingText " + allowed );

    skoreWindow.menuBuilder.toDisableWhileTypingText.forEach( function( action ) {

        action.enabled = allowed;

    } );

} );

ipcMain.on( "enableMenuItem", function( event, actionName, enabled ) {

    if ( typeof actionName === "string" ) {

        actionName = [ actionName ];

    }

    actionName.forEach( function( action ) {

        var menuItem = skoreWindow.menuBuilder.skoreActionName[ action ];
        if ( menuItem ) {

            menuItem.enabled = enabled;

        }

    } );

} );

// Report crashes to our server.
// require( "crash-reporter" ).start();

/**
 * @param {browser-window} win, target window to send console log message two.
 * @param {String} msg, the message we are sending.
 */
console.send = function( msg ) {

    if ( BrowserWindow.getFocusedWindow() ) {

        BrowserWindow.getFocusedWindow().webContents.send( "send-console", "main : " + msg );

    }

};

// Quit when all windows are closed.
app.on( "window-all-closed", function() {

    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if ( process.platform != "darwin" ) {

        app.quit();

    }
    skoreWindow.menuBuilder.enableMenusForWindow( false );

} );

function createPrintPreview( arg ) {

    // Create the browser window.
    var currentWindow = new BrowserWindow( {
        width: 800,
        height: 1200
    } );

    // And load the index.html of the app.
    currentWindow.loadUrl( "file://" + __dirname + "/template_printPreview.html" );

    var id = windowsOpened.push( currentWindow ) - 1;

    currentWindow.on( "closed", function() {

        delete windowsOpened[ id ];

    } );

    currentWindow.webContents.on( "did-stop-loading", function() {

        currentWindow.webContents.send( "loadprintpreview", arg );

    } );

}

ipcMain.on( "newPrintPreview", function( event, arg ) {

    console.send( event.sender, "new print preview" );

    createPrintPreview( arg );

} );

ipcMain.on( "getLicenseCode", function( event, arg ) {

    var a = licenseCodeCheck();
    var code = a.getSavedCode().code;
    event.sender.send( "getLicenseCodeAnswer", code );

} );

ipcMain.on( "saveLicenseCode", function( event, arg ) {

    var a = licenseCodeCheck();
    a.saveCode( arg );
    event.sender.send( "saveLicenseCodeAnswer", "ok" );

} );

ipcMain.on( "checkLicense", function( event, arg ) {

    var a = licenseCodeCheck();
    var result = a.checkCode( arg );

    event.sender.send( "checkLicenseAnswer", result.answer, result.expiry.toString() );

} );

